/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.customercouponservices.cronjob;

import de.hybris.platform.core.model.ItemModel;
import de.hybris.platform.customercouponservices.constants.CustomercouponservicesConstants;
import de.hybris.platform.customercouponservices.model.CouponNotificationModel;
import de.hybris.platform.notificationservices.enums.NotificationChannel;
import de.hybris.platform.notificationservices.service.NotificationService;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * A task to send notification to customers
 */
public class CouponNotificationTask implements Runnable
{
	private final NotificationService notificationService;
	private final Map data;
	private final CouponNotificationModel couponNotification;

	public CouponNotificationTask(final NotificationService notificationService, final Map data)
	{
		this.notificationService = notificationService;
		this.data = data;
		this.couponNotification = (CouponNotificationModel) data.get(CustomercouponservicesConstants.COUPON_NOTIFICATION);
	}

	@Override
	public void run()
	{
		final ItemModel notifycationType = (ItemModel) data.get(CustomercouponservicesConstants.NOTIFICATION_TYPE);
		notificationService.notifyCustomer(notifycationType.getProperty(CustomercouponservicesConstants.NOTIFICATION_TYPE),
				couponNotification.getCustomer(), channels(), data);
	}

	protected Set<NotificationChannel> channels()
	{
		final Set<NotificationChannel> channels = new HashSet<>();

		if (couponNotification.getEmailEnabled().booleanValue())
		{
			channels.add(NotificationChannel.EMAIL);
		}

		if (couponNotification.getSmsEnabled().booleanValue())
		{
			channels.add(NotificationChannel.SMS);
		}
		return channels;
	}
}
