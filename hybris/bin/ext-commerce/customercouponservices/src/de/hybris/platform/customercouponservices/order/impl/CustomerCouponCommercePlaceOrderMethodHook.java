/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.customercouponservices.order.impl;

import java.util.Collection;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Required;

import de.hybris.platform.commerceservices.order.hook.CommercePlaceOrderMethodHook;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.commerceservices.service.data.CommerceOrderResult;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.customercouponservices.CustomerCouponService;
import de.hybris.platform.customercouponservices.order.CustomerCouponsPlaceOrderStrategy;
import de.hybris.platform.order.InvalidCartException;


public class CustomerCouponCommercePlaceOrderMethodHook implements CommercePlaceOrderMethodHook,
		CustomerCouponsPlaceOrderStrategy
{

	private CustomerCouponService customerCouponService;

	@Override
	public void removeCouponsForCustomer(final UserModel currentUser, final OrderModel order)
	{
		final CustomerModel customer = (CustomerModel) currentUser;
		final Collection<String> appliedCoupons = order.getAppliedCouponCodes();

		if (CollectionUtils.isNotEmpty(appliedCoupons))
		{
			appliedCoupons.forEach(couponCode -> getCustomerCouponService().removeCouponForCustomer(couponCode, customer));
		}

	}

	@Override
	public void afterPlaceOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		final CartModel cartModel = parameter.getCart();
		final UserModel currentUser = cartModel.getUser();

		final OrderModel order = result.getOrder();
		removeCouponsForCustomer(currentUser, order);

	}

	@Override
	public void beforePlaceOrder(final CommerceCheckoutParameter parameter) throws InvalidCartException
	{
		// not implemented

	}

	@Override
	public void beforeSubmitOrder(final CommerceCheckoutParameter parameter, final CommerceOrderResult result)
			throws InvalidCartException
	{
		// not implemented

	}

	protected CustomerCouponService getCustomerCouponService()
	{
		return customerCouponService;
	}

	@Required
	public void setCustomerCouponService(final CustomerCouponService customerCouponService)
	{
		this.customerCouponService = customerCouponService;
	}

}
