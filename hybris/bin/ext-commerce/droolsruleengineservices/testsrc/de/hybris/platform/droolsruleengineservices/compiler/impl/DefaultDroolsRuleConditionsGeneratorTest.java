/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.droolsruleengineservices.compiler.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.droolsruleengineservices.compiler.DroolsRuleGeneratorContext;
import de.hybris.platform.droolsruleengineservices.compiler.DroolsRuleValueFormatter;
import de.hybris.platform.ruleengineservices.compiler.RuleIr;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrAttributeRelCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrExistsCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrGroupOperator;
import de.hybris.platform.ruleengineservices.compiler.RuleIrLocalVariablesContainer;
import de.hybris.platform.ruleengineservices.compiler.RuleIrNotCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrTypeCondition;
import de.hybris.platform.ruleengineservices.compiler.RuleIrVariable;
import de.hybris.platform.ruleengineservices.rao.CartRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryGroupRAO;
import de.hybris.platform.ruleengineservices.rao.OrderEntryRAO;
import de.hybris.platform.ruleengineservices.rao.ProductRAO;
import de.hybris.platform.ruleengineservices.rao.RuleEngineResultRAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultDroolsRuleConditionsGeneratorTest extends AbstractGeneratorTest
{
	private static final String ORDER_ENTRY_CLASS_NAME = "OrderEntryRAO";
	private static final String ORDER_ENTRY_VARIABLE_NAME = "orderEntry";
	private static final String INDENTATION = "  ";
	private static final String VARIABLE_PREFIX = "$";
	private static final String ATTRIBUTE_DELIMITER = ".";

	private static final String CART_VARIABLE_NAME = "cart";
	private static final String CART_VARIABLE_CLASS_NAME = "CartRAO";

	private static final String ENTRYGROUP_VARIABLE_NAME = "entryGroup";
	private static final String ENTRYGROUP_VARIABLE_CLASS_NAME = "OrderEntryGroupRAO";

	private static final String PRODUCT_VARIABLE_NAME = "product";
	private static final String PRODUCT_VARIABLE_CLASS_NAME = "ProductRAO";

	private static final String RESULT_VARIABLE_NAME = "result";
	private static final String RESULT_VARIABLE_CLASS_NAME = "RuleEngineResultRAO";

	private static final String ACTION_CONTEXT_CLASS_NAME = "DefaultDroolsRuleActionContext";

	@Mock
	private DroolsRuleGeneratorContext droolsRuleGeneratorContext;

	@Mock
	private DroolsRuleValueFormatter droolsRuleValueFormatter;

	private RuleIrVariable cartVariable;
	private RuleIrVariable productVariable;
	private RuleIrVariable resultVariable;
	private RuleIrVariable orderEntryVariable;
	private RuleIrVariable entryGroupVariable;

	private RuleIr ruleIr;
	private Map<String, RuleIrVariable> ruleIrVariables;
	private RuleIrTypeCondition ruleIrResultCondition;

	private DefaultDroolsRuleConditionsGenerator conditionsGenerator;

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);

		cartVariable = new RuleIrVariable();
		cartVariable.setName(CART_VARIABLE_NAME);
		cartVariable.setType(CartRAO.class);

		productVariable = new RuleIrVariable();
		productVariable.setName(PRODUCT_VARIABLE_NAME);
		productVariable.setType(ProductRAO.class);

		orderEntryVariable = new RuleIrVariable();
		orderEntryVariable.setName(ORDER_ENTRY_VARIABLE_NAME);
		orderEntryVariable.setType(OrderEntryRAO.class);

		entryGroupVariable = new RuleIrVariable();
		entryGroupVariable.setName(ENTRYGROUP_VARIABLE_NAME);
		entryGroupVariable.setType(OrderEntryGroupRAO.class);

		resultVariable = new RuleIrVariable();
		resultVariable.setName(RESULT_VARIABLE_NAME);
		resultVariable.setType(RuleEngineResultRAO.class);

		ruleIr = new RuleIr();
		ruleIrVariables = new LinkedHashMap<>();

		ruleIrResultCondition = new RuleIrTypeCondition();
		ruleIrResultCondition.setVariable(RESULT_VARIABLE_NAME);

		when(droolsRuleGeneratorContext.getIndentationSize()).thenReturn(INDENTATION);
		when(droolsRuleGeneratorContext.getVariablePrefix()).thenReturn(VARIABLE_PREFIX);
		when(droolsRuleGeneratorContext.getAttributeDelimiter()).thenReturn(ATTRIBUTE_DELIMITER);
		when(droolsRuleGeneratorContext.getRuleIr()).thenReturn(ruleIr);
		when(droolsRuleGeneratorContext.getVariables()).thenReturn(ruleIrVariables);
		when(droolsRuleGeneratorContext.getLocalVariables()).thenReturn(new ArrayDeque<>());
		when(droolsRuleGeneratorContext.generateClassName(CartRAO.class)).thenReturn(CART_VARIABLE_CLASS_NAME);
		when(droolsRuleGeneratorContext.generateClassName(ProductRAO.class)).thenReturn(PRODUCT_VARIABLE_CLASS_NAME);
		when(droolsRuleGeneratorContext.generateClassName(RuleEngineResultRAO.class)).thenReturn(RESULT_VARIABLE_CLASS_NAME);
		when(droolsRuleGeneratorContext.generateClassName(OrderEntryRAO.class)).thenReturn(ORDER_ENTRY_CLASS_NAME);
		when(droolsRuleGeneratorContext.generateClassName(OrderEntryGroupRAO.class)).thenReturn(ENTRYGROUP_VARIABLE_CLASS_NAME);

		conditionsGenerator = new DefaultDroolsRuleConditionsGenerator();
		conditionsGenerator.setDroolsRuleValueFormatter(droolsRuleValueFormatter);
	}

	@Test
	public void testSingleNotCondition() throws Exception
	{
		// given
		final String expectedDroolsCode = getResourceAsString("/droolsruleengineservices/test/compiler/generatedConditionsForSingleNotCondition.bin");

		final RuleIrLocalVariablesContainer varContainer = new RuleIrLocalVariablesContainer();
		varContainer.setVariables(ruleIrVariables);
		ruleIrVariables.put(productVariable.getName(), productVariable);
		ruleIrVariables.put(orderEntryVariable.getName(), orderEntryVariable);

		final RuleIrAttributeRelCondition entry = new RuleIrAttributeRelCondition();
		entry.setVariable(orderEntryVariable.getName());
		entry.setAttribute(productVariable.getName());
		entry.setOperator(RuleIrAttributeOperator.EQUAL);
		entry.setTargetVariable(productVariable.getName());

		final RuleIrExistsCondition exists = new RuleIrExistsCondition();
		exists.setVariablesContainer(varContainer);
		exists.setChildren(Collections.singletonList(entry));

		final RuleIrNotCondition irNot = new RuleIrNotCondition();
		irNot.setChildren(Collections.singletonList(exists));

		ruleIr.setConditions(Arrays.asList(irNot));

		// when
		final String generatedDroolsCode = conditionsGenerator.generateConditions(droolsRuleGeneratorContext, INDENTATION);

		// then
		assertEquals(expectedDroolsCode, generatedDroolsCode);

	}

	@Test
	public void testSingleCondition() throws Exception
	{
		// given
		final String expectedDroolsCode = getResourceAsString("/droolsruleengineservices/test/compiler/generatedConditionsForSingleCondition.bin");

		final BigDecimal totalValue = BigDecimal.valueOf(20);

		final RuleIrAttributeCondition amountCondition = new RuleIrAttributeCondition();
		amountCondition.setVariable(CART_VARIABLE_NAME);
		amountCondition.setAttribute("total");
		amountCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN);
		amountCondition.setValue(totalValue);

		ruleIrVariables.put(CART_VARIABLE_NAME, cartVariable);
		ruleIrVariables.put(RESULT_VARIABLE_NAME, resultVariable);
		ruleIr.setConditions(Arrays.asList(ruleIrResultCondition, amountCondition));

		when(droolsRuleValueFormatter.formatValue(droolsRuleGeneratorContext, totalValue)).thenReturn(
				"new java.math.BigDecimal(\"100\")");

		// when
		final String generatedDroolsCode = conditionsGenerator.generateConditions(droolsRuleGeneratorContext, INDENTATION);

		// then
		assertEquals(expectedDroolsCode, generatedDroolsCode);
	}

	@Test
	public void testSingleRelConditionWithTargetAttribute() throws Exception
	{
		// given
		final String expectedDroolsCode = getResourceAsString("/droolsruleengineservices/test/compiler/generatedConditionsForSingleRelConditionWithTargetAttribute.bin");

		final Integer quantity = Integer.valueOf(1);

		final RuleIrAttributeCondition amountCondition = new RuleIrAttributeCondition();
		amountCondition.setVariable(ORDER_ENTRY_VARIABLE_NAME);
		amountCondition.setAttribute("quantity");
		amountCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN_OR_EQUAL);
		amountCondition.setValue(quantity);

		final RuleIrAttributeRelCondition relationCondition = new RuleIrAttributeRelCondition();
		relationCondition.setVariable(ENTRYGROUP_VARIABLE_NAME);
		relationCondition.setAttribute("entryGroupId");
		relationCondition.setOperator(RuleIrAttributeOperator.MEMBER_OF);
		relationCondition.setTargetVariable(ORDER_ENTRY_VARIABLE_NAME);
		relationCondition.setTargetAttribute("entryGroupNumbers");

		ruleIrVariables.put(ORDER_ENTRY_VARIABLE_NAME, orderEntryVariable);
		ruleIrVariables.put(ENTRYGROUP_VARIABLE_NAME, entryGroupVariable);
		ruleIr.setConditions(Arrays.asList(amountCondition, relationCondition));

		when(droolsRuleValueFormatter.formatValue(droolsRuleGeneratorContext, quantity)).thenReturn("new Integer(1)");

		// when
		final String generatedDroolsCode = conditionsGenerator.generateConditions(droolsRuleGeneratorContext, INDENTATION);

		// then
		assertEquals(expectedDroolsCode, generatedDroolsCode);
	}

	@Test
	public void testMultipleConditions() throws Exception
	{
		// given
		final String expectedDroolsCode = getResourceAsString("/droolsruleengineservices/test/compiler/generatedConditionsForMultipleConditions.bin");


		final String colorValue = "blue";

		final RuleIrAttributeCondition colorCondition = new RuleIrAttributeCondition();
		colorCondition.setVariable(PRODUCT_VARIABLE_NAME);
		colorCondition.setAttribute("color");
		colorCondition.setOperator(RuleIrAttributeOperator.EQUAL);
		colorCondition.setValue(colorValue);

		final BigDecimal totalValue = BigDecimal.valueOf(100);

		final RuleIrAttributeCondition amountCondition = new RuleIrAttributeCondition();
		amountCondition.setVariable(CART_VARIABLE_NAME);
		amountCondition.setAttribute("total");
		amountCondition.setOperator(RuleIrAttributeOperator.GREATER_THAN);
		amountCondition.setValue(totalValue);

		final RuleIrGroupCondition blueOrCartCondition = new RuleIrGroupCondition();
		blueOrCartCondition.setOperator(RuleIrGroupOperator.AND);
		blueOrCartCondition.setChildren(Arrays.asList(colorCondition, amountCondition));

		final List<String> codeValues = Arrays.asList("123", "456", "789");

		final RuleIrAttributeCondition code = new RuleIrAttributeCondition();
		code.setVariable(PRODUCT_VARIABLE_NAME);
		code.setAttribute("code");
		code.setOperator(RuleIrAttributeOperator.IN);
		code.setValue(codeValues);

		final RuleIrGroupCondition group = new RuleIrGroupCondition();
		group.setOperator(RuleIrGroupOperator.OR);
		group.setChildren(Arrays.asList(code, blueOrCartCondition));

		ruleIrVariables.put(PRODUCT_VARIABLE_NAME, productVariable);
		ruleIrVariables.put(CART_VARIABLE_NAME, cartVariable);
		ruleIrVariables.put(RESULT_VARIABLE_NAME, resultVariable);
		ruleIr.setConditions(Arrays.asList(ruleIrResultCondition, group));

		when(droolsRuleValueFormatter.formatValue(droolsRuleGeneratorContext, colorValue)).thenReturn("\"blue\"");
		when(droolsRuleValueFormatter.formatValue(droolsRuleGeneratorContext, totalValue)).thenReturn(
				"new java.math.BigDecimal(\"100\")");
		when(droolsRuleValueFormatter.formatValue(droolsRuleGeneratorContext, codeValues)).thenReturn("(\"123\",\"456\",\"789\")");

		// when
		final String generatedDroolsCode = conditionsGenerator.generateConditions(droolsRuleGeneratorContext, INDENTATION);

		// then
		assertEquals(expectedDroolsCode, generatedDroolsCode);
	}
}
