/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.configurablebundleservices.bundle.impl;

import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.configurablebundleservices.bundle.AbstractBundleComponentEditableChecker;


/**
 * Default implementation of the {@link AbstractBundleComponentEditableChecker} for the types {@link CartModel} and
 * {@link CartEntryModel}
 */
public class DefaultCartBundleComponentEditableChecker extends
		DefaultAbstractBundleComponentEditableChecker<CartModel, CartEntryModel>
{
	//
}
