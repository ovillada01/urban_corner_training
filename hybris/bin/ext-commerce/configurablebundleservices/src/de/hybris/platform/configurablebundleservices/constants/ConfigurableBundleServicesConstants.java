/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

package de.hybris.platform.configurablebundleservices.constants;


@SuppressWarnings({ "PMD", "squid:CallToDeprecatedMethod" })
public class ConfigurableBundleServicesConstants extends GeneratedConfigurableBundleServicesConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "configurablebundleservices";
	public static final int NEW_BUNDLE = -1;
	public static final int NO_BUNDLE = 0;

	private ConfigurableBundleServicesConstants()
	{
		//empty
	}


}
