/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.consignmenttrackingmock.utils;

import de.hybris.platform.servicelayer.exceptions.SystemException;

import java.io.BufferedReader;
import java.io.IOException;


/**
 * Handle input buffer data.
 *
 */
public class BufferReaderUtils
{

	private static final int MAX_LENGTH = 10 * 1000;

	private BufferReaderUtils()
	{
	}

	public static StringBuilder getStringBuiderByBufferedReader(final BufferedReader reader) throws IOException
	{
		final StringBuilder result = new StringBuilder("");
		int ch;
		while ((ch = reader.read()) != -1)
		{
			if (result.length() >= MAX_LENGTH)
			{
				throw new SystemException("Input too long");
			}
			result.append((char) ch);
		}
		return result;
	}

}