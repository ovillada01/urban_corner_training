/**
 *
 */
package de.hybris.platform.ticket.job;

import static org.mockito.BDDMockito.given;

import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.jobs.AbstractMaintenanceJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.time.TimeService;
import de.hybris.platform.ticket.enums.CsTicketState;
import de.hybris.platform.ticket.jalo.AbstractTicketsystemTest;
import de.hybris.platform.ticket.service.TicketService;
import de.hybris.platform.ticket.strategies.impl.DefaultCSTicketStagnationStrategy;
import de.hybris.platform.ticketsystem.model.CSTicketStagnationCronJobModel;

import java.util.Calendar;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


/**
 * 
 * A test class for ticket stagnation cron job
 *
 */
public class DefaultTicketCronJobTest extends AbstractTicketsystemTest
{
	private static String TICKET_ID = "test-ticket-7";

	@Resource(name = "csTicketStagnationProcessJobsPerformable")
	private AbstractMaintenanceJobPerformable csTicketStagnationProcessJobsPerformable;

	@Resource(name = "ticketService")
	private TicketService ticketService;

	@Mock
	private TimeService timeService;

	@Resource(name = "csTicketStagnationStrategy")
	private DefaultCSTicketStagnationStrategy csTicketStagnationStrategy;

	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Override
	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		super.setUp();
	}

	@Test
	public void testNoTicketsToClose()
	{
		final PerformResult result = csTicketStagnationProcessJobsPerformable
				.perform(modelService.create(CSTicketStagnationCronJobModel.class));

		Assert.assertEquals("Job should be successful", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals("Job should be finished", CronJobStatus.FINISHED, result.getStatus());
	}

	@Test
	public void testNullValidation()
	{
		exception.expect(IllegalArgumentException.class);

		final PerformResult result = csTicketStagnationProcessJobsPerformable.perform(null);

		Assert.assertEquals("Job should fail due to validation", CronJobResult.FAILURE, result.getResult());
	}

	@Test
	public void testNegativeStagnationPeriodValidation()
	{
		exception.expect(IllegalArgumentException.class);

		final PerformResult result = csTicketStagnationProcessJobsPerformable.perform(createCronJobModel(-3, "NEW"));

		Assert.assertEquals("Job should fail due to negative stagnation period", CronJobResult.FAILURE, result.getResult());
	}

	@Test
	public void testClosingTicket()
	{
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, 5);

		given(timeService.getCurrentTime()).willReturn(cal.getTime());

		csTicketStagnationStrategy.setTimeService(timeService);

		final PerformResult result = csTicketStagnationProcessJobsPerformable.perform(createCronJobModel(1, "Open"));

		Assert.assertEquals("Job should be successful with 1 ticket closed", CronJobResult.SUCCESS, result.getResult());
		Assert.assertEquals(CsTicketState.CLOSED, ticketService.getTicketForTicketId(TICKET_ID).getState());
	}

	protected CSTicketStagnationCronJobModel createCronJobModel(final int stagnationPeriod, final String statuses)
	{
		final CSTicketStagnationCronJobModel cronJobModel = modelService.create(CSTicketStagnationCronJobModel.class);
		cronJobModel.setStagnationPeriod(stagnationPeriod);
		cronJobModel.setEligibleStates(statuses);
		return cronJobModel;
	}
}
