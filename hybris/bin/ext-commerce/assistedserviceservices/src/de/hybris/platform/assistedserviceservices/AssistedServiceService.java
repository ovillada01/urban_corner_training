/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.assistedserviceservices;

import de.hybris.platform.assistedserviceservices.exception.AssistedServiceException;
import de.hybris.platform.assistedserviceservices.utils.AssistedServiceSession;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.OrderModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.storelocator.model.PointOfServiceModel;

import java.util.Collection;
import java.util.List;


/**
 * Service layer interface for Assisted Service methods.
 */
public interface AssistedServiceService
{

	/**
	 * Get list of customers which username or email starts with provided value.
	 *
	 * @deprecated since 6.4, use {@link AssistedServiceService#getCustomers(String, PageableData)} instead
	 *
	 * @param username
	 *           uid or customer's name
	 * @return suggested customers
	 */
	@Deprecated
	List<CustomerModel> getSuggestedCustomers(final String username);

	/**
	 * Binds customer with provided id to cart if it's anonymous cart.
	 *
	 * @param customerId
	 * @param cartId
	 */
	void bindCustomerToCart(final String customerId, final String cartId) throws AssistedServiceException;

	/**
	 * Creates a new customer by it email and name.
	 *
	 * @param customerId
	 *           email of to be newly created customer that is used as uid
	 * @param customerName
	 *           name of to be newly created customer (firstname and surname separated by space symbol)
	 * @return newly created {@link CustomerModel}
	 */
	CustomerModel createNewCustomer(final String customerId, final String customerName) throws DuplicateUidException;

	/**
	 * Returns collection of a customer's carts
	 *
	 * @param customer
	 *           customer model whose carts to be returned
	 * @return collection of the customer's cart models
	 */
	Collection<CartModel> getCartsForCustomer(final CustomerModel customer);

	/**
	 * returns customer
	 *
	 * @param customerId
	 * @return
	 */
	UserModel getCustomer(final String customerId);

	/**
	 * search for a cart with most resent CartModel::getModifiedtime
	 *
	 * @param customer
	 * @return
	 */
	CartModel getLatestModifiedCart(final UserModel customer);

	/**
	 * search for order
	 *
	 * @param orderCode
	 * @param customer
	 * @return
	 */
	OrderModel gerOrderByCode(final String orderCode, final UserModel customer);

	/**
	 * Returns true when provided abstractOrderModel relates to current base site
	 *
	 * @param abstractOrderModel
	 * @return true in case order is made against current {@link de.hybris.platform.basecommerce.jalo.site.BaseSite}
	 */
	boolean isAbstractOrderMatchBaseSite(final AbstractOrderModel abstractOrderModel);

	/**
	 * search cart by code and customer
	 *
	 * @param cartCode
	 * @param customer
	 * @return
	 */
	CartModel getCartByCode(final String cartCode, final UserModel customer);

	/**
	 * Returns ASM session object with all information about current asm session.
	 *
	 * @return asm session object
	 */
	AssistedServiceSession getAsmSession();

	/**
	 * Restore cart to provided user
	 *
	 * @param cart
	 * @param user
	 */
	void restoreCartToUser(final CartModel cart, final UserModel user);


	/**
	 * Returns the PointOfServiceModel for the logged in as agent.
	 *
	 * @return PointOfServiceModel
	 */
	PointOfServiceModel getAssistedServiceAgentStore();


	/**
	 * Returns the PointOfServiceModel for the given as agent.
	 *
	 * @param agent
	 * @return PointOfServiceModel
	 */
	PointOfServiceModel getAssistedServiceAgentStore(final UserModel agent);

	/**
	 * Get list of customers which username or email starts with provided value.
	 *
	 * @param searchCriteria
	 *           uid or customer's name
	 * @param pageableData
	 *           pageable properties
	 * @return suggested customers
	 */
	SearchPageData<CustomerModel> getCustomers(final String searchCriteria, final PageableData pageableData);
}