/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.marketplaceservices.strategies.impl;

import java.math.BigDecimal;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.marketplaceservices.strategies.AutoApproveProductStrategy;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.util.Config;
import de.hybris.platform.validation.coverage.CoverageInfo;
import de.hybris.platform.validation.coverage.strategies.impl.ValidationBasedCoverageCalculationStrategy;
import de.hybris.platform.variants.model.VariantProductModel;
import de.hybris.platform.yacceleratorcore.model.ApparelProductModel;


public class DefaultAutoApproveProductStrategy implements AutoApproveProductStrategy
{

	private static final String PRODUCT_COVERAGE_INDEX_KEY = "marketplaceservices.default.product.coverage.index";

	private ModelService modelService;
	private ValidationBasedCoverageCalculationStrategy validationCoverageCalculationStrategy;

	@Override
	public CoverageInfo autoApproveVariantAndApparelProduct(final ProductModel product)
	{

		final BigDecimal defaultThresholdIndex = BigDecimal.valueOf(Config.getDouble(PRODUCT_COVERAGE_INDEX_KEY, 1.0));

		final boolean isVariantProduct = product instanceof ApparelProductModel;
		final boolean isApparelProduct = product instanceof VariantProductModel;
		final CoverageInfo coverageInfo = getValidationCoverageCalculationStrategy().calculate(product);

		if (!isVariantProduct && !isApparelProduct && coverageInfo != null
				&& BigDecimal.valueOf(coverageInfo.getCoverageIndex()).compareTo(defaultThresholdIndex) < 0)
		{
			return coverageInfo;
		}
		else
		{
			return null;
		}
	}

	public ModelService getModelService()
	{
		return modelService;
	}

	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	public ValidationBasedCoverageCalculationStrategy getValidationCoverageCalculationStrategy()
	{
		return validationCoverageCalculationStrategy;
	}

	public void setValidationCoverageCalculationStrategy(
			final ValidationBasedCoverageCalculationStrategy validationCoverageCalculationStrategy)
	{
		this.validationCoverageCalculationStrategy = validationCoverageCalculationStrategy;
	}
}
