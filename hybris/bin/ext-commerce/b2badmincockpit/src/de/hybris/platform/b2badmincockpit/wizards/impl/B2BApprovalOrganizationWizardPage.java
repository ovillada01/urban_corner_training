/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.b2badmincockpit.wizards.impl;

import org.zkoss.zk.ui.Component;


/**
 *
 */
public class B2BApprovalOrganizationWizardPage extends AbstractB2BOrganizationWizardPage
{

	@Override
	public void renderView(final Component parent)
	{
		//custom render code
	}

}
