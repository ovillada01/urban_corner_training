/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.customercouponfacades;

import de.hybris.platform.commercefacades.voucher.exceptions.VoucherOperationException;
import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.customercouponfacades.customercoupon.data.CustomerCouponData;
import de.hybris.platform.customercouponfacades.impl.DefaultCustomerCouponFacade;

import java.util.List;


/**
 * Customer Coupon Facade {@link DefaultCustomerCouponFacade}
 */
public interface CustomerCouponFacade
{

	/**
	 * @param pageableData
	 *           the pageable data
	 * @return paged list of coupon data
	 */
	SearchPageData<CustomerCouponData> getPagedCouponsData(PageableData pageableData);

	/**
	 * Add custom to the coupon group. When custom alreay in coupon group that will return a alert msg.
	 *
	 * @param couponCode
	 *           coupon group code
	 * @return the result of add custom to coupon group
	 */
	String grantCouponAccessForCurrentUser(String couponCode);

	/**
	 *
	 * @return List<CouponData> list of coupon data
	 */
	List<CustomerCouponData> getCouponsData();


	/**
	 * Add coupon notification
	 *
	 * @param couponCode
	 *           coupon code
	 */
	void saveCouponNotification(String couponCode);

	/**
	 * Remove coupon notification
	 *
	 * @param couponCode
	 *           coupon code
	 */
	void removeCouponNotificationByCode(String couponCode);

	/**
	 * get assignable customer coupons data
	 *
	 * @param text
	 *           search text
	 * @return List<CustomerCouponData> list of coupon data
	 */
	List<CustomerCouponData> getAssignableCustomerCoupons(String text);

	/**
	 * get assigned customer coupons data
	 *
	 * @param text
	 *           search text
	 * @return List<CustomerCouponData> list of coupon data
	 */
	List<CustomerCouponData> getAssignedCustomerCoupons(String text);

	/**
	 * release the specific coupon for current customer
	 *
	 * @param couponCode
	 *           the specific coupon code
	 * @throws VoucherOperationException
	 */
	void releaseCoupon(String couponCode) throws VoucherOperationException;
	
	/**
	 * get CustomerCouponData by id
	 *
	 * @param couponId
	 *           the coupon id
	 * @return CustomerCouponData
	 */
	CustomerCouponData getCustomerCouponForCode(String couponId);
}
