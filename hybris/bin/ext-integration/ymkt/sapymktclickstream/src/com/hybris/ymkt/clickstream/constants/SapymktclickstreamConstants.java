/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.clickstream.constants;

@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public class SapymktclickstreamConstants extends GeneratedSapymktclickstreamConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapymktclickstream";

	private SapymktclickstreamConstants()
	{
		//empty
	}


}
