/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecobuffer.service.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecommendationBufferModel;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class DefaultRecommendationBufferServiceTest
{
	private static final Date EXPIRY_DATE = new Date(1000); //some old date

	@InjectMocks
	private DefaultRecommendationBufferService recommendationBufferService;

	@Mock
	private SAPRecommendationBufferModel recommendationModel;

	@Before
	public void setUp()
	{
		recommendationBufferService.setEnableRecommendationBuffer(true);
	}

	@Test
	public void testExpiredRecommendationByDate()
	{
		Mockito.when(recommendationModel.getExpiresOn()).thenReturn(EXPIRY_DATE);
		Assert.assertTrue(recommendationBufferService.isRecommendationExpired(recommendationModel));
	}

}
