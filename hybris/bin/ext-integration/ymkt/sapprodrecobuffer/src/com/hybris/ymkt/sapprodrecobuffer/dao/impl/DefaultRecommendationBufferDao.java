/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecobuffer.dao.impl;

import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.ymkt.sapprodrecobuffer.dao.RecommendationBufferDao;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPOfferInteractionModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoClickthroughModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoImpressionAggrModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoImpressionModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoTypeMappingModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecommendationBufferModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecommendationMappingModel;


/**
 * Default interface implementation
 * 
 * @see RecommendationBufferDao
 */
public class DefaultRecommendationBufferDao implements RecommendationBufferDao
{
	protected static final String HASH_ID = "hashId";
	protected static final String LEADING_ITEMS = "leadingItems";
	protected static final String SCENARIO_ID = "scenarioId";
	protected static final String USER_ID = "userId";

	private FlexibleSearchService flexibleSearchService;

	@Override
	public List<SAPRecoClickthroughModel> findClickthroughs(int batchSize)
	{
		return this.searchAnyNoCaching(SAPRecoClickthroughModel._TYPECODE, batchSize);
	}

	protected <T> List<T> findExpiredAny(final String typecode, final Date expiresOn)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery( //
				"SELECT {pk} FROM {" + typecode + "} WHERE {expiresOn} < ?expiresOn");
		fQuery.addQueryParameter("expiresOn", expiresOn);
		fQuery.setDisableCaching(true);
		return this.search(fQuery);
	}

	@Override
	public List<SAPRecommendationMappingModel> findExpiredRecommendationMappings(final Date expiresOn)
	{
		return this.findExpiredAny(SAPRecommendationMappingModel._TYPECODE, expiresOn);
	}

	@Override
	public List<SAPRecommendationBufferModel> findExpiredRecommendations(final Date expiresOn)
	{
		return this.findExpiredAny(SAPRecommendationBufferModel._TYPECODE, expiresOn);
	}

	@Override
	public List<SAPRecoTypeMappingModel> findExpiredRecoTypeMappings(final Date expiresOn)
	{
		return this.findExpiredAny(SAPRecoTypeMappingModel._TYPECODE, expiresOn);
	}

	@Override
	public List<SAPRecoImpressionModel> findImpressions(final int batchSize)
	{
		return this.searchAnyNoCaching(SAPRecoImpressionModel._TYPECODE, batchSize);
	}

	@Override
	public List<SAPRecoImpressionAggrModel> findImpressionsAggregated(final int batchSize)
	{
		return this.searchAnyNoCaching(SAPRecoImpressionAggrModel._TYPECODE, batchSize);
	}
	
	@Override
	public List<SAPOfferInteractionModel> findOfferInteractions(final int batchSize)
	{
		return this.searchAnyNoCaching(SAPOfferInteractionModel._TYPECODE, batchSize);
	}

	@Override
	public List<SAPRecommendationBufferModel> findRecommendation(final String scenarioId, final String hashId,
			final String leadingItems)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery( //
				"SELECT {pk} FROM {" + SAPRecommendationBufferModel._TYPECODE + "} " //
						+ "WHERE {scenarioId} = ?scenarioId " //
						+ "AND {leadingItems} = ?leadingItems " //
						+ "AND {hashId} IN (?hashId)");
		fQuery.addQueryParameter(SCENARIO_ID, scenarioId);
		fQuery.addQueryParameter(LEADING_ITEMS, leadingItems);
		fQuery.addQueryParameter(HASH_ID, hashId);
		return this.search(fQuery);
	}

	@Override
	public List<SAPRecommendationMappingModel> findRecommendationMapping(final String userId, final String scenarioId)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery( //
				"SELECT {pk} FROM {" + SAPRecommendationMappingModel._TYPECODE + "} " //
						+ "WHERE {userId} = ?userId " //
						+ "AND {scenarioId} = ?scenarioId");
		fQuery.addQueryParameter(USER_ID, userId);
		fQuery.addQueryParameter(SCENARIO_ID, scenarioId);
		return this.search(fQuery);
	}

	@Override
	public List<SAPRecommendationMappingModel> findRecommendationMapping(final String userId, final String scenarioId,
			final String hashId)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery( //
				"SELECT {pk} FROM {" + SAPRecommendationMappingModel._TYPECODE + "} " //
						+ "WHERE {userId} = ?userId " //
						+ "AND {scenarioId} = ?scenarioId " //
						+ "AND {hashId} = ?hashId");
		fQuery.addQueryParameter(USER_ID, userId);
		fQuery.addQueryParameter(SCENARIO_ID, scenarioId);
		fQuery.addQueryParameter(HASH_ID, hashId);
		return this.search(fQuery);
	}

	@Override
	public List<SAPRecoTypeMappingModel> findRecoTypeMapping(final String recoType, final String scenarioId)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery( //
				"SELECT {pk} FROM {" + SAPRecoTypeMappingModel._TYPECODE + "} " //
						+ "WHERE {scenarioId} = ?scenarioId " //
						+ "AND {recoType} = ?recoType");
		fQuery.addQueryParameter(SCENARIO_ID, scenarioId);
		fQuery.addQueryParameter("recoType", recoType);
		return this.search(fQuery);
	}

	protected <T> List<T> search(final FlexibleSearchQuery fQuery)
	{
		return this.flexibleSearchService.<T> search(fQuery).getResult();
	}

	protected <T> List<T> searchAnyNoCaching(final String typecode, int batchSize)
	{
		final FlexibleSearchQuery fQuery = new FlexibleSearchQuery("SELECT {pk} FROM {" + typecode + "} ");
		fQuery.setDisableCaching(true);
		fQuery.setCount(batchSize);
		fQuery.setStart(0);
		return this.search(fQuery);
	}

	/**
	 * @param flexibleSearchService
	 *           the flexibleSearchService to set
	 */
	@Required
	public void setFlexibleSearchService(final FlexibleSearchService flexibleSearchService)
	{
		this.flexibleSearchService = flexibleSearchService;
	}
}
