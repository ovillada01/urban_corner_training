/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecobuffer.constants;

import com.hybris.ymkt.sapprodrecobuffer.constants.GeneratedSapprodrecobufferConstants;

/**
 * Global class for all Sapprodrecobuffer constants. You can add global constants for your extension into this class.
 */

@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapprodrecobufferConstants extends GeneratedSapprodrecobufferConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapprodrecobuffer";

	public static final String GENERIC_RECO_TYPE = "G";
	public static final String RESTRICTED_RECO_TYPE = "R";

	private SapprodrecobufferConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
