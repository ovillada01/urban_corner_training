/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecobuffer.service;

import java.util.Date;
import java.util.List;

import com.hybris.ymkt.sapprodrecobuffer.model.SAPOfferInteractionModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoClickthroughModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoImpressionAggrModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecoImpressionModel;
import com.hybris.ymkt.sapprodrecobuffer.model.SAPRecommendationBufferModel;


/**
 * Collection of database usage for recommendation.
 */
public interface RecommendationBufferService
{
	/**
	 * @param batchSize
	 * @return {@link List} of {@link SAPRecoImpressionAggrModel}
	 */
	List<SAPRecoImpressionAggrModel> getAggregatedImpressions(int batchSize);

	/**
	 * @param batchSize
	 * @return {@link List} of {@link SAPRecoClickthroughModel}
	 */
	List<SAPRecoClickthroughModel> getClickthroughs(int batchSize);

	/**
	 * Get recommendations with scope R. If none are found, get recommendations with scope G
	 *
	 * @param scenarioId
	 * @param leadingItems
	 * @return SAPRecommendationBufferModel
	 */
	SAPRecommendationBufferModel getGenericRecommendation(String scenarioId, String leadingItems);

	/**
	 * @param batchSize
	 * @return {@link List} of {@link SAPRecoImpressionModel}
	 */
	List<SAPRecoImpressionModel> getImpressions(int batchSize);

	/**
	 * Get a recommendation
	 *
	 * @param userId
	 * @param scenarioId
	 * @param leadingItems
	 * @return SAPRecommendationBufferModel
	 */
	SAPRecommendationBufferModel getRecommendation(String userId, String scenarioId, String leadingItems);

	/**
	 * Check if a recommendation is expired
	 *
	 * @param recommendation
	 *
	 * @return true/false
	 */
	boolean isRecommendationExpired(SAPRecommendationBufferModel recommendation);

	/**
	 * Remove expired mappings based on the expiry offset
	 *
	 */
	void removeExpiredMappings();

	/**
	 * Remove expired recommendations based on the expiry offset
	 *
	 */
	void removeExpiredRecommendations();

	/**
	 * Remove expired mappings based on the expiry offset
	 *
	 */
	void removeExpiredTypeMappings();

	/**
	 * Add a new recommendation entry
	 *
	 * @param userId
	 * @param scenarioId
	 * @param hashId
	 * @param leadingItems
	 * @param recoList
	 * @param recoType
	 * @param expiresOn
	 */
	void saveRecommendation(String userId, String scenarioId, String hashId, String leadingItems, String recoList, String recoType,
			Date expiresOn);

	/**
	 * @param batchSize
	 * @return {@link List} of {@link SAPOfferInteractionModel}
	 */
	List<SAPOfferInteractionModel> getOfferInteractions(int batchSize);

}
