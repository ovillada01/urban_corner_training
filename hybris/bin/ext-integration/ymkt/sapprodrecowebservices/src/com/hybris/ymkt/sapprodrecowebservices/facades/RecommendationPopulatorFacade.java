/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecowebservices.facades;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.ymkt.sapprodreco.services.RecommendationDataSourceTypeService;
import com.hybris.ymkt.sapprodreco.services.RecommendationScenarioService;

import de.hybris.platform.cmsfacades.data.OptionData;
import de.hybris.platform.util.localization.Localization;


/**
 * Populator to generate dropdown values 
 */
public class RecommendationPopulatorFacade {

	private static final String PRODUCT_CODE = "P";
	private static final String CATEGORY_CODE = "C";

	protected RecommendationScenarioService recommendationScenarioService;
	protected RecommendationDataSourceTypeService recommendationDataSourceTypeService;

	private static final Logger LOGGER = LoggerFactory.getLogger(RecommendationPopulatorFacade.class);
	
	/**
	 * Call method to fill the appropriate dropdown
	 * 
	 * @param sourceField: dropdown that needs to be filled
	 * @return dropdownList: list of values for dropdown
	 */
	public List<OptionData> populateDropDown(final String sourceField)
	{
		switch (sourceField) 
		{
			case "recotype":
				return getRecommendationTypes();
			case "leadingitemtype":
				return getLeadingItemTypes();
			case "leadingitemdstype":
			case "cartitemdstype":
				return getItemDataSourceTypes();
			default:
				return Collections.emptyList();
		}
	}

	/**
	 * 
	 * Gets a list of SAPRecommendationType using the Recommendation Scenario Service & 
	 * populates the dropdown using the OptionData object
	 * 
	 * @return List of scenario IDs
	 * @throws IOException
	 */
	protected List<OptionData> getRecommendationTypes()
	{
		try
		{
			return recommendationScenarioService.getRecommendationScenarios().stream()
					.map(s -> createOptionData(s.getId(), s.getId())).collect(Collectors.toList());
		}
		catch(IOException e) 
		{
			LOGGER.error("Error retrieving scenario IDs", e);
			return Collections.emptyList();
		}
	}

	/**
	 * 
	 * Gets a list of SAPRecommendationItemDataSourceType using the Recommendation Data Source Type Service & 
	 * populates the dropdown using the OptionData object
	 * 
	 * @return Leading Item or Cart data source types
	 * @throws IOException
	 */
	protected List<OptionData> getItemDataSourceTypes()
	{
		try
		{
			return recommendationDataSourceTypeService.getItemDataSourceTypes().stream()					
					.map(s -> createOptionData(s.getId(), s.getDescription())).collect(Collectors.toList());
		}
		catch(IOException e) 
		{
			LOGGER.error("Error retrieving data source types", e);
			return Collections.emptyList();
		}
	}

	/**
	 * 
	 * Hardcoded the dropdown data for the LeadingItemTypes
	 * 
	 * opData1 : {
	 * 		ID: "P",
	 * 		Value: "Product"
	 * }
	 * 
	 * opData2: {
	 * 		ID: "C",
	 * 		Value: "Category"
	 * }
	 * @return Product and Category values
	 */
	protected List<OptionData> getLeadingItemTypes() 
	{
		final OptionData opData1 = createOptionData(PRODUCT_CODE, Localization.getLocalizedString("type.CMSSAPRecommendationComponent.product"));
		final OptionData opData2 = createOptionData(CATEGORY_CODE, Localization.getLocalizedString("type.CMSSAPRecommendationComponent.category"));
		return Arrays.asList(opData1, opData2);
	}

	/**
	 * 
	 * Helper function that creates an OptionData object since the constructor is not provided
	 * 
	 * @param id : id of the dropdown selection
	 * @param label : label shown in the dropdown selection
	 * @return OptionData
	 */
	@SuppressWarnings("deprecation")
	private OptionData createOptionData(final String id, final String label)
	{
		OptionData opData = new OptionData();
		opData.setId(id);
		opData.setLabel(label);
		opData.setValue(id);
		return opData;
	}
	
	public void setRecommendationScenarioService(RecommendationScenarioService recommendationScenarioService) 
	{
		this.recommendationScenarioService = recommendationScenarioService;
	}

	public void setRecommendationDataSourceTypeService(RecommendationDataSourceTypeService recommendationDataSourceTypeService) 
	{
		this.recommendationDataSourceTypeService = recommendationDataSourceTypeService;
	}

}

