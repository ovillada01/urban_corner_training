/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecowebservices.facades;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hybris.ymkt.sapprodreco.services.OfferDiscoveryService;

import de.hybris.platform.cmsfacades.data.OptionData;
import de.hybris.platform.util.localization.Localization;

/**
 * Populator to generate dropdown values
 */
public class OfferRecommendationPopulatorFacade {
	protected OfferDiscoveryService offerDiscoveryService;

	private static final String PRODUCT_CODE = "P";
	private static final String CATEGORY_CODE = "C";
	private static final Logger LOGGER = LoggerFactory.getLogger(OfferRecommendationPopulatorFacade.class);

	/**
	 * Call method to fill the appropriate dropdown
	 * 
	 * @param sourceField:
	 *            dropdown that needs to be filled
	 * @return dropdownList: list of values for dropdown
	 */
	public List<OptionData> populateDropDown(final String sourceField) 
	{
		switch (sourceField) 
		{
   		case "recotype":
   			return getRecommendationTypes();
   		case "contentposition":
   			return getContentPositionValues();
   		case "leadingitemtype":
   			return  getLeadingItemTypes();
   		case "leadingitemdstype":
   		case "cartitemdstype":
   			return getItemDataSourceTypes();
   		default:
   			return Collections.emptyList();
		}
	}

	/**
	 * 
	 * Gets a list of SAPRecommendationType using the Recommendation Scenario
	 * Service & populates the dropdown using the OptionData object
	 * 
	 * @return List of scenario IDs
	 * @throws IOException
	 */
	protected List<OptionData> getRecommendationTypes() {
		try {
			return offerDiscoveryService.getOfferRecommendationScenarios().stream()
					.map(s -> createOptionData(s.getId(), s.getId())).collect(Collectors.toList());
		} catch (IOException e) {
			LOGGER.error("Error retrieving scenario IDs", e);
			return Collections.emptyList();
		}
	}
	
	/**
	 * 
	 * Gets a list of SAPRecommendationItemDataSourceType using the Recommendation Data Source Type Service & 
	 * populates the dropdown using the OptionData object
	 * 
	 * @return Leading Item or Cart data source types
	 * @throws IOException
	 */
	protected List<OptionData> getItemDataSourceTypes()
	{
		try
		{
			return offerDiscoveryService.getItemDataSourceTypes().stream()					
					.map(s -> createOptionData(s.getId(), s.getDescription())).collect(Collectors.toList());
		}
		catch(IOException e) 
		{
			LOGGER.error("Error retrieving data source types", e);
			return Collections.emptyList();
		}
	}
	
	/**
	 * 
	 * Hardcoded the dropdown data for the LeadingItemTypes
	 * 
	 * opData1 : {
	 * 		ID: "P",
	 * 		Value: "Product"
	 * }
	 * 
	 * opData2: {
	 * 		ID: "C",
	 * 		Value: "Category"
	 * }
	 * @return Product and Category values
	 */
	protected List<OptionData> getLeadingItemTypes() 
	{
		final OptionData opData1 = createOptionData(PRODUCT_CODE, Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.product"));
		final OptionData opData2 = createOptionData(CATEGORY_CODE, Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.category"));
		return Arrays.asList(opData1, opData2);
	}
	
	/**
	 * 
	 * Gets a list of content position on ymkt using the ContentPositions
	 * service & populates the dropdown using the OptionData object
	 * 
	 * @return List of scenario IDs
	 * @throws IOException
	 */
	protected List<OptionData> getContentPositionValues() {
		try {
			List<OptionData> contentPosition = offerDiscoveryService.getContentPositionValues().stream()
					.filter(s -> !s.getContentPositionId().isEmpty())
					.map(s -> createOptionData(s.getContentPositionId(), s.getContentPositionId()))
					.collect(Collectors.toList());
			contentPosition.add(createOptionData("",Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.noContentPosition")));
			return contentPosition;

		} catch (IOException e) {
			LOGGER.error("Error retrieving content position values", e);
			return Collections.emptyList();
		}
	}

	/**
	 * 
	 * Helper function that creates an OptionData object since the constructor
	 * is not provided
	 * 
	 * @param id
	 *            : id of the dropdown selection
	 * @param label
	 *            : label shown in the dropdown selection
	 * @return OptionData
	 */
	private OptionData createOptionData(final String id, final String label) {
		OptionData opData = new OptionData();
		opData.setId(id);
		opData.setLabel(label);
		return opData;
	}

	public void setOfferDiscoveryService(OfferDiscoveryService offerDiscoveryService) {
		this.offerDiscoveryService = offerDiscoveryService;
	}

}
