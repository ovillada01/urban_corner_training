/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecowebservices.populators;

import de.hybris.platform.cmsfacades.data.ComponentTypeAttributeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

public class RecommendationDropdownComponentTypeAttributePopulator
		implements Populator<AttributeDescriptorModel, ComponentTypeAttributeData> {

	/**
	 * Populator for dropdown controls<br>
	 * The URI is built using the control's qualifier and maps to the 
	 * {@link RecommendationScenariosController#populateDropdown(String)} method.
	 * 
	 * @param source
	 * @param target
	 */
	@Override
	public void populate(final AttributeDescriptorModel source, final ComponentTypeAttributeData target) throws ConversionException 
	{
		target.setUri("/sapprodrecowebservices/v1/data/product/" + source.getQualifier());
		target.setPaged(false);
	}

}