/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecoaddon.constants;

import com.hybris.ymkt.sapprodrecoaddon.constants.GeneratedSapprodrecoaddonConstants;


/**
 * 
 * Global class for all Sapproductrecommendation constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapprodrecoaddonConstants extends GeneratedSapprodrecoaddonConstants
{
	/**
	 * Extension Name sapprodrecoaddon
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapprodrecoaddon";
	/**
	 * RecommendationComponentCount Session Attribute
	 */
	public static final String RECOMMENDATIONCOMPONENTCOUNT = "RecommendationComponentCount";
	/**
	 * RecommendationContext Session Attribute
	 */
	public static final String RECOMMENDATIONCONTEXT = "RecommendationContext";

	private SapprodrecoaddonConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
