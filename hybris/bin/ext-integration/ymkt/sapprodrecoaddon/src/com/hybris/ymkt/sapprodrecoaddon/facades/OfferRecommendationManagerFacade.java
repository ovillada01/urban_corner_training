/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodrecoaddon.facades;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.ymkt.sapprodreco.dao.OfferInteractionContext;
import com.hybris.ymkt.sapprodreco.dao.OfferRecommendation;
import com.hybris.ymkt.sapprodreco.dao.OfferRecommendationContext;
import com.hybris.ymkt.sapprodreco.services.OfferDiscoveryService;
import com.hybris.ymkt.sapprodreco.services.OfferInteractionService;


/**
 * Facade for offer recommendation controller
 */
public class OfferRecommendationManagerFacade
{
	protected OfferDiscoveryService offerDiscoveryService;
	protected OfferInteractionService offerInteractionService;

	/**
	 * 
	 * @param context
	 * @return List<OfferRecommendation>
	 */
	public List<OfferRecommendation> getOfferRecommendations(final OfferRecommendationContext context)
	{
		return offerDiscoveryService.getOfferRecommendations(context);
	}

	/**
	 * 
	 * @param offerInteractionContext
	 */
	public void saveOfferInteraction(final OfferInteractionContext offerInteractionContext)
	{
		offerInteractionService.saveOfferInteraction(offerInteractionContext);
	}

	@Required
	public void setOfferDiscoveryService(OfferDiscoveryService offerDiscoveryService)
	{
		this.offerDiscoveryService = offerDiscoveryService;
	}

	@Required
	public void setOfferInteractionService(OfferInteractionService offerInteractionService)
	{
		this.offerInteractionService = offerInteractionService;
	}

}
