/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btg.condition.impl;

import de.hybris.platform.btg.condition.impl.PlainCollectionExpressionEvaluator;

import java.util.Collections;

import com.sap.wec.adtreco.btg.SAPInitiativeSet;


public class SAPInitiativesExpressionEvaluator extends PlainCollectionExpressionEvaluator
{
	public SAPInitiativesExpressionEvaluator()
	{
		super(Collections.emptyMap());
		this.addSupportedOperator(CONTAINS_ALL, SAPInitiativeSet.class);
		this.addSupportedOperator(CONTAINS_ANY, SAPInitiativeSet.class);
		this.addSupportedOperator(NOT_CONTAINS, SAPInitiativeSet.class);
	}

	@Override
	public Class<SAPInitiativeSet> getLeftType()
	{
		return SAPInitiativeSet.class;
	}
}