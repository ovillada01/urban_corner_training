/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btgcockpit;

import de.hybris.platform.btg.model.BTGReferencePrincipalGroupsOperandModel;
import de.hybris.platform.btgcockpit.service.label.AbstractBTGItemCollectionLabelProvider;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.ymkt.segmentation.model.BTGReferenceSAPInitiativeOperandModel;


/**
 * Provides a text representation for given {@link BTGReferencePrincipalGroupsOperandModel}
 */
public class BTGReferencePrincipalGroupsOperandLabelProvider
		extends AbstractBTGItemCollectionLabelProvider<BTGReferenceSAPInitiativeOperandModel, String>
{

	@Override
	protected Collection<String> getItemObjectCollection(final BTGReferenceSAPInitiativeOperandModel item)
	{
		return item.getInitiatives();
	}

	@Override
	protected String getItemObjectName(final String itemObject)
	{
		return itemObject;
	}

	@Override
	protected String getMessagePrefix()
	{
		return "initiatives";
	}

	@Required
	@Override
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		super.setCommonI18NService(commonI18NService);
	}

}
