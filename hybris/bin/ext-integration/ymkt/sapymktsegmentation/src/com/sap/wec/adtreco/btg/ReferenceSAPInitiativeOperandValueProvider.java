/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btg;

import de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.core.model.user.UserModel;

import java.util.ArrayList;

import com.hybris.ymkt.segmentation.model.BTGReferenceSAPInitiativeOperandModel;


/**
 *
 */
public class ReferenceSAPInitiativeOperandValueProvider implements
		CollectionOperandValueProvider<BTGReferenceSAPInitiativeOperandModel>
{
	@Override
	public Object getValue(final BTGReferenceSAPInitiativeOperandModel operand, final UserModel user,
			final BTGConditionEvaluationScope evaluationScope)
	{
		return new ArrayList<String>(operand.getInitiatives());
	}

	@Override
	public Class<SAPInitiativeSet> getValueType(final BTGReferenceSAPInitiativeOperandModel operand)
	{
		return SAPInitiativeSet.class;
	}

	@Override
	public Class<String> getAtomicValueType(final BTGReferenceSAPInitiativeOperandModel operand)
	{
		return String.class;
	}
}
