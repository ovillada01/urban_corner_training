/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco;

import de.hybris.platform.btg.enums.BTGRuleType;
import de.hybris.platform.btg.model.BTGConfigModel;
import de.hybris.platform.btg.services.BTGConfigurationService;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.core.initialization.SystemSetupContext;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.type.TypeService;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import com.hybris.ymkt.segmentation.constants.SapymktsegmentationConstants;
import com.hybris.ymkt.segmentation.model.BTGSAPInitiativeOperandModel;


/**
 *
 */
@SystemSetup(extension = SapymktsegmentationConstants.EXTENSIONNAME)
public class SAPADTRecoSetup
{
	private BTGConfigurationService btgConfigurationService;
	private ModelService modelService;
	private TypeService typeService;

	/**
	 * @param context
	 */
	@SystemSetup(type = SystemSetup.Type.ALL, process = SystemSetup.Process.ALL)
	public void configureBTG(final SystemSetupContext context)
	{
		final BTGConfigModel config = btgConfigurationService.getConfig();

		final Map<BTGRuleType, Collection<ComposedTypeModel>> operandMapping = new LinkedHashMap<>(config.getOperandMapping());
		final Collection<ComposedTypeModel> oldOperands = operandMapping.get(BTGRuleType.USER);
		final Collection<ComposedTypeModel> newOperands = oldOperands == null ? new LinkedHashSet<>()
				: new LinkedHashSet<>(oldOperands);

		final ComposedTypeModel operand = typeService.getComposedTypeForClass(BTGSAPInitiativeOperandModel.class);
		newOperands.add(operand);
		operandMapping.put(BTGRuleType.USER, newOperands);

		config.setOperandMapping(operandMapping);
		modelService.save(config);
	}

	@Required
	public void setBtgConfigurationService(final BTGConfigurationService btgConfigurationService)
	{
		this.btgConfigurationService = btgConfigurationService;
	}

	@Required
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	@Required
	public void setTypeService(final TypeService typeService)
	{
		this.typeService = typeService;
	}
}
