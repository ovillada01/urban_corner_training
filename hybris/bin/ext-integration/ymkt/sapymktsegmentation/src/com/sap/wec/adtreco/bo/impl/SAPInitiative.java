/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.bo.impl;

/**
 *
 */
public class SAPInitiative implements Comparable<SAPInitiative>
{
	protected String id;
	protected String idInternal; // With leading zeroes.
	protected String memberCount;
	protected String name;

	@Override
	public int compareTo(SAPInitiative o)
	{
		return this.idInternal.compareTo(o.idInternal);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (!(obj instanceof SAPInitiative))
		{
			return false;
		}
		final SAPInitiative other = (SAPInitiative) obj;
		return this.idInternal.equals(other.idInternal);
	}

	public String getId()
	{
		return id;
	}

	public String getMemberCount()
	{
		return memberCount;
	}

	public String getName()
	{
		return name;
	}

	@Override
	public int hashCode()
	{
		return this.idInternal.hashCode();
	}

	public void setId(final String id)
	{
		this.id = id;
	}

	public void setIdInternal(String idInternal)
	{
		this.idInternal = idInternal;
	}

	public void setMemberCount(final String memberCount)
	{
		this.memberCount = memberCount;
	}

	public void setName(final String name)
	{
		this.name = name;
	}

	@Override
	public String toString()
	{
		return "id: " + id + " name: " + name + " Member Count: " + memberCount;
	}
}
