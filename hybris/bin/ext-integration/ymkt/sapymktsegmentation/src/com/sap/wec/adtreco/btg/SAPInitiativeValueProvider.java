/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.wec.adtreco.btg;

import de.hybris.platform.btg.condition.operand.valueproviders.CollectionOperandValueProvider;
import de.hybris.platform.btg.enums.BTGConditionEvaluationScope;
import de.hybris.platform.cockpit.model.editor.impl.DefaultSAPInitiativeUIEditor;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.session.SessionService;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.hybris.ymkt.common.user.UserContextService;
import com.hybris.ymkt.segmentation.model.BTGSAPInitiativeOperandModel;
import com.hybris.ymkt.segmentation.services.InitiativeService;
import com.hybris.ymkt.segmentation.services.InitiativeService.InitiativeQuery;
import com.hybris.ymkt.segmentation.services.InitiativeService.InitiativeQuery.TileFilterCategory;
import com.sap.wec.adtreco.bo.ADTUserIdProvider;
import com.sap.wec.adtreco.bo.impl.SAPInitiative;


public class SAPInitiativeValueProvider implements CollectionOperandValueProvider<BTGSAPInitiativeOperandModel>
{
	private static final String INITIATIVES_PREFIX = "Initiatives_";
	private static final Logger LOG = LoggerFactory.getLogger(DefaultSAPInitiativeUIEditor.class);
	protected InitiativeService initiativeService;
	protected SessionService sessionService;
	protected UserContextService userContextService;
	protected ADTUserIdProvider userIdProvider;

	@Override
	public Class<String> getAtomicValueType(final BTGSAPInitiativeOperandModel operand)
	{
		return String.class;
	}

	protected String getPiwikId()
	{
		final ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder
				.currentRequestAttributes();
		final Cookie[] cookies = servletRequestAttributes.getRequest().getCookies();

		if (cookies == null)
		{
			return "";
		}

		for (Cookie cookie : cookies)
		{
			if (cookie.getName().startsWith("_pk_id"))
			{
				return cookie.getValue().substring(0, 16);
			}
		}
		return "";
	}

	@Override
	public Collection<String> getValue(final BTGSAPInitiativeOperandModel operand, final UserModel user,
			final BTGConditionEvaluationScope scope)
	{
		if (user == null)
		{
			return Collections.emptyList();
		}

		String cookieId = this.getPiwikId();
		String userId = userIdProvider.getUserId(user);

		cookieId = cookieId == null ? "" : cookieId;
		userId = userId == null ? "" : userId;

		if (cookieId.isEmpty() && userId.isEmpty())
		{
			return Collections.emptyList();
		}

		final List<String> sessionInitiatives = sessionService.getAttribute(INITIATIVES_PREFIX + cookieId + userId);
		if (sessionInitiatives != null)
		{
			return sessionInitiatives;
		}

		final List<String> result;
		if (!cookieId.isEmpty() && !userId.isEmpty())
		{
			result = this.getValueForMultiUsers(cookieId, userId);
		}
		else
		{
			result = this.getValueForUser(cookieId + userId);
		}
		this.sessionService.setAttribute(INITIATIVES_PREFIX + cookieId + userId, result);
		return result;
	}

	protected List<String> getValueForMultiUsers(String... users)
	{
		try
		{
			final InitiativeQuery query = new InitiativeQuery.Builder() //
					.tileFilterCategories(TileFilterCategory.ACTIVE) //
					.contacts(users) //
					.contactOrigins(this.userContextService.getAnonymousUserOrigin(), this.userContextService.getLoggedInUserOrigin()) //
					.build();

			return this.initiativeService.getInitiatives(query).stream() //
					.map(SAPInitiative::getId) //
					.map(String::intern) //
					.collect(Collectors.toList());
		}
		catch (final IOException e)
		{
			LOG.error("Error reading value for MultiUsers='{}'", Arrays.toString(users), e);
			return Collections.emptyList();
		}
	}

	protected List<String> getValueForUser(String userId)
	{
		try
		{
			final InitiativeQuery query = new InitiativeQuery.Builder() //
					.tileFilterCategories(TileFilterCategory.ACTIVE) //
					.filterByUserContext(true) //
					.build();
			return this.initiativeService.getInitiatives(query).stream() //
					.map(SAPInitiative::getId) //
					.map(String::intern) //
					.collect(Collectors.toList());
		}
		catch (final IOException e)
		{
			LOG.error("Error reading value for user='{}'", userId, e);
			return Collections.emptyList();
		}
	}

	@Override
	public Class<SAPInitiativeSet> getValueType(final BTGSAPInitiativeOperandModel operand)
	{
		return SAPInitiativeSet.class;
	}

	@Required
	public void setInitiativeService(final InitiativeService initiativeService)
	{
		this.initiativeService = initiativeService;
	}

	@Required
	public void setsessionService(final SessionService sessionService)
	{
		this.sessionService = sessionService;
	}

	@Required
	public void setUserContextService(final UserContextService userContextService)
	{
		this.userContextService = userContextService;
	}

	@Required
	public void setUserIdProvider(final ADTUserIdProvider userIdProvider)
	{
		this.userIdProvider = userIdProvider;
	}
}
