/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodreco.constants;

/**
 * Global class for all Sapprodreco constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapprodrecoConstants extends GeneratedSapprodrecoConstants
{
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapprodreco";
	public static final String PRODUCT = "P";
	public static final String CATEGORY = "C";

	private SapprodrecoConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
