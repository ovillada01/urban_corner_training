/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.hybris.ymkt.sapprodreco.cockpit.editor;


import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.editor.impl.AbstractTextBasedUIEditor;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.core.Registry;
import de.hybris.platform.util.localization.Localization;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;

import com.hybris.ymkt.sapprodreco.dao.SAPOfferContentPositionType;
import com.hybris.ymkt.sapprodreco.dao.SAPRecommendationItemDataSourceType;
import com.hybris.ymkt.sapprodreco.dao.SAPRecommendationType;
import com.hybris.ymkt.sapprodreco.services.OfferDiscoveryService;


/**
 *
 */
public class DefaultOfferRecoUIEditor extends AbstractTextBasedUIEditor
{

	private static final String OFFER_DISCOVERY_SERVICE = "offerDiscoveryService";
	private static final String ENUM_EDITOR_SCLASS = "enumEditor";
	private static final String PRODUCT_CODE = "P";
	private static final String CATEGORY_CODE = "C";
	private Combobox editorView = new Combobox();
	private static final Logger LOG = LoggerFactory.getLogger(DefaultOfferRecoUIEditor.class);
	protected OfferDiscoveryService offerDiscoveryService;

	@Override
	public HtmlBasedComponent createViewComponent(final Object initialValue, final Map<String, ? extends Object> parameters,
			final EditorListener listener)
	{
		final String attribute = parameters.get("attributeQualifier").toString();
		final String targetField = attribute.substring(attribute.lastIndexOf('.') + 1).trim();

		try
		{
			return generateCommonComboBox(initialValue, parameters, listener, targetField);
		}
		catch (final IOException e)
		{
			LOG.error("Error generating ComboBox with targetField : {}", targetField, e);
		}
		return null;
	}

	public HtmlBasedComponent generateCommonComboBox(final Object initialValue, final Map<String, ? extends Object> parameters,
			final EditorListener listener, final String targetField) throws IOException
	{

		parseInitialInputString(parameters);
		editorView.setConstraint("strict");
		editorView.setAutodrop(true);

		if (isEditable())
		{
			//populate ComboBox according to target field
			this.populateComboBox(targetField);
			//select saved value (if any)
			setComboBoxSelectedIndex(initialValue);
			//Add UI Events listener & return HtmlBasedComponent
			return this.addUIEventListeners(listener);
		}
		else
		{
			editorView.setDisabled(true);
			return null;
		}
	}

	public void populateComboBox(String targetField) throws IOException
	{
		clearComboBox();


		if (offerDiscoveryService == null)
		{
			offerDiscoveryService = (OfferDiscoveryService) Registry.getApplicationContext().getBean(OFFER_DISCOVERY_SERVICE);
		}

		switch (targetField)
		{
			case "recotype":
				this.getOfferRecommendationScenarios().stream().map(x -> createComboItem(x.getId(), x.getId()))
						.forEach(editorView::appendChild);
				return;
			case "contentposition":
				this.populateContentPositionComboBox();
				return;
			case "leadingitemtype":
				this.populateLeadingItemTypeComboBox();
				return;
			case "leadingitemdstype":
			case "cartitemdstype":
				this.getItemDataSourceTypes().stream().filter(y -> StringUtils.isNotEmpty(y.getId()))
						.map(x -> createComboItem(x.getId(), x.getDescription())).forEach(editorView::appendChild);
				return;
			default:
				break;
		}

	}

	private void populateContentPositionComboBox() throws IOException
	{
		this.getContentPositionTypes().stream().map(x -> createComboItem(x.getContentPositionId(), x.getContentPositionId()))
				.forEach(editorView::appendChild);

		//manually add "no content position" value
		final Comboitem comboItemModelNoContentPosition = new Comboitem();
		comboItemModelNoContentPosition.setValue("");
		comboItemModelNoContentPosition
				.setLabel(Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.noContentPosition"));
		editorView.appendChild(comboItemModelNoContentPosition);
	}

	private void populateLeadingItemTypeComboBox()
	{
		//hardcode "Product" and "Category" values
		final Comboitem comboItemModelCategory = new Comboitem();
		comboItemModelCategory.setValue(CATEGORY_CODE);
		comboItemModelCategory.setLabel(Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.category"));
		final Comboitem comboItemModelProduct = new Comboitem();
		comboItemModelProduct.setValue(PRODUCT_CODE);
		comboItemModelProduct.setLabel(Localization.getLocalizedString("type.CMSSAPOfferRecoComponent.product"));
		editorView.appendChild(comboItemModelCategory);
		editorView.appendChild(comboItemModelProduct);
	}

	public CancelButtonContainer addUIEventListeners(final EditorListener listener)
	{

		final CancelButtonContainer ret = new CancelButtonContainer(listener, () -> {
			setComboBoxSelectedIndex(initialEditValue);
			setValue(initialEditValue);
			fireValueChanged(listener);
			listener.actionPerformed(EditorListener.ESCAPE_PRESSED);
		});

		ret.setSclass(ENUM_EDITOR_SCLASS);
		ret.setContent(editorView);

		editorView.addEventListener(Events.ON_FOCUS, event -> {
			if (editorView.getSelectedItem() != null)
			{
				initialEditValue = editorView.getValue();
			}
			ret.showButton(Boolean.TRUE.booleanValue());
		});

		editorView.addEventListener(Events.ON_CHANGE, event -> validateAndFireEvent(listener));
		editorView.addEventListener(Events.ON_BLUR, event -> ret.showButton(false));
		editorView.addEventListener(Events.ON_OK, event -> {
			validateAndFireEvent(listener);
			listener.actionPerformed(EditorListener.ENTER_PRESSED);
		});
		editorView.addEventListener(Events.ON_CANCEL, event -> {
			ret.showButton(false);
			DefaultOfferRecoUIEditor.this.setComboBoxSelectedIndex(initialEditValue);
			setValue(initialEditValue);
			fireValueChanged(listener);
			listener.actionPerformed(EditorListener.ESCAPE_PRESSED);
		});

		return ret;
	}

	private Comboitem createComboItem(String value, String label)
	{
		final Comboitem item = new Comboitem();
		item.setLabel(label);
		item.setValue(value);
		return item;
	}

	protected void clearComboBox()
	{
		final int size = editorView.getChildren().size();
		for (int i = 0; i < size; i++)
		{
			editorView.removeItemAt(0);
		}
	}

	protected void setComboBoxSelectedIndex(final Object value)
	{
		if (editorView.getChildren().size() > 0 && value != null)
		{
			final int index = this.getIndexByValue(editorView.getChildren(), value.toString());
			editorView.setSelectedIndex(index);
		}
	}

	private int getIndexByValue(List<Comboitem> list, String value)
	{
		for (int i = 0; i < list.size(); i++)
		{
			if (list.get(i) != null && (list.get(i).getValue().equals(value)))
			{
				return i;
			}
		}
		return -1;
	}

	protected void validateAndFireEvent(final EditorListener listener)
	{
		if (editorView.getSelectedItem() == null)
		{
			setComboBoxSelectedIndex(initialEditValue);
		}
		else
		{
			DefaultOfferRecoUIEditor.this.setValue(editorView.getSelectedItem());
			listener.valueChanged(editorView.getSelectedItem().getValue());
		}
	}


	protected List<SAPRecommendationType> getOfferRecommendationScenarios() throws IOException
	{
		try
		{
			return offerDiscoveryService.getOfferRecommendationScenarios();
		}
		catch (IOException e)
		{
			throw new IOException("Error retrieving recommendation scenario.", e);
		}
	}

	protected List<SAPRecommendationItemDataSourceType> getItemDataSourceTypes() throws IOException
	{
		try
		{
			return offerDiscoveryService.getItemDataSourceTypes();
		}
		catch (IOException e)
		{
			throw new IOException("Error retrieving item data source types.", e);
		}
	}

	protected List<SAPOfferContentPositionType> getContentPositionTypes() throws IOException
	{
		try
		{
			return offerDiscoveryService.getContentPositionValues();
		}
		catch (IOException e)
		{
			throw new IOException("Error retrieving item data source types.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see de.hybris.platform.cockpit.model.editor.UIEditor#getEditorType()
	 */
	@Override
	public boolean isInline()
	{
		return true;
	}

	@Override
	public String getEditorType()
	{
		return PropertyDescriptor.TEXT;
	}

}
