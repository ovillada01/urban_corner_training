/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.sapordermgmtservices.converters.populator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.catalog.enums.ProductInfoStatus;
import de.hybris.platform.commercefacades.order.data.ConfigurationInfoData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.PriceDataFactory;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.sap.core.bol.businessobject.BusinessObjectException;
import de.hybris.platform.sap.core.common.TechKey;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.sap.productconfig.services.data.CartEntryConfigurationAttributes;
import de.hybris.platform.sap.productconfig.services.intf.ProductConfigurationService;
import de.hybris.platform.sap.sapcommonbol.common.businessobject.interf.Converter;
import de.hybris.platform.sap.sapordermgmtbol.transaction.item.businessobject.impl.ItemSalesDoc;
import de.hybris.platform.sap.sapordermgmtbol.transaction.item.businessobject.interf.Item;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


@SuppressWarnings("javadoc")
public class DefaultAbstractOrderEntryPopulatorTest
{

	final Item item = new ItemSalesDoc();
	final String productId = "1";
	final String internalProductId = "01";

	/**
	 *
	 */
	private static final String USD = "USD";
	/**
	 *
	 */
	private static final String TEST_PRODUCT_ID = "TestProductID";
	/**
	 *
	 */
	private static final int NUMBER_INT = 1;
	/**
	 *
	 */
	private static final BigDecimal QUANTITY = new BigDecimal(15);
	/**
	 *
	 */
	private static final BigDecimal GROSS_VALUE = new BigDecimal(8);
	/**
	 *
	 */
	private static final BigDecimal NET_VALUE_WO_FREIGHT = new BigDecimal(5);

	private final DefaultAbstractOrderEntryPopulator classUnderTest = new DefaultAbstractOrderEntryPopulator();
	private final Date creationDate = new Date(System.currentTimeMillis());

	private static final String ITEM_HANDLE = "123";
	private static final String VALUE_NAME = "Value A";
	private static final String CHARACTERISTIC_NAME = "Characteristic One";

	@Before
	public void setUp()
	{
		final Converter converter = EasyMock.createMock(Converter.class);
		try
		{
			EasyMock.expect(converter.getCurrencyScale(USD)).andReturn(new Integer(5)).anyTimes();
		}
		catch (final BusinessObjectException e)
		{
			e.printStackTrace();
		}
		EasyMock.replay(converter);

		mockConfigurationEntities();

		final PriceDataFactory priceFactory = new DefaultPriceDataFactoryForTest();

		classUnderTest.setPriceFactory(priceFactory);
	}

	@Test
	public void testBeanInstanciation()
	{
		Assert.assertNotNull(classUnderTest);
	}

	@Test
	public void testPopulateAbstractOrderEntryPopulator()
	{
		final OrderEntryData target = new OrderEntryData();

		final Item item = new ItemSalesDoc();

		item.setNetValueWOFreight(NET_VALUE_WO_FREIGHT);
		item.setGrossValue(GROSS_VALUE);
		item.setQuantity(QUANTITY);
		item.setNumberInt(NUMBER_INT);
		item.setProductId(TEST_PRODUCT_ID);
		item.setCurrency(USD);


		classUnderTest.populate(item, target);

		final OrderEntryData orderEntry = target;
		Assert.assertEquals(orderEntry.getBasePrice().getValue(), NET_VALUE_WO_FREIGHT);
		Assert.assertEquals(orderEntry.getTotalPrice().getValue(), GROSS_VALUE);
		Assert.assertEquals(orderEntry.getQuantity(), new java.lang.Long(15));
		Assert.assertEquals(orderEntry.getEntryNumber(), new Integer(NUMBER_INT));
		Assert.assertEquals(orderEntry.getProduct().getCode(), TEST_PRODUCT_ID);
	}

	@Test
	public void testCreateProductItemFromERP()
	{
		item.setProductId(productId);
		item.setProductGuid(new TechKey(internalProductId));
		final ProductData productData = classUnderTest.createProductFromItem(item);
		assertEquals(productId, productData.getName());
		assertEquals(internalProductId, productData.getCode());
	}


	@Test
	public void testCreateProductItemDoesNotExistInERP()
	{
		item.setProductId(productId);
		item.setProductGuid(TechKey.EMPTY_KEY);
		final ProductData productData = classUnderTest.createProductFromItem(item);
		checkIdIsSet(productData);
	}


	@Test
	public void testCreateProductItemWOTechKey()
	{
		item.setProductId(productId);
		item.setProductGuid(null);
		final ProductData productData = classUnderTest.createProductFromItem(item);
		checkIdIsSet(productData);
	}

	void checkIdIsSet(final ProductData productData)
	{
		assertEquals(productId, productData.getName());
		assertEquals(productId, productData.getCode());
	}

	@Test
	public void testFormatProductId()
	{
		String productId = "000000000000000001";
		assertEquals("000000000000000001", classUnderTest.formatProductIdForHybris(productId));
		productId = "01";
		assertEquals("01", classUnderTest.formatProductIdForHybris(productId));
	}

	@Test
	public void testFormatProductIdAlphaNum()
	{
		final String productId = "KD990KAP";
		assertEquals(productId, classUnderTest.formatProductIdForHybris(productId));
	}

	@Test
	public void testKbIsPresent()
	{
		item.setKbDate(creationDate);
		item.setProductId(productId);
		final boolean isPresent = classUnderTest.isKbPresent(item);
		assertTrue(isPresent);
	}

	@Test
	public void testKbIsPresentNoKbDateOnItem()
	{
		item.setKbDate(null);
		final boolean isPresent = classUnderTest.isKbPresent(item);
		assertFalse(isPresent);
	}

	@Test
	public void testHandleConfigurationBackendLeads()
	{
		final OrderEntryData target = new OrderEntryData();
		final ProductData product = new ProductData();
		item.setKbDate(creationDate);
		item.setProductId(productId);
		item.setConfigurable(Boolean.TRUE.booleanValue());
		classUnderTest.handleConfigurationBackendLeads(item, target, product);
		assertTrue(target.isConfigurationAttached());
		assertTrue(product.getConfigurable().booleanValue());
	}

	@Test
	public void testHandleConfigurationBackendLeadsNoKb()
	{
		final OrderEntryData target = new OrderEntryData();
		final ProductData product = new ProductData();
		item.setKbDate(null);
		classUnderTest.handleConfigurationBackendLeads(item, target, product);
		assertFalse(target.isConfigurationAttached());
		assertNull(product.getConfigurable());
	}


	protected void mockConfigurationEntities()
	{
		final String configId = mockProductConfigurationService();
		mockSessionAccessService(configId);
		final List<ConfigurationInfoData> configInfoDataList = mockConfigurationInfoDataList();
		mockConfigurationInfoConverter(configInfoDataList);
	}

	protected void mockConfigurationInfoConverter(final List<ConfigurationInfoData> configInfoDataList)
	{
		final de.hybris.platform.servicelayer.dto.converter.Converter<ConfigModel, List<ConfigurationInfoData>> orderEntryConfigurationInfoConverter = EasyMock
				.createMock(de.hybris.platform.servicelayer.dto.converter.Converter.class);
		EasyMock.expect(orderEntryConfigurationInfoConverter.convert((ConfigModel) EasyMock.anyObject()))
				.andReturn(configInfoDataList);
		EasyMock.replay(orderEntryConfigurationInfoConverter);
		classUnderTest.setOrderEntryConfigurationInfoConverter(orderEntryConfigurationInfoConverter);
	}

	protected List<ConfigurationInfoData> mockConfigurationInfoDataList()
	{
		final List<ConfigurationInfoData> configInfoDataList = new ArrayList<>();

		final ConfigurationInfoData configInfoInline = new ConfigurationInfoData();
		configInfoInline.setConfigurationLabel(CHARACTERISTIC_NAME);
		configInfoInline.setConfigurationValue(VALUE_NAME);
		configInfoInline.setConfiguratorType(ConfiguratorType.CPQCONFIGURATOR);
		configInfoInline.setStatus(ProductInfoStatus.SUCCESS);
		configInfoDataList.add(configInfoInline);
		return configInfoDataList;
	}

	protected void mockSessionAccessService(final String configId)
	{
		final SessionAccessService sessionAccessService = EasyMock.createMock(SessionAccessService.class);
		EasyMock.expect(sessionAccessService.getConfigIdForCartEntry(ITEM_HANDLE)).andReturn(configId);
		EasyMock.replay(sessionAccessService);
		classUnderTest.setSessionAccessService(sessionAccessService);
	}

	protected String mockProductConfigurationService()
	{
		final CartEntryConfigurationAttributes configAttributes = new CartEntryConfigurationAttributes();
		configAttributes.setConfigurationConsistent(Boolean.TRUE);
		final ProductConfigurationService productConfigurationService = EasyMock.createMock(ProductConfigurationService.class);
		EasyMock.expect(productConfigurationService.calculateCartEntryConfigurationAttributes((String) EasyMock.anyObject(),
				(String) EasyMock.anyObject(), (String) EasyMock.eq(null))).andReturn(configAttributes);
		EasyMock
				.expect(Boolean
						.valueOf(productConfigurationService.hasKbForDate((String) EasyMock.anyObject(), (Date) EasyMock.anyObject())))
				.andReturn(Boolean.TRUE);

		final ConfigModel configModel = new ConfigModelImpl();
		final String configId = "S1";
		configModel.setId(configId);
		EasyMock.expect(productConfigurationService.retrieveConfigurationModel(configId)).andReturn(configModel);
		EasyMock.replay(productConfigurationService);
		classUnderTest.setProductConfigurationService(productConfigurationService);
		return configId;
	}

	@Test
	public void testHandleConfiguration()
	{
		item.setConfigurable(true);
		item.setHandle(ITEM_HANDLE);
		final ProductData productData = new ProductData();
		final OrderEntryData target = new OrderEntryData();
		classUnderTest.handleConfiguration(item, target, productData);
		final List<ConfigurationInfoData> configInfos = target.getConfigurationInfos();
		assertNotNull(configInfos);
		assertEquals(1, configInfos.size());
		assertEquals(CHARACTERISTIC_NAME, configInfos.get(0).getConfigurationLabel());
		assertEquals(VALUE_NAME, configInfos.get(0).getConfigurationValue());
		assertEquals(ConfiguratorType.CPQCONFIGURATOR, configInfos.get(0).getConfiguratorType());
	}

}
