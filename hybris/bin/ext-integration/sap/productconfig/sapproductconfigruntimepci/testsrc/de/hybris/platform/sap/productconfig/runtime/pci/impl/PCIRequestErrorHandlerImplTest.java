/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.pci.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.same;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.interf.analytics.model.AnalyticsDocument;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.hybris.charon.exp.HttpException;


@SuppressWarnings("javadoc")
@UnitTest
public class PCIRequestErrorHandlerImplTest
{

	private PCIRequestErrorHandlerImpl classUnderTest;
	private AnalyticsDocument input;

	@Mock
	private Logger loggerMock;
	private HttpException ex;


	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = spy(new PCIRequestErrorHandlerImpl());
		given(classUnderTest.getLogger()).willReturn(loggerMock);
		input = new AnalyticsDocument();
		input.setRootProduct("pCode");
		ex = new HttpException(500, "some err", null);
		willReturn("server error msg").given(classUnderTest).getServerMessage(ex);
	}

	@Test
	public void testCreateEmptyAnalyticsDocument()
	{
		final AnalyticsDocument output = classUnderTest.createEmptyAnalyticsDocument(input);
		assertDocumentEmpty(output);
	}

	@Test
	public void testProcessCreateAnalyticsDocumentHttpError()
	{

		final AnalyticsDocument output = classUnderTest.processCreateAnalyticsDocumentHttpError(ex, input);
		assertDocumentEmpty(output);
		verify(loggerMock).error(contains("server error msg"), same(ex));
	}


	protected void assertDocumentEmpty(final AnalyticsDocument output)
	{
		assertNotNull(output);
		assertEquals("pCode", output.getRootProduct());
		assertNull(output.getRootItem());
	}
}
