/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.rules.backoffice.constants;

/**
 * Global class for all Sapproductconfigrulesbackoffice constants.
 */
@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapproductconfigrulesbackofficeConstants extends GeneratedSapproductconfigrulesbackofficeConstants
{
	/**
	 * name of the sapproductconfigrulesbackoffice extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigrulesbackoffice";

	private SapproductconfigrulesbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
