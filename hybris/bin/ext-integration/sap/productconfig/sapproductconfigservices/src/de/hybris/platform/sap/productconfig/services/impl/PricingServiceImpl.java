/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.services.impl;

import de.hybris.platform.sap.productconfig.runtime.interf.ProviderFactory;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceSummaryModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceValueUpdateModel;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.sap.productconfig.services.intf.PricingService;

import java.util.List;

import org.springframework.beans.factory.annotation.Required;


/**
 * Provides price information for configuration
 *
 */
public class PricingServiceImpl implements PricingService
{
	private ProviderFactory providerFactory;
	private SessionAccessService sessionAccessService;

	@Override
	public PriceSummaryModel getPriceSummary(final String configId)
	{
		PriceSummaryModel priceSummaryModel = getSessionAccessService().getPriceSummaryState(configId);
		if (priceSummaryModel == null)
		{
			priceSummaryModel = getProviderFactory().getPricingProvider().getPriceSummary(configId);
			getSessionAccessService().setPriceSummaryState(configId, priceSummaryModel);
		}
		return priceSummaryModel;

	}

	@Override
	public void fillDeltaPrices(final List<PriceValueUpdateModel> updateModels, final String kbId)
	{
		getProviderFactory().getPricingProvider().fillDeltaPrices(updateModels, kbId);
	}


	protected ProviderFactory getProviderFactory()
	{
		return providerFactory;
	}


	/**
	 * @param providerFactory
	 *           the providerFactory to set
	 */
	@Required
	public void setProviderFactory(final ProviderFactory providerFactory)
	{
		this.providerFactory = providerFactory;
	}


	@Override
	public boolean isActive()
	{
		return getProviderFactory().getPricingProvider().isActive();
	}

	protected SessionAccessService getSessionAccessService()
	{
		return sessionAccessService;
	}

	/**
	 * Setter for sessionAccessService
	 * 
	 * @param sessionAccessService
	 *           sessionAccessService to set
	 */
	@Required
	public void setSessionAccessService(final SessionAccessService sessionAccessService)
	{
		this.sessionAccessService = sessionAccessService;
	}

	@Override
	public void fillOverviewPrices(final ConfigModel configModel)
	{
		final PriceSummaryModel summary = getPriceSummary(configModel.getId());
		fillConfigPrices(summary, configModel);
		getProviderFactory().getPricingProvider().fillValuePrices(configModel);
	}

	protected void fillConfigPrices(final PriceSummaryModel summary, final ConfigModel configModel)
	{
		configModel.setBasePrice(summary.getBasePrice());
		configModel.setCurrentTotalPrice(summary.getCurrentTotalPrice());
		configModel.setSelectedOptionsPrice(summary.getSelectedOptionsPrice());
	}



}
