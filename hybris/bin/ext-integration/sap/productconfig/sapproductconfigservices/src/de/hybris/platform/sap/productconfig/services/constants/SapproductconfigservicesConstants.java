/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.services.constants;


/**
 * Global class for all Sapproductconfigservices constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public final class SapproductconfigservicesConstants extends GeneratedSapproductconfigservicesConstants
{
	/**
	 * name of the sapproductconfigservices extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigservices";

	private SapproductconfigservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
