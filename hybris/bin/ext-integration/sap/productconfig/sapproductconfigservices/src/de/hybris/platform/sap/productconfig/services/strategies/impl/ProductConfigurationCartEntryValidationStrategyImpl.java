/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.services.strategies.impl;

import de.hybris.platform.catalog.enums.ConfiguratorType;
import de.hybris.platform.commerceservices.order.CommerceCartModification;
import de.hybris.platform.core.model.order.CartEntryModel;
import de.hybris.platform.order.model.AbstractOrderEntryProductInfoModel;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.impl.KBKeyImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.sap.productconfig.services.intf.ProductConfigurationService;
import de.hybris.platform.sap.productconfig.services.model.CPQOrderEntryProductInfoModel;
import de.hybris.platform.sap.productconfig.services.strategies.intf.ProductConfigurationCartEntryValidationStrategy;
import de.hybris.platform.servicelayer.model.ModelService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;


/**
 * Default implementation of {@link ProductConfigurationCartEntryValidationStrategy}
 */
public class ProductConfigurationCartEntryValidationStrategyImpl implements ProductConfigurationCartEntryValidationStrategy
{

	/**
	 * Indicates that customer needs to revisit product configuration. Postfix in corresponding resource text
	 */
	public static final String REVIEW_CONFIGURATION = "reviewConfiguration";
	/**
	 * Indicates that the KB-Version which was used to create the external configuratiuon, is not known/valid anymore
	 */
	public static final String KB_NOT_VALID = "kbNotValid";
	private SessionAccessService sessionAccessService;
	private ProductConfigurationService productConfigurationService;
	private ModelService modelService;

	private static final Logger LOG = Logger.getLogger(ProductConfigurationCartEntryValidationStrategyImpl.class);


	protected SessionAccessService getSessionAccessService()
	{
		return sessionAccessService;
	}


	/**
	 * @param sessionAccessService
	 */
	public void setSessionAccessService(final SessionAccessService sessionAccessService)
	{
		this.sessionAccessService = sessionAccessService;
	}


	protected ProductConfigurationService getProductConfigurationService()
	{
		return productConfigurationService;
	}

	/**
	 * @param productConfigurationService
	 *           the productConfigurationService to set
	 */
	public void setProductConfigurationService(final ProductConfigurationService productConfigurationService)
	{
		this.productConfigurationService = productConfigurationService;
	}

	@Override
	public CommerceCartModification validateConfiguration(final CartEntryModel cartEntryModel)
	{
		//No issues so far: We check for consistency and completeness of configuration
		CommerceCartModification configurationModification = null;
		final String externalConfiguration = cartEntryModel.getExternalConfiguration();
		if (externalConfiguration != null && (!externalConfiguration.isEmpty()))
		{
			final KBKey kbKey = new KBKeyImpl(cartEntryModel.getProduct().getCode());
			final boolean validKB = getProductConfigurationService().hasKbForVersion(kbKey, externalConfiguration);
			boolean completeAndConsistent = false;
			if (validKB)
			{
				final ConfigModel configurationModel = retrieveConfigurationModel(cartEntryModel, externalConfiguration);
				completeAndConsistent = configurationModel.isComplete() && configurationModel.isConsistent();
			}
			else
			{
				// delete deprecated configuration and forece creation of default configuration on next access
				cartEntryModel.setExternalConfiguration(null);
				resetConfigurationInfo(cartEntryModel);
				getModelService().save(cartEntryModel);

			}
			configurationModification = createCommerceCartModification(cartEntryModel, completeAndConsistent, validKB);


			if (LOG.isDebugEnabled() && configurationModification != null)
			{
				LOG.debug("Validate configuration for product '" + configurationModification.getProduct().getCode()
						+ "' with status '" + configurationModification.getStatusCode() + "'");
			}
		}


		return configurationModification;
	}

	protected void resetConfigurationInfo(final CartEntryModel orderEntry)
	{
		final List<AbstractOrderEntryProductInfoModel> configInfos = new ArrayList<>();
		final CPQOrderEntryProductInfoModel configInfo = getModelService().create(CPQOrderEntryProductInfoModel.class);
		configInfo.setOrderEntry(orderEntry);
		configInfo.setConfiguratorType(ConfiguratorType.CPQCONFIGURATOR);
		configInfos.add(configInfo);
		orderEntry.setProductInfos(Collections.unmodifiableList(configInfos));
	}

	/**
	 * Creates modification bean
	 *
	 * @param cartEntryModel
	 * @param completeAndConsistent
	 *           <code>true</code> only if the KB is complete and consistent
	 * @param validKB
	 *           <code>true</code> only if the KB is valid
	 * @return Modification bean
	 */
	protected CommerceCartModification createCommerceCartModification(final CartEntryModel cartEntryModel,
			final boolean completeAndConsistent, final boolean validKB)
	{
		CommerceCartModification configurationModification = null;
		if (!completeAndConsistent || !validKB)
		{
			configurationModification = new CommerceCartModification();
			if (!validKB)
			{
				configurationModification.setStatusCode(KB_NOT_VALID);
			}
			else
			{
				configurationModification.setStatusCode(REVIEW_CONFIGURATION);
			}
			configurationModification.setEntry(cartEntryModel);
			configurationModification.setProduct(cartEntryModel.getProduct());
		}
		return configurationModification;
	}

	/**
	 * Retrieves the configuration model, either from the session, or as new instance from the external configuration
	 *
	 * @param cartEntryModel
	 * @param externalConfiguration
	 * @return Model representation of configuration
	 */
	protected ConfigModel retrieveConfigurationModel(final CartEntryModel cartEntryModel, final String externalConfiguration)
	{
		ConfigModel configurationModel;
		final String configIdForCartEntry = getSessionAccessService().getConfigIdForCartEntry(cartEntryModel.getPk().toString());
		//Is there a configuration attached?
		if (configIdForCartEntry != null)
		{
			//Do we have a runtime representation yet?
			configurationModel = getConfigFromSession(configIdForCartEntry);
		}
		else
		{
			//We need to retrieve from external configuration
			configurationModel = getConfigFromExtCFG(cartEntryModel, externalConfiguration);
		}
		return configurationModel;
	}

	/**
	 * Retrieve configuration from session
	 *
	 * @param configIdForCartEntry
	 *           ID of configuration in session
	 * @return Configuration Model, representing the configuration attached to the current cart entry
	 */
	protected ConfigModel getConfigFromSession(final String configIdForCartEntry)
	{
		ConfigModel configurationModel;
		configurationModel = getProductConfigurationService().retrieveConfigurationModel(configIdForCartEntry);
		return configurationModel;
	}

	/**
	 * Creates a configuration from external representation and return it. Also stores SSC session ID in hybris session
	 * to be able to call the configuration UI later on.
	 *
	 * @param cartEntryModel
	 *           Cart entry
	 * @param externalConfiguration
	 *           XML representation of configuration
	 * @return Configuration Model, representing the configuration attached to the current cart entry
	 */
	protected ConfigModel getConfigFromExtCFG(final CartEntryModel cartEntryModel, final String externalConfiguration)
	{
		ConfigModel configurationModel;
		final KBKey kbKey = new KBKeyImpl(cartEntryModel.getProduct().getCode());
		configurationModel = getProductConfigurationService().createConfigurationFromExternal(kbKey, externalConfiguration);
		getSessionAccessService().setConfigIdForCartEntry(cartEntryModel.getPk().toString(), configurationModel.getId());
		return configurationModel;
	}

	protected ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}
}
