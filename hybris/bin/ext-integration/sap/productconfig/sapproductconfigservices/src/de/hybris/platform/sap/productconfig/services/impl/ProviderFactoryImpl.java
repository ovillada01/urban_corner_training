/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.services.impl;

import de.hybris.platform.sap.productconfig.runtime.interf.AnalyticsProvider;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationProvider;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationProviderFactory;
import de.hybris.platform.sap.productconfig.runtime.interf.PricingProvider;
import de.hybris.platform.sap.productconfig.runtime.interf.ProviderFactory;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.servicelayer.internal.service.ServicelayerUtils;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;


/**
 * Default implementtaion of the {@link ProviderFactory}.
 */
@SuppressWarnings(
{ "deprecation", "squid:CallToDeprecatedMethod" })
public class ProviderFactoryImpl implements ProviderFactory, ConfigurationProviderFactory
{
	private static final String ANALYTICS_PROVIDER_BEAN_ALIAS = "sapProductConfigAnalyticsProvider";
	private static final String CONFIG_PROVIDER_BEAN_ALIAS = "sapProductConfigConfigurationProvider";
	private static final String PRICING_PROVIDER_BEAN_ALIAS = "sapProductConfigPricingProvider";
	private static final Logger LOG = Logger.getLogger(ProviderFactoryImpl.class);

	private SessionAccessService sessionAccessService;
	private ApplicationContext applicationContext;

	protected static final String SESSION_CACHE_KEY = ConfigurationProvider.class.getName();

	@Override
	public ConfigurationProvider getConfigurationProvider()
	{
		ConfigurationProvider provider = sessionAccessService.getConfigurationProvider();
		if (provider == null)
		{
			provider = (ConfigurationProvider) createProviderInstance(getConfigurationProviderBeanName());
			sessionAccessService.setConfigurationProvider(provider);
		}
		return provider;
	}

	@Override
	public PricingProvider getPricingProvider()
	{
		PricingProvider provider = sessionAccessService.getPricingProvider();
		if (provider == null)
		{
			provider = (PricingProvider) createProviderInstance(getPricingProviderBeanName());
			sessionAccessService.setPricingProvider(provider);
		}
		return provider;
	}

	@Override
	public AnalyticsProvider getAnalyticsProvider()
	{
		AnalyticsProvider analyticsProvider = getSessionAccessService().getAnalyticsProvider();
		if (null == analyticsProvider)
		{
			analyticsProvider = (AnalyticsProvider) createProviderInstance(getAnalyticsProviderBeanName());
			getSessionAccessService().setAnalyticsProvider(analyticsProvider);
		}
		return analyticsProvider;
	}

	protected Object createProviderInstance(final String providerBean)
	{
		Object provider;

		ApplicationContext applCtxt = getApplicationContext();

		if (applCtxt == null)
		{
			applCtxt = ServicelayerUtils.getApplicationContext();
			setApplicationContext(applCtxt);
		}

		if (applCtxt == null)
		{
			throw new IllegalStateException("Application Context not available");
		}


		provider = applicationContext.getBean(providerBean);

		if (LOG.isDebugEnabled())
		{
			LOG.debug("created a new provider instance of " + providerBean);
		}

		return provider;
	}

	protected ApplicationContext getApplicationContext()
	{
		return applicationContext;
	}

	/**
	 * used for tests
	 */
	void setApplicationContext(final ApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}

	protected SessionAccessService getSessionAccessService()
	{
		return sessionAccessService;
	}

	protected String getConfigurationProviderBeanName()
	{
		return CONFIG_PROVIDER_BEAN_ALIAS;
	}

	protected String getPricingProviderBeanName()
	{
		return PRICING_PROVIDER_BEAN_ALIAS;
	}

	protected String getAnalyticsProviderBeanName()
	{
		return ANALYTICS_PROVIDER_BEAN_ALIAS;
	}

	/**
	 * @param sessionAccessService
	 *           the sessionAccessService to set
	 */
	public void setSessionAccessService(final SessionAccessService sessionAccessService)
	{
		this.sessionAccessService = sessionAccessService;
	}

	/**
	 * @deprecated since 6.5, use {@link ProviderFactory#getConfigurationProvider} instead
	 * @return configuration provider bean
	 **/
	@SuppressWarnings("squid:S1133")
	@Deprecated
	@Override
	public ConfigurationProvider getProvider()
	{
		return getConfigurationProvider();
	}
}
