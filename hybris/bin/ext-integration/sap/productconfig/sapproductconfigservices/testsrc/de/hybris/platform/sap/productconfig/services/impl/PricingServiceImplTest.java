/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.interf.PricingProvider;
import de.hybris.platform.sap.productconfig.runtime.interf.ProviderFactory;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceSummaryModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceValueUpdateModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.PriceModelImpl;
import de.hybris.platform.sap.productconfig.service.testutil.DummySessionAccessService;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


/**
 *
 */
@UnitTest
public class PricingServiceImplTest
{
	private PricingServiceImpl pricingService;

	@Mock
	private PricingProvider mockedPricingProvider;

	@Mock
	private ProviderFactory mockedProviderFactory;

	private SessionAccessService dummySessionAccessService;

	private static final String configId = "1";

	@Before
	public void setUp()
	{
		MockitoAnnotations.initMocks(this);
		pricingService = Mockito.spy(new PricingServiceImpl());
		pricingService.setProviderFactory(mockedProviderFactory);
		given(mockedProviderFactory.getPricingProvider()).willReturn(mockedPricingProvider);

		dummySessionAccessService = new DummySessionAccessService();
		pricingService.setSessionAccessService(dummySessionAccessService);

	}

	@Test
	public void testGetPriceSummaryFromService()
	{
		final PriceSummaryModel priceSummaryModel = new PriceSummaryModel();
		Mockito.when(mockedPricingProvider.getPriceSummary(configId)).thenReturn(priceSummaryModel);

		assertEquals(priceSummaryModel, pricingService.getPriceSummary(configId));
		assertEquals(priceSummaryModel, dummySessionAccessService.getPriceSummaryState(configId));
	}

	@Test
	public void testGetPriceSummaryFromCache()
	{
		final PriceSummaryModel priceSummaryModel = new PriceSummaryModel();
		dummySessionAccessService.setPriceSummaryState(configId, priceSummaryModel);

		assertEquals(pricingService.getPriceSummary(configId), priceSummaryModel);
	}

	@Test
	public void testFillDeltaPrices()
	{
		final String kbId = "111";
		final List<PriceValueUpdateModel> updateModels = new ArrayList<>();
		pricingService.fillDeltaPrices(updateModels, kbId);
		Mockito.verify(mockedPricingProvider, Mockito.times(1)).fillDeltaPrices(updateModels, kbId);

	}

	@Test
	public void testIsActive()
	{
		given(Boolean.valueOf(mockedPricingProvider.isActive())).willReturn(Boolean.TRUE);
		assertTrue(pricingService.isActive());
	}

	@Test
	public void testIsNotActive()
	{
		given(Boolean.valueOf(mockedPricingProvider.isActive())).willReturn(Boolean.FALSE);
		assertFalse(pricingService.isActive());
	}

	@Test
	public void testFillConfigPrices()
	{
		final ConfigModel configModel = new ConfigModelImpl();
		final PriceSummaryModel priceSummary = new PriceSummaryModel();
		priceSummary.setBasePrice(new PriceModelImpl());
		priceSummary.setCurrentTotalPrice(new PriceModelImpl());
		priceSummary.setSelectedOptionsPrice(new PriceModelImpl());
		pricingService.fillConfigPrices(priceSummary, configModel);
		assertEquals(priceSummary.getBasePrice(), configModel.getBasePrice());
		assertEquals(priceSummary.getCurrentTotalPrice(), configModel.getCurrentTotalPrice());
		assertEquals(priceSummary.getSelectedOptionsPrice(), configModel.getSelectedOptionsPrice());

	}

	@Test
	public void testFillOverviewPrices()
	{
		final PriceSummaryModel priceSummaryModel = new PriceSummaryModel();
		Mockito.when(mockedPricingProvider.getPriceSummary(configId)).thenReturn(priceSummaryModel);
		final ConfigModel configModel = new ConfigModelImpl();
		configModel.setId(configId);
		pricingService.fillOverviewPrices(configModel);
		Mockito.verify(mockedPricingProvider).fillValuePrices(configModel);
		Mockito.verify(pricingService).getPriceSummary(configId);
	}

}
