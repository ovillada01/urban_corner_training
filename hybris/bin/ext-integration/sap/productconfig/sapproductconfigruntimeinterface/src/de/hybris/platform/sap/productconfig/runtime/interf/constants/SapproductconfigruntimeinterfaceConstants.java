/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.constants;


/**
 * Global class for all Sapproductconfigruntimeinterface constants.
 */
@SuppressWarnings(
{ "deprecation", "PMD", "squid:CallToDeprecatedMethod" })
public final class SapproductconfigruntimeinterfaceConstants extends GeneratedSapproductconfigruntimeinterfaceConstants
{
	/**
	 * Name of the sapproductconfigruntimeinterface extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigruntimeinterface";
	/**
	 * backoffice field name of the flag indicating whether pricing should be enabled or not
	 */
	public static final String CONFIGURATION_PRICING_SUPPORTED = "sapproductconfig_enable_pricing";
	/**
	 * backoffice field name of the flag indicating whether base and options price should be shown on the UI in addition
	 * to the total price
	 */
	public static final String SHOW_BASEPRICE_AND_OPTIONS = "sapproductconfig_show_baseprice_and_options";

	private SapproductconfigruntimeinterfaceConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
