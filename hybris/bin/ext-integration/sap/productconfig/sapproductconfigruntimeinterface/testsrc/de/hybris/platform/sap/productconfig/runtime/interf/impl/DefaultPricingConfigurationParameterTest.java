/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.c2l.CurrencyModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.sap.core.module.ModuleConfigurationAccess;
import de.hybris.platform.sap.sapmodel.services.SalesAreaService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;



@UnitTest
public class DefaultPricingConfigurationParameterTest
{
	private static final String SALES_ORGANIZATION_EXAMPLE = "SALES_ORGANIZATION";
	private static final String DISTRIBUTION_CHANNEL_EXAMPLE = "DISTRIBUTION_CHANNEL";
	private static final String DIVISION_EXAMPLE = "DIVISION";
	private static final String CURRENCY_SAP_CODE = "USD";
	private static final String CURRENCY_ISO_CODE = "USD";
	private static final String UNIT_SAP_CODE = "ST";
	private static final String UNIT_ISO_CODE = "PCE";

	@Mock
	protected ModuleConfigurationAccess moduleConfigurationAccess;

	@Mock
	protected SalesAreaService commonSalesAreaService;

	@Mock
	protected CurrencyModel currencyModel;

	@Mock
	protected UnitModel unitModel;


	private DefaultPricingConfigurationParameter pricingParameter;


	private static class DummyDefaultPricingConfigurationParameter extends DefaultPricingConfigurationParameter
	{

		@Override
		public String getPricingProcedure()
		{
			return null;
		}

		@Override
		public String getTargetForBasePrice()
		{
			return null;
		}

		@Override
		public String getTargetForSelectedOptions()
		{
			return null;
		}

	}

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		pricingParameter = new DummyDefaultPricingConfigurationParameter();
		pricingParameter.setModuleConfigurationAccess(moduleConfigurationAccess);
		pricingParameter.setCommonSalesAreaService(commonSalesAreaService);
	}



	@Test
	public void testGetSalesOrganization()
	{
		when(commonSalesAreaService.getSalesOrganization()).thenReturn(SALES_ORGANIZATION_EXAMPLE);

		final String salesOrganization = pricingParameter.getSalesOrganization();
		assertEquals(SALES_ORGANIZATION_EXAMPLE, salesOrganization);
	}

	@Test
	public void testGetSalesOrganizationValueNotDefined()
	{
		final String salesOrganization = pricingParameter.getSalesOrganization();
		assertNull(salesOrganization);
	}

	@Test
	public void testGetDistributionChannel()
	{
		when(commonSalesAreaService.getDistributionChannelForConditions()).thenReturn(DISTRIBUTION_CHANNEL_EXAMPLE);

		final String distributionChannel = pricingParameter.getDistributionChannelForConditions();
		assertEquals(DISTRIBUTION_CHANNEL_EXAMPLE, distributionChannel);
	}

	@Test
	public void testGetDistributionChannelValueNotDefined()
	{
		final String distributionChannel = pricingParameter.getDistributionChannelForConditions();
		assertNull(distributionChannel);
	}

	@Test
	public void testGetDivision()
	{
		when(commonSalesAreaService.getDivisionForConditions()).thenReturn(DIVISION_EXAMPLE);

		final String division = pricingParameter.getDivisionForConditions();
		assertEquals(DIVISION_EXAMPLE, division);
	}

	@Test
	public void testGetDivisionValueNotDefined()
	{
		final String division = pricingParameter.getDivisionForConditions();
		assertNull(division);
	}

	@Test
	public void testRetrieveCurrencySapCode()
	{
		when(currencyModel.getSapCode()).thenReturn(CURRENCY_SAP_CODE);

		final String currencySapCode = pricingParameter.retrieveCurrencySapCode(currencyModel);
		assertEquals(CURRENCY_SAP_CODE, currencySapCode);
	}

	@Test
	public void testRetrieveUnitSapCode()
	{
		when(unitModel.getSapCode()).thenReturn(UNIT_SAP_CODE);

		final String unitSapCode = pricingParameter.retrieveUnitSapCode(unitModel);
		assertEquals(UNIT_SAP_CODE, unitSapCode);
	}

	@Test
	public void testRetrieveCurrencyIsoCode()
	{
		when(currencyModel.getIsocode()).thenReturn(CURRENCY_ISO_CODE);

		final String currencyIsoCode = pricingParameter.retrieveCurrencyIsoCode(currencyModel);
		assertEquals(CURRENCY_ISO_CODE, currencyIsoCode);
	}

	@Test
	public void testRetrieveUnitIsoCode()
	{
		when(unitModel.getCode()).thenReturn(UNIT_ISO_CODE);

		final String unitIsoCode = pricingParameter.retrieveUnitIsoCode(unitModel);
		assertEquals(UNIT_ISO_CODE, unitIsoCode);
	}

}
