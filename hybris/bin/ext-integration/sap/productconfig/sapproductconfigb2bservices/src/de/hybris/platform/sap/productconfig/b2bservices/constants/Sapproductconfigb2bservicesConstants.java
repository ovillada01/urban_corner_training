/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.b2bservices.constants;

/**
 * Global class for all Sapproductconfigb2bservices constants. You can add global constants for your extension into this
 * class.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public final class Sapproductconfigb2bservicesConstants extends GeneratedSapproductconfigb2bservicesConstants
{
	/**
	 * unique name of this extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigb2bservices";

	private Sapproductconfigb2bservicesConstants()
	{
		//empty to avoid instantiating this constant class
	}
}
