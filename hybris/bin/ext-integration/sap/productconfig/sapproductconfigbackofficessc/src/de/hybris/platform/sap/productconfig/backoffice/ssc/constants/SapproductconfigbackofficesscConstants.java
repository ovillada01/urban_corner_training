/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.backoffice.ssc.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public final class SapproductconfigbackofficesscConstants extends GeneratedSapproductconfigbackofficesscConstants
{
	/**
	 * unique name of this extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigbackofficessc";

	private SapproductconfigbackofficesscConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
