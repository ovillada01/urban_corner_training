/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.model.constants;

/**
 * Global class for all Sapproductconfigmodel constants.
 */
@SuppressWarnings("squid:CallToDeprecatedMethod")
public final class SapproductconfigmodelConstants extends GeneratedSapproductconfigmodelConstants
{
	/**
	 * Name of the sapproductconfigmodel extension
	 */
	@SuppressWarnings("squid:S2387")
	public static final String EXTENSIONNAME = "sapproductconfigmodel";
	/**
	 * backoffice field name of the flag indicating whetherea delta load should be automatically triggered after the
	 * initial download
	 */
	public static final String START_DELTA_LOAD_AFTER_INITIAL = "sapproductconfigmodel.startDeltaloadAfterInitial";

	private SapproductconfigmodelConstants()
	{
		//empty to avoid instantiating this constant class
	}

}
