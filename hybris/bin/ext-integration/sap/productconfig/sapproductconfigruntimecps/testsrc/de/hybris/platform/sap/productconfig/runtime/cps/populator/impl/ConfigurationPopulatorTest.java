/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.populator.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.cps.masterdata.service.ConfigurationMasterDataService;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.InstanceModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ProductConfigMessage;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ProductConfigMessageSeverity;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ProductConfigMessageSource;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ProductConfigMessageSourceSubType;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.InstanceModelImpl;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class ConfigurationPopulatorTest
{
	ConfigurationPopulator classUnderTest;
	private ConfigModel target;
	private CPSConfiguration source;
	private final CPSItem rootItem = new CPSItem();
	private static final String ROOT_ITEM_ID = "A";
	private static final String CFG_ID = "1";
	private static final String KB_ID = "99";
	private static final String PRODUCT_KEY = "pCode";

	@Mock
	private Converter<CPSItem, InstanceModel> mockedInstanceConverter;
	@Mock
	private ConfigurationMasterDataService masterDataService;

	@Before
	public void initialize()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = spy(new ConfigurationPopulator());
		doReturn("There are conflicting options, please change your configuration").when(classUnderTest)
				.callLocalization(ConfigurationPopulator.SAPPRODUCTCONFIG_CPS_HEADER_CONFLICT_MESSAGE);

		target = new ConfigModelImpl();
		source = new CPSConfiguration();
		rootItem.setId(ROOT_ITEM_ID);
		source.setRootItem(rootItem);

		classUnderTest.setInstanceModelConverter(mockedInstanceConverter);
		classUnderTest.setMasterDataService(masterDataService);
		Mockito.when(Boolean.valueOf(masterDataService.isProductMultilevel(KB_ID, PRODUCT_KEY))).thenReturn(Boolean.FALSE);
		source.setId(CFG_ID);
		source.setKbId(KB_ID);
		source.setProductKey(PRODUCT_KEY);
	}

	@Test
	public void testPopulateId()
	{
		classUnderTest.populate(source, target);
		assertEquals(CFG_ID, target.getId());
	}

	@Test
	public void testPopulateKbId()
	{
		classUnderTest.populate(source, target);
		assertEquals(KB_ID, target.getKbId());
	}

	@Test
	public void testPopulateName()
	{
		classUnderTest.populate(source, target);
		assertEquals(PRODUCT_KEY, target.getName());
	}

	@Test
	public void testPopulateConsistent()
	{
		source.setConsistent(true);
		classUnderTest.populate(source, target);
		assertTrue(target.isConsistent());
	}

	@Test
	public void testPopulateComplete()
	{
		source.setComplete(true);
		classUnderTest.populate(source, target);
		assertTrue(target.isComplete());
	}

	@Test
	public void testPopulateRootItem()
	{
		rootItem.setParentConfiguration(null);
		given(mockedInstanceConverter.convert(rootItem)).willReturn(new InstanceModelImpl());
		classUnderTest.populate(source, target);
		final InstanceModel rootInstance = target.getRootInstance();
		assertNotNull(rootInstance);
		assertEquals(source, rootItem.getParentConfiguration());
	}

	@Test
	public void testKbIdAtSubItem()
	{
		classUnderTest.populate(source, target);
		final CPSItem cloudEngineItem = source.getRootItem();
		assertNotNull(cloudEngineItem);
		assertEquals(KB_ID, cloudEngineItem.getParentConfiguration().getKbId());
	}

	@Test
	public void testPopulateInConsistent()
	{
		source.setConsistent(false);
		classUnderTest.populateCoreAttributes(source, target);
		assertFalse(target.isConsistent());
		final Set<ProductConfigMessage> messages = target.getMessages();
		assertTrue(messages.size() == 1);
		final ProductConfigMessage message = messages.iterator().next();

		assertEquals("There are conflicting options, please change your configuration", message.getMessage());
		assertEquals(ConfigurationPopulator.SAPPRODUCTCONFIG_CPS_HEADER_CONFLICT_MESSAGE, message.getKey());
		assertEquals(ProductConfigMessageSeverity.WARNING, message.getSeverity());
		assertEquals(ProductConfigMessageSource.ENGINE, message.getSource());
		assertEquals(ProductConfigMessageSourceSubType.DEFAULT, message.getSourceSubType());
	}

	@Test
	public void testPopulateMultilevel()
	{
		Mockito.when(Boolean.valueOf(masterDataService.isProductMultilevel(KB_ID, PRODUCT_KEY))).thenReturn(Boolean.FALSE);
		classUnderTest.populate(source, target);
		assertTrue(target.isSingleLevel());
	}

	@Test(expected = IllegalStateException.class)
	public void testPopulateMultilevelNotSupported()
	{
		Mockito.when(Boolean.valueOf(masterDataService.isProductMultilevel(KB_ID, PRODUCT_KEY))).thenReturn(Boolean.TRUE);
		classUnderTest.populate(source, target);
	}

}
