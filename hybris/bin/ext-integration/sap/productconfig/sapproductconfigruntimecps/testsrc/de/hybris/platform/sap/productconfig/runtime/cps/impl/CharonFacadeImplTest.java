/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.cps.CPSContextSupplier;
import de.hybris.platform.sap.productconfig.runtime.cps.RequestErrorHandler;
import de.hybris.platform.sap.productconfig.runtime.cps.cache.CPSSessionCache;
import de.hybris.platform.sap.productconfig.runtime.cps.client.ConfigurationClient;
import de.hybris.platform.sap.productconfig.runtime.cps.client.ConfigurationClientBase;
import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalItem;
import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalObjectKey;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristic;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristicGroup;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristicInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCreateConfigInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSValue;
import de.hybris.platform.sap.productconfig.runtime.cps.session.impl.CookieHandlerImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationEngineException;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.external.Configuration;
import de.hybris.platform.sap.productconfig.runtime.interf.external.impl.ConfigurationImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.impl.KBKeyImpl;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.yaasconfiguration.service.YaasServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.ws.rs.core.NewCookie;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.hybris.charon.RawResponse;
import com.hybris.charon.exp.HttpException;

import rx.Observable;


@SuppressWarnings("javadoc")
@UnitTest
public class CharonFacadeImplTest
{
	private static final String PRODUCT_CODE = "ProductCode";
	private static final String EXTERNAL_CONFIG_STRING = "external configuration string";
	private static final Integer kbId = Integer.valueOf(1234);
	CharonFacadeImpl classUnderTest;
	private CookieHandlerImpl cookieHandler;
	private final CPSConfiguration configuration = new CPSConfiguration();
	private final CPSItem item = new CPSItem();
	private final CPSCharacteristicGroup group = new CPSCharacteristicGroup();
	private final CPSCharacteristic characteristic = new CPSCharacteristic();
	private final List<NewCookie> responseCookies = new ArrayList<NewCookie>();
	private final CPSExternalConfiguration externalConfiguration = new CPSExternalConfiguration();

	@Mock
	private NewCookie cookie;

	@Mock
	private ConfigurationClient client;

	@Mock
	private YaasServiceFactory yaasServiceFactory;

	private static final String cookieName = "CookieName";
	private static final String cookieValue = "CookieValue";
	private static final String cookieAsString = "CookieName=CookieValue";
	private static final String cfgId = "99";
	@Mock
	private RequestErrorHandler errorHandler;
	private String itemId;
	private String groupId;
	private String csticId;
	private CPSCharacteristicInput changes;
	@Mock
	private ObjectMapper objectMapperMock;
	@Mock
	private Observable<RawResponse<CPSConfiguration>> rawResponse;
	@Mock
	private Observable<CPSConfiguration> observable;

	private Observable<CPSCharacteristic> configObs;

	@Mock
	private Converter<Configuration, CPSExternalConfiguration> externalConfigConverter;

	@Mock
	private CPSContextSupplier contextSupplier;
	private Configuration extConfigurationTypedFormat;

	@Mock
	private CPSSessionCache cpsSessionCache;
	private final List<String> cookieList = new ArrayList<>();

	@Before
	public void initialize()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = Mockito.spy(new CharonFacadeImpl());
		cookieHandler = Mockito.spy(new CookieHandlerImpl());
		classUnderTest.setRequestErrorHandler(errorHandler);
		classUnderTest.setYaasServiceFactory(yaasServiceFactory);
		classUnderTest.setExternalConfigConverter(externalConfigConverter);

		classUnderTest.setContextSupplier(contextSupplier);

		Mockito.when(cookie.getName()).thenReturn(cookieName);
		Mockito.when(cookie.getValue()).thenReturn(cookieValue);
		Mockito.when(yaasServiceFactory.lookupService(ConfigurationClient.class)).thenReturn(client);

		cookieList.add(cookieAsString);
		cookieList.add(cookieAsString);

		Mockito.when(cpsSessionCache.getCookies(Mockito.anyString())).thenReturn(cookieList);
		//2 cookies sufficient, we don't care that their content is the same
		responseCookies.add(cookie);
		responseCookies.add(cookie);
		classUnderTest.setCookieHandler(cookieHandler);
		configuration.setId(cfgId);
		cookieHandler.setCPSSessionCache(cpsSessionCache);
		classUnderTest.setSessionCache(cpsSessionCache);
		cookieHandler.setCookies(cfgId, responseCookies);
		itemId = "1";
		groupId = "Group";
		csticId = "Cstic";
		changes = new CPSCharacteristicInput();
		changes.setValues(new ArrayList<>());

		configuration.setRootItem(item);
		item.setId(itemId);
		item.setParentConfiguration(configuration);
		item.setCharacteristicGroups(new ArrayList<>());
		item.setSubItems(new ArrayList<>());
		item.setCharacteristics(new ArrayList<CPSCharacteristic>());

		group.setId(groupId);
		group.setParentItem(item);
		addRuntimeCsticGroup(item, group);
		characteristic.setId(csticId);
		characteristic.setParentItem(item);
		characteristic.setPossibleValues(new ArrayList<>());
		characteristic.setValues(new ArrayList<>());
		addRuntimeCstic(item, characteristic);

		externalConfiguration.setComplete(false);
		externalConfiguration.setConsistent(true);

		final CPSExternalObjectKey objKey = new CPSExternalObjectKey();
		objKey.setId("PRODUCTCODE");
		final CPSExternalItem extRootItem = new CPSExternalItem();
		extRootItem.setObjectKey(objKey);
		externalConfiguration.setRootItem(extRootItem);

		Mockito.doReturn(configuration).when(classUnderTest).retrieveConfigurationAndSaveCookies(rawResponse);

		configObs = Observable.from(Arrays.asList(new CPSCharacteristic()));
		Mockito.when(client.updateConfiguration(changes, cfgId, itemId, csticId, cookieAsString, cookieAsString))
				.thenReturn(configObs);

		extConfigurationTypedFormat = new ConfigurationImpl();
		Mockito.when(externalConfigConverter.convert(extConfigurationTypedFormat)).thenReturn(externalConfiguration);
		Mockito.when(client.createRuntimeConfigurationFromExternal(externalConfiguration)).thenReturn(rawResponse);

	}

	protected void addRuntimeCsticGroup(final CPSItem item, final CPSCharacteristicGroup characteristicGroup)
	{
		if (characteristicGroup == null)
		{
			throw new IllegalArgumentException(
					new StringBuilder().append("tried to add null CharacteristicGroup to Item ").append(item.getId()).toString());
		}
		if (isRuntimeCsticGroupPresent(item, characteristicGroup.getId()))
		{
			throw new IllegalArgumentException(
					new StringBuilder().append("tried to add CharacteristicGroup with already existing id ")
							.append(characteristicGroup.getId()).append(" to Item ").append(item.getId()).toString());
		}
		item.getCharacteristicGroups().add(characteristicGroup);
	}

	protected boolean isRuntimeCsticGroupPresent(final CPSItem item, final String id)
	{
		for (final CPSCharacteristicGroup group : item.getCharacteristicGroups())
		{
			if (group.getId().equals(id))
			{
				return true;
			}
		}
		return false;
	}

	protected boolean isRuntimeCsticPresent(final CPSItem item, final String id)
	{
		for (final CPSCharacteristic characteristic : item.getCharacteristics())
		{
			if (characteristic.getId().equals(id))
			{
				return true;
			}
		}
		return false;
	}

	protected void addRuntimeCstic(final CPSItem item, final CPSCharacteristic characteristic)
	{
		if (characteristic == null)
		{
			throw new IllegalArgumentException(
					new StringBuilder().append("tried to add null Characteristic to Item ").append(item.getId()).toString());
		}
		if (isRuntimeCsticPresent(item, characteristic.getId()))
		{
			throw new IllegalArgumentException(new StringBuilder().append("tried to add Characteristic with already existing id ")
					.append(characteristic.getId()).append(" to Item ").append(item.getId()).toString());
		}
		item.getCharacteristics().add(characteristic);
	}


	@Test
	public void testAssembleCreateDefaultConfigurationRequest()
	{
		final KBKey kbKey = new KBKeyImpl(PRODUCT_CODE);
		final CPSCreateConfigInput result = classUnderTest.assembleCreateDefaultConfigurationRequest(kbKey);
		Mockito.verify(contextSupplier).retrieveContext(kbKey.getProductCode());
		assertNotNull(result);
		assertEquals(PRODUCT_CODE, result.getProductKey());
	}

	@Test
	public void testCookieHandler()
	{
		assertEquals(cookieHandler, classUnderTest.getCookieHandler());
	}

	@Test
	public void testUpdateConfigurationSingleCstic() throws ConfigurationEngineException
	{
		classUnderTest.setClient(client);
		classUnderTest.updateConfiguration(cfgId, itemId, csticId, changes);
		Mockito.verify(client).updateConfiguration(changes, cfgId, itemId, csticId, cookieAsString, cookieAsString);
	}

	@Test(expected = IllegalStateException.class)
	public void testUpdateConfigurationSingleCsticWrongNumberOFCookies() throws ConfigurationEngineException
	{
		classUnderTest.setClient(client);
		cookieList.remove(1);
		Mockito.when(cpsSessionCache.getCookies(Mockito.anyString())).thenReturn(cookieList);
		classUnderTest.updateConfiguration(cfgId, itemId, csticId, changes);
	}

	@Test
	public void testCreateCharacteristicInput()
	{
		final CPSValue value = new CPSValue();
		value.setValue("value");
		value.setSelected(true);
		characteristic.getValues().add(value);
		final CPSCharacteristicInput result = classUnderTest.createCharacteristicInput(characteristic);
		assertNotNull(result);
		assertNotNull(result.getValues());
		assertEquals(1, result.getValues().size());

	}

	@Test
	public void testUpdateConfiguration() throws ConfigurationEngineException
	{
		Mockito.when(client.updateConfiguration(Mockito.any(), Mockito.eq(cfgId), Mockito.eq(itemId), Mockito.eq(csticId),
				Mockito.eq(cookieAsString), Mockito.eq(cookieAsString))).thenReturn(configObs);
		classUnderTest.setClient(client);
		assertTrue(classUnderTest.updateConfiguration(configuration));
		Mockito.verify(client).updateConfiguration(Mockito.any(), Mockito.eq(cfgId), Mockito.eq(itemId), Mockito.eq(csticId),
				Mockito.eq(cookieAsString), Mockito.eq(cookieAsString));
	}

	@Test
	public void testUpdateConfigurationNoUpdatePerformed() throws ConfigurationEngineException
	{
		configuration.getRootItem().setCharacteristics(Collections.emptyList());
		assertFalse(classUnderTest.updateConfiguration(configuration));
	}

	@Test
	public void testGetConfiguration() throws ConfigurationEngineException
	{
		final Observable<CPSConfiguration> configObs = Observable.from(Arrays.asList(configuration));
		Mockito.when(client.getConfiguration(cfgId, cookieAsString, cookieAsString)).thenReturn(configObs);
		classUnderTest.setClient(client);
		classUnderTest.getConfiguration(cfgId);
		Mockito.verify(client).getConfiguration(cfgId, cookieAsString, cookieAsString);
	}

	@Test
	public void testGetClient()
	{
		classUnderTest.setClient(null);
		final ConfigurationClientBase result = classUnderTest.getClient();
		assertNotNull(result);
	}

	@Test
	public void testUpdateErrorHandlerCalled() throws ConfigurationEngineException
	{
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).updateConfiguration(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any());
		classUnderTest.updateConfiguration(cfgId, null, csticId, new CPSCharacteristicInput());
		Mockito.verify(errorHandler).processUpdateConfigurationError(ex);
	}

	@Test
	public void testCreateDefaultErrorHandlerCalled()
	{
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).createDefaultConfiguration(Mockito.any());
		final KBKey kbKey = new KBKeyImpl(PRODUCT_CODE);
		classUnderTest.createDefaultConfiguration(kbKey);
		Mockito.verify(errorHandler).processCreateDefaultConfigurationError(ex);
		Mockito.verify(contextSupplier).retrieveContext(kbKey.getProductCode());
	}

	@Test
	public void testGetErrorHandlerCalled() throws ConfigurationEngineException
	{
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).getConfiguration(Mockito.any(), Mockito.any(), Mockito.any());
		classUnderTest.getConfiguration(cfgId);
		Mockito.verify(errorHandler).processGetConfigurationError(ex);
	}

	@Test
	public void testGetExternalErrorHandlerCalled()
	{
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).getExternalConfiguration(Mockito.any(), Mockito.any(), Mockito.any());
		classUnderTest.getExternalConfiguration(cfgId);
		Mockito.verify(errorHandler).processGetExternalConfigurationError(ex);
	}

	@Test
	public void testDeleteErrorHandlerCalled()
	{
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).deleteConfiguration(Mockito.any(), Mockito.any(), Mockito.any());
		classUnderTest.releaseSession(cfgId);
		Mockito.verify(errorHandler).processDeleteConfigurationError(ex);
	}

	@Test
	public void testCreateConfigurationFromExternalErrorHandlerCalled()
			throws JsonParseException, JsonMappingException, IOException
	{
		Mockito.when(objectMapperMock.readValue(EXTERNAL_CONFIG_STRING, CPSExternalConfiguration.class))
				.thenReturn(externalConfiguration);
		classUnderTest.setObjectMapper(objectMapperMock);
		classUnderTest.setClient(client);
		final HttpException ex = new HttpException(Integer.valueOf(666), "something went horribly wrong");
		Mockito.doThrow(ex).when(client).createRuntimeConfigurationFromExternal(Mockito.any());
		classUnderTest.createConfigurationFromExternal(EXTERNAL_CONFIG_STRING);
		Mockito.verify(errorHandler).processCreateRuntimeConfigurationFromExternalError(ex);
	}

	@Test
	public void testEncodeNull()
	{
		assertNull(classUnderTest.encode(null));
	}

	@Test
	public void testEncode()
	{
		assertEquals("%5BGEN%5D", classUnderTest.encode("[GEN]"));
	}

	@Test
	public void testGetObjectMapper()
	{
		classUnderTest.setObjectMapper(null);
		assertNotNull(classUnderTest.getObjectMapper());
	}

	@Test
	public void testGetExternalConfiguration()
	{
		classUnderTest.setClient(client);
		final Observable<CPSExternalConfiguration> externalConfigObs = Observable.from(Arrays.asList(externalConfiguration));
		Mockito.when(client.getExternalConfiguration(cfgId, cookieAsString, cookieAsString)).thenReturn(externalConfigObs);
		final String result = classUnderTest.getExternalConfiguration(cfgId);
		assertNotNull(result);
		assertTrue(result.contains("complete"));
		assertTrue(result.contains("consistent"));
		assertTrue(result.contains("rootItem"));
		assertFalse(result.contains("non existing field"));
		assertTrue(result.startsWith("{"));
		assertTrue(result.endsWith("}"));
	}

	@Test(expected = IllegalStateException.class)
	public void testGetExternalConfiguration_invalidResponse() throws JsonProcessingException
	{
		final Observable<CPSExternalConfiguration> externalConfigObs = Observable.from(Arrays.asList(externalConfiguration));
		Mockito.when(client.getExternalConfiguration(cfgId, cookieAsString, cookieAsString)).thenReturn(externalConfigObs);
		Mockito.when(objectMapperMock.writeValueAsString(externalConfiguration))
				.thenThrow(new InvalidFormatException("message", externalConfiguration, CPSExternalConfiguration.class));
		classUnderTest.setClient(client);
		classUnderTest.setObjectMapper(objectMapperMock);
		classUnderTest.getExternalConfiguration(cfgId);
	}

	@Test
	public void testCreateConfigurationFromExternal() throws JsonParseException, JsonMappingException, IOException
	{
		Mockito.when(objectMapperMock.readValue(EXTERNAL_CONFIG_STRING, CPSExternalConfiguration.class))
				.thenReturn(externalConfiguration);
		Mockito.when(client.createRuntimeConfigurationFromExternal(externalConfiguration)).thenReturn(rawResponse);
		classUnderTest.setClient(client);
		classUnderTest.setObjectMapper(objectMapperMock);
		final CPSConfiguration result = classUnderTest.createConfigurationFromExternal(EXTERNAL_CONFIG_STRING);
		assertNotNull(result);
		assertEquals(configuration, result);
		Mockito.verify(objectMapperMock).readValue(EXTERNAL_CONFIG_STRING, CPSExternalConfiguration.class);
		Mockito.verify(client).createRuntimeConfigurationFromExternal(externalConfiguration);

	}


	@Test
	public void testCreateConfigurationFromExternal_somKbId()
	{
		final CPSConfiguration result = classUnderTest.createConfigurationFromExternal(extConfigurationTypedFormat, kbId);
		checkResultAndVerifyMocks(result);
	}

	protected void checkResultAndVerifyMocks(final CPSConfiguration result)
	{
		assertNotNull(result);
		assertEquals(configuration, result);
		Mockito.verify(client).createRuntimeConfigurationFromExternal(externalConfiguration);
	}

	@Test
	public void testReleaseSession()
	{
		classUnderTest.setClient(client);
		classUnderTest.releaseSession(cfgId);
		Mockito.verify(client).deleteConfiguration(cfgId, cookieAsString, cookieAsString);
		Mockito.verify(cookieHandler).removeCookies(cfgId);
	}

	@Test
	public void testSessionCache()
	{
		classUnderTest.setSessionCache(cpsSessionCache);
		assertEquals(cpsSessionCache, classUnderTest.getSessionCache());
	}

}
