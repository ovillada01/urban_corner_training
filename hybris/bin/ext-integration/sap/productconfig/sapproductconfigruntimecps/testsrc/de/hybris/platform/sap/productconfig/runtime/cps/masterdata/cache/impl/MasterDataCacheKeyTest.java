/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.media.storage.impl.DefaultLocalMediaFileCacheService.MediaCacheKey;
import de.hybris.platform.regioncache.key.CacheKey;
import de.hybris.platform.regioncache.key.CacheUnitValueType;
import de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.impl.MasterDataCacheKey;

import org.junit.Before;
import org.junit.Test;


@UnitTest
public class MasterDataCacheKeyTest
{
	private static final String TENANT_ID = "tenantId";
	private static final String LANG = "en";
	private static final String KB_ID = "kbId";
	private MasterDataCacheKey classUnderTest;

	@Before
	public void setup()
	{
		classUnderTest = new MasterDataCacheKey(KB_ID, LANG, TENANT_ID);
	}

	@Test
	public void testConstructor()
	{
		assertEquals(KB_ID, classUnderTest.getKbId());
		assertEquals(LANG, classUnderTest.getLang());
		assertEquals(CacheUnitValueType.NON_SERIALIZABLE, classUnderTest.getCacheValueType());
		assertEquals(TENANT_ID, classUnderTest.getTenantId());
		assertEquals(MasterDataCacheKey.TYPECODE_MASTER_DATA, classUnderTest.getTypeCode());
	}

	@Test
	public void testHashCodeEquals()
	{
		final int hashValue = classUnderTest.hashCode();
		assertTrue(hashValue != 0);
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(KB_ID, LANG, TENANT_ID);
		final int anotherHashValue = anotherKey.hashCode();
		assertTrue(hashValue == anotherHashValue);
	}

	@Test
	public void testHashCodeDifferentKb()
	{
		final int hashValue = classUnderTest.hashCode();
		assertTrue(hashValue != 0);
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey("anotherId", LANG, TENANT_ID);
		final int anotherHashValue = anotherKey.hashCode();
		assertFalse(hashValue == anotherHashValue);
	}

	@Test
	public void testHashCodeDifferentLanguage()
	{
		final int hashValue = classUnderTest.hashCode();
		assertTrue(hashValue != 0);
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(KB_ID, "anotherLang", TENANT_ID);
		final int anotherHashValue = anotherKey.hashCode();
		assertFalse(hashValue == anotherHashValue);
	}

	@Test
	public void testHashCodeDifferentTenant()
	{
		final int hashValue = classUnderTest.hashCode();
		assertTrue(hashValue != 0);
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(KB_ID, LANG, "anotherTenant");
		final int anotherHashValue = anotherKey.hashCode();
		assertFalse(hashValue == anotherHashValue);
	}

	@Test
	public void testHashCodeAllNull()
	{
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(null, null, null);
		assertEquals(31 * 31 * 31, anotherKey.hashCode());
	}

	@Test
	public void testEquals()
	{
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(KB_ID, LANG, TENANT_ID);
		assertTrue(classUnderTest.equals(anotherKey));
	}

	@Test
	public void testEqualsDifferentClass()
	{
		final CacheKey anotherKey = new MediaCacheKey(TENANT_ID, "folder", "location");
		assertFalse(classUnderTest.equals(anotherKey));
	}

	@Test
	public void testEqualsDifferentKb()
	{
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey("anotherkb", LANG, TENANT_ID);
		assertFalse(classUnderTest.equals(anotherKey));
	}

	@Test
	public void testEqualsDifferentLang()
	{
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(KB_ID, "anotherLang", TENANT_ID);
		assertFalse(classUnderTest.equals(anotherKey));
	}

	@Test(expected = NullPointerException.class)
	public void testEqualsNulls()
	{
		final MasterDataCacheKey anotherKey = new MasterDataCacheKey(null, null, TENANT_ID);
		assertFalse(anotherKey.equals(classUnderTest));
	}

	@Test
	public void testToString()
	{
		final String result = classUnderTest.toString();
		assertTrue(result.contains(KB_ID));
		assertTrue(result.contains(LANG));
		assertTrue(result.contains(TENANT_ID));
		assertTrue(result.contains(MasterDataCacheKey.TYPECODE_MASTER_DATA));
	}

}
