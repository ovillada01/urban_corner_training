/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.populator.impl;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.cps.model.pricing.PricingDocumentInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.pricing.PricingItemInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class PricingDocumentInputPopulatorTest
{
	private static final String ITEM_ID = "item1";
	private PricingDocumentInputPopulator classUnderTest;
	private PricingDocumentInput pricingDocumentInput;
	@Mock
	private Converter<CPSItem, PricingItemInput> pricingItemInputConverter;



	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = new PricingDocumentInputPopulator();
		classUnderTest.setPricingItemInputConverter(pricingItemInputConverter);
		pricingDocumentInput = new PricingDocumentInput();

	}

	@Test
	public void testfillPricingItemsInput()
	{
		pricingDocumentInput.setItems(new ArrayList<PricingItemInput>());
		final CPSConfiguration source = new CPSConfiguration();
		final CPSItem rootItems = createItem();
		source.setRootItem(rootItems);
		classUnderTest.fillPricingItemsInput(source, pricingDocumentInput);
		Mockito.verify(pricingItemInputConverter, Mockito.times(1)).convert(Mockito.any());

	}

	protected CPSItem createItem()
	{
		final CPSItem item = new CPSItem();
		item.setId(ITEM_ID);
		return item;
	}

}
