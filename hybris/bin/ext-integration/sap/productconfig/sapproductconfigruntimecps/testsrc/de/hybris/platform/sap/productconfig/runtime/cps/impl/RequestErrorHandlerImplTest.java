/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.impl;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationEngineException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hybris.charon.exp.ForbiddenException;
import com.hybris.charon.exp.HttpException;
import com.hybris.charon.exp.NotFoundException;
import com.hybris.charon.exp.ServiceUnavailableException;


@SuppressWarnings("javadoc")
@UnitTest
public class RequestErrorHandlerImplTest
{
	private static final String MESSAGE_TEXT = "message";
	private RequestErrorHandlerImpl classUnderTest;
	HttpException ex;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = spy(new RequestErrorHandlerImpl());
		doReturn("server error msg").when(classUnderTest).getServerMessage(Mockito.any());
	}

	@Test(expected = ConfigurationEngineException.class)
	public void testProcessUpdateConfigurationError() throws ConfigurationEngineException
	{
		final HttpException ex = new ForbiddenException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processGetConfigurationError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessCreateDefaultConfigurationError()
	{
		final HttpException ex = new ServiceUnavailableException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processCreateDefaultConfigurationError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessGetConfigurationError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processCreateDefaultConfigurationError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessDeleteConfigurationError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processDeleteConfigurationError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessGetExternalConfigurationError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processGetExternalConfigurationError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessCreateRuntimeConfigurationFromExternalError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processCreateRuntimeConfigurationFromExternalError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessCreatePricingDocumentError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processCreatePricingDocumentError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testProcessHasKbError()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(123), MESSAGE_TEXT);
		classUnderTest.processHasKbError(ex);
	}

	@Test(expected = IllegalStateException.class)
	public void testIfNotFound()
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(404), MESSAGE_TEXT);
		classUnderTest.ifNotFoundThrowIllegalState(ex);
	}

	@Test
	public void testIfNotFoundGenericException()
	{
		final HttpException ex = new HttpException(Integer.valueOf(404), MESSAGE_TEXT);
		//In this case we don't expect a RT exception since the http exception doesn't indicate 'not found'
		classUnderTest.ifNotFoundThrowIllegalState(ex);
	}

	@Test(expected = ConfigurationEngineException.class)
	public void testCheckNotFound() throws ConfigurationEngineException
	{
		final HttpException ex = new NotFoundException(Integer.valueOf(404), MESSAGE_TEXT);
		classUnderTest.checkNotFound(ex);
	}

	@Test
	public void testCheckNotFoundGenericException() throws ConfigurationEngineException
	{
		final HttpException ex = new HttpException(Integer.valueOf(404), MESSAGE_TEXT);
		//In this case we don't expect a RT exception since the http exception doesn't indicate 'not found'
		classUnderTest.checkNotFound(ex);
	}

	@Test
	public void testTraceRequestError()
	{
		ex = new HttpException(Integer.valueOf(500), "some error msg", null);
		classUnderTest.logRequestError("pci", ex);
		verify(classUnderTest).getServerMessage(ex);
	}

}
