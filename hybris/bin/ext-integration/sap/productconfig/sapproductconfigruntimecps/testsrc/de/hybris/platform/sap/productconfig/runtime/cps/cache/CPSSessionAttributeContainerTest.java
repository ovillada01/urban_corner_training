package de.hybris.platform.sap.productconfig.runtime.cps.cache;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import de.hybris.bootstrap.annotations.UnitTest;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;


@SuppressWarnings("javadoc")
@UnitTest
public class CPSSessionAttributeContainerTest
{
	private static final String configid = "Id";
	private static final String cookie = "Cookie";
	CPSSessionAttributeContainer classUnderTest = new CPSSessionAttributeContainer();
	private final List<String> cookieList = new ArrayList<String>();

	@Before
	public void initialize()
	{
		cookieList.add(cookie);
		classUnderTest.setCookies(configid, cookieList);
	}

	@Test
	public void testGetCookies()
	{
		assertEquals(cookieList, classUnderTest.getCookies(configid));
	}

	@Test
	public void testRemoveCookies()
	{
		classUnderTest.removeCookies(configid);
		assertNull(classUnderTest.getCookies(configid));
	}
}
