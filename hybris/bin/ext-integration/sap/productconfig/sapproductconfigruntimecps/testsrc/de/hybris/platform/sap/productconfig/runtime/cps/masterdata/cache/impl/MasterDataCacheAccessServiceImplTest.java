/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.regioncache.CacheValueLoader;
import de.hybris.platform.regioncache.region.impl.EHCacheRegion;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataKnowledgeBaseContainer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@SuppressWarnings("javadoc")
@UnitTest
public class MasterDataCacheAccessServiceImplTest
{
	private static final String TENANT_ID = "tenantId";
	private static final String LANGUAGE = "language";
	private static final String KB_ID = "kbId";
	private MasterDataCacheAccessServiceImpl classUnderTest;

	@Mock
	private EHCacheRegion cache;
	@Mock
	private CacheValueLoader<CPSMasterDataKnowledgeBaseContainer> loader;

	private CPSMasterDataKnowledgeBaseContainer kbContainer;

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = new MasterDataCacheAccessServiceImpl();
		classUnderTest.setTenantId(TENANT_ID);
		classUnderTest.setCache(cache);
		classUnderTest.setLoader(loader);

		kbContainer = new CPSMasterDataKnowledgeBaseContainer();

		Mockito.when(cache.getWithLoader(Mockito.any(), Mockito.any())).thenReturn(kbContainer);
	}

	@Test
	public void testGetKB()
	{
		final CPSMasterDataKnowledgeBaseContainer result = classUnderTest.getKbContainer(KB_ID, LANGUAGE);
		Mockito.verify(cache).getWithLoader(Mockito.any(), Mockito.any());
		assertNotNull(result);
		assertEquals(kbContainer, result);
	}

	@Test
	public void testGetTenantId()
	{
		assertEquals(TENANT_ID, classUnderTest.getTenantId());
	}

	@Test
	public void testCreateCacheKey()
	{
		final MasterDataCacheKey result = classUnderTest.createCacheKey(KB_ID, LANGUAGE);
		assertNotNull(result);
		assertEquals(KB_ID, result.getKbId());
		assertEquals(LANGUAGE, result.getLang());
	}
}
