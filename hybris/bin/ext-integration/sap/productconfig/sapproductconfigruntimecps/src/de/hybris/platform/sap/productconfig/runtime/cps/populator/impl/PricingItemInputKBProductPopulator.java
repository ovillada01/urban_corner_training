/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.populator.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.product.UnitModel;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.CPSMasterDataPossibleValueSpecific;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataCharacteristicSpecificContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataProductContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.pricing.PricingItemInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSVariantCondition;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;


/**
 * Popuplates the pricing item input data for querrying static pricing information, such as value prices, based on the
 * configuration master data.
 */
public class PricingItemInputKBProductPopulator extends AbstractPricingItemInputPopulator
		implements Populator<CPSMasterDataProductContainer, PricingItemInput>
{

	@Override
	public void populate(final CPSMasterDataProductContainer source, final PricingItemInput target)
	{
		fillCoreAttributes(source.getId(), createQty(Double.valueOf(1), getIsoUOM(source)), target);
		fillPricingAttributes(source.getId(), target);
		fillAccessDates(target);
		fillVariantConditions(source, target);
	}

	protected void fillVariantConditions(final CPSMasterDataProductContainer source, final PricingItemInput target)
	{
		target.setVariantConditions(new ArrayList<>());
		handleCstics(target, source.getCstics());
	}

	protected void handleCstics(final PricingItemInput target,
			final Map<String, CPSMasterDataCharacteristicSpecificContainer> cstics)
	{
		final Iterator<Entry<String, CPSMasterDataCharacteristicSpecificContainer>> csticsIterator = cstics.entrySet().iterator();
		while (csticsIterator.hasNext())
		{
			final Entry<String, CPSMasterDataCharacteristicSpecificContainer> csticSpecificContainerEntry = csticsIterator.next();
			final CPSMasterDataCharacteristicSpecificContainer csticSpecificContainer = csticSpecificContainerEntry.getValue();
			addVariantConditionsForPossibleValueSpecifics(target, csticSpecificContainer);
		}
	}

	protected void addVariantConditionsForPossibleValueSpecifics(final PricingItemInput target,
			final CPSMasterDataCharacteristicSpecificContainer csticSpecificContainer)
	{
		final Iterator<Entry<String, CPSMasterDataPossibleValueSpecific>> valuesIterator = csticSpecificContainer
				.getPossibleValueSpecifics().entrySet().iterator();
		while (valuesIterator.hasNext())
		{
			final Entry<String, CPSMasterDataPossibleValueSpecific> possibleValueSpecificEntry = valuesIterator.next();
			final CPSMasterDataPossibleValueSpecific possibleValueSpecific = possibleValueSpecificEntry.getValue();
			target.getVariantConditions().add(createVariantCondition(possibleValueSpecific.getVariantConditionKey()));
		}
	}

	protected CPSVariantCondition createVariantCondition(final String key)
	{
		final CPSVariantCondition variantCondition = new CPSVariantCondition();
		variantCondition.setFactor(String.valueOf(1));
		variantCondition.setKey(key);
		return variantCondition;
	}

	protected String getIsoUOM(final CPSMasterDataProductContainer product)
	{
		final ProductModel productModel = getProductService().getProductForCode(product.getId());
		final UnitModel unitModel = productModel.getUnit();
		return getPricingConfigurationParameter().retrieveUnitIsoCode(unitModel);
	}



}
