/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps;

import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.pricing.PricingDocumentResult;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationEngineException;

import com.hybris.charon.exp.HttpException;


/**
 * Used to wrap exceptions originating from REST calls through the charon framework.
 */
public interface RequestErrorHandler
{
	/**
	 * processes exception from configuration update request
	 *
	 * @param ex
	 *           exception to process
	 * @throws ConfigurationEngineException
	 *
	 */
	void processUpdateConfigurationError(HttpException ex) throws ConfigurationEngineException;

	/**
	 * processes exception from configuration create default configuration request
	 *
	 * @param ex
	 *           exception to process
	 * @return default configuration
	 *
	 */
	CPSConfiguration processCreateDefaultConfigurationError(HttpException ex);

	/**
	 * processes exception from configuration create default configuration request
	 *
	 * @param ex
	 *           exception to process
	 * @return configuration update result
	 * @throws ConfigurationEngineException
	 */
	CPSConfiguration processGetConfigurationError(HttpException ex) throws ConfigurationEngineException;

	/**
	 * processes exception from delete configuration request
	 *
	 * @param ex
	 *           exception to process
	 */
	void processDeleteConfigurationError(HttpException ex);

	/**
	 * processes exception from get external configuration request
	 *
	 * @param ex
	 *           exception to process
	 * @return external representation of configuration
	 */
	CPSExternalConfiguration processGetExternalConfigurationError(HttpException ex);

	/**
	 * processes exception from create configuration from external request
	 *
	 * @param ex
	 *           exception to process
	 * @return runtime configuration
	 */
	CPSConfiguration processCreateRuntimeConfigurationFromExternalError(HttpException ex);

	/**
	 * processes exception from create pricing document request
	 *
	 * @param ex
	 *           exception to process
	 * @return pricing document result
	 */
	PricingDocumentResult processCreatePricingDocumentError(HttpException ex);

	/**
	 * processes exception from get knowledgebase request
	 *
	 * @param ex
	 *           exception to process
	 * @return whether kb exists
	 */
	boolean processHasKbError(HttpException ex);

}
