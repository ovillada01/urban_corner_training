/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.impl;

import de.hybris.platform.sap.productconfig.runtime.cps.CharonFacade;
import de.hybris.platform.sap.productconfig.runtime.cps.CharonKbDeterminationFacade;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristic;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSValue;
import de.hybris.platform.sap.productconfig.runtime.cps.pricing.PricingHandler;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationEngineException;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationProvider;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.external.Configuration;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticValueModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.InstanceModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;



/**
 * Communication to cloud engine
 */
public class CPSConfigurationProvider implements ConfigurationProvider
{
	private Converter<CPSConfiguration, ConfigModel> configModelConverter;
	private CharonFacade charonFacade;
	private PricingHandler pricingHandler;
	private CharonKbDeterminationFacade charonKbDeterminationFacade;


	@Override
	public ConfigModel createDefaultConfiguration(final KBKey kbKey)
	{
		final CPSConfiguration cloudEngineDefaultConfiguration = charonFacade.createDefaultConfiguration(kbKey);
		getPricingHandler().preparePricingDocumentInput(cloudEngineDefaultConfiguration);
		return configModelConverter.convert(cloudEngineDefaultConfiguration);
	}

	@Override
	public boolean updateConfiguration(final ConfigModel model) throws ConfigurationEngineException
	{
		final CPSConfiguration changedConfiguration = prepareChangedConfiguration(model);
		return charonFacade.updateConfiguration(changedConfiguration);
	}

	protected CPSConfiguration prepareChangedConfiguration(final ConfigModel model)
	{
		final CPSConfiguration changedConfiguration = new CPSConfiguration();
		changedConfiguration.setId(model.getId());
		final CPSItem changedRootItem = processInstance(model.getRootInstance());
		changedConfiguration.setRootItem(changedRootItem);
		return changedConfiguration;
	}

	protected CPSItem processInstance(final InstanceModel instanceModel)
	{
		final CPSItem changedItem = new CPSItem();
		changedItem.setSubItems(new ArrayList<>());
		changedItem.setId(instanceModel.getId());
		changedItem.setCharacteristics(collectInstanceCsticChanges(instanceModel));
		final List<InstanceModel> subInstances = instanceModel.getSubInstances();
		for (final InstanceModel subInstanceModel : subInstances)
		{
			final CPSItem changedSubItem = processInstance(subInstanceModel);
			changedItem.getSubItems().add(changedSubItem);
		}
		return changedItem;
	}

	protected List<CPSCharacteristic> collectInstanceCsticChanges(final InstanceModel instanceModel)
	{
		final List<CPSCharacteristic> changedCstics = new ArrayList<>();

		final List<CsticModel> csticModels = instanceModel.getCstics();
		for (final CsticModel csticModel : csticModels)
		{
			if (csticModel.isChangedByFrontend())
			{
				final CPSCharacteristic changedCstic = new CPSCharacteristic();
				changedCstic.setId(csticModel.getName());
				changedCstic.setValues(collectChangedValues(csticModel));
				changedCstics.add(changedCstic);
			}
		}
		return changedCstics;
	}

	protected List<CPSValue> collectChangedValues(final CsticModel csticModel)
	{
		final List<CPSValue> changedValues = new ArrayList<>();
		final List<CsticValueModel> assignedValues = csticModel.getAssignedValues();
		final List<CsticValueModel> assignableValues = csticModel.getAssignableValues();
		if (csticModel.isMultivalued())
		{
			for (final CsticValueModel valueModel : assignableValues)
			{
				addCloudEngineValue(valueModel.getName(), assignedValues.contains(valueModel), changedValues);
			}
		}
		else
		{
			final String value = csticModel.getSingleValue();
			if (value != null && !value.isEmpty())
			{
				addCloudEngineValue(csticModel.getSingleValue(), true, changedValues);
			}
			else
			{
				handleDeselection(changedValues, assignableValues);
			}
		}
		return changedValues;

	}

	protected void handleDeselection(final List<CPSValue> changedValues, final List<CsticValueModel> assignableValues)
	{
		for (final CsticValueModel valueModel : assignableValues)
		{
			if (CsticValueModel.AUTHOR_USER.equals(valueModel.getAuthor()))
			{
				addCloudEngineValue(valueModel.getName(), false, changedValues);
			}
		}
	}

	protected void addCloudEngineValue(final String value, final boolean selected, final List<CPSValue> valuesList)
	{
		final CPSValue changedValue = new CPSValue();
		changedValue.setValue(value);

		//do not set characteristicId here as cloud engine is not aware of this attribute
		//its purpose only is to add it as key to be able to fetch master data for it

		changedValue.setSelected(selected);
		valuesList.add(changedValue);
	}



	@Override
	public ConfigModel retrieveConfigurationModel(final String configId) throws ConfigurationEngineException
	{
		final CPSConfiguration cloudEngineConfigurationState = charonFacade.getConfiguration(configId);
		getPricingHandler().preparePricingDocumentInput(cloudEngineConfigurationState);
		return configModelConverter.convert(cloudEngineConfigurationState);
	}

	@Override
	public String retrieveExternalConfiguration(final String configId)
	{
		return getCharonFacade().getExternalConfiguration(configId);
	}



	@Override
	public ConfigModel createConfigurationFromExternalSource(final KBKey kbKey, final String extConfig)
	{
		final CPSConfiguration configuration = getCharonFacade().createConfigurationFromExternal(extConfig);
		getPricingHandler().preparePricingDocumentInput(configuration);
		return getConfigModelConverter().convert(configuration);
	}

	@Override
	public void releaseSession(final String configId)
	{
		getCharonFacade().releaseSession(configId);
	}

	@Override
	public ConfigModel createConfigurationFromExternalSource(final Configuration extConfig)
	{
		final Integer kbId = findKbId(extConfig.getKbKey());
		final CPSConfiguration configuration = getCharonFacade().createConfigurationFromExternal(extConfig, kbId);
		getPricingHandler().preparePricingDocumentInput(configuration);
		return getConfigModelConverter().convert(configuration);
	}

	/**
	 * This method is only intended for situations where product and date are provided as part of the KB key. If this is
	 * not the case, an exception is thrown
	 *
	 * @param kbKey
	 * @return KB ID
	 */
	protected Integer findKbId(final KBKey kbKey)
	{
		if (kbKey == null || kbKey.getProductCode() == null || kbKey.getDate() == null)
		{
			throw new IllegalArgumentException("Either kbKey, product or date are not provided");
		}
		return getCharonKbDeterminationFacade().readKbIdForDate(kbKey.getProductCode(), kbKey.getDate());
	}

	/**
	 * Set converter, target: (engine independent) models
	 *
	 * @param configModelConverter
	 */
	@Required
	public void setConfigModelConverter(final Converter<CPSConfiguration, ConfigModel> configModelConverter)
	{
		this.configModelConverter = configModelConverter;
	}

	protected Converter<CPSConfiguration, ConfigModel> getConfigModelConverter()
	{
		return configModelConverter;
	}

	protected CharonFacade getCharonFacade()
	{
		return charonFacade;
	}

	/**
	 * @param charonFacade
	 *           the charonFacade to set
	 */
	@Required
	public void setCharonFacade(final CharonFacade charonFacade)
	{
		this.charonFacade = charonFacade;
	}

	@Override
	public boolean isKbForDateExists(final String productCode, final Date kbDate)
	{
		return getCharonKbDeterminationFacade().hasKbForDate(productCode, kbDate);
	}

	@Override
	public boolean isKbVersionExists(final KBKey kbKey, final String externalConfig)
	{
		return getCharonKbDeterminationFacade().hasKbForExtConfig(kbKey.getProductCode(), externalConfig);
	}

	protected PricingHandler getPricingHandler()
	{
		return pricingHandler;
	}

	/**
	 * @param pricingHandler
	 */
	@Required
	public void setPricingHandler(final PricingHandler pricingHandler)
	{
		this.pricingHandler = pricingHandler;
	}

	/**
	 * @param charonKbDeterminationFacade
	 *           the charonKbDeterminationFacade to set
	 */
	@Required
	public void setCharonKbDeterminationFacade(final CharonKbDeterminationFacade charonKbDeterminationFacade)
	{
		this.charonKbDeterminationFacade = charonKbDeterminationFacade;
	}

	protected CharonKbDeterminationFacade getCharonKbDeterminationFacade()
	{
		return this.charonKbDeterminationFacade;
	}

	@Override
	public boolean isConfigureVariantSupported()
	{
		return false;
	}

}
