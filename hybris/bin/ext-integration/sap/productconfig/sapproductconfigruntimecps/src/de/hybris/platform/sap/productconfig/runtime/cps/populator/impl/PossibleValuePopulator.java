/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.populator.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSPossibleValue;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticValueModel;

import org.apache.commons.lang.StringUtils;




/**
 * Responsible to populate characteristics
 */
public class PossibleValuePopulator implements Populator<CPSPossibleValue, CsticValueModel>
{

	@Override
	public void populate(final CPSPossibleValue source, final CsticValueModel target)
	{
		populateCoreAttributes(source, target);

	}


	protected void populateCoreAttributes(final CPSPossibleValue source, final CsticValueModel target)
	{
		final String valueLow = source.getValueLow();
		if (StringUtils.isNotEmpty(valueLow) && StringUtils.isEmpty(source.getValueHigh()))
		{
			target.setName(valueLow);
		}
		else
		{
			populateAttributesForIntervalCstics(source, target);
		}
		target.setSelectable(source.isSelectable());
		target.setDomainValue(source.isSelectable());
	}


	protected void populateAttributesForIntervalCstics(final CPSPossibleValue source, final CsticValueModel target)
	{
		final String valueLow = source.getValueLow();
		final String valueHigh = source.getValueHigh();
		if (StringUtils.isEmpty(valueLow))
		{
			throw new IllegalStateException("ValueLow must not be null or empty");
		}

		if (StringUtils.isNotEmpty(valueHigh))
		{
			final String interval = new StringBuilder().append(valueLow).append("-").append(valueHigh).toString();
			target.setName(interval);
			target.setLanguageDependentName(interval);
		}
	}

}
