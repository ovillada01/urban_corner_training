/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.pricing.impl;

import de.hybris.platform.sap.productconfig.runtime.cps.constants.SapproductconfigruntimecpsConstants;
import de.hybris.platform.sap.productconfig.runtime.cps.pricing.PricingConfigurationParameterCPS;
import de.hybris.platform.sap.productconfig.runtime.interf.impl.DefaultPricingConfigurationParameter;

import org.jboss.logging.Logger;


/**
 * Default implementation of {@link PricingConfigurationParameterCPS}
 */
public class DefaultPricingConfigurationParameterCPS extends DefaultPricingConfigurationParameter implements
		PricingConfigurationParameterCPS
{

	private static final Logger LOG = Logger.getLogger(DefaultPricingConfigurationParameterCPS.class);

	@Override
	public String getTargetForBasePrice()
	{
		String targetBasePrice = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess().getProperty(
					SapproductconfigruntimecpsConstants.CONFIGURATION_CONDITION_FUNCTION_BASE_PRICE);

			if (propertyValue instanceof String)
			{
				targetBasePrice = (String) propertyValue;
				LOG.debug("Condition for base price is: " + targetBasePrice);
			}
		}

		return targetBasePrice;
	}

	@Override
	public String getTargetForSelectedOptions()
	{
		String targetSelectedOptions = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess().getProperty(
					SapproductconfigruntimecpsConstants.CONFIGURATION_CONDITION_FUNCTION_SELECTED_OPTIONS);

			if (propertyValue instanceof String)
			{
				targetSelectedOptions = (String) propertyValue;
				LOG.debug("Condition for selected options is: " + targetSelectedOptions);
			}
		}

		return targetSelectedOptions;
	}

	@Override
	public String getPricingProcedure()
	{
		String pricingProcedure = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess().getProperty(
					SapproductconfigruntimecpsConstants.CONFIGURATION_PRICING_PROCEDURE);

			if (propertyValue instanceof String)
			{
				pricingProcedure = (String) propertyValue;
				LOG.debug("Pricing procedure is: " + pricingProcedure);
			}
		}

		return pricingProcedure;
	}
}
