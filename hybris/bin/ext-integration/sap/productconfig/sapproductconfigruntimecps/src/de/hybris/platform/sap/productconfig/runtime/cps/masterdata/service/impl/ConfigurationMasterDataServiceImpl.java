/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.masterdata.service.impl;

import de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.MasterDataCacheAccessService;
import de.hybris.platform.sap.productconfig.runtime.cps.masterdata.service.ConfigurationMasterDataService;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.CPSMasterDataCharacteristicGroup;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.CPSMasterDataPossibleValue;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.CPSMasterDataPossibleValueSpecific;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataCharacteristicContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataCharacteristicSpecificContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataClassContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataKnowledgeBaseContainer;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataProductContainer;
import de.hybris.platform.servicelayer.i18n.I18NService;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;


/**
 * Default implementation of {@link ConfigurationMasterDataService}. Accesses a hybris cache via
 * {@link MasterDataCacheAccessService} for providing configuration related master data
 */
public class ConfigurationMasterDataServiceImpl implements ConfigurationMasterDataService
{

	private MasterDataCacheAccessService cacheAccessService;
	private I18NService i18NService;

	private static final String TYPE_MARA = "MARA";
	private static final String TYPE_KLAH = "KLAH";
	private static final String CSTIC_TYPE_STRING = "string";

	@Override
	public String getItemName(final String kbId, final String id, final String type)
	{
		String name;
		if (type == null)
		{
			throw new IllegalStateException("Type is null.");
		}
		switch (type)
		{
			case TYPE_MARA:
				name = getProductName(kbId, id);
				break;
			case TYPE_KLAH:
				name = getClassName(kbId, id);
				break;
			default:
				throw new IllegalStateException("Invalide type: " + type);

		}

		return name;
	}

	protected String getProductName(final String kbId, final String id)
	{
		return getProduct(kbId, id).getName();
	}

	@Override
	public boolean isProductMultilevel(final String kbId, final String id)
	{
		return getProduct(kbId, id).isMultilevel();
	}

	protected CPSMasterDataProductContainer getProduct(final String kbId, final String id)
	{
		final CPSMasterDataProductContainer product = cacheAccessService
				.getKbContainer(kbId, getI18NService().getCurrentLocale().getLanguage()).getProducts().get(id);
		if (product == null)
		{
			throw new IllegalStateException("Could not find product for: " + id);
		}
		return product;
	}

	protected String getClassName(final String kbId, final String id)
	{
		final CPSMasterDataClassContainer classContainer = cacheAccessService
				.getKbContainer(kbId, getI18NService().getCurrentLocale().getLanguage()).getClasses().get(id);
		if (classContainer == null)
		{
			throw new IllegalStateException("Could not find class for: " + id);
		}
		return classContainer.getName();
	}

	@Override
	public String getGroupName(final String kbId, final String productId, final String groupId)
	{
		return getGroup(kbId, productId, groupId).getName();
	}

	protected CPSMasterDataCharacteristicGroup getGroup(final String kbId, final String productId, final String groupId)
	{
		final CPSMasterDataProductContainer product = getProduct(kbId, productId);

		final CPSMasterDataCharacteristicGroup group = product.getGroups().get(groupId);
		if (group == null)
		{
			throw new IllegalStateException("Could not find group for: " + groupId);
		}
		return group;
	}

	@Override
	public CPSMasterDataCharacteristicContainer getCharacteristic(final String kbId, final String characteristicId)
	{
		final CPSMasterDataCharacteristicContainer csticContainer = getMasterData(kbId).getCharacteristics().get(characteristicId);
		if (csticContainer == null)
		{
			throw new IllegalStateException("Could not find characteristic for: " + characteristicId);
		}
		return csticContainer;
	}

	@Override
	public String getValueName(final String kbId, final String characteristicId, final String valueId)
	{
		final CPSMasterDataCharacteristicContainer cstic = getCharacteristic(kbId, characteristicId);
		if (!isCsticStringType(cstic))
		{
			// For cstics with a type different from "string", the value names are formatted in facade-layer.
			return null;
		}
		final CPSMasterDataPossibleValue value = cstic.getPossibleValueGlobals().get(valueId);
		if (value == null)
		{
			return valueId;
		}
		return value.getName();

	}

	@Override
	public CPSMasterDataKnowledgeBaseContainer getMasterData(final String kbId)
	{
		if (StringUtils.isEmpty(kbId))
		{
			throw new IllegalArgumentException("KbId is not provided, expecting a non-empty string");
		}
		return getCacheAccessService().getKbContainer(kbId, getI18NService().getCurrentLocale().getLanguage());
	}

	@Override
	public List<String> getGroupCharacteristicIDs(final String kbId, final String productId, final String groupId)
	{
		return getGroup(kbId, productId, groupId).getCharacteristicIDs();
	}

	@Override
	public String getValuePricingKey(final String kbId, final String productId, final String characteristicId,
			final String valueId)
	{
		final CPSMasterDataCharacteristicSpecificContainer csticSpecific = getMasterData(kbId).getProducts().get(productId)
				.getCstics().get(characteristicId);
		if (csticSpecific == null)
		{
			return null;
		}
		final CPSMasterDataPossibleValueSpecific value = csticSpecific.getPossibleValueSpecifics().get(valueId);
		if (value == null)
		{
			return null;
		}
		return value.getVariantConditionKey();

	}

	@Override
	public Set<String> getSpecificPossibleValueIds(final String kbId, final String productId, final String characteristicId)
	{
		final CPSMasterDataCharacteristicSpecificContainer csticSpecific = getMasterData(kbId).getProducts().get(productId)
				.getCstics().get(characteristicId);
		if (csticSpecific == null)
		{
			return Collections.emptySet();
		}
		return csticSpecific.getPossibleValueSpecifics().keySet();
	}

	@Override
	public Set<String> getPossibleValueIds(final String kbId, final String characteristicId)
	{
		final CPSMasterDataCharacteristicContainer cstic = getMasterData(kbId).getCharacteristics().get(characteristicId);
		if (cstic == null)
		{
			return Collections.emptySet();
		}
		return cstic.getPossibleValueGlobals().keySet();
	}

	protected boolean isCsticStringType(final CPSMasterDataCharacteristicContainer cstic)
	{
		return CSTIC_TYPE_STRING.equals(cstic.getType());
	}

	/**
	 * @return the cacheAccessService
	 */
	public MasterDataCacheAccessService getCacheAccessService()
	{
		return cacheAccessService;
	}

	/**
	 * @param cacheAccessService
	 *           the cacheAccessService to set
	 */
	public void setCacheAccessService(final MasterDataCacheAccessService cacheAccessService)
	{
		this.cacheAccessService = cacheAccessService;
	}

	/**
	 * @return the i18NService
	 */
	public I18NService getI18NService()
	{
		return i18NService;
	}

	/**
	 * @param i18nService
	 *           the i18NService to set
	 */
	public void setI18NService(final I18NService i18nService)
	{
		i18NService = i18nService;
	}

}
