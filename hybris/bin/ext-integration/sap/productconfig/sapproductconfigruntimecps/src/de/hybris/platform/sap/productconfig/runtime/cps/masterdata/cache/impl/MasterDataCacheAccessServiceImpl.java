/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.impl;

import de.hybris.platform.core.Registry;
import de.hybris.platform.regioncache.CacheValueLoader;
import de.hybris.platform.regioncache.region.impl.EHCacheRegion;
import de.hybris.platform.sap.productconfig.runtime.cps.masterdata.cache.MasterDataCacheAccessService;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.cache.CPSMasterDataKnowledgeBaseContainer;


/**
 * Default implementation of {@link MasterDataCacheAccessService}. Uses {@link EHCacheRegion} for caching. Caches KB
 * data per id and language, which means that master data attributes like characteristic types e.g. are cached
 * redundantly.
 */
public class MasterDataCacheAccessServiceImpl implements MasterDataCacheAccessService
{
	private String tenantId;
	private EHCacheRegion cache;
	private CacheValueLoader<CPSMasterDataKnowledgeBaseContainer> loader;

	@Override
	public CPSMasterDataKnowledgeBaseContainer getKbContainer(final String kbId, final String language)
	{
		final MasterDataCacheKey cacheKey = createCacheKey(kbId, language);

		return (CPSMasterDataKnowledgeBaseContainer) getCache().getWithLoader(cacheKey, getLoader());
	}

	protected MasterDataCacheKey createCacheKey(final String kbId, final String language)
	{
		return new MasterDataCacheKey(kbId, language, getTenantId());
	}

	protected String getTenantId()
	{
		if (tenantId == null)
		{
			return Registry.getCurrentTenant().getTenantID();
		}
		return tenantId;
	}

	@Override
	public void clearCache()
	{
		getCache().clearCache();
	}

	public void setTenantId(final String tenantId)
	{
		this.tenantId = tenantId;
	}

	@Override
	public EHCacheRegion getCache()
	{
		return cache;
	}

	/**
	 * @param cache
	 *           Cache region
	 */
	public void setCache(final EHCacheRegion cache)
	{
		this.cache = cache;
	}

	protected CacheValueLoader<CPSMasterDataKnowledgeBaseContainer> getLoader()
	{
		return loader;
	}

	/**
	 * @param loader
	 *           Loader for reading KB data via REST
	 */
	public void setLoader(final CacheValueLoader<CPSMasterDataKnowledgeBaseContainer> loader)
	{
		this.loader = loader;
	}
}
