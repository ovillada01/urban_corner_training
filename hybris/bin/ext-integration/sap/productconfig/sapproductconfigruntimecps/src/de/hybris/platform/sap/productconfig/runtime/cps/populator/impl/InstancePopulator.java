/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.populator.impl;

import de.hybris.platform.converters.Populator;
import de.hybris.platform.sap.productconfig.runtime.cps.constants.SapproductconfigruntimecpsConstants;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristic;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristicGroup;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticGroupModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.InstanceModel;
import de.hybris.platform.servicelayer.dto.converter.Converter;


/**
 * Responsible to populate instances. Also orchestrates the conversion of dependent objects (sub instances, groups)
 */
public class InstancePopulator implements Populator<CPSItem, InstanceModel>
{

	private Converter<CPSItem, InstanceModel> instanceModelConverter;
	private Converter<CPSCharacteristicGroup, CsticGroupModel> characteristicGroupConverter;
	private Converter<CPSCharacteristic, CsticModel> characteristicConverter;


	/**
	 * @return the characteristicConverter
	 */
	public Converter<CPSCharacteristic, CsticModel> getCharacteristicConverter()
	{
		return characteristicConverter;
	}

	/**
	 * @param characteristicConverter
	 *           the characteristicConverter to set
	 */
	public void setCharacteristicConverter(final Converter<CPSCharacteristic, CsticModel> characteristicConverter)
	{
		this.characteristicConverter = characteristicConverter;
	}

	/**
	 * @return the characteristicGroupConverter
	 */
	public Converter<CPSCharacteristicGroup, CsticGroupModel> getCharacteristicGroupConverter()
	{
		return characteristicGroupConverter;
	}

	/**
	 * @param characteristicGroupConverter
	 *           the characteristicGroupConverter to set
	 */
	public void setCharacteristicGroupConverter(
			final Converter<CPSCharacteristicGroup, CsticGroupModel> characteristicGroupConverter)
	{
		this.characteristicGroupConverter = characteristicGroupConverter;
	}

	/**
	 * @return the instanceModelConverter
	 */
	public Converter<CPSItem, InstanceModel> getInstanceModelConverter()
	{
		return instanceModelConverter;
	}

	/**
	 * @param instanceModelConverter
	 *           the instanceModelConverter to set
	 */
	public void setInstanceModelConverter(final Converter<CPSItem, InstanceModel> instanceModelConverter)
	{
		this.instanceModelConverter = instanceModelConverter;
	}



	@Override
	public void populate(final CPSItem source, final InstanceModel target)
	{
		populateCoreAttributes(source, target);
		populateSubItems(source, target);
		populateGroups(source, target);
		populateCstics(source, target);
	}


	protected void populateCoreAttributes(final CPSItem source, final InstanceModel target)
	{
		target.setId(source.getId());
		target.setPosition(source.getBomPosition());
		target.setName(source.getKey());
		target.setComplete(source.isComplete());
		target.setConsistent(source.isConsistent());
	}


	protected void populateSubItems(final CPSItem source, final InstanceModel target)
	{
		if (source.getSubItems() != null)
		{
			for (final CPSItem subitem : source.getSubItems())
			{
				subitem.setParentConfiguration(source.getParentConfiguration());
				final InstanceModel subInstance = instanceModelConverter.convert(subitem);
				target.getSubInstances().add(subInstance);
			}
		}
	}

	protected void populateGroups(final CPSItem source, final InstanceModel target)
	{
		for (final CPSCharacteristicGroup characteristicGroup : source.getCharacteristicGroups())
		{
			final String groupId = characteristicGroup.getId();

			// ignore characteristic groups with non-existing id
			if (groupId == null)
			{
				return;
			}

			characteristicGroup.setParentItem(source);
			final CsticGroupModel characteristicGroupModel = characteristicGroupConverter.convert(characteristicGroup);

			if (groupId.equalsIgnoreCase(SapproductconfigruntimecpsConstants.CPS_GENERAL_GROUP_ID))
			{
				target.getCsticGroups().add(0, characteristicGroupModel);
			}
			else
			{
				target.getCsticGroups().add(characteristicGroupModel);
			}
		}
	}

	protected void populateCstics(final CPSItem source, final InstanceModel target)
	{
		if (source.getCharacteristics() != null)
		{
			for (final CPSCharacteristic characteristic : source.getCharacteristics())
			{
				characteristic.setParentItem(source);
				final CsticModel characteristicModel = characteristicConverter.convert(characteristic);
				target.addCstic(characteristicModel);
			}
		}
	}

}
