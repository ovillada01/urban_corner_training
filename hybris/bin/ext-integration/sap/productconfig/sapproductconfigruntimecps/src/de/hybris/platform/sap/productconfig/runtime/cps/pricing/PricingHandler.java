/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.pricing;

import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.interf.model.CsticModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceSummaryModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.PriceValueUpdateModel;


/**
 * Obtains and exposes pricing information from CPS
 */
public interface PricingHandler
{
	/**
	 * Retrieves the price summary
	 *
	 * @param configId
	 *           id of the runtime configuration
	 *
	 * @return price summary
	 */
	PriceSummaryModel getPriceSummary(String configId);

	/**
	 * Creates the pricing input document for the given configuration
	 *
	 * @param configuration
	 */
	void preparePricingDocumentInput(CPSConfiguration configuration);

	/**
	 * Attaches delta prices to the price value update model
	 *
	 * @param kbId
	 *           knowledgebase id
	 *
	 * @param updateModel
	 *           represents a characteristic to be updated with delta prices for every possible value
	 */
	void fillDeltaPrices(final String kbId, final PriceValueUpdateModel updateModel);


	/**
	 * Attaches value prices to the cstic model
	 *
	 * @param kbId
	 *           knowledgebase id
	 * @param cstic
	 *           cstic model
	 */
	void fillValuePrices(final String kbId, final CsticModel cstic);

}
