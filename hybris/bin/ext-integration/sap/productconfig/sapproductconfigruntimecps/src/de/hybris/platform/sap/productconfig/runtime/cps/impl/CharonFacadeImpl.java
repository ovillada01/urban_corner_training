/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.impl;

import de.hybris.platform.sap.productconfig.runtime.cps.CPSContextSupplier;
import de.hybris.platform.sap.productconfig.runtime.cps.CharonFacade;
import de.hybris.platform.sap.productconfig.runtime.cps.RequestErrorHandler;
import de.hybris.platform.sap.productconfig.runtime.cps.cache.CPSSessionCache;
import de.hybris.platform.sap.productconfig.runtime.cps.client.ConfigurationClient;
import de.hybris.platform.sap.productconfig.runtime.cps.client.ConfigurationClientBase;
import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristic;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCharacteristicInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSCreateConfigInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSItem;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSValue;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.CPSValueInput;
import de.hybris.platform.sap.productconfig.runtime.cps.model.runtime.common.CPSContextInfo;
import de.hybris.platform.sap.productconfig.runtime.cps.session.CookieHandler;
import de.hybris.platform.sap.productconfig.runtime.interf.ConfigurationEngineException;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.external.Configuration;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.yaasconfiguration.service.YaasServiceFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.util.URLEncoder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybris.charon.RawResponse;
import com.hybris.charon.exp.HttpException;

import rx.Observable;
import rx.Scheduler;
import rx.schedulers.Schedulers;


/**
 * Default implementation of {@link CharonFacade}. This bean has prototype scope
 */
public class CharonFacadeImpl implements CharonFacade
{
	private static final Logger LOG = Logger.getLogger(CharonFacadeImpl.class);

	private static final String UTF_8 = "UTF-8";
	private ConfigurationClientBase clientSetExternally = null;
	private CookieHandler cookieHandler;
	private RequestErrorHandler requestErrorHandler;
	private ObjectMapper objectMapper;
	private final Scheduler scheduler = Schedulers.io();
	private YaasServiceFactory yaasServiceFactory;
	private Converter<Configuration, CPSExternalConfiguration> externalConfigConverter;
	private CPSContextSupplier contextSupplier;

	private CPSSessionCache sessionCache;

	/**
	 * @return the sessionCache
	 */
	protected CPSSessionCache getSessionCache()
	{
		return sessionCache;
	}

	@Override
	public CPSConfiguration createDefaultConfiguration(final KBKey kbKey)
	{
		try
		{
			final CPSCreateConfigInput cloudEngineConfigurationRequest = assembleCreateDefaultConfigurationRequest(kbKey);

			if (LOG.isDebugEnabled())
			{
				traceJsonRequestBody("Input for REST call (create default configuration): ", cloudEngineConfigurationRequest);
			}
			final Observable<RawResponse<CPSConfiguration>> rawResponse = getClient()
					.createDefaultConfiguration(cloudEngineConfigurationRequest);

			return retrieveConfigurationAndSaveCookies(rawResponse);
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processCreateDefaultConfigurationError(ex);
		}
	}

	protected void traceJsonRequestBody(final String prefix, final Object obj)
	{
		try
		{
			LOG.debug(prefix + getObjectMapper().writeValueAsString(obj));
		}
		catch (final JsonProcessingException e)
		{
			LOG.warn("Could not trace " + prefix, e);
		}
	}



	protected CPSConfiguration retrieveConfigurationAndSaveCookies(final Observable<RawResponse<CPSConfiguration>> rawResponse)
	{
		//Stateful calls are required and this is facilitated via cookies. These cookies can be extracted from the RawResponse.
		final RawResponse<CPSConfiguration> response = rawResponse.subscribeOn(getScheduler()).toBlocking().first();
		final CPSConfiguration responseValue = response.content().subscribeOn(getScheduler()).toBlocking().first();
		getCookieHandler().setCookies(responseValue.getId(), response.getSetCookies());
		if (LOG.isDebugEnabled())
		{
			traceJsonRequestBody("Output for REST call (create default/from external configuration): ", responseValue);
		}
		return responseValue;
	}

	protected CPSCreateConfigInput assembleCreateDefaultConfigurationRequest(final KBKey kbKey)
	{
		final CPSCreateConfigInput cloudEngineConfigurationRequest = new CPSCreateConfigInput();
		cloudEngineConfigurationRequest.setProductKey(kbKey.getProductCode());

		final List<CPSContextInfo> context = getContextSupplier().retrieveContext(kbKey.getProductCode());
		cloudEngineConfigurationRequest.setContext(context);

		return cloudEngineConfigurationRequest;
	}

	protected CookieHandler getCookieHandler()
	{
		return cookieHandler;
	}

	/**
	 * @param cookieHandler
	 *           the cookieHandler to set
	 */
	@Required
	public void setCookieHandler(final CookieHandler cookieHandler)
	{
		this.cookieHandler = cookieHandler;
	}


	@Override
	public void updateConfiguration(final String cfgId, final String itemId, final String csticId,
			final CPSCharacteristicInput changes) throws ConfigurationEngineException
	{
		final List<String> cookiesAsString = getCookies(cfgId);

		try
		{
			final String csticIdEncoded = encode(csticId);
			if (LOG.isDebugEnabled())
			{
				traceJsonRequestBody("Input for REST call (update configuration): ", changes);
			}

			getClient().updateConfiguration(changes, cfgId, itemId, csticIdEncoded, cookiesAsString.get(0), cookiesAsString.get(1))
					.subscribeOn(getScheduler()).toBlocking().first();
		}
		catch (final HttpException e)
		{
			getRequestErrorHandler().processUpdateConfigurationError(e);
		}
	}



	protected String encode(final String requestParam)
	{
		if (requestParam == null)
		{
			return null;
		}
		final URLEncoder encoder = new URLEncoder();

		return encoder.encode(requestParam, UTF_8);
	}

	protected List<String> getCookies(final String cfgId)
	{
		final List<String> cookiesAsString = getCookieHandler().getCookiesAsString(cfgId);

		//We expect 2 cookies thus we need to check the size of the list that we got
		if (cookiesAsString.size() != 2)
		{
			throw new IllegalStateException("We expect exactly 2 cookies");
		}
		return cookiesAsString;
	}

	@Override
	public CPSConfiguration getConfiguration(final String configId) throws ConfigurationEngineException
	{
		try
		{
			final List<String> cookiesAsString = getCookies(configId);
			final CPSConfiguration config = getClient().getConfiguration(configId, cookiesAsString.get(0), cookiesAsString.get(1))
					.subscribeOn(getScheduler()).toBlocking().first();
			if (LOG.isDebugEnabled())
			{
				traceJsonRequestBody("Output for REST call (get configuration): ", config);
			}
			return config;
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processGetConfigurationError(ex);
		}
	}


	@Override
	public boolean updateConfiguration(final CPSConfiguration configuration) throws ConfigurationEngineException
	{
		boolean updateWasPerformed = false;
		final CPSItem rootItem = configuration.getRootItem();
		if (rootItem == null)
		{
			throw new IllegalStateException("Root item not available");
		}

		final String cfgId = configuration.getId();
		final String itemId = rootItem.getId();

		final List<CPSCharacteristic> characteristics = rootItem.getCharacteristics();

		for (final CPSCharacteristic characteristic : characteristics)
		{
			final CPSCharacteristicInput characteristicInput = createCharacteristicInput(characteristic);
			updateConfiguration(cfgId, itemId, characteristic.getId(), characteristicInput);
			updateWasPerformed = true;
		}
		return updateWasPerformed;
	}


	protected CPSCharacteristicInput createCharacteristicInput(final CPSCharacteristic characteristic)
	{
		final CPSCharacteristicInput characteristicInput = new CPSCharacteristicInput();
		characteristicInput.setValues(new ArrayList<>());
		for (final CPSValue value : characteristic.getValues())
		{
			final CPSValueInput valueInput = new CPSValueInput();
			valueInput.setValue(value.getValue());
			valueInput.setSelected(value.isSelected());
			characteristicInput.getValues().add(valueInput);
		}
		return characteristicInput;
	}


	protected RequestErrorHandler getRequestErrorHandler()
	{
		return requestErrorHandler;
	}


	/**
	 * @param requestErrorHandler
	 *           For wrapping the http errors we receive from the REST service call
	 */
	@Required
	public void setRequestErrorHandler(final RequestErrorHandler requestErrorHandler)
	{
		this.requestErrorHandler = requestErrorHandler;
	}


	protected ConfigurationClientBase getClient()
	{
		if (clientSetExternally != null)
		{
			return clientSetExternally;
		}
		else
		{
			return yaasServiceFactory.lookupService(ConfigurationClient.class);
		}
	}


	/**
	 * @param clientSetExternally
	 *           Charon client representing REST calls for product configuration
	 */
	public void setClient(final ConfigurationClientBase newClient)
	{
		clientSetExternally = newClient;
	}


	@Override
	public String getExternalConfiguration(final String configId)
	{
		try
		{
			final CPSExternalConfiguration externalConfigStructured = placeGetExternalConfigurationRequest(configId);

			final String extConfig = getObjectMapper().writeValueAsString(externalConfigStructured);
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Output for REST call (get ext configuration): " + extConfig);
			}
			return extConfig;

		}
		catch (final JsonProcessingException e)
		{
			throw new IllegalStateException("External configuration from client cannot be parsed to string", e);
		}
	}

	protected CPSExternalConfiguration placeGetExternalConfigurationRequest(final String configId)
	{
		try
		{
			final List<String> cookiesAsString = getCookies(configId);
			return getClient().getExternalConfiguration(configId, cookiesAsString.get(0), cookiesAsString.get(1))
					.subscribeOn(getScheduler()).toBlocking().first();
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processGetExternalConfigurationError(ex);
		}
	}


	@Override
	public CPSConfiguration createConfigurationFromExternal(final String externalConfiguration)
	{

		final CPSExternalConfiguration externalConfigStructured = convertFromStringToStructured(externalConfiguration);

		final String productCode = externalConfigStructured.getRootItem().getObjectKey().getId();
		final List<CPSContextInfo> context = getContextSupplier().retrieveContext(productCode);
		externalConfigStructured.setContext(context);

		return createConfigurationFromExternal(externalConfigStructured);
	}


	private CPSConfiguration createConfigurationFromExternal(final CPSExternalConfiguration externalConfigStructured)
	{
		try
		{
			if (LOG.isDebugEnabled())
			{
				traceJsonRequestBody("Input for REST call (create form external configuration): ", externalConfigStructured);
			}
			final Observable<RawResponse<CPSConfiguration>> rawResponse = getClient()
					.createRuntimeConfigurationFromExternal(externalConfigStructured);

			return retrieveConfigurationAndSaveCookies(rawResponse);
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processCreateRuntimeConfigurationFromExternalError(ex);
		}
	}

	protected CPSExternalConfiguration convertFromStringToStructured(final String externalConfiguration)
	{
		CPSExternalConfiguration externalConfigStructured;
		try
		{
			externalConfigStructured = getObjectMapper().readValue(externalConfiguration, CPSExternalConfiguration.class);
		}
		catch (final IOException e)
		{
			throw new IllegalStateException("Parsing from JSON failed", e);

		}
		return externalConfigStructured;
	}

	protected ObjectMapper getObjectMapper()
	{
		if (objectMapper == null)
		{
			objectMapper = new ObjectMapper();
		}
		return objectMapper;
	}

	protected void setObjectMapper(final ObjectMapper objectMapper)
	{
		this.objectMapper = objectMapper;
	}


	@Override
	public void releaseSession(final String configId)
	{
		final List<String> cookiesAsString = getCookies(configId);
		try
		{
			getClient().deleteConfiguration(configId, cookiesAsString.get(0), cookiesAsString.get(1));
		}
		catch (final HttpException ex)
		{
			getRequestErrorHandler().processDeleteConfigurationError(ex);
		}
		finally
		{
			getCookieHandler().removeCookies(configId);
			//do not remove pricing document input as it is needed in order entry overview case
			getSessionCache().removePricingDocumentResult(configId);
		}
	}


	protected Scheduler getScheduler()
	{
		return scheduler;
	}

	protected YaasServiceFactory getYaasServiceFactory()
	{
		return yaasServiceFactory;
	}

	/**
	 * @param yaasServiceFactory
	 *           the YaasServiceFactory to set
	 */
	@Required
	public void setYaasServiceFactory(final YaasServiceFactory yaasServiceFactory)
	{
		this.yaasServiceFactory = yaasServiceFactory;
	}

	/**
	 * @return the externalConfigConverter
	 */
	protected Converter<Configuration, CPSExternalConfiguration> getExternalConfigConverter()
	{
		return externalConfigConverter;
	}

	/**
	 * @param externalConfigConverter
	 *           the externalConfigConverter to set
	 */
	@Required
	public void setExternalConfigConverter(final Converter<Configuration, CPSExternalConfiguration> externalConfigConverter)
	{
		this.externalConfigConverter = externalConfigConverter;
	}

	/**
	 * @return the contextSupplier
	 */
	protected CPSContextSupplier getContextSupplier()
	{
		return contextSupplier;
	}

	/**
	 * @param contextSupplier
	 *           the contextSupplier to set
	 */
	public void setContextSupplier(final CPSContextSupplier contextSupplier)
	{
		this.contextSupplier = contextSupplier;
	}

	@Override
	public CPSConfiguration createConfigurationFromExternal(final Configuration externalConfiguration, final Integer kbid)
	{
		final CPSExternalConfiguration externalConfigStructured = getExternalConfigConverter().convert(externalConfiguration);
		externalConfigStructured.setKbId(String.valueOf(kbid));

		return createConfigurationFromExternal(externalConfigStructured);

	}

	/**
	 * @param cpsSessionCache
	 */
	public void setSessionCache(final CPSSessionCache cpsSessionCache)
	{
		this.sessionCache = cpsSessionCache;

	}


}
