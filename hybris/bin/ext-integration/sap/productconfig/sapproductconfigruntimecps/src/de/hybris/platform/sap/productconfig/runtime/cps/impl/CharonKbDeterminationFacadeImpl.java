/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.cps.impl;

import de.hybris.platform.sap.productconfig.runtime.cps.CharonKbDeterminationFacade;
import de.hybris.platform.sap.productconfig.runtime.cps.RequestErrorHandler;
import de.hybris.platform.sap.productconfig.runtime.cps.client.KbDeterminationClient;
import de.hybris.platform.sap.productconfig.runtime.cps.client.KbDeterminationClientBase;
import de.hybris.platform.sap.productconfig.runtime.cps.model.external.CPSExternalConfiguration;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.common.CPSMasterDataKBHeaderInfo;
import de.hybris.platform.sap.productconfig.runtime.cps.model.masterdata.common.CPSMasterDataKnowledgebaseKey;
import de.hybris.platform.yaasconfiguration.service.YaasServiceFactory;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Required;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybris.charon.exp.HttpException;

import rx.Scheduler;
import rx.schedulers.Schedulers;


/**
 * Default implementation of {@link CharonKbDeterminationFacade}
 */
public class CharonKbDeterminationFacadeImpl implements CharonKbDeterminationFacade
{

	private KbDeterminationClientBase clientSetExternally = null;
	private ObjectMapper objectMapper;
	private RequestErrorHandler requestErrorHandler;
	private final Scheduler scheduler = Schedulers.io();
	private YaasServiceFactory yaasServiceFactory;

	/**
	 * @return the objectMapper
	 */
	protected ObjectMapper getObjectMapper()
	{
		if (objectMapper == null)
		{
			objectMapper = new ObjectMapper();
		}
		return objectMapper;
	}

	protected boolean resultAvailable(final List<CPSMasterDataKBHeaderInfo> knowledgebases)
	{
		return knowledgebases != null && !knowledgebases.isEmpty();
	}

	protected boolean resultAvailable(final List<CPSMasterDataKBHeaderInfo> knowledgebases,
			final CPSExternalConfiguration externalConfigStructured)
	{
		return knowledgebases.stream().anyMatch(kb -> isMatch(kb, externalConfigStructured));
	}

	protected boolean isMatch(final CPSMasterDataKBHeaderInfo kb, final CPSExternalConfiguration externalConfigStructured)
	{
		final CPSMasterDataKnowledgebaseKey cpsKbKey = kb.getKey();
		final CPSMasterDataKnowledgebaseKey kbKey = externalConfigStructured.getKbKey();
		if (cpsKbKey == null || kbKey == null)
		{
			return false;
		}
		return kbKey.getLogsys().equals(cpsKbKey.getLogsys()) && kbKey.getName().equals(cpsKbKey.getName())
				&& kbKey.getVersion().equals(cpsKbKey.getVersion());
	}

	protected String convertToString(final Date kbdate)
	{
		return Instant.ofEpochMilli(kbdate.getTime()).toString().substring(0, 10);
	}

	protected KbDeterminationClientBase getClient()
	{
		if (clientSetExternally != null)
		{
			return clientSetExternally;
		}
		else
		{
			return yaasServiceFactory.lookupService(KbDeterminationClient.class);
		}
	}

	protected void setClient(final KbDeterminationClientBase newClient)
	{
		clientSetExternally = newClient;
	}

	@Override
	public boolean hasKbForDate(final String productcode, final Date kbdate)
	{
		try
		{
			final List<CPSMasterDataKBHeaderInfo> knowledgebases = readKbList(productcode, kbdate);
			return resultAvailable(knowledgebases);
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processHasKbError(ex);
		}
	}



	@Override
	public Integer readKbIdForDate(final String productcode, final Date kbdate)
	{
		try
		{
			final List<CPSMasterDataKBHeaderInfo> knowledgebases = readKbList(productcode, kbdate);
			return resultIdAvailable(productcode, kbdate, knowledgebases);
		}
		catch (final HttpException ex)
		{
			getRequestErrorHandler().processHasKbError(ex);
			return null;
		}
	}

	protected Integer resultIdAvailable(final String productcode, final Date kbdate,
			final List<CPSMasterDataKBHeaderInfo> knowledgebases)
	{
		if (knowledgebases.isEmpty())
		{
			throw new IllegalStateException("No KB found for product and date: " + productcode + " / " + kbdate);
		}
		return knowledgebases.get(0).getId();
	}

	protected List<CPSMasterDataKBHeaderInfo> readKbList(final String productcode, final Date kbdate)
	{
		final String date = convertToString(kbdate);
		return getClient().getKnowledgebases(productcode, date).subscribeOn(getScheduler()).toBlocking().first();
	}

	@Override
	public boolean hasKbForExtConfig(final String product, final String externalcfg)
	{
		try
		{
			final CPSExternalConfiguration externalConfigStructured = parseFromJSON(externalcfg);
			final List<CPSMasterDataKBHeaderInfo> knowledgebases = getClient().getKnowledgebases(product).subscribeOn(getScheduler())
					.toBlocking().first();
			return resultAvailable(knowledgebases, externalConfigStructured);
		}
		catch (final HttpException ex)
		{
			return getRequestErrorHandler().processHasKbError(ex);
		}
	}

	protected CPSExternalConfiguration parseFromJSON(final String externalcfg)
	{
		CPSExternalConfiguration externalConfigStructured;
		try
		{
			externalConfigStructured = getObjectMapper().readValue(externalcfg, CPSExternalConfiguration.class);
		}
		catch (final IOException e)
		{
			throw new IllegalStateException("Parsing from JSON failed", e);
		}
		return externalConfigStructured;
	}

	protected Scheduler getScheduler()
	{
		return scheduler;
	}

	protected RequestErrorHandler getRequestErrorHandler()
	{
		return requestErrorHandler;
	}

	/**
	 * @param requestErrorHandler
	 *           For wrapping the http errors we receive from the REST service call
	 */
	public void setRequestErrorHandler(final RequestErrorHandler requestErrorHandler)
	{
		this.requestErrorHandler = requestErrorHandler;
	}

	protected YaasServiceFactory getYaasServiceFactory()
	{
		return yaasServiceFactory;
	}

	/**
	 * @param yaasServiceFactory
	 *           the YaasServiceFactory to set
	 */
	@Required
	public void setYaasServiceFactory(final YaasServiceFactory yaasServiceFactory)
	{
		this.yaasServiceFactory = yaasServiceFactory;
	}

}
