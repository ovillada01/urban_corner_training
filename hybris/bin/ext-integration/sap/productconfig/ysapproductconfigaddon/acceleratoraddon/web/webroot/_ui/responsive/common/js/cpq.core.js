/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
















var CPQ = CPQ || {};

CPQ.core = {
	pageType : "config",
	baseUrl : "",
	ajaxRunning : false,
	ajaxRunCounter : 0,
	ajaxRunCounterAsyncServices: 0,
	configState: 0,
	formNameId : "#configform",
	waitingMsgTriggeredHandle : "",
	successMsgHideTriggeredHandle : "",

	firePost : function(fx, args) {
		CPQ.core.ajaxRunCounter++;
		if (CPQ.core.ajaxRunCounter === 1) {
			CPQ.core.waitingMsgTriggeredHandle = setTimeout(function() {
				CPQ.core.showWaitingMessage();
			}, 500);
		}
		CPQ.core.waitToFirePost(fx, args);
	},

	waitToFirePost : function(fx, args) {
		if (CPQ.core.ajaxRunning === true) {
			setTimeout(function() {
				CPQ.core.waitToFirePost.call(this, fx, args);
			}, 100);
		} else {
			CPQ.core.ajaxRunning = true;
			fx.apply(this, args);
		}
	},

	showWaitingMessage : function() {
		if($(".cpq-engine-state-success").is(":visible")){
			clearTimeout(CPQ.core.successMsgHideTriggeredHandle);
			$(".cpq-engine-state-success").slideUp(300,function() {
				if(CPQ.core.ajaxRunning === true)
					$(".cpq-engine-state-running").slideDown(300);
				});		
		}else{
			$(".cpq-engine-state-running").slideDown(300);
		}
	},

	lastAjaxDone : function() {
		clearTimeout(CPQ.core.waitingMsgTriggeredHandle);
		if ($(".cpq-engine-state-running").is(":visible")) {
			$(".cpq-engine-state-running").slideUp(300,function() {
				$(".cpq-engine-state-success").slideDown();
				CPQ.core.successMsgHideTriggeredHandle = setTimeout(function() {
					$(".cpq-engine-state-success").slideUp(300);
				}, 5000);
			});
		}
	},

	// To use any of the meta-characters ( such as
	// !"#$%&'()*+,./:;<=>?@[\]^`{|}~ ) as a literal part of a name, it must be
	// escaped with with two backslashes
	// (https://api.jquery.com/category/selectors/)
	// can be replaced by jQuery.escapeSelector when upgrading to jQuery3
	// (https://api.jquery.com/jQuery.escapeSelector/)
	// Note in the expression below every character is escaped, although this
	// would only be necessary for characters, that have a special meaning in a
	// regex. However escaping all is no harm and more robust.
	// regex: /(char2|char2|char3|...)/g
	encodeId : function(id) {
		var encodedId = "#"
				+ id
						.replace(
								/(\!|\"|\#|\$|\%|\&|\'|\(|\)|\*|\+|\,|\.|\/|\:|\;|\<|\=|\>|\?|\@|\[|\\|\]|\^|\`|\{|\||\}|\~)/g,
								"\\\$1");
		return encodedId;
	},

	getPageUrl : function() {
		return this.baseUrl + "/" + this.pageType;
	},

	getVaraiantSearchUrl : function() {
		return this.baseUrl + "/searchConfigVariant";
	},

	getResetUrl : function() {
		return "reset";
	},

	getAddToCartUrl : function() {
		return "addToCart";
	},

	getAddVariantToCartCleanUpUrl : function() {
		return "addVariantToCartCleanUp";
	},

	getOverviewUrl : function() {
		if (CPQ.core.pageType === "variantOverview") {
			return "variantOverview";
		}
		return "configOverview";
	},
	
	getPricingUrl : function() {
		return this.baseUrl + "/updatePricing";
	},
	
	getAnalyticsUrl : function() {
		return this.baseUrl + "/updateAnalytics";
	},

	actionAndRedirect : function(e, action, url) {
		var input = $('input[name=CSRFToken]');
		var token = null;
		var form = null;

		if (input && input.length !== 0) {
			token = input.attr("value");
		}
		if (token) {
			form = $('<form action="' + action
					+ '" method="post" style="display: none;">'
					+ '<input type="text" name="url" value="' + url + '" />'
					+ '<input type="hidden" name="CSRFToken" value="' + token
					+ '" />' + '</form>');
		} else {
			form = $('<form action="' + action
					+ '" method="post" style="display: none;">'
					+ '<input type="text" name="url" value="' + url + '" />'
					+ '" />' + '</form>');
		}

		$('body').append(form);
		$(form).submit();
	}
};
