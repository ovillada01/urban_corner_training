<%@ tag language="java" pageEncoding ="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="config" tagdir="/WEB-INF/tags/addons/ysapproductconfigaddon/responsive/configuration"%>

<%@ attribute name="csticValueKey" required="true" type="java.lang.String"%>
<%@ attribute name="shortMessage" required="flase" type="java.lang.String"%>

<c:if test="${config.analyticsEnabled}">
	<%-- Analytics data fetched asynchrounsly, hence this tag just acts as a place holder without any value --%>
	<div id="${csticValueKey}.analytics" class="cpq-csticValueAnalyticsTemplate">
		<c:choose>
		<c:when test="${shortMessage}">
			<spring:theme code="sapproductconfig.analytics.popularityInPercent.short" text="{0}% chose this!" arguments="XX" />
		</c:when>
		<c:otherwise>
			<spring:theme code="sapproductconfig.analytics.popularityInPercent" text="{0}% of customers chose this option!" arguments="XX" />
		</c:otherwise>
	</c:choose>
	</div>
</c:if>