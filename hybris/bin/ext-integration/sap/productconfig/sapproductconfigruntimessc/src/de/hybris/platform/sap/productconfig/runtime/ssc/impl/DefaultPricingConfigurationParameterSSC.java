/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.ssc.impl;

import de.hybris.platform.sap.productconfig.runtime.interf.impl.DefaultPricingConfigurationParameter;
import de.hybris.platform.sap.productconfig.runtime.ssc.PricingConfigurationParameterSSC;
import de.hybris.platform.sap.productconfig.runtime.ssc.constants.SapproductconfigruntimesscConstants;


/**
 * Default implementation of {@link PricingConfigurationParameterSSC}
 */
public class DefaultPricingConfigurationParameterSSC extends DefaultPricingConfigurationParameter
		implements PricingConfigurationParameterSSC
{

	@Override
	public String getTargetForBasePrice()
	{
		String targetBasePrice = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess()
					.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_CONDITION_FUNCTION_BASE_PRICE);

			if (propertyValue instanceof String)
			{
				targetBasePrice = (String) propertyValue;
			}
		}

		return targetBasePrice;
	}

	@Override
	public String getTargetForSelectedOptions()
	{
		String targetSelectedOptions = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess()
					.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_CONDITION_FUNCTION_SELECTED_OPTIONS);

			if (propertyValue instanceof String)
			{
				targetSelectedOptions = (String) propertyValue;
			}
		}

		return targetSelectedOptions;
	}

	@Override
	public String getPricingProcedure()
	{
		String pricingProcedure = null;
		if (getModuleConfigurationAccess() != null)
		{
			final Object propertyValue = getModuleConfigurationAccess()
					.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_PRICING_PROCEDURE);

			if (propertyValue instanceof String)
			{
				pricingProcedure = (String) propertyValue;
			}
		}

		return pricingProcedure;
	}
}
