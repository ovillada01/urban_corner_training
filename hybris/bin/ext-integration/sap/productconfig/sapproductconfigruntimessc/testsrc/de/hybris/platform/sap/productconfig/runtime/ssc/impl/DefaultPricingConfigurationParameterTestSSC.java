/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.runtime.ssc.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.sap.core.module.ModuleConfigurationAccess;
import de.hybris.platform.sap.productconfig.runtime.ssc.constants.SapproductconfigruntimesscConstants;
import de.hybris.platform.sap.sapmodel.services.SalesAreaService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


@UnitTest
public class DefaultPricingConfigurationParameterTestSSC
{
	private static final String PRICING_PROCEDURE_EXAMPLE = "PRICING_PROCEDURE";
	private static final String BASE_PRICE_EXAMPLE = "BASE_PRICE";
	private static final String SELECTED_OPTIONS_EXAMPLE = "SELECTED_OPTION";

	private DefaultPricingConfigurationParameterSSC pricingParameterSSC;

	@Mock
	protected ModuleConfigurationAccess moduleConfigurationAccess;

	@Mock
	protected SalesAreaService commonSalesAreaService;

	@Before
	public void setUp() throws Exception
	{
		MockitoAnnotations.initMocks(this);

		pricingParameterSSC = new DefaultPricingConfigurationParameterSSC();
		pricingParameterSSC.setModuleConfigurationAccess(moduleConfigurationAccess);
		pricingParameterSSC.setCommonSalesAreaService(commonSalesAreaService);
	}

	@Test
	public void testGetPricingProcedure()
	{
		when(moduleConfigurationAccess.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_PRICING_PROCEDURE))
				.thenReturn(PRICING_PROCEDURE_EXAMPLE);

		final String priceProcedure = pricingParameterSSC.getPricingProcedure();
		assertEquals(PRICING_PROCEDURE_EXAMPLE, priceProcedure);
	}

	@Test
	public void testGetCondFuncForBasePrice()
	{
		when(moduleConfigurationAccess.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_CONDITION_FUNCTION_BASE_PRICE))
				.thenReturn(BASE_PRICE_EXAMPLE);

		final String condFuncBasePrice = pricingParameterSSC.getTargetForBasePrice();
		assertEquals(BASE_PRICE_EXAMPLE, condFuncBasePrice);
	}

	@Test
	public void testGetCondFuncForSelectedOptions()
	{
		when(moduleConfigurationAccess
				.getProperty(SapproductconfigruntimesscConstants.CONFIGURATION_CONDITION_FUNCTION_SELECTED_OPTIONS))
						.thenReturn(SELECTED_OPTIONS_EXAMPLE);

		final String condFuncSelectedOptions = pricingParameterSSC.getTargetForSelectedOptions();
		assertEquals(SELECTED_OPTIONS_EXAMPLE, condFuncSelectedOptions);
	}

	@Test
	public void testGetPricingProcedureValueNotDefined()
	{
		final String priceProcedure = pricingParameterSSC.getPricingProcedure();
		assertNull(priceProcedure);
	}

	@Test
	public void testGetCondFuncForBasePriceValueNotDefined()
	{
		final String condFuncBasePrice = pricingParameterSSC.getTargetForBasePrice();
		assertNull(condFuncBasePrice);
	}

	@Test
	public void testGetCondFuncForSelectedOptionsValueNotDefined()
	{
		final String condFuncSelectedOptions = pricingParameterSSC.getTargetForSelectedOptions();
		assertNull(condFuncSelectedOptions);
	}

}
