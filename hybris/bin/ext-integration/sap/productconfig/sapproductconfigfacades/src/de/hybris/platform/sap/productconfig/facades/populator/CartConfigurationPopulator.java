/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.facades.populator;

import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.sap.productconfig.services.SessionAccessService;
import de.hybris.platform.sap.productconfig.services.intf.ProductConfigurationService;

import org.apache.log4j.Logger;


/**
 * Takes care of populating product configuration relevant attributes
 */
public class CartConfigurationPopulator extends AbstractOrderConfigurationPopulator implements Populator<CartModel, CartData>
{

	private static final Logger LOG = Logger.getLogger(CartConfigurationPopulator.class);
	private SessionAccessService sessionAccessService;
	private ProductConfigurationService productConfigurationService;

	/**
	 * @param sessionAccessService
	 *           the sessionAccessService to set
	 */
	public void setSessionAccessService(final SessionAccessService sessionAccessService)
	{
		this.sessionAccessService = sessionAccessService;
	}

	/**
	 * @return sessionAccessService
	 */
	public SessionAccessService getSessionAccessService()
	{
		return this.sessionAccessService;
	}

	protected ProductConfigurationService getProductConfigurationService()
	{
		return productConfigurationService;
	}

	/**
	 * @param productConfigurationService
	 */
	public void setProductConfigurationService(final ProductConfigurationService productConfigurationService)
	{
		this.productConfigurationService = productConfigurationService;
	}


	@Override
	public void populate(final CartModel source, final CartData target)
	{
		long startTime = 0;
		if (LOG.isDebugEnabled())
		{
			startTime = System.currentTimeMillis();
		}

		for (final AbstractOrderEntryModel entry : source.getEntries())
		{
			populateCartEntry(entry, target);
		}

		if (LOG.isDebugEnabled())
		{
			final long duration = System.currentTimeMillis() - startTime;
			LOG.debug("CPQ Populating for cart took " + duration + " ms");
		}
	}

	/**
	 * Transfers configuration related attributes from order entry into its DTO representation
	 *
	 * @param entry
	 *           Cart entry model
	 * @param target
	 *           Cart DTO, used to get the cart entry DTO via searching for key
	 */
	protected void populateCartEntry(final AbstractOrderEntryModel entry, final CartData target)
	{
		final Boolean isConfigurable = entry.getProduct().getSapConfigurable();
		if (isConfigurable != null && isConfigurable.booleanValue())
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("CartItem with PK " + entry.getPk() + " is Configurable ==> populating DTO.");
			}
			getProductConfigurationService().ensureConfigurationInSession(entry.getPk().toString(), entry.getProduct().getCode(),
					entry.getExternalConfiguration());
			checkForExternalConfiguration(entry);
			writeToTargetEntry(entry, target);
		}
		else
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("CartItem with PK " + entry.getPk() + " is NOT Configurable ==> skipping population of DTO.");
			}
		}
	}

	/**
	 * Writes external configuration to cart entry if it is not present yet
	 *
	 * @param entry
	 *           AbstractOrder entry
	 */
	protected void checkForExternalConfiguration(final AbstractOrderEntryModel entry)
	{
		final String xml = entry.getExternalConfiguration();
		if (xml == null || xml.isEmpty())
		{
			final String configId = getSessionAccessService().getConfigIdForCartEntry(entry.getPk().toString());
			entry.setExternalConfiguration(getProductConfigurationService().retrieveExternalConfiguration(configId));
		}
	}
}
