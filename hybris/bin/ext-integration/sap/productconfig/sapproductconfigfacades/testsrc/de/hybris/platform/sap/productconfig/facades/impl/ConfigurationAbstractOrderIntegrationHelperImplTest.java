/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.productconfig.facades.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.sap.productconfig.facades.overview.ConfigurationOverviewData;
import de.hybris.platform.sap.productconfig.facades.populator.VariantOverviewPopulator;
import de.hybris.platform.sap.productconfig.runtime.interf.KBKey;
import de.hybris.platform.sap.productconfig.runtime.interf.impl.KBKeyImpl;
import de.hybris.platform.sap.productconfig.runtime.interf.model.ConfigModel;
import de.hybris.platform.sap.productconfig.runtime.interf.model.impl.ConfigModelImpl;
import de.hybris.platform.sap.productconfig.services.intf.ProductConfigurationService;
import de.hybris.platform.variants.model.VariantProductModel;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


@UnitTest
public class ConfigurationAbstractOrderIntegrationHelperImplTest
{
	private static final int NON_EXISTING_ENTRY_NUMBER = 1;
	private static final int ENTRY_NUMBER = 3;
	private static final String CONFIG_ID = "configId";
	private static final String PRODUCT_CODE = "productCode";
	private String extConfig;
	private KBKey kbKey;

	private ConfigurationAbstractOrderIntegrationHelperImpl classUnderTest;

	@Mock
	private ProductConfigurationService productConfigurationService;
	@Mock
	private VariantOverviewPopulator variantOverviewPopulator;

	private AbstractOrderModel order;
	private List<AbstractOrderEntryModel> orderEntryList;
	private ConfigModel configModel;
	private static final String EXT_CONFIG_KB_NOT_EXISTING = "kbNotExisting";

	protected void addOrderEntry(final String configId, final int entryNumber)
	{
		configModel = new ConfigModelImpl();
		configModel.setId(configId);
		extConfig = "myExtConfigForId" + configId;
		given(productConfigurationService.createConfigurationFromExternal(Mockito.any(KBKey.class), Mockito.eq(extConfig)))
				.willReturn(configModel);

		final AbstractOrderEntryModel entry = new AbstractOrderEntryModel();
		entry.setEntryNumber(Integer.valueOf(entryNumber));
		entry.setExternalConfiguration(extConfig);
		entry.setProduct(new ProductModel());
		entry.getProduct().setSapConfigurable(Boolean.TRUE);
		entry.getProduct().setCode(PRODUCT_CODE);
		entry.setOrder(order);
		orderEntryList.add(entry);

		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(), Mockito.eq(extConfig)))).willReturn(
				Boolean.TRUE);
	}

	@Before
	public void setup()
	{
		MockitoAnnotations.initMocks(this);
		classUnderTest = new ConfigurationAbstractOrderIntegrationHelperImpl();
		classUnderTest.setProductConfigurationService(productConfigurationService);
		classUnderTest.setVariantOverviewPopulator(variantOverviewPopulator);

		order = new AbstractOrderModel();
		orderEntryList = new ArrayList<>();
		order.setEntries(orderEntryList);
		addOrderEntry(CONFIG_ID, ENTRY_NUMBER);

		kbKey = new KBKeyImpl(PRODUCT_CODE);
	}

	@Test
	public void testFindEntry()
	{
		final AbstractOrderEntryModel result = classUnderTest.findEntry(order, ENTRY_NUMBER);
		assertNotNull(result);
		assertEquals(orderEntryList.get(0), result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testFindEntryNotFound()
	{
		classUnderTest.findEntry(order, NON_EXISTING_ENTRY_NUMBER);
	}

	@Test
	public void testRetrieveConfigModelAndDiscardSession()
	{
		classUnderTest.retrieveConfigModelAndDiscardSession(extConfig, kbKey);
		Mockito.verify(productConfigurationService).createConfigurationFromExternal(kbKey, extConfig);
		Mockito.verify(productConfigurationService).releaseSession(CONFIG_ID, true);
	}

	@Test
	public void testPrepareOverviewData()
	{
		final ConfigurationOverviewData result = classUnderTest.prepareOverviewData(kbKey, configModel);
		assertNotNull(result);
		assertEquals(kbKey.getProductCode(), result.getProductCode());
		assertEquals(CONFIG_ID, result.getId());
	}

	@Test
	public void testRetrieveConfigurationOverviewData_versionExists()
	{
		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(KBKey.class), Mockito.eq(extConfig))))
				.willReturn(Boolean.TRUE);
		final ConfigurationOverviewData result = classUnderTest.retrieveConfigurationOverviewData(order, ENTRY_NUMBER);
		assertNotNull(result);
		assertEquals(kbKey.getProductCode(), result.getProductCode());
		assertEquals(CONFIG_ID, result.getId());
	}

	@Test
	public void testRetrieveConfigurationOverviewData_versionNotExists()
	{
		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(KBKey.class), Mockito.eq(extConfig))))
				.willReturn(Boolean.FALSE);
		final ConfigurationOverviewData result = classUnderTest.retrieveConfigurationOverviewData(order, ENTRY_NUMBER);
		assertNull(result);
	}

	@Test
	public void testValidateExternalConfig()
	{
		classUnderTest.validateExternalConfig(orderEntryList.get(0));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetAndValidateExternalConfigNull()
	{
		orderEntryList.get(0).setExternalConfiguration(null);
		classUnderTest.validateExternalConfig(orderEntryList.get(0));
	}

	@Test
	public void testGetConfigurationOverviewDataForVariant()
	{
		final VariantProductModel variantProductModel = new VariantProductModel();
		variantProductModel.setCode(PRODUCT_CODE);
		final ConfigurationOverviewData result = classUnderTest.getConfigurationOverviewDataForVariant(orderEntryList.get(0),
				variantProductModel);
		assertNotNull(result);
		assertEquals(PRODUCT_CODE, result.getProductCode());
		Mockito.verify(variantOverviewPopulator).populate(Mockito.eq(variantProductModel), Mockito.any());
	}

	@Test
	public void testRetrieveConfigurationForVariant()
	{
		orderEntryList.get(0).setExternalConfiguration(null);
		final VariantProductModel variantProductModel = new VariantProductModel();
		variantProductModel.setCode(PRODUCT_CODE);
		orderEntryList.get(0).setProduct(variantProductModel);
		final ConfigurationOverviewData result = classUnderTest.retrieveConfigurationOverviewData(order, ENTRY_NUMBER);

		assertNotNull(result);
		assertEquals(PRODUCT_CODE, result.getProductCode());
		Mockito.verify(variantOverviewPopulator).populate(Mockito.eq(variantProductModel), Mockito.any());
	}

	@Test
	public void testIsKbExistingFalse()
	{
		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(), Mockito.eq(extConfig)))).willReturn(
				Boolean.FALSE);
		assertFalse(classUnderTest.isKbVersionForEntryExisting(orderEntryList.get(0)));
	}

	@Test
	public void testIsKbExistingTrue()
	{
		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(), Mockito.eq(extConfig)))).willReturn(
				Boolean.TRUE);
		assertTrue(classUnderTest.isKbVersionForEntryExisting(orderEntryList.get(0)));
	}

	@Test
	public void testIsKbExistingForNotConfigurable()
	{

		final AbstractOrderEntryModel orderEntry = orderEntryList.get(0);
		orderEntry.getProduct().setSapConfigurable(Boolean.FALSE);
		assertTrue(classUnderTest.isKbVersionForEntryExisting(orderEntry));
	}

	@Test
	public void testIsReorderable_true()
	{
		assertTrue(classUnderTest.isReorderable(order));
	}



	@Test
	public void testIsReorderable_false()
	{
		final AbstractOrderEntryModel orderEntry = new AbstractOrderEntryModel();
		orderEntry.setProduct(new ProductModel());
		orderEntry.getProduct().setSapConfigurable(Boolean.TRUE);
		orderEntry.getProduct().setCode(PRODUCT_CODE);
		orderEntry.setExternalConfiguration(EXT_CONFIG_KB_NOT_EXISTING);
		given(Boolean.valueOf(productConfigurationService.hasKbForVersion(Mockito.any(), Mockito.eq(EXT_CONFIG_KB_NOT_EXISTING))))
				.willReturn(Boolean.FALSE);
		order.getEntries().add(orderEntry);
		assertFalse(classUnderTest.isReorderable(order));

	}
}
