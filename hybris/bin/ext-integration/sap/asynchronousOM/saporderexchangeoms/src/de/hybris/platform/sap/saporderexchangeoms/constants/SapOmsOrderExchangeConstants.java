/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.sap.saporderexchangeoms.constants;

/**
 * Global class for all SAP OMS data hub inbound constants
 */
public class SapOmsOrderExchangeConstants
{

	@SuppressWarnings("javadoc")
	public static final String CONSIGNMENT_ACTION_EVENT_NAME = "SapConsignmentActionEvent";

	@SuppressWarnings("javadoc")
	public static final String CONSIGNMENT_PROCESS_CANCELLED = "consignmentProcessCancelled";

	@SuppressWarnings("javadoc")
	public static final String CONSIGNMENT_DELIVERY_CONFIRMED = "consignmentDeliveryConfirmed";
	
	@SuppressWarnings("javadoc")
	public static final String INTERNAl_VENDOR = "ERP";

	@SuppressWarnings("javadoc")
	public static final String VENDOR_ITEM_CATEGORY = "TAB";

}
