/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.sapomsreturnprocess.returns.strategy.impl;

import de.hybris.platform.ordersplitting.model.ConsignmentEntryModel;
import de.hybris.platform.returns.model.ReturnEntryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.sap.hybris.sapomsreturnprocess.returns.strategy.ReturnOrderStartegy;


public class DefaultReturnOrderSplittingStrategy implements ReturnOrderStartegy
{
	@Override
	public void splitOrder(final Map<ReturnEntryModel, List<ConsignmentEntryModel>> returnOrderEntryConsignmentListMap)
	{
		for (final Map.Entry<ReturnEntryModel, List<ConsignmentEntryModel>> entry : returnOrderEntryConsignmentListMap.entrySet())
		{
			final Long expectedQuantity = entry.getKey().getExpectedQuantity();
			List<ConsignmentEntryModel> consignmentList = entry.getValue();

			consignmentList = distributeQuantityInMultipleConsignments(expectedQuantity, consignmentList);

			entry.setValue(consignmentList);
			
		}
	}

	/**
	 * 
	 */
	private List<ConsignmentEntryModel> distributeQuantityInMultipleConsignments(final Long expectedQuantity,
			final List<ConsignmentEntryModel> consignmentList)
	{
		final List<ConsignmentEntryModel> consignmentListTemp = new ArrayList<>();

		Long totalReturnQuantity = new Long(0);
	
		for (int i = 0; i <= consignmentList.size() - 1; i++)
		{
			final long returnableQuantity = consignmentList.get(i).getQuantityShipped()
					- consignmentList.get(i).getQuantityReturnedUptil();
			if ((expectedQuantity - totalReturnQuantity) <= returnableQuantity)
			{
				consignmentListTemp.add(consignmentList.get(i));
				consignmentList.get(i).setQuantityReturnedUptil(
						expectedQuantity - totalReturnQuantity + consignmentList.get(i).getQuantityReturnedUptil());
				consignmentList.get(i).setReturnQuantity(expectedQuantity - totalReturnQuantity);

				break;
			}
			else
			{

				if (returnableQuantity > 0)
				{
					consignmentListTemp.add(consignmentList.get(i));
					totalReturnQuantity = totalReturnQuantity + returnableQuantity;
					consignmentList.get(i).setQuantityReturnedUptil(consignmentList.get(i).getQuantityShipped());
					consignmentList.get(i).setReturnQuantity(returnableQuantity);
				}
			}
		}
		return consignmentListTemp;
	}
}
