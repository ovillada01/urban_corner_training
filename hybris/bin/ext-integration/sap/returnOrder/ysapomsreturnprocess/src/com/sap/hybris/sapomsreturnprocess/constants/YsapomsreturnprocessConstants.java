/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.sapomsreturnprocess.constants;

/**
 * Global class for all Ysapomsreturnprocess constants. You can add global constants for your extension into this class.
 */
public final class YsapomsreturnprocessConstants extends GeneratedYsapomsreturnprocessConstants 

{
	public static final String PLATFORM_LOGO_CODE = "ysapomsreturnprocessPlatformLogo";
	public static final String MISSING_SALES_ORG = "MISSING_SALES_ORG";
	public static final String MISSING_LOGICAL_SYSTEM = "MISSING_LOGICAL_SYSTEM";
	public static final String UNDERSCORE = "_";
	public static final String COMMA = ",";
	public static final String PIPE = "|";
	private YsapomsreturnprocessConstants()
	{
		//empty to avoid instantiating this constant class
	}


}
