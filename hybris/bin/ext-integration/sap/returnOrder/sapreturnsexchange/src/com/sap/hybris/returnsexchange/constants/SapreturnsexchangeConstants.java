/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.sap.hybris.returnsexchange.constants;

/**
 * Global class for all Sapreturnsexchange constants. You can add global constants for your extension into this class.
 */
public final class SapreturnsexchangeConstants extends GeneratedSapreturnsexchangeConstants 
{


	public static final String RETURNORDER_CONFIRMATION_EVENT = "ReturnRequestCreationEvent_";
	public static final String RETURNORDER_GOOD_EVENT = "ApproveOrCancelGoodsEvent_";
	public static final String RETURNORDER_PAYMENT_REVERSAL_EVENT = "PaymentReversalEvent_";
	public static final String CODE = "code";
	public static final String SEPERATING_SYMBOL = "#";
	public static final String PLATFORM_LOGO_CODE = "sapreturnsexchangePlatformLogo";
	
	private SapreturnsexchangeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension


}
