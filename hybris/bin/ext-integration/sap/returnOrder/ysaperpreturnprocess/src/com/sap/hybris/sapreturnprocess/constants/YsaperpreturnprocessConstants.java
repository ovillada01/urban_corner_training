package com.sap.hybris.sapreturnprocess.constants;

@SuppressWarnings({"deprecation","PMD","squid:CallToDeprecatedMethod"})
public class YsaperpreturnprocessConstants extends GeneratedYsaperpreturnprocessConstants
{
	public static final String EXTENSIONNAME = "ysaperpreturnprocess";
	
	private YsaperpreturnprocessConstants()
	{
		//empty
	}
	
	
}
