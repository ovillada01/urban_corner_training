/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.sap.digitalpayment.controllers;

/**
 *
 * Controller constant class for sapdigitalpaymentaddon extension
 */
public interface ControllerConstants
{
	final String ADDON_PREFIX = "addon:/sapdigitalpaymentaddon/"; // NOSONAR

	/**
	 * Class with view name constants
	 */
	interface Views
	{
		interface Pages
		{

			interface MultiStepCheckout // NOSONAR
			{
				String AddPaymentMethodPage = "pages/checkout/multi/addPaymentMethodPage"; // NOSONAR
				String AddEditBillingAddressPage = ADDON_PREFIX + "pages/checkout/multi/addEditBillingAddressPage"; // NOSONAR
				String AddEditCardDetailsPage = ADDON_PREFIX + "pages/checkout/multi/addEditCardDetailsPage"; // NOSONAR
				String DigitalPaymentGeneralErrorPage = ADDON_PREFIX + "pages/checkout/multi/digitalPaymentGeneralErrorPage"; // NOSONAR
			}

			interface Error // NOSONAR
			{
				String ErrorNotFoundPage = "pages/error/errorNotFoundPage"; // NOSONAR
			}
		}
	}

}
