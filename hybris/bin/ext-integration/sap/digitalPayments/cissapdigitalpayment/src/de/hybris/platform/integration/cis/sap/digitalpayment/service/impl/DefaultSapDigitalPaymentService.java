/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.sap.digitalpayment.service.impl;

import static de.hybris.platform.servicelayer.util.ServicesUtil.validateParameterNotNullStandardMessage;

import de.hybris.platform.acceleratorservices.payment.strategies.PaymentFormActionUrlStrategy;
import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.commercefacades.user.data.AddressData;
import de.hybris.platform.commerceservices.customer.CustomerAccountService;
import de.hybris.platform.commerceservices.order.CommerceCardTypeService;
import de.hybris.platform.commerceservices.order.CommerceCheckoutService;
import de.hybris.platform.commerceservices.service.data.CommerceCheckoutParameter;
import de.hybris.platform.core.model.order.CartModel;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorization;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationResult;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentCard;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentTransactionResult;
import de.hybris.platform.integration.cis.sap.digitalpayment.constants.CisSapDigitalPaymentConstant;
import de.hybris.platform.integration.cis.sap.digitalpayment.exceptions.SapDigitalPaymentPollRegisteredCardException;
import de.hybris.platform.integration.cis.sap.digitalpayment.model.SapDigitPayPollCardProcessModel;
import de.hybris.platform.integration.cis.sap.digitalpayment.service.CisSapDigitalPaymentService;
import de.hybris.platform.integration.cis.sap.digitalpayment.service.SapDigitalPaymentService;
import de.hybris.platform.order.CalculationService;
import de.hybris.platform.order.CartService;
import de.hybris.platform.payment.PaymentService;
import de.hybris.platform.payment.dto.BillingInfo;
import de.hybris.platform.payment.dto.CardInfo;
import de.hybris.platform.payment.dto.CardType;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.enums.PaymentTransactionType;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;
import de.hybris.platform.payment.model.PaymentTransactionModel;
import de.hybris.platform.processengine.BusinessProcessService;
import de.hybris.platform.servicelayer.dto.converter.Converter;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.store.BaseStoreModel;
import de.hybris.platform.store.services.BaseStoreService;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Default implementation of {@link SapDigitalPaymentService}
 */

public class DefaultSapDigitalPaymentService implements SapDigitalPaymentService
{

	private static final Logger LOG = LoggerFactory.getLogger(DefaultSapDigitalPaymentService.class);

	private CisSapDigitalPaymentService cisSapDigitalPaymentService;
	private ModelService modelService;
	private PaymentService paymentService;
	private CommonI18NService commonI18NService;
	private PaymentFormActionUrlStrategy paymentFormActionUrlStrategy;
	private BusinessProcessService businessProcessService;
	private CartService cartService;
	private CommerceCardTypeService commerceCardTypeService;
	private CustomerAccountService customerAccountService;
	private CommerceCheckoutService commerceCheckoutService;
	private Converter<AddressModel, AddressData> addressConverter;
	private UserService userService;
	private BaseStoreService baseStoreService;
	private CalculationService calculationService;

	private Map<String, String> sapDigiPayAuthTranResult;


	@Override
	public PaymentTransactionEntryModel authorize(final String merchantTransactionCode, final String paymentProvider,
			final AddressModel deliveryAddress,
			final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{

		final PaymentTransactionModel transaction = (PaymentTransactionModel) this.getModelService()
				.create(PaymentTransactionModel.class);
		transaction.setCode(merchantTransactionCode);


		final String subscriptionID = getSubscriptionId(cisSapDigitalPaymentAuthorizationResult);

		final CisSapDigitalPaymentTransactionResult transactionResult = getCisSapDigitalPaymentTransactionResult(
				cisSapDigitalPaymentAuthorizationResult);


		PaymentTransactionEntryModel entry = (PaymentTransactionEntryModel) getModelService()
				.create(PaymentTransactionEntryModel.class);

		final PaymentTransactionType paymentTransactionType = PaymentTransactionType.AUTHORIZATION;
		entry.setType(paymentTransactionType);

		if (transactionResult != null)
		{


			transaction.setRequestId(transactionResult.getDigitalPaymentTransaction());
			transaction.setRequestToken(transactionResult.getDigitalPaymentTransaction());
			transaction.setPaymentProvider(paymentProvider);

			this.getModelService().save(transaction);
			if (checkIfPaymentAuthorizationIsSucess(transactionResult))
			{
				entry = fillPaymentTransactionEntryFields(cisSapDigitalPaymentAuthorizationResult, entry);
				transaction.setPlannedAmount(getAuthorizedAmount(cisSapDigitalPaymentAuthorizationResult));
			}

			entry.setPaymentTransaction(transaction);
			entry.setRequestId(transactionResult.getDigitalPaymentTransaction());
			entry.setRequestToken(transactionResult.getDigitalPaymentTransaction());
			entry.setTransactionStatus(getTransactionStatus(transactionResult));
			entry.setTransactionStatusDetails(transactionResult.getDigitalPaytTransRsltDesc());

			final String newEntryCode = getPaymentService().getNewPaymentTransactionEntryCode(transaction, paymentTransactionType);
			entry.setCode(newEntryCode);

			if (subscriptionID != null)
			{
				entry.setSubscriptionID(subscriptionID);
			}
		}

		this.getModelService().save(entry);
		this.getModelService().refresh(transaction);
		return entry;
	}




	/**
	 * @param entry
	 *
	 */
	private PaymentTransactionEntryModel fillPaymentTransactionEntryFields(
			final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult,
			final PaymentTransactionEntryModel entry)
	{
		entry.setAmount(getAuthorizedAmount(cisSapDigitalPaymentAuthorizationResult));

		final CisSapDigitalPaymentAuthorization authorizationResult = getCisSapDigitalPaymentAuthorization(
				cisSapDigitalPaymentAuthorizationResult);
		if (authorizationResult != null)
		{
			entry.setCurrency(this.getCommonI18NService().getCurrency(authorizationResult.getAuthorizationCurrency()));
			//Adding the new filed added PaymentTransaction -start

			entry.setAuthByPaytSrvcPrvdr(authorizationResult.getAuthorizationByPaytSrvcPrvdr());
			entry.setAuthByAcquirer(authorizationResult.getAuthorizationByAcquirer());
			entry.setAuthByDigitalPaytSrvc(authorizationResult.getAuthorizationByDigitalPaytSrvc());


			entry.setAuthStatus(authorizationResult.getAuthorizationStatus());
			entry.setAuthStatusName(authorizationResult.getAuthorizationStatusName());
			entry.setTime(authorizationResult.getAuthorizationDateTime() == null ? new Date()
					: constructAuthorizationDateTime(authorizationResult.getAuthorizationDateTime()));
		}
		return entry;
	}




	/**
	 *
	 */
	private boolean checkIfPaymentAuthorizationIsSucess(final CisSapDigitalPaymentTransactionResult transactionResult)
	{
		// YTODO Auto-generated method stub
		if (StringUtils.isNotEmpty(transactionResult.getDigitalPaytTransResult()))
		{
			return CisSapDigitalPaymentConstant.AUTH_TRANS_RES_SUCCESS_STAT
					.equals(getSapDigiPayAuthTranResult().get(transactionResult.getDigitalPaytTransResult()));
		}
		return false;

	}




	/**
	 * This method will get the card registration URL from the SAP Digital payment
	 */
	@Override
	public String getCardRegistrationUrl()
	{
		return getPaymentFormActionUrlStrategy().getHopRequestUrl();
	}



	@Override
	public void createPollRegisteredCardProcess(final String sessionId)
	{
		if (StringUtils.isNotEmpty(sessionId))
		{

			final String processCode = CisSapDigitalPaymentConstant.SAP_DIGITAL_PAYMENT_POLL_REG_CARD_PROCESS_DEF_NAME + "-"
					+ sessionId + "-" + System.currentTimeMillis();
			final SapDigitPayPollCardProcessModel pollCardProcessModel = getBusinessProcessService().createProcess(processCode,
					CisSapDigitalPaymentConstant.SAP_DIGITAL_PAYMENT_POLL_REG_CARD_PROCESS_DEF_NAME);
			pollCardProcessModel.setSessionId(sessionId);

			final CartModel currentCart = getCartService().getSessionCart();
			//Set the sessionCart and session user to the process
			getModelService().refresh(currentCart);
			pollCardProcessModel.setSessionCart(currentCart);
			pollCardProcessModel.setSessionUser(getUserService().getCurrentUser());
			pollCardProcessModel.setBaseStore(getBaseStoreService().getCurrentBaseStore());
			try
			{
				getBusinessProcessService().startProcess(pollCardProcessModel);
			}
			catch (final Exception e)
			{
				if (LOG.isDebugEnabled())
				{
					LOG.info("Error while executing poll Registered card process" + e.getMessage());
				}
				throw new SapDigitalPaymentPollRegisteredCardException(
						"Error while polling the registered card. Service unavailable");
			}
		}
	}

	@Override
	public CreditCardPaymentInfoModel createPaymentSubscription(final CCPaymentInfoData paymentInfoData,
			final Map<String, Object> params)
	{
		validateParameterNotNullStandardMessage("paymentInfoData", paymentInfoData);


		if (params.get(CisSapDigitalPaymentConstant.CART) instanceof CartModel)
		{
			saveDeliveryAddressToPaymentInfoData(paymentInfoData, (CartModel) params.get(CisSapDigitalPaymentConstant.CART));
		}
		final AddressData billingAddressData = paymentInfoData.getBillingAddress();
		validateParameterNotNullStandardMessage("billingAddress", billingAddressData);
		try
		{
			if (params.get(CisSapDigitalPaymentConstant.CART) instanceof CartModel
					&& params.get(CisSapDigitalPaymentConstant.USER) instanceof CustomerModel
					&& checkIfCurrentUserIsTheCartUser((UserModel) params.get(CisSapDigitalPaymentConstant.USER),
							(CartModel) params.get(CisSapDigitalPaymentConstant.CART)))
			{

				final CardInfo cardInfo = populateCardInfo(paymentInfoData);
				final BillingInfo billingInfo = populateBillingInfo(paymentInfoData.getBillingAddress());
				return getCustomerAccountService().createPaymentSubscription(
						(CustomerModel) params.get(CisSapDigitalPaymentConstant.USER), cardInfo, billingInfo,
						billingAddressData.getTitleCode(), getPaymentProvider(params), paymentInfoData.isSaved());
			}
		}
		catch (final Exception e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.info("Error while creating payment subscription. " + e.getMessage());
			}
		}
		return null;
	}

	/**
	 *
	 */
	private String getPaymentProvider(final Map<String, Object> params)
	{
		if (params.get(CisSapDigitalPaymentConstant.BASE_STORE) instanceof BaseStoreModel)
		{
			final BaseStoreModel currentStore = (BaseStoreModel) params.get(CisSapDigitalPaymentConstant.BASE_STORE);
			return currentStore.getPaymentProvider() != null ? currentStore.getPaymentProvider()
					: CisSapDigitalPaymentConstant.PAYMENT_PROVIDER;
		}
		return CisSapDigitalPaymentConstant.PAYMENT_PROVIDER;
	}




	@Override
	public boolean saveCreditCardPaymentDetailsToCart(final String paymentInfoId, final Map<String, Object> params)
	{

		validateParameterNotNullStandardMessage("paymentInfoId", paymentInfoId);
		try
		{
			if (params.get(CisSapDigitalPaymentConstant.CART) instanceof CartModel
					&& params.get(CisSapDigitalPaymentConstant.USER) instanceof CustomerModel
					&& checkIfCurrentUserIsTheCartUser((UserModel) params.get(CisSapDigitalPaymentConstant.USER),
							(CartModel) params.get(CisSapDigitalPaymentConstant.CART))
					&& StringUtils.isNotBlank(paymentInfoId))
			{
				final CustomerModel currentUserForCheckout = (CustomerModel) params.get(CisSapDigitalPaymentConstant.USER);
				final CreditCardPaymentInfoModel ccPaymentInfoModel = getCustomerAccountService()
						.getCreditCardPaymentInfoForCode(currentUserForCheckout, paymentInfoId);
				final CartModel cartModel = (CartModel) params.get(CisSapDigitalPaymentConstant.CART);

				boolean paymentAddedToCart = false;
				if (ccPaymentInfoModel != null)
				{
					final CommerceCheckoutParameter parameter = new CommerceCheckoutParameter();
					parameter.setCart(cartModel);
					parameter.setPaymentInfo(ccPaymentInfoModel);
					paymentAddedToCart = getCommerceCheckoutService().setPaymentInfo(parameter);
				}
				if (paymentAddedToCart)
				{
					getModelService().save(cartModel);
				}
				return paymentAddedToCart;

			}
		}
		catch (final Exception e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.info("Error while save the card payment information to the cart  " + e.getMessage());
			}
		}
		return false;
	}


	protected void saveDeliveryAddressToPaymentInfoData(final CCPaymentInfoData paymentInfoData, final CartModel currentCart)
	{
		//Get the shipping address from the cart and set it as the billing address. This will be replaced when the billing address is captured at Hybris side.
		if (null != currentCart.getDeliveryAddress())
		{
			paymentInfoData.setBillingAddress(getAddressConverter().convert(currentCart.getDeliveryAddress()));
		}

	}


	/**
	 *
	 */
	private BillingInfo populateBillingInfo(final AddressData billingAddressData)
	{
		final BillingInfo billingInfo = new BillingInfo();
		billingInfo.setCity(billingAddressData.getTown());
		billingInfo.setCountry(billingAddressData.getCountry() == null ? null : billingAddressData.getCountry().getIsocode());
		billingInfo.setFirstName(billingAddressData.getFirstName());
		billingInfo.setLastName(billingAddressData.getLastName());
		billingInfo.setEmail(billingAddressData.getEmail());
		billingInfo.setPhoneNumber(billingAddressData.getPhone());
		billingInfo.setPostalCode(billingAddressData.getPostalCode());
		billingInfo.setStreet1(billingAddressData.getLine1());
		billingInfo.setStreet2(billingAddressData.getLine2());
		return billingInfo;
	}




	/**
	 *
	 */
	protected CardInfo populateCardInfo(final CCPaymentInfoData paymentInfoData)
	{
		final CardInfo cardInfo = new CardInfo();
		cardInfo.setCardHolderFullName(paymentInfoData.getAccountHolderName());
		cardInfo.setCardNumber(paymentInfoData.getCardNumber());
		final CardType cardType = getCommerceCardTypeService().getCardTypeForCode(paymentInfoData.getCardType());
		cardInfo.setCardType(cardType == null ? null : cardType.getCode());
		cardInfo.setExpirationMonth(Integer.valueOf(paymentInfoData.getExpiryMonth()));
		cardInfo.setExpirationYear(Integer.valueOf(paymentInfoData.getExpiryYear()));
		cardInfo.setIssueNumber(paymentInfoData.getIssueNumber());
		//Adding payment token to cardInfo
		cardInfo.setCardToken(paymentInfoData.getSubscriptionId());
		return cardInfo;
	}




	protected boolean checkIfCurrentUserIsTheCartUser(final UserModel currentUser, final CartModel currentCart)
	{
		return currentCart == null ? false : currentCart.getUser().equals(currentUser);
	}


	/**
	 *
	 */
	protected String getTransactionStatus(final CisSapDigitalPaymentTransactionResult transactionResult)
	{
		if (StringUtils.isNotEmpty(transactionResult.getDigitalPaytTransResult())
				&& CisSapDigitalPaymentConstant.AUTH_TRANS_RES_SUCCESS_STAT
						.equals(getSapDigiPayAuthTranResult().get(transactionResult.getDigitalPaytTransResult())))
		{
			return TransactionStatus.ACCEPTED.toString();
		}
		//Need to check all the transaction codes from the digital payment and map the results
		return TransactionStatus.ERROR.toString();
	}

	/**
	 *
	 */
	protected Date constructAuthorizationDateTime(final String authorizationDateTime)
	{
		final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		try
		{
			return df.parse(authorizationDateTime);
		}
		catch (final ParseException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.info("Error while parsing the Authorization date." + e.getMessage());
			}
		}
		return new Date();

	}

	/**
	 *
	 */
	protected BigDecimal getAuthorizedAmount(final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{
		BigDecimal authAmount = null;
		final CisSapDigitalPaymentAuthorization cisSapDigitalPaymentAuthorization = getCisSapDigitalPaymentAuthorization(
				cisSapDigitalPaymentAuthorizationResult);
		if (null != cisSapDigitalPaymentAuthorization)
		{
			authAmount = new BigDecimal(cisSapDigitalPaymentAuthorization.getAuthorizedAmountInAuthznCrcy());
		}
		return authAmount;
	}

	/**
	 * Check for the subscription ID from the authorization response. If empty,set an empty string
	 */
	protected String getSubscriptionId(final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{
		final CisSapDigitalPaymentCard card = getCisSapDigitalPaymentCard(cisSapDigitalPaymentAuthorizationResult);
		return card != null ? card.getPaytCardByDigitalPaymentSrvc() : StringUtils.EMPTY;
	}


	protected CisSapDigitalPaymentTransactionResult getCisSapDigitalPaymentTransactionResult(
			final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{
		if (null != cisSapDigitalPaymentAuthorizationResult
				&& null != cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentTransactionResult())
		{
			return cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentTransactionResult();
		}

		return null;
	}


	protected CisSapDigitalPaymentAuthorization getCisSapDigitalPaymentAuthorization(
			final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{
		if (null != cisSapDigitalPaymentAuthorizationResult
				&& null != cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentAuthorization())
		{
			return cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentAuthorization();
		}
		return null;
	}

	protected CisSapDigitalPaymentCard getCisSapDigitalPaymentCard(
			final CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult)
	{
		if (null != cisSapDigitalPaymentAuthorizationResult
				&& null != cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentSource()
				&& null != cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentSource().getCisSapDigitalPaymentCard())
		{
			return cisSapDigitalPaymentAuthorizationResult.getCisSapDigitalPaymentSource().getCisSapDigitalPaymentCard();
		}
		return null;
	}

	/**
	 * @return the cisSapDigitalPaymentService
	 */
	public CisSapDigitalPaymentService getCisSapDigitalPaymentService()
	{
		return cisSapDigitalPaymentService;
	}

	/**
	 * @param cisSapDigitalPaymentService
	 *           the cisSapDigitalPaymentService to set
	 */
	public void setCisSapDigitalPaymentService(final CisSapDigitalPaymentService cisSapDigitalPaymentService)
	{
		this.cisSapDigitalPaymentService = cisSapDigitalPaymentService;
	}

	/**
	 * @return the modelService
	 */
	public ModelService getModelService()
	{
		return modelService;
	}

	/**
	 * @param modelService
	 *           the modelService to set
	 */
	public void setModelService(final ModelService modelService)
	{
		this.modelService = modelService;
	}

	/**
	 * @return the paymentService
	 */
	public PaymentService getPaymentService()
	{
		return paymentService;
	}

	/**
	 * @param paymentService
	 *           the paymentService to set
	 */
	public void setPaymentService(final PaymentService paymentService)
	{
		this.paymentService = paymentService;
	}

	/**
	 * @return the commonI18NService
	 */
	public CommonI18NService getCommonI18NService()
	{
		return commonI18NService;
	}

	/**
	 * @param commonI18NService
	 *           the commonI18NService to set
	 */
	public void setCommonI18NService(final CommonI18NService commonI18NService)
	{
		this.commonI18NService = commonI18NService;
	}

	/**
	 * @return the paymentFormActionUrlStrategy
	 */
	public PaymentFormActionUrlStrategy getPaymentFormActionUrlStrategy()
	{
		return paymentFormActionUrlStrategy;
	}

	/**
	 * @param paymentFormActionUrlStrategy
	 *           the paymentFormActionUrlStrategy to set
	 */
	public void setPaymentFormActionUrlStrategy(final PaymentFormActionUrlStrategy paymentFormActionUrlStrategy)
	{
		this.paymentFormActionUrlStrategy = paymentFormActionUrlStrategy;
	}

	/**
	 * @return the businessProcessService
	 */
	public BusinessProcessService getBusinessProcessService()
	{
		return businessProcessService;
	}

	/**
	 * @param businessProcessService
	 *           the businessProcessService to set
	 */
	public void setBusinessProcessService(final BusinessProcessService businessProcessService)
	{
		this.businessProcessService = businessProcessService;
	}




	/**
	 * @return the cartService
	 */
	public CartService getCartService()
	{
		return cartService;
	}


	/**
	 * @param cartService
	 *           the cartService to set
	 */
	public void setCartService(final CartService cartService)
	{
		this.cartService = cartService;
	}





	/**
	 * @return the commerceCardTypeService
	 */
	public CommerceCardTypeService getCommerceCardTypeService()
	{
		return commerceCardTypeService;
	}




	/**
	 * @param commerceCardTypeService
	 *           the commerceCardTypeService to set
	 */
	public void setCommerceCardTypeService(final CommerceCardTypeService commerceCardTypeService)
	{
		this.commerceCardTypeService = commerceCardTypeService;
	}




	/**
	 * @return the customerAccountService
	 */
	public CustomerAccountService getCustomerAccountService()
	{
		return customerAccountService;
	}




	/**
	 * @param customerAccountService
	 *           the customerAccountService to set
	 */
	public void setCustomerAccountService(final CustomerAccountService customerAccountService)
	{
		this.customerAccountService = customerAccountService;
	}




	/**
	 * @return the commerceCheckoutService
	 */
	public CommerceCheckoutService getCommerceCheckoutService()
	{
		return commerceCheckoutService;
	}




	/**
	 * @param commerceCheckoutService
	 *           the commerceCheckoutService to set
	 */
	public void setCommerceCheckoutService(final CommerceCheckoutService commerceCheckoutService)
	{
		this.commerceCheckoutService = commerceCheckoutService;
	}


	/**
	 * @return the addressConverter
	 */
	public Converter<AddressModel, AddressData> getAddressConverter()
	{
		return addressConverter;
	}

	/**
	 * @param addressConverter
	 *           the addressConverter to set
	 */
	public void setAddressConverter(final Converter<AddressModel, AddressData> addressConverter)
	{
		this.addressConverter = addressConverter;
	}




	/**
	 * @return the userService
	 */
	public UserService getUserService()
	{
		return userService;
	}




	/**
	 * @param userService
	 *           the userService to set
	 */
	public void setUserService(final UserService userService)
	{
		this.userService = userService;
	}




	/**
	 * @return the baseStoreService
	 */
	public BaseStoreService getBaseStoreService()
	{
		return baseStoreService;
	}




	/**
	 * @param baseStoreService
	 *           the baseStoreService to set
	 */
	public void setBaseStoreService(final BaseStoreService baseStoreService)
	{
		this.baseStoreService = baseStoreService;
	}




	/**
	 * @return the calculationService
	 */
	public CalculationService getCalculationService()
	{
		return calculationService;
	}




	/**
	 * @param calculationService
	 *           the calculationService to set
	 */
	public void setCalculationService(final CalculationService calculationService)
	{
		this.calculationService = calculationService;
	}




	/**
	 * @return the sapDigiPayAuthTranResult
	 */
	public Map<String, String> getSapDigiPayAuthTranResult()
	{
		return sapDigiPayAuthTranResult;
	}




	/**
	 * @param sapDigiPayAuthTranResult
	 *           the sapDigiPayAuthTranResult to set
	 */
	public void setSapDigiPayAuthTranResult(final Map<String, String> sapDigiPayAuthTranResult)
	{
		this.sapDigiPayAuthTranResult = sapDigiPayAuthTranResult;
	}


}
