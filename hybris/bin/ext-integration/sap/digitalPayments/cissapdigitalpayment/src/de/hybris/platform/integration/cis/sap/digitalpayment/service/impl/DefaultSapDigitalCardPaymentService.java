/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.sap.digitalpayment.service.impl;

import de.hybris.platform.payment.AdapterException;
import de.hybris.platform.payment.commands.request.CreateSubscriptionRequest;
import de.hybris.platform.payment.commands.result.SubscriptionResult;
import de.hybris.platform.payment.dto.TransactionStatus;
import de.hybris.platform.payment.methods.impl.DefaultCardPaymentServiceImpl;

import org.apache.log4j.Logger;


/**
 * SAP Digital-Payment Addon specific implementation extending {@link DefaultCardPaymentServiceImpl } for creating
 * subscription
 */
public class DefaultSapDigitalCardPaymentService extends DefaultCardPaymentServiceImpl
{

	private static final Logger LOG = Logger.getLogger(DefaultSapDigitalCardPaymentService.class);

	//Subscription ID is copied from the
	@Override
	public SubscriptionResult createSubscription(final CreateSubscriptionRequest request) throws AdapterException
	{
		final SubscriptionResult subscriptionResult = new SubscriptionResult();
		try
		{


			if (null != request && null != request.getCard() && null != request.getCard().getCardToken())
			{
				subscriptionResult.setSubscriptionID(request.getCard().getCardToken());
			}
			subscriptionResult.setTransactionStatus(TransactionStatus.ACCEPTED);

		}
		catch (final AdapterException e)
		{
			if (LOG.isDebugEnabled())
			{
				LOG.info("Error while creating subscription" + e.getMessage());
			}
		}
		return subscriptionResult;

	}

}
