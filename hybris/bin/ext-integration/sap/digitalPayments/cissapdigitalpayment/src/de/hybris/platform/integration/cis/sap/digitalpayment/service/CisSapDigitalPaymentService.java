/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.sap.digitalpayment.service;

import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationRequestList;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationResultList;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentPollRegisteredCardResult;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentRegistrationUrlResult;

import java.util.concurrent.TimeoutException;

import com.hybris.cis.service.CisClientService;

import rx.Observable;


/**
 * CIS service which exposes the payment functionalities of SAP Digital Payments Addon
 */
public interface CisSapDigitalPaymentService extends CisClientService
{

	/**
	 * Retrieves the payment service url from SAP Digital Payments Addon
	 *
	 * @param cisSapDigitalPaymentRegistrationUrlRequest
	 *           - contains all the request parameters to be sent to SAP Digital Payments Addon
	 * @return a cisSapDigitalPaymentRegistrationUrlResult response with the card registration url
	 */
	Observable<CisSapDigitalPaymentRegistrationUrlResult> getRegistrationUrl() throws TimeoutException;


	/**
	 * Requests card details from SAP Digital Payments Addon
	 *
	 * @param sessionId
	 *           - used to fetch the card details upon registration
	 * @return registered card details
	 */
	Observable<CisSapDigitalPaymentPollRegisteredCardResult> pollRegisteredCard(String sessionId);


	/**
	 * Requests an authorization of a payment
	 *
	 * @param authorizationRequests
	 *           - external payment request
	 * @return authorization result
	 */
	Observable<CisSapDigitalPaymentAuthorizationResultList> authorizePayment(
			CisSapDigitalPaymentAuthorizationRequestList authorizationRequests);

}
