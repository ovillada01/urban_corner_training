/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.sap.digitalpayment.service;

import de.hybris.platform.commercefacades.order.data.CCPaymentInfoData;
import de.hybris.platform.core.model.order.payment.CreditCardPaymentInfoModel;
import de.hybris.platform.core.model.user.AddressModel;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationResult;
import de.hybris.platform.payment.model.PaymentTransactionEntryModel;

import java.util.Map;


/**
 * Defines payment related services
 */
public interface SapDigitalPaymentService
{


	/**
	 * Requests a payment authorization
	 *
	 * @param merchantTransactionCode
	 *           - merchant transaction code
	 * @param paymentProvider
	 *           - payment service provider
	 * @param deliveryAddress
	 *           - delivery address
	 * @param cisSapDigitalPaymentAuthorizationResult
	 *
	 * @return authorize result
	 */
	PaymentTransactionEntryModel authorize(String merchantTransactionCode, String paymentProvider, AddressModel deliveryAddress,
			CisSapDigitalPaymentAuthorizationResult cisSapDigitalPaymentAuthorizationResult);

	/**
	 * Requests card registration URL from Digital payment
	 *
	 * @return registration URL
	 */
	String getCardRegistrationUrl();

	/**
	 * triggers a process which polls backend for a card
	 *
	 * @param sessionId
	 *           sessionId w.r.t registered card
	 */
	void createPollRegisteredCardProcess(String sessionId);


	/**
	 * creates payment subscription
	 *
	 * @param paymentInfoData
	 *           has card information
	 * @param params
	 *           contains card and user details
	 * @return credit card details from backend
	 *
	 */
	CreditCardPaymentInfoModel createPaymentSubscription(CCPaymentInfoData paymentInfoData, Map<String, Object> params);

	/**
	 * save credit card details to the cart
	 *
	 * @param paymentInfoId
	 *           paymentID
	 *
	 * @param params
	 *           contains card and user details
	 * @return success or failure
	 */
	boolean saveCreditCardPaymentDetailsToCart(String paymentInfoId, Map<String, Object> params);


}
