/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.integration.cis.sap.digitalpayment.service.impl;

import de.hybris.platform.integration.cis.sap.digitalpayment.client.SapDigitalPaymentClient;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationRequestList;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentAuthorizationResultList;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentPollRegisteredCardResult;
import de.hybris.platform.integration.cis.sap.digitalpayment.client.model.CisSapDigitalPaymentRegistrationUrlResult;
import de.hybris.platform.integration.cis.sap.digitalpayment.constants.CisSapDigitalPaymentConstant;
import de.hybris.platform.integration.cis.sap.digitalpayment.service.CisSapDigitalPaymentService;

import java.util.Map;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;

import rx.Observable;


/**
 * Default implementation of {@link CisSapDigitalPaymentService}
 */
public class DefaultCisSapDigitalPaymentService implements CisSapDigitalPaymentService
{


	private static final Logger LOG = Logger.getLogger(DefaultCisSapDigitalPaymentService.class);

	private SapDigitalPaymentClient cisSapDigitalPaymentClient;

	private static Map<String, String> pollCardStatusMap;



	@Override
	public boolean ping(final String xCisClientRef, final String tenantId)
	{
		return true;
	}

	@Override
	public Observable<CisSapDigitalPaymentRegistrationUrlResult> getRegistrationUrl() throws TimeoutException
	{
		return getCisSapDigitalPaymentClient().getRegistrationUrl().map(registratioUrlResp -> {
			logSuccess(registratioUrlResp.toString(), "successfully received registarion URL response");
			return registratioUrlResp;
		}).doOnError(DefaultCisSapDigitalPaymentService::logError);
	}

	@Override
	public Observable<CisSapDigitalPaymentPollRegisteredCardResult> pollRegisteredCard(final String sessionId)
	{
		return getCisSapDigitalPaymentClient().pollRegisteredCard(sessionId).map(registeredCard -> {
			logSuccess(registeredCard.toString(), "Successfully poll the registed card");
			return registeredCard;
		}).doOnError(DefaultCisSapDigitalPaymentService::logError).repeat()
				.takeUntil(DefaultCisSapDigitalPaymentService::checkPollCardTransactionResult);
	}

	@Override
	public Observable<CisSapDigitalPaymentAuthorizationResultList> authorizePayment(
			final CisSapDigitalPaymentAuthorizationRequestList authorizationRequests)
	{
		return getCisSapDigitalPaymentClient().authorizatePayment(authorizationRequests).map(authResp -> {
			logSuccess(authResp.toString(), "successfully received the payment authorization response");
			return authResp;
		}).doOnError(DefaultCisSapDigitalPaymentService::logError);
	}

	/**
	 * @return the cisSapDigitalPaymentClient
	 */
	public SapDigitalPaymentClient getCisSapDigitalPaymentClient()
	{
		return cisSapDigitalPaymentClient;
	}

	/**
	 * @param cisSapDigitalPaymentClient
	 *           the cisSapDigitalPaymentClient to set
	 */
	public void setCisSapDigitalPaymentClient(final SapDigitalPaymentClient cisSapDigitalPaymentClient)
	{
		this.cisSapDigitalPaymentClient = cisSapDigitalPaymentClient;
	}

	private static void logSuccess(final String response, final String message)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug(message + response);
		}
	}

	private static void logError(final Throwable error)
	{
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Error while fetching the response" + error);
		}
	}

	private static boolean checkPollCardTransactionResult(
			final CisSapDigitalPaymentPollRegisteredCardResult sapDigitalPaymentPollRegisteredCardResult)
	{
		if (null != sapDigitalPaymentPollRegisteredCardResult
				&& null != sapDigitalPaymentPollRegisteredCardResult.getCisSapDigitalPaymentTransactionResult())
		{
			final String pollStatus = getPollCardStatusMap().get(
					sapDigitalPaymentPollRegisteredCardResult.getCisSapDigitalPaymentTransactionResult().getDigitalPaytTransResult());
			if (CisSapDigitalPaymentConstant.POLL_REG_CARD_PENDING_STAT.equals(pollStatus))
			{
				return false;
			}
			else if (CisSapDigitalPaymentConstant.POLL_REG_CARD_CANCELLED_STAT.equals(pollStatus)
					|| CisSapDigitalPaymentConstant.POLL_REG_CARD_SUCCESS_STAT.equals(pollStatus)
					|| CisSapDigitalPaymentConstant.POLL_REG_CARD_TIMEOUT_STAT.equals(pollStatus))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * @return the pollCardStatusMap
	 */
	public static Map<String, String> getPollCardStatusMap()
	{
		return pollCardStatusMap;
	}

	/**
	 * @param pollCardStatusMap
	 *           the pollCardStatusMap to set
	 */
	public static void setPollCardStatusMap(final Map<String, String> pollCardStatusMap)
	{
		DefaultCisSapDigitalPaymentService.pollCardStatusMap = pollCardStatusMap;
	}




}
