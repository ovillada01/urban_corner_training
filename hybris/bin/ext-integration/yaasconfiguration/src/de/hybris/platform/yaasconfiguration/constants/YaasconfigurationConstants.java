/*
* [y] hybris Platform
*
* Copyright (c) 2017 SAP SE or an SAP affiliate company.
* All rights reserved.
*
* This software is the confidential and proprietary information of SAP
* ("Confidential Information"). You shall not disclose such Confidential
* Information and shall use it only in accordance with the terms of the
* license agreement you entered into with SAP.
*
*/
package de.hybris.platform.yaasconfiguration.constants;

/**
 * Global class for all Yaasconfiguration constants. You can add global constants for your extension into this class.
 */
public final class YaasconfigurationConstants extends GeneratedYaasconfigurationConstants
{
	public static final String EXTENSIONNAME = "yaasconfiguration";

	public static final String YAAS_OAUTH_URL = "oauth.url";
	public static final String YAAS_OAUTH_CLIENTID = "oauth.clientId";
	public static final String YAAS_OAUTH_CLIENTSECRET = "oauth.clientSecret";
	public static final String YAAS_PROJECT_ID = "projectId";
	public static final String YAAS_CLIENT_URL = "url";
	public static final String YAAS_CLIENT_SCOPE = "oauth.scope";
	public static final String YAAS_TENANT = "tenant";

	public static final String YAAS_CACHE_DELIMITER = "#";

	public static final String STRING_CONSTANT_DOT = ".";

	public static final String YAAS_APPLICATION_TYPECODE = "6602";
	public static final String YAAS_CLIENT_TYPECODE = "6603";

	public static final String SESSION_YAAS_APPLICATIONID = "yaasAppId";

	private YaasconfigurationConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
