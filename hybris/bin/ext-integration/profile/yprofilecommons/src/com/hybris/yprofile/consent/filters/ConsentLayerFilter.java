/*
 * [y] hybris Platform
 * 
 * Copyright (c) 2000-2016 SAP SE
 * All rights reserved.
 * 
 * This software is the confidential and proprietary information of SAP 
 * Hybris ("Confidential Information"). You shall not disclose such 
 * Confidential Information and shall use it only in accordance with the 
 * terms of the license agreement you entered into with SAP Hybris.
 */
package com.hybris.yprofile.consent.filters;

import com.hybris.yprofile.common.Utils;
import com.hybris.yprofile.consent.services.ConsentService;
import com.hybris.yprofile.services.ProfileConfigurationService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.Cookie;

import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * Generates the consent reference for the user and stores it in a cookie and in session
 */
public class ConsentLayerFilter extends OncePerRequestFilter
{
    private static final Logger LOG = Logger.getLogger(ConsentLayerFilter.class);
    private static final AntPathMatcher pathMatcher = new AntPathMatcher();
    private ConsentService consentService;
    private boolean enabled;
    private String excludeUrlPatterns;

    private ProfileConfigurationService profileConfigurationService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        if (isProfileTrackingPaused(httpServletRequest)) {
            LOG.debug("Profile tracking disabled");
          } else {
            getConsentService().generateConsentReference(httpServletRequest, httpServletResponse, isEnabled());
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    protected boolean isProfileTrackingPaused(HttpServletRequest httpServletRequest) {

        Optional<Cookie> pauseProfileTrackingCoockie = Utils.getCookie(httpServletRequest, ProfileConfigurationService.PROFILE_TRACKING_PAUSE);
        profileConfigurationService.storeProfileTrackingPauseValue(pauseProfileTrackingCoockie.isPresent());

        return pauseProfileTrackingCoockie.isPresent();
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {

        if (isBlank(excludeUrlPatterns)){
            return false;
        }

        final List<String> patterns = Arrays.asList(excludeUrlPatterns.split("\\s*,\\s*"));

        return patterns.stream()
                .anyMatch(p -> (request.getRequestURI() !=null && pathMatcher.match(p, request.getRequestURI()))
                        || (request.getQueryString() !=null && pathMatcher.match(p, request.getQueryString())));
    }

    public ConsentService getConsentService() {
        return consentService;
    }

    @Required
    public void setConsentService(ConsentService consentService) {
        this.consentService = consentService;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Required
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Required
    public void setProfileConfigurationService(ProfileConfigurationService profileConfigurationService) {
        this.profileConfigurationService = profileConfigurationService;
    }

    @Required
    public void setExcludeUrlPatterns(String excludeUrlPatterns) {
        this.excludeUrlPatterns = excludeUrlPatterns;
    }
}
