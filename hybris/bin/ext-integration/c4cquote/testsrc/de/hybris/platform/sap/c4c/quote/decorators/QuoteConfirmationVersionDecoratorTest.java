package de.hybris.platform.sap.c4c.quote.decorators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteHelper;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteVersionControlHelper;

@UnitTest
public class QuoteConfirmationVersionDecoratorTest
{
	@InjectMocks
	private QuoteConfirmationVersionDecorator confirmationVersionDecorator = new QuoteConfirmationVersionDecorator();
	
	@Mock
	private InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper;
	@Mock
	private InboundQuoteHelper inboundQuoteHelper;
	
	@Before
   public void setUp() {
       MockitoAnnotations.initMocks(this);
   }
	
	@Test
	public void testDecorate()
	{
		int position = 1;
		String quoteId = new String("12345");
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);
		QuoteModel quote = mock(QuoteModel.class);
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn("12345");
		when(inboundQuoteVersionControlHelper.getQuoteforCode(Mockito.anyString())).thenReturn(quote);
		when(quote.getVersion()).thenReturn(new Integer(2));
		
		String result = confirmationVersionDecorator.decorate(position, impexLine);
		
		Assert.assertEquals("2", result);
		
	}
	
	@Test
	public void testDecorateWithoutQuoteId()
	{
		int position = 1;
		String quoteId = new String();
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn(null);
		String result = confirmationVersionDecorator.decorate(position, impexLine);
		
		Assert.assertNull(result);
	}
		
}
