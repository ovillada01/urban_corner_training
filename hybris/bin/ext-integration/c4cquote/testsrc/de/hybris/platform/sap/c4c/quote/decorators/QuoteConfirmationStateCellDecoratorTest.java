package de.hybris.platform.sap.c4c.quote.decorators;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.core.enums.QuoteState;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteHelper;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteVersionControlHelper;

@UnitTest
public class QuoteConfirmationStateCellDecoratorTest
{
	@InjectMocks
	private QuoteConfirmationStateCellDecorator quoteConfirmationStateCellDecorator = new QuoteConfirmationStateCellDecorator();
	
	@Mock
	private InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper;
	@Mock
	private InboundQuoteHelper inboundQuoteHelper;
	
	@Before
   public void setUp() {
       MockitoAnnotations.initMocks(this);
   }
	
	@Test
	public void testDecorateWithBuyerSubmittedState()
	{
		int position = 1;
		String quoteId = new String("12345");
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);
		QuoteModel quote = mock(QuoteModel.class);
		QuoteModel resultQuote = mock(QuoteModel.class);
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn("12345");
		when(inboundQuoteVersionControlHelper.getQuoteforCode(Mockito.anyString())).thenReturn(quote);
		when(quote.getState()).thenReturn(QuoteState.BUYER_SUBMITTED);
		when(inboundQuoteHelper.createQuoteSnapshot(Mockito.anyString(), Mockito.anyString())).thenReturn(resultQuote);
		when(resultQuote.getState()).thenReturn(QuoteState.SELLER_REQUEST);
		
		String resultState = quoteConfirmationStateCellDecorator.decorate(position, impexLine);
		
		Assert.assertNotNull(resultState);
		Assert.assertEquals(QuoteState.SELLER_REQUEST.toString(), resultState);
		
	}
	
	@Test
	public void testDecorateWithCancelledState()
	{
		int position = 1;
		String quoteId = new String("12345");
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);
		QuoteModel quote = mock(QuoteModel.class);
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn("12345");
		when(inboundQuoteVersionControlHelper.getQuoteforCode(Mockito.anyString())).thenReturn(quote);
		when(quote.getState()).thenReturn(QuoteState.CANCELLED);		
		String resultState = quoteConfirmationStateCellDecorator.decorate(position, impexLine);
		
		Assert.assertNotNull(resultState);
		Assert.assertEquals(QuoteState.CANCELLED.toString(), resultState);
		
	}
	
	@Test
	public void testDecorateWithQuoteIdNull()
	{
		int position = 1;
		String quoteId = null;
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);	
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn(quoteId);
		String resultState = quoteConfirmationStateCellDecorator.decorate(position, impexLine);
		
		Assert.assertNull(resultState);
		
	}
	
	@Test
	public void testDecorateWithQuoteNull()
	{
		int position = 1;
		String quoteId = "12345";
		Map<Integer, String> impexLine = new HashMap<>();
		impexLine.put(new Integer(position), quoteId);	
		QuoteModel quote = null;
		when(inboundQuoteHelper.getValidCode(Mockito.anyString())).thenReturn(quoteId);
		when(inboundQuoteVersionControlHelper.getQuoteforCode(Mockito.anyString())).thenReturn(quote);
		String resultState = quoteConfirmationStateCellDecorator.decorate(position, impexLine);
		
		Assert.assertNull(resultState);
		
	}
}
