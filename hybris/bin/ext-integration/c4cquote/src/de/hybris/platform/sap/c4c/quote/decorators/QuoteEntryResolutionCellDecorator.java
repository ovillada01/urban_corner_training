package de.hybris.platform.sap.c4c.quote.decorators;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.sap.c4c.quote.constants.C4cquoteConstants;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteHelper;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteVersionControlHelper;
import de.hybris.platform.util.CSVCellDecorator;

public class QuoteEntryResolutionCellDecorator implements CSVCellDecorator {

	private static final Logger LOG = LoggerFactory.getLogger(QuoteEntryResolutionCellDecorator.class);
	private static final String SEPARATOR = ":";
	private InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper = (InboundQuoteVersionControlHelper) Registry.getApplicationContext().getBean("inboundQuoteVersionControlHelper");

	private InboundQuoteHelper inboundQuoteHelper = (InboundQuoteHelper) Registry.getApplicationContext().getBean("inboundQuoteHelper");

	@Override
	public String decorate(int position, Map<Integer, String> impexLine) {
		LOG.info("Decorating order entry information from canonical into target models.");
		final String orderEntryInfo = impexLine.get(Integer.valueOf(position));
		String result = "";
		if (orderEntryInfo != null && !orderEntryInfo.equals(C4cquoteConstants.IGNORE)) {
			final List<String> orderEntryData = Arrays.asList(StringUtils.split(orderEntryInfo, '|'));
			final String quoteId = orderEntryData.get(0);
			final String entryNumber = getInboundQuoteHelper().convertEntryNumber(orderEntryData.get(1));
			String validQuoteId = getInboundQuoteHelper().getValidCode(quoteId);
			QuoteModel quote = getInboundQuoteVersionControlHelper().getQuoteforCode(validQuoteId);
			StringBuilder quoteEntryString = new StringBuilder();
			if (quote != null) {
				quoteEntryString = quoteEntryString.append(quote.getCode()).append(SEPARATOR).append(entryNumber);
			} else {
				LOG.info("No quote exist in system with quoteId= " + validQuoteId);
			}
			result = quoteEntryString.toString();
		}
		return result;
	}

	public InboundQuoteVersionControlHelper getInboundQuoteVersionControlHelper() {
		return inboundQuoteVersionControlHelper;
	}

	public void setInboundQuoteVersionControlHelper(InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper) {
		this.inboundQuoteVersionControlHelper = inboundQuoteVersionControlHelper;
	}

	public InboundQuoteHelper getInboundQuoteHelper() {
		return inboundQuoteHelper;
	}

	public void setInboundQuoteHelper(InboundQuoteHelper inboundQuoteHelper) {
		this.inboundQuoteHelper = inboundQuoteHelper;
	}

}
