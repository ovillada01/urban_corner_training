/*
 * [y] hybris Platform
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.sap.c4c.quote.decorators;

import java.util.Map;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteHelper;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteVersionControlHelper;
import de.hybris.platform.util.CSVCellDecorator;


/**
 * Decorator class for resolving version while updating a given quote
 */
public class UpdateQuoteCellDecorator implements CSVCellDecorator
{

	private InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper = (InboundQuoteVersionControlHelper) Registry
			.getApplicationContext().getBean("inboundQuoteVersionControlHelper");



	private InboundQuoteHelper inboundQuoteHelper = (InboundQuoteHelper) Registry.getApplicationContext().getBean(
			"inboundQuoteHelper");


	@Override
	public String decorate(int position, Map<Integer, String> impexLine)
	{
		final String quoteId = impexLine.get(Integer.valueOf(position));
		QuoteModel quote = getInboundQuoteVersionControlHelper().getQuoteforCode(getInboundQuoteHelper().getValidCode(quoteId));
		return Integer.toString(quote.getVersion());
	}

	public InboundQuoteVersionControlHelper getInboundQuoteVersionControlHelper()
	{
		return inboundQuoteVersionControlHelper;
	}

	public void setInboundQuoteVersionControlHelper(InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper)
	{
		this.inboundQuoteVersionControlHelper = inboundQuoteVersionControlHelper;
	}

	public InboundQuoteHelper getInboundQuoteHelper()
	{
		return inboundQuoteHelper;
	}

	public void setInboundQuoteHelper(InboundQuoteHelper inboundQuoteHelper)
	{
		this.inboundQuoteHelper = inboundQuoteHelper;
	}

}
