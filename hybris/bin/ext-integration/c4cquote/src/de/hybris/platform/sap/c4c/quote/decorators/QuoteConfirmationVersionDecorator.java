/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2017 SAP SE
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * Hybris ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with the
 * terms of the license agreement you entered into with SAP Hybris.
 */
package de.hybris.platform.sap.c4c.quote.decorators;

import java.util.Map;

import de.hybris.platform.core.Registry;
import de.hybris.platform.core.model.order.QuoteModel;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteHelper;
import de.hybris.platform.sap.c4c.quote.inbound.InboundQuoteVersionControlHelper;
import de.hybris.platform.util.CSVCellDecorator;


public class QuoteConfirmationVersionDecorator implements CSVCellDecorator
{

	private InboundQuoteHelper inboundQuoteHelper = (InboundQuoteHelper) Registry.getApplicationContext().getBean(
			"inboundQuoteHelper");
	
	private InboundQuoteVersionControlHelper inboundQuoteVersionControlHelper = (InboundQuoteVersionControlHelper) Registry.getApplicationContext().getBean(
			"inboundQuoteVersionControlHelper");

	@Override
	public String decorate(int position, Map<Integer, String> impexLine)
	{
		String quoteId = impexLine.get(position);
		quoteId = inboundQuoteHelper.getValidCode(quoteId);
		String result = null;
		QuoteModel quote = null;
		if (quoteId != null && !quoteId.isEmpty())
		{
			quote = inboundQuoteVersionControlHelper.getQuoteforCode(quoteId);
		}
		if (quote != null)
		{
			result = quote.getVersion().toString();
		}
		return result;
	}

}
