/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('ycmssmarteditModule', ['genericEditorModule', 'ycmssmartedit/cmssmarteditContainerTemplates']);
//    .run(function(GenericEditor, $log, $q) {
//
//        GenericEditor.prototype.submit = function(componentForm) {
//            $log.info("GenericEditor.submit override invocation", componentForm);
//            return $q.when("something");
//        };
//    });
