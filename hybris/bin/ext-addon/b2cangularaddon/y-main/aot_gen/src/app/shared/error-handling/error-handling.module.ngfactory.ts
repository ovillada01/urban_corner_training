/**
 * @fileoverview This file is generated by the Angular template compiler.
 * Do not edit.
 * @suppress {suspiciousCode,uselessCode,missingProperties,missingOverride}
 */
 /* tslint:disable */


import * as import0 from '@angular/core';
import * as import1 from '../../../../../src/app/shared/error-handling/error-handling.module';
import * as import2 from '@angular/common';
import * as import3 from '../../../../../src/app/shared/error-handling/validation.service';
class ErrorHandlingModuleInjector extends import0.ɵNgModuleInjector<import1.ErrorHandlingModule> {
  _CommonModule_0:import2.CommonModule;
  _ErrorHandlingModule_1:import1.ErrorHandlingModule;
  _LOCALE_ID_2:any;
  __NgLocalization_3:import2.NgLocaleLocalization;
  __ValidationService_4:import3.ValidationService;
  _TRANSLATIONS_FORMAT_5:any;
  constructor(parent:import0.Injector) {
    super(parent,([] as any[]),([] as any[]));
  }
  get _NgLocalization_3():import2.NgLocaleLocalization {
    if ((this.__NgLocalization_3 == null)) { (this.__NgLocalization_3 = new import2.NgLocaleLocalization(this._LOCALE_ID_2)); }
    return this.__NgLocalization_3;
  }
  get _ValidationService_4():import3.ValidationService {
    if ((this.__ValidationService_4 == null)) { (this.__ValidationService_4 = new import3.ValidationService()); }
    return this.__ValidationService_4;
  }
  createInternal():import1.ErrorHandlingModule {
    this._CommonModule_0 = new import2.CommonModule();
    this._ErrorHandlingModule_1 = new import1.ErrorHandlingModule();
    this._LOCALE_ID_2 = 'zh';
    this._TRANSLATIONS_FORMAT_5 = 'xlf';
    return this._ErrorHandlingModule_1;
  }
  getInternal(token:any,notFoundResult:any):any {
    if ((token === import2.CommonModule)) { return this._CommonModule_0; }
    if ((token === import1.ErrorHandlingModule)) { return this._ErrorHandlingModule_1; }
    if ((token === import0.LOCALE_ID)) { return this._LOCALE_ID_2; }
    if ((token === import2.NgLocalization)) { return this._NgLocalization_3; }
    if ((token === import3.ValidationService)) { return this._ValidationService_4; }
    if ((token === import0.TRANSLATIONS_FORMAT)) { return this._TRANSLATIONS_FORMAT_5; }
    return notFoundResult;
  }
  destroyInternal():void {
  }
}
export const ErrorHandlingModuleNgFactory:import0.NgModuleFactory<import1.ErrorHandlingModule> = new import0.NgModuleFactory<any>(ErrorHandlingModuleInjector,import1.ErrorHandlingModule);
//# sourceMappingURL=data:application/json;base64,eyJmaWxlIjoiL3Nydi9qZW5raW5zL3dvcmtzcGFjZS8wMi1wYWNrYWdlL3NvdXJjZS9iMmNhbmd1bGFyYWRkb24veS1tYWluL3NyYy9hcHAvc2hhcmVkL2Vycm9yLWhhbmRsaW5nL2Vycm9yLWhhbmRsaW5nLm1vZHVsZS5uZ2ZhY3RvcnkudHMiLCJ2ZXJzaW9uIjozLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJuZzovLy9zcnYvamVua2lucy93b3Jrc3BhY2UvMDItcGFja2FnZS9zb3VyY2UvYjJjYW5ndWxhcmFkZG9uL3ktbWFpbi9zcmMvYXBwL3NoYXJlZC9lcnJvci1oYW5kbGluZy9lcnJvci1oYW5kbGluZy5tb2R1bGUudHMiXSwic291cmNlc0NvbnRlbnQiOlsiICJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=
