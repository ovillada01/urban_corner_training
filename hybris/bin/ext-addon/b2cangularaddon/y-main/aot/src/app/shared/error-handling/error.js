var Error = (function () {
    function Error(subject, message) {
        this.subject = subject;
        this.message = message;
    }
    return Error;
}());
export { Error };
//# sourceMappingURL=error.js.map