
export class TicketType {
  constructor(
    public active: boolean,
    public description: string,
    public type: string) { }
}
