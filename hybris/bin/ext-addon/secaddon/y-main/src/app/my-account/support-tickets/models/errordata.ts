
export class ErrorData {
  constructor(
    public status: string,
    public message: string) { }
}
