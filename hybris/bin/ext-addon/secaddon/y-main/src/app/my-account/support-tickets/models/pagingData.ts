export class PagingData {
  constructor(
    public pageNumber: number,
    public pageSize: number,
    public sort: string) {
  }
}
