import {Owner} from "./owner";
export class Transcript {
  constructor(
    public owner:Owner,
    public description: string,
  ) { }

}
