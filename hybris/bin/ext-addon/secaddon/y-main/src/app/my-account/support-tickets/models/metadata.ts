
export class MetaData {
  constructor(
    public createdAt: string,
    public modifiedAt: string,
    public version: string) { }

}
