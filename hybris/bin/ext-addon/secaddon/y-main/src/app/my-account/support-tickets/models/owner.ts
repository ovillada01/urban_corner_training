export class Owner {
  constructor(
    public isCustomer: boolean,
    public createdAt:string,
    public displayName: string
  ) { }

}
