import { Component } from '@angular/core';

@Component({
  selector: 'y-main',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent { }
