
<style>
    #__interaction0--header{
        display:none;
    }
    #__vbox1{
        height: auto !important;
        width: auto !important;
    }
    .sapMBtnReject {
        display: none;
    }
    
    .sapMLabel{
        font-weight: bold !important;
    }


</style>

<script>
    var script = document.createElement('script');
    script.onload = function() {
      setupShat();
    };
    script.setAttribute("data-sap-ui-theme","sap_bluecrystal");
    script.setAttribute("data-sap-ui-libs","sap.ui.commons,sap.m");
    script.src = parent.CHAT_CONFIG.bootstrapUrl;
    document.getElementsByTagName('head')[0].appendChild(script);
</script>

<script>
function getUrlParameter(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};
var chatType = getUrlParameter("type");

</script>

<script type="text/javascript">
   function setupShat() {
    jQuery.sap.registerModulePath("sap.ecf", parent.CHAT_CONFIG.modulePath);
    jQuery.sap.require("sap.ecf.sap-ecf");
    jQuery.sap.require("sap.ecf.controls.Interaction");
    var widgetInteraction = new sap.ecf.controls.Interaction();

    widgetInteraction.placeAt("interactions");

    //Create a configuration object
    var ecfInitConfig = new sap.ecf.models.InitConfig();
    // Set Anonymous "Visitor" authentication
    ecfInitConfig.setAuthenticationType("Anonymous");
    //Set the URL for SAP ECF Server
    ecfInitConfig.setCctrUrl(parent.CHAT_CONFIG.cctrUrl);
    //Set language
    ecfInitConfig.setLocale(parent.CHAT_CONFIG.currentLanguage);

    //Session object - core
    jQuery.sap.require("sap.ecf.core.Session");

    var ecfSession = sap.ecf.core.Session.getInstance();
    function ecfConnect() {
        $(document).ready(function() {
            ecfSession.connect(ecfInitConfig);
        });
    };

    ecfConnect();

    // When the Session is connected enable the form controls
    ecfSession.attachConnected(function(evt) {
        startChat(evt);
    });

    // Wire up button press

    // Handlers to start the chat
    function startChat(evt) {

    ecfSession.initiateChat({
        chat_address: parent.CHAT_CONFIG.customerName,
        alias : parent.CHAT_CONFIG.customerName,
        queue : parent.CHAT_CONFIG.queue,
        subChannelType : chatType
    });

    };

    // Handler for when Chat is queued to visitor
    widgetInteraction.attachQueuedInteraction(function(evt) {
        // Disable form elements because we are already in a chat
        var oInteraction = evt.getParameter("oInteraction");
        var oTab = widgetInteraction.getInteractionTabByID(oInteraction.id);
    });

    // Handler for when Chat ends
    widgetInteraction.attachRemovingInteraction(function(evt) {
        // Destroy the Session connection to ECFS
        parent.closeTextChat();
        ecfSession.disconnect();
    });

    // When the Session is destroyed, create a new Session and connect again
    ecfSession.attachDisconnected(function(evt) {
        if(chatType === 'text') {
            parent.closeTextChat();
        } else {
            parent.closeVideoChat();
        }
    });

        };
</script>

<div >
    <div id="interactions"></div>
</div>
