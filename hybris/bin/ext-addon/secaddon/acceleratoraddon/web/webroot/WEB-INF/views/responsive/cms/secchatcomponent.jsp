<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

<%-- JS configuration --%>
<script type="text/javascript">
    var CHAT_CONFIG = {};
    CHAT_CONFIG.customerName = '${customerName}';
    CHAT_CONFIG.customerEmail = '${customerEmail}';
    CHAT_CONFIG.controllerPath = '${chatControllerPath}';
    CHAT_CONFIG.queue = '${chatQueue}';
    CHAT_CONFIG.modulePath = '${chatEcfModulePath}';
    CHAT_CONFIG.cctrUrl = '${chatCctrUrl}';
    CHAT_CONFIG.bootstrapUrl = '${chatBootstrapUrl}';
    CHAT_CONFIG.currentLanguage = '${currentLanguage}';

    var isChrome = !!window.chrome && !!window.chrome.webstore;
    var isFirefox = typeof InstallTrigger !== 'undefined';
</script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<script>
    $(document).ready(function () {
        if(isFirefox || isChrome) {
            prepareVideoChat();
        } else {
            $("#videoChatBtn1").hide();
        }
        prepareTextChat();
    });

    function prepareTextChat() {
        $("#text-chat-wrapper").dialog({
            'title': '${textChatTitle}',
            autoOpen: false,
            height: '500',
            width: '590',
            resizable: false,
            scrollable: "no",
            closeOnEscape: false,
            position: { my: "center top", at: "center bottom", of: $(".js_navigation--bottom") },
            close: function(event, ui) {
                $("#textChat").attr("src", "");
                enableButtons();
            }
        });
        $(".open-text-chat-popup").click(function (e) {
            e.preventDefault();
            $("#textChat").attr("src", "https://" + location.host + CHAT_CONFIG.controllerPath + "?type=text");
            disableButtons();
            $("#text-chat-wrapper").dialog('open');
        });
    };

    function prepareVideoChat() {
        $("#video-chat-wrapper").dialog({
            'title': '${videoChatTitle}',
            autoOpen: false,
            height: '700',
            width: '790',
            resizable: false,
            scrollable: "no",
            closeOnEscape: false,
            position: { my: "center top", at: "center bottom", of: $(".js_navigation--bottom") },
            close: function(event, ui) {
                $("#videoChat").attr("src", "");
                enableButtons();
            }
        });
        $(".open-video-chat-popup").click(function (e) {
            e.preventDefault();
            $("#videoChat").attr("src", "https://" + location.host + CHAT_CONFIG.controllerPath + "?type=video");
            disableButtons();
            $("#video-chat-wrapper").dialog('open');
        });
    };

    function closeTextChat() {
        $("#text-chat-wrapper").dialog('close');
    };

    function closeVideoChat() {
        $("#video-chat-wrapper").dialog('close');
    };

    function disableButtons() {
        $("#textChatBtn1").attr('disabled', true);
        $("#videoChatBtn1").attr('disabled', true);
    }

    function enableButtons() {
        $("#textChatBtn1").removeAttr('disabled');
        $("#videoChatBtn1").removeAttr('disabled');
    }

</script>

<div class="secchat-wrapper">
    <a class="open-text-chat-popup btn btn-default secchat-btn" role="button" id="textChatBtn1">${textChatTitle}</a>
    <c:if test="${videoChatEnabled}">
        <a class="open-video-chat-popup btn btn-default secchat-btn" class="btn btn-default" role="button" id="videoChatBtn1">${videoChatTitle}</a>
    </c:if>
</div>


<div id="text-chat-wrapper">
    <div id="text-chat-popup">
        <iframe id="textChat" src="" scrolling="no" frameborder="0"  style="overflow:hidden; display:block; position: absolute; height: 95%; width: 97%"></iframe>
    </div>
</div>

<div id="video-chat-wrapper">
    <div id="video-chat-popup">
        <iframe id="videoChat" src="" scrolling="no" frameborder="0"  style="overflow:hidden; display:block; position: absolute; height: 95%; width: 97%"></iframe>
    </div>
</div>
