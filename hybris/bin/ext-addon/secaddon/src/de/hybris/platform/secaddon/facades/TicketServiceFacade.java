/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.secaddon.facades;

import de.hybris.platform.commerceservices.search.pagedata.PageableData;
import de.hybris.platform.commerceservices.search.pagedata.SearchPageData;
import de.hybris.platform.secaddon.data.*;

import java.util.List;

import com.hybris.charon.RawResponse;


/**
 * Facade for our TicketServiceClient
 */
public interface TicketServiceFacade
{
	TicketData getTicketDetails(String ticketId);

	SearchPageData<TicketData> getTickets(PageableData pageableData);

	RawResponse createTicket(TicketData ticketData);

	RawResponse addMessage(String ticketId,Transcript transcript);

	List<TicketType> getTicketTypes();
}
