/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
ACC.customerCoupon = {
	_autoload : ['onCouponReadMoreClick' , 'autoCompleteCouponList', 'swicthNotification'],

	handleApplyVoucher : function(e) {
		var voucherCode = $.trim($("#js-voucher-code-text").val());
		if (voucherCode != '' && voucherCode.length > 0) {
			$("#applyVoucherForm").submit();
		}
	},

	onCouponReadMoreClick : function() {
		$(".js-coupon-read-more").click(function() {
			var title = $(this).data("colorbox-title");
			var index = $(this).data("index");

			$("#coupon-bell-popup-" + index).removeClass();
			$("#coupon-bell-popup-" + index).addClass($("#coupon-bell-" + index).attr("class"));
			ACC.common.checkAuthenticationStatusBeforeAction(function() {
				ACC.colorbox.open(title, {

					html : $("#coupon-popup-content-" + index).html(),
					maxWidth : "100%",
					width : "450px",
					initialWidth : "450px"
				})
			});
		});
	},
	
	autoCompleteCouponList : function() {
		function split(val) {
			return val.split(/,\s*/);
		}
		function extractLast(term) {
			return split(term).pop();
		}
		var cache = {};
		$("#js-voucher-code-text").autocomplete({
			minLength: 0,
			width: 50,
			delay: 0,
			source: function( request, response ){
					var term = request.term;
					if (cache && term in cache)
					{
						return response(cache[term]);
					}
				 $.ajax({
						url : ACC.config.encodedContextPath + '/cart/effectivecoupons',
						type : 'GET',
						async : false,
						success : function(data){
							var autoSearchData = [];
							var autoSearchNoData = [];
							$.each(data, function (i, obj)
									{
										autoSearchData.push({
											value: obj.couponId,
											label: obj.name + " (" +obj.couponCode +")",
											type: "autoSuggestion"
										});
									});
							autoSearchNoData.push(
									{
										value: "",
										label: ACC.addons.customercouponaddon['customer.coupon.nocustomercoupon'],
									});
							if (autoSearchData.length == 0){
								cache[term] = autoSearchNoData;
								return response(autoSearchNoData);
							}
							cache[term] = $.ui.autocomplete.filter(autoSearchData, extractLast( request.term ) ) ;
								return response(cache[term]);
						}
					});
			},
		      select: function( event, ui, e ) {
		    	$( "#js-voucher-code-text" ).val( ui.item.value );
		    	ACC.customerCoupon.handleApplyVoucher(e);
		    }
		
		}).focus(function () {
            $(this).autocomplete("search");
		}).autocomplete("widget").addClass("coupon-autocomplete-scroll");
		
	},
	
	swicthNotification : function(){
		$('input[name="my-checkbox"]').bootstrapSwitch({  
	        onText:"ON",  
	        offText:"OFF",  
	        onColor:"success",  
	        offColor:"default",  
	        size:"small",
	        onInit:function(event,state){
	        	$(".coupon-noti-switch").removeClass("coupon-switch-hide");
	        },
	        onSwitchChange:function(event,state){  
	        	$('input[name="my-checkbox"]').bootstrapSwitch('readonly',true);
				var notificationOn = $(this).attr("notification");
				var couponCode = $(this).data("coupon-code");
				var rmUrl = ACC.config.encodedContextPath + "/my-account/coupon/notification/"+couponCode;
				$.get(rmUrl, {isNotificationOn:notificationOn}, function(data) {
					if (data == "success") {
						$('input[name="my-checkbox"]').bootstrapSwitch('readonly',false);
					}
				});
	        }  
	    })  ;
	}
	
}

ACC.customer360 = {
		
	loadCoupons : function(params){
		$('#customer-coupons').hide();
		$('#long-load-example').show();
		$.ajax({
			type : "GET",
			data : params,
			url : ACC.config.encodedContextPath + "/asm/customer-coupons",
			success : function(data) {
				$('#customer-coupons').html(data);
				$('#long-load-example').hide();
				$('#customer-coupons').show();
				ACC.customer360.resetPageScroll();
			},
			error : function(){
				$('#long-load-example').hide();
				$('#customer-coupons').html('Get customer coupons error!');
				console.log('Get customer coupons error!');
			}
		});
	},
		
	switchCouponTab : function() {
		$('.menu_customer_coupons li').click(function(e) {
			if(!$(this).hasClass('nav-tabs-mobile-caret')){
				var type = $(this).attr('value');
				ACC.customer360.loadCoupons({type : type, text : ''});
			}
			$('#search-input')[0].value = '';
		});
	},
	
	handleCouponAction : function(){
		$('.js-coupons-action').click(function(){
			var action = $(this).data('action');
			var code = $(this).attr('id');
			var url = $(this).data('url');
			$.ajax({
				type : "POST",
				data : {action : action},
				url : url,
				success : function(data){
					if(data){
						ACC.customer360.loadCoupons({
							type : $('.menu_customer_coupons .active').attr('value'), 
							text : $('#search-input')[0].value
						});
					}
				},
				error : function(data){
					console.log(action + ' coupon[' + code + '] error!');
					$('#customer-coupons').html(action + ' coupon[' + code + '] error!');
				}
			});
		});
	},
	
	searchHandler : function(){
		var type = $('.menu_customer_coupons .active').attr('value');
		var text = $('#search-input')[0].value;
		ACC.customer360.loadCoupons({type : type, text : text});
	},
	
	handleSearch : function(){
		$('#coupons-search-btn').click(function(){
			ACC.customer360.searchHandler();
		});
		
		$('#search-input').keydown(function(e){
			if(e.keyCode == 13){
				ACC.customer360.searchHandler();
			}
		});
	},
	
	resetPageScroll : function(){
		var offset = $('body').scrollTop();
		$('body').scrollTop(0);
		$.colorbox.resize();
		$('body').scrollTop(offset);
	}
};




$(window).resize(function( ) {
	$("#js-voucher-code-text").autocomplete( "close" );
	$("#js-voucher-code-text").autocomplete( "search");
});