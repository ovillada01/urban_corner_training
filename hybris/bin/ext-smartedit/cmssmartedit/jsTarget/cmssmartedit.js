/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('cmssmartedit', [
    'resourceLocationsModule',
    'decoratorServiceModule',
    'contextualMenuServiceModule',
    'removeComponentServiceModule',
    'experienceInterceptorModule',
    'editorEnablerServiceModule',
    'alertServiceModule',
    'translationServiceModule',
    'featureServiceModule',
    'slotVisibilityButtonModule',
    'slotVisibilityServiceModule',
    'cmssmarteditTemplates',
    'cmscommonsTemplates',
    'componentHandlerServiceModule',
    'assetsServiceModule',
    'slotSharedButtonModule',
    'cmsDragAndDropServiceModule',
    'syncIndicatorDecoratorModule',
    'slotSyncButtonModule',
    'synchronizationPollingServiceModule',
    'confirmationModalServiceModule',
    'sharedSlotDisabledDecoratorModule',
    'externalSlotDisabledDecoratorModule',
    'slotRestrictionsServiceModule',
    'slotSharedServiceModule',
    'contextualMenuDropdownServiceModule',
    'externalComponentDecoratorModule',
    'externalComponentButtonModule'
])

.run(
    // Note: only instances can be injected in a run function
    ['$rootScope', '$q', '$translate', 'alertService', 'assetsService', 'cmsDragAndDropService', 'componentHandlerService', 'confirmationModalService', 'contextualMenuService', 'decoratorService', 'editorEnablerService', 'featureService', 'removeComponentService', 'slotRestrictionsService', 'slotSharedService', 'slotVisibilityService', function($rootScope, $q, $translate,
        alertService,
        assetsService,
        cmsDragAndDropService,
        componentHandlerService,
        confirmationModalService,
        contextualMenuService,
        decoratorService,
        editorEnablerService,
        featureService,
        removeComponentService,
        slotRestrictionsService,
        slotSharedService,
        slotVisibilityService) {

        editorEnablerService.enableForComponents(['^.*Component$']);

        decoratorService.addMappings({
            '^((?!Slot).)*$': ['se.contextualMenu', 'externalComponentDecorator'],
            '^.*Slot$': ['se.slotContextualMenu', 'se.basicSlotContextualMenu', 'syncIndicator', 'sharedSlotDisabledDecorator', 'externalSlotDisabledDecorator']
        });

        featureService.addContextualMenuButton({
            key: 'externalcomponentbutton',
            nameI18nKey: 'se.cms.contextmenu.title.externalcomponent',
            i18nKey: 'se.cms.contextmenu.title.externalcomponentbutton',
            regexpKeys: ['^((?!Slot).)*$'],
            condition: function(configuration) {
                return componentHandlerService.isExternalComponent(configuration.componentId, configuration.componentType);
            },
            callbacks: {},
            action: {
                template: '<external-component-button data-catalog-version-uuid="ctrl.componentAttributes.smarteditCatalogVersionUuid"></external-component-button>'
            },
            callback: function() {},
            displayClass: 'externalcomponentbutton',
            displayIconClass: 'hyicon hyicon-globe',
            displaySmallIconClass: 'hyicon hyicon-globe'
        });

        featureService.addContextualMenuButton({
            key: 'se.cms.dragandropbutton',
            nameI18nKey: 'se.cms.contextmenu.title.dragndrop',
            i18nKey: 'se.cms.contextmenu.title.dragndrop',
            regexpKeys: ['^((?!Slot).)*$'],
            condition: function(configuration) {
                var slotId = componentHandlerService.getParentSlotForComponent(configuration.element);
                return slotRestrictionsService.isSlotEditable(slotId);
            },
            callback: function() {},
            callbacks: {
                mousedown: function() {
                    cmsDragAndDropService.update();
                }
            },
            displayClass: 'movebutton',
            displayIconClass: 'hyicon hyicon-dragdroplg',
            displaySmallIconClass: 'hyicon hyicon-dragdroplg',
            permissions: ['se.context.menu.drag.and.drop.component']
        });

        featureService.register({
            key: 'se.cms.html5DragAndDrop',
            nameI18nKey: 'se.cms.dragAndDrop.name',
            descriptionI18nKey: 'se.cms.dragAndDrop.description',
            enablingCallback: function() {
                cmsDragAndDropService.register();
                cmsDragAndDropService.apply();
            },
            disablingCallback: function() {
                cmsDragAndDropService.unregister();
            }
        });

        featureService.addContextualMenuButton({
            key: 'se.cms.remove',
            i18nKey: 'se.cms.contextmenu.title.remove',
            nameI18nKey: 'se.cms.contextmenu.title.remove',
            regexpKeys: ['^((?!Slot).)*$'],
            condition: function(configuration) {
                var slotId = componentHandlerService.getParentSlotForComponent(configuration.element);
                return slotRestrictionsService.isSlotEditable(slotId);
            },
            callback: function(configuration, $event) {
                var slotOperationRelatedId = componentHandlerService.getSlotOperationRelatedId(configuration.element);
                var slotOperationRelatedType = componentHandlerService.getSlotOperationRelatedType(configuration.element);

                var message = {};
                message.description = "se.cms.contextmenu.removecomponent.confirmation.message";
                message.title = "se.cms.contextmenu.removecomponent.confirmation.title";

                confirmationModalService.confirm(message).then(function() {
                    removeComponentService.removeComponent({
                        slotId: configuration.slotId,
                        componentId: configuration.componentId,
                        componentType: configuration.componentType,
                        slotOperationRelatedId: slotOperationRelatedId,
                        slotOperationRelatedType: slotOperationRelatedType,
                    }).then(
                        function() {
                            slotVisibilityService.reloadSlotsInfo();
                            $translate('se.cms.alert.component.removed.from.slot', {
                                componentID: slotOperationRelatedId,
                                slotID: configuration.slotId
                            }).then(function(translation) {
                                alertService.showSuccess({
                                    message: translation
                                });
                                $event.preventDefault();
                                $event.stopPropagation();
                            });
                        });
                });
            },
            displayClass: 'removebutton',
            displayIconClass: 'hyicon hyicon-removelg',
            displaySmallIconClass: 'hyicon hyicon-removelg',
            permissions: ['se.context.menu.remove.component']
        });

        featureService.addContextualMenuButton({
            key: 'se.slotContextualMenuVisibility',
            nameI18nKey: 'slotcontextmenu.title.visibility',
            regexpKeys: ['^.*ContentSlot$'],
            callback: function() {},
            templateUrl: 'slotVisibilityWidgetTemplate.html',
            permissions: ['se.slot.context.menu.visibility']
        });

        featureService.addContextualMenuButton({
            key: 'se.slotSharedButton',
            nameI18nKey: 'slotcontextmenu.title.shared.button',
            regexpKeys: ['^.*Slot$'],
            callback: function() {},
            templateUrl: 'slotSharedTemplate.html',
            permissions: ['se.slot.context.menu.shared.icon']
        });

        featureService.addContextualMenuButton({
            key: 'se.slotSyncButton',
            nameI18nKey: 'slotcontextmenu.title.sync.button',
            regexpKeys: ['^.*Slot$'],
            callback: function() {},
            templateUrl: 'slotSyncTemplate.html',
            permissions: ['se.sync.slot.context.menu']
        });

        featureService.addDecorator({
            key: 'syncIndicator',
            nameI18nKey: 'syncIndicator',
            permissions: ['se.sync.slot.indicator']
        });

        featureService.register({
            key: 'disableSharedSlotEditing',
            nameI18nKey: 'se.cms.disableSharedSlotEditing',
            descriptionI18nKey: 'se.cms.disableSharedSlotEditing.description',
            enablingCallback: function() {
                slotSharedService.setSharedSlotEnablementStatus(true);
            },
            disablingCallback: function() {
                slotSharedService.setSharedSlotEnablementStatus(false);
            }
        });

        featureService.addDecorator({
            key: 'sharedSlotDisabledDecorator',
            nameI18nKey: 'se.cms.shared.slot.disabled.decorator',
            displayCondition: function(componentType, componentId) {
                return slotRestrictionsService.isSlotEditable(componentId).then(function(isEditable) {
                    return !isEditable;
                });
            }
        });

        featureService.addDecorator({
            key: 'externalSlotDisabledDecorator',
            nameI18nKey: 'se.cms.external.slot.disabled.decorator',
            displayCondition: function(componentType, componentId) {
                return $q.when(componentHandlerService.isExternalComponent(componentId, componentType));
            }
        });

        featureService.addDecorator({
            key: 'externalComponentDecorator',
            nameI18nKey: 'se.cms.external.component.decorator',
            displayCondition: function(componentType, componentId) {
                return $q.when(componentHandlerService.isExternalComponent(componentId, componentType));
            }
        });

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('externalComponentButtonModule', ['l10nModule', 'catalogServiceModule'])
    .controller('externalComponentButtonController', ['l10nFilter', 'catalogService', function(l10nFilter, catalogService) {

        this.$onInit = function() {
            this.isReady = false;

            return catalogService.getCatalogVersionByUuid(this.catalogVersionUuid).then(function(catalogVersion) {
                this.catalogVersion = l10nFilter(catalogVersion.catalogName) + ' (' + catalogVersion.version + ')';
                this.isReady = true;
            }.bind(this));

        };

    }])
    .component('externalComponentButton', {
        templateUrl: 'externalComponentButtonTemplate.html',
        controller: 'externalComponentButtonController',
        controllerAs: 'ctrl',
        bindings: {
            catalogVersionUuid: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('externalComponentDecoratorModule', ['componentHandlerServiceModule'])
    .controller('externalComponentDecoratorController', ['$element', 'CONTENT_SLOT_TYPE', 'componentHandlerService', function($element, CONTENT_SLOT_TYPE, componentHandlerService) {

        this.$onInit = function() {

            var parentSlotIdForComponent = componentHandlerService.getParentSlotForComponent($element);
            this.isExtenalSlot = componentHandlerService.isExternalComponent(parentSlotIdForComponent, CONTENT_SLOT_TYPE);

        };

    }])
    .directive('externalComponentDecorator', function() {
        return {
            templateUrl: 'externalComponentDecoratorTemplate.html',
            restrict: 'C',
            transclude: true,
            replace: false,
            controller: 'externalComponentDecoratorController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '=',
                componentAttributes: '<'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotSharedButtonModule', ['slotSharedServiceModule', 'translationServiceModule'])
    .controller('slotSharedButtonController', ['slotSharedService', function(slotSharedService) {
        this.slotSharedFlag = false;
        this.buttonName = 'slotSharedButton';

        this.$onInit = function() {
            slotSharedService.isSlotShared(this.slotId).then(function(result) {
                this.slotSharedFlag = result;
            }.bind(this));
        };
    }])
    .directive('slotSharedButton', function() {
        return {
            templateUrl: 'slotSharedButtonTemplate.html',
            restrict: 'E',
            controller: 'slotSharedButtonController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '=',
                slotId: '@'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('externalSlotDisabledDecoratorModule', ['slotDisabledDecoratorModule'])
    .directive('externalSlotDisabledDecorator', function() {
        return {
            templateUrl: 'externalSlotDisabledDecoratorTemplate.html',
            restrict: 'C',
            controllerAs: 'ctrl',
            controller: function() {},
            scope: {},
            bindToController: {
                active: '=',
                componentAttributes: '<'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('sharedSlotDisabledDecoratorModule', ['slotDisabledDecoratorModule'])
    .directive('sharedSlotDisabledDecorator', function() {
        return {
            templateUrl: 'sharedSlotDisabledDecoratorTemplate.html',
            restrict: 'C',
            controllerAs: 'ctrl',
            controller: function() {},
            scope: {},
            bindToController: {
                active: '=',
                componentAttributes: '<'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotDisabledDecoratorModule', ['componentHandlerServiceModule', 'catalogServiceModule', 'yPopoverModule', 'l10nModule'])
    .controller('slotDisabledComponentController', ['$translate', 'componentHandlerService', 'catalogService', 'l10nFilter', function($translate, componentHandlerService, catalogService, l10nFilter) {

        var DEFAULT_DECORATOR_MSG = "se.cms.sharedslot.decorator.label";
        var EXTERNAL_SLOT_DECORATOR_MSG = "se.cms.externalsharedslot.decorator.label";
        var ICONS_CLASSES = {
            GLOBE: 'hyicon-globe',
            LOCK: 'hyicon-lock',
            HOVERED: 'hyicon-linked'
        };

        this.$onInit = function() {
            this._checkIfSlotIsInherited();
            this._getSourceCatalogName();
        };

        this._checkIfSlotIsInherited = function() {
            var componentCatalogVersion = this.componentAttributes.smarteditCatalogVersionUuid;
            var pageCatalogVersion = componentHandlerService.getCatalogVersionUUIDFromPage();

            this.isExternalSlot = (componentCatalogVersion !== pageCatalogVersion);
        };

        this._getSourceCatalogName = function() {
            var catalogVersionUUID = this.componentAttributes.smarteditCatalogVersionUuid;
            catalogService.getCatalogVersionByUuid(catalogVersionUUID).then(function(catalogVersion) {
                this.catalogName = catalogVersion.catalogName;
            }.bind(this));
        };

        this.getPopoverMessage = function() {
            var msgToLocalize = (this.isExternalSlot) ? EXTERNAL_SLOT_DECORATOR_MSG : DEFAULT_DECORATOR_MSG;
            var msgParams = {
                catalogName: l10nFilter(this.catalogName),
                slotId: this.componentAttributes.smarteditComponentId
            };

            return $translate.instant(msgToLocalize, msgParams);
        };

        this.getSlotIconClass = function() {
            var iconClass = "";
            if (this.active) {
                iconClass = ICONS_CLASSES.HOVERED;
            } else if (this.isExternalSlot !== undefined) {
                iconClass = (this.isExternalSlot) ? ICONS_CLASSES.GLOBE : ICONS_CLASSES.LOCK;
            }

            return iconClass;
        };

        this.getOuterSlotClass = function() {
            return this.getSlotIconClass() === ICONS_CLASSES.GLOBE ? 'disabled-shared-slot__icon--outer-globe' : '';
        }.bind(this);
    }])
    .component('slotDisabledComponent', {
        templateUrl: 'slotDisabledTemplate.html',
        controller: 'slotDisabledComponentController',
        controllerAs: 'ctrl',
        bindings: {
            active: '=',
            componentAttributes: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotVisibilityButtonModule
 * @description
 *
 * The slot visibility button module provides a directive and controller to manage the button within the slot contextual menu 
 * and the hidden component list, which is also part of the dropdown menu associated with the directive's template.     
 */
angular.module('slotVisibilityButtonModule', ['slotVisibilityServiceModule', 'slotVisibilityComponentModule', 'sharedDataServiceModule'])

/**
 * @ngdoc controller
 * @name slotVisibilityButtonModule.controller:slotVisibilityButtonController
 *
 * @description
 * The slot visibility button controller is responsible for enabling and disabling the hidden components button, 
 * as well as displaying the hidden components list. It also provides functions to open and close the hidden component list.
 *
 * @param {Object} slotVisibilityService slot visibility service instance
 * @param {Object} $scope current scope instance
 */
.controller('slotVisibilityButtonController', ['slotVisibilityService', '$scope', 'sharedDataService', '$translate', function(slotVisibilityService, $scope, sharedDataService, $translate) {
        this.buttonName = 'slotVisibilityButton';
        this.eyeOnImageUrl = '/cmssmartedit/images/visibility_slot_menu_on.png';
        this.eyeOffImageUrl = '/cmssmartedit/images/visibility_slot_menu_off.png';
        this.eyeImageUrl = this.eyeOffImageUrl;
        this.closeImageUrl = '/cmssmartedit/images/close_button.png';
        this.buttonVisible = false;
        this.hiddenComponents = [];
        this.isComponentListOpened = false;

        $scope.$watch('ctrl.isComponentListOpened', function(newValue, oldValue) {
            this.eyeImageUrl = (newValue ? this.eyeOnImageUrl : this.eyeOffImageUrl);
            if (newValue !== oldValue) {
                this.setRemainOpen({
                    button: this.buttonName,
                    remainOpen: this.isComponentListOpened
                });
            }
        }.bind(this));

        this.markExternalComponents = function(experience, hiddenComponents) {
            hiddenComponents.forEach(function(hiddenComponent) {
                hiddenComponent.isExternal = hiddenComponent.catalogVersion !== experience.pageContext.catalogVersionUuid;
            });
        };

        this.getTemplateInfoForExternalComponent = function() {
            return "<div class='slot-visiblity-component--popover'>" + $translate.instant('se.cms.slotvisibility.external.component') + "</div>";
        };

        this.$onInit = function() {
            slotVisibilityService.getHiddenComponents(this.slotId).then(function(hiddenComponents) {
                sharedDataService.get('experience').then(function(experience) {
                    this.hiddenComponents = hiddenComponents;
                    this.markExternalComponents(experience, this.hiddenComponents);
                    this.hiddenComponentCount = hiddenComponents.length;
                    if (this.hiddenComponentCount > 0) {
                        this.buttonVisible = true;
                    }
                }.bind(this));
            }.bind(this));
        };

    }])
    /**
     * @ngdoc directive
     * @name slotVisibilityButtonModule.directive:slotVisibilityButton
     *
     * @description
     * The slot visibility button component is used inside the slot contextual menu and provides a button 
     * image that displays the number of hidden components, as well as a dropdown menu of hidden component.
     *
     * The directive expects that the parent, the slot contextual menu, has a setRemainOpen function and a 
     * slotId value on the parent's scope. setRemainOpen is used to send a command to the parent to leave 
     * the slot contextual menu open.
     */
    .component('slotVisibilityButton', {
        templateUrl: 'slotVisibilityButtonTemplate.html',
        transclude: true,
        controller: 'slotVisibilityButtonController',
        controllerAs: 'ctrl',
        bindings: {
            setRemainOpen: '&',
            slotId: '@',
            initButton: '@'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotVisibilityComponentModule
 *
 * @description
 * The slot visibility component module provides a directive and controller to display the hidden components of a specified content slot.
 *
 * @requires componentVisibilityAlertServiceModule
 * @requires editorModalServiceModule
 */
angular.module('slotVisibilityComponentModule', [
    "componentVisibilityAlertServiceModule",
    "editorModalServiceModule"
])

.controller('slotVisibilityComponentController', ['componentVisibilityAlertService', 'editorModalService', function(
    componentVisibilityAlertService,
    editorModalService
) {

    this.imageRoot = '/cmssmartedit/images';

    this.openEditorModal = function() {
        editorModalService.openAndRerenderSlot(
            this.component.typeCode,
            this.component.uuid,
            this.slotId,
            "visibilityTab"
        ).then(function(item) {
            componentVisibilityAlertService.checkAndAlertOnComponentVisibility({
                itemId: item.uuid,
                itemType: item.itemtype,
                catalogVersion: item.catalogVersion,
                restricted: item.restricted,
                slotId: this.slotId,
                visible: item.visible
            });
        }.bind(this));
    };

    this.$onInit = function() {
        this.componentVisbilitySwitch = this.component.visible ? 'se.cms.component.visibility.status.on' : 'se.cms.component.visibility.status.off';
        this.componentRestrictionsCount = '(' + (this.component.restrictions ? this.component.restrictions.length : 0) + ')';
    };
}])

/**
 * @ngdoc directive
 * @name slotVisibilityComponentModule.directive:slotVisibilityComponent
 *
 * @description
 * The slot visibility component directive is used to display information about a specified hidden component.
 * It receives the component on its scope and it binds it to its own controller.
 */
.component('slotVisibilityComponent', {
    templateUrl: 'slotVisibilityComponentTemplate.html',
    transclude: false,
    controller: 'slotVisibilityComponentController',
    controllerAs: 'ctrl',
    bindings: {
        component: '=',
        slotId: '@'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotSyncButtonModule', ['catalogServiceModule', 'slotSynchronizationPanelModule', 'slotSynchronizationServiceModule', 'componentHandlerServiceModule', 'crossFrameEventServiceModule'])
    .controller('slotSyncButtonController', ['$scope', 'SYNCHRONIZATION_POLLING', 'SYNCHRONIZATION_STATUSES', 'catalogService', 'slotSynchronizationService', 'componentHandlerService', 'crossFrameEventService', function($scope, SYNCHRONIZATION_POLLING, SYNCHRONIZATION_STATUSES, catalogService, slotSynchronizationService, componentHandlerService, crossFrameEventService) {

        catalogService.isContentCatalogVersionNonActive().then(function(isContentCatalogVersionNonActive) {
            this.isContentCatalogVersionNonActive = isContentCatalogVersionNonActive;
        }.bind(this));

        this.buttonName = 'slotSyncButton';
        this.isPopupOpened = false;

        $scope.$watch('ctrl.isPopupOpened', function() {
            this.setRemainOpen({
                button: this.buttonName,
                remainOpen: this.isPopupOpened
            });
        }.bind(this));

        this.getSyncStatus = function() {
            var pageUUID = componentHandlerService.getPageUUID();
            slotSynchronizationService.getSyncStatus(pageUUID, this.slotId).then(function(syncStatus) {
                this.isSlotInSync = syncStatus.status && syncStatus.status === SYNCHRONIZATION_STATUSES.IN_SYNC ? true : false;
            }.bind(this));
        }.bind(this);

        this.updateStatus = function(evenId, syncStatus) {
            var slotSyncStatus = (syncStatus.selectedDependencies || []).concat(syncStatus.sharedDependencies || []).find(function(slot) {
                return slot.itemId === this.slotId;
            }.bind(this)) || {};
            this.isSlotInSync = slotSyncStatus.status && slotSyncStatus.status === SYNCHRONIZATION_STATUSES.IN_SYNC ? true : false;
        };

        //var updateStatusCallback = this.updateStatus.bind(this);
        var updateStatusCallback = this.getSyncStatus;

        this.$onInit = function() {
            this.isSlotInSync = true;
            this.getSyncStatus();
            this.unRegisterSyncPolling = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, updateStatusCallback);
        };

        this.$onDestroy = function() {
            this.unRegisterSyncPolling();
        };

    }])
    .component('slotSyncButton', {
        templateUrl: 'slotSyncButtonTemplate.html',
        controller: 'slotSyncButtonController',
        controllerAs: 'ctrl',
        bindings: {
            setRemainOpen: '&',
            slotId: '@'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotSynchronizationPanelModule', ['synchronizationPanelModule', 'slotSynchronizationServiceModule', 'componentHandlerServiceModule'])
    .controller('slotSynchronizationPanelController', ['slotSynchronizationService', 'componentHandlerService', function(slotSynchronizationService, componentHandlerService) {

        this.getSyncStatus = function() {
            var pageId = componentHandlerService.getPageUID();
            return slotSynchronizationService.getSyncStatus(pageId, this.slotId);
        }.bind(this);

        this.performSync = function(array) {
            return slotSynchronizationService.performSync(array);
        };

    }])
    .component('slotSynchronizationPanel', {
        templateUrl: 'slotSynchronizationPanelTemplate.html',
        controller: 'slotSynchronizationPanelController',
        controllerAs: 'slotSync',
        bindings: {
            slotId: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('syncIndicatorDecoratorModule', ['catalogServiceModule', 'slotSynchronizationServiceModule', 'componentHandlerServiceModule', 'synchronizationConstantsModule', 'crossFrameEventServiceModule'])
    .controller('syncIndicatorController', ['$q', 'catalogService', 'slotSynchronizationService', 'crossFrameEventService', 'componentHandlerService', 'SYNCHRONIZATION_STATUSES', 'SYNCHRONIZATION_POLLING', function($q, catalogService, slotSynchronizationService, crossFrameEventService, componentHandlerService, SYNCHRONIZATION_STATUSES, SYNCHRONIZATION_POLLING) {

        this.isVersionNonActive = false;

        this.$onInit = function() {
            // initial sync status is set to unavailable until the first fetch
            this.syncStatus = {
                status: SYNCHRONIZATION_STATUSES.UNAVAILABLE
            };
            this.pageUUID = componentHandlerService.getPageUUID();

            this.unRegisterSyncPolling = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, this.fetchSyncStatus.bind(this));

            catalogService.isContentCatalogVersionNonActive().then(function(isNonActive) {
                this.isVersionNonActive = isNonActive;
                if (this.isVersionNonActive) {
                    this.fetchSyncStatus();
                }
            }.bind(this));
        };

        this.$onDestroy = function() {
            this.unRegisterSyncPolling();
        };

        this.fetchSyncStatus = function() {
            return this.isVersionNonActive ? slotSynchronizationService.getSyncStatus(this.pageUUID, this.componentAttributes.smarteditComponentId).then(function(response) {
                this.syncStatus = response;
            }.bind(this), function() {
                this.syncStatus.status = SYNCHRONIZATION_STATUSES.UNAVAILABLE;
            }.bind(this)) : $q.when();
        }.bind(this);

    }])
    .directive('syncIndicator', [function() {
        return {
            templateUrl: 'syncIndicatorDecoratorTemplate.html',
            restrict: 'C',
            transclude: true,
            replace: false,
            controller: 'syncIndicatorController',
            controllerAs: 'ctrl',
            bindToController: {
                active: '=',
                componentAttributes: '<'
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module("componentVisibilityAlertServiceModule", [
    "componentVisibilityAlertServiceInterfaceModule",
    "gatewayProxyModule"
])

.factory('componentVisibilityAlertService', ['ComponentVisibilityAlertServiceInterface', 'extend', 'gatewayProxy', function(
    ComponentVisibilityAlertServiceInterface,
    extend,
    gatewayProxy
) {

    var ComponentVisibilityAlertService = function() {
        this.gatewayId = 'ComponentVisibilityAlertService';
        gatewayProxy.initForService(this, ["checkAndAlertOnComponentVisibility"]);
    };

    ComponentVisibilityAlertService = extend(ComponentVisibilityAlertServiceInterface, ComponentVisibilityAlertService);

    return new ComponentVisibilityAlertService();

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name contextualMenuDropdownServiceModule.contextualMenuDropdownService
 * @description
 * contextualMenuDropdownService is an internal service that provides methods for interaction between
 * Drag and Drop Service and the Contextual Menu.
 */

angular.module('contextualMenuDropdownServiceModule', ['contextualMenuDecoratorModule', 'eventServiceModule'])
    .run(['contextualMenuDropdownService', function(contextualMenuDropdownService) {
        contextualMenuDropdownService._registerIsOpenEvent();
    }])

/**
 *  Note: The contextualMenuDropdownService functions as a glue between the Drag and Drop Service and the Contextual Menu.
 *  The service was created to solve the issue of closing any contextual menu that is open whenever a drag operation is started.
 *  It does so while keeping the DnD and Contextual Menu services decoupled.
 */
.service('contextualMenuDropdownService', ['systemEventService', 'CTX_MENU_DROPDOWN_IS_OPEN', 'CLOSE_CTX_MENU', 'DRAG_AND_DROP_EVENTS', function(systemEventService, CTX_MENU_DROPDOWN_IS_OPEN, CLOSE_CTX_MENU, DRAG_AND_DROP_EVENTS) {

    this._registerIsOpenEvent = function() {
        systemEventService.registerEventHandler(CTX_MENU_DROPDOWN_IS_OPEN, function() {
            this._registerDragStarted();
        }.bind(this));
    };

    this._registerDragStarted = function() {
        systemEventService.registerEventHandler(DRAG_AND_DROP_EVENTS.DRAG_STARTED, this._triggerCloseOperation);
    };

    this._triggerCloseOperation = function() {
        systemEventService.sendAsynchEvent(CLOSE_CTX_MENU);
        systemEventService.unRegisterEventHandler(DRAG_AND_DROP_EVENTS.DRAG_STARTED, this._triggerCloseOperation);
    }.bind(this);

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    /**
     * @ngdoc overview
     * @name cmsDragAndDropServiceModule
     * @description
     * # The cmsDragAndDropServiceModule
     *
     * The cmsDragAndDropServiceModule contains a service that provides a rich drag and drop experience tailored for CMS operations.
     *
     */
    angular.module('cmsDragAndDropServiceModule', ['yjqueryModule', 'dragAndDropServiceModule', 'alertServiceModule', 'yLoDashModule', 'componentHandlerServiceModule', 'eventServiceModule', 'gatewayFactoryModule', 'slotRestrictionsServiceModule', 'componentEditingFacadeModule', 'translationServiceModule', 'removeComponentServiceModule', 'assetsServiceModule', 'browserServiceModule'])
        /**
         * @ngdoc object
         * @name cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS
         *
         * @description
         * Injectable angular constant<br/>
         * Constants identifying CMS drag and drop events.
         *
         */
        .constant('DRAG_AND_DROP_EVENTS', {
            /**
             * @ngdoc property
             * @name DRAG_STARTED
             * @propertyOf cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS
             *
             * @description
             * Name of event executed when a drag and drop event starts.
             **/
            DRAG_STARTED: 'CMS_DRAG_STARTED',
            /**
             * @ngdoc property
             * @name DRAG_STOPPED
             * @propertyOf cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS
             *
             * @description
             * Name of event executed when a drag and drop event stops.
             **/
            DRAG_STOPPED: 'CMS_DRAG_STOPPED',
            /**
             * @ngdoc property
             * @name DRAG_STOPPED
             * @propertyOf cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS
             *
             * @description
             * Name of event executed when onDragOver is triggered.
             **/
            DRAG_OVER: 'CMS_DRAG_OVER',
            /**
             * @ngdoc property
             * @name DRAG_STOPPED
             * @propertyOf cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS
             *
             * @description
             * Name of event executed when onDragLeave is triggered.
             **/
            DRAG_LEAVE: 'CMS_DRAG_LEAVE'
        })
        /**
         * @ngdoc service
         * @name cmsDragAndDropServiceModule.service:cmsDragAndDropService
         *
         * @description
         * This service provides a rich drag and drop experience tailored for CMS operations.
         */
        .service('cmsDragAndDropService', ['$q', '$window', '$translate', '$timeout', 'yjQuery', 'lodash', 'dragAndDropService', 'componentHandlerService', 'systemEventService', 'gatewayFactory', 'slotRestrictionsService', 'alertService', 'assetsService', 'browserService', 'componentEditingFacade', 'DRAG_AND_DROP_EVENTS', 'OVERLAY_RERENDERED_EVENT', 'COMPONENT_REMOVED_EVENT', 'CONTENT_SLOT_TYPE', 'OVERLAY_RERENDERED_EVENT', 'SUPPORTED_BROWSERS', function($q, $window, $translate, $timeout, yjQuery, lodash, dragAndDropService, componentHandlerService, systemEventService, gatewayFactory, slotRestrictionsService, alertService, assetsService, browserService, componentEditingFacade, DRAG_AND_DROP_EVENTS, OVERLAY_RERENDERED_EVENT, COMPONENT_REMOVED_EVENT, CONTENT_SLOT_TYPE, OVERLAY_RERENDERED_EVENT, SUPPORTED_BROWSERS) {
            // Constants
            var CMS_DRAG_AND_DROP_ID = 'se.cms.dragAndDrop';

            var TARGET_SELECTOR = "#smarteditoverlay .smartEditComponentX[data-smartedit-component-type='ContentSlot']";
            var SOURCE_SELECTOR = "#smarteditoverlay .smartEditComponentX[data-smartedit-component-type!='ContentSlot'] .movebutton";
            var MORE_MENU_SOURCE_SELECTOR = ".movebutton";

            var SLOT_SELECTOR = ".smartEditComponentX[data-smartedit-component-type='ContentSlot']";
            var COMPONENT_SELECTOR = ".smartEditComponentX[data-smartedit-component-type!='ContentSlot']";
            var HINT_SELECTOR = '.overlayDropzone';

            var CSS_CLASSES = {
                UI_HELPER_OVERLAY: 'overlayDnd',
                DROPZONE: 'overlayDropzone',
                DROPZONE_FULL: 'overlayDropzone--full',
                DROPZONE_TOP: 'overlayDropzone--top',
                DROPZONE_BOTTOM: 'overlayDropzone--bottom',
                DROPZONE_LEFT: 'overlayDropzone--left',
                DROPZONE_RIGHT: 'overlayDropzone--right',
                DROPZONE_HOVERED: 'overlayDropzone--hovered',
                OVERLAY_IN_DRAG_DROP: 'smarteditoverlay_dndRendering',
                COMPONENT_DRAGGED: 'component_dragged',
                COMPONENT_DRAGGED_HOVERED: 'component_dragged_hovered',
                SLOTS_MARKED: 'slot-marked',
                SLOT_ALLOWED: 'over-slot-enabled',
                SLOT_NOT_ALLOWED: 'over-slot-disabled'
            };

            var DEFAULT_DRAG_IMG = '/images/contextualmenu_move_on.png';

            // Variables
            this._cachedSlots = {};
            this._highlightedSlot = null;
            this._highlightedComponent = null;
            this._highlightedHint = null;

            this._gateway = gatewayFactory.createGateway("cmsDragAndDrop");

            /**
             * @ngdoc method
             * @name cmsDragAndDropServiceModule.service:cmsDragAndDropService#register
             * @methodOf cmsDragAndDropServiceModule.service:cmsDragAndDropService
             *
             * @description
             * This method registers this drag and drop instance in SmartEdit.
             */
            this.register = function() {
                dragAndDropService.register({
                    id: CMS_DRAG_AND_DROP_ID,
                    sourceSelector: [SOURCE_SELECTOR, MORE_MENU_SOURCE_SELECTOR], //the source selectors are DnD menus located both inside and outside the more options of the overlay
                    targetSelector: TARGET_SELECTOR,
                    startCallback: this.onStart,
                    dragEnterCallback: this.onDragEnter,
                    dragOverCallback: this.onDragOver,
                    dropCallback: this.onDrop,
                    outCallback: this.onDragLeave,
                    stopCallback: this.onStop,
                    enableScrolling: true,
                    helper: this._getDragImageSrc
                });
            };

            /**
             * @ngdoc method
             * @name cmsDragAndDropServiceModule.service:cmsDragAndDropService#unregister
             * @methodOf cmsDragAndDropServiceModule.service:cmsDragAndDropService
             *
             * @description
             * This method unregisters this drag and drop instance from SmartEdit.
             */
            this.unregister = function() {
                dragAndDropService.unregister([CMS_DRAG_AND_DROP_ID]);
                slotRestrictionsService.emptyCache();

                systemEventService.unRegisterEventHandler(OVERLAY_RERENDERED_EVENT, this._onOverlayUpdate);
                systemEventService.unRegisterEventHandler(COMPONENT_REMOVED_EVENT, this._onOverlayUpdate);
            };

            /**
             * @ngdoc method
             * @name cmsDragAndDropServiceModule.service:cmsDragAndDropService#apply
             * @methodOf cmsDragAndDropServiceModule.service:cmsDragAndDropService
             *
             * @description
             * This method applies this drag and drop instance in the current page. After this method is executed,
             * the user can start a drag and drop operation.
             */
            this.apply = function() {
                dragAndDropService.apply(CMS_DRAG_AND_DROP_ID);
                this._addUIHelpers();

                // Register a listener for every time the overlay is updated.
                systemEventService.registerEventHandler(OVERLAY_RERENDERED_EVENT, this._onOverlayUpdate);
                systemEventService.registerEventHandler(COMPONENT_REMOVED_EVENT, this._onOverlayUpdate);

                this._gateway.subscribe(DRAG_AND_DROP_EVENTS.DRAG_STARTED, function(eventId, data) {
                    dragAndDropService.markDragStarted();
                    this._initializeDragOperation(data);
                }.bind(this));

                this._gateway.subscribe(DRAG_AND_DROP_EVENTS.DRAG_STOPPED, function() {
                    dragAndDropService.markDragStopped();
                    this._cleanDragOperation();
                }.bind(this));
            };

            /**
             * @ngdoc method
             * @name cmsDragAndDropServiceModule.service:cmsDragAndDropService#update
             * @methodOf cmsDragAndDropServiceModule.service:cmsDragAndDropService
             *
             * @description
             * This method updates this drag and drop instance in the current page. It is important to execute
             * this method every time a draggable or droppable element is added or removed from the page DOM.
             */
            this.update = function() {
                dragAndDropService.update(CMS_DRAG_AND_DROP_ID);

                // Add UI helpers -> They identify the places where you can drop components.
                this._addUIHelpers();
            };

            // Other Event Handlers
            this._onOverlayUpdate = function() {
                this.update();
                return $q.when();
            }.bind(this);

            // Drag and Drop Event Handlers
            this.onStart = function(event) {
                // Find element
                var targetElm = this._getSelector(event.target);
                var component = targetElm.closest(COMPONENT_SELECTOR);
                var slot = component.closest(SLOT_SELECTOR);

                // Here if the component evaluated above exits that means the component has been located and we can fetch its attributes 
                // else it is not located as the DnD option is hidden inside the more option of the contextual menu in which case 
                // we find the component/slot info by accessing attributes of the DnD icon.
                var componentId = component.length > 0 ? componentHandlerService.getSlotOperationRelatedId(component) : targetElm.attr('data-component-id');
                var componentUuid = component.length > 0 ? componentHandlerService.getSlotOperationRelatedUuid(component) : targetElm.attr('data-component-uuid');
                var componentType = component.length > 0 ? componentHandlerService.getSlotOperationRelatedType(component) : targetElm.attr('data-component-type');

                var slotId = component.length > 0 ? componentHandlerService.getId(slot) : targetElm.attr('data-slot-id');
                var slotUuid = component.length > 0 ? componentHandlerService.getId(slot) : targetElm.attr('data-slot-uuid');

                var dragInfo = {
                    componentId: componentId,
                    componentUuid: componentUuid,
                    componentType: componentType,
                    slotUuid: slotUuid,
                    slotId: slotId
                };
                component.addClass(CSS_CLASSES.COMPONENT_DRAGGED);

                this._initializeDragOperation(dragInfo);
            }.bind(this);

            this.onDragEnter = function(event) {
                this._highlightSlot(event);
            }.bind(this);

            this.onDragOver = function(event) {
                this._highlightSlot(event).then(function() {
                    if (!this._highlightedSlot || !this._highlightedSlot.isAllowed) {
                        return;
                    }

                    var slotId = componentHandlerService.getId(this._highlightedSlot.original);

                    // Check which component is highlighted
                    if (this._highlightedHint && this._isMouseInRegion(event, this._highlightedHint)) {
                        // If right hint is already highlighted don't do anything.
                        return;
                    } else if (this._highlightedHint) {
                        // Hint is not longer hovered.
                        this._clearHighlightedHint();
                    }

                    var cachedSlot = this._cachedSlots[slotId];
                    if (cachedSlot.components.length > 0) {
                        // Find the hovered component.
                        if (!this._highlightedComponent || !this._isMouseInRegion(event, this._highlightedComponent)) {
                            this._clearHighlightedComponent();

                            lodash.forEach(cachedSlot.components, function(component) {
                                if (this._isMouseInRegion(event, component)) {
                                    this._highlightedComponent = component;
                                    return false;
                                }
                            }.bind(this));
                        }

                        // Find the hint, if any, to highlight.
                        if (this._highlightedComponent) {
                            lodash.forEach(this._highlightedComponent.hints, function(hint) {
                                if (this._isMouseInRegion(event, hint)) {
                                    this._highlightedHint = hint;
                                    return false;
                                }
                            }.bind(this));
                        }
                    }

                    if (this._highlightedComponent && this._highlightedComponent.id === this._dragInfo.componentId) {
                        this._highlightedComponent.original.addClass(CSS_CLASSES.COMPONENT_DRAGGED_HOVERED);
                    } else if (this._highlightedHint) {
                        this._highlightedHint.original.addClass(CSS_CLASSES.DROPZONE_HOVERED);
                    }

                }.bind(this));

            }.bind(this);

            this.onDrop = function(event) {
                if (this._highlightedSlot) {
                    var sourceSlotId = this._dragInfo.slotId;
                    var targetSlotId = componentHandlerService.getId(this._highlightedSlot.original);
                    var targetSlotUUId = componentHandlerService.getUuid(this._highlightedSlot.original);
                    var sourceComponentId = this._dragInfo.componentId;
                    var componentType = this._dragInfo.componentType;

                    var catalogVersionUuid = componentHandlerService.getCatalogVersionUuid(this._highlightedSlot.original);

                    if (!this._highlightedSlot.isAllowed) {
                        var translation = $translate.instant("se.drag.and.drop.not.valid.component.type", {
                            slotUID: targetSlotId,
                            componentUID: sourceComponentId
                        });
                        alertService.showDanger({
                            message: translation,
                        });

                        return this.onStop(event);
                    }
                    if (this._highlightedHint || this._highlightedSlot.components.length === 0) {
                        var position = (this._highlightedHint) ? this._highlightedHint.position : 0;
                        var result;

                        if (!sourceSlotId) {
                            if (!sourceComponentId) {
                                var slotInfo = {
                                    targetSlotId: targetSlotId,
                                    targetSlotUUId: targetSlotUUId
                                };
                                result = componentEditingFacade.addNewComponentToSlot(slotInfo, catalogVersionUuid, componentType, position);
                            } else {
                                result = componentEditingFacade.addExistingComponentToSlot(targetSlotId, this._dragInfo, position);
                            }
                        } else {
                            if (sourceSlotId === targetSlotId) {
                                var currentComponentPos = this._getComponentPositionInSlot(sourceSlotId, sourceComponentId);
                                if (currentComponentPos < position) {
                                    // The current component will be removed from its current position, thus the target
                                    // position needs to take this into account. 
                                    position--;
                                }

                            }
                            result = componentEditingFacade.moveComponent(sourceSlotId, targetSlotId, sourceComponentId, position);
                        }

                        return result.then(function() {
                            this._scrollToModifiedSlot(targetSlotId);
                        }.bind(this), function() {
                            this.onStop(event);
                        }.bind(this));
                    }

                    return this.onStop(event);
                }
            }.bind(this);

            this.onDragLeave = function(event) {
                if (this._highlightedSlot) {
                    var slotId = componentHandlerService.getId(this._highlightedSlot.original);
                    var cachedSlot = this._cachedSlots[slotId];

                    if (!this._isMouseInRegion(event, cachedSlot)) {
                        this._clearHighlightedSlot();
                    }

                }
            }.bind(this);

            this.onStop = function(event) {
                var component = this._getSelector(event.target).closest(COMPONENT_SELECTOR);
                this._cleanDragOperation(component);
            }.bind(this);

            // Helpers
            /**
             * This function returns the source of the image used as drag image. Currently, the 
             * image is only returned for Safari; all the other browsers display default images 
             * properly. 
             */
            this._getDragImageSrc = function() {
                var imagePath = '';
                if (browserService.getCurrentBrowser() === SUPPORTED_BROWSERS.SAFARI) {
                    imagePath = assetsService.getAssetsRoot() + DEFAULT_DRAG_IMG;
                }

                return imagePath;
            };


            this._initializeDragOperation = function(dragInfo) {
                this._dragInfo = dragInfo;
                this._cacheElements();

                // Prepare UI
                var overlay = componentHandlerService.getOverlay();
                overlay.addClass(CSS_CLASSES.OVERLAY_IN_DRAG_DROP);

                // Send an event to signal that the drag operation is started. Other pieces of SE, like contextual menus
                // need to be aware.
                systemEventService.sendAsynchEvent(DRAG_AND_DROP_EVENTS.DRAG_STARTED);
            };

            this._cleanDragOperation = function(draggedComponent) {
                this._clearHighlightedSlot();
                if (draggedComponent) {
                    draggedComponent.removeClass(CSS_CLASSES.COMPONENT_DRAGGED);
                }

                var overlay = componentHandlerService.getOverlay();
                overlay.removeClass(CSS_CLASSES.OVERLAY_IN_DRAG_DROP);
                systemEventService.sendAsynchEvent(DRAG_AND_DROP_EVENTS.DRAG_STOPPED);

                this._dragInfo = null;
                this._cachedSlots = {};
                this._highlightedSlot = null;
            };

            this._highlightSlot = function(event) {
                var slot = yjQuery(event.target).closest(SLOT_SELECTOR);
                var slotId = componentHandlerService.getId(slot);

                var oldSlotId;
                if (this._highlightedSlot) {
                    oldSlotId = componentHandlerService.getId(this._highlightedSlot.original);

                    if (oldSlotId !== slotId) {
                        this._clearHighlightedSlot();
                    }
                }

                if (!this._highlightedSlot || this._highlightedSlot.isAllowed === undefined) {
                    this._highlightedSlot = this._cachedSlots[slotId];

                    return slotRestrictionsService.isComponentAllowedInSlot(this._highlightedSlot, this._dragInfo).then(function(componentIsAllowed) {
                        slotRestrictionsService.isSlotEditable(slotId).then(function(slotIsEditable) {
                            // The highlighted slot might have changed while waiting for the promise to be resolved.
                            if (this._highlightedSlot && this._highlightedSlot.id === slotId) {
                                this._highlightedSlot.isAllowed = (componentIsAllowed && slotIsEditable) || this._dragInfo.slotId === slotId;

                                if (this._highlightedSlot.isAllowed) {
                                    slot.addClass(CSS_CLASSES.SLOT_ALLOWED);
                                } else {
                                    slot.addClass(CSS_CLASSES.SLOT_NOT_ALLOWED);
                                }

                                if (event.type === "dragenter" && (!oldSlotId || oldSlotId !== slotId)) {
                                    $timeout(function() {
                                        if (this._highlightedSlot && this._highlightedSlot.id === slotId) {
                                            systemEventService.sendAsynchEvent(slotId + '_SHOW_SLOT_MENU', slotId);
                                            systemEventService.sendAsynchEvent(DRAG_AND_DROP_EVENTS.DRAG_OVER, slotId); // can be used to perform any actions on encountering a drag over event.
                                        }
                                    }.bind(this), 500);
                                }

                            }
                        }.bind(this));
                    }.bind(this));
                }

                return $q.when();
            };

            this._addUIHelpers = function() {
                var overlay = componentHandlerService.getOverlay();

                // First remove all dropzones.
                overlay.find('.' + CSS_CLASSES.UI_HELPER_OVERLAY).remove();

                overlay.find(SLOT_SELECTOR).each(function() {
                    var slot = yjQuery(this);
                    var slotHeight = slot.height();
                    var slotWidth = slot.width();

                    // Make a call to the restrictions service to cache the call if necessary.
                    // NOTE: This call only checks for the slot's restrictions, which don't change
                    // too often. So it makes sense to cache them.
                    var slotId = componentHandlerService.getId(slot);

                    slotRestrictionsService.getSlotRestrictions(slotId);

                    var components = slot.find(COMPONENT_SELECTOR);

                    if (components.length === 0) {
                        var uiHelperOverlay = yjQuery("<div></div>");
                        uiHelperOverlay.addClass(CSS_CLASSES.UI_HELPER_OVERLAY);

                        var uiHelper = yjQuery("<div></div>");
                        uiHelper.addClass(CSS_CLASSES.DROPZONE);
                        uiHelper.addClass(CSS_CLASSES.DROPZONE_FULL);

                        uiHelperOverlay.height(slotHeight);
                        uiHelperOverlay.width(slotWidth);

                        uiHelperOverlay.append(uiHelper);
                        slot.append(uiHelperOverlay);
                    } else {
                        components.each(function() {
                            var component = yjQuery(this);
                            var componentHeight = component.height();
                            var componentWidth = component.width();

                            var uiHelperOverlay = yjQuery("<div></div>");
                            uiHelperOverlay.addClass(CSS_CLASSES.UI_HELPER_OVERLAY);

                            uiHelperOverlay.height(componentHeight);
                            uiHelperOverlay.width(componentWidth);

                            var firstHelper = yjQuery('<div></div>');
                            var secondHelper = yjQuery('<div></div>');

                            firstHelper.addClass(CSS_CLASSES.DROPZONE);
                            secondHelper.addClass(CSS_CLASSES.DROPZONE);

                            if (componentWidth === slotWidth) {
                                firstHelper.addClass(CSS_CLASSES.DROPZONE_TOP);
                                secondHelper.addClass(CSS_CLASSES.DROPZONE_BOTTOM);
                            } else {
                                firstHelper.addClass(CSS_CLASSES.DROPZONE_LEFT);
                                secondHelper.addClass(CSS_CLASSES.DROPZONE_RIGHT);
                            }

                            uiHelperOverlay.append(firstHelper);
                            uiHelperOverlay.append(secondHelper);

                            component.append(uiHelperOverlay);
                        });
                    }
                });
            };

            this._cacheElements = function() {
                var overlay = componentHandlerService.getOverlay();

                var currentService = this;
                var scrollY = this._getWindowScrolling();

                overlay.children(SLOT_SELECTOR).each(function() {
                    var slot = yjQuery(this);
                    var slotId = componentHandlerService.getId(slot);

                    var cachedSlot = {
                        id: slotId,
                        original: slot,
                        components: [],
                        rect: currentService._getElementRects(slot, scrollY)
                    };

                    var components = slot.children(COMPONENT_SELECTOR);
                    if (components.length === 0) {
                        var hint = slot.find(HINT_SELECTOR);
                        cachedSlot.hint = (hint.length > 0) ? {
                            original: hint,
                            rect: currentService._getElementRects(hint, scrollY)
                        } : null;
                    } else {

                        var positionInSlot = 0;
                        components.each(function() {
                            var component = yjQuery(this);
                            var cachedComponent = {
                                id: componentHandlerService.getSlotOperationRelatedId(component),
                                type: componentHandlerService.getSlotOperationRelatedType(component),
                                original: component,
                                position: positionInSlot,
                                hints: [],
                                rect: currentService._getElementRects(component, scrollY)
                            };

                            var positionInComponent = positionInSlot++;
                            component.find(HINT_SELECTOR).each(function() {
                                var hint = yjQuery(this);
                                var cachedHint = {
                                    original: hint,
                                    position: positionInComponent++,
                                    rect: currentService._getElementRects(hint, scrollY)
                                };

                                cachedComponent.hints.push(cachedHint);
                            });

                            cachedSlot.components.push(cachedComponent);
                        });
                    }

                    currentService._cachedSlots[cachedSlot.id] = cachedSlot;
                });
            };

            this._clearHighlightedHint = function() {
                if (this._highlightedHint) {
                    this._highlightedHint.original.removeClass(CSS_CLASSES.DROPZONE_HOVERED);
                    this._highlightedHint = null;
                }
            };

            this._clearHighlightedComponent = function() {
                this._clearHighlightedHint();
                if (this._highlightedComponent) {
                    this._highlightedComponent.original.removeClass(CSS_CLASSES.COMPONENT_DRAGGED_HOVERED);
                    this._highlightedComponent = null;
                }
            };

            this._clearHighlightedSlot = function() {
                this._clearHighlightedComponent();

                if (this._highlightedSlot) {
                    this._highlightedSlot.original.removeClass(CSS_CLASSES.SLOT_ALLOWED);
                    this._highlightedSlot.original.removeClass(CSS_CLASSES.SLOT_NOT_ALLOWED);

                    systemEventService.sendAsynchEvent('HIDE_SLOT_MENU');
                    systemEventService.sendAsynchEvent(DRAG_AND_DROP_EVENTS.DRAG_LEAVE); // can be used to perform any actions on encountering a drag leave event.
                }

                this._highlightedSlot = null;
            };

            this._isMouseInRegion = function(event, element) {
                var boundingRect = element.rect;

                return (event.pageX >= boundingRect.left && event.pageX <= boundingRect.right && event.pageY >= boundingRect.top && event.pageY <= boundingRect.bottom);
            };

            this._getElementRects = function(element, scrollY) {
                var baseRect = element[0].getBoundingClientRect();
                var rect = {
                    left: baseRect.left,
                    right: baseRect.right,
                    bottom: baseRect.bottom + scrollY,
                    top: baseRect.top + scrollY
                };

                return rect;
            };

            this._getWindowScrolling = function() {
                return ($window.scrollY || $window.pageYOffset);
            };

            this._getComponentPositionInSlot = function(slotId, componentId) {
                var components = this._cachedSlots[slotId].components;

                var result = lodash.takeWhile(components, function(comp) {
                    return comp.id !== componentId;
                });

                return (result.length !== components.length) ? result.length : -1;
            };

            this._scrollToModifiedSlot = function(componentId) {
                var component = componentHandlerService.getComponentInOverlay(componentId, CONTENT_SLOT_TYPE);
                if (component && component.length > 0) {
                    component[0].scrollIntoView();
                }
            };

            this._getSelector = function(selector) {
                return yjQuery(selector);
            };

        }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name componentEditingFacadeModule
 * @description
 * # The componentEditingFacadeModule
 *
 * The componentEditingFacadeModule contains a service with methods that allow adding or removing components in the page.
 *
 */
angular.module('componentEditingFacadeModule', [
    'alertServiceModule',
    'componentServiceModule',
    'componentVisibilityAlertServiceModule',
    'editorModalServiceModule',
    'renderServiceModule',
    'restServiceFactoryModule',
    'slotVisibilityServiceModule',
    'translationServiceModule'
])

/**
 * @ngdoc service
 * @name componentEditingFacadeModule.service:componentEditingFacade
 *
 * @description
 * This service provides methods that allow adding or removing components in the page.
 */
.service('componentEditingFacade', ['$q', 'ComponentService', 'componentHandlerService', 'restServiceFactory', 'editorModalService', 'removeComponentService', 'renderService', 'alertService', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', 'componentVisibilityAlertService', 'slotVisibilityService', function($q, ComponentService, componentHandlerService, restServiceFactory, editorModalService, removeComponentService, renderService, alertService, PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI, componentVisibilityAlertService, slotVisibilityService) {

    var _contentSlotComponentsRestService;

    function _generateSuccessMessage(sourceComponentId, targetSlotId) {
        return {
            message: "se.cms.draganddrop.success",
            messagePlaceholders: {
                sourceComponentId: sourceComponentId,
                targetSlotId: targetSlotId
            }
        };
    }

    function _generateErrorMessage(sourceComponentId, targetSlotId, requestResponse) {
        var detailedError = (requestResponse.data && requestResponse.data.errors && requestResponse.data.errors.length > 0) ?
            requestResponse.data.errors[0].message : "";

        return {
            message: "se.cms.draganddrop.error",
            messagePlaceholders: {
                sourceComponentId: sourceComponentId,
                targetSlotId: targetSlotId,
                detailedError: detailedError
            }
        };
    }

    /**
     * @ngdoc method
     * @name componentEditingFacadeModule.service:componentEditingFacade#addNewComponentToSlot
     * @methodOf componentEditingFacadeModule.service:componentEditingFacade
     *
     * @description
     * This methods adds a new component to the slot and opens a component modal to edit its properties.
     *
     * @param {Object} slotInfo The target slot for the new component
     * @param {Object} slotInfo.targetSlotId The Uid of the slot where to drop the new component.
     * @param {Object} slotInfo.targetSlotUUId The UUid of the slot where to drop the new component.
     * @param {String} catalogVersionUuid The catalog version on which to create the new component
     * @param {String} componentType The type of the new component to add.
     * @param {Number} position The position in the slot where to add the new component.
     *
     */
    this.addNewComponentToSlot = function(slotInfo, catalogVersionUuid, componentType, position) {

        var componentAttributes = {
            smarteditComponentType: componentType,
            catalogVersionUuid: catalogVersionUuid
        };

        return editorModalService.open(componentAttributes, slotInfo.targetSlotUUId, position).then(function(response) {

            alertService.showSuccess(_generateSuccessMessage(response.uid, slotInfo.targetSlotId));

            renderService.renderSlots([slotInfo.targetSlotId]).then(function() {
                slotVisibilityService.reloadSlotsInfo();
            });
        }).catch(function(response) {
            var errorMessageObject = _generateErrorMessage(0, slotInfo.targetSlotId, response);
            alertService.showDanger(errorMessageObject);
        }.bind(this));
    };

    /**
     * @ngdoc method
     * @name componentEditingFacadeModule.service:componentEditingFacade#addExistingComponentToSlot
     * @methodOf componentEditingFacadeModule.service:componentEditingFacade
     *
     * @description
     * This methods adds an existing component to the slot and display an Alert whenever the component is either hidden or restricted.
     *
     * @param {String} targetSlotId The ID of the slot where to drop the component.
     * @param {Object} dragInfo The dragInfo object containing the componentId, componentUuid and componentType.
     * @param {Number} position The position in the slot where to add the component.
     *
     */
    this.addExistingComponentToSlot = function(targetSlotId, dragInfo, position) {
        var pageId = componentHandlerService.getPageUID();

        return ComponentService.addExistingComponent(pageId, dragInfo.componentId, targetSlotId, position).then(

            function() {
                return ComponentService.loadComponentItem(dragInfo.componentUuid).then(function(item) {
                    componentVisibilityAlertService.checkAndAlertOnComponentVisibility({
                        itemId: dragInfo.componentUuid,
                        itemType: dragInfo.componentType,
                        catalogVersion: item.catalogVersion,
                        restricted: item.restricted,
                        slotId: targetSlotId,
                        visible: item.visible
                    });

                    alertService.showSuccess(_generateSuccessMessage(dragInfo.componentId, targetSlotId));

                    renderService.renderSlots(targetSlotId).then(function() {
                        slotVisibilityService.reloadSlotsInfo();
                    });

                });
            },

            function(response) {
                var errorMessageObject = _generateErrorMessage(dragInfo.componentId, targetSlotId, response);
                alertService.showDanger(errorMessageObject);
                return $q.reject();
            }.bind(this)

        );
    };


    /**
     * @ngdoc method
     * @name componentEditingFacadeModule.service:componentEditingFacade#moveComponent
     * @methodOf componentEditingFacadeModule.service:componentEditingFacade
     *
     * @description
     * This methods moves a component from two slots in a page.
     *
     * @param {String} sourceSlotId The ID of the slot where the component is initially located.
     * @param {String} targetSlotId The ID of the slot where to drop the component.
     * @param {String} componentId The ID of the component to add into the slot.
     * @param {Number} position The position in the slot where to add the component.
     *
     */
    this.moveComponent = function(sourceSlotId, targetSlotId, componentId, position) {
        var contentSlotComponentsResourceLocation = PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI + '/pages/:pageId/contentslots/:currentSlotId/components/:componentId';
        _contentSlotComponentsRestService = _contentSlotComponentsRestService || restServiceFactory.get(contentSlotComponentsResourceLocation, 'componentId');
        return _contentSlotComponentsRestService.update({
            pageId: componentHandlerService.getPageUID(),
            currentSlotId: sourceSlotId,
            componentId: componentId,
            slotId: targetSlotId,
            position: position
        }).then(
            function() {
                renderService.renderSlots([sourceSlotId, targetSlotId]).then(function() {
                    slotVisibilityService.reloadSlotsInfo();
                });
            },
            function(response) {
                var errorMessageObject = (response === undefined) ? {
                        message: "se.cms.draganddrop.move.failed",
                        messagePlaceholders: {
                            slotID: targetSlotId,
                            componentID: componentId
                        }
                    } :
                    _generateErrorMessage(componentId, targetSlotId, response);

                alertService.showDanger(errorMessageObject);

            }.bind(this)
        );
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotRestrictionsServiceModule
 * @description
 * # The slotRestrictionsServiceModule
 *
 * The slotRestrictionsServiceModule contains a service that caches and returns the restrictions of a slot in a page. This restrictions determine
 * whether a component of a certain type is allowed or forbidden in a particular slot.
 *
 */
angular.module('slotRestrictionsServiceModule', ['yLoDashModule', 'gatewayProxyModule', 'crossFrameEventServiceModule', 'componentHandlerServiceModule', 'slotSharedServiceModule', 'restServiceFactoryModule'])
    /**
     * @ngdoc service
     * @name slotRestrictionsServiceModule.service:slotRestrictionsService
     *
     * @description
     * This service provides methods that cache and return the restrictions of a slot in a page. This restrictions determine
     * whether a component of a certain type is allowed or forbidden in a particular slot.
     */
    .service('slotRestrictionsService', ['$q', '$log', 'lodash', 'gatewayProxy', 'crossFrameEventService', 'EVENTS', 'componentHandlerService', 'slotSharedService', 'restServiceFactory', 'CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI', 'CONTENT_SLOT_TYPE', function($q, $log, lodash, gatewayProxy, crossFrameEventService, EVENTS, componentHandlerService, slotSharedService, restServiceFactory, CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI, CONTENT_SLOT_TYPE) {
        var _slotRestrictions = {};
        var _currentPageId = null;
        var _slotRestrictionsRestService;


        /**
         * @ngdoc method
         * @name slotRestrictionsServiceModule.service:slotRestrictionsService#getAllComponentTypesSupportedOnPage
         * @methodOf slotRestrictionsServiceModule.service:slotRestrictionsService
         *
         * @description
         * This methods retrieves the list of component types droppable in at least one of the slots of the current page
         * @returns {Promise} A promise containing an array with the component types droppable on the current page
         */
        this.getAllComponentTypesSupportedOnPage = function() {
            var slots = componentHandlerService.getFromSelector(componentHandlerService.getAllSlotsSelector());
            var slotIds = Array.prototype.slice.call(slots.map(function() {
                return componentHandlerService.getId(componentHandlerService.getFromSelector(this));
            }));

            return $q.all(slotIds.map(function(slotId) {
                return this.getSlotRestrictions(slotId);
            }.bind(this))).then(function(arrayOfSlotRestrictions) {
                return lodash.flatten(arrayOfSlotRestrictions);
            }, function(error) {
                $log.info(error);
            });
        };

        /**
         * @ngdoc method
         * @name slotRestrictionsServiceModule.service:slotRestrictionsService#getSlotRestrictions
         * @methodOf slotRestrictionsServiceModule.service:slotRestrictionsService
         *
         * @description
         * This methods retrieves the list of restrictions applied to the slot identified by the provided ID.
         *
         * @param {String} slotId The ID of the slot whose restrictions to retrieve.
         * @returns {Promise} A promise containing an array with the restrictions applied to the slot.
         */
        this.getSlotRestrictions = function(slotId) {
            _slotRestrictionsRestService = _slotRestrictionsRestService || restServiceFactory.get(CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI);
            _currentPageId = _currentPageId || componentHandlerService.getPageUID();

            var restrictionId = this._getEntryId(_currentPageId, slotId);
            if (_slotRestrictions[restrictionId]) {
                return $q.when(_slotRestrictions[restrictionId]);
            } else if (this._isExternalSlot(slotId)) {
                _slotRestrictions[restrictionId] = [];
                return $q.when(_slotRestrictions[restrictionId]);
            }

            return _slotRestrictionsRestService.get({
                pageUid: _currentPageId,
                slotUid: slotId
            }).then(function(response) {
                _slotRestrictions[restrictionId] = response.validComponentTypes;
                return _slotRestrictions[restrictionId];
            }.bind(this), function(error) {
                $log.info(error);
            });
        };

        /**
         * @ngdoc method
         * @name slotRestrictionsServiceModule.service:slotRestrictionsService#isComponentAllowedInSlot
         * @methodOf slotRestrictionsServiceModule.service:slotRestrictionsService
         *
         * @description
         * This methods determines whether a component of the provided type is allowed in the slot.
         *
         * @param {Object} slot the slot for which to verify if it allows a component of the provided type.
         * @param {String} slot.id The ID of the slot.
         * @param {Array} slot.components the list of components contained in the slot, they must contain an "id" property.
         * @param {Object} dragInfo contains the dragged object information
         * @param {String} dragInfo.componentType The smartedit type of the component being checked.
         * @param {String} dragInfo.componentId The smartedit id of the component being checked.
         * @param {String} dragInfo.slotId The smartedit id of the slot from which the component originates
         * @returns {Promise} A promise containing a boolean flag that determines whether a component of the provided type is allowed in the slot.
         */
        this.isComponentAllowedInSlot = function(slot, dragInfo) {
            return this.getSlotRestrictions(slot.id).then(function(currentSlotRestrictions) {

                var isComponentIdAllowed = slot.id === dragInfo.slotId || !slot.components.some(function(component) {
                    return component.id === dragInfo.componentId;
                });
                return isComponentIdAllowed && lodash.includes(currentSlotRestrictions, dragInfo.componentType);
            });
        };

        /**
         * @ngdoc method
         * @name slotRestrictionsServiceModule.service:slotRestrictionsService#isSlotEditable
         * @methodOf slotRestrictionsServiceModule.service:slotRestrictionsService
         *
         * @description
         * This method determines whether slot is editable or not.
         *
         * @param {String} slotId The ID of the slot.
         *
         * @returns {Promise} A promise containing a boolean flag that shows whether if the slot is editable or not.
         */
        this.isSlotEditable = function(slotId) {
            return slotSharedService.isSlotShared(slotId).then(function(isShared) {
                if (isShared) {
                    var isExternalSlot = this._isExternalSlot(slotId);
                    return !isExternalSlot && !slotSharedService.areSharedSlotsDisabled();
                }

                return true;
            }.bind(this));
        };

        this.emptyCache = function() {
            _slotRestrictions = {};
            _currentPageId = null;
        };

        this._getEntryId = function(pageId, slotId) {
            return pageId + '_' + slotId;
        };

        this._isExternalSlot = function(slotId) {
            return componentHandlerService.isExternalComponent(slotId, CONTENT_SLOT_TYPE);
        };

        crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
            this.emptyCache();
        }.bind(this));

        gatewayProxy.initForService(this, ['getAllComponentTypesSupportedOnPage', 'getSlotRestrictions'], "SLOT_RESTRICTIONS");

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name editorEnablerServiceModule
 * @description
 * # The editorEnablerServiceModule
 *
 * The editor enabler service module provides a service that allows enabling the Edit Component contextual menu item,
 * providing a SmartEdit CMS admin the ability to edit various properties of the given component.
 */
angular.module("editorEnablerServiceModule", [
    "componentVisibilityAlertServiceModule",
    "componentHandlerServiceModule",
    "editorModalServiceModule",
    "featureServiceModule",
    "slotRestrictionsServiceModule"
])

/**
 * @ngdoc service
 * @name editorEnablerServiceModule.service:editorEnablerService
 *
 * @description
 * Convenience service to attach the open editor modal action to the contextual menu of a given component type, or
 * given regex corresponding to a selection of component types.
 *
 * Example: The Edit button is added to the contextual menu of the CMSParagraphComponent, and all types postfixed
 * with BannerComponent.
 *
 * <pre>
 angular.module('app', ['editorEnablerServiceModule'])
 .run(function(editorEnablerService) {
            editorEnablerService.enableForComponents(['CMSParagraphComponent', '*BannerComponent']);
        });
 * </pre>
 */
.factory("editorEnablerService", ['componentVisibilityAlertService', 'componentHandlerService', 'editorModalService', 'featureService', 'slotRestrictionsService', function(
    componentVisibilityAlertService,
    componentHandlerService,
    editorModalService,
    featureService,
    slotRestrictionsService
) {

    // Class Definition
    function EditorEnablerService() {}

    // Private


    EditorEnablerService.prototype._key = "se.cms.edit";

    EditorEnablerService.prototype._nameI18nKey = "se.cms.contextmenu.nameI18nKey.edit";

    EditorEnablerService.prototype._i18nKey = "se.cms.contextmenu.title.edit";

    EditorEnablerService.prototype._descriptionI18nKey = "se.cms.contextmenu.descriptionI18n.edit";

    EditorEnablerService.prototype._editDisplayClass = "editbutton";

    EditorEnablerService.prototype._editDisplayIconClass = "hyicon hyicon-edit";

    EditorEnablerService.prototype._editDisplaySmallIconClass = "hyicon hyicon-edit";

    EditorEnablerService.prototype._editButtonCallback = function(contextualMenuParams) {
        var slotUuid = contextualMenuParams.slotUuid;
        editorModalService.open(contextualMenuParams.componentAttributes).then(function(item) {
            componentVisibilityAlertService.checkAndAlertOnComponentVisibility({
                itemId: item.uuid,
                itemType: item.itemtype,
                catalogVersion: item.catalogVersion,
                restricted: item.restricted,
                slotId: slotUuid,
                visible: item.visible
            });
        });
    };

    EditorEnablerService.prototype._condition = function(contextualMenuParams) {
        var slotId = componentHandlerService.getParentSlotForComponent(contextualMenuParams.element);

        return slotRestrictionsService.isSlotEditable(slotId).then(function(isEditable) {
            return isEditable && !componentHandlerService.isExternalComponent(contextualMenuParams.componentId, contextualMenuParams.componentType);
        });
    };

    // Public
    /**
     * @ngdoc method
     * @name editorEnablerServiceModule.service:editorEnablerService#enableForComponents
     * @methodOf editorEnablerServiceModule.service:editorEnablerService
     *
     * @description
     * Enables the Edit contextual menu item for the given component types.
     *
     * @param {Array} componentTypes The list of component types, as defined in the platform, for which to enable the
     * Edit contextual menu.
     */
    EditorEnablerService.prototype.enableForComponents = function(componentTypes) {
        var contextualMenuConfig = {
            key: this._key, // It's the same key as the i18n, however here we're not doing any i18n work.
            nameI18nKey: this._nameI18nKey,
            descriptionI18nKey: this._descriptionI18nKey,
            regexpKeys: componentTypes,
            displayClass: this._editDisplayClass,
            displayIconClass: this._editDisplayIconClass,
            displaySmallIconClass: this._editDisplaySmallIconClass,
            i18nKey: this._i18nKey,
            callback: this._editButtonCallback,
            condition: this._condition,
            permissions: ['se.context.menu.edit.component']
        };

        featureService.addContextualMenuButton(contextualMenuConfig);
    };

    // Factory Definition
    return new EditorEnablerService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name editorModalServiceModule
 * @description
 * # The editorModalServiceModule
 *
 * The editor modal service module provides a service that allows opening an editor modal for a given component type and
 * component ID. The editor modal is populated with a save and cancel button, and is loaded with the 
 * editorTabset of cmssmarteditContainer as its content, providing a way to edit
 * various fields of the given component.
 */
angular.module('editorModalServiceModule', ['gatewayProxyModule'])

/**
 * @ngdoc service
 * @name editorModalServiceModule.service:editorModalService
 *
 * @description
 * Convenience service to open an editor modal window for a given component type and component ID.
 *
 * Example: A button is bound to the function '$scope.onClick' via the ng-click directive. Clicking the button will
 * trigger the opening of an editor modal for a CMSParagraphComponent with the ID 'termsAndConditionsParagraph'
 *
 * <pre>
    angular.module('app', ['editorModalServiceModule'])
        .controller('someController', function($scope, editorModalService) {
            $scope.onClick = function() {
                editorModalService.open(componentAttributes);
            };
        });
 * </pre>
*/
.factory('editorModalService', ['gatewayProxy', function(gatewayProxy) {
    function EditorModalService() {
        this.gatewayId = 'EditorModal';
        gatewayProxy.initForService(this, ["open", "openAndRerenderSlot"]);
    }

    /**
     * @ngdoc method
     * @name editorModalServiceModule.service:editorModalService#open
     * @methodOf editorModalServiceModule.service:editorModalService
     *
     * @description
     * Proxy function which delegates opening an editor modal for a given component type and component ID to the
     * SmartEdit container.
     *
     * @param {Object} componentAttributes The details of the component to be created/edited
     * @param {Object} componentAttributes.smarteditComponentId The id of the component
     * @param {Object} componentAttributes.smarteditComponentUuid The Universally unique id of the component
     * @param {Object} componentAttributes.smarteditComponentType The component type
     * @param {Object} componentAttributes.catalogVersionUuid The catalog version UUID to add the component to
     * @param {String} [targetSlotId] The ID of the slot in which the component is placed.
     * @param {String} [position] The position in a given slot where the component should be placed.
     *
     * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
     */
    EditorModalService.prototype.open = function() {};


    /**
     * @ngdoc method
     * @name editorModalServiceModule.service:editorModalService#open
     * @methodOf editorModalServiceModule.service:editorModalService
     *
     * @description
     * Proxy function which delegates opening an editor modal for a given component type and component ID to the
     * SmartEdit container.
     *
     * @param {String} componentType The type of component as defined in the platform.
     * @param {String} componentId The UUID of the component as defined in the database.
     * @param {=String} selectedTabTitle I18n key of the tab being selected by default on modal opening
     *
     * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
     */
    EditorModalService.prototype.openAndRerenderSlot = function() {};

    return new EditorModalService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('removeComponentServiceModule', ['restServiceFactoryModule', 'renderServiceModule', 'gatewayProxyModule', 'removeComponentServiceInterfaceModule', 'experienceInterceptorModule', 'functionsModule', 'resourceLocationsModule', 'eventServiceModule'])
    .constant('COMPONENT_REMOVED_EVENT', 'componentRemovedEvent')
    /**
     * @ngdoc service
     * @name removeComponentService.removeComponentService
     *
     * @description
     * Service to remove a component from a slot
     */
    .factory('removeComponentService', ['restServiceFactory', 'renderService', 'extend', 'gatewayProxy', '$q', '$log', 'RemoveComponentServiceInterface', 'experienceInterceptor', 'systemEventService', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', 'COMPONENT_REMOVED_EVENT', function(restServiceFactory, renderService, extend, gatewayProxy, $q, $log, RemoveComponentServiceInterface, experienceInterceptor, systemEventService, PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI, COMPONENT_REMOVED_EVENT) {
        var REMOVE_COMPONENT_CHANNEL_ID = "RemoveComponent";

        var RemoveComponentService = function(gatewayId) {
            this.gatewayId = gatewayId;

            gatewayProxy.initForService(this, ["removeComponent"]);
        };

        RemoveComponentService = extend(RemoveComponentServiceInterface, RemoveComponentService);

        var restServiceForRemoveComponent = restServiceFactory.get(PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI + '/contentslots/:slotId/components/:componentId', 'componentId');

        RemoveComponentService.prototype.removeComponent = function(configuration) {

            return restServiceForRemoveComponent.remove({
                slotId: configuration.slotId,
                componentId: configuration.slotOperationRelatedId
            }).then(function() {
                renderService.renderSlots(configuration.slotId);
                // Send an event specifying that some component was removed.
                systemEventService.sendAsynchEvent(COMPONENT_REMOVED_EVENT);
            });

        };

        return new RemoveComponentService(REMOVE_COMPONENT_CHANNEL_ID);

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotSharedServiceModule.slotSharedService
 * @description
 * SlotSharedService provides methods to interact with the backend for shared slot information. 
 */
angular.module('slotSharedServiceModule', ['resourceModule', 'crossFrameEventServiceModule', 'componentHandlerServiceModule', 'resourceLocationsModule'])
    .service('slotSharedService', ['$q', 'restServiceFactory', 'crossFrameEventService', 'EVENTS', 'componentHandlerService', 'PAGES_CONTENT_SLOT_RESOURCE_URI', function($q, restServiceFactory, crossFrameEventService, EVENTS, componentHandlerService, PAGES_CONTENT_SLOT_RESOURCE_URI) {
        var pagesContentSlotsResource = restServiceFactory.get(PAGES_CONTENT_SLOT_RESOURCE_URI);

        var sharedSlotsPromise;

        var getUniqueArray = function(id, position, ids) {
            return ids.indexOf(id) === position;
        };

        /**
         * @ngdoc method
         * @name slotSharedServiceModule.slotSharedService#reloadSharedSlotMap
         * @methodOf slotSharedServiceModule.slotSharedService
         *
         * @description
         * This function fetches all the slots with the pageUID and then retrieves the slotShared property for each slot.
         *
         * @param {String} pageUID of the page
         */
        this.reloadSharedSlotMap = function() {
            sharedSlotsPromise = pagesContentSlotsResource.get({
                pageId: componentHandlerService.getPageUID()
            }).then(function(pagesContentSlotsResponse) {
                return $q.when((pagesContentSlotsResponse.pageContentSlotList || [])
                    .filter(getUniqueArray)
                    .reduce(function(acc, slot) {
                        acc[slot.slotId] = slot.slotShared;
                        return acc;
                    }, {}));
            });

            return sharedSlotsPromise;
        };

        /**
         * @ngdoc method
         * @name slotSharedServiceModule.slotSharedService#isSlotShared
         * @methodOf slotSharedServiceModule.slotSharedService
         *
         * @description
         * Checks if the slot is shared and returns true in case slot is shared and returns false if it is not. 
         * Based on this service method the slot shared button is shown or hidden for a particular slotId
         *
         * @param {String} slotId of the slot
         */
        this.isSlotShared = function(slotId) {
            return (sharedSlotsPromise ? $q.when(sharedSlotsPromise) : this.reloadSharedSlotMap()).then(function(response) {
                sharedSlotsPromise = response;
                return sharedSlotsPromise[slotId];
            });
        };

        /**
         * @ngdoc method
         * @name slotSharedServiceModule.slotSharedService#setSharedSlotEnablementStatus
         * @methodOf slotSharedServiceModule.slotSharedService
         *
         * @description
         * Sets the status of the disableSharedSlot feature
         *
         * @param {Boolean} status of the disableSharedSlot feature
         */
        this.setSharedSlotEnablementStatus = function(status) {
            this.disableShareSlotStatus = status;
        };

        /**
         * @ngdoc method
         * @name slotSharedServiceModule.slotSharedService#isSharedSlotDisabled
         * @methodOf slotSharedServiceModule.slotSharedService
         *
         * @description
         * Checks the status of the disableSharedSlot feature
         *
         */
        this.areSharedSlotsDisabled = function() {
            return this.disableShareSlotStatus;
        };

        crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
            this.reloadSharedSlotMap();
        }.bind(this));

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotSynchronizationServiceModule', ['synchronizationPollingServiceModule'])
    .constant("SYNCHRONIZATION_SLOTS_SELECT_ALL_COMPONENTS_LABEL", "se.cms.synchronization.slots.select.all.components")
    .service('slotSynchronizationService', ['SYNCHRONIZATION_SLOTS_SELECT_ALL_COMPONENTS_LABEL', 'syncPollingService', function(SYNCHRONIZATION_SLOTS_SELECT_ALL_COMPONENTS_LABEL, syncPollingService) {

        this.getSyncStatus = function(pageUUID, slotId) {
            return syncPollingService.getSyncStatus(pageUUID).then(function(syncStatus) {
                var slotSyncStatus = (syncStatus.selectedDependencies || []).concat(syncStatus.sharedDependencies || []).find(function(slot) {
                    return slot.name === slotId;
                }) || {};
                slotSyncStatus.selectAll = SYNCHRONIZATION_SLOTS_SELECT_ALL_COMPONENTS_LABEL;
                return slotSyncStatus;
            });
        };

        this.performSync = function(array, uriContext) {
            return syncPollingService.performSync(array, uriContext);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('synchronizationPollingServiceModule', ['gatewayProxyModule', 'eventServiceModule', 'synchronizationConstantsModule'])
    .run(['syncPollingService', function(syncPollingService) {
        syncPollingService.registerSyncPollingEvents();
    }])
    .factory('syncPollingService', ['gatewayProxy', 'systemEventService', 'SYNCHRONIZATION_POLLING', function(gatewayProxy, systemEventService, SYNCHRONIZATION_POLLING) {

        var SyncPollingService = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };

        SyncPollingService.prototype.getSyncStatus = function() {};

        SyncPollingService.prototype._fetchSyncStatus = function() {};

        SyncPollingService.prototype.changePollingSpeed = function() {};

        SyncPollingService.prototype.registerSyncPollingEvents = function() {
            systemEventService.registerEventHandler(SYNCHRONIZATION_POLLING.SPEED_UP, this.changePollingSpeed.bind(this));
            systemEventService.registerEventHandler(SYNCHRONIZATION_POLLING.SLOW_DOWN, this.changePollingSpeed.bind(this));
        };

        SyncPollingService.prototype.performSync = function() {};


        return new SyncPollingService('syncPollingService');

    }]);

angular.module('cmssmarteditTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmssmartedit/components/externalComponent/externalComponentButtonTemplate.html',
    "<div class=\"cms-external-component-button\"\n" +
    "    data-ng-if=\"ctrl.isReady\">\n" +
    "    {{ctrl.catalogVersion}}\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/externalComponent/externalComponentDecoratorTemplate.html',
    "<div class=\"cms-external-component-decorator\"\n" +
    "    data-ng-if=\"!ctrl.isExtenalSlot\">\n" +
    "    <div class=\"contextualMenuOverlay\">\n" +
    "        <span class=\"hyicon hyicon-globe hyicon-globe--normal\"\n" +
    "            data-ng-if=\"!ctrl.active\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"yWrapperData\"\n" +
    "        data-ng-transclude>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotShared/slotSharedButtonTemplate.html',
    "<div class=\"shared-slot-button-template\">\n" +
    "    <a type=\"button\"\n" +
    "        data-ng-if=\"ctrl.slotSharedFlag\">\n" +
    "        <div class=\"btn-group se-slot-ctx-menu__btn-group\"\n" +
    "            data-uib-dropdown\n" +
    "            data-dropdown-append-to-body\n" +
    "            data-auto-close=\"outsideClick\"\n" +
    "            data-is-open=\"ctrl.isPopupOpened\">\n" +
    "            <button type=\"button\"\n" +
    "                data-uib-dropdown-toggle\n" +
    "                class=\"btn btn-primary shared-slot-button-template__btn se-slot-ctx-menu__divider\"\n" +
    "                data-ng-class=\"{'ySESharedSlotOpen': ctrl.isPopupOpened}\"\n" +
    "                id=\"sharedSlotButton-{{::ctrl.slotId}}\">\n" +
    "                <span class=\"hyicon hyicon-linked se-slot-ctx-menu__icon-shared\"></span>\n" +
    "            </button>\n" +
    "            <ul class=\"dropdown-menu dropdown-menu-right btn-block shared-slot-button-template__menu\"\n" +
    "                data-uib-dropdown-menu\n" +
    "                role=\"menu\">\n" +
    "                <li role=\"menuitem\">\n" +
    "                    <div class=\"shared-slot-button-template__menu__title__text\">\n" +
    "                        <span class=\"shared-slot-button-template__menu__title__text__msg\"\n" +
    "                            data-translate=\"se.cms.slot.shared.popover.message\"></span>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotShared/slotSharedTemplate.html',
    "<slot-shared-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"></slot-shared-button>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/externalSlotDisabledDecoratorTemplate.html',
    "<slot-disabled-component data-active=\"ctrl.active\"\n" +
    "    data-component-attributes=\"ctrl.componentAttributes\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/sharedSlotDisabledDecoratorTemplate.html',
    "<slot-disabled-component data-active=\"ctrl.active\"\n" +
    "    data-component-attributes=\"ctrl.componentAttributes\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/slotDisabledTemplate.html',
    "<div class=\"disabled-shared-slot popoveredElement\"\n" +
    "    data-ng-class=\"{ 'disabled-shared-slot-hovered': ctrl.active, 'external-shared-slot': ctrl.isExternalSlot }\"\n" +
    "    data-popover-content=\"{{ctrl.getPopoverMessage()}}\">\n" +
    "    <div class=\"disabled-shared-slot__icon--outer\"\n" +
    "        data-ng-class=\"[ctrl.getOuterSlotClass(), { 'disabled-shared-slot__icon--outer-hovered': ctrl.active }]\">\n" +
    "        <span class=\"hyicon shared_slot_disabled_icon disabled-shared-slot__icon--inner\"\n" +
    "            data-ng-class=\"ctrl.getSlotIconClass()\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityButtonTemplate.html',
    "<div class=\"slot-visibility-button-template\">\n" +
    "    <div class=\"btn-group slot-visibility-button-template__btn-group\"\n" +
    "        data-uib-dropdown\n" +
    "        data-dropdown-append-to-body\n" +
    "        data-is-open=\"ctrl.isComponentListOpened\">\n" +
    "        <button type=\"button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            class=\"btn btn-primary slot-visibility-button-template__btn se-slot-ctx-menu__divider\"\n" +
    "            data-ng-if=\"ctrl.buttonVisible\"\n" +
    "            data-ng-class=\"{'ySEVisibilityOpen': ctrl.isComponentListOpened}\"\n" +
    "            id=\"slot-visibility-button-{{::ctrl.slotId}}\">\n" +
    "            <img class=\"slot-visibility-button-template__btn__img \"\n" +
    "                data-ng-src=\"{{ ctrl.eyeImageUrl }}\" />{{ ::ctrl.hiddenComponentCount }}\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu dropdown-menu-right slot-visibility-button-template__menu\"\n" +
    "            data-uib-dropdown-menu\n" +
    "            role=\"menu\"\n" +
    "            id=\"slot-visibility-list-{{::ctrl.slotId}}\">\n" +
    "            <lh class=\"slot-visibility-button-template__menu__title\">\n" +
    "                <p class=\"slot-visibility-button-template__menu__title__text\">{{'se.cms.slotvisibility.list.title' | translate}}</p>\n" +
    "            </lh>\n" +
    "            <li class=\"slot-visibility-component slot-visibility-button-template__menu__component\"\n" +
    "                data-ng-repeat=\"component in ctrl.hiddenComponents track by $index\">\n" +
    "\n" +
    "                <span data-y-popover\n" +
    "                    data-ng-if=\"component.isExternal\"\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-template=\"ctrl.getTemplateInfoForExternalComponent()\">\n" +
    "                    <slot-visibility-component data-component=\"component\"\n" +
    "                        data-slot-id=\"{{::ctrl.slotId}}\"></slot-visibility-component>\n" +
    "                </span>\n" +
    "\n" +
    "                <slot-visibility-component data-component=\"component\"\n" +
    "                    data-ng-if=\"!component.isExternal\"\n" +
    "                    data-slot-id=\"{{::ctrl.slotId}}\"></slot-visibility-component>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityComponentTemplate.html',
    "<div class=\"slot-visiblity-component-template\"\n" +
    "    data-ng-class=\"{'slot-visiblity-component-template__external': ctrl.component.isExternal}\">\n" +
    "    <div class=\"slot-visiblity-component-template__icon\">\n" +
    "        <img data-ng-src=\"{{::ctrl.imageRoot}}/{{ ctrl.component.isExternal ? 'component_external.png': 'component_default.png' }}\"\n" +
    "            class=\"slot-visiblity-component-template__icon__image\"\n" +
    "            alt=\"{{::ctrl.component.typeCode}}\" />\n" +
    "    </div>\n" +
    "    <div class=\"slot-visiblity-component-template__text-wrapper\">\n" +
    "        <a href=\"#\"\n" +
    "            data-ng-if=\"!ctrl.component.isExternal\"\n" +
    "            class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__name\"\n" +
    "            data-ng-click=\"ctrl.openEditorModal()\">{{ ::ctrl.component.name }}</a>\n" +
    "        <a data-ng-if=\"ctrl.component.isExternal\"\n" +
    "            class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__name\">{{ ::ctrl.component.name }}</a>\n" +
    "        <p class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__type\">{{ ::ctrl.component.typeCode }}</p>\n" +
    "        <p class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__visibility\">{{'se.cms.editortabset.visibilitytab.title' | translate}} {{ctrl.componentVisbilitySwitch | translate}} / {{ctrl.componentRestrictionsCount}} {{'se.cms.restrictions.editor.tab' | translate}}</p>\n" +
    "    </div>\n" +
    "    <span data-ng-if=\"ctrl.component.isExternal === true\"\n" +
    "        class=\"hyicon hyicon-globe slot-visiblity-component--globe-icon\"></span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityWidgetTemplate.html',
    "<slot-visibility-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"\n" +
    "    data-set-remain-open=\"ctrl.setRemainOpen(button, remainOpen)\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSyncButtonTemplate.html',
    "<div class=\"slot-sync-button-template\"\n" +
    "    data-ng-if=\"ctrl.isContentCatalogVersionNonActive\">\n" +
    "    <div class=\"btn-group se-slot-ctx-menu__btn-group\"\n" +
    "        data-uib-dropdown\n" +
    "        data-dropdown-append-to-body\n" +
    "        data-auto-close=\"outsideClick\"\n" +
    "        data-is-open=\"ctrl.isPopupOpened\">\n" +
    "        <button type=\"button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            class=\"btn btn-primary se-slot-ctx-menu__btn se-slot-ctx-menu__divider\"\n" +
    "            data-ng-class=\"{'ySESyncOpen': ctrl.isPopupOpened}\"\n" +
    "            id=\"slot-sync-button-{{::ctrl.slotId}}\">\n" +
    "            <span class=\"hyicon hyicon-sync se-slot-ctx-menu__btn__sync\"></span>\n" +
    "            <span data-ng-if=\"!ctrl.isSlotInSync\"\n" +
    "                class=\"hyicon hyicon-caution se-slot-ctx-menu__btn__caution\"></span>\n" +
    "            </span>\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu dropdown-menu-right btn-block se-toolbar-menu-content se-toolbar-menu-content__page-sync slot-sync-button-template__menu\"\n" +
    "            data-uib-dropdown-menu\n" +
    "            role=\"menu\">\n" +
    "            <li role=\"menuitem\">\n" +
    "                <div class=\"se-toolbar-menu-content--wrapper\">\n" +
    "                    <div class=\"se-toolbar-menu-content--header\">\n" +
    "                        <div class=\"se-toolbar-menu-content--header__title\"\n" +
    "                            data-translate=\"se.cms.synchronization.slot.header\">\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"se-toolbar-menu-content--body\">\n" +
    "                        <slot-synchronization-panel data-ng-if=\"ctrl.isPopupOpened\"\n" +
    "                            data-slot-id=\"ctrl.slotId\"></slot-synchronization-panel>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSyncTemplate.html',
    "<slot-sync-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"\n" +
    "    data-set-remain-open=\"ctrl.setRemainOpen(button, remainOpen)\"></slot-sync-button>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSynchronizationPanelTemplate.html',
    "<div data-page-sensitive>\n" +
    "    <synchronization-panel data-item-id=\"slotSync.slotId\"\n" +
    "        data-get-sync-status=\"slotSync.getSyncStatus\"\n" +
    "        data-perform-sync=\"slotSync.performSync\">\n" +
    "    </synchronization-panel>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/syncIndicatorDecorator/syncIndicatorDecoratorTemplate.html',
    "<div class=\"sync-indicator-decorator\"\n" +
    "    data-ng-class=\"ctrl.syncStatus.status\"\n" +
    "    data-sync-status=\"{{ctrl.syncStatus.status}}\">\n" +
    "    <div class=\"yWrapperData\"\n" +
    "        data-ng-transclude></div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<!-- temporary styling -->\n" +
    "<style>\n" +
    ".sync-indicator-decorator:not(.IN_SYNC):not(.UNAVAILABLE) .decorator-slot-border {\n" +
    "    border: 2px dashed #0486e0;\n" +
    "    /* @smartedit-blue */\n" +
    "}\n" +
    "/* or */\n" +
    "/* .sync-indicator-decorator:not([data-sync-status=\"IN_SYNC\"]):not([data-sync-status=\"UNAVAILABLE\"]) .decorator-slot-border{\n" +
    "    border: 2px dashed #0486e0;\n" +
    "}\n" +
    "*/\n" +
    "</style>"
  );

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */


/**
 * @ngdoc overview
 * @name synchronizationConstantsModule
 * @description
 * Contains various constants used by the synchronization modules.
 *
 * * {@link synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES SYNCHRONIZATION_STATUSES}
 * * {@link synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING SYNCHRONIZATION_POLLING}
 *
 */
angular.module('synchronizationConstantsModule', [])

/**
 * @ngdoc object
 * @name synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES
 *
 * @description
 * Constant containing the different sync statuses
 * * UNAVAILABLE
 * * IN_SYNC
 * * NOT_SYNC
 * * IN_PROGRESS
 * * SYNC_FAILED
 * 
 */
.constant(
    "SYNCHRONIZATION_STATUSES", {
        "UNAVAILABLE": "UNAVAILABLE",
        "IN_SYNC": "IN_SYNC",
        "NOT_SYNC": "NOT_SYNC",
        "IN_PROGRESS": "IN_PROGRESS",
        "SYNC_FAILED": "SYNC_FAILED"
    }
)


/**
 * @ngdoc object
 * @name synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING
 *
 * @description
 * Constant containing polling related values
 * * `SLOW_POLLING_TIME` : the slow polling time in milliseconds (60000ms)
 * * `FAST_POLLING_TIME` : the slow polling time in milliseconds (60000ms)
 * * `SPEED_UP` : event used to speed up polling (`syncPollingSpeedUp`)
 * * `SLOW_DOWN` : event used to slow down polling (`syncPollingSlowDown`)
 * * `FAST_FETCH` : event used to trigger a sync fetch (`syncFastFetch`)
 * * `FETCH_SYNC_STATUS_ONCE`: event used to trigger a one time sync (`fetchSyncStatusOnce`)
 *
 */
.constant(
    "SYNCHRONIZATION_POLLING", {
        "SLOW_POLLING_TIME": 20000,
        "FAST_POLLING_TIME": 2000,
        "SPEED_UP": "syncPollingSpeedUp",
        "SLOW_DOWN": "syncPollingSlowDown",
        "FAST_FETCH": "syncFastFetch",
        "FETCH_SYNC_STATUS_ONCE": "fetchSyncStatusOnce"
    }
);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('synchronizationPanelModule', [
        'yHelpModule',
        'translationServiceModule',
        'alertServiceModule',
        'timerModule',
        'alertsBoxModule',
        'eventServiceModule',
        'synchronizationPollingServiceModule',
        'crossFrameEventServiceModule',
        'waitDialogServiceModule',
        'synchronizationConstantsModule',
        'yMessageModule',
        'sharedDataServiceModule',
        'catalogServiceModule',
        'l10nModule'
    ])
    .controller('SynchronizationPanelController', ['$attrs', '$translate', 'isBlank', 'SYNCHRONIZATION_POLLING', 'SYNCHRONIZATION_STATUSES', 'alertService', 'systemEventService', 'timerService', 'crossFrameEventService', 'waitDialogService', 'catalogService', 'l10nFilter', 'sharedDataService', function($attrs, $translate, isBlank, SYNCHRONIZATION_POLLING, SYNCHRONIZATION_STATUSES, alertService, systemEventService, timerService, crossFrameEventService, waitDialogService, catalogService, l10nFilter, sharedDataService) {

        this.syncPendingItems = [];
        this.selectedItems = [];
        this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
        this.getRows = function() {
            return this.syncStatus ? [this.syncStatus].concat(this.syncStatus.selectedDependencies) : [];
        };
        this.selectionChange = function(index) {

            this.syncStatus.selectedDependencies.forEach(function(item) {
                item.selected = (index === 0 && item.status !== SYNCHRONIZATION_STATUSES.IN_SYNC && !item.isExternal) ? true : item.selected;
            }.bind(this));

            this.selectedItems = this.getRows().filter(function(item) {
                return item.selected;
            });

            if (this.onSelectedItemsUpdate) {
                this.onSelectedItemsUpdate(this.selectedItems);
            }

        };
        this.preserveSelection = function(selectedItems) {

            var itemIds = (selectedItems || []).map(function(item) {
                return item.itemId;
            }, []);

            this.getRows().forEach(function(item) {
                item.selected = itemIds.indexOf(item.itemId) > -1 && item.status !== SYNCHRONIZATION_STATUSES.IN_SYNC;
            });
        };
        this.isDisabled = function(item) {
            return (item !== this.syncStatus && this.syncStatus.selected) || item.status === SYNCHRONIZATION_STATUSES.IN_SYNC;
        };

        this.isSyncButtonDisabled = function() {
            return (this.selectedItems.length === 0 || this.syncPendingItems.length !== 0);
        };
        this.isInSync = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.IN_SYNC;
        };
        this.isOutOfSync = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.NOT_IN_SYNC;
        };
        this.isInProgress = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.IN_PROGRESS;
        };
        this.isSyncFailed = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.SYNC_FAILED;
        };
        this.hasHelp = function(dependency) {
            return dependency.dependentItemTypesOutOfSync && dependency.dependentItemTypesOutOfSync.length > 0;
        };
        this.buildInfoTemplate = function(dependency) {
            if (dependency && dependency.dependentItemTypesOutOfSync && !dependency.isExternal) {
                var infoTemplate = dependency.dependentItemTypesOutOfSync.reduce(function(accumulator, item) {
                    accumulator += ("<br/>" + $translate.instant(item.i18nKey));
                    return accumulator;
                }, "");
                return "<div class='se-synchronization-info-template'>" + infoTemplate + "</div>";
            } else if (dependency.isExternal) {
                return dependency.catalogVersionNameTemplate;
            }
        };

        this.fetchSyncStatus = function(eventId, eventData) {
            if (eventData && eventData.itemId !== this.itemId) {
                return;
            }

            return this.getSyncStatus(this.itemId).then(function(syncStatus) {
                sharedDataService.get('experience').then(function(experience) {
                    var targetCatalogVersion = syncStatus.catalogVersionUuid;
                    this.syncStatus = syncStatus;
                    this.preserveSelection(this.selectedItems);
                    this.selectionChange();
                    this._updateStatus();
                    this.markExternalComponents(targetCatalogVersion, this.getRows());
                    this.setTemplateExternalCatalogVersionName(experience, this.getRows());
                    if (this.syncPendingItems.length === 0) {
                        this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
                        this.resynchTimer.restart(this.refreshInterval);
                        systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SLOW_DOWN, this.itemId);
                    }
                }.bind(this));
            }.bind(this));
        };

        this.markExternalComponents = function(targetCatalogVersion, syncStatuses) {
            syncStatuses.forEach(function(syncStatus) {
                syncStatus.isExternal = syncStatus.catalogVersionUuid !== targetCatalogVersion;
            });
        };

        this.getInfoTitle = function(dependency) {
            if (!dependency.isExternal) {
                return "se.cms.synchronization.panel.update.title";
            }
        };

        this.getTemplateInfoForExternalComponent = function() {
            return "<div class='se-sync-panel--popover'>" + $translate.instant('se.cms.synchronization.slot.external.component') + "</div>";
        };

        this.setTemplateExternalCatalogVersionName = function(experience, syncStatuses) {
            syncStatuses.forEach(function(syncStatus) {
                syncStatus.catalogVersionNameTemplate = "<span></span>";
                catalogService.getCatalogVersionByUuid(syncStatus.catalogVersionUuid, experience.siteDescriptor.uid).then(function(catalogVersion) {
                    syncStatus.catalogVersionNameTemplate = "<div class='se-sync-panel--popover'>" + l10nFilter(catalogVersion.catalogName) + "</div>";
                });
            });
        };

        this._updateStatus = function() {
            var anyFailures = false;

            var itemsInErrors = '';
            var preNbSyncPendingItems = this.syncPendingItems.length;
            this.getRows().forEach(function(item) {
                if (this.syncPendingItems.indexOf(item.itemId) > -1 && item.status === SYNCHRONIZATION_STATUSES.SYNC_FAILED) {
                    itemsInErrors = ' ' + itemsInErrors + item.itemId;
                    anyFailures = true;
                }
                if (this.syncPendingItems.indexOf(item.itemId) > -1 && item.status !== SYNCHRONIZATION_STATUSES.IN_PROGRESS) {
                    this.syncPendingItems.splice(this.syncPendingItems.indexOf(item.itemId), 1);
                }

                // if there was at least one item in the sync queue, and the last item has been resolved
                // or if there wasn't any item in the sync queue
                if ((preNbSyncPendingItems && this.syncPendingItems.length === 0) || preNbSyncPendingItems === 0) {
                    preNbSyncPendingItems = 0;
                    waitDialogService.hideWaitModal();
                }
                if (item.status === SYNCHRONIZATION_STATUSES.IN_PROGRESS && this.syncPendingItems.indexOf(item.itemId) === -1) {
                    this.syncPendingItems.push(item.itemId);
                }
            }.bind(this));

            if (anyFailures) {
                alertService.showDanger({
                    message: $translate.instant('se.cms.synchronization.panel.failure.message', {
                        items: itemsInErrors
                    })
                });
            }

        };

        this.$onInit = function() {
            this.syncItems = function() {

                var syncPayload = this.getRows().filter(function(item) {
                    return item.selected;
                }).map(function(item) {
                    return {
                        itemId: item.itemId,
                        itemType: item.itemType
                    };
                });
                Array.prototype.push.apply(this.syncPendingItems, syncPayload.map(function(el) {
                    return el.itemId;
                }));

                if (this.selectedItems.length > 0) {
                    waitDialogService.showWaitModal("se.sync.synchronizing");
                    this.performSync(syncPayload).then(function() {
                        this.refreshInterval = SYNCHRONIZATION_POLLING.FAST_POLLING_TIME;
                        this.resynchTimer.restart(this.refreshInterval);
                        systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SPEED_UP, this.itemId);
                    }.bind(this));
                }
            }.bind(this);

            this.unSubscribeFastFetch = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, this.fetchSyncStatus.bind(this));
            this.fetchSyncStatus();

            //start timer polling
            this.resynchTimer = timerService.createTimer(this.fetchSyncStatus.bind(this), this.refreshInterval);
            this.resynchTimer.start();
        };

        this.$onDestroy = function() {
            this.resynchTimer.teardown();
            this.unSubscribeFastFetch();
            systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SLOW_DOWN, this.itemId);
        };

        this.$postLink = function() {
            this.showSyncButton = isBlank($attrs.syncItems);
        };
    }])
    /**
     * @ngdoc directive
     * @name synchronizationPanelModule.component:synchronizationPanel
     * @scope
     * @restrict E
     * @element synchronization-panel
     *
     * @description
     * This component reads and performs synchronization for a main object and its dependencies (from a synchronization perspective).
     * The component will list statuses of backend-configured dependent types.
     * The main object and the listed dependencies will display as well lists of logical grouping of dependencies (in a popover)
     * that cause the main object or the listed dependency to be out of sync.
     * @param {String} itemId the unique identifier of the main object of the synchronization panel
     * @param {Function} getSyncStatus the callback, invoked with itemId that returns a promise of the aggregated sync status required for the component to initialize.
     * the expected format of the aggregated status is the following:
     * ````
     *{
     *  itemId:'someUid',
     *  itemType:'AbstractPage',
     *  name:'Page1',
     *  lastSyncTime: long,
     *  selectAll: 'some.key.for.first.row.item' // Some i18nKey to display the title for the first item
     *  status:{name: 'IN_SYNC', i18nKey: 'some.key.for.in.sync'}  //'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *  dependentItemTypesOutOfSync:[
     *                          {code: 'MetaData', i18nKey: 'some.key.for.MetaData'},
     *                          {code: 'Restrictions', i18nKey: 'some.key.for.Restrictions'},
     *                          {code: 'Slot', i18nKey: 'some.key.for.Slot'},
     *                          {code: 'Component', i18nKey: 'some.key.for.Component'},
     *                          {code: 'Navigation', i18nKey: 'some.key.for.Navigation'},
     *                          {code: 'Customization', i18nKey: 'some.key.for.Customization'}
     *                          ],
     *  selectedDependencies:[
     *  {
     *      itemId:'someUid',
     *      itemType:'ContentSlot',
     *      name:'Slot1',
     *      lastSyncTime: long,
     *      selectAll: 'some.key.for.first.row.item' // Some i18nKey to display the title for the first item
     *      status:{name: 'IN_SYNC', i18nKey: 'some.key.for.in.sync'}  //'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *      dependentItemTypesOutOfSync:[
     *                              {code: 'Component', i18nKey: 'some.key.for.Component'},
     *                              {code: 'Navigation', i18nKey: 'some.key.for.Navigation'},
     *                              {code: 'Customization', i18nKey: 'some.key.for.Customization'}
     *                              ]
     *  }
     *  ]
     *}
     * ````
     *  @param {Function} performSync the function invoked with the list of item unique identifiers to perform synchronization. It returns a promise of the resulting sync status with the following values : 'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *  @param {String} headerTemplateUrl the path to an HTML template to customize the header of the synchronization panel. Optional.
     *  @param {Function} syncItems enables parent directive/component to invoke the internal synchronization. Optional, if not set, the panel will display a sync button for manual synchronization.
     *  @param {Function} [onSelectedItemsUpdate] callback function invoked when the the list of selected items change
     */
    .component('synchronizationPanel', {

        templateUrl: 'synchronizationPanelTemplate.html',
        controller: 'SynchronizationPanelController',
        controllerAs: 'sync',
        bindings: {
            itemId: '<',
            getSyncStatus: '<',
            performSync: '<',
            headerTemplateUrl: '<?',
            syncItems: '=?',
            onSelectedItemsUpdate: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("configModule", [])
    .config(['$locationProvider', '$qProvider', function($locationProvider, $qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name assetsServiceModule
 * @description
 * # The assetsServiceModule
 *
 * The assetsServiceModule provides methods to handle assets such as images
 *
 */
angular.module('assetsServiceModule', [])
    /**
     * @ngdoc object
     * @name cmsConstantsModule.service:assetsService
     *
     * @description
     * returns the assets resources root depending whether or not we are in test mode
     */
    .factory('assetsService', ['$injector', '$window', function($injector, $window) {
        return {
            getAssetsRoot: function() {
                var testAssets = false;

                if ($injector.has('testAssets')) {
                    testAssets = $injector.get('testAssets');
                }

                return (testAssets || $window.testAssets) ? '/web/webroot' : '/cmssmartedit';
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name cmsItemsRestServiceModule
 * 
 * @description
 * The cmsitemsRestServiceModule provides a service to CRUD operations on CMS Items API.
 */
angular.module('cmsitemsRestServiceModule', [
    'functionsModule',
    'resourceLocationsModule',
    'restServiceFactoryModule',
    'catalogServiceModule',
    'yLoDashModule'
])

.provider('cmsitemsUri', ['CONTEXT_SITE_ID', function(CONTEXT_SITE_ID) {

    this.uri = '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/cmsitems';

    this.$get = function() {
        return this.uri;
    }.bind(this);

}])

/**
 * @ngdoc service
 * @name cmsitemsRestServiceModule.cmsitemsRestService
 * 
 * @description
 * Service to deal with CMS Items related CRUD operations.
 */
.service('cmsitemsRestService', ['lodash', 'restServiceFactory', 'cmsitemsUri', 'catalogService', 'operationContextService', 'OPERATION_CONTEXT', function(lodash, restServiceFactory, cmsitemsUri, catalogService, operationContextService, OPERATION_CONTEXT) {

    var resource = restServiceFactory.get(cmsitemsUri);

    operationContextService.register(cmsitemsUri.replace(/CONTEXT_SITE_ID/, ':CONTEXT_SITE_ID'), OPERATION_CONTEXT.CMS);

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#create
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Create a new CMS Item.
     * 
     * @param {Object} cmsitem The object representing the CMS Item to create
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.create = function(cmsitem) {
        return catalogService.getCatalogVersionUUid().then(function(catalogVersionUUid) {
            cmsitem.catalogVersion = cmsitem.catalogVersion || catalogVersionUUid;
            if (cmsitem.onlyOneRestrictionMustApply === undefined) {
                cmsitem.onlyOneRestrictionMustApply = false;
            }
            return resource.save(cmsitem);
        });
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#getById
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Get the CMS Item that matches the given item uuid (Universally Unique Identifier).
     * 
     * @param {Number} cmsitemUuid The CMS Item uuid
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.getById = function(cmsitemUuid) {
        return resource.getById(cmsitemUuid);
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#get
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Fetch CMS Items serach result by making a REST call to the CMS Items API.
     * A search can be performed by a typeCode (optionnaly in combination of a mask parameter), or by providing a list of cms items uuid.
     *
     * @param {Object} queryParams The object representing the query params
     * @param {String} queryParams.pageSize number of items in the page
     * @param {String} queryParams.currentPage current page number
     * @param {String =} queryParams.typeCode for filtering on the cms item typeCode
     * @param {String =} queryParams.mask for filtering the search
     * @param {String =} queryParams.itemSearchParams search on additional fields using a comma separated list of field name and value
     * pairs which are separated by a colon. Exact matches only.
     * @param {Array =} queryParams.uuids list of cms items uuids
     * @param {String =} queryParams.catalogId the catalog to search items in. If empty, the current context catalog will be used.
     * @param {String =} queryParams.catalogVersion the catalog version to search items in. If empty, the current context catalog version will be used.
     *
     * @returns {Promise} If request is successful, it returns a promise that resolves with the paged search result. If the
     * request fails, it resolves with errors from the backend.
     */
    this.get = function(queryParams) {
        return catalogService.retrieveUriContext().then(function(uriContext) {

            var catalogDetailsParams = {
                catalogId: queryParams.catalogId || uriContext.CURRENT_CONTEXT_CATALOG,
                catalogVersion: queryParams.catalogVersion || uriContext.CURRENT_CONTEXT_CATALOG_VERSION
            };

            queryParams = lodash.merge(catalogDetailsParams, queryParams);

            return resource.get(queryParams);
        });
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#update
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Update a CMS Item.
     * 
     * @param {Object} cmsitem The object representing the CMS Item to update
     * @param {String} cmsitem.identifier The cms item identifier (uuid)
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the updated CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.update = function(cmsitem) {
        return resource.update(cmsitem);
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#delete
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Remove a CMS Item.
     * 
     * @param {Number} cmsitemUuid The CMS Item uuid
     */
    this.delete = function(cmsitemUuid) {
        return resource.delete(cmsitemUuid);
    };


    this._getByUIdAndType = function(uid, typeCode) {
        return this.get({
            pageSize: 100,
            currentPage: 0,
            mask: uid,
            typeCode: typeCode
        }).then(function(response) {
            return response.response.find(function(element) {
                return element.uid === uid;
            });
        });
    };


}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name resourceModule
     *
     * @description
     * The resource module provides $resource factories.
     */
    angular.module('resourceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'functionsModule'])
        /**
         * @ngdoc service
         * @name resourceModule.service:itemsResource
         *
         * @description
         * This service is used to retrieve the $resource factor for retrieving component items.
         */
        .factory('itemsResource', ['restServiceFactory', 'ITEMS_RESOURCE_URI', 'URIBuilder', function(restServiceFactory, ITEMS_RESOURCE_URI, URIBuilder) {

            return {
                /**
                 * @ngdoc method
                 * @name resourceModule.service:itemsResource#getItemResourceByContext
                 * @methodOf resourceModule.service:itemsResource
                 * 
                 * @description
                 * Returns  the resource of the custom components REST service by replacing the placeholders with the currently selected catalog version.
                 */
                getItemResource: function() {
                    return restServiceFactory.get(ITEMS_RESOURCE_URI);
                },

                /**
                 * @ngdoc method
                 * @name resourceModule.service:itemsResource#getItemResourceByContext
                 * @methodOf resourceModule.service:itemsResource
                 * 
                 * @description
                 * Returns  the resource of the custom components REST service by providing the current uri context as the input object.
                 * 
                 * The input object contains the necessary site and catalog information to retrieve the items.
                 * 
                 * @param {Object} uriContext A  {@link resourceLocationsModule.object:UriContext uriContext}
                 */
                getItemResourceByContext: function(context) {
                    return restServiceFactory.get(new URIBuilder(ITEMS_RESOURCE_URI).replaceParams(context).build());
                }

            };

        }])
        .factory('navigationNodeEntriesResource', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI, 'uid');
        }])
        .factory('pagesContentSlotsComponentsResource', ['restServiceFactory', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', function(restServiceFactory, PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI) {
            return restServiceFactory.get(PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI);
        }])
        // @deprecated since 6.5
        .factory('pageInfoRestService', ['restServiceFactory', 'PAGEINFO_RESOURCE_URI', function(restServiceFactory, PAGEINFO_RESOURCE_URI) {
            return restServiceFactory.get(PAGEINFO_RESOURCE_URI);
        }])
        .factory('navigationNodeRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_RESOURCE_URI);
        }])
        .factory('navigationEntriesRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI);
        }])
        .factory('navigationEntryTypesRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI);
        }])
        .factory('synchronizationResource', ['restServiceFactory', 'URIBuilder', 'GET_PAGE_SYNCHRONIZATION_RESOURCE_URI', 'POST_PAGE_SYNCHRONIZATION_RESOURCE_URI', function(restServiceFactory, URIBuilder, GET_PAGE_SYNCHRONIZATION_RESOURCE_URI, POST_PAGE_SYNCHRONIZATION_RESOURCE_URI) {

            return {

                getPageSynchronizationGetRestService: function(uriContext) {
                    var getURI = new URIBuilder(GET_PAGE_SYNCHRONIZATION_RESOURCE_URI).replaceParams(uriContext).build();
                    return restServiceFactory.get(getURI);
                },

                getPageSynchronizationPostRestService: function(uriContext) {
                    var postURI = new URIBuilder(POST_PAGE_SYNCHRONIZATION_RESOURCE_URI).replaceParams(uriContext).build();
                    return restServiceFactory.get(postURI);
                }

            };

        }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    var CONTEXT_CATALOG = 'CURRENT_CONTEXT_CATALOG',
        CONTEXT_CATALOG_VERSION = 'CURRENT_CONTEXT_CATALOG_VERSION',
        CONTEXT_SITE_ID = 'CURRENT_CONTEXT_SITE_ID';

    var PAGE_CONTEXT_CATALOG = 'CURRENT_PAGE_CONTEXT_CATALOG',
        PAGE_CONTEXT_CATALOG_VERSION = 'CURRENT_PAGE_CONTEXT_CATALOG_VERSION',
        PAGE_CONTEXT_SITE_ID = 'CURRENT_PAGE_CONTEXT_SITE_ID';

    angular.module('resourceLocationsModule')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_SITE_ID
     *
     * @description
     * Constant containing the name of the site uid placeholder in URLs
     */
    .constant('CONTEXT_SITE_ID', CONTEXT_SITE_ID)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_CATALOG
     *
     * @description
     * Constant containing the name of the catalog uid placeholder in URLs
     */
    .constant('CONTEXT_CATALOG', CONTEXT_CATALOG)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_CATALOG_VERSION
     *
     * @description
     * Constant containing the name of the catalog version placeholder in URLs
     */
    .constant('CONTEXT_CATALOG_VERSION', CONTEXT_CATALOG_VERSION)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID
     *
     * @description
     * Constant containing the name of the current page site uid placeholder in URLs
     */
    .constant('PAGE_CONTEXT_SITE_ID', PAGE_CONTEXT_SITE_ID)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG
     *
     * @description
     * Constant containing the name of the current page catalog uid placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG', PAGE_CONTEXT_CATALOG)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION
     *
     * @description
     * Constant containing the name of the current page catalog version placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG_VERSION', PAGE_CONTEXT_CATALOG_VERSION)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:UriContext
     *
     * @description
     * A map that contains the necessary site and catalog information for CMS services and directives.
     * It contains the following keys:
     * {@link resourceLocationsModule.object:CONTEXT_SITE_ID CONTEXT_SITE_ID} for the site uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG CONTEXT_CATALOG} for the catalog uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG_VERSION CONTEXT_CATALOG_VERSION} for the catalog version.
     */

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:TYPES_RESOURCE_URI
     *
     * @description
     * Resource URI of the component types REST service.
     */
    .constant('TYPES_RESOURCE_URI', '/cmswebservices/v1/types')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:STRUCTURES_RESOURCE_URI
     *
     * @description
     * Resource URI of the component structures REST service.
     */
    .constant('STRUCTURES_RESOURCE_URI', '/cmssmarteditwebservices/v1/structures')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:ITEMS_RESOURCE_URI
     *
     * @description
     * Resource URI of the custom components REST service.
     */
    .constant('ITEMS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/items')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages content slot component REST service.
     */
    .constant('PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagescontentslotscomponents')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI
     *
     * @description
     * Resource URI of the content slot type restrictions REST service.
     */
    .constant('CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI', '/cmswebservices/v1/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pages/:pageUid/contentslots/:slotUid/typerestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsMod`ule.object:PAGES_LIST_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages REST service.
     */
    .constant('PAGES_LIST_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_LIST_PATH
     *
     * @description
     * Path of the page list
     */
    .constant('PAGE_LIST_PATH', '/pages/:siteId/:catalogId/:catalogVersion')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGES_CONTENT_SLOT_RESOURCE_URI
     *
     * @description
     * Resource URI of the page content slots REST service
     */
    .constant('PAGES_CONTENT_SLOT_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagescontentslots')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_TEMPLATES_URI
     *
     * @description
     * Resource URI of the page templates REST service
     */
    .constant('PAGE_TEMPLATES_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/pagetemplates')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_PAGE_PATH
     *
     * @description
     * Path to the Navigation Management
     */
    .constant('NAVIGATION_MANAGEMENT_PAGE_PATH', '/navigations/:siteId/:catalogId/:catalogVersion')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigations REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_RESOURCE_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/navigations')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigations REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/navigations/:navigationUid/entries')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigation entry types REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI', '/cmswebservices/v1/navigationentrytypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the pages REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('CONTEXTUAL_PAGES_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagesrestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/:siteUID/catalogs/:catalogId/versions/:catalogVersion/pagesrestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pagesrestrictions/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/restrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTION_TYPES_URI
     *
     * @description
     * Resource URI of the restriction types REST service.
     */
    .constant('RESTRICTION_TYPES_URI', '/cmswebservices/v1/restrictiontypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTION_TYPES_URI
     *
     * @description
     * Resource URI of the pageTypes-restrictionTypes relationship REST service.
     */
    .constant('PAGE_TYPES_RESTRICTION_TYPES_URI', '/cmswebservices/v1/pagetypesrestrictiontypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGEINFO_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the page info REST service.
     */
    .constant('PAGEINFO_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages/:pageUid')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGE_TYPES_URI
     *
     * @description
     * Resource URI of the page types REST service.
     */
    .constant('PAGE_TYPES_URI', '/cmswebservices/v1/pagetypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:GET_PAGE_SYNCHRONIZATION_RESOURCE_URI
     *
     * @description
     * Resource URI to retrieve the full synchronization status of page related items
     */
    .constant('GET_PAGE_SYNCHRONIZATION_RESOURCE_URI', '/cmssmarteditwebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/synchronizations/versions/:target/pages/:pageUid')


    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:POST_PAGE_SYNCHRONIZATION_RESOURCE_URI
     *
     * @description
     * Resource URI to perform synchronization of page related items
     */
    .constant('POST_PAGE_SYNCHRONIZATION_RESOURCE_URI', '/cmssmarteditwebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/synchronizations/versions/:target');
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentServiceModule', [
    'restServiceFactoryModule',
    'resourceLocationsModule',
    'cmsitemsRestServiceModule',
    'catalogServiceModule',
    'slotRestrictionsServiceModule'
])

/**
 * @ngdoc service
 * @name componentMenuModule.ComponentService
 *
 * @description
 * Service which manages component types and items
 */
.service('ComponentService', ['restServiceFactory', 'TYPES_RESOURCE_URI', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'cmsitemsRestService', 'catalogService', 'slotRestrictionsService', function(
    restServiceFactory,
    TYPES_RESOURCE_URI,
    PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI,
    CONTEXT_CATALOG,
    CONTEXT_CATALOG_VERSION,
    cmsitemsRestService,
    catalogService,
    slotRestrictionsService) {

    var restServiceForTypes = restServiceFactory.get(TYPES_RESOURCE_URI);
    var restServiceForItems = cmsitemsRestService;
    var restServiceForAddExistingComponent = restServiceFactory.get(PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI);

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#createNewComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description given a component info and the component payload, a new componentItem is created and added to a slot
     *
     * @param {Object} componentInfo The basic information of the ComponentType to be created and added to the slot.
     * @param {String} componentInfo.componenCode componenCode of the ComponentType to be created and added to the slot.
     * @param {String} componentInfo.name name of the new component to be created.
     * @param {String} componentInfo.pageId pageId used to identify the current page template.
     * @param {String} componentInfo.slotId slotId used to identify the slot in the current template.
     * @param {String} componentInfo.position position used to identify the position in the slot in the current template.
     * @param {String} componentInfo.type type of the component being created.
     * @param {Object} componentPayload payload of the new component to be created.
     */
    this.createNewComponent = function(componentInfo, componentPayload) {

        //FIXME fix naming of slotId
        var _payload = {};
        _payload.name = componentInfo.name;
        _payload.slotId = componentInfo.targetSlotId;
        _payload.pageId = componentInfo.pageId;
        _payload.position = componentInfo.position;
        _payload.typeCode = componentInfo.componentType;
        _payload.itemtype = componentInfo.componentType;
        _payload.catalogVersion = componentInfo.catalogVersionUuid;

        if (typeof componentPayload === "object") {
            for (var property in componentPayload) {
                _payload[property] = componentPayload[property];
            }
        } else if (componentPayload) {
            throw "ComponentService.createNewComponent() - Illegal componentPayload - [" + componentPayload + "]";
        }

        return restServiceForItems.create(_payload);

    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#updateComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description Given a component info and the payload related to an existing component, the latter will be updated with the new supplied values.
     *
     * @param {Object} componentPayload of the new component to be created, including the info.
     * @param {String} componentPayload.componenCode of the ComponentType to be created and added to the slot.
     * @param {String} componentPayload.name of the new component to be created.
     * @param {String} componentPayload.pageId used to identify the current page template.
     * @param {String} componentPayload.slotId used to identify the slot in the current template.
     * @param {String} componentPayload.position used to identify the position in the slot in the current template.
     * @param {String} componentPayload.type of the component being created.
     */
    this.updateComponent = function(componentPayload) {
        return restServiceForItems.update(componentPayload);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#addExistingComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description add an existing component item to a slot
     *
     * @param {String} pageId used to identify the page containing the slot in the current template.
     * @param {String} componentId used to identify the existing component which will be added to the slot.
     * @param {String} slotId used to identify the slot in the current template.
     * @param {String} position used to identify the position in the slot in the current template.
     */
    this.addExistingComponent = function(pageId, componentId, slotId, position) {

        var _payload = {};
        _payload.pageId = pageId;
        _payload.slotId = slotId;
        _payload.componentId = componentId;
        _payload.position = position;

        return restServiceForAddExistingComponent.save(_payload);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadComponentTypes
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all component types are retrieved
     */
    this.loadComponentTypes = function() {
        return restServiceForTypes.get({
            category: 'COMPONENT'
        });
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#getSupportedComponentTypesForCurrentPage
     * @methodOf componentMenuModule.ComponentService
     *
     * @description Fetches all component types supported by the system, then filters this list
     * using the restricted component types for all the slots on the current page.
     *
     * @returns {Array} A promise resolving to the component types that can be added to the current page
     */
    this.getSupportedComponentTypesForCurrentPage = function() {
        return this.loadComponentTypes().then(function(response) {
            return slotRestrictionsService.getAllComponentTypesSupportedOnPage().then(function(supportedTypes) {
                return response.componentTypes.filter(function(componentType) {
                    return supportedTypes.indexOf(componentType.code) > -1;
                });
            });
        });
    }.bind(this);

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadComponentItem
     * @methodOf componentMenuModule.ComponentService
     *
     * @description load a component identified by its id
     */
    this.loadComponentItem = function(id) {
        return restServiceForItems.getById(id);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadPagedComponentItems
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all existing component items for the current catalog are retrieved in the form of pages
     * used for pagination especially when the result set is very large.
     * 
     * @param {String} mask the search string to filter the results.
     * @param {String} pageSize the number of elements that a page can contain.
     * @param {String} page the current page number.
     */
    this.loadPagedComponentItems = function(mask, pageSize, page) {

        return catalogService.retrieveUriContext().then(function(uriContext) {

            var requestParams = {
                pageSize: pageSize,
                currentPage: page,
                mask: mask,
                sort: 'name',
                typeCode: 'AbstractCMSComponent',
                catalogId: uriContext[CONTEXT_CATALOG],
                catalogVersion: uriContext[CONTEXT_CATALOG_VERSION]
            };

            return restServiceForItems.get(requestParams);
        });
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadPagedComponentItemsByCatalogVersion
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all existing component items for the provided content catalog are retrieved in the form of pages
     * used for pagination especially when the result set is very large.
     * 
     * @param {Object} payload The payload that contains the information of the page of components to load
     * @param {String} payload.catalogId the id of the catalog for which to retrieve the component items. 
     * @param {String} payload.catalogVersion the id of the catalog version for which to retrieve the component items. 
     * @param {String} payload.mask the search string to filter the results.
     * @param {String} payload.pageSize the number of elements that a page can contain.
     * @param {String} payload.page the current page number.
     * 
     * @returns {Promise} A promise resolving to a page of component items retrieved from the provided catalog version. 
     */
    this.loadPagedComponentItemsByCatalogVersion = function(payload) {
        var requestParams = {
            pageSize: payload.pageSize,
            currentPage: payload.page,
            mask: payload.mask,
            sort: 'name',
            typeCode: 'AbstractCMSComponent',
            catalogId: payload.catalogId,
            catalogVersion: payload.catalogVersion
        };

        return restServiceForItems.get(requestParams);
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name componentVisibilityAlertServiceInterfaceModule
 *
 * @description
 * This module defines the componentVisibilityAlertServiceInterfaceModule Angular component
 * and its associated service. It uses the cmsContextualAlertModule to display
 * a contextual alert whenever a component get either hidden or restricted.
 */
angular.module("componentVisibilityAlertServiceInterfaceModule", [])

/**
 * @ngdoc service
 * @name componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface
 *
 * @description
 * The componentVisibilityAlertServiceInterface is used by external modules to check
 * on a component visibility and trigger the display of a contextual alert when
 * the component is either hidden or restricted.
 */
.factory('ComponentVisibilityAlertServiceInterface', function() {

    var ComponentVisibilityAlertServiceInterface = function() {};

    /**
     * @ngdoc method
     * @name componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface#checkAndAlertOnComponentVisibility
     * @methodOf componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface
     *
     * @description
     * Method checks on a component visibility and triggering the display of a
     * contextual alert when the component is either hidden or restricted. This
     * method defines a custom Angular controller which will get passed to and
     * consumed by the contextual alert.
     *
     * @param {Object} component A JSON object containing the specific configuration to be applied on the actionableAlert.
     * @param {String} component.componentId Uuid of the cmsItem
     * @param {String} component.componentType Type of the cmsItem
     * @param {String} component.catalogVersion CatalogVersion uuid of the cmsItem
     * @param {String} component.restricted Boolean stating whether a restriction is applied to the cmsItem.
     * @param {String} component.slotId Id of the slot where the cmsItem was added or modified.
     * @param {String} component.visibility Boolean stating whether the cmsItem is rendered.
     */
    ComponentVisibilityAlertServiceInterface.prototype.checkAndAlertOnComponentVisibility = function() {};

    return ComponentVisibilityAlertServiceInterface;

});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name removeComponentServiceInterfaceModule
 * @description
 * # The removeComponentServiceInterfaceModule
 *
 * Provides a service with the ability to remove a component from a slot.
 */
angular.module('removeComponentServiceInterfaceModule', [])
    /**
     * @ngdoc service
     * @name removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface
     * @description
     * Service interface specifying the contract used to remove a component from a slot.
     *
     * This class serves as an interface and should be extended, not instantiated.
     */
    .factory('RemoveComponentServiceInterface', function() {
        function RemoveComponentServiceInterface() {}

        /**
         * @ngdoc method
         * @name removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface#removeComponent
         * @methodOf removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface
         *
         * @description
         * Removes the component specified by the given ID from the component specified by the given ID.
         *
         * @param {String} slotId The ID of the slot from which to remove the component.
         * @param {String} componentId The ID of the component to remove from the slot.
         */
        RemoveComponentServiceInterface.prototype.removeComponent = function() {};

        return RemoveComponentServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotVisibilityServiceModule
 * @description
 *
 * The slot visibility service module provides factories and services to manage all backend calls and loads an internal
 * structure that provides the necessary data to the slot visibility button and slot visibility component.
 */
angular.module('slotVisibilityServiceModule', ['resourceModule', 'componentHandlerServiceModule', 'cmsitemsRestServiceModule', 'crossFrameEventServiceModule'])
    /**
     * @ngdoc service
     * @name slotVisibilityServiceModule.service:SlotVisibilityService
     * @description
     *
     * The SlotVisibilityService class provides methods to interact with the backend.
     * The definition of the class is not instantiated immediately, whereas the service instance of
     * this same class (@see slotVisibilityService) returns an instance of this service definition.
     *
     * @param {Object} cmsitemsRestService Gets all components based on their UUIDs.
     * @param {Object} pagesContentSlotsComponentsResource Gets content slots and components based on their page IDs.
     * @param {Object} componentHandlerService Gets the current page ID.
     */
    .service('SlotVisibilityService', ['$q', 'cmsitemsRestService', 'pagesContentSlotsComponentsResource', 'componentHandlerService', function($q, cmsitemsRestService, pagesContentSlotsComponentsResource, componentHandlerService) {

        var SlotVisibilityService = function() {};

        /**
         * Function that filters the given SlotsToComponentsMap to return only those components that are hidden in the storefront.
         * @param {Object} allSlotsToComponentsMap object containing slotId - components list.
         *
         * @return {Object} allSlotsToComponentsMap object containing slotId - components list.
         */
        SlotVisibilityService.prototype._filterVisibleComponents = function(allSlotsToComponentsMap) {

            //filter allSlotsToComponentsMap to show only hidden components
            Object.keys(allSlotsToComponentsMap).forEach(function(slotId) {

                var componentsOnDOM = [];
                componentHandlerService.getOriginalComponentsWithinSlot(slotId).get().forEach(function(component) {
                    componentsOnDOM.push(componentHandlerService.getId(component));
                });

                var hiddenComponents = allSlotsToComponentsMap[slotId].filter(function(component) {
                    return componentsOnDOM.indexOf(component.uid) === -1;
                });

                allSlotsToComponentsMap[slotId] = hiddenComponents;

            });

            return allSlotsToComponentsMap;
        };

        /**
         * Converts the provided list of pageContentSlotsComponents to slotId - components list map.
         * @param {Object} pageContentSlotsComponents object containing list of slots and components for the page in context
         */
        SlotVisibilityService.prototype._loadSlotsToComponentsMap = function(pageContentSlotsComponents) {
            var componentUuids = (pageContentSlotsComponents.pageContentSlotComponentList || [])
                .map(function(pageContentSlotComponent) {
                    return pageContentSlotComponent.componentUuid;
                });
            return cmsitemsRestService.get({
                uuids: componentUuids.join(',')
            }).then(function(components) {

                //load all components as ComponentUuid-Component map
                var allComponentsMap = (components.response || []).reduce(function(map, component) {
                    map[component.uuid] = component;
                    return map;
                }, {});

                //load all components as SlotUuid-Component[] map
                var allSlotsToComponentsMap = (pageContentSlotsComponents.pageContentSlotComponentList || [])
                    .reduce(function(map, pageContentSlotComponent) {
                        map[pageContentSlotComponent.slotId] = map[pageContentSlotComponent.slotId] || [];
                        if (allComponentsMap[pageContentSlotComponent.componentUuid]) {
                            map[pageContentSlotComponent.slotId].push(allComponentsMap[pageContentSlotComponent.componentUuid]);
                        }
                        return map;
                    }, {});

                return allSlotsToComponentsMap;
            });
        };

        SlotVisibilityService.prototype._getPagesContentSlotsComponentsPromise = function() {
            return this.hiddenComponentsMapPromise || this.reloadSlotsInfo();
        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#reloadSlotInfo
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Reloads and cache's the pagesContentSlotsComponents for the current page in context.
         * this method can be called when ever a component is added or modified to the slot so that the pagesContentSlotsComponents is re-evalated.
         *
         * @return {Promise} A promise that resolves to the pagesContentSlotsComponents for the page in context.
         */
        SlotVisibilityService.prototype.reloadSlotsInfo = function() {
            try {
                var currentPageId = componentHandlerService.getPageUID();
                this.hiddenComponentsMapPromise = pagesContentSlotsComponentsResource.get({
                    pageId: currentPageId
                });
                return this.hiddenComponentsMapPromise;
            } catch (e) {
                if (e.name === "InvalidStorefrontPageError") {
                    return $q.when({});
                } else {
                    throw e;
                }
            }

        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#getSlotsForComponent
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Returns an array of slotId's in which the provided component (identified by its componentUuid) exists.
         *
         * @param {String} componentUuid The uuid of the component.
         *
         * @return {Promise} A promise that resolves to a array of slotId's for the given componnet uuid.
         */
        SlotVisibilityService.prototype.getSlotsForComponent = function(componentUuid) {
            var slotIds = [];
            return this._getSlotToComponentsMap(true).then(function(allSlotsToComponentsMap) {
                Object.keys(allSlotsToComponentsMap).forEach(function(slotId) {
                    if (allSlotsToComponentsMap[slotId].find(function(component) {
                            return component.uuid === componentUuid;
                        })) {
                        slotIds.push(slotId);
                    }
                });

                return slotIds;
            });
        };

        /**
         * Function to load slot to component map for the current page in context
         * @param reload boolean to specify if the pagesContentSlotsComponents resource needs to be re-called.
         */
        SlotVisibilityService.prototype._getSlotToComponentsMap = function(reload) {
            return (reload ? this.reloadSlotsInfo() : this._getPagesContentSlotsComponentsPromise()).then(this._loadSlotsToComponentsMap);
        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#getHiddenComponents
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Returns the list of hidden components for a given slotId
         *
         * @param {String} slotId the slot id
         *
         * @return {Promise} A promise that resolves to a list of hidden components for the slotId
         */
        SlotVisibilityService.prototype.getHiddenComponents = function(slotId) {
            return this._getSlotToComponentsMap(false).then(this._filterVisibleComponents).then(function(hiddenComponentsMap) {
                return hiddenComponentsMap[slotId] || [];
            }, function() {
                return [];
            });
        };

        return SlotVisibilityService;

    }])
    .service('slotVisibilityService', ['SlotVisibilityService', 'crossFrameEventService', 'EVENTS', function(SlotVisibilityService, crossFrameEventService, EVENTS) {
        var instance = new SlotVisibilityService();
        crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
            instance._getSlotToComponentsMap(true);
        });
        return instance;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name synchronizationServiceModule
 * @description
 *
 * The synchronization module contains the service necessary to perform catalog synchronization.
 *
 * The {@link synchronizationServiceModule.service:synchronizationService synchronizationService} 
 * calls backend API in order to get synchronization status or trigger a catalog synchronization between two catalog versions.
 *
 */
angular.module('synchronizationServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'alertServiceModule', 'authenticationModule', 'timerModule'])
    /**
     * @ngdoc object
     * @name synchronizationServiceModule.object:CATALOG_SYNC_INTERVAL_IN_MILLISECONDS
     * @description
     * This object defines an injectable Angular constant that determines the frequency to update catalog synchronization information. 
     * Value given in milliseconds. 
     */
    .constant('CATALOG_SYNC_INTERVAL_IN_MILLISECONDS', 5000)
    /**
     * @ngdoc service
     * @name synchronizationServiceModule.service:synchronizationService
     * @description
     *
     * The synchronization service manages RESTful calls to the synchronization service's backend API.
     * 
     */
    .service('synchronizationService', ['restServiceFactory', 'timerService', '$q', '$translate', 'alertService', 'authenticationService', 'operationContextService', 'OPERATION_CONTEXT', 'CATALOG_SYNC_INTERVAL_IN_MILLISECONDS', function(restServiceFactory, timerService, $q, $translate, alertService, authenticationService, operationContextService, OPERATION_CONTEXT, CATALOG_SYNC_INTERVAL_IN_MILLISECONDS) {

        // Constants
        var BASE_URL = "/cmswebservices";
        var SYNC_JOB_INFO_BY_TARGET_URI = '/cmswebservices/v1/catalogs/:catalog/synchronizations/targetversions/:target';
        var SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI = '/cmswebservices/v1/catalogs/:catalog/versions/:source/synchronizations/versions/:target';

        // Variables
        var intervalHandle = {};
        var syncJobInfoByTargetRestService = restServiceFactory.get(SYNC_JOB_INFO_BY_TARGET_URI);
        var syncJobInfoBySourceAndTargetRestService = restServiceFactory.get(SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI, 'catalog');

        operationContextService.register(SYNC_JOB_INFO_BY_TARGET_URI, OPERATION_CONTEXT.CMS);
        operationContextService.register(SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI, OPERATION_CONTEXT.CMS);

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#updateCatalogSync
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to synchronize a catalog between two catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.updateCatalogSync = function(catalog) {
            return syncJobInfoBySourceAndTargetRestService.update({
                'catalog': catalog.catalogId,
                'source': catalog.sourceCatalogVersion,
                'target': catalog.targetCatalogVersion
            }).then(function(response) {
                return response;
            }.bind(this), function(reason) {
                var translationErrorMsg = $translate.instant('sync.running.error.msg', {
                    catalogName: catalog.name
                });
                if (reason.statusText === 'Conflict') {
                    alertService.showDanger({
                        message: translationErrorMsg
                    });
                }
                return false;
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job between two catalog versions. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getCatalogSyncStatus = function(catalog) {
            if (catalog.sourceCatalogVersion) {
                return this.getSyncJobInfoBySourceAndTarget(catalog);
            } else {
                return this.getLastSyncJobInfoByTarget(catalog);
            }
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job between two catalog versions. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getSyncJobInfoBySourceAndTarget = function(catalog) {
            return syncJobInfoBySourceAndTargetRestService.get({
                'catalog': catalog.catalogId,
                'source': catalog.sourceCatalogVersion,
                'target': catalog.targetCatalogVersion
            });
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getLastSyncJobInfoByTarget = function(catalog) {
            return syncJobInfoByTargetRestService.get({
                'catalog': catalog.catalogId,
                'target': catalog.targetCatalogVersion
            });
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#stopAutoGetSyncData
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method starts the auto synchronization status update in a catalog between two given catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.startAutoGetSyncData = function(catalog, callback) {
            var catalogId = catalog.catalogId;
            var sourceCatalogVersion = catalog.sourceCatalogVersion;
            var targetCatalogVersion = catalog.targetCatalogVersion;

            var jobKey = this._getJobKey(catalogId, sourceCatalogVersion, targetCatalogVersion);

            var syncJobTimer = timerService.createTimer(this._autoSyncCallback.bind(this, catalog, callback, jobKey), CATALOG_SYNC_INTERVAL_IN_MILLISECONDS);
            syncJobTimer.start();

            intervalHandle[jobKey] = syncJobTimer;
        };

        this._autoSyncCallback = function(catalog, callback, jobKey) {
            authenticationService.isAuthenticated(BASE_URL).then(function(response) {
                if (!response) {
                    this.stopAutoGetSyncData(catalog);
                }
                this.getCatalogSyncStatus(catalog)
                    .then(callback)
                    .then(function() {
                        if (!intervalHandle[jobKey]) {
                            this.startAutoGetSyncData(catalog, callback);
                        }
                    }.bind(this));
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#stopAutoGetSyncData
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method stops the auto synchronization status update in a catalog between two given catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.stopAutoGetSyncData = function(catalog) {
            var jobKey = this._getJobKey(catalog.catalogId, catalog.sourceCatalogVersion, catalog.targetCatalogVersion);
            if (intervalHandle[jobKey]) {
                intervalHandle[jobKey].stop();
                intervalHandle[jobKey] = undefined;
            }
        };

        this._getJobKey = function(catalogId, sourceCatalogVersion, targetCatalogVersion) {
            return catalogId + "_" + sourceCatalogVersion + "_" + targetCatalogVersion;
        };
    }]);

angular.module('cmscommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmscommons/components/synchronize/synchronizationPanelTemplate.html',
    "<div class=\"se-sync-panel\">\n" +
    "    <y-message data-ng-if=\"false\"\n" +
    "        data-message-id=\"yMsgWarningId\"\n" +
    "        data-type=\"warning\"\n" +
    "        data-ng-class=\"!sync.showSyncButton ? 'se-sync-panel--y-message--modal-adjusted' : 'se-sync-panel--y-message--toolbar-adjusted'\">\n" +
    "        <message-title>{{ 'se.cms.synchronization.panel.live.recent.notice' | translate }}</message-title>\n" +
    "        <message-description>{{ 'se.cms.synchronization.panel.live.override.warning' | translate }}</message-description>\n" +
    "    </y-message>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-status\"\n" +
    "        data-ng-if=\"sync.headerTemplateUrl\"\n" +
    "        data-ng-include=\"sync.headerTemplateUrl\">\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-info\">\n" +
    "        <div data-ng-repeat=\"dependency in sync.getRows()\"\n" +
    "            data-ng-class=\"{active: $index==0, 'se-sync-panel--item__external': dependency.isExternal}\"\n" +
    "            class=\"se-sync-panel__sync-info__row\">\n" +
    "\n" +
    "            <div class=\"checkbox se-sync-panel__sync-info__checkbox se-nowrap-ellipsis\">\n" +
    "                <input type=\"checkbox\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-model=\"dependency.selected\"\n" +
    "                    data-ng-disabled=\"sync.isDisabled(dependency)\"\n" +
    "                    data-ng-change=\"sync.selectionChange($index)\"\n" +
    "                    id=\"sync-info__checkbox_{{$index}}\">\n" +
    "                <label data-ng-if=\"$index===0\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.selectAll | translate}}\">\n" +
    "                    {{::dependency.selectAll | translate}}</label>\n" +
    "\n" +
    "                <label data-ng-if=\"$index!==0 && !dependency.isExternal\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.name | translate}}\">\n" +
    "                    {{::dependency.name | translate}}</label>\n" +
    "\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    data-y-popover\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-template=\"sync.getTemplateInfoForExternalComponent()\">\n" +
    "                    <label data-ng-if=\"$index!==0\"\n" +
    "                        for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                        class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                        title=\"{{::dependency.name | translate}}\">\n" +
    "                        {{::dependency.name | translate}}</label>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "            <span data-ng-if=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-y-popover\n" +
    "                data-trigger=\"'hover'\"\n" +
    "                data-title=\"sync.getInfoTitle(dependency)\"\n" +
    "                data-template=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-ng-class=\"{'pull-right se-sync-panel__sync-info__right-icon': true, 'se-sync-panel--icon-globe': dependency.isExternal} \">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    class=\"hyicon hyicon-globe\"></span>\n" +
    "            </span>\n" +
    "\n" +
    "            <span data-ng-if=\"!sync.buildInfoTemplate(dependency)\"\n" +
    "                class=\"pull-right se-sync-panel__sync-info__right-icon\">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__footer\"\n" +
    "        data-ng-if=\"sync.showSyncButton\">\n" +
    "        <button class=\"btn btn-lg btn-primary se-sync-panel__footer__btn\"\n" +
    "            data-ng-disabled=\"sync.isSyncButtonDisabled()\"\n" +
    "            data-ng-click=\"sync.syncItems()\"\n" +
    "            data-translate=\"se.cms.pagelist.dropdown.sync\"></button>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );

}]);
