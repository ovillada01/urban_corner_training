/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('cmssmarteditContainer', [
        'experienceInterceptorModule',
        'resourceLocationsModule',
        'cmssmarteditContainerTemplates',
        'featureServiceModule',
        'componentMenuModule',
        'cmscommonsTemplates',
        'restrictionsMenuModule',
        'pageInfoMenuModule',
        'editorModalServiceModule',
        'genericEditorModule',
        'eventServiceModule',
        'catalogDetailsModule',
        'synchronizeCatalogModule',
        'perspectiveServiceModule',
        'pageListLinkModule',
        'pageListControllerModule',
        'clientPagedListModule',
        'assetsServiceModule',
        'navigationEditorModule',
        'slotRestrictionsServiceModule',
        'cmsDragAndDropServiceModule',
        'seMediaFieldModule',
        'seMediaContainerFieldModule',
        'editorFieldMappingServiceModule',
        'navigationNodeEditorModule',
        'entrySearchSelectorModule',
        'pageRestrictionsModule',
        'restrictionsEditorModule',
        'yActionableSearchItemModule',
        'seNavigationNodeSelector',
        'pageSyncMenuToolbarItemModule',
        'synchronizationPollingServiceModule',
        'productSelectorModule',
        'categorySelectorModule',
        'urlServiceModule',
        'clonePageWizardServiceModule',
        'cmsLinkToSelectModule',
        'permissionServiceModule',
        'rulesAndPermissionsRegistrationModule',
        'catalogServiceModule',
        'experienceServiceModule',
        'sharedDataServiceModule',
        'singleActiveCatalogAwareItemSelectorModule',
        'productCatalogDropdownPopulatorModule',
        'productDropdownPopulatorModule',
        'categoryDropdownPopulatorModule',
        'cmsItemDropdownModule',
        'catalogAwareRouteResolverModule',
        'catalogVersionPermissionModule',
        'componentRestrictionsEditorModule',
        'linkToggleModule',
        'functionsModule',
        'componentVisibilityAlertServiceModule'
    ])
    .config(['PAGE_LIST_PATH', 'NAVIGATION_MANAGEMENT_PAGE_PATH', '$routeProvider', 'catalogAwareRouteResolverFunctions', function(PAGE_LIST_PATH, NAVIGATION_MANAGEMENT_PAGE_PATH, $routeProvider, catalogAwareRouteResolverFunctions) {
        $routeProvider.when(PAGE_LIST_PATH, {
            templateUrl: 'pageListTemplate.html',
            controller: 'pageListController',
            controllerAs: 'pageListCtl',
            resolve: {
                setExperience: catalogAwareRouteResolverFunctions.setExperience
            }
        });
        $routeProvider.when(NAVIGATION_MANAGEMENT_PAGE_PATH, {
            templateUrl: 'navigationTemplate.html',
            controller: 'navigationController',
            controllerAs: 'nav',
            resolve: {
                setExperience: catalogAwareRouteResolverFunctions.setExperience
            }
        });
    }])

.controller('navigationController', ['$routeParams', 'urlService', 'permissionService', 'CONTEXT_CATALOG_VERSION', 'CONTEXT_SITE_ID', 'catalogService', function($routeParams, urlService, permissionService, CONTEXT_CATALOG_VERSION, CONTEXT_SITE_ID, catalogService) {
        this.uriContext = urlService.buildUriContext($routeParams.siteId, $routeParams.catalogId, $routeParams.catalogVersion);
        this.catalogName = "";
        this.catalogVersion = this.uriContext[CONTEXT_CATALOG_VERSION];

        permissionService.isPermitted([{
            names: ['se.edit.navigation']
        }]).then(function(isPermissionGranted) {
            this.readOnly = !isPermissionGranted;
        }.bind(this), function(e) {
            throw e;
        });

        catalogService.getContentCatalogsForSite(this.uriContext[CONTEXT_SITE_ID]).then(function(catalogs) {
            this.catalogName = catalogs.filter(function(catalog) {
                return catalog.catalogId === $routeParams.catalogId;
            }.bind(this))[0].name;
        }.bind(this));
    }])
    .run(
        /* jshint -W098*/
        /*need to inject for gatewayProxy initialization of componentVisibilityAlertService*/
        ['$log', '$rootScope', '$routeParams', 'NAVIGATION_MANAGEMENT_PAGE_PATH', 'ComponentService', 'systemEventService', 'catalogDetailsService', 'featureService', 'perspectiveService', 'assetsService', 'editorFieldMappingService', 'cmsDragAndDropService', 'editorModalService', 'clonePageWizardService', 'CATALOG_DETAILS_COLUMNS', 'sanitize', 'componentVisibilityAlertService', function($log, $rootScope, $routeParams, NAVIGATION_MANAGEMENT_PAGE_PATH, ComponentService, systemEventService, catalogDetailsService, featureService, perspectiveService, assetsService, editorFieldMappingService, cmsDragAndDropService, editorModalService, clonePageWizardService, CATALOG_DETAILS_COLUMNS, sanitize, componentVisibilityAlertService) {

            // Add the mapping for the generic editor.
            editorFieldMappingService.addFieldMapping('EntrySearchSelector', null, null, {
                template: 'entrySearchSelectorTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('Media', null, null, {
                template: 'mediaTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('MediaContainer', null, null, {
                template: 'mediaContainerTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('NavigationNodeSelector', null, null, {
                template: 'navigationNodeSelectorWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('MultiProductSelector', null, null, {
                template: 'multiProductSelectorTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('MultiCategorySelector', null, null, {
                template: 'multiCategorySelectorTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('CMSLinkToSelect', null, null, {
                template: 'cmsLinkToSelectWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('SingleOnlineProductSelector', null, null, {
                template: 'singleActiveCatalogAwareItemSelectorWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('SingleOnlineCategorySelector', null, null, {
                template: 'singleActiveCatalogAwareItemSelectorWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('CMSItemDropdown', null, null, {
                template: 'cmsItemDropdownWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('CMSComponentRestrictionsEditor', null, null, {
                template: 'componentRestrictionsEditorWrapperTemplate.html'
            });

            editorFieldMappingService.addFieldMapping('LinkToggle', null, null, {
                template: 'linkToggleWrapperTemplate.html',
                customSanitize: function(payload, sanitizeFn) {
                    if (sanitizeFn === undefined) {
                        sanitizeFn = sanitize;
                    }
                    payload.urlLink = sanitizeFn(payload.urlLink);
                }
            });

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'se.cms.componentMenuTemplate',
                type: 'HYBRID_ACTION',
                nameI18nKey: 'se.cms.componentmenu.btn.label.addcomponent',
                descriptionI18nKey: 'cms.toolbaritem.componentmenutemplate.description',
                priority: 1,
                section: 'left',
                iconClassName: 'hyicon hyicon-addlg se-toolbar-menu-ddlb--button__icon',
                callback: function() {
                    systemEventService.sendSynchEvent('ySEComponentMenuOpen', {});
                },
                include: 'componentMenuWrapperTemplate.html',
                permissions: ['se.add.component'],
                keepAliveOnClose: true
            });

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'se.cms.restrictionsMenu',
                type: 'HYBRID_ACTION',
                nameI18nKey: 'se.cms.restrictions.toolbar.menu',
                priority: 2,
                section: 'left',
                iconClassName: 'hyicon hyicon-restrictions se-toolbar-menu-ddlb--button__icon',
                include: 'pageRestrictionsMenuToolbarItemWrapperTemplate.html',
                permissions: ['se.read.restriction']
            });

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'se.cms.pageInfoMenu',
                type: 'HYBRID_ACTION',
                nameI18nKey: 'se.cms.pageinfo.menu.btn.label',
                descriptionI18nKey: 'cms.toolbarItem.pageInfoMenu.description',
                priority: 3,
                section: 'left',
                iconClassName: 'hyicon hyicon-info se-toolbar-menu-ddlb--button__icon',
                include: 'pageInfoMenuToolbarItemWrapperTemplate.html',
                permissions: ['se.read.page']
            });

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'se.cms.clonePageMenu',
                type: 'ACTION',
                nameI18nKey: 'se.cms.clonepage.menu.btn.label',
                iconClassName: 'hyicon hyicon-clone se-toolbar-menu-ddlb--button__icon',
                callback: function() {
                    clonePageWizardService.openClonePageWizard();
                },
                priority: 4,
                section: 'left',
                permissions: ['se.clone.page']
            });

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'se.cms.pageSyncMenu',
                nameI18nKey: 'se.cms.toolbaritem.pagesyncmenu.name',
                type: 'TEMPLATE',
                include: 'pageSyncMenuToolbarItemWrapperTemplate.html',
                priority: 5,
                section: 'left',
                permissions: ['se.sync.page']
            });

            catalogDetailsService.addItems([{
                include: 'pageListLinkTemplate.html'
            }, {
                include: 'navigationEditorLinkTemplate.html'
            }]);

            catalogDetailsService.addItems([{
                include: 'catalogDetailsSyncTemplate.html'
            }], CATALOG_DETAILS_COLUMNS.RIGHT);

            featureService.register({
                key: 'se.cms.html5DragAndDrop.outer',
                nameI18nKey: 'se.cms.dragAndDrop.name',
                descriptionI18nKey: 'se.cms.dragAndDrop.description',
                enablingCallback: function() {
                    cmsDragAndDropService.register();
                    cmsDragAndDropService.apply();
                },
                disablingCallback: function() {
                    cmsDragAndDropService.unregister();
                }
            });

            perspectiveService.register({
                key: 'se.cms.perspective.basic',
                nameI18nKey: 'se.cms.perspective.basic.name',
                descriptionI18nKey: 'se.cms.perspective.basic.description',
                features: ['se.contextualMenu', 'se.cms.dragandropbutton', 'se.cms.remove', 'se.cms.edit', 'se.cms.componentMenuTemplate', 'se.cms.restrictionsMenu', 'se.cms.clonePageMenu', 'se.cms.pageInfoMenu', 'se.emptySlotFix', 'se.cms.html5DragAndDrop', 'disableSharedSlotEditing', 'sharedSlotDisabledDecorator', 'se.cms.html5DragAndDrop.outer', 'externalComponentDecorator', 'externalcomponentbutton', 'externalSlotDisabledDecorator'],
                perspectives: []
            });

            /* Note: For advance edit mode, the ordering of the entries in the features list will determine the order the buttons will show in the slot contextual menu */
            perspectiveService.register({
                key: 'se.cms.perspective.advanced',
                nameI18nKey: 'se.cms.perspective.advanced.name',
                descriptionI18nKey: 'se.cms.perspective.advanced.description',
                features: ['se.slotContextualMenu', 'se.slotSyncButton', 'se.slotSharedButton', 'se.slotContextualMenuVisibility', 'se.contextualMenu', 'se.cms.dragandropbutton', 'se.cms.remove', 'se.cms.edit', 'se.cms.componentMenuTemplate', 'se.cms.restrictionsMenu', 'se.cms.clonePageMenu', 'se.cms.pageInfoMenu', 'se.cms.pageSyncMenu', 'se.emptySlotFix', 'se.cms.html5DragAndDrop', 'se.cms.html5DragAndDrop.outer', 'syncIndicator', 'externalSlotDisabledDecorator', 'externalComponentDecorator', 'externalcomponentbutton'],
                perspectives: []
            });

        }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('cmsItemDropdownModule', [])
    .controller('cmsItemDropdownController', function() {

        this.$onInit = function() {
            this.itemTemplateUrl = 'cmsItemSearchTemplate.html';

            this.field.uri = this.field.uri || "/cmswebservices/v1/sites/CURRENT_CONTEXT_SITE_ID/cmsitems";

            this.field.params = this.field.params || {};

            this.field.params.catalogId = 'CURRENT_CONTEXT_CATALOG';
            this.field.params.catalogVersion = 'CURRENT_CONTEXT_CATALOG_VERSION';

        };

    })
    /**
     * @name cmsItemDropdownModule.directive:cmsItemDropdown
     * @scope
     * @restrict E
     * @element cms-item-dropdown
     * 
     * @description
     * Component wrapper for CMS Item's on top of seDropdown component that upon selection of an option, will print the CMSItem
     * in the provided format.
     * 
     * @param {=Object} field The component field
     * @param {=String} id The component id
     * @param {=Object} model The component model
     * @param {=String} qualifier The qualifier within the structure attribute
     */
    .component('cmsItemDropdown', {
        templateUrl: 'cmsItemDropdownTemplate.html',
        controller: 'cmsItemDropdownController',
        controllerAs: 'cmsItemDropdownCtrl',
        bindings: {
            field: '=',
            qualifier: '=',
            model: '=',
            id: '='
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentMenuModule', ['eventServiceModule', 'componentTypesTabModule', 'componentsTabModule', 'cmsDragAndDropServiceModule', 'componentMenuServiceModule'])
    .controller('componentMenuController', ['$q', 'systemEventService', 'componentMenuService', 'DRAG_AND_DROP_EVENTS', 'EVENTS', function($q, systemEventService, componentMenuService, DRAG_AND_DROP_EVENTS, EVENTS) {

        // --------------------------------------------------------------------------------------------------
        // Constants
        // --------------------------------------------------------------------------------------------------
        var OPEN_COMPONENT_EVENT = 'ySEComponentMenuOpen';
        var OVERLAY_DISABLED_EVENT = 'OVERLAY_DISABLED';
        var RESET_COMPONENT_MENU_EVENT = 'RESET_COMPONENT_MENU_EVENT';

        var TAB_IDS = {
            COMPONENT_TYPES_TAB_ID: 'componentTypesTab',
            COMPONENTS_TAB_ID: 'componentsTab'
        };

        // --------------------------------------------------------------------------------------------------
        // Methods
        // --------------------------------------------------------------------------------------------------
        this.$onInit = function() {
            this._recompileDom = function() {};
            this._initializeComponentMenu();
            this.removePageChangeEventHandler = systemEventService.registerEventHandler(EVENTS.PAGE_CHANGE, this._initializeComponentMenu);
            this.removeOpenComponentEventHandler = systemEventService.registerEventHandler(OPEN_COMPONENT_EVENT, this._resetComponentMenu);
            this.removeDropdownEvent = systemEventService.registerEventHandler(DRAG_AND_DROP_EVENTS.DRAG_STARTED, this._closeMenu);
            this.removeOverlapEvent = systemEventService.registerEventHandler(OVERLAY_DISABLED_EVENT, this._closeMenu);
        };

        this.$onDestroy = function() {
            this.removePageChangeEventHandler();
            this.removeOpenComponentEventHandler();
            this.removeDropdownEvent();
            this.removeOverlapEvent();
        };

        // --------------------------------------------------------------------------------------------------
        // Helper Methods
        // --------------------------------------------------------------------------------------------------
        this._initializeComponentMenu = function() {
            // This is to ensure that the component menu DOM is completely clean, even after a page change.
            this.tabsList = null;
            this._recompileDom();

            componentMenuService.hasMultipleContentCatalogs().then(function(hasMultipleContentCatalogs) {
                this.hasMultipleContentCatalogs = hasMultipleContentCatalogs;

                var componentTypesTab = {
                    id: TAB_IDS.COMPONENT_TYPES_TAB_ID,
                    title: 'se.cms.compomentmenu.tabs.componenttypes',
                    templateUrl: 'componentTypesTabWrapperTemplate.html'
                };
                var componentsTab = {
                    id: TAB_IDS.COMPONENTS_TAB_ID,
                    title: 'se.cms.compomentmenu.tabs.customizedcomp',
                    templateUrl: 'componentsTabWrapperTemplate.html',
                    hasMultipleContentCatalogs: hasMultipleContentCatalogs
                };
                this.tabsList = [componentTypesTab, componentsTab];

                // This variable is assigned in the tabsList to make sure there's no
                // tight coupling with the view (instead of relying on a position in the array). 
                this.tabsList.componentsTab = componentsTab;
                this._resetComponentMenu();
            }.bind(this));
        }.bind(this);

        this._resetComponentMenu = function() {
            if (this.tabsList) {
                this.tabsList.map(function(tab) {
                    tab.active = (tab.id === TAB_IDS.COMPONENT_TYPES_TAB_ID);
                    return tab.active;
                });

                // Reset tab contents. 
                systemEventService.sendAsynchEvent(RESET_COMPONENT_MENU_EVENT);
            }
        }.bind(this);

        this._closeMenu = function() {
            if (this.actionItem) {
                this.actionItem.isOpen = false;
            }
        }.bind(this);

    }])
    .component('componentMenu', {
        templateUrl: 'componentMenuTemplate.html',
        controller: 'componentMenuController',
        bindings: {
            actionItem: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentItemModule', ['assetsServiceModule'])
    .controller('componentItemController', ['assetsService', function(assetsService) {

        this.$onInit = function() {
            this.imageRoot = assetsService.getAssetsRoot();
        };

    }])
    .component('componentItem', {
        templateUrl: 'componentItemTemplate.html',
        controller: 'componentItemController',
        bindings: {
            componentInfo: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentSearchModule', ['eventServiceModule'])
    .controller('componentSearchController', ['systemEventService', function(systemEventService) {

        // Constants
        var RESET_COMPONENT_MENU_EVENT = 'RESET_COMPONENT_MENU_EVENT';

        // Methods
        this.$onInit = function() {
            this._oldValue = '';
            this._resetSearchBox();

            this.removeResetEventListener = systemEventService.registerEventHandler(RESET_COMPONENT_MENU_EVENT, this._resetSearchBox);
        };

        this.$onDestroy = function() {
            this.removeResetEventListener();
        };

        this.$doCheck = function() {
            if (this.searchTerm !== this._oldValue) {
                this._oldValue = this.searchTerm;
                this.showResetButton = this.searchTerm !== '';
                this.onChange({
                    searchTerm: this.searchTerm
                });
            }
        };

        // Helper Methods
        this._resetSearchBox = function() {
            this.searchTerm = '';
        }.bind(this);

    }])
    .component('componentSearch', {
        templateUrl: 'componentSearchTemplate.html',
        controller: 'componentSearchController',
        bindings: {
            onChange: '&',
            placeholder: '@'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentTypeModule', ['assetsServiceModule'])
    .controller('componentTypeController', ['assetsService', function(assetsService) {

        this.$onInit = function() {
            this.imageRoot = assetsService.getAssetsRoot();
        };

    }])
    .component('componentType', {
        templateUrl: 'componentTypeTemplate.html',
        controller: 'componentTypeController',
        bindings: {
            typeInfo: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('nameFilterModule', [])
    .filter('nameFilter', function() {
        return function(components, criteria) {
            var filterResult = [];
            if (!criteria || criteria.length < 3) {
                return components;
            }

            criteria = criteria.toLowerCase();
            var criteriaList = criteria.split(" ");

            (components || []).forEach(function(component) {
                var match = true;
                var term = component.name.toLowerCase();

                criteriaList.forEach(function(item) {
                    if (term.indexOf(item) === -1) {
                        match = false;
                        return false;
                    }
                });

                if (match && filterResult.indexOf(component) === -1) {
                    filterResult.push(component);
                }
            });
            return filterResult;
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentMenuServiceModule', ['catalogServiceModule', 'experienceServiceModule', 'cmsDragAndDropServiceModule', 'yLoDashModule', 'storageServiceModule'])
    /**
     * This service provides functionality to the component menu; it's meant to be used internally. Thus, no ng-docs are added.
     */
    .service('componentMenuService', ['$timeout', 'storageService', 'catalogService', 'experienceService', 'cmsDragAndDropService', 'lodash', function($timeout, storageService, catalogService, experienceService, cmsDragAndDropService, lodash) {

        // --------------------------------------------------------------------------------------------------
        // Constants
        // --------------------------------------------------------------------------------------------------
        var SELECTED_CATALOG_VERSION_COOKIE_NAME = "se_catalogmenu_catalogversion_cookie";

        // --------------------------------------------------------------------------------------------------
        // Methods
        // --------------------------------------------------------------------------------------------------
        this.hasMultipleContentCatalogs = function() {
            return this.getContentCatalogs().then(function(contentCatalogs) {
                return contentCatalogs.length > 1;
            });
        };

        /**
         * This method is used to retrieve the content catalogs of the site in the page context. 
         */
        this.getContentCatalogs = function() {
            return this._getPageContext().then(function(pageContext) {
                var siteId = pageContext.siteId;
                return catalogService.getContentCatalogsForSite(siteId);
            }.bind(this));
        }.bind(this);

        /**
         * Gets the list of catalog/catalog versions where components can be retrieved from for this page. 
         */
        this.getValidContentCatalogVersions = function() {
            return this._getPageContext().then(function(pageContext) {
                return this.getContentCatalogs().then(function(contentCatalogs) {
                    // Return 'active' catalog versions for content catalogs, except for the 
                    // catalog in the current experience. 
                    var result = contentCatalogs.map(function(catalog) {
                        return this._getActiveOrCurrentVersionForCatalog(pageContext, catalog);
                    }.bind(this));

                    return result;
                }.bind(this));
            }.bind(this));
        }.bind(this);

        /**
         * Gets the list of catalog/catalog versions where components can be retrieved from for this page. 
         */
        this._getActiveOrCurrentVersionForCatalog = function(pageContext, catalog) {
            var catalogVersion = catalog.versions.filter(function(catalogVersion) {
                if (pageContext.catalogId === catalog.catalogId) {
                    return pageContext.catalogVersion === catalogVersion.version;
                }
                return catalogVersion.active;
            })[0];

            return {
                isCurrentCatalog: pageContext.catalogVersion === catalogVersion.version,
                catalogName: catalog.name,
                catalogId: catalog.catalogId,
                catalogVersionId: catalogVersion.version,
                id: catalogVersion.uuid
            };
        };

        this._getPageContext = function() {
            return experienceService.getCurrentExperience().then(function(experience) {
                return experience.pageContext;
            });
        };

        this.refreshDragAndDrop = function() {
            $timeout(function() {
                cmsDragAndDropService.update();
            }, 0);
        };

        // --------------------------------------------------------------------------------------------------
        // Cookie Management Methods
        // --------------------------------------------------------------------------------------------------
        this.getInitialCatalogVersion = function(catalogVersions) {
            return storageService.getValueFromCookie(SELECTED_CATALOG_VERSION_COOKIE_NAME).then(function(rawValue) {
                var selectedCatalogVersionId = (typeof rawValue === 'string') ? rawValue : null;

                var selectedCatalogVersion = catalogVersions.filter(function(catalogVersion) {
                    return catalogVersion.id === selectedCatalogVersionId;
                }.bind(this))[0];

                return (selectedCatalogVersion) ? selectedCatalogVersion : lodash.last(catalogVersions);
            });
        };

        this.persistCatalogVersion = function(catalogVersionId) {
            storageService._putValueInCookie(SELECTED_CATALOG_VERSION_COOKIE_NAME, catalogVersionId);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentTypesTabModule', ['eventServiceModule', 'componentSearchModule', 'componentServiceModule', 'componentTypeModule', 'nameFilterModule', 'componentMenuServiceModule'])
    .controller('componentTypesTabController', ['$log', 'systemEventService', 'ComponentService', 'componentMenuService', function($log, systemEventService, ComponentService, componentMenuService) {

        // --------------------------------------------------------------------------------------------------
        // Constants
        // --------------------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------------------
        // Lifecycle Methods
        // --------------------------------------------------------------------------------------------------
        this.$onInit = function() {
            this._retrieveComponentTypes();
        };

        // --------------------------------------------------------------------------------------------------
        // Event Handlers
        // --------------------------------------------------------------------------------------------------
        this.onSearchTermChanged = function(searchTerm) {
            this.searchTerm = searchTerm;
            componentMenuService.refreshDragAndDrop();
        }.bind(this);

        // --------------------------------------------------------------------------------------------------
        // Helper Methods
        // --------------------------------------------------------------------------------------------------
        /*
            This method is only called on init since the whole component menu is wrapped in the 'page-sensitive' directive.
            This means that if there's a page change, the directive will be recreated. 
        */
        this._retrieveComponentTypes = function() {
            ComponentService.getSupportedComponentTypesForCurrentPage().then(function(supportedTypes) {
                this.componentTypes = supportedTypes;
                componentMenuService.refreshDragAndDrop();
            }.bind(this)).catch(function(errData) {
                $log.error('ComponentMenuController.$onInit() - error loading types. ' + errData);
            });
        };
    }])
    .component('componentTypesTab', {
        templateUrl: 'componentTypesTabTemplate.html',
        controller: 'componentTypesTabController',
        bindings: {}
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentsTabModule', ['eventServiceModule', 'componentMenuServiceModule', 'componentServiceModule', 'componentItemModule'])
    .controller('componentsTabController', ['$q', 'systemEventService', 'componentMenuService', 'ComponentService', function($q, systemEventService, componentMenuService, ComponentService) {

        // --------------------------------------------------------------------------------------------------
        // Constants
        // --------------------------------------------------------------------------------------------------

        // --------------------------------------------------------------------------------------------------
        // Lifecycle Methods
        // --------------------------------------------------------------------------------------------------
        this.$onInit = function() {
            // Catalog Version Selector
            this.catalogVersions = null;
            this.selectedCatalogVersionId = null;
            this.selectedCatalogVersion = null;
            this.catalogVersionTemplate = 'catalogVersionTemplate.html';

            if (this.hasMultipleContentCatalogs) {
                this.catalogVersionsFetchStrategy = {
                    fetchAll: this.fetchCatalogVersions
                };
            } else {
                this.fetchCatalogVersions().then(function() {
                    this.onCatalogVersionChange();
                }.bind(this));
            }

            // Infinite Scrolling
            this.resetComponentsList = function() {};
            this.searchTerm = '';
        };

        // --------------------------------------------------------------------------------------------------
        // Event Handlers
        // --------------------------------------------------------------------------------------------------
        this.onCatalogVersionChange = function() {
            if (this.selectedCatalogVersionId) {
                this.selectedCatalogVersion = this.catalogVersions.filter(function(catalogVersion) {
                    return catalogVersion.id === this.selectedCatalogVersionId;
                }.bind(this))[0];

                componentMenuService.persistCatalogVersion(this.selectedCatalogVersionId);
                this.resetComponentsList();
            }
        }.bind(this);

        this.onSearchTermChanged = function(searchTerm) {
            this.searchTerm = searchTerm;
        }.bind(this);

        // --------------------------------------------------------------------------------------------------
        // Helper Methods
        // --------------------------------------------------------------------------------------------------
        this.fetchCatalogVersions = function() {
            return componentMenuService.getValidContentCatalogVersions().then(function(catalogVersions) {
                this.catalogVersions = catalogVersions;

                return componentMenuService.getInitialCatalogVersion(this.catalogVersions).then(function(selectedCatalogVersion) {
                    this.selectedCatalogVersion = selectedCatalogVersion;
                    this.selectedCatalogVersionId = this.selectedCatalogVersion.id;

                    return catalogVersions;
                }.bind(this));

            }.bind(this));
        }.bind(this);

        this.loadComponentItems = function(mask, pageSize, currentPage) {
            if (!this.selectedCatalogVersion) {
                return $q.when({
                    results: []
                });
            }

            var payload = {
                catalogId: this.selectedCatalogVersion.catalogId,
                catalogVersion: this.selectedCatalogVersion.catalogVersionId,
                mask: mask,
                pageSize: pageSize,
                page: currentPage
            };

            return ComponentService.loadPagedComponentItemsByCatalogVersion(payload)
                .then(function(loadedPage) {
                    componentMenuService.refreshDragAndDrop();
                    loadedPage.results = loadedPage.response;
                    delete loadedPage.response;

                    return loadedPage;
                }.bind(this));
        }.bind(this);

    }])
    .component('componentsTab', {
        templateUrl: 'componentsTabTemplate.html',
        controller: 'componentsTabController',
        bindings: {
            hasMultipleContentCatalogs: '<',
            isActive: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentEditorModule', ['yLoDashModule', 'componentServiceModule', 'eventServiceModule', 'functionsModule'])
    /**
     * @ngdoc object
     * @name componentEditorModule.COMPONENT_EDITOR_CHANGED
     * @description
     * Event is triggered whenever the isDirty state changes. The payload is object {tabId, component}.
     */
    .constant('COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT', 'ComponentEditorTabContentChangedEvent')
    /**
     * @ngdoc service
     * @name componentEditorModule.service:componentEditorService
     *
     * @description
     * Service that aggregates calls from multiple tabs of the editorModalService
     * and makes a call to the ITEMS API when all the data is ready for save or update. 
     * This service is also responsible for loading the initial content into all the tabs when the component is being edited.
     *
     * This service can manage multiple instances associated to different components (the association is made by using the componentId). Therefore,
     * in order to use this service, the `getInstance(componentId)` method must be called in order to get the relevant ComponentEditor instance. The returned
     * instance exposes the different methods described below. An instance can also be discarded (deleted) by calling the `destroyInstance(componentId)` method.
     * 
     * Each tab loaded into the editorModalService will invoke the {@link componentEditorModule.directive:componentEditor componentEditor} component
     * that, on initialization, will register the tab with this service so that any changes made to the tab are recorded by the componentEditorService service.
     */
    .factory('componentEditorService', ['$q', 'lodash', 'ComponentService', function($q, lodash, ComponentService) {

        var _instances = {};
        var DEFAULT_ID = "DEFAULT_ID";
        var componentId;
        /**
         * @constructor
         */
        var ComponentEditor = (function() {

            function ComponentEditor() {
                this._tabs = [];
                this._saveTabs = [];
                this._componentInfo = {};
                this._tabDirtyState = {};
                this._fullPayload = {};
                this._partialPayload = {};
                this._deferreds = [];
                this._content = {};
            }

            function _preparePartialPayload(payload, tabId, fields) {
                var filteredPayload = {};

                var tabFields = fields.map(function(field) {
                    return field.qualifier;
                }).filter(function(field) {
                    return Object.keys(payload).indexOf(field) > -1;
                });

                tabFields.forEach(function(field) {
                    filteredPayload[field] = payload[field];
                });

                this._partialPayload[tabId] = lodash.cloneDeep(filteredPayload);
            }

            function _prepareFullPayload(payload) {

                lodash.forEach(this._partialPayload, function(tabContent) {
                    this._fullPayload = lodash.merge(this._fullPayload, tabContent);
                }.bind(this));

                lodash.forEach(payload, function(value, key) {
                    if (!this._fullPayload[key]) {
                        this._fullPayload[key] = payload[key];
                    }
                }.bind(this));
            }

            function _saveTabsData(payload) {
                if (!payload.identifier) {
                    return ComponentService.createNewComponent(this._componentInfo, payload);
                } else {
                    return ComponentService.updateComponent(payload);
                }
            }


            /**
             * @ngdoc method
             * @name componentEditorModule.service:componentEditor#registerTab
             * @methodOf componentEditorModule.service:componentEditorService
             *
             * @description
             * Method used by the {@link componentEditorModule.directive:componentEditor componentEditor} component every time it is invoked by
             * a tab of the editorModalService to register the tab with the service.
             * 
             * @param {String} tabId The identifier of the tab to be registered.
             * @param {object} componentInfo The component data passed on from the editorModalService 
             *                 that holds the basic information about the component being created or modified.
             */
            ComponentEditor.prototype.registerTab = function(tabId, componentInfo) {
                this._tabs.push(tabId);
                this._componentInfo = (lodash.isEmpty(this._componentInfo)) ? lodash.cloneDeep(componentInfo) : lodash.cloneDeep(this._componentInfo);
            };


            /**
             * @ngdoc method
             * @name componentEditorModule.service:componentEditor#saveTabData
             * @methodOf componentEditorModule.service:componentEditorService
             *
             * @description
             * Method called once for every registered tab when saveTabs of {@link editorTabsetModule.directive:editorTabset editorTabset} is triggered.
             * 
             * The saveTabs of {@link editorTabsetModule.directive:editorTabset editorTabset} will send any asynchronous event to all registered tabs that 
             * will trigger this method. This method will wait until it receives the content of all the tabs that are in dirty state in order to aggregate
             * the content of all said tabs. Afterwards, it will make a POST/PUT call to the ITEMS API, depending on whether componentId is present or not.
             * 
             * @param {Object} partial The content of the tab.
             * @param {String} tabId The identifier of the tab to be registered.
             * @param {Array} fields The fields parameter of the GenericEditor service service scope containing the fields present in the tab.
             */
            ComponentEditor.prototype.saveTabData = function(partial, tabId, fields) {
                //prepare partial payload's for each tab
                _preparePartialPayload.call(this, partial, tabId, fields);
                this._saveTabs.push(tabId);

                //execute save only when tab data of all tabs in dirty states are collected
                if (this._saveTabs.length === Object.keys(this._tabDirtyState).length) {
                    //prepare full payload's containing additional info and then call save
                    _prepareFullPayload.call(this, partial);
                    return _saveTabsData.call(this, this._fullPayload).then(function(response) {
                        var payloads = lodash.cloneDeep(this._partialPayload);
                        this._deferreds.forEach(function(def, index) {
                            def.resolve({
                                payload: payloads[Object.keys(payloads)[index]],
                                response: response
                            });
                        }.bind(this));
                        this.resetTabData();
                        return {
                            payload: partial,
                            response: response
                        };
                    }.bind(this), function(response) {
                        this._deferreds.forEach(function(def) {
                            def.reject(response);
                        });
                        this._deferreds = [];

                        var deferred = $q.defer();
                        deferred.reject(response);
                        this.resetTabData();
                        return deferred.promise;
                    }.bind(this));
                } else {
                    var deferred = $q.defer();
                    this._deferreds.push(deferred);
                    return deferred.promise;
                }
            };

            /**
             * @ngdoc method
             * @name componentEditorModule.service:componentEditor#fetchTabsContent
             * @methodOf componentEditorModule.service:componentEditorService
             *
             * @description
             * Method called when the component is being edited to fetch and load the initial content from the ITEMS API.
             * 
             * @param {String} componentId The identifier of the component being edited to fetch the data.
             */
            ComponentEditor.prototype.fetchTabsContent = function(componentId) {
                if (lodash.isEmpty(this._content)) {
                    this._content = ComponentService.loadComponentItem(componentId);
                }
                return this._content;
            };

            /**
             * @ngdoc method
             * @name componentEditorModule.service:componentEditor#setTabDirtyState
             * @methodOf componentEditorModule.service:componentEditorService
             *
             * @description
             * Method invoked every time the content of a registered tab is modified.
             * Will be responsible for collecting all those registered tabs that are in dirty state (or modified when compared with initial content).
             * 
             * @param {String} tabId The identifier of the tab to be registered.
             * @param {boolean} isDirty The dirty state of the tab content.
             */
            ComponentEditor.prototype.setTabDirtyState = function(tabId, isDirty) {
                if (isDirty) {
                    this._tabDirtyState[tabId] = isDirty;
                } else {
                    if (this._tabDirtyState[tabId]) {
                        delete this._tabDirtyState[tabId];
                    }
                }
            };

            /**
             * @ngdoc method
             * @name componentEditorModule.service:componentEditor#setTabDirtyState
             * @methodOf componentEditorModule.service:componentEditorService
             *
             * @description
             * Method called to reset the service scope to initial state.
             */
            ComponentEditor.prototype.resetTabData = function() {
                this._saveTabs = [];
                this._fullPayload = {};
                this._partialPayload = {};
                this._content = {};
            };

            return ComponentEditor;

        })();


        /**
         * @ngdoc method
         * @name componentEditorModule.service:componentEditor#getInstance
         * @methodOf componentEditorModule.service:componentEditorService
         *
         * @description
         * Gets the instance of the `ComponentEditor` that corresponds to the given `componentId`. If no matching instance has been found, a
         * new instance will be created and associated to the given `componentId` If no `componentId` is supplied, an instance associated
         * to `componentId = 0` will be created/returned.
         * 
         * 
         * @param {String} [componentId] componentId The identifier of the associated component
         */
        this.getInstance = function(_componentId) {
            componentId = _componentId || DEFAULT_ID;
            _instances[componentId] = _instances[componentId] || new ComponentEditor();
            return _instances[componentId];
        };


        /**
         * @ngdoc method
         * @name componentEditorModule.service:componentEditor#destroyInstance
         * @methodOf componentEditorModule.service:componentEditorService
         *
         * @description
         * Method called to discard the current instance of `ComponentEditor` associated to a given `componentId`.
         */
        this.destroyInstance = function() {
            delete _instances[componentId];
        };


        return {
            getInstance: this.getInstance,
            destroyInstance: this.destroyInstance
        };

    }])
    .controller('componentEditorController', ['$q', '$scope', 'componentEditorService', 'systemEventService', 'COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT', 'copy', function($q, $scope, componentEditorService, systemEventService, COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT, copy) {
        var componentEditor;
        var componentEditorApi;
        var oldComponentData;

        this._validateStructure = function() {
            if (this.tabStructure && this.structureApi) {
                throw "please provide only one of either structure or structure api";
            } else if (!this.tabStructure && !this.structureApi) {
                throw "neither a structure nor a structure api is provided. The tab cannot be loaded with an empty structure.";
            }
        };

        // on initialization, register tabs and for update mode fetch the initial content
        this.$onInit = function() {
            componentEditor = componentEditorService.getInstance(this.componentId);
            this._validateStructure();
            componentEditor.registerTab(this.tabId, this.componentInfo);
            if (this.componentId) {
                this.content = componentEditor.fetchTabsContent(this.componentId);
            } else {
                this.content = this.content || {};
                this.content.visible = true;
            }
        };

        this.getApi = function($api) {
            componentEditorApi = $api;
        };

        // watch on which tabs are in dirty state and track them
        $scope.$watch(function() {
            var isDirty = typeof this.isDirtyTab === 'function' && this.isDirtyTab();
            return isDirty;
        }.bind(this), function(isDirty) {
            componentEditor.setTabDirtyState(this.tabId, isDirty);
        }.bind(this));

        // override default onSubmit of generic editor to have a single save once all tab data is collected
        this.onSubmit = function() {
            var payload = copy(this.component);
            if (this.isDirty()) {
                payload = this.sanitizePayload(payload, this.fields);
                if (this.smarteditComponentId) {
                    payload.identifier = this.smarteditComponentId;
                }

                return componentEditor.saveTabData(payload, this.id, this.fields);
            }
            return $q.when({
                payload: payload,
                response: payload
            });
        };

        this.$doCheck = function() {
            if (componentEditorApi) {
                if (oldComponentData !== angular.toJson(componentEditorApi.getContent())) {
                    oldComponentData = angular.toJson(componentEditorApi.getContent());
                    systemEventService.sendAsynchEvent(COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT, {
                        tabId: this.tabId,
                        component: componentEditorApi.getContent()
                    });
                }
            }
        };

        this.$onDestroy = function() {
            componentEditor.resetTabData();
            componentEditorService.destroyInstance();
        }.bind(this);
    }])

/**
 * @ngdoc directive
 * @name componentEditorModule.directive:componentEditor
 * @scope
 * @restrict E
 * @element component-editor
 *
 * @description
 * Angular component responsible for loading the structure and the content of the tab that invokes this component into the generic-editor directive.
 * More specifically, this component acts as a bridge between the independent tabs that are part of an editorTabset in order to consolidate the 
 * tabs' calls (GET/POST/PUT) to the content API into one single call. This is particularly relevant when the different tabs use the same content 
 * API endpoint.
 *
 * Every tab of the editorModalService must invoke the componentEditor so that this component will
 * delegate the structure/structureAPI provided to the generic editor to display the tab info.
 * 
 * This component requires either one of tabStructure or structureApi to load the structure within the tab else an error is thrown.
 *
 * @param {Function} saveTab saveTabs passed on from {@link editorTabsetModule.directive:editorTabset editorTabset} that instructs the tab to execute the onSave callback when save button is clicked.
 * @param {Function} resetTab resetTabs passed on from {@link editorTabsetModule.directive:editorTabset editorTabset} that instructs the tab to execute its onReset callback when reset button is clicked.
 * @param {Function} cancelTab cancelTabs passed on from {@link editorTabsetModule.directive:editorTabset editorTabset} that instructs the tab to execute its onCancel callback when cancel icon is clicked.
 * @param {Boolean} isDirtyTab States whether the tab is in pristine state or not (e.g., have been modified).
 * @param {String} componentId The Universally unique identifier of the component being created/modified.
 * @param {String} componentType The typeCode of the component being created/modified.
 * @param {String} tabId The identifier of the tab (is the identifier that is passed on to the generic editor).
 * @param {<Object} componentInfo The component data that holds the basic information about the component being created or modified. 
 * @param {String} componentInfo.componenCode componentCode of the ComponentType to be created and added to the slot.
 * @param {String} componentInfo.name name of the new component to be created.
 * @param {String} componentInfo.pageId pageId used to identify the current page template.
 * @param {String} componentInfo.slotId slotId used to identify the slot in the current template.
 * @param {String} componentInfo.position position used to identify the position in the slot in the current template.
 * @param {String} componentInfo.type type (typeCode) of the component being created.
 * @param {<Object=} tabStructure An optional parameter. The structure of the tab to be displayed.
 * @param {String=} tabStructure.cmsStructureType Value that is used to determine which form widget (property editor) to display for a specified property.
 * @param {String=} tabStructure.qualifier Name of the property.
 * @param {String=} tabStructure.prefixText Text to display before the property in the editor.
 * @param {String=} tabStructure.labelText Text to display after the property in the editor.
 * @param {Boolean=} tabStructure.editable Set to false in order to make the field uneditable.
 * @param {String=} tabStructure.i18nKey The i18nKey corresponding to the property's label.
 * @param {<String=} structureApi An optional parameter. The data binding to a REST Structure API that fulfills the contract described in the  GenericEditor service service.
 * @param {<String=} content The model that is passed on to the generic editor
 */
.component('componentEditor', {
    transclude: false,
    templateUrl: 'componentEditorTemplate.html',
    controller: 'componentEditorController',
    controllerAs: 'compCtrl',
    bindings: {
        saveTab: '=',
        resetTab: '=',
        cancelTab: '=',
        isDirtyTab: '=',
        componentId: '=',
        componentType: '=',
        tabId: '=',
        componentInfo: '<',
        tabStructure: '<?',
        structureApi: '<?',
        content: '<?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('editorModalServiceModule', ['genericEditorModalServiceModule', 'gatewayProxyModule', 'typeStructureRestServiceModule', 'basicTabModule', 'visibilityTabModule', 'genericTabModule', 'renderServiceModule', 'componentEditorModule', 'cmsitemsRestServiceModule', 'slotVisibilityServiceModule'])
    .factory('editorModalService', ['$q', 'genericEditorModalService', 'gatewayProxy', 'renderService', 'cmsitemsRestService', 'slotVisibilityService', function($q, genericEditorModalService, gatewayProxy, renderService, cmsitemsRestService, slotVisibilityService) {

        function EditorModalService() {
            this.gatewayId = 'EditorModal';
            gatewayProxy.initForService(this, ["open", "openAndRerenderSlot"]);
        }

        var tabs = [{
            id: 'genericTab',
            title: 'se.cms.editortabset.generictab.title',
            templateUrl: 'genericTabTemplate.html'
        }, {
            id: 'basicTab',
            title: 'se.cms.editortabset.basictab.title',
            templateUrl: 'basicTabTemplate.html'
        }, {
            id: 'visibilityTab',
            title: 'se.cms.editortabset.visibilitytab.title',
            templateUrl: 'visibilityTabTemplate.html'
        }];

        var _createComponentData = function(componentAttributes, targetSlotId, position) {
            var type;
            try {
                type = componentAttributes.smarteditComponentType.toLowerCase();
            } catch (e) {
                throw "editorModalService._createComponentData - invalid component type in componentAttributes." + e;
            }
            return {
                componentId: componentAttributes.smarteditComponentId,
                componentUuid: componentAttributes.smarteditComponentUuid,
                componentType: componentAttributes.smarteditComponentType,
                title: 'type.' + type + '.name',
                catalogVersionUuid: componentAttributes.catalogVersionUuid,
                targetSlotId: targetSlotId,
                position: targetSlotId ? position : undefined
            };
        };

        var _setActiveTab = function(tabs, selectedTabId) {
            tabs.map(function(tab) {
                tab.active = (tab.id === selectedTabId);
                return tab.active;
            });
        };

        EditorModalService.prototype.openAndRerenderSlot = function(componentType, componentUuid, slotId, selectedTabId) {

            var componentAttributes = {
                smarteditComponentType: componentType,
                smarteditComponentUuid: componentUuid
            };

            var componentData = _createComponentData(componentAttributes);

            _setActiveTab(tabs, selectedTabId);

            return genericEditorModalService.open(componentData, tabs, function() {
                slotVisibilityService.getSlotsForComponent(componentData.componentUuid).then(function(slotIds) {
                    renderService.renderSlots(slotIds);
                });
            });

        };

        EditorModalService.prototype.open = function(_componentAttributes, _targetSlotId, position, selectedTabTitle) {

            return legacyMode(_componentAttributes, _targetSlotId, position).then(function(componentAttributes) {
                var componentData = _createComponentData(componentAttributes, componentAttributes.targetSlotId, position);

                _setActiveTab(tabs, selectedTabTitle);

                return genericEditorModalService.open(componentData, tabs, function() {
                    if (componentData.componentId) {
                        return slotVisibilityService.getSlotsForComponent(componentData.componentUuid).then(function(slotIds) {
                            renderService.renderSlots(slotIds);
                        });
                    }
                });

            });
        };


        //@deprecated since 6.4
        //old signature of EditorModalService.prototype.open was componentType, componentId
        function legacyMode(_componentAttributes /*is componentType in legacy mode*/ , _targetSlotId /*is componentId in legacy mode*/ ) {

            var retunedValue;
            if (typeof _componentAttributes === 'string') {

                var componentType = _componentAttributes;
                var componentUID = _targetSlotId !== 0 ? _targetSlotId : undefined;
                if (componentUID) {
                    retunedValue = cmsitemsRestService._getByUIdAndType(componentUID, _componentAttributes).then(function(component) {
                        return {
                            smarteditComponentType: componentType,
                            smarteditComponentId: component.uid,
                            smarteditComponentUuid: component.uuid
                        };
                    });
                } else {
                    retunedValue = {
                        smarteditComponentType: componentType
                    };
                }
            } else {
                retunedValue = _componentAttributes;
                retunedValue.targetSlotId = _targetSlotId;
            }

            return $q.when(retunedValue);
        }


        return new EditorModalService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    angular.module('basicTabModule', ['genericEditorModule', 'resourceLocationsModule', 'componentEditorModule'])
        .controller('basicTabCtrl', function() {

            this.$onInit = function() {

                this.tabStructure = [{
                    cmsStructureType: "ShortString",
                    qualifier: "uid",
                    i18nKey: 'type.cmsitem.uid.name',
                    editable: false
                }, {
                    cmsStructureType: "DateTime",
                    qualifier: "creationtime",
                    i18nKey: 'type.item.creationtime.name',
                    editable: false
                }, {
                    cmsStructureType: "DateTime",
                    qualifier: "modifiedtime",
                    i18nKey: 'type.item.modifiedtime.name',
                    editable: false
                }];


            };

        })
        .component('basicTab', {
            transclude: false,
            templateUrl: 'componentEditorWrapperTemplate.html',
            controller: 'basicTabCtrl',
            bindings: {
                saveTab: '=',
                resetTab: '=',
                cancelTab: '=',
                isDirtyTab: '=',
                componentId: '<',
                componentType: '<',
                tabId: '<',
                componentInfo: '<'
            }
        });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    angular.module('genericTabModule', ['genericEditorModule', 'resourceLocationsModule', 'componentEditorModule', 'eventServiceModule', 'cmsLinkToSelectModule'])
        .controller('genericTabCtrl', ['TYPES_RESOURCE_URI', 'CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID', 'systemEventService', function(TYPES_RESOURCE_URI, CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID, systemEventService) {
            var STRUCTURE_API_BASE_URL = TYPES_RESOURCE_URI + '?code=:smarteditComponentType&mode=:structureApiMode';

            this.$onInit = function() {
                this.structureApi = this.getStructureApiByMode('DEFAULT');
                this.changeStructureEventListener = systemEventService.registerEventHandler(CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID, this.onChangeStructureEvent.bind(this));
            };

            this.onChangeStructureEvent = function(eventId, payload) {
                if (payload.structureApiMode) {
                    this.tabStructure = null;
                    this.structureApi = this.getStructureApiByMode(payload.structureApiMode);
                } else if (payload.structure) {
                    this.structureApi = null;
                    this.tabStructure = payload.structure.attributes;
                }
                this.content = payload.content;
            };

            this.$onDestroy = function() {
                this.changeStructureEventListener();
            };

            this.getStructureApiByMode = function(structureApiMode) {
                return STRUCTURE_API_BASE_URL.replace(/:structureApiMode/gi, structureApiMode);
            };
        }])
        .component('genericTab', {
            transclude: false,
            templateUrl: 'componentEditorWrapperTemplate.html',
            controller: 'genericTabCtrl',
            bindings: {
                saveTab: '=',
                resetTab: '=',
                cancelTab: '=',
                isDirtyTab: '=',
                componentId: '<',
                componentType: '<',
                tabId: '<',
                componentInfo: '<'
            }
        });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    angular.module('visibilityTabModule', ['genericEditorModule', 'resourceLocationsModule', 'componentEditorModule', 'eventServiceModule', 'seConstantsModule'])
        .controller('visibilityTabCtrl', ['systemEventService', 'COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'VALIDATION_MESSAGE_TYPES', function(systemEventService, COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, VALIDATION_MESSAGE_TYPES) {

            this.$onInit = function() {

                this.showDisclaimer = false;

                this.tabStructure = [{
                    cmsStructureType: 'Boolean',
                    qualifier: 'visible',
                    prefixText: 'se.cms.visible.prefix.text',
                    labelText: 'se.cms.visible.postfix.text'
                }, {
                    cmsStructureType: 'CMSComponentRestrictionsEditor',
                    qualifier: 'restrictions',
                    i18nKey: 'se.cms.display.conditions.header.restrictions'
                }];

                //initialize default content if there is not componentId associated
                if (!this.componentId) {
                    this.content = {
                        onlyOneRestrictionMustApply: false,
                        restrictions: []
                    };
                }

                this.handleComponentEditorChange = function(eventId, data) {
                    if (data.tabId === this.tabId) {
                        this.showDisclaimer = !!data.component && !data.component.visible;
                    }
                };

                this.handleUnrelatedMessagesEvent = function(eventId, data) {
                    var restrictionRelatedError = data.messages.filter(function(message) {
                        return message.type === VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR;
                    }).find(function(message) {
                        return message.subject.indexOf('restrictions') === 0;
                    });

                    if (restrictionRelatedError) {
                        systemEventService.sendAsynchEvent("EDITOR_IN_ERROR_EVENT", this.tabId);
                    }
                };

                this.unregisterChangeEvent = systemEventService.registerEventHandler(COMPONENT_EDITOR_TAB_CONTENT_CHANGED_EVENT, this.handleComponentEditorChange.bind(this));
                this.unregisterUnrelatedMessagesEvent = systemEventService.registerEventHandler(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, this.handleUnrelatedMessagesEvent.bind(this));
            };

            this.$onDestroy = function() {
                this.unregisterChangeEvent();
                this.unregisterUnrelatedMessagesEvent();
            };

        }])
        .component('visibilityTab', {
            transclude: false,
            templateUrl: 'componentEditorWrapperTemplate.html',
            controller: 'visibilityTabCtrl',
            bindings: {
                saveTab: '=',
                resetTab: '=',
                cancelTab: '=',
                isDirtyTab: '=',
                componentId: '<',
                componentType: '<',
                tabId: '<',
                componentInfo: '<'
            }
        });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentRestrictionsEditorModule', ['restrictionTypesServiceModule', 'restrictionsServiceModule'])
    .controller('componentRestrictionsEditorController', ['restrictionTypesService', 'restrictionsService', function(restrictionTypesService, restrictionsService) {

        this.restrictionsResult = function(onlyOneRestrictionMustApply, restrictions) {
            this.model.onlyOneRestrictionMustApply = onlyOneRestrictionMustApply;
            this.model.restrictions = restrictions;
        }.bind(this);

        this.getRestrictionTypes = function() {
            return restrictionTypesService.getRestrictionTypes();
        };

        this.getSupportedRestrictionTypes = function() {
            return restrictionsService.getSupportedRestrictionTypeCodes();
        };

        this.$onChanges = function(changesObj) {

            if (changesObj.model) {
                this.model.restrictions = changesObj.model.currentValue.restrictions;
                this.model.onlyOneRestrictionMustApply = changesObj.model.currentValue.onlyOneRestrictionMustApply;
            }
        };

    }])
    /**
     * @name cmsItemDropdownModule.directive:cmsItemDropdown
     * @scope
     * @restrict E
     * @element component-restrictions-editor
     * 
     * @description
     * Component wrapper for Restrictions on top of {@link restrictionsEditorModule.restrictionsEditor restrictionsEditor} component.
     * 
     * @param {<Object} model The component model
     */
    .component('componentRestrictionsEditor', {
        templateUrl: 'componentRestrictionsEditorTemplate.html',
        controller: 'componentRestrictionsEditorController',
        controllerAs: 'componentRestrictionsEditorCtrl',
        bindings: {
            model: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('cmsDragAndDropServiceModule', [
    'yjqueryModule',
    'dragAndDropServiceModule',
    'eventServiceModule',
    'gatewayFactoryModule'
])

.constant('DRAG_AND_DROP_EVENTS', {
    DRAG_STARTED: 'CMS_DRAG_STARTED',
    DRAG_STOPPED: 'CMS_DRAG_STOPPED'
})

.service('cmsDragAndDropService', ['yjQuery', 'dragAndDropService', 'systemEventService', 'gatewayFactory', 'ID_ATTRIBUTE', 'UUID_ATTRIBUTE', 'TYPE_ATTRIBUTE', 'DRAG_AND_DROP_EVENTS', function(yjQuery, dragAndDropService, systemEventService, gatewayFactory, ID_ATTRIBUTE, UUID_ATTRIBUTE, TYPE_ATTRIBUTE, DRAG_AND_DROP_EVENTS) {
    // Constants
    var CMS_DRAG_AND_DROP_ID = "se.cms.dragAndDrop";

    var TARGET_SELECTOR = "";
    var SOURCE_SELECTOR = ".smartEditComponent[data-smartedit-component-type!='ContentSlot']";

    var COMPONENT_SELECTOR = ".smartEditComponent";

    // Variables
    this._gateway = gatewayFactory.createGateway("cmsDragAndDrop");

    this.register = function() {
        dragAndDropService.register({
            id: CMS_DRAG_AND_DROP_ID,
            sourceSelector: SOURCE_SELECTOR,
            targetSelector: TARGET_SELECTOR,
            startCallback: this._onStart,
            stopCallback: this._onStop,
            enableScrolling: false
        });
    };

    this.unregister = function() {
        dragAndDropService.unregister([CMS_DRAG_AND_DROP_ID]);
    };

    this.apply = function() {
        dragAndDropService.apply();
    };

    this.update = function() {
        dragAndDropService.update(CMS_DRAG_AND_DROP_ID);
    };

    this._onStart = function(event) {
        var component = this._getSelector(event.target).closest(COMPONENT_SELECTOR);

        var dragInfo = {
            componentId: component.attr(ID_ATTRIBUTE),
            componentUuid: component.attr(UUID_ATTRIBUTE),
            componentType: component.attr(TYPE_ATTRIBUTE),
            slotUuid: null,
            slotId: null
        };

        this._gateway.publish(DRAG_AND_DROP_EVENTS.DRAG_STARTED, dragInfo);
        systemEventService.sendAsynchEvent(DRAG_AND_DROP_EVENTS.DRAG_STARTED);
    }.bind(this);

    this._onStop = function() {
        this._gateway.publish(DRAG_AND_DROP_EVENTS.DRAG_STOPPED);
    }.bind(this);

    this._getSelector = function(selector) {
        return yjQuery(selector);
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    var DIALOG_SAVE_EVENT = "sm-editor-save";
    var DIALOG_CANCEL_EVENT = "sm-editor-cancel";
    var DIALOG_RESET_EVENT = "sm-editor-reset";
    var DIALOG_IS_DIRTY_EVENT = "sm-editor-is-dirty";

    /**
     * @ngdoc overview
     * @name editorTabsetModule
     * @description
     *
     * The editor tabset module contains the service and the directives necessary to organize and display editing forms
     * in tabs.
     *
     * Use the {@link editorTabsetModule.service:editorTabsetService editorTabsetService} to manage the list of
     * tabs that will be present in the tabset.
     *
     * Use the {@link editorTabsetModule.directive:editorTabset editorTabset} directive to display editing
     * forms organized as tabs within a tabset (it will contain the tabs registered in the editorTabsetService).
     *
     */
    angular.module('editorTabsetModule', ['ui.bootstrap', 'functionsModule', 'experienceInterceptorModule', 'tabsetModule', 'eventServiceModule', 'cmssmarteditContainerTemplates'])

    /**
     * @ngdoc service
     * @name editorTabsetModule.service:editorTabsetService
     * @deprecated
     * @description
     *
     * The Editor Tabset Service keeps track of a list of tabs and their configuration, which includes the id, title,
     * and template  URL. The service allows registering, removing, and listing tabs. By default it contains the following ones:
     * - Admin Tab
     * - Basic Tab
     * - Generic Tab
     *
     * This service is used by the {@link editorTabsetModule.directive:editorTabset editorTabset}
     * directive to determine the tabs to be displayed in the tabset.
     */
    .factory('editorTabsetService', function() {
        var EditorTabsetService = function() {
            this.tabsList = [];
        };

        /**
         * @ngdoc method
         * @name editorTabsetModule.service:editorTabsetService#registerTab
         * @methodOf editorTabsetModule.service:editorTabsetService
         *
         * @description
         * This method allows registering a tab to be displayed in an editing tabset.
         *
         * Note: If the tabId is not unique, this method will override the previous tab configuration.
         *
         * @param {String} tabId An ID for the tab. It has to be unique, otherwise the previous tab configuration will be overriden.
         * @param {String} tabTitle The string displayed as the tab header.
         * @param {String} tabTemplateUrl Path to the HTML fragment to be displayed as the tab content.
         */
        EditorTabsetService.prototype.registerTab = function(tabId, tabTitle, tabTemplateUrl) {
            this._validateTab(tabId, tabTitle, tabTemplateUrl);

            this.tabsList.push({
                id: tabId,
                title: tabTitle,
                templateUrl: tabTemplateUrl,
                hasErrors: false
            });
        };

        EditorTabsetService.prototype._validateTabs = function(tabs) {
            tabs.forEach(function(tab) {
                this._validateTab(tab.id, tab.title, tab.templateUrl);
            }.bind(this));

        };

        EditorTabsetService.prototype._validateTab = function(tabId, tabTitle, tabTemplateUrl) {
            if (!tabId) {
                throw new Error("editorTabsetService.registerTab.invalidTabID");
            }

            if (!tabTitle) {
                throw new Error("editorTabsetService.registerTab.missingTabTitle");
            }

            if (!tabTemplateUrl) {
                throw new Error("editorTabsetService.registerTab.missingTemplateUrl");
            }
        };

        /**
         * @ngdoc method
         * @name editorTabsetModule.service:editorTabsetService#deleteTab
         * @methodOf editorTabsetModule.service:editorTabsetService
         *
         * @description
         * Removes a tab from the list of registered tabs.
         *
         * @param {String} tabId The ID of the tab to be removed.
         */
        EditorTabsetService.prototype.deleteTab = function(tabId) {
            var tabIndex = getIndexOfElementByAttr(this.tabsList, 'id', tabId);

            if (tabIndex >= 0) {
                delete this.tabsList[tabIndex];
            } else {
                throw new Error("editorTabsetService.deleteTab.tabNotFound");
            }
        };

        return new EditorTabsetService();
    })

    /**
     * @ngdoc directive
     * @name editorTabsetModule.directive:editorTabset
     * @scope
     * @restrict E
     * @element smartedit-editor-tabset
     *
     * @description
     * Directive responsible for displaying and organizing editing forms as tabs within a tabset.
     *
     * The directive allows communication with it via the control object. To do so, it exposes several methods
     * (saveTabs, resetTabs, and cancelTabs). When called, the directive will delegate the corresponding operation to
     * each of the tabs, which are represented internally as SmartEdit yTab directives.
     *
     * @param {Object} control The object that enables communication with the directive itself. It exposes the
     * following methods:
     * @param {Function} control.saveTabs Instructs internal tabs to execute their onSave callback, where they shall
     * validate and persist their changes. If a tab is unable to complete the save operation successfully, the tabset
     * will display an error in the tab's header.
     * @param {Function} control.resetTabs Instructs internal tabs to execute their onReset callback, where they shall
     * discard their modifications and return to a pristine state. It also clears all errors in the tabset.
     * @param {Function} control.cancelTabs Instructs internal tabs to execute their onCancel callback, which allows them to discard
     * their modifications and clean up as necessary. It also clears all errors in the tabset.
     * @param {Boolean} control.isDirty States whether one or more tabs are not in pristine state (e.g., have been modified).
     * @param {Object} data The custom data to pass to each of the individual tabs. When used within the editor modal
     * it must contain component information, such as the componentID and the componentType.
     *
     */
    .directive('editorTabset', ['$q', '$log', 'systemEventService', 'merge', 'copy', function($q, $log, systemEventService, merge, copy) {
        return {
            restrict: 'E',
            transclude: false,
            templateUrl: 'editorTabsetTemplate.html',
            scope: {
                control: '=',
                model: '=',
                tabs: '='
            },
            link: function(scope) {
                var isDirty = false;

                var removeTabErrors = function() {
                    for (var tabKey in scope.tabsList) {
                        scope.tabsList[tabKey].hasErrors = false;
                    }
                };

                scope.tabs.forEach(function(tab) {
                    _validateTab(tab.id, tab.title, tab.templateUrl);
                });
                scope.tabsList = copy(scope.tabs);
                scope.numTabsDisplayed = 6; //it includes more tab

                scope.control = {

                    saveTabs: function() {
                        var result = {
                            errorsList: [],
                            item: {},
                            operationSuccessful: true
                        };
                        var deferred = $q.defer();

                        systemEventService.sendAsynchEvent(DIALOG_SAVE_EVENT, result)
                            .then(function() {
                                removeTabErrors();

                                for (var idx in result.errorsList) {
                                    var tabId = result.errorsList[idx];
                                    var tabIndex = getIndexOfElementByAttr(scope.tabsList, "id", tabId);

                                    if (tabIndex >= 0) {
                                        scope.tabsList[tabIndex].hasErrors = true;
                                    }
                                }

                                if (result.operationSuccessful) {
                                    deferred.resolve(result.item);
                                } else {
                                    deferred.reject();
                                }
                            });

                        return deferred.promise;
                    },

                    resetTabs: function() {
                        return systemEventService.sendAsynchEvent(DIALOG_RESET_EVENT)
                            .then(function() {
                                removeTabErrors();
                            });
                    },

                    cancelTabs: function() {
                        return systemEventService.sendAsynchEvent(DIALOG_CANCEL_EVENT)
                            .then(function() {
                                removeTabErrors();
                            });
                    },

                    isDirty: function() {
                        return isDirty;
                    }
                };

                var dirtyBook = {};

                var onIsDirty = function(eventId, newTabEntry) {
                    dirtyBook[newTabEntry.tabId] = newTabEntry.isDirty;
                    for (var tabId in dirtyBook) {
                        if (dirtyBook[tabId]) {
                            isDirty = true;
                            return $q.when(true);
                        }
                    }
                    isDirty = false;
                    return $q.when(true);
                };

                //when outside of a DIALOG_SAVE_EVENT event, an editor with a tab may require to be marked in error
                systemEventService.registerEventHandler("EDITOR_IN_ERROR_EVENT", function(key, tabId) {
                    var tabIndex = getIndexOfElementByAttr(scope.tabsList, "id", tabId);
                    if (tabIndex >= 0) {
                        scope.tabsList[tabIndex].hasErrors = true;
                    }
                    return $q.when();
                });


                systemEventService.registerEventHandler(DIALOG_IS_DIRTY_EVENT, onIsDirty);

                scope.$on('$destroy', function() {
                    systemEventService.unRegisterEventHandler(DIALOG_IS_DIRTY_EVENT, onIsDirty);
                });

                scope.tabControl = function(tabScope) {
                    var onSave = function(event, result) {
                        return $q.when(function() {
                            if (tabScope.onSave) {
                                return tabScope.onSave()
                                    .then(function(data) {
                                        result.item = merge(result.item, data);
                                    }, function(errResponse) {
                                        if (errResponse) {
                                            if (errResponse.data && errResponse.data.errors) {
                                                errResponse.data.errors.forEach(function(error) {
                                                    var tabId = error.tabId || tabScope.tabId;

                                                    if (!result.errorsList.includes(tabId)) {
                                                        result.errorsList.push(tabId);
                                                    }
                                                });
                                            } else {
                                                result.errorsList.push(tabScope.tabId);
                                            }
                                        }

                                        result.operationSuccessful = false;

                                        return errResponse;
                                    });
                            } else {
                                $log.warn("Cannot save tab", tabScope.tabId, ". Save callback not defined.");
                            }
                        }());
                    };

                    var onReset = function() {
                        return $q.when(function() {
                            if (tabScope.onReset) {
                                return tabScope.onReset()
                                    .then(function(data) {
                                        return data;
                                    }, function(error) {
                                        return error;
                                    });
                            } else {
                                $log.warn("Cannot reset tab", tabScope.tabId, ". Reset callback not defined.");
                            }
                        }());
                    };

                    var onCancel = function() {
                        return $q.when(function() {
                            if (tabScope.onCancel) {
                                return tabScope.onCancel()
                                    .then(function(data) {
                                        return data;
                                    }, function(error) {
                                        return error;
                                    });
                            } else {
                                $log.warn("Cannot cancel tab", tabScope.tabId, ". Cancel callback not defined.");
                            }
                        }());
                    };

                    tabScope.$watch(function() {
                        return typeof tabScope.isDirty === 'function' && tabScope.isDirty();
                    }, function(currentDirtyState, oldDirtyState) {
                        if (typeof currentDirtyState === 'boolean' && currentDirtyState !== oldDirtyState) {
                            systemEventService.sendAsynchEvent(DIALOG_IS_DIRTY_EVENT, {
                                tabId: tabScope.tabId,
                                isDirty: currentDirtyState
                            });
                        }
                    });

                    systemEventService.registerEventHandler(DIALOG_SAVE_EVENT, onSave);
                    systemEventService.registerEventHandler(DIALOG_RESET_EVENT, onReset);
                    systemEventService.registerEventHandler(DIALOG_CANCEL_EVENT, onCancel);

                    tabScope.$on('$destroy', function() {
                        systemEventService.unRegisterEventHandler(DIALOG_SAVE_EVENT, onSave);
                        systemEventService.unRegisterEventHandler(DIALOG_RESET_EVENT, onReset);
                        systemEventService.unRegisterEventHandler(DIALOG_CANCEL_EVENT, onCancel);
                    });
                };

                function _validateTab(tabId, tabTitle, tabTemplateUrl) {
                    if (!tabId) {
                        throw new Error("editorTabset.invalidTabID");
                    }

                    if (!tabTitle) {
                        throw new Error("editorTabset.missingTabTitle");
                    }

                    if (!tabTemplateUrl) {
                        throw new Error("editorTabset.missingTemplateUrl");
                    }
                }
            }
        };
    }]);

    function getIndexOfElementByAttr(arr, attr, value) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][attr] === value) {
                return i;
            }
        }

        return -1;
    }
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name catalogAwareSelectorModule
 * @description
 * # The catalogAwareSelectorModule
 *
 * The catalogAwareSelectorModule contains a directive that allows users to select items that depend on a catalog.
 *
 */
angular.module('catalogAwareSelectorModule', ['itemSelectorPanelModule', 'yEditableListModule'])
    .controller('CatalogAwareSelectorController', ['$q', '$log', '$translate', 'lodash', function($q, $log, $translate, lodash) {

        // Constants
        var DEFAULT_ITEM_TYPE_KEY = 'se.cms.catalogaware.catalogitemtype.default';

        // Variables
        this.model = [];
        this.currentOptions = {};
        this.itemsList = [];

        // Initialization
        this.$onInit = function() {
            if (this.editable === undefined) {
                this.editable = true;
            }

            // First, connect with the outside model.
            this.exposedModel.$viewChangeListeners.push(this._syncFromExternalModel);
            this.exposedModel.$render = this._syncFromExternalModel;

            this.catalogItemTypeKey = (this.catalogItemTypeKey) ? this.catalogItemTypeKey : DEFAULT_ITEM_TYPE_KEY;
            $translate(this.catalogItemTypeKey).then(function(itemType) {
                this.catalogItemType = itemType;
            }.bind(this));
        };

        // Basic Functions
        /**
         * This method is used to sync the internal model with the changes happening outside. It is mostly used when
         * the component first opens and we need to synchronize the internal model with the items that have already
         * been selected.
         */
        this._syncFromExternalModel = function() {
            if (this.model.length === 0) {
                this.model.push.apply(this.model, this.exposedModel.$modelValue);
                this._syncFromModelToItemsList();
            }
        }.bind(this);

        /**
         * This method is used to synchronize changes that happen in the yEditableList with the internal model.
         */
        this._syncFromItemsListToModel = function() {
            this.model.length = 0;
            this.itemsList.map(function(item) {
                this.model.push(item.id);
            }.bind(this));
        };

        /**
         * This method is used to synchronize changes that happen in the panel with the internal model.
         */
        this._syncFromModelToItemsList = function() {
            var itemsFound = {};
            var itemsMissing = [];

            // Find which items have already been retrieved and which ones are missing.
            this.model.map(function(itemKey) {
                var match = lodash.find(this.itemsList, function(item) {
                    return item.id === itemKey;
                });

                if (match) {
                    itemsFound[itemKey] = match;
                } else {
                    itemsMissing.push(itemKey);
                }
            });

            // Get the missing items
            this._fetchItems(itemsMissing, itemsFound).then(function() {
                // Update the itemsList with the new items. They are added in the order of the model.
                this.itemsList.length = 0;
                this.model.map(function(itemKey) {
                    var item = itemsFound[itemKey];
                    if (!item) {
                        $log.warn("[seCatalogAwareSelector] - Cannot find item with key ", itemKey);
                    } else {
                        this.itemsList.push(item);
                    }

                }.bind(this));

                if (this._refreshListWidget && lodash.isFunction(this._refreshListWidget)) {
                    this._refreshListWidget();
                }

            }.bind(this));


        };

        this._fetchItems = function(itemsToRetrieve, itemsFound) {
            var result = $q.when(null);

            if (itemsToRetrieve.length > 0) {
                if (this.itemsFetchStrategy.fetchAll) {
                    result = this.itemsFetchStrategy.fetchAll().then(function(itemsList) {
                        itemsToRetrieve.map(function(itemKey) {
                            itemsFound[itemKey] = itemsList.find(function(itemInList) {
                                return itemInList.id === itemKey;
                            });
                        }.bind(this));
                    });
                } else if (this.itemsFetchStrategy.fetchEntities) {
                    result = this.itemsFetchStrategy.fetchEntities(this.model).then(function(itemsList) {
                        itemsList.map(function(itemInList) {
                            itemsFound[itemInList.id] = itemInList;
                        });
                    }.bind(this));
                } else if (this.itemsFetchStrategy.fetchEntity) {
                    var promises = [];
                    itemsToRetrieve.map(function(itemKey) {
                        promises.push(this.itemsFetchStrategy.fetchEntity(itemKey));
                    }.bind(this));

                    return $q.all(promises).then(function(itemsList) {
                        itemsList.map(function(itemInList) {
                            itemsFound[itemInList.id] = itemInList;
                        });
                    });
                } else {
                    throw Error('[seCatalogAwareSelector] Invalid items fetch strategy. Cannot retrieve information.');
                }
            }

            return result;
        };

        // Helper Functions
        this.listIsEmpty = function() {
            return (this.model.length === 0);
        };

        this.openEditingPanel = function() {
            this.showPanel();
        };

        this.onPanelChange = function() {
            this._syncFromModelToItemsList();

            // Sync with the outside
            this.exposedModel.$setViewValue(this.model);
        }.bind(this);

        this.onListChange = function() {
            this._syncFromItemsListToModel();

            // Sync with the outside
            this.exposedModel.$setViewValue(this.model);
        }.bind(this);
    }])

/**
 * @ngdoc directive
 * @name catalogAwareSelectorModule.directive:seCatalogAwareSelector
 * @scope
 * @restrict E
 * @element ANY
 *
 * @description
 * A component that allows users to select items from one or more catalogs. This component is catalog aware; the list
 * of items displayed is dependent on the catalog and catalog version selected by the user within the component.
 *
 * @param {@String} id Identifier used to track the component in the page.
 * @param {<Function} getCatalogs Function called with no arguments by the component to retrieve the list of catalogs
 * where the items to select reside.
 * @param {<Object} itemsFetchStrategy Object that defines the strategies necessary to retrieve the items from a
 * particular catalog. There are three possible scenarios:
 * - Provide a fetchAll function in the itemsFetchStrategy. This function must retrieve all items from the catalog
 * at once. No pagination is performed.
 * - Provide fetchPage + fetchEntity functions in the itemsFetchStrategy. Information about items already selected
 * are retrieved one by one with fetchEntity. Items to choose from, on the other hand, are retrieved in a page fashion.
 * - Provide fetchPage + fetchEntities Information about items already selected are retrieved in one call with
 * fetchEntities. Items to choose from, on the other hand, are retrieved in a page fashion.
 * @param {Function} catalogItemsFetchStrategy.fetchAll (Optional)
 * @param {Function} catalogItemsFetchStrategy.fetchPage (Optional)
 * @param {Function} catalogItemsFetchStrategy.fetchEntity (Optional)
 * @param {Function} catalogItemsFetchStrategy.fetchEntities (Optional)
 * @param {<String=} itemTemplate The URL of the template used to display the items in the selector. If none is chosen
 * a default one is used (it shows the property name of each item).
 * @param {@String=} catalogItemTypeKey This property is a localized key. It is used to display the type of item being
 * selected in the component. If no key is provided, the type defaults to item.
 * @param {<Boolean=} editable This property specifies whether the selector can be edited or not. If this flag is false,
 * then the selector is treated as read-only; the selection cannot be modified in any way.
 * @param {<maxNumItems=} maxNumItems The maximum number of items that can be selected in the control.
 */
.component('seCatalogAwareSelector', {
    templateUrl: 'catalogAwareSelectorTemplate.html',
    controller: 'CatalogAwareSelectorController',
    controllerAs: 'ctrl',
    require: {
        exposedModel: 'ngModel'
    },
    bindings: {
        id: '@',
        getCatalogs: '<',
        itemsFetchStrategy: '<',
        itemTemplate: '<?',
        catalogItemTypeKey: '@?',
        editable: '<?',
        maxNumItems: '<?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name categorySelectorModule
 * @description
 *
 * The categorySelectorModule contains a directive that allows selecting categories from a catalog.
 *
 */
angular.module('categorySelectorModule', ['catalogAwareSelectorModule', 'catalogInformationServiceModule'])
    /**
     * @ngdoc object
     * @name categorySelectorModule.object:CATEGORY_TEMPLATE_FOR_SELECTOR
     * @description
     * Constant that specifies the URL of the template used to display a category.
     */
    .constant('CATEGORY_TEMPLATE_FOR_SELECTOR', 'categoryTemplate.html')
    .controller('CategorySelectorController', ['$q', 'lodash', 'sharedDataService', 'seCatalogInformationService', 'CATEGORY_TEMPLATE_FOR_SELECTOR', function($q, lodash, sharedDataService, seCatalogInformationService, CATEGORY_TEMPLATE_FOR_SELECTOR) {
        this.$onInit = function() {

            this.categoryTemplate = CATEGORY_TEMPLATE_FOR_SELECTOR;
            this.getCatalogs = seCatalogInformationService.getProductCatalogsInformation;
            this.itemsFetchStrategy = seCatalogInformationService.categoriesFetchStrategy;

            if (this.editable === undefined) {
                this.editable = true;
            }
        };

    }])
    /**
     * @ngdoc directive
     * @name categorySelectorModule.directive:seCategorySelector
     * @scope
     * @restrict E
     *
     * @description
     * A component that allows users to select categories from one or more catalogs. This component is catalog aware; the list of categories displayed is dependent on
     * the product catalog and catalog version selected by the user within the component.
     *
     * @param {@String} id An identifier used to track down the component in the page.
     * @param {<Object} model The object where the list of selected categories will be stored. The model must contain a property with the same name as the qualifier. That property must be
     * of type array and is used to store the UIDs of the categories selected.
     * @param {<String} qualifier The key of the property in the model that will hold the list of categories selected.
     * @param {<Boolean=} editable A flag that specifies whether the component can be modified or not. If the component cannot be modified, then the categories selected are read-only.
     */
    .component('seCategorySelector', {
        templateUrl: 'categorySelectorTemplate.html',
        controller: 'CategorySelectorController',
        controllerAs: 'ctrl',
        bindings: {
            id: '@',
            model: '<',
            qualifier: '<',
            editable: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name itemSelectorPanelModule
 * @description
 * # The itemSelectorPanelModule
 *
 * This module contains a directive used to display a panel where a user can add items from one or more catalogs.
 */
angular.module('itemSelectorPanelModule', ['sliderPanelModule', 'l10nModule', 'recompileDomModule'])
    .controller('ItemSelectorPanelController', ['$q', '$element', 'lodash', function($q, $element, lodash) {

        // Variables
        this.catalogInfo = {};
        this.catalogItemTemplate = "catalogItemTemplate.html";
        this.saveButtonDisabled = true;

        // Initialization
        this.$onInit = function() {

            this.sliderConfiguration = {
                cssSelector: '#y-modal-dialog',
                greyedOutOverlay: false,
                modal: {
                    showDismissButton: true,
                    title: this.catalogItemType,
                    cancel: {
                        onClick: this._cancelItemsChanges,
                        label: 'se.cms.catalogaware.panel.button.cancel'
                    },
                    save: {
                        onClick: this._saveItemChanges,
                        label: 'se.cms.catalogaware.panel.button.add',
                        isDisabledFn: this._isSaveButtonDisabled
                    }
                }
            };

            this._initCatalogs();

            if (this.maxNumItems === undefined || this.maxNumItems < 0) {
                this.maxNumItems = 0;
            }

            // Basic Functions
            this.showPanel = function() {
                this.catalogInfo = {};
                this._initCatalogs();
                this._internalItemsSelected = (this.itemsSelected) ? lodash.clone(this.itemsSelected) : [];
                this._internalShowPanel();
            }.bind(this);

        };

        // Catalog
        this._initCatalogs = function() {
            return this.getCatalogs().then(function(catalogs) {
                this.catalogs = catalogs;
                if (this.catalogs.length === 1) {
                    this.catalogInfo.catalogId = this.catalogs[0].id;
                }

                this._initCatalogSelector();
                this._initCatalogVersionSelector();
                this._initItemsSelector();

            }.bind(this));
        };

        this._initCatalogSelector = function() {
            if (Array.isArray(this.catalogList)) {
                throw Error('Cannot show panel - Invalid list of catalogs.');
            }

            this.catalogSelectorFetchStrategy = {
                fetchAll: function() {
                    return $q.when(this.catalogs);
                }.bind(this)
            };
        };

        this._onCatalogSelectorChange = function() {
            if (this.resetCatalogVersionSelector) {
                this.resetCatalogVersionSelector();
            }
        }.bind(this);

        // Catalog Version
        this._initCatalogVersionSelector = function() {
            this.catalogVersionSelectorFetchStrategy = {
                fetchAll: function() {
                    var result = [];
                    if (this.catalogInfo.catalogId) {
                        result = this.catalogs.find(function(catalog) {
                            return catalog.id === this.catalogInfo.catalogId;
                        }.bind(this)).versions;
                    }

                    return $q.when(result);
                }.bind(this)
            };
        };

        this._onCatalogVersionSelectorChange = function() {
            if (this.catalogInfo.catalogId && this.catalogInfo.catalogVersion) {
                if (this.resetItemsListSelector) {
                    this.resetItemsListSelector();
                }
            }
        }.bind(this);

        // Items
        this._initItemsSelector = function() {
            var callWithCatalogInfo = function(fn, filterArguments) {
                var newArgs = lodash.concat([this.catalogInfo], Array.from(arguments).slice(2));
                return fn.apply(null, newArgs).then(function(page) {
                    if (filterArguments) {
                        // Remove the items that are already selected. Otherwise, the ySelect would duplicate them, producing an exception.
                        var itemsIndex = page.results.length;
                        while (itemsIndex--) {
                            var item = page.results[itemsIndex];
                            if (lodash.includes(this._internalItemsSelected, item.uid)) {
                                page.results.splice(itemsIndex, 1);
                                page.pagination.count--; // TODO: Review with Andres changes to see if it makes sense or not to remove it from the count.
                            }
                        }
                    }

                    return page;
                }.bind(this));
            }.bind(this);

            this._internalItemsFetchStrategy = {
                fetchAll: (this.itemsFetchStrategy.fetchAll) ? callWithCatalogInfo.bind(null, this.itemsFetchStrategy.fetchAll, false) : null,
                fetchPage: (this.itemsFetchStrategy.fetchPage) ? callWithCatalogInfo.bind(null, this.itemsFetchStrategy.fetchPage, true) : null,
                fetchEntity: this.itemsFetchStrategy.fetchEntity,
                fetchEntities: this.itemsFetchStrategy.fetchEntities
            };
        };

        this._onItemsSelectorChange = function() {
            if (this._isItemSelectorEnabled()) {
                // Only consider changes when the item is enabled. Otherwise the changes are happening during initialization.
                this.saveButtonDisabled = false;
            }
        }.bind(this);

        this._isSaveButtonDisabled = function() {
            return this.saveButtonDisabled;
        }.bind(this);

        this._cancelItemsChanges = function() {
            // Discard changes in the list.
            this.catalogInfo = {};
            this._internalHidePanel();
        }.bind(this);

        this._saveItemChanges = function() {
            // Copy the new elements into the other array. We need to use the same array so that the reference is not lost in the parent.
            this.itemsSelected.splice.apply(this.itemsSelected, [0, this.itemsSelected.length].concat(this._internalItemsSelected));

            if (this.onChange) {
                this.onChange();
            }

            this._internalHidePanel();


        }.bind(this);

        // Event Listeners
        this._isItemSelectorEnabled = function() {
            return this.catalogInfo && this.catalogInfo.catalogId && this.catalogInfo.catalogVersion;
        }.bind(this);

    }])
    /**
     * @ngdoc directive
     * @name itemSelectorPanelModule.directive:seItemSelectorPanel
     * @scope
     * @restrict E
     * @element se-item-selector-panel
     *
     * @description
     * Directive used to create a panel where a user can add items from one or more catalogs. Note that this directive is intended to be used within a
     * {@link catalogAwareSelectorModule.directive:seCatalogAwareSelector seCatalogAwareSelector}; it is not designed to work alone.
     *
     * @param {<String} panelTitle The title displayed at the top of the panel.
     * @param {=Array} itemsSelected Array containing the UIDs of the items selected. There's a double binding, which
     * means that the panel will show items already selected and will update back whenever the user changes the
     * selection.
     * @param {<String} itemTemplate The URL of the template used to display items in the component.
     * @param {<Function} getCatalogs Function called by the component to retrieve the catalogs from which to select
     * items from.
     * @param {<Object} itemsFetchStrategy Object containing the strategy to retrieve items from the catalogs. There are three possible scenarios:
     * - Provide a fetchAll function in the itemsFetchStrategy. This function must retrieve all items from the catalog
     * at once. No pagination is performed.
     * - Provide fetchPage + fetchEntity functions in the itemsFetchStrategy. Information about items already selected
     * are retrieved one by one with fetchEntity. Items to choose from, on the other hand, are retrieved in a page fashion.
     * - Provide fetchPage + fetchEntities Information about items already selected are retrieved in one call with
     * fetchEntities. Items to choose from, on the other hand, are retrieved in a page fashion.
     * @param {Function} catalogItemsFetchStrategy.fetchAll (Optional)
     * @param {Function} catalogItemsFetchStrategy.fetchPage (Optional)
     * @param {Function} catalogItemsFetchStrategy.fetchEntity (Optional)
     * @param {Function} catalogItemsFetchStrategy.fetchEntities (Optional)
     * @param {=Function} onChange Function executed whenever the selection of items change in the panel.
     * @param {=Function} showPanel Function passed to the parent to be executed whenever the panel is opened.
     * @param {=Function=} hidePanel Function passed to the parent to be executed whenever the panel is hid.
     * @param {<String} catalogItemType String specifying the type of elements being selected in the panel. This string is used only for localization purposes.
     * @param {<Number=} maxNumItems The maximum number of items that a user can select within the component.
     *
     */
    .component('seItemSelectorPanel', {
        templateUrl: 'itemSelectorPanelTemplate.html',
        controller: 'ItemSelectorPanelController',
        controllerAs: 'ctrl',
        bindings: {
            panelTitle: '<',
            itemsSelected: '=',
            itemTemplate: '<',
            getCatalogs: '<',
            itemsFetchStrategy: '<',
            onChange: '=',
            showPanel: '=',
            hidePanel: '=?',
            catalogItemType: '<',
            maxNumItems: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name productSelectorModule
 * @description
 *
 * The productSelectorModule contains a directive that allows selecting products from a catalog.
 *
 */
angular.module('productSelectorModule', ['catalogAwareSelectorModule', 'catalogInformationServiceModule'])
    /**
     * @ngdoc object
     * @name productSelectorModule.object:PRODUCT_TEMPLATE_FOR_SELECTOR
     * @description
     * Constant that specifies the URL of the template used to display a product.
     */
    .constant('PRODUCT_TEMPLATE_FOR_SELECTOR', 'productTemplate.html')
    /**
     * @ngdoc object
     * @name productSelectorModule.object:PRODUCT_TEMPLATE_FOR_SELECTOR
     * @description
     * Constant that specifies the maximum number of products a user can select.
     */
    .constant('MAX_NUM_PRODUCTS_IN_SELECTOR', 10)
    .controller('ProductSelectorController', ['$q', 'lodash', 'seCatalogInformationService', 'PRODUCT_TEMPLATE_FOR_SELECTOR', 'MAX_NUM_PRODUCTS_IN_SELECTOR', function($q, lodash, seCatalogInformationService, PRODUCT_TEMPLATE_FOR_SELECTOR, MAX_NUM_PRODUCTS_IN_SELECTOR) {

        // Variables
        this.$onInit = function() {
            this.productTemplate = PRODUCT_TEMPLATE_FOR_SELECTOR;
            this.itemsFetchStrategy = seCatalogInformationService.productsFetchStrategy;
        };

        this.getCatalogs = seCatalogInformationService.getProductCatalogsInformation;
        this.maxNumItems = MAX_NUM_PRODUCTS_IN_SELECTOR;
    }])
    /**
     * @ngdoc directive
     * @name productSelectorModule.directive:seProductSelector
     * @scope
     * @restrict E
     *
     * @description
     * A component that allows users to select products from one or more catalogs. This component is catalog aware; the list of products displayed is dependent on
     * the product catalog and catalog version selected by the user within the component.
     *
     * @param {@String} id An identifier used to track down the component in the page.
     * @param {<Object} model The object where the list of selected products will be stored. The model must contain a property with the same name as the qualifier. That property must be
     * of type array and is used to store the UIDs of the products selected.
     * @param {<String} qualifier The key of the property in the model that will hold the list of products selected.
     */
    .component('seProductSelector', {
        templateUrl: 'productSelectorTemplate.html',
        controller: 'ProductSelectorController',
        controllerAs: 'ctrl',
        bindings: {
            id: '@',
            model: '<',
            qualifier: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name catalogInformationServiceModule
 * @description
 *
 * The catalogInformationServiceModule contains a service with helper methods used by catalog aware selector components.
 *
 */
angular.module('catalogInformationServiceModule', ['catalogServiceModule', 'productServiceModule', 'productCategoryServiceModule'])
    /**
     * @ngdoc service
     * @name catalogInformationServiceModule.service:seCatalogInformationService
     *
     * @description
     * This service contains helper methods used by catalog aware components.
     *
     */
    .service('seCatalogInformationService', ['$q', 'lodash', 'catalogService', 'productService', 'productCategoryService', 'sharedDataService', function($q, lodash, catalogService, productService, productCategoryService, sharedDataService) {

        /**
         * @ngdoc method
         * @name catalogInformationServiceModule.service:seCatalogInformationService#getProductCatalogsInformation
         * @methodOf catalogInformationServiceModule.service:seCatalogInformationService
         *
         * @description
         *
         * This method retrieves the information of the product catalogs available in the current site.
         *
         * @returns {Promise} A promise that resolves to an array containing the information of all the product catalogs available in the current site.
         *
         */
        this.getProductCatalogsInformation = function() {
            return this._getSiteUID().then(function(siteUID) {
                if (this._cachedSiteUID === siteUID && this._parsedCatalogs) {
                    // Return the cached catalogs only if the site hasn't changed
                    // otherwise it's necessary to reload them. 
                    return this._parsedCatalogs;
                } else {
                    this._cachedSiteUID = siteUID;
                    return catalogService.getProductCatalogsForSite(siteUID).then(function(catalogs) {
                        var result = {};

                        catalogs.map(function(catalog) {
                            result[catalog.catalogId] = {
                                id: catalog.catalogId,
                                name: catalog.name,
                                versions: catalog.versions.map(function(catalogVersion) {
                                    return {
                                        id: catalogVersion.version,
                                        label: catalogVersion.version
                                    };
                                })
                            };
                        });
                        this._parsedCatalogs = lodash.values(result);

                        return this._parsedCatalogs;
                    }.bind(this));
                }
            }.bind(this));
        }.bind(this);

        /**
         * @ngdoc object
         * @name catalogInformationServiceModule.object:productsFetchStrategy
         * @description
         * This object contains the strategy necessary to display products in a paged way; it contains a method to retrieve pages of products and another method to
         * retrieve individual products. Such a strategy is necessary to work with products in a ySelect .
         */
        this.productsFetchStrategy = {
            fetchPage: function(catalogInfo, mask, pageSize, currentPage) {
                return this._getSiteUID().then(function(siteUID) {
                    catalogInfo.siteUID = siteUID;
                    return productService.getProducts(catalogInfo, mask, pageSize, currentPage);
                }).then(function(result) {
                    result.products.map(function(rawProduct) {
                        rawProduct.id = rawProduct.uid;
                    }.bind(this));

                    return {
                        pagination: result.pagination,
                        results: result.products
                    };
                }.bind(this));
            }.bind(this),
            fetchEntity: function(productUID) {
                return this._getSiteUID().then(function(siteUID) {
                    return productService.getProductById(siteUID, productUID);
                }.bind(this)).then(function(product) {
                    // ySelect requires an id, while other parts of the selector rely on the UID. 
                    product.id = product.uid;

                    return product;
                }.bind(this));
            }.bind(this)
        };

        /**
         * @ngdoc object
         * @name catalogInformationServiceModule.object:categoriesFetchStrategy
         * @description
         * This object contains the strategy necessary to display categories in a paged way; it contains a method to retrieve pages of categories and another method to
         * retrieve individual categories. Such a strategy is necessary to work with categories in a ySelect.
         */
        this.categoriesFetchStrategy = {
            fetchPage: function(catalogInfo, mask, pageSize, currentPage) {
                return this._getSiteUID().then(function(siteUID) {
                    catalogInfo.siteUID = siteUID;
                    return productCategoryService.getCategories(catalogInfo, mask, pageSize, currentPage);
                }).then(function(result) {
                    result.productCategories.map(function(rawCategory) {
                        // ySelect requires an id, while other parts of the selector rely on the UID. 
                        rawCategory.id = rawCategory.uid;
                    }.bind(this));

                    return {
                        pagination: result.pagination,
                        results: result.productCategories
                    };
                });
            }.bind(this),
            fetchEntity: function(categoryUID) {
                return this._getSiteUID().then(function(siteUID) {
                    return productCategoryService.getCategoryById(siteUID, categoryUID);
                }).then(function(category) {
                    category.id = category.uid;
                    return category;
                });
            }.bind(this)
        };

        // Helper Methods
        this._getSiteUID = function() {
            return sharedDataService.get('experience').then(function(currentExperience) {
                return currentExperience.siteDescriptor.uid;
            });
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @name cmsLinkToSelectModule
 * @description
 * # The cmsLinkToSelectModule
 *
 * The cmsLinkToSelectModule contains the CMSLinkTo component which is a wrapper on top of the seDropDown component.
 * When one of the dropdown option is selected, the `CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID` event is sent through the systemEvent Service.
 * That event is listened in the Generic Tab component to dynamically reload the Generic Editor structure (the Generic Editor listens for structure and structureApi changes)
 *
 */
angular.module('cmsLinkToSelectModule', ['eventServiceModule', 'seDropdownModule', 'resourceLocationsModule', 'cmsLinkComponentContentPageDropdownPopulatorModule'])
    .constant('CMS_LINK_TO_SELECT_OPTIONS', [{
        id: 'content',
        structureApiMode: 'CONTENT'
    }, {
        id: 'product',
        structureApiMode: 'PRODUCT',
        hasCatalog: true
    }, {
        id: 'category',
        structureApiMode: 'CATEGORY',
        hasCatalog: true
    }, {
        id: 'external',
        structureApiMode: 'EXTERNAL'
    }])
    /**
     * Custom event id that is listened in Generic Tab to dynamically reload the Generic Editor structure
     */
    .constant('CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID', 'cms-link-to-reload-structure')
    .controller('CmsLinkToSelectController', ['CMS_LINK_TO_SELECT_OPTIONS', 'CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID', 'LINKED_DROPDOWN', 'systemEventService', function(CMS_LINK_TO_SELECT_OPTIONS, CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID, LINKED_DROPDOWN, systemEventService) {
        this.$onInit = function() {
            var linkTo = _getLinkToValue(this.model);
            if (linkTo !== null) {
                this.model.linkTo = linkTo;
            }
            this.unregisterDropdownListener = systemEventService.registerEventHandler(this.id + LINKED_DROPDOWN, _onLinkToSelectValueChanged.bind(this));
        };

        this.$onDestroy = function() {
            this.unregisterDropdownListener();
        };

        function _onLinkToSelectValueChanged(eventId, handle) {

            if (this.qualifier !== handle.qualifier) {
                return;
            }

            if (!handle.optionObject) {
                return;
            }
            var optionValue = handle.optionObject.id;
            var selectedOption = CMS_LINK_TO_SELECT_OPTIONS.find(function(selectOption) {
                return selectOption.id === optionValue;
            });

            if (!selectedOption) {
                throw new Error('error.selected.option.not.supported');
            }

            // Prevent cycling infinitely because the Generic Editor append the component each time the structure change
            if (optionValue === this.model.currentSelectedOptionValue) {
                return;
            }
            this.model.currentSelectedOptionValue = optionValue;
            this.model.external = optionValue !== 'external';

            _cleanModel.call(this, selectedOption);

            systemEventService.sendEvent(CMS_LINK_TO_RELOAD_STRUCTURE_EVENT_ID, {
                content: this.model,
                structureApiMode: selectedOption.structureApiMode
            });
        }

        function _getLinkToValue(model) {
            if (model.url) {
                return 'external';
            } else if (model.product) {
                return 'product';
            } else if (model.contentPage) {
                return 'content';
            } else if (model.category) {
                return 'category';
            }
            return null;
        }

        function _cleanModel(selectedOption) {
            if (!this.model) {
                return;
            }

            if (selectedOption.id !== 'category') {
                delete this.model.category;
            }

            if (selectedOption.id !== 'product') {
                delete this.model.product;
            }

            if (!selectedOption.hasCatalog) {
                delete this.model.productCatalog;
            }

            if (selectedOption.id !== 'content') {
                delete this.model.contentPage;
            }

            if (selectedOption.id !== 'external') {
                delete this.model.url;
            }
        }
    }])
    /**
     * @name cmsLinkToSelectModule.directive:cmsLinkToSelect
     * @scope
     * @restrict E
     * @element cms-link-to-select
     * 
     * @description
     * Component wrapper on top of seDropdown component that upon selection of an option, will trigger an event with a new structureApi or structure
     * The `CMSLinkToSelect` custom component is registered in cmssmarteditContainerApp and contains the following option values:
     * - content (link to a content page)
     * - product (link to a product page)
     * - category (link to a specific category page)
     * - external (external link)
     * 
     * @param {<Object} field The component field
     * @param {<String} id The component id
     * @param {<Object} model The component model
     * @param {<String} qualifier The qualifier within the structure attribute
     */
    .component('cmsLinkToSelect', {
        transclude: true,
        replace: false,
        templateUrl: 'cmsLinkToSelectTemplate.html',
        controller: 'CmsLinkToSelectController',
        controllerAs: 'ctrl',
        bindings: {
            field: '<',
            id: '<',
            model: '<',
            qualifier: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('cmsLinkComponentContentPageDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule', 'contextAwareCatalogModule', 'uriDropdownPopulatorModule'])
    .factory('CMSLinkComponentcontentPageDropdownPopulator', ['DropdownPopulatorInterface', 'extend', 'contextAwareCatalogService', 'uriDropdownPopulator', function(DropdownPopulatorInterface, extend, contextAwareCatalogService, uriDropdownPopulator) {
        var dropdownPopulator = function() {};

        dropdownPopulator = extend(DropdownPopulatorInterface, dropdownPopulator);

        dropdownPopulator.prototype.fetchPage = function(payload) {
            return contextAwareCatalogService.getContentPageSearchUri().then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.fetchPage(payload);
            }.bind(this));
        };

        dropdownPopulator.prototype.isPaged = function() {
            return true;
        };

        dropdownPopulator.prototype.getItem = function(payload) {
            return contextAwareCatalogService.getContentPageItemUri().then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.getItem(payload);
            }.bind(this));
        };

        return new dropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('linkToggleModule', [])
    .controller('linkToggleController', function() {
        this.emptyUrlLink = function() {
            this.model.linkToggle.urlLink = '';
        };
        this.$onInit = function() {
            if (this.model.linkToggle === undefined) {
                this.model.linkToggle = {};
            }

            if (this.model.linkToggle.external === undefined) {
                this.model.linkToggle.external = true;
            }
        };
    })
    .component('linkToggle', {
        templateUrl: 'linkToggleTemplate.html',
        transclude: true,
        controller: 'linkToggleController',
        bindings: {
            field: '<',
            model: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seErrorsListModule', [])
    .controller('seErrorsListController', function() {
        this.getSubjectErrors = function() {
            return (this.subject ? this.errors.filter(function(error) {
                return error.subject === this.subject;
            }.bind(this)) : this.errors);
        };
    })
    .directive('seErrorsList', function() {
        return {
            templateUrl: 'seErrorsListTemplate.html',
            restrict: 'E',
            scope: {},
            controller: 'seErrorsListController',
            controllerAs: 'ctrl',
            bindToController: {
                errors: '=',
                subject: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seFileSelectorModule', [])
    .controller('seFileSelectorController', function() {
        this.imageRoot = this.imageRoot || "";
        this.disabled = this.disabled || false;
        this.customClass = this.customClass || "";

        this.buildAcceptedFileTypesList = function() {
            return this.acceptedFileTypes.map(function(fileType) {
                return '.' + fileType;
            }).join(',');
        };
    })
    .directive('seFileSelector', function() {
        return {
            templateUrl: 'seFileSelectorTemplate.html',
            restrict: 'E',
            scope: {},
            controller: 'seFileSelectorController',
            controllerAs: 'ctrl',
            bindToController: {
                imageRoot: '=?',
                uploadIcon: '=',
                labelI18nKey: '=',
                acceptedFileTypes: '=',
                customClass: '=?',
                disabled: '=?',
                onFileSelect: '&'
            },
            link: function($scope, $element) {
                $element.find('input').on('change', function(event) {
                    $scope.$apply(function() {
                        $scope.ctrl.onFileSelect({
                            files: event.target.files
                        });
                        var input = $element.find('input');
                        input.replaceWith(input.clone(true));
                    });
                });
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaAdvancedPropertiesModule', ['ui.bootstrap'])
    .constant('seMediaAdvancedPropertiesConstants', {
        CONTENT_URL: 'seMediaAdvancedPropertiesContentTemplate.html',
        I18N_KEYS: {
            DESCRIPTION: 'se.media.advanced.information.description',
            CODE: 'se.media.advanced.information.code',
            ALT_TEXT: 'se.media.advanced.information.alt.text',
            ADVANCED_INFORMATION: 'se.media.advanced.information',
            INFORMATION: 'se.media.information'
        }
    })
    .controller('seMediaAdvancedPropertiesController', ['seMediaAdvancedPropertiesConstants', function(seMediaAdvancedPropertiesConstants) {

        this.$onInit = function() {
            this.i18nKeys = seMediaAdvancedPropertiesConstants.I18N_KEYS;
            this.contentUrl = seMediaAdvancedPropertiesConstants.CONTENT_URL;
        };

    }])
    .component('seMediaAdvancedProperties', {
        bindings: {
            code: '=',
            advInfoIcon: '=',
            description: '=',
            altText: '='
        },
        controller: 'seMediaAdvancedPropertiesController',
        controllerAs: 'ctrl',
        templateUrl: 'seMediaAdvancedPropertiesTemplate.html'
    })
    .component('seMediaAdvancedPropertiesCondensed', {
        bindings: {
            code: '=',
            advInfoIcon: '=',
            description: '=',
            altText: '='
        },
        controller: 'seMediaAdvancedPropertiesController',
        controllerAs: 'ctrl',
        templateUrl: 'seMediaAdvancedPropertiesCondensedTemplate.html'
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaContainerFieldModule', [
        'seMediaUploadFormModule',
        'seMediaFormatModule',
        'seErrorsListModule',
        'seFileValidationServiceModule'
    ])
    .controller('seMediaContainerFieldController', ['seFileValidationService', function(seFileValidationService) {
        this.image = {};
        this.fileErrors = [];

        this.fileSelected = function(files, format) {
            var previousFormat = this.image.format;
            this.resetImage();

            if (files.length === 1) {
                seFileValidationService.validate(files[0], this.fileErrors).then(function() {
                    this.image = {
                        file: files[0],
                        format: format || previousFormat
                    };
                }.bind(this));
            }
        };

        this.resetImage = function() {
            this.fileErrors = [];
            this.image = {};
        };

        this.imageUploaded = function(uuid) {
            if (this.model && this.model[this.qualifier]) {
                this.model[this.qualifier][this.image.format] = uuid;
            } else {
                this.model[this.qualifier] = {};
                this.model[this.qualifier][this.image.format] = uuid;
            }

            this.resetImage();
        };

        this.imageDeleted = function(format) {
            delete this.model[this.qualifier][format];
        };

        this.isFormatUnderEdit = function(format) {
            return format === this.image.format;
        };
    }])
    .directive('seMediaContainerField', function() {
        return {
            templateUrl: 'seMediaContainerFieldTemplate.html',
            restrict: 'E',
            controller: 'seMediaContainerFieldController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                field: '=',
                model: '=',
                editor: '=',
                qualifier: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaFieldModule', [
        'seMediaSelectorModule',
        'seMediaUploadFormModule',
        'seFileValidationServiceModule',
        'seErrorsListModule',
        'assetsServiceModule'
    ])
    .factory('seMediaFieldConstants', ['assetsService', function(assetsService) {
        var assetsRoot = assetsService.getAssetsRoot();

        return {
            I18N_KEYS: {
                UPLOAD_IMAGE_TO_LIBRARY: 'se.upload.image.to.library'
            },
            UPLOAD_ICON_URL: assetsRoot + '/images/upload_image.png'
        };
    }])
    .controller('seMediaFieldController', ['seMediaFieldConstants', 'seFileValidationServiceConstants', 'seFileValidationService', function(seMediaFieldConstants, seFileValidationServiceConstants, seFileValidationService) {
        this.i18nKeys = seMediaFieldConstants.I18N_KEYS;
        this.acceptedFileTypes = seFileValidationServiceConstants.ACCEPTED_FILE_TYPES;
        this.uploadIconUrl = seMediaFieldConstants.UPLOAD_ICON_URL;
        this.image = {};
        this.fileErrors = [];

        this.fileSelected = function(files, format) {
            this.resetImage();
            if (files.length === 1) {
                seFileValidationService.validate(files[0], this.fileErrors).then(function() {
                    this.image = {
                        file: files[0],
                        format: format || this.image.format
                    };
                }.bind(this));
            }
        };

        this.resetImage = function() {
            this.fileErrors = [];
            this.image = {};
        };

        this.imageUploaded = function(uuid) {
            this.resetImage();
            this.model[this.qualifier] = uuid;
            if (this.field.initiated) {
                this.field.initiated.length = 0;
            }
        };

        this.showFileSelector = function() {
            //enable file selector only if model exists but field is not set
            return this.model && !this.model[this.qualifier] && !this.image.file;
        };
    }])
    .directive('seMediaField', function() {
        return {
            templateUrl: 'seMediaFieldTemplate.html',
            restrict: 'E',
            controller: 'seMediaFieldController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                field: '=',
                model: '=',
                editor: '=',
                qualifier: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaFormatModule', ['mediaServiceModule', 'seFileSelectorModule', 'seFileValidationServiceModule', 'assetsServiceModule'])
    .constant('seMediaFormatConstants', {
        I18N_KEYS: {
            UPLOAD: 'se.media.format.upload',
            REPLACE: 'se.media.format.replace',
            UNDER_EDIT: 'se.media.format.under.edit',
            REMOVE: 'se.media.format.remove'
        },
        UPLOAD_ICON_URL: '/images/upload_image.png',
        UPLOAD_ICON_DIS_URL: '/images/upload_image_disabled.png',
        DELETE_ICON_URL: '/images/remove_image_small.png',
        REPLACE_ICON_URL: '/images/replace_image_small.png',
        ADV_INFO_ICON_URL: '/images/info_image_small.png'
    })
    .controller('seMediaFormatController', ['mediaService', 'seMediaFormatConstants', 'seFileValidationServiceConstants', 'assetsService', '$scope', function(mediaService, seMediaFormatConstants, seFileValidationServiceConstants, assetsService, $scope) {
        this.i18nKeys = seMediaFormatConstants.I18N_KEYS;
        this.acceptedFileTypes = seFileValidationServiceConstants.ACCEPTED_FILE_TYPES;

        var assetsRoot = assetsService.getAssetsRoot();
        this.uploadIconUrl = assetsRoot + seMediaFormatConstants.UPLOAD_ICON_URL;
        this.uploadIconDisabledUrl = assetsRoot + seMediaFormatConstants.UPLOAD_ICON_DIS_URL;

        this.deleteIconUrl = assetsRoot + seMediaFormatConstants.DELETE_ICON_URL;
        this.replaceIconUrl = assetsRoot + seMediaFormatConstants.REPLACE_ICON_URL;
        this.advInfoIconUrl = assetsRoot + seMediaFormatConstants.ADV_INFO_ICON_URL;

        this.fetchMedia = function() {
            mediaService.getMedia(this.mediaUuid).then(function(val) {
                this.media = val;
            }.bind(this));
        };

        this.isMediaCodeValid = function() {
            return this.mediaUuid && typeof this.mediaUuid === 'string';
        };

        this.isMediaPreviewEnabled = function() {
            return this.isMediaCodeValid() && !this.isUnderEdit && this.media && this.media.code;
        };

        this.isMediaEditEnabled = function() {
            return !this.isMediaCodeValid() && !this.isUnderEdit;
        };

        this.getErrors = function() {
            return (this.errors || []).filter(function(error) {
                return error.format === this.mediaFormat;
            }.bind(this)).map(function(error) {
                return error.message;
            });
        };

        this.$onInit = function() {
            if (this.isMediaCodeValid()) {
                this.fetchMedia();
            }
            this.mediaFormatI18NKey = "se.media.format." + this.mediaFormat;
        };

        $scope.$watch(function() {
            return this.mediaUuid;
        }.bind(this), function(mediaUuid, oldMediaUuid) {
            if (mediaUuid && typeof mediaUuid === 'string') {
                if (mediaUuid !== oldMediaUuid) {
                    this.fetchMedia();
                }
            } else {
                this.media = {};
            }
        }.bind(this));

    }])
    .component('seMediaFormat', {
        templateUrl: 'seMediaFormatTemplate.html',
        controller: 'seMediaFormatController',
        controllerAs: 'ctrl',
        bindings: {
            mediaUuid: '=',
            mediaFormat: '=',
            isUnderEdit: '=',
            errors: '=',
            onFileSelect: '&?',
            onDelete: '&?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaPreviewModule', [])
    .constant('seMediaPreviewConstants', {
        CONTENT_URL: 'seMediaPreviewContentTemplate.html',
        I18N_KEYS: {
            PREVIEW: 'media.preview'
        }
    })
    .controller('seMediaPreviewController', ['seMediaPreviewConstants', function(seMediaPreviewConstants) {
        this.i18nKeys = seMediaPreviewConstants.I18N_KEYS;
        this.contentUrl = seMediaPreviewConstants.CONTENT_URL;
    }])
    .directive('seMediaPreview', function() {
        return {
            restrict: 'E',
            scope: {},
            bindToController: {
                imageUrl: '='
            },
            controller: 'seMediaPreviewController',
            controllerAs: 'ctrl',
            templateUrl: 'seMediaPreviewTemplate.html'
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaPrinterModule', ['seMediaPreviewModule'])
    .factory('seMediaPrinterConstants', ['assetsService', function(assetsService) {
        var assetsRoot = assetsService.getAssetsRoot();

        return {
            DELETE_ICON_URL: assetsRoot + '/images/remove_image_small.png',
            REPLACE_ICON_URL: assetsRoot + '/images/replace_image_small.png',
            ADV_INFO_ICON_URL: assetsRoot + '/images/info_image_small.png'
        };
    }])
    .controller('seMediaPrinterController', ['$scope', 'seMediaPrinterConstants', function($scope, seMediaPrinterConstants) {
        this.deleteIcon = seMediaPrinterConstants.DELETE_ICON_URL;
        this.replaceIcon = seMediaPrinterConstants.REPLACE_ICON_URL;
        this.advInfoIcon = seMediaPrinterConstants.ADV_INFO_ICON_URL;

        this.$onChanges = function() {
            $scope.selected = this.selected;
            $scope.item = this.item;
            $scope.ySelect = this.ySelect;
        };

    }])
    .component('seMediaPrinter', {
        templateUrl: 'seMediaPrinterTemplate.html',
        controller: 'seMediaPrinterController',
        controllerAs: 'printer',
        require: {
            ySelect: '^ySelect'
        },
        bindings: {
            selected: '<',
            item: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaSelectorModule', ['seMediaAdvancedPropertiesModule', 'seMediaPrinterModule', 'mediaServiceModule'])
    .controller('seMediaSelectorController', ['mediaService', function(mediaService) {

        this.mediaTemplate = 'seMediaPrinterWrapperTemplate.html';

        this.fetchStrategy = {
            fetchEntity: function(uuid) {
                return mediaService.getMedia(uuid);
            },
            fetchPage: function(mask, pageSize, currentPage) {
                return mediaService.getPage(mask, pageSize, currentPage);
            }
        };

    }])
    .component('seMediaSelector', {
        templateUrl: 'seMediaSelectorTemplate.html',
        bindings: {
            field: '=',
            model: '=',
            editor: '=',
            qualifier: '=',
            deleteIcon: '=',
            replaceIcon: '=',
            advInfoIcon: '='
        },
        controller: 'seMediaSelectorController',
        controllerAs: 'ctrl'
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaUploadFieldModule', ['assetsServiceModule'])
    .controller('seMediaUploadFieldController', ['assetsService', function(assetsService) {
        this.displayImage = false;
        this.assetsRoot = assetsService.getAssetsRoot();
    }])
    .directive('seMediaUploadField', function() {
        return {
            templateUrl: 'seMediaUploadFieldTemplate.html',
            restrict: 'E',
            scope: {},
            bindToController: {
                field: '=',
                model: '=',
                error: '='
            },
            controller: 'seMediaUploadFieldController',
            controllerAs: 'ctrl',
            link: function(scope, element, attrs, ctrl) {
                element.bind("mouseover", function() {
                    ctrl.displayImage = true;
                    scope.$digest();
                });
                element.bind("mouseout", function() {
                    ctrl.displayImage = false;
                    scope.$digest();
                });
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seMediaUploadFormModule', [
        'seFileValidationServiceModule',
        'seMediaServiceModule',
        'seObjectValidatorFactoryModule',
        'seBackendValidationHandlerModule',
        'seMediaUploadFieldModule',
        'functionsModule',
        'assetsServiceModule'
    ])
    .constant('seMediaUploadFormConstants', {
        MAX_FILENAME_CHARACTERS_DISPLAYED: 15,
        I18N_KEYS: {
            UPLOAD_IMAGE_CANCEL: 'se.upload.image.cancel',
            UPLOAD_IMAGE_SUBMIT: 'se.upload.image.submit',
            UPLOAD_IMAGE_REPLACE: 'se.upload.image.replace',
            UPLOAD_IMAGE_TO_LIBRARY: 'se.upload.image.to.library',
            DESCRIPTION: 'se.uploaded.image.description',
            CODE: 'se.uploaded.image.code',
            ALT_TEXT: 'se.uploaded.image.alt.text',
            UPLOADING: 'se.uploaded.is.uploading',
            CODE_IS_REQUIRED: 'se.uploaded.image.code.is.required'
        }
    })
    .factory('seMediaUploadFormValidators', ['seMediaUploadFormConstants', function(seMediaUploadFormConstants) {
        return [{
            subject: 'code',
            message: seMediaUploadFormConstants.I18N_KEYS.CODE_IS_REQUIRED,
            validate: function(code) {
                return !!code;
            }
        }];
    }])
    .controller('seMediaUploadFormController', ['seFileValidationServiceConstants', 'seMediaService', 'seMediaUploadFormConstants', 'seObjectValidatorFactory', 'seMediaUploadFormValidators', 'seBackendValidationHandler', 'escapeHtml', 'assetsService', '$scope', function(seFileValidationServiceConstants, seMediaService, seMediaUploadFormConstants, seObjectValidatorFactory, seMediaUploadFormValidators, seBackendValidationHandler, escapeHtml, assetsService, $scope) {
        this.i18nKeys = seMediaUploadFormConstants.I18N_KEYS;
        this.acceptedFileTypes = seFileValidationServiceConstants.ACCEPTED_FILE_TYPES;

        this.imageParameters = {};
        this.isUploading = false;
        this.fieldErrors = [];

        this.assetsRoot = assetsService.getAssetsRoot();

        // TODO replace with this.$onChanges in Angular 1.5
        $scope.$watch(function() {
            return this.image;
        }.bind(this), function() {
            if (this.image && this.image.file) {
                this.imageParameters = {
                    code: this.image.file.name,
                    description: this.image.file.name,
                    altText: this.image.file.name
                };
            }
        }.bind(this));

        this.onCancel = function() {
            this.imageParameters = {};
            this.fieldErrors = [];
            this.isUploading = false;
            this.onCancelCallback();
        };

        this.onImageUploadSuccess = function(response) {
            this.imageParameters = {};
            this.fieldErrors = [];
            this.isUploading = false;
            this.onUploadCallback({
                uuid: response.uuid
            });
        };

        this.onImageUploadFail = function(response) {
            this.isUploading = false;
            seBackendValidationHandler.handleResponse(response, this.fieldErrors);
        };

        this.onMediaUploadSubmit = function() {
            this.fieldErrors = [];
            var validator = seObjectValidatorFactory.build(seMediaUploadFormValidators);
            if (validator.validate(this.imageParameters, this.fieldErrors)) {
                this.isUploading = true;

                seMediaService.uploadMedia({
                    file: this.image.file,
                    code: escapeHtml(this.imageParameters.code),
                    description: escapeHtml(this.imageParameters.description),
                    altText: escapeHtml(this.imageParameters.altText)
                }).then(this.onImageUploadSuccess.bind(this), this.onImageUploadFail.bind(this));
            }
        };

        this.getErrorsForField = function(field) {
            return this.fieldErrors.filter(function(error) {
                return error.subject === field;
            }).map(function(error) {
                return error.message;
            });
        };

        this.hasError = function(field) {
            return this.fieldErrors.some(function(error) {
                return error.subject === field;
            });
        };

        this.getTruncatedName = function() {
            var truncatedName = this.image && this.image.file && this.image.file.name || '';
            if (truncatedName.length > seMediaUploadFormConstants.MAX_FILENAME_CHARACTERS_DISPLAYED) {
                truncatedName = truncatedName.substring(0, seMediaUploadFormConstants.MAX_FILENAME_CHARACTERS_DISPLAYED);
                truncatedName += '...';
            }
            return truncatedName;
        };
    }])
    .directive('seMediaUploadForm', function() {
        return {
            templateUrl: 'seMediaUploadFormTemplate.html',
            restrict: 'E',
            scope: {},
            controller: 'seMediaUploadFormController',
            controllerAs: 'ctrl',
            bindToController: {
                field: '=',
                image: '=',
                onUploadCallback: '&',
                onCancelCallback: '&',
                onSelectCallback: '&'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("mediaServiceModule", ['resourceModule', 'resourceLocationsModule', 'functionsModule', 'restServiceFactoryModule'])
    /**
     * @ngdoc service
     * @name mediaServiceModule.service:mediaService
     * @description
     * Service to deal with media related CRUD operations
     */
    .service("mediaService", ['restServiceFactory', 'MEDIA_PATH', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'isBlank', function(restServiceFactory, MEDIA_PATH, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, isBlank) {

        this.uriParameters = {};


        /*
         * @ngdoc method
         * @name mediaServiceModule.service:mediaService#getPage
         * @methodOf mediaServiceModule.service:mediaService
         *
         * Fetch paged search results by making a REST call to the appropriate item end point.
         * Must return a Page of type Page as per SmartEdit documentation
         * @param {String} mask for filtering the search
         * @param {String} pageSize number of items in the page
         * @param {String} currentPage current page number
         * @param {Object} parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        this.getPage = function(mask, pageSize, currentPage, parameters) {

            this.uriParameters = parameters || {};

            var payload = {
                catalogId: this.uriParameters[CONTEXT_CATALOG] || CONTEXT_CATALOG,
                catalogVersion: this.uriParameters[CONTEXT_CATALOG_VERSION] || CONTEXT_CATALOG_VERSION
            };
            if (!isBlank(mask)) {
                payload.code = mask;
            }

            var subParams = Object.keys(payload).reduce(function(accumulator, next) {
                accumulator += "," + next + ":" + payload[next];
                return accumulator;
            }, "").substring(1);

            var params = {
                namedQuery: "namedQueryMediaSearchByCodeCatalogVersion",
                params: subParams,
                pageSize: pageSize,
                currentPage: currentPage
            };
            return restServiceFactory.get(MEDIA_PATH).get(params).then(function(response) {
                response.results = response.media.map(function(media) {
                    return {
                        id: media.uuid,
                        code: media.code,
                        description: media.description,
                        altText: media.altText,
                        url: media.url,
                        downloadUrl: media.downloadUrl
                    };
                });
                delete response.media;
                return response;
            });

        };

        /*
         * @ngdoc method
         * @name mediaServiceModule.service:mediaService#getMedia
         * @methodOf mediaServiceModule.service:mediaService
         *
         * @description
         * This method fetches a Media by its UUID
         * @param {String} uuid the universal uid the unique identifier of a media (contains catalog information)
         */
        this.getMedia = function(uuid, parameters) {
            this.uriParameters = parameters;
            //identifier is added to URI and not getByid argument because it contains slashes
            return restServiceFactory.get(MEDIA_PATH + "/" + uuid).get().then(function(media) {
                return {
                    id: media.uuid,
                    code: media.code,
                    description: media.description,
                    altText: media.altText,
                    url: media.url,
                    downloadUrl: media.downloadUrl
                };
            });
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seBackendValidationHandlerModule
 * @description
 * This module provides the seBackendValidationHandler service, which handles standard OCC validation errors received
 * from the backend.
 */
angular.module('seBackendValidationHandlerModule', [])

/**
 * @ngdoc service
 * @name seBackendValidationHandlerModule.seBackendValidationHandler
 * @description
 * The seBackendValidationHandler service handles validation errors received from the backend.
 */
.factory('seBackendValidationHandler', function() {

    /**
     * @ngdoc method
     * @name seBackendValidationHandlerModule.seBackendValidationHandler.handleResponse
     * @methodOf seBackendValidationHandlerModule.seBackendValidationHandler
     * @description
     * Extracts validation errors from the provided response and appends them to a specified contextual errors list.
     *
     * The expected error response from the backend matches the contract of the following response example:
     *
     * <pre>
     * var response = {
     *     data: {
     *         errors: [{
     *             type: 'ValidationError',
     *             subject: 'mySubject',
     *             message: 'Some validation exception occurred'
     *         }, {
     *             type: 'SomeOtherError',
     *             subject: 'mySubject'
     *             message: 'Some other exception occurred'
     *         }]
     *     }
     * }
     * </pre>
     *
     * Example of use:
     * <pre>
     * var errorsContext = [];
     * seBackendValidationHandler.handleResponse(response, errorsContext);
     * </pre>
     *
     * The resulting errorsContext would be as follows:
     * <pre>
     * [{
     *     subject: 'mySubject',
     *     message: 'Some validation exception occurred'
     * }]
     * </pre>
     *
     * @param {Object} response A response consisting of a list of errors; for details of the expected format, see the
     * example above.
     * @param {Array} errorsContext An array that all validation errors are appended to. It is an output parameter.
     * @returns {Array} The error context list originally provided, or a new list, appended with the validation errors
     */
    var handleResponse = function(response, errorsContext) {
        errorsContext = errorsContext || [];
        if (response && response.data && response.data.errors) {
            response.data.errors.filter(function(error) {
                return error.type === 'ValidationError';
            }).forEach(function(validationError) {
                var subject = validationError.subject;
                if (subject) {
                    errorsContext.push({
                        'subject': subject,
                        'message': validationError.message
                    });
                }
            });
        }
        return errorsContext;
    };

    return {
        handleResponse: handleResponse
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seFileMimeTypeServiceModule', [])
    .constant('seFileMimeTypeServiceConstants', {
        VALID_IMAGE_MIME_TYPE_CODES: ['FFD8FFDB', 'FFD8FFE0', 'FFD8FFE1', '474946383761', '424D', '49492A00', '4D4D002A', '89504E470D0A1A0A']
    })
    .factory('seFileReader', function() {
        var read = function(file, config) {
            var fileReader = new FileReader();

            config = config || {};
            fileReader.onloadend = config.onLoadEnd;
            fileReader.onerror = config.onError;

            fileReader.readAsArrayBuffer(file);
            return fileReader;
        };

        return {
            read: read
        };
    })
    .factory('seFileMimeTypeService', ['seFileMimeTypeServiceConstants', 'seFileReader', '$q', function(seFileMimeTypeServiceConstants, seFileReader, $q) {
        var _validateMimeTypeFromFile = function(loadedFile) {
            var fileAsBytes = (new Uint8Array(loadedFile)).subarray(0, 8);
            var header = fileAsBytes.reduce(function(header, byte) {
                var byteAsStr = byte.toString(16);
                if (byteAsStr.length === 1) {
                    byteAsStr = '0' + byteAsStr;
                }
                header += byteAsStr;
                return header;
            }, '');

            return seFileMimeTypeServiceConstants.VALID_IMAGE_MIME_TYPE_CODES.some(function(mimeTypeCode) {
                return header.toLowerCase().indexOf(mimeTypeCode.toLowerCase()) === 0;
            });
        };

        var isFileMimeTypeValid = function(file) {
            var deferred = $q.defer();
            seFileReader.read(file, {
                onLoadEnd: function(e) {
                    if (_validateMimeTypeFromFile(e.target.result)) {
                        deferred.resolve();
                    } else {
                        deferred.reject();
                    }
                },
                onError: function() {
                    deferred.reject();
                }
            });
            return deferred.promise;
        };

        return {
            isFileMimeTypeValid: isFileMimeTypeValid
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seFileValidationServiceModule
 * @description
 * This module provides the seFileValidationService service, which validates if a specified file meets the required file
 * type and file size constraints of SAP Hybris Commerce.
 */
angular.module('seFileValidationServiceModule', ['seObjectValidatorFactoryModule', 'seFileMimeTypeServiceModule'])

/**
 * @ngdoc object
 * @name seFileValidationServiceModule.seFileValidationServiceConstants
 * @description
 * The constants provided by the file validation service.
 *
 * <b>ACCEPTED_FILE_TYPES</b>: A list of file types supported by the platform.
 * <b>MAX_FILE_SIZE_IN_BYTES</b>: The maximum size, in bytes, for an uploaded file.
 * <b>I18N_KEYS</b>: A map of all the internationalization keys used by the file validation service.
 */
.constant('seFileValidationServiceConstants', {
    ACCEPTED_FILE_TYPES: ['jpeg', 'jpg', 'gif', 'bmp', 'tiff', 'tif', 'png'],
    MAX_FILE_SIZE_IN_BYTES: 20 * 1024 * 1024,
    I18N_KEYS: {
        FILE_TYPE_INVALID: 'se.upload.file.type.invalid',
        FILE_SIZE_INVALID: 'se.upload.file.size.invalid'
    }
})

/**
 * @ngdoc object
 * @name seFileValidationServiceModule.seFileObjectValidators
 * @description
 * A list of file validators, that includes a validator for file-size constraints and a validator for file-type
 * constraints.
 */
.factory('seFileObjectValidators', ['seFileValidationServiceConstants', function(seFileValidationServiceConstants) {
    return [{
        subject: 'size',
        message: seFileValidationServiceConstants.I18N_KEYS.FILE_SIZE_INVALID,
        validate: function(size) {
            return size <= seFileValidationServiceConstants.MAX_FILE_SIZE_IN_BYTES;
        }
    }];
}])

/**
 * @ngdoc service
 * @name seFileValidationServiceModule.seFileValidationService
 * @description
 * The seFileValidationService validates that the file provided is of a specified file type and that the file does not
 * exceed the maxium size limit for the file's file type.
 */
.factory('seFileValidationService', ['seObjectValidatorFactory', 'seFileObjectValidators', 'seFileValidationServiceConstants', 'seFileMimeTypeService', '$q', function(seObjectValidatorFactory, seFileObjectValidators, seFileValidationServiceConstants, seFileMimeTypeService, $q) {
    /**
     * @ngdoc method
     * @name seFileValidationServiceModule.seFileValidationService.buildAcceptedFileTypesList
     * @methodOf seFileValidationServiceModule.seFileValidationService
     * @description
     * Transforms the supported file types provided by the seFileValidationServiceConstants service into a comma-
     * separated list of file type extensions.
     *
     * @returns {String} A comma-separated list of supported file type extensions
     */
    var buildAcceptedFileTypesList = function() {
        return seFileValidationServiceConstants.ACCEPTED_FILE_TYPES.map(function(fileType) {
            return '.' + fileType;
        }).join(',');
    };

    /**
     * @ngdoc method
     * @name seFileValidationServiceModule.seFileValidationService.validate
     * @methodOf seFileValidationServiceModule.seFileValidationService
     * @description
     * Validates the specified file object using validator provided by the {@link
     * seFileValidationServiceModule.seFileObjectValidators seFileObjectValidators} and the file header validator
     * provided by the {@link seFileMimeTypeServiceModule.seFileMimeTypeService seFileMimeTypeService}. It appends the
     * errors to the error context array provided or it creates a new error context array.
     *
     * @param {File} file The web API file object to be validated.
     * @param {Array} context The contextual error array to append the errors to. It is an output parameter.
     * @returns {Promise} A promise that resolves if the file is valid or a list of errors if the promise is rejected.
     */
    var validate = function(file, errorsContext) {
        seObjectValidatorFactory.build(seFileObjectValidators).validate(file, errorsContext);
        return seFileMimeTypeService.isFileMimeTypeValid(file).then(function() {
            return errorsContext.length === 0 ? $q.when() : $q.reject(errorsContext);
        }, function() {
            errorsContext.push({
                subject: 'type',
                message: seFileValidationServiceConstants.I18N_KEYS.FILE_TYPE_INVALID
            });
            return $q.reject(errorsContext);
        });
    };

    return {
        buildAcceptedFileTypesList: buildAcceptedFileTypesList,
        validate: validate
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seMediaServiceModule
 * @description
 * The media service module provides a service to create an image file for a catalog through AJAX calls. This module
 * uses a dedicated transformed $resource that posts a multipart form data in the request.
 * @deprecated since 6.4, use mediaServiceModule
 */
angular.module('seMediaServiceModule', ['resourceLocationsModule', 'experienceInterceptorModule', 'ngResource'])

/**
 * @ngdoc service
 * @name seMediaServiceModule.seMediaResource
 * @description
 * A {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource} that makes REST calls to the default
 * CMS catalog media API. It supports HTTP GET and POST methods against this API. The GET method is used to retrieve a collection of media.
 *
 * The POST methods transform the POJO into a {@link https://developer.mozilla.org/en/docs/Web/API/FormData FormData}
 * object before the request is made to the API. This transformation is required for file uploads that use the
 * Content-Type 'multipart/form-data'.
 * @deprecated since 6.4, use mediaService
 */
.factory('seMediaResource', ['$resource', 'MEDIA_RESOURCE_URI', function($resource, MEDIA_RESOURCE_URI) {
    return $resource(MEDIA_RESOURCE_URI, {}, {
        get: {
            method: 'GET',
            cache: true,
            headers: {}
        },
        save: {
            method: 'POST',
            headers: {
                'Content-Type': undefined,
                enctype: 'multipart/form-data',
                'x-requested-with': 'Angular'
            },
            transformRequest: function(data) {
                var formData = new FormData();
                angular.forEach(data, function(value, key) {
                    formData.append(key, value);
                });
                return formData;
            }
        }
    });
}])

/**
 * @ngdoc service
 * @name seMediaServiceModule.seMediaResourceService
 * @description
 * This service provides an interface to the {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource} that makes REST 
 * calls to the default CMS catalog media API. It supports HTTP GET method returning against this API. 
 * The GET method is used to retrieve a single media.
 */
.factory('seMediaResourceService', ['$resource', 'MEDIA_RESOURCE_URI', function($resource, MEDIA_RESOURCE_URI) {

    var getMediaByCode = function(mediaCode) {
        return $resource(MEDIA_RESOURCE_URI + "/" + mediaCode, {}, {
            get: {
                method: 'GET',
                cache: true,
                headers: {}
            }
        });
    };

    return {
        getMediaByCode: getMediaByCode
    };
}])

/**
 * @ngdoc service
 * @name seMediaServiceModule.seMediaService
 * @description
 * This service provides an interface to the {@link https://docs.angularjs.org/api/ngResource/service/$resource
 * $resource} provided by the {@link seMediaServiceModule.seMediaResource seMediaResource} service and 
 * the {@link seMediaServiceModule.seMediaResourceService seMediaResourceService} service. It provides the
 * functionality to upload images and to fetch images by code for a specific catalog-catalog version combination.
 */
.factory('seMediaService', ['seMediaResource', 'seMediaResourceService', function(seMediaResource, seMediaResourceService) {
    /**
     * @ngdoc method
     * @name seMediaServiceModule.seMediaService.uploadMedia
     * @methodOf seMediaServiceModule.seMediaService
     *
     * @description
     * Uploads the media to the catalog.
     *
     * @param {Object} media The media to be uploaded
     * @param {String} media.code A unique code identifier for the media
     * @param {String} media.description A description of the media
     * @param {String} media.altText An alternate text to be shown for the media
     * @param {File} media.file The {@link https://developer.mozilla.org/en/docs/Web/API/File File} object to be
     * uploaded.
     *
     * @returns {Promise} If request is successful, it returns a promise that resolves with the media POJO. If the
     * request fails, it resolves with errors from the backend.
     */
    var uploadMedia = function(media) {
        return seMediaResource.save(media).$promise;
    };

    /**
     * @ngdoc method
     * @name seMediaServiceModule.seMediaService.getMediaByCode
     * @methodOf seMediaServiceModule.seMediaService
     *
     * @description
     * Fetches the media for the selected catalog corresponding to the specified code
     *
     * @param {String} code A unique code identifier that corresponds to the media as it exists in the backend.
     *
     * @returns {Promise} If request is successful, it returns a promise that resolves with the media POJO. If the
     * request fails, it resolves with errors from the backend.
     */
    var getMediaByCode = function(code) {
        return seMediaResourceService.getMediaByCode(code).get().$promise;
    };

    return {
        uploadMedia: uploadMedia,
        getMediaByCode: getMediaByCode
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seObjectValidatorFactoryModule
 * @description
 * This module provides the seObjectValidatorFactory service, which is used to build a validator for a specified list of
 * validator objects.
 */
angular.module('seObjectValidatorFactoryModule', [])

/**
 * @ngdoc service
 * @name seObjectValidatorFactoryModule.seObjectValidatorFactory
 * @description
 * This service provides a factory method to build a validator for a specified list of validator objects.
 */
.factory('seObjectValidatorFactory', function() {
    function _validate(validators, objectUnderValidation, errorsContext) {
        errorsContext = errorsContext || [];
        validators.forEach(function(validator) {
            var valueToValidate = objectUnderValidation[validator.subject];
            if (!validator.validate(valueToValidate, objectUnderValidation)) {
                errorsContext.push({
                    'subject': validator.subject,
                    'message': validator.message
                });
            }
        });
        return errorsContext.length === 0;
    }

    /**
     * @ngdoc method
     * @name seObjectValidatorFactoryModule.seObjectValidatorFactory.build
     * @methodOf seObjectValidatorFactoryModule.seObjectValidatorFactory
     * @description
     *
     * Builds a new validator for a specified list of validator objects. Each validator object must consist of a
     * parameter to validate, a predicate function to run against the value, and a message to associate with this
     * predicate function's fail case.
     *
     * For example, The resulting validating object has a single validate function that takes two parameters: an object
     * to validate against and a contextual error list to append errors to:
     *
     * <pre>
     * var validators = [{
     *     subject: 'code',
     *     validate: function(code) {
     *         return code !== 'Invalid';
     *     },
     *     message: 'Code must not be "Invalid"'
     * }]
     *
     * var validator = seObjectValidatorFactory.build(validators);
     * var errorsContext = []
     * var objectUnderValidation = {
     *     code: 'Invalid'
     * };
     * var isValid = validate.validate(objectUnderValidation, errorsContext);
     * </pre>
     *
     * The result of the above code block would be that isValid is false beause it failed the predicate function of the
     * single validator in the validator list and the errorsContext would be as follows:
     *
     * <pre>
     * [{
     *     subject: 'code',
     *     message: 'Code must not be "Invalid"'
     * }]
     * </pre>
     *
     * @param {Array} validators A list of validator objects as specified above.
     * @returns {Object} A validator that consists of a validate function, as described above.
     */
    var build = function(validators) {
        return {
            validate: _validate.bind(null, validators)
        };
    };

    return {
        build: build
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seBreadcrumbModule', ['resourceLocationsModule',
        'navigationEditorNodeServiceModule',
        'resourceModule',
        'functionsModule',
        'assetsServiceModule',
        'cmsitemsRestServiceModule'
    ])
    .controller('BreadcrumbController', ['$q', 'navigationEditorNodeService', 'assetsService', 'cmsitemsRestService', function($q, navigationEditorNodeService, assetsService, cmsitemsRestService) {

        this.$onInit = function() {

            if (!this.nodeUid && !this.nodeUuid) {
                throw new Error("seBreadcrumb component requires either nodeUid or nodeUuid");
            }

            var promise = this.nodeUid ? $q.when({
                uid: this.nodeUid
            }) : cmsitemsRestService.getById(this.nodeUuid);

            promise.then(function(response) {

                navigationEditorNodeService.getNavigationNodeAncestry(response.uid, this.uriContext).then(function(ancestry) {
                    this.breadcrumb = ancestry;
                }.bind(this));

            }.bind(this));


        };
        this.arrowIconUrl = assetsService.getAssetsRoot() + '/images/slash_icon.png';
    }])
    /**
     * @ngdoc directive
     * @name seBreadcrumbModule.directive:seBreadcrumb
     * @scope
     * @restrict E
     * @element ANY
     *
     * @description
     * Directive that will build a navigation breadcrumb for the Node identified by either uuid or uid.
     * @param {String=} nodeUid the uid of the node the breadcrumb of which we want to build.
     * @param {String=} nodeUuid the uuid of the node the breadcrumb of which we want to build.
     * @param {Object} uriContext the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations.
     */
    .component('seBreadcrumb', {
        templateUrl: 'seBreadcrumbTemplate.html',
        bindings: {
            nodeUid: '<?',
            nodeUuid: '<?',
            uriContext: '<'
        },
        controller: 'BreadcrumbController',
        controllerAs: 'bc',
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seNavigationNodePickerModule', ['functionsModule', 'resourceLocationsModule', 'cmsitemsRestServiceModule', 'eventServiceModule'])
    .constant('SELECTED_NODE', 'selected_node')
    .controller('NavigationNodePickerController', ['URIBuilder', 'NAVIGATION_MANAGEMENT_RESOURCE_URI', 'NAVIGATION_NODE_ROOT_NODE_UID', 'SELECTED_NODE', 'cmsitemsRestService', 'systemEventService', function(URIBuilder, NAVIGATION_MANAGEMENT_RESOURCE_URI, NAVIGATION_NODE_ROOT_NODE_UID, SELECTED_NODE, cmsitemsRestService, systemEventService) {

        this.$onInit = function() {
            this.nodeURI = new URIBuilder(NAVIGATION_MANAGEMENT_RESOURCE_URI).replaceParams(this.uriContext).build();
        };

        this.nodeTemplateUrl = 'navigationNodePickerRenderTemplate.html';
        this.rootNodeUid = NAVIGATION_NODE_ROOT_NODE_UID;

        this.actions = {
            pick: function(treeService, handle) {
                var requestParams = {
                    pageSize: 10,
                    currentPage: 0,
                    mask: handle.$modelValue.uid,
                };

                cmsitemsRestService.get(requestParams).then(function(response) {

                    var idObj = {
                        nodeUuid: response.response.find(function(element) {
                            return element.uid === handle.$modelValue.uid;
                        }).uuid,
                        nodeUid: handle.$modelValue.uid
                    };
                    systemEventService.sendAsynchEvent(SELECTED_NODE, idObj);
                }.bind(this));

            }.bind(this)
        };

    }])
    /**
     * @ngdoc directive
     * @name seNavigationNodePickerModule.directive:seNavigationPicker
     * @scope
     * @restrict E
     * @element ANY
     *
     * @description
     * Directive that will build a navigation node picker and assign the uid of the selected node to model[qualifier].
     * @param {Object} uriContext the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations.
     * @param {Object} model the model a property a property of which will be set to the selected node uid.
     * @param {String} qualifier the name of the model property that will be set to the selected node uid.
     */
    .component('seNavigationPicker', {
        templateUrl: 'seNavigationNodePickerTemplate.html',
        bindings: {
            uriContext: '<',
            model: '=',
            qualifier: '=',
        },
        controller: 'NavigationNodePickerController',
        controllerAs: 'ctrl',
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seNavigationNodeSelector', ['catalogServiceModule', 'seBreadcrumbModule', 'seNavigationNodePickerModule', 'resourceLocationsModule', 'cmsitemsRestServiceModule', 'eventServiceModule'])
    .controller('NavigationNodeSelectorController', ['SELECTED_NODE', 'catalogService', 'cmsitemsRestService', 'systemEventService', function(SELECTED_NODE, catalogService, cmsitemsRestService, systemEventService) {

        this.isReady = function() {
            return this.ready;
        };

        this.remove = function() {
            delete this.model[this.qualifier];
        }.bind(this);

        this.$onInit = function() {
            this.ready = false;
            catalogService.retrieveUriContext().then(function(uriContext) {
                this.uriContext = uriContext;

                if (this.model[this.qualifier]) {
                    cmsitemsRestService.getById(this.model[this.qualifier]).then(function(response) {
                        this.nodeUid = response.uid;
                        this.ready = true;
                    }.bind(this));
                } else {
                    this.ready = true;
                }

            }.bind(this));

            this.unregFn = systemEventService.registerEventHandler(SELECTED_NODE, function(eventId, idObj) {
                this.nodeUid = idObj.nodeUid;
                this.model[this.qualifier] = idObj.nodeUuid;
            }.bind(this));

        };

        this.$onDestroy = function() {
            this.unregFn();
        };

    }])
    .component('seNavigationNodeSelector', {
        templateUrl: 'seNavigationNodeSelectorTemplate.html',
        controller: 'NavigationNodeSelectorController',
        controllerAs: 'nav',
        bindings: {
            field: '<',
            model: '<',
            qualifier: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('categoryDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule', 'contextAwareCatalogModule', 'uriDropdownPopulatorModule', 'functionsModule'])
    .factory('categoryDropdownPopulator', ['DropdownPopulatorInterface', 'extend', 'contextAwareCatalogService', 'uriDropdownPopulator', function(DropdownPopulatorInterface, extend, contextAwareCatalogService, uriDropdownPopulator) {
        var dropdownPopulator = function() {};
        dropdownPopulator = extend(DropdownPopulatorInterface, dropdownPopulator);

        dropdownPopulator.prototype.fetchPage = function(payload) {
            return contextAwareCatalogService.getProductCategorySearchUri(payload.model.productCatalog).then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.fetchPage(payload);
            }.bind(this));
        };

        dropdownPopulator.prototype.isPaged = function() {
            return true;
        };

        dropdownPopulator.prototype.getItem = function(payload) {
            return contextAwareCatalogService.getProductCategoryItemUri().then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.getItem(payload);
            }.bind(this));
        };

        return new dropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('productCatalogDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule', 'optionsDropdownPopulatorModule', 'resourceLocationsModule', 'catalogServiceModule', 'functionsModule'])
    .factory('productCatalogDropdownPopulator', ['DropdownPopulatorInterface', 'optionsDropdownPopulator', 'CONTEXT_SITE_ID', 'catalogService', 'extend', function(DropdownPopulatorInterface, optionsDropdownPopulator, CONTEXT_SITE_ID, catalogService, extend) {
        var dropdownPopulator = function() {};
        dropdownPopulator = extend(DropdownPopulatorInterface, dropdownPopulator);

        dropdownPopulator.prototype.fetchAll = function(payload) {
            return catalogService.getProductCatalogsForSite(CONTEXT_SITE_ID).then(function(catalogs) {
                payload.field.options = catalogs.filter(function(catalog) {
                    return catalog.versions.filter(function(version) {
                        return version.active === true;
                    }).length === 1;
                });
                return optionsDropdownPopulator.populate(payload);
            }.bind(this));
        };

        dropdownPopulator.prototype.isPaged = function() {
            return false;
        };

        return new dropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('productDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule', 'contextAwareCatalogModule', 'uriDropdownPopulatorModule', 'functionsModule'])
    .factory('productDropdownPopulator', ['DropdownPopulatorInterface', 'extend', 'contextAwareCatalogService', 'uriDropdownPopulator', function(DropdownPopulatorInterface, extend, contextAwareCatalogService, uriDropdownPopulator) {
        var dropdownPopulator = function() {};

        dropdownPopulator = extend(DropdownPopulatorInterface, dropdownPopulator);

        dropdownPopulator.prototype.fetchPage = function(payload) {
            return contextAwareCatalogService.getProductSearchUri(payload.model.productCatalog).then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.fetchPage(payload);
            }.bind(this));
        };

        dropdownPopulator.prototype.isPaged = function() {
            return true;
        };

        dropdownPopulator.prototype.getItem = function(payload) {
            return contextAwareCatalogService.getProductItemUri().then(function(uri) {
                payload.field.uri = uri;
                return uriDropdownPopulator.getItem(payload);
            }.bind(this));
        };

        return new dropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('singleActiveCatalogAwareItemSelectorModule', ['resourceLocationsModule', 'catalogServiceModule', 'yLoDashModule', 'l10nModule'])
    .controller('SingleActiveCatalogAwareItemSelectorController', ['catalogService', 'CONTEXT_SITE_ID', function(catalogService, CONTEXT_SITE_ID) {
        this.$onInit = function() {
            augmentDropdownsAttributes.call(this);
            initProductCatalogs.call(this);
        };

        /**
         * Augment the seDropdown attributes bindings to init the seDropdown with proper settings
         * The 'propertyType' value enable the usage of custom populators in seDropdown
         */
        var augmentDropdownsAttributes = function() {
            this.productCatalogField = {
                idAttribute: 'catalogId',
                labelAttributes: ['name'],
                editable: true,
                propertyType: 'productCatalog',
            };

            this.mainDropDownI18nKey = this.field.i18nKey;
            delete this.field.i18nKey;
            this.field.paged = true;
            this.field.editable = true;
            this.field.idAttribute = 'uid';
            this.field.labelAttributes = ['name'];
            this.field.dependsOn = 'productCatalog';
            this.field.propertyType = this.qualifier;
        };

        /*
            Filter on active product catalogs:
            - If there is only one product catalog, will hide the product catalog seDropDown and show the product catalog name
            - If there is more than one product catalog, will show the product catalog seDropDown
        */
        var initProductCatalogs = function() {
            catalogService.getProductCatalogsForSite(CONTEXT_SITE_ID).then(function(catalogs) {
                this.catalogs = catalogs;
                if (this.catalogs.length === 1) {
                    this.model.productCatalog = this.catalogs[0].catalogId;
                    this.catalogName = this.catalogs[0].name;
                }
            }.bind(this));
        };
    }])
    .component('singleActiveCatalogAwareItemSelector', {
        templateUrl: 'singleActiveCatalogAwareItemSelectorTemplate.html',
        controller: 'SingleActiveCatalogAwareItemSelectorController',
        controllerAs: 'ctrl',
        bindings: {
            field: '<',
            id: '<',
            model: '<',
            qualifier: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name itemManagementModule
 * @description
 * This module contains the itemManager component.
 */
angular.module('itemManagementModule', ['functionsModule'])
    .constant('ITEM_MANAGEMENT_EDITOR_ID', 'se-item-management-editor')
    .controller('ItemManagementController', ['$q', 'ITEM_MANAGEMENT_EDITOR_ID', function($q, ITEM_MANAGEMENT_EDITOR_ID) {

        var supportedModes = ['add', 'edit', 'create'];

        this.editorId = ITEM_MANAGEMENT_EDITOR_ID;

        this._submitInternal = function() {
            switch (this.mode.toLowerCase()) {
                case "add":
                    return $q.when(this.item);
                case "edit":
                    return this.submit();
                case "create":
                    return this.submit().then(function(itemResponse) {
                        return itemResponse;
                    }.bind(this));

                default:
                    throw "ItemManagementController - The given mode [" + this.mode + "] has not been implemented for this component";
            }
        }.bind(this);

        this._isDirtyLocal = function _isDirtyLocal() {
            if (this.isDirtyInternal) {
                return this.isDirtyInternal();
            }
            return false;
        }.bind(this);

        // all overridden by the embedded generic editor
        this.isDirty = function isDirty() {};
        this.submit = function submit() {};
        this.reset = function reset() {};

        this.$onInit = function $onInit() {
            if (supportedModes.indexOf(this.mode) === -1) {
                throw "ItemManagementController.$onInit() - Mode not supported: " + this.mode;
            }
            this.submitFunction = this._submitInternal;
            this.isDirty = this._isDirtyLocal;

            if (!this.componentType && this.item) {
                this.componentType = this.item.typeCode;
            }
            if (!this.item) {
                this.itemId = null;
            }

            if (this.item && this.item.uuid) {
                this.itemId = this.item.uuid;
            } else if (this.item && this.item.uid) {
                this.itemId = this.item.uid;
            }

        };

    }])


/**
 * @ngdoc directive
 * @name itemManagementModule.itemManager
 * @description
 * The purpose of this component is handle the logic of displaying the fields and PUT/POST logic
 * to add, edit or create CMS Items
 *
 * @param {Object} item An item to use as preset fields in the editor
 * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
 * @param {String} mode Either 'edit', 'add', or 'create'
 * @param {String} contentApi A URI for GET/PUT/POST of Item content
 * @param {String} structureApi A URI for fetching the structure of the editor
 * @param {String} componentType Component type code for the underlying generic editor
 * @param {Function} isDirty A function defined within itemManager. Returns true when editing an Item, creating a new
 * non-empty Item, or adding an existing Item.
 * @param {Function} submitFunction A function defined within itemManager. Call this function to invoke submit,
 * triggering any PUT/POST operations and returning the item created/edited, wrapped in a promise.
 */
.component('itemManager', {
    controller: "ItemManagementController",
    templateUrl: 'itemManagementTemplate.html',
    bindings: {
        // in
        item: '<',
        uriContext: '<',
        mode: '<', // add, edit, or create
        contentApi: '<',
        structureApi: '<',
        componentType: '<?',
        // out
        isDirty: '=',
        submitFunction: '='
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("itemTitleAbstractCMSComponentHandlerServiceModule", ['resourceModule', 'functionsModule', 'resourceLocationsModule', 'componentServiceModule', 'itemTitleStrategyInterfaceModule'])
    .factory("itemTitleAbstractCMSComponentHandlerService", ['$q', 'extend', 'ITEMS_RESOURCE_URI', 'URIBuilder', 'restServiceFactory', 'ItemTitleStrategyInterface', function($q, extend, ITEMS_RESOURCE_URI, URIBuilder, restServiceFactory, ItemTitleStrategyInterface) {

        var itemTitleAbstractCMSComponentHandlerService = function() {};
        itemTitleAbstractCMSComponentHandlerService = extend(ItemTitleStrategyInterface, itemTitleAbstractCMSComponentHandlerService);

        itemTitleAbstractCMSComponentHandlerService.prototype.getItemTitleById = function(itemId, uriParameters) {

            var itemsUri = new URIBuilder(ITEMS_RESOURCE_URI).replaceParams(uriParameters).build();
            var restServiceItemsResource = restServiceFactory.get(itemsUri);
            return restServiceItemsResource.getById(itemId).then(function(response) {
                var itemInfo = {};
                itemInfo.itemType = response.typeCode;
                if (response.typeCode === "CMSLinkComponent") {
                    itemInfo.title = response.linkName;
                    return itemInfo;
                } else {
                    itemInfo.title = response.name;
                    return itemInfo;
                }
            });
        };

        return new itemTitleAbstractCMSComponentHandlerService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("itemTitleAbstractPageHandlerServiceModule", ['functionsModule', 'resourceModule', 'resourceLocationsModule', 'componentServiceModule', 'itemTitleStrategyInterfaceModule'])
    .factory("itemTitleAbstractPageHandlerService", ['$q', 'extend', 'restServiceFactory', 'ItemTitleStrategyInterface', 'URIBuilder', 'PAGES_LIST_RESOURCE_URI', function($q, extend, restServiceFactory, ItemTitleStrategyInterface, URIBuilder, PAGES_LIST_RESOURCE_URI) {

        var itemTitleAbstractPageHandlerService = function() {};
        itemTitleAbstractPageHandlerService = extend(ItemTitleStrategyInterface, itemTitleAbstractPageHandlerService);

        itemTitleAbstractPageHandlerService.prototype.getItemTitleById = function(itemId, uriContext) {

            var pagesUri = new URIBuilder(PAGES_LIST_RESOURCE_URI).replaceParams(uriContext).build();
            var restServiceItemsResource = restServiceFactory.get(pagesUri);
            return restServiceItemsResource.getById(itemId).then(function(response) {
                var itemInfo = {};
                itemInfo.itemType = response.typeCode;
                itemInfo.title = response.name;
                return itemInfo;
            });
        };

        return new itemTitleAbstractPageHandlerService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("itemTitleMediaHandlerServiceModule", ['resourceModule', 'functionsModule', 'resourceLocationsModule', 'functionsModule', 'itemTitleStrategyInterfaceModule'])
    .factory("itemTitleMediaHandlerService", ['extend', 'URIBuilder', 'MEDIA_RESOURCE_URI', 'ItemTitleStrategyInterface', 'restServiceFactory', function(extend, URIBuilder, MEDIA_RESOURCE_URI, ItemTitleStrategyInterface, restServiceFactory) {

        var itemTitleMediaHandlerService = function() {
            this.uriParameters = {};
        };
        itemTitleMediaHandlerService = extend(ItemTitleStrategyInterface, itemTitleMediaHandlerService);


        itemTitleMediaHandlerService.prototype.getItemTitleById = function(itemId, uriParameters) {
            var mediaUri = new URIBuilder(MEDIA_RESOURCE_URI + '/' + itemId).replaceParams(uriParameters).build();

            var restServiceMediaSearchResource = restServiceFactory.get(mediaUri);
            return restServiceMediaSearchResource.get().then(function(response) {
                var itemInfo = {};
                itemInfo.itemType = "Media";
                itemInfo.title = response.code;
                return itemInfo;
            });
        };

        return new itemTitleMediaHandlerService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('itemTitleStrategyInterfaceModule', [])
    /**
     * @ngdoc service
     * @name itemStrategyInterfaceModule.service:ItemTitleStrategyInterface
     * @description
     * Interface describing the contract to retrieve an item in navigation editor.
     * It is used in the navigation editor to display the entry's item title, and it does that by finding 
     * the service handler by name ie. "'item' + itemTypeSelected + 'HandlerService'".
     */
    .factory('ItemTitleStrategyInterface', function() {

        var ItemTitleStrategyInterface = function() {};

        /**
         * @ngdoc method
         * @name itemStrategyInterfaceModule.service:ItemTitleStrategyInterface#getItemById
         * @methodOf itemStrategyInterfaceModule.service:ItemTitleStrategyInterface
         *
         * @description
         * Get the Item title for a given itemId by making a call to the cmswebservices item type API. 
         *
         * @param {String} itemId the item unique identifier
         * @param {Object} parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        ItemTitleStrategyInterface.prototype.getItemTitleById = function() {
            throw "getItemById is not implemented";
        };

        return ItemTitleStrategyInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    angular.module("navigationEditorModule", [
            'functionsModule',
            'navigationEditorNodeServiceModule',
            'resourceLocationsModule',
            'navigationNodeEditorModalServiceModule',
            'navigationEntryItemServiceModule',
            'confirmationModalServiceModule',
            'resourceModule',
            'eventServiceModule',
            'yLoDashModule'
        ])
        .constant('NAVIGATION_NODE_ROOT_NODE_UID', 'root')
        .controller("navigationEditorController", ['$q', '$translate', 'lodash', 'URIBuilder', 'NAVIGATION_MANAGEMENT_RESOURCE_URI', 'NAVIGATION_NODE_ROOT_NODE_UID', 'TreeService', 'navigationEditorNodeService', 'navigationNodeEditorModalService', 'navigationEntryItemService', 'confirmationModalService', 'navigationNodeRestService', 'systemEventService', 'CONTEXT_SITE_ID', 'CONTEXT_CATALOG_VERSION', 'NODE_CREATION_EVENT', function($q, $translate, lodash, URIBuilder, NAVIGATION_MANAGEMENT_RESOURCE_URI, NAVIGATION_NODE_ROOT_NODE_UID, TreeService, navigationEditorNodeService, navigationNodeEditorModalService, navigationEntryItemService, confirmationModalService, navigationNodeRestService, systemEventService, CONTEXT_SITE_ID, CONTEXT_CATALOG_VERSION, NODE_CREATION_EVENT) {

            this.$onInit = function() {

                this.readOnly = this.readOnly || false;

                if (!this.readOnly) {
                    this.dragOptions = {
                        onDropCallback: function(event) {
                            this.actions.dragAndDrop(event);
                        }.bind(this),
                        allowDropCallback: function(event) {
                            if (event.sourceNode.parent.uid !== event.destinationNodes[0].parent.uid) {
                                return false;
                            }

                            if (event.position < event.destinationNodes.length && !_sameNature(event.destinationNodes[event.position], event.sourceNode)) {
                                return false;
                            }
                            return true;
                        },
                        beforeDropCallback: function(event) {
                            if (event.sourceNode.parent.uid !== event.destinationNodes[0].parent.uid) {
                                return {
                                    confirmDropI18nKey: 'se.cms.navigationmanagement.navnode.confirmation'
                                };
                            } else {
                                return true;
                            }
                        }

                    };
                }

                this.readOnlyErrorKey = "navigation.in.readonly.mode";

                this.nodeTemplateUrl = 'navigationNodeRenderTemplate.html';

                this.nodeURI = new URIBuilder(NAVIGATION_MANAGEMENT_RESOURCE_URI).replaceParams(this.uriContext).build();

                this.rootNodeUid = this.rootNodeUid || NAVIGATION_NODE_ROOT_NODE_UID;

                var uriContext = this.uriContext;

                var dropdownItems = [{
                    key: 'se.cms.navigationmanagement.navnode.edit',
                    callback: function(handle) {
                        this.actions.editNavigationNode(handle);
                    }.bind(this)
                }, {
                    key: 'se.cms.navigationmanagement.navnode.removenode',
                    callback: function(handle) {
                        this.actions.removeItem(handle);
                    }.bind(this)
                }, {
                    key: 'se.cms.navigationmanagement.navnode.move.up',
                    condition: function(handle) {
                        return this.actions.isMoveUpAllowed(handle);
                    }.bind(this),
                    callback: function(handle) {
                        this.actions.moveUp(handle);
                    }.bind(this)
                }, {
                    key: 'se.cms.navigationmanagement.navnode.move.down',
                    condition: function(handle) {
                        return this.actions.isMoveDownAllowed(handle);
                    }.bind(this),
                    callback: function(handle) {
                        this.actions.moveDown(handle);
                    }.bind(this)
                }, {
                    key: 'se.cms.navigationmanagement.navnode.addchild',
                    condition: function(handle) {
                        return !handle.$modelValue.itemId;
                    },
                    callback: function(handle) {
                        this.actions.addNewChild(handle);
                    }.bind(this)
                }, {
                    key: 'se.cms.navigationmanagement.navnode.addsibling',
                    condition: function(handle) {
                        return !handle.$modelValue.itemId;
                    },
                    callback: function(handle) {
                        this.actions.addNewSibling(handle);
                    }.bind(this)
                }];

                //those functions will be closure bound inside ytree
                this.actions = {

                    isReadOnly: function() {
                        return this.readOnly;
                    }.bind(this),

                    hasChildren: function(treeService, handle) {
                        var nodeData = handle.$modelValue;
                        return nodeData.hasChildren || !lodash.isEmpty(nodeData.entries);
                    },

                    fetchData: function(treeService, nodeData) {
                        if (nodeData.initiated) {
                            return $q.when(nodeData.nodes);
                        } else {

                            //need to fetch entries of the node used as root since it was not initialized but only if it is not the absolute root
                            var promiseReturningTargetNodeEntries = (this.rootNodeUid === nodeData.uid && this.rootNodeUid !== NAVIGATION_NODE_ROOT_NODE_UID) ? navigationEditorNodeService.getNavigationNode(this.rootNodeUid, this.uriContext).then(function(response) {
                                lodash.assign(nodeData, response);
                                return response.entries;
                            }) : $q.when(nodeData.entries || []);

                            return promiseReturningTargetNodeEntries.then(function(entries) {
                                return navigationEntryItemService.finalizeNavigationEntries(entries, uriContext, true).then(function() {
                                    nodeData.nodes = [];
                                    entries.forEach(function(entry) {
                                        entry.parent = nodeData;
                                        nodeData.nodes.push(entry);
                                    });

                                    return treeService.fetchChildren(nodeData);
                                });
                            });
                        }

                    }.bind(this),


                    removeItem: function(treeService, handle) {

                        if (this.readOnly) {
                            throw this.readOnlyErrorKey;
                        }

                        var nodeData = handle.$modelValue;
                        var message = {};
                        message.description = nodeData.itemId ? "se.cms.navigationmanagement.navnode.removeentry.confirmation.message" : "se.cms.navigationmanagement.navnode.removenode.confirmation.message";
                        message.title = nodeData.itemId ? "se.cms.navigationmanagement.navnode.removeentry.confirmation.title" : "se.cms.navigationmanagement.navnode.removenode.confirmation.title";

                        confirmationModalService.confirm(message).then(function() {
                            if (!nodeData.itemId) {
                                this.remove(handle);
                            } else {

                                var parent = lodash.cloneDeep(nodeData.parent);
                                parent.entries = parent.entries
                                    .filter(function(entry) {
                                        return entry.id !== nodeData.id;
                                    })
                                    .map(function(entry) {
                                        delete entry.parent;
                                        delete entry.title;
                                        delete entry.id;
                                        return entry;
                                    });

                                var payload = angular.extend({
                                    identifier: parent.uid
                                }, uriContext, parent);
                                delete payload.parent;
                                delete payload.title;
                                delete payload.nodes;
                                navigationNodeRestService.update(payload).then(function() {
                                    var par = nodeData.parent;
                                    par.entries = par.entries
                                        .filter(function(entry) {
                                            return entry.id !== nodeData.id;
                                        })
                                        .map(function(entry, index) {
                                            entry.position = index;
                                            return entry;
                                        });
                                    this.refreshParentNode(handle);
                                }.bind(this));
                            }
                        }.bind(this));

                    },

                    performMove: function(treeService, nodeData, handle, refreshNodeItself) {
                        if (this.readOnly) {
                            throw this.readOnlyErrorKey;
                        }

                        return navigationEditorNodeService.updateNavigationNode(nodeData, uriContext).then(function() {

                            if (!handle) {
                                this.fetchData(this.root);
                            } else if (refreshNodeItself) {
                                this.refreshNode(handle);
                            } else {
                                this.refreshParentNode(handle);
                            }
                        }.bind(this));

                    },
                    dragAndDrop: function(treeService, event) {
                        var nodeData = event.sourceNode.itemId ? event.sourceNode.parent : event.sourceNode;
                        var destinationNodes = event.destinationNodes;

                        if (event.sourceNode.itemId) {
                            nodeData.entries = nodeData.nodes.filter(function(node) {
                                return node.itemId;
                            });
                        } else {
                            var offset = _recalculatePositionBasedOnNodesOfSameType(nodeData, destinationNodes, event.position);
                            var position = event.position - offset;

                            var destinationParent = (lodash.find(destinationNodes, function(node) {
                                return node.uid !== nodeData.uid;
                            })).parent;

                            if (_hasNotMoved(nodeData, event.position, destinationParent)) {
                                return;
                            }

                            nodeData.position = position;
                            nodeData.parentUid = destinationParent.uid;
                            nodeData.parent = destinationParent;
                        }
                        this.performMove(nodeData, event.targetParentHandle, true).then(function() {
                            if (event.sourceParentHandle !== event.targetParentHandle) {
                                this.refreshNode(event.sourceParentHandle);
                            }
                        }.bind(this));
                    },
                    moveUp: function(treeService, handle) {

                        if (this.readOnly) {
                            throw this.readOnlyErrorKey;
                        }

                        var nodeData = handle.$modelValue;
                        var parent = nodeData.parent;

                        if (nodeData.itemId) {
                            var pos = parent.entries.indexOf(nodeData);
                            var upperEntry = parent.entries[pos - 1];
                            parent.entries.splice(pos - 1, 2, nodeData, upperEntry);
                            this.performMove(parent, handle);
                        } else {
                            nodeData.position = parseInt(nodeData.position) - 1;
                            this.performMove(nodeData, handle);
                        }

                    },

                    moveDown: function(treeService, handle) {

                        if (this.readOnly) {
                            throw this.readOnlyErrorKey;
                        }

                        var nodeData = handle.$modelValue;
                        var parent = nodeData.parent;

                        if (nodeData.itemId) {
                            var pos = parent.entries.indexOf(nodeData);
                            var lowerEntry = parent.entries[pos + 1];
                            parent.entries.splice(pos, 2, lowerEntry, nodeData);
                            this.performMove(parent, handle);
                        } else {
                            nodeData.position = parseInt(nodeData.position) + 1;
                            this.performMove(nodeData, handle);
                        }

                    },

                    isMoveUpAllowed: function(treeService, handle) {

                        var nodeData = handle.$modelValue;
                        if (nodeData.itemId) {
                            return nodeData.parent.entries.indexOf(nodeData) > 0;
                        } else {
                            return parseInt(nodeData.position) !== 0;
                        }

                    },

                    isMoveDownAllowed: function(treeService, handle) {

                        var nodeData = handle.$modelValue;

                        if (nodeData.itemId) {
                            var entriesArrayLength = nodeData.parent.entries.length;

                            return nodeData.parent.entries.indexOf(nodeData) !== (entriesArrayLength - 1);
                        } else {
                            var nodesArrayLength = nodeData.parent.nodes.filter(function(node) {
                                return !node.itemId;
                            }).length;

                            return parseInt(nodeData.position) !== (nodesArrayLength - 1);

                        }
                    },

                    refreshNode: function(treeService, handle) {
                        return this.refresh(handle);
                    },
                    refreshParentNode: function(treeService, handle) {
                        return this.refreshParent(handle);
                    },

                    editNavigationNode: function(treeService, handle) {
                        var nodeData = handle.$modelValue;
                        var target = {};
                        //to differentiate between the edit of node and entry
                        if (handle.$modelValue.itemType) {
                            target.nodeUid = nodeData.parent.uid;
                            target.entryIndex = nodeData.parent.entries.indexOf(nodeData);
                        } else {
                            target.nodeUid = nodeData.uid;
                            target.entryIndex = undefined;
                        }
                        return navigationNodeEditorModalService.openNodeEditor(target, this.uriContext).then(function() {
                            var target;

                            if (nodeData.parent.uid === NAVIGATION_NODE_ROOT_NODE_UID) {
                                target = nodeData;
                            } else {
                                target = nodeData.parent;
                            }

                            return navigationEditorNodeService.getNavigationNode(target.uid, this.uriContext).then(function(refreshedNode) {
                                lodash.assign(target, refreshedNode);
                                if (nodeData.parent.uid === NAVIGATION_NODE_ROOT_NODE_UID) {
                                    return this.actions.refreshNode(handle);
                                } else {
                                    return this.actions.refreshParentNode(handle);
                                }
                            }.bind(this));
                        }.bind(this));
                    }.bind(this),


                    addTopLevelNode: function() {
                        return this.addNewChild();
                    },

                    addNewChild: function(treeService, handle) {
                        this.actions._expandIfNeeded(handle).then(function() {
                            return navigationNodeEditorModalService.openNodeEditor({
                                parentUid: handle ? handle.$modelValue.uid : this.rootNodeUid
                            }, this.uriContext);
                        }.bind(this));
                    }.bind(this),

                    addNewSibling: function(treeService, handle) {
                        var nodeData = handle.$modelValue;
                        return navigationNodeEditorModalService.openNodeEditor({
                            parentUid: nodeData.parent.uid
                        }, this.uriContext);
                    }.bind(this),

                    getDropdownItems: function() {
                        return dropdownItems;
                    },

                    _findNodeById: function(treeService, nodeUid) {
                        return this.getNodeById(nodeUid);
                    },
                    _expandIfNeeded: function(treeServic, handle) {
                        return handle && handle.collapsed ? this.toggleAndfetch(handle) : $q.when();
                    }

                };

                systemEventService.registerEventHandler(NODE_CREATION_EVENT, _nodeCreationEventHandler);
            };

            var _nodeCreationEventHandler = function(eventId, newNode) {
                var parent = this.actions._findNodeById(newNode.parentUid);
                if (parent && !parent.nodes.find(function(node) {
                        return node.uid === newNode.uid;
                    })) {
                    newNode.parent = parent;
                    parent.nodes = parent.nodes || [];
                    parent.nodes.push(newNode);
                    parent.hasChildren = parent.nodes.length > 0;
                }
                return $q.when();
            }.bind(this);



            this.$onDestroy = function() {
                systemEventService.unRegisterEventHandler(NODE_CREATION_EVENT, _nodeCreationEventHandler);
            };

        }])
        /**
         * @ngdoc directive
         * @name navigationEditorModule.directive:navigationEditor
         * @scope
         * @restrict E
         * @element ANY
         *
         * @description
         * Navigation Editor directive used to display navigation editor tree
         * @param {Object} uriContext the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         * @param {Boolean} readOnly when true, no CRUD facility shows on the editor. OPTIONAL, default false.
         * @param {String} rootNodeUid the uid of the node to be taken as root, OPTIONAL, default "root"
         */
        .directive('navigationEditor', function() {

            return {
                restrict: 'E',
                transclude: false,
                replace: false,
                templateUrl: 'navigationEditorTemplate.html',
                controller: 'navigationEditorController',
                controllerAs: 'nav',
                scope: {},
                bindToController: {
                    uriContext: '<',
                    readOnly: '<?',
                    rootNodeUid: '<?'
                }
            };
        });

    function _isEntry(element) {
        return element.hasOwnProperty('itemId');
    }

    function _recalculatePositionBasedOnNodesOfSameType(source, destinationNodes, eventPosition) {
        var sourceIsEntry = _isEntry(source);
        var offset = 0;
        for (var i = 0; i < eventPosition; i++) {
            if (_isEntry(destinationNodes[i]) !== sourceIsEntry) {
                offset++;
            }
        }
        return offset;
    }

    function _hasNotMoved(source, destinationPosition, destinationParent) {
        return source.position === destinationPosition && source.parentUid === destinationParent.uid;
    }

    function _sameNature(source, target) {
        return (source.itemId && target.itemId) || (!source.itemId && !target.itemId);
    }


})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('navigationEditorNodeServiceModule', ['resourceModule', 'yLoDashModule', 'functionsModule'])
    .service('_nodeAncestryService', ['lodash', function(lodash) {

        this._fetchAncestors = function(sourceArray, uid) {
            var parent = sourceArray.find(function(element) {
                return element.uid === uid;
            });
            if (parent) {
                return [parent].concat(this._fetchAncestors(sourceArray, parent.parentUid));
            } else {
                return [];
            }
        };

        this.buildOrderedListOfAncestors = function(sourceArray, uid) {
            var ancestry = lodash.reverse(this._fetchAncestors(sourceArray, uid));
            var level = -1;
            return ancestry.map(function(node) {
                var nextLevel = ++level;
                return lodash.assign(lodash.cloneDeep(node), {
                    level: nextLevel,
                    formattedLevel: nextLevel === 0 ? "se.cms.navigationcomponent.management.node.level.root" : "se.cms.navigationcomponent.management.node.level.non.root"
                });
            });
        };
    }])
    /**
     * @ngdoc service
     * @name navigationEditorNodeServiceModule.service:navigationEditorNodeService
     * @description
     * This service updates the navigation node by making REST call to the cmswebservices navigations API.
     */
    .service('navigationEditorNodeService', ['$q', '_nodeAncestryService', 'navigationNodeRestService', 'lodash', 'getDataFromResponse', function($q, _nodeAncestryService, navigationNodeRestService, lodash, getDataFromResponse) {

        this.getNavigationNode = function(nodeUid, uriParams) {

            var payload = angular.extend({
                identifier: nodeUid
            }, uriParams);
            return navigationNodeRestService.get(payload);
        };

        /**
         * @ngdoc method
         * @name navigationEditorNodeServiceModule.service:navigationEditorNodeService#updateNavigationNode
         * @methodOf navigationEditorNodeServiceModule.service:navigationEditorNodeService
         *
         * @description
         * Updates a navigation node that corresponds to specific site UID, catalogId and catalogVersion. The request is sent
         * to the cmswebservices navigations API using a REST call.
         *
         * @param {Object} node The navigation node that needs to be updated.
         * @param {Object} uriParams the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        this.updateNavigationNode = function(node, uriParams) {

            var payload = lodash.assign({
                identifier: node.uid
            }, node, uriParams);
            delete payload.parent;
            delete payload.nodes;

            payload.entries = payload.entries.map(function(_entry) {
                var clone = lodash.cloneDeep(_entry);
                delete clone.parent;
                return clone;
            });

            return navigationNodeRestService.update(payload).then(function() {
                node.parent.initiated = false;
                return;
            });
        };

        /**
         * @ngdoc method
         * @name navigationEditorNodeServiceModule.service:navigationEditorNodeService#getNavigationNodeAncestry
         * @methodOf navigationEditorNodeServiceModule.service:navigationEditorNodeService
         *
         * @description
         * Returns the list of nodes belonging to the ancestry of the node identified by its uid. This list includes the queried node as well.
         *
         * @param {Object} node The navigation node that needs to be updated.
         * @param {Object} uriParams the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         * @returns {Array} an array of {@link treeModule.object:Node nodes}
         */
        this.getNavigationNodeAncestry = function(nodeUid, uriParams) {

            var payload = lodash.assign({
                ancestorTrailFrom: nodeUid
            }, uriParams);
            return navigationNodeRestService.get(payload).then(function(response) {
                return _nodeAncestryService.buildOrderedListOfAncestors(getDataFromResponse(response), nodeUid);
            });
        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('navigationEditorServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule'])
    .factory('navigationEditorService', ['NAVIGATION_MANAGEMENT_RESOURCE_URI', 'restServiceFactory', function(NAVIGATION_MANAGEMENT_RESOURCE_URI, restServiceFactory) {
        var navigationRestService = restServiceFactory.get(NAVIGATION_MANAGEMENT_RESOURCE_URI);

        return {

            getNavigationNode: function(nodeUid, uriParams) {
                return navigationRestService.get({
                    identifier: nodeUid,
                    siteUID: uriParams.siteId,
                    catalogId: uriParams.catalogId,
                    catalogVersion: uriParams.catalogVersion
                }).then(function() {});
            },

            updateNavigationNode: function(node, uriParams) {
                var parent = node.parent;
                return navigationRestService.update({
                    identifier: node.uid,
                    siteUID: uriParams.siteId,
                    catalogId: uriParams.catalogId,
                    catalogVersion: uriParams.catalogVersion,
                    parentUid: node.parentUid,
                    uid: node.uid,
                    name: node.name,
                    title: node.title,
                    position: node.position
                }).then(function() {
                    parent.initiated = false;
                    return;
                });
            }

        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('navigationEntryItemServiceModule', ['yLoDashModule', 'restServiceFactoryModule', 'itemTitleAbstractPageHandlerServiceModule', 'itemTitleAbstractCMSComponentHandlerServiceModule', 'itemTitleMediaHandlerServiceModule'])

/**
 * @ngdoc service
 * @name navigationEntryItemServiceModule.service:navigationEntryItemService
 * @description
 * This service is used to retrieve component items by making a REST call to the items API.
 */
.factory('navigationEntryItemService', ['$q', '$injector', 'lodash', 'generateIdentifier', function($q, $injector, lodash, generateIdentifier) {

    var HANDLER_PREFIX = 'itemTitle';
    var HANDLER_SUFFIX = 'HandlerService';

    return {

        /**
         * @ngdoc method
         * @name navigationEntryItemServiceModule.service:navigationEntryItemService#finalizeNavigationEntries
         * @methodOf navigationEntryItemServiceModule.service:navigationEntryItemService
         *
         * @description
         * Assigns a generated id if not set
         * Assigns titles or name to entries based on the titles calculated form their respective entries by means of item super type specific strategies
         * 
         * @param {Object} entries A list of entries associated to a node.
         * @param {Object} uriParams The object that contains site UID, catalogId and catalogVersion.
         * @param {Boolean} setName when true, the calculated title is set in the name property, if false in the title property. Default value is false;
         * 
         */
        finalizeNavigationEntries: function(entries, uriParams, setName) {

            var promises = [];

            entries.forEach(function(entry) {

                var clone = lodash.cloneDeep(entry);
                delete clone.parent;
                entry.id = entry.id || generateIdentifier();

                if (!entry.itemSuperType) {
                    return;
                }
                var itemHandlerServiceStrategy = HANDLER_PREFIX + entry.itemSuperType + HANDLER_SUFFIX;
                if (!$injector.has(itemHandlerServiceStrategy)) {
                    console.error("handler not found for " + itemHandlerServiceStrategy);
                    return;
                }

                promises.push($injector.get(itemHandlerServiceStrategy).getItemTitleById(entry.itemId, uriParams).then(function(itemInfo) {
                    if (setName) {
                        this.name = itemInfo.title;
                    } else {
                        this.title = itemInfo.title;
                    }
                    this.itemType = itemInfo.itemType;
                }.bind(entry)));
            });

            return $q.all(promises);
        }
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("entryDropdownMatcherModule", [])
    .directive('entryDropdownMatcher', function() {
        return {
            restrict: 'E',
            transclude: false,
            replace: false,
            template: '<div class="ySEEntryDropdownMatcher" ng-include="getTemplateUrl()"></div>',
            scope: {
                templateUrl: "<",
                templateData: "<"
            },
            link: function(scope) {

                scope.$watch(function() {
                    return scope.templateData;
                }, function() {
                    scope.option = scope.templateData;
                });
                scope.getTemplateUrl = function() {
                    return scope.templateUrl;
                };
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("entrySearchSelectorModule", ['ui.bootstrap', 'ui.select', 'urlServiceModule', 'entryDropdownMatcherModule', 'eventServiceModule', 'searchAbstractCMSComponentHandlerServiceModule', 'searchAbstractPageHandlerServiceModule', 'searchMediaHandlerServiceModule'])
    .controller("entrySearchSelectorController", ['$injector', '$q', 'systemEventService', 'LINKED_DROPDOWN', 'urlService', 'isBlank', function($injector, $q, systemEventService, LINKED_DROPDOWN, urlService, isBlank) {

        var HANDLER_PREFIX = 'search';
        var HANDLER_Suffix = 'HandlerService';

        /*
         * Event that will be triggered on item type drop down selection change.   
         */
        var selectedItemTypeDropdownEvent = function(key, event) {
            //reset selected values on change

            if (this.reset) {
                var newItemType = this._getItemType(event);
                var forceReset = this.itemType && newItemType !== this.itemType;
                this.reset(forceReset);
            }

            //return if it does not have itemType, otherwise handler will fail
            if (!this.setup(event)) {
                return;
            }

        }.bind(this);

        this._fetchEntity = function(id) {
            return this.handlerService.getItem(id, this.uriContext);
        }.bind(this);

        this._fetchOptions = function(mask) {
            if (this.handlerService) {
                return this.handlerService.getSearchResults(mask, this.uriContext);
            } else {
                return $q.when([]);
            }
        }.bind(this);

        this._fetchPage = function(mask, pageSize, currentPage) {
            if (this.handlerService) {
                return this.handlerService.getPage(mask, pageSize, currentPage, this.uriContext);
            } else {
                return $q.when();
            }
        }.bind(this);

        this.fetchStrategy = {
            fetchEntity: this._fetchEntity
        };

        this.setup = function(event) {
            this.uriContext = urlService.buildUriContext(this.editor.parameters.siteId, this.editor.parameters.catalogId, this.editor.parameters.catalogVersion);
            this.itemType = this._getItemType(event);
            if (this.itemType) {
                // retrieve the search handler for this item type
                var searchHandlerServiceStrategy = HANDLER_PREFIX + this.itemType + HANDLER_Suffix;
                if ($injector.has(searchHandlerServiceStrategy)) {
                    this.handlerService = $injector.get(searchHandlerServiceStrategy);
                } else {
                    var errorMessage = "handler not found for " + searchHandlerServiceStrategy;
                    throw errorMessage;
                }

                this.dropdownProperties = this.handlerService.getSearchDropdownProperties();
                this.fetchStrategy = {
                    fetchEntity: this._fetchEntity
                };

                if (this.dropdownProperties.isPaged) {
                    this.fetchStrategy.fetchPage = this._fetchPage;
                    delete this.fetchStrategy.fetchAll;
                } else {
                    delete this.fetchStrategy.fetchPage;
                    this.fetchStrategy.fetchAll = this._fetchOptions;
                }

                this.initialized = true;
            }
            return !isBlank(this.itemType);
        }.bind(this);

        this._getItemType = function(event) {
            return event && this.model ? this.model[event.qualifier] : this.model.itemSuperType;
        };

        this.$onInit = function() {
            this.unRegisterer = systemEventService.registerEventHandler(this.id + LINKED_DROPDOWN, selectedItemTypeDropdownEvent);
        };

        this.$onChanges = function() {

            this.setup();
        };

        this.$onDestroy = function() {
            this.unRegisterer();
        };

        this.dropdownProperties = {};

    }])
    .component('entrySearchSelector', {
        transclude: false,
        templateUrl: 'entrySearchSelectorDropdownTemplate.html',
        controller: 'entrySearchSelectorController',
        controllerAs: 'ctrl',
        bindings: {
            model: "<",
            qualifier: "<",
            field: "<",
            id: '<',
            editor: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('entrySearchStrategyInterfaceModule', [])
    /**
     * @ngdoc service
     * @name entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface
     * @description
     * Interface describing the contract for entry search in navigation node editor and is part of strategy for the search of EditableDropdown widget which is configured by name.
     * For example when we select the value from the ITEMTYPE dropdown
     * then it tries to find the service handler by name ie. "'search' + itemTypeSelected from Dropdown + 'HandlerService'".
     * This service manage the search for components in the dropdown during the creation of navigation entry.
     */
    .factory('EntrySearchStrategyInterface', function() {

        var EntrySearchStrategyInterface = function() {};

        /**
         * @ngdoc method
         * @name entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface#getSearchDropdownProperties
         * @methodOf entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface
         *
         * @description
         * This method provides search result render properties. It returns a object which has a templateURL required to render search choices and placeholder key for the dropdown.
         *
         */
        EntrySearchStrategyInterface.prototype.getSearchDropdownProperties = function() {
            throw "getSearchDropdownProperties is not implemented";
        };

        /**
         * @ngdoc method
         * @name entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface#getItem
         * @methodOf entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface
         *
         * @description
         * Fetch an item identified by the given id by making a REST call to the appropriate item end point.
         *
         * @param {String} id the item identifier
         * @param {Object} parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        EntrySearchStrategyInterface.prototype.getItem = function() {
            throw "getItem is not implemented";
        };

        /**
         * @ngdoc method
         * @name entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface#getSearchResults
         * @methodOf entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface
         *
         * @description
         * Fetch the search results by making a REST call to the appropriate item end point.
         *
         * @param {String} mask for filtering the search
         * @param {Object} parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        EntrySearchStrategyInterface.prototype.getSearchResults = function() {};

        /**
         * @ngdoc method
         * @name entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface#getPage
         * @methodOf entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface
         *
         * @description
         * Fetch paged search results by making a REST call to the appropriate item end point.
         * Must return a Page of type Page as per SmartEdit documentation
         * @param {String} mask for filtering the search
         * @param {String} pageSize number of items in the page
         * @param {String} currentPage current page number
         * @param {Object} parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations
         */
        EntrySearchStrategyInterface.prototype.getPage = function() {};

        return EntrySearchStrategyInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("searchAbstractCMSComponentHandlerServiceModule", ['functionsModule', 'resourceModule', 'resourceLocationsModule', 'componentServiceModule', 'entrySearchStrategyInterfaceModule'])
    .factory("searchAbstractCMSComponentHandlerService", ['$q', 'extend', 'ITEMS_RESOURCE_URI', 'restServiceFactory', 'EntrySearchStrategyInterface', 'URIBuilder', function($q, extend, ITEMS_RESOURCE_URI, restServiceFactory, EntrySearchStrategyInterface, URIBuilder) {

        var searchAbstractCMSComponentHandlerService = function() {
            this.SEARCH_TEMPLATE = "itemSearchHandlerTemplate.html";
            this.PLACEHOLDER_KEY = 'se.cms.navigationmanagement.navnode.node.entry.dropdown.component.search';
            this.uriParameters = {};
        };
        searchAbstractCMSComponentHandlerService = extend(EntrySearchStrategyInterface, searchAbstractCMSComponentHandlerService);

        searchAbstractCMSComponentHandlerService.prototype._getItem = function(id) {
            var restServiceItemsResource = restServiceFactory.get(new URIBuilder(ITEMS_RESOURCE_URI).replaceParams(this.uriParameters).build());
            return restServiceItemsResource.getById(id).then(function(response) {
                return {
                    name: response.name,
                    id: response.uid,
                    typeCode: response.typeCode
                };
            });
        };

        searchAbstractCMSComponentHandlerService.prototype._getComponents = function() {
            return this._getPage().then(function(response) {
                return response.results;
            });
        };

        searchAbstractCMSComponentHandlerService.prototype._getPage = function(mask, pageSize, currentPage) {
            var restServiceItemsResource = restServiceFactory.get(new URIBuilder(ITEMS_RESOURCE_URI).replaceParams(this.uriParameters).build());
            return restServiceItemsResource.get({
                mask: mask,
                pageSize: pageSize,
                currentPage: currentPage
            }).then(function(response) {
                response.results = response.componentItems.map(function(component) {
                    return {
                        name: component.name,
                        id: component.uid,
                        typeCode: component.typeCode
                    };
                });
                delete response.componentItems;
                return response;
            });
        };

        searchAbstractCMSComponentHandlerService.prototype.getSearchDropdownProperties = function() {
            var properties = {
                templateUrl: this.SEARCH_TEMPLATE,
                placeHolderI18nKey: this.PLACEHOLDER_KEY,
                isPaged: true
            };
            return properties;
        };

        searchAbstractCMSComponentHandlerService.prototype.getSearchResults = function(mask, parameters) {
            this.uriParameters = parameters;
            return this._getComponents();
        };

        searchAbstractCMSComponentHandlerService.prototype.getPage = function(mask, pageSize, currentPage, parameters) {
            this.uriParameters = parameters;
            return this._getPage(mask, pageSize, currentPage);
        };

        searchAbstractCMSComponentHandlerService.prototype.getItem = function(id, parameters) {
            this.uriParameters = parameters;
            return this._getItem(id);
        };


        return new searchAbstractCMSComponentHandlerService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("searchAbstractPageHandlerServiceModule", ['pageListServiceModule', 'entrySearchStrategyInterfaceModule'])
    .factory("searchAbstractPageHandlerService", ['$q', 'extend', 'EntrySearchStrategyInterface', 'pageListService', function($q, extend, EntrySearchStrategyInterface, pageListService) {

        var searchAbstractPageHandlerService = function() {
            this.SEARCH_TEMPLATE = "itemSearchHandlerTemplate.html";
            this.PLACEHOLDER_KEY = 'se.cms.navigationmanagement.navnode.node.entry.dropdown.page.search';
            this.uriParameters = {};
        };
        searchAbstractPageHandlerService = extend(EntrySearchStrategyInterface, searchAbstractPageHandlerService);

        searchAbstractPageHandlerService.prototype._getPageObjects = function() {
            return pageListService.getPageListForCatalog(this.uriParameters).then(function(pages) {
                return pages.map(function(page) {
                    return {
                        name: page.name,
                        id: page.uid,
                        typeCode: page.typeCode
                    };
                });
            });
        };

        searchAbstractPageHandlerService.prototype.getSearchDropdownProperties = function() {
            var properties = {
                templateUrl: this.SEARCH_TEMPLATE,
                placeHolderI18nKey: this.PLACEHOLDER_KEY,
                isPaged: false
            };
            return properties;
        };

        searchAbstractPageHandlerService.prototype.getSearchResults = function(mask, parameters) {
            this.uriParameters = parameters;
            return this._getPageObjects();
        };

        searchAbstractPageHandlerService.prototype.getItem = function(id, parameters) {
            this.uriParameters = parameters;
            return this._getPageObjects().then(function(items) {
                return items.find(function(item) {
                    return item.id === id;
                });
            });
        };

        return new searchAbstractPageHandlerService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("searchMediaHandlerServiceModule", [
    'entrySearchStrategyInterfaceModule',
    'functionsModule',
    'restServiceFactoryModule',
    'resourceLocationsModule'
])

.factory("searchMediaHandlerService", ['extend', 'EntrySearchStrategyInterface', 'restServiceFactory', 'MEDIA_PATH', 'MEDIA_RESOURCE_URI', 'URIBuilder', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'isBlank', function(extend, EntrySearchStrategyInterface, restServiceFactory,
    MEDIA_PATH, MEDIA_RESOURCE_URI, URIBuilder, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, isBlank) {

    var SearchMediaHandlerService = function() {
        this.SEARCH_TEMPLATE = 'mediaSearchHandlerTemplate.html';
        this.PLACEHOLDER_KEY = 'se.cms.navigationmanagement.navnode.node.entry.dropdown.media.search';
        this.uriParameters = {};
    };
    SearchMediaHandlerService = extend(EntrySearchStrategyInterface, SearchMediaHandlerService);

    SearchMediaHandlerService.prototype._getMedias = function(mask, pageSize, currentPage) {

        var payload = {
            catalogId: this.uriParameters[CONTEXT_CATALOG] || CONTEXT_CATALOG,
            catalogVersion: this.uriParameters[CONTEXT_CATALOG_VERSION] || CONTEXT_CATALOG_VERSION
        };
        if (!isBlank(mask)) {
            payload.code = mask;
        }

        var subParams = Object.keys(payload).reduce(function(accumulator, next) {
            accumulator += "," + next + ":" + payload[next];
            return accumulator;
        }, "").substring(1);

        var params = {
            namedQuery: "namedQueryMediaSearchByCodeCatalogVersion",
            params: subParams,
            pageSize: pageSize,
            currentPage: currentPage
        };
        return restServiceFactory.get(MEDIA_PATH).get(params).then(function(response) {
            response.results = response.media.map(function(media) {
                return {
                    id: media.code,
                    code: media.code,
                    description: media.description,
                    altText: media.altText,
                    url: media.url,
                    downloadUrl: media.downloadUrl
                };
            });
            delete response.media;
            return response;
        });
    };

    SearchMediaHandlerService.prototype.getSearchDropdownProperties = function() {
        var properties = {
            templateUrl: this.SEARCH_TEMPLATE,
            placeHolderI18nKey: this.PLACEHOLDER_KEY,
            isPaged: true
        };
        return properties;
    };

    SearchMediaHandlerService.prototype.getSearchResults = function(mask, parameters) {
        this.uriParameters = parameters;
        return this._getMedias(mask).then(function(items) {
            return items.results;
        });
    };

    SearchMediaHandlerService.prototype.getPage = function(mask, pageSize, currentPage, parameters) {
        this.uriParameters = parameters;
        return this._getMedias(mask, pageSize, currentPage);
    };

    SearchMediaHandlerService.prototype.getItem = function(identifier, parameters) {
        this.uriParameters = parameters;
        //identifier is added to URI and not getByid argument because it contains slashes
        var mediaResourceURI = new URIBuilder(MEDIA_RESOURCE_URI).replaceParams(this.uriParameters).build();
        return restServiceFactory.get(mediaResourceURI + "/" + identifier).get().then(function(media) {
            return {
                id: media.code,
                code: media.code,
                description: media.description,
                altText: media.altText,
                url: media.url,
                downloadUrl: media.downloadUrl
            };
        });
    };


    return new SearchMediaHandlerService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name navigationNodeEditorModule
 * @description
 *
 * The navigation node editor module provides a directive and controller to manage the navigation node selected on the parent's scope.     
 */
angular.module("navigationNodeEditorModule", ['navigationNodeEditorCreateEntryModule', 'navigationNodeEditorEntryListModule', 'navigationNodeEditorAttributesModule', 'seBreadcrumbModule'])

/**
 * @ngdoc controller
 * @name navigationNodeEditorModule.controller:navigationNodeEditorController
 *
 * @description
 * The navigation node editor controller is responsible for initializing the shared data that will be managed by other 
 * nested directives. 
 */
.controller("navigationNodeEditorController", function() {

    this.$onInit = function() {
        this.navigationNodeEntryData = {
            navigationNodeEntry: null,
            prepareEntryNodeEditor: function() {}
        };
    };
})

/**
 * @ngdoc directive
 * @name navigationNodeEditorModule.directive:navigationNodeEditor
 *
 * @description
 * The navigation node editor directive is used to edit the navigation node, and displays essentially the entry list directive, 
 * the entry creation directive and the attribute node directive.
 *    
 * The directive expects that the parent passes a node object that reflects the information about the navigation node being edited. 
 */
.directive('navigationNodeEditor', function() {
    return {
        restrict: 'E',
        transclude: false,
        replace: false,
        templateUrl: 'navigationNodeEditorTemplate.html',
        controller: 'navigationNodeEditorController',
        controllerAs: 'ctrl',
        scope: {},
        bindToController: {
            nodeUid: '<',
            parentUid: '<?',
            entryIndex: '<',
            uriContext: '<',
            reset: '=',
            submit: '=',
            isDirty: '='
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("navigationNodeEditorAttributesModule", ['functionsModule', 'resourceLocationsModule', 'eventServiceModule', 'resourceModule', 'yLoDashModule'])
    .constant('NODE_CREATION_EVENT', 'nodeCreationEvent')
    .controller("navigationNodeEditorAttributesController", ['lodash', 'NAVIGATION_MANAGEMENT_RESOURCE_URI', 'URIBuilder', 'systemEventService', 'navigationNodeRestService', 'NODE_CREATION_EVENT', function(lodash, NAVIGATION_MANAGEMENT_RESOURCE_URI, URIBuilder, systemEventService, navigationNodeRestService, NODE_CREATION_EVENT) {

        var MODIFY_ENTRY_SET = 'modifyEntrySet';

        this.isContentLoaded = false;

        this.tabId = 'ge-nav-node-editor';

        this.typeCode = "CMSNavigationNode";

        this.tabStructure = [{
            cmsStructureType: "ShortString",
            qualifier: "name",
            i18nKey: 'se.cms.navigationmanagement.navnode.node.name',
            required: true
        }, {
            cmsStructureType: "ShortString",
            qualifier: "title",
            i18nKey: 'se.cms.navigationmanagement.navnode.node.title',
            localized: true
        }];

        var _updateEntrySet = function(key, entries) {
            var clone = lodash.cloneDeep(entries);
            var updatedClone = clone.map(function(entry) {
                delete entry.id;
                return entry;
            });
            this.content.entries = updatedClone;
        }.bind(this);


        this.$onInit = function() {

            this.contentApi = new URIBuilder(NAVIGATION_MANAGEMENT_RESOURCE_URI).replaceParams(this.uriContext).build();

            if (this.nodeUid) {
                var payload = angular.extend({
                    identifier: this.nodeUid
                }, this.uriContext);

                navigationNodeRestService.get(payload).then(function(response) {
                    this.content = response;
                    this.isContentLoaded = true;
                }.bind(this));
            } else if (this.parentUid) {
                this.content = {
                    parentUid: this.parentUid
                };
                this.isContentLoaded = true;
            } else {
                throw "navigationNodeEditorAttributes directive was provided with neither nodeUid nore parentUid";
            }

            // event listener 
            systemEventService.registerEventHandler(MODIFY_ENTRY_SET, _updateEntrySet);
        };

        this.updateCallback = function(originalData, newData) {
            if (!this.nodeUid) {
                systemEventService.sendAsynchEvent(NODE_CREATION_EVENT, newData);
            }
        }.bind(this);


        // destroy listener on destroy controller scope
        this.$onDestroy = function() {
            systemEventService.unRegisterEventHandler(MODIFY_ENTRY_SET, _updateEntrySet);
        };

    }])
    .directive('navigationNodeEditorAttributes', function() {

        return {
            restrict: 'E',
            transclude: false,
            replace: false,
            templateUrl: 'navigationNodeEditorAttributesTemplate.html',
            controller: 'navigationNodeEditorAttributesController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                nodeUid: '=?',
                parentUid: '<?',
                uriContext: '=',
                reset: '=',
                submit: '=',
                isDirty: '=',
                navigationNodeEntryData: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("navigationNodeEditorCreateEntryModule", [
        'resourceLocationsModule',
        'eventServiceModule',
        'entryDropdownMatcherModule',
        'yLoDashModule',
        'resourceModule',
        'genericEditorModule',
        'dropdownPopulatorInterfaceModule',
        'navigationNodeEntryTypesServiceModule'
    ])
    .constant('NAV_ENTRY_EDITOR_ID', 'se-nav-entry-editor')
    .service("navigationEntryStructureService", ['$q', '$translate', function($q, $translate) {

        this.getGenericEditorTabStructure = function() {
            var tabStructure = [{
                i18nKey: 'se.cms.navigationmanagement.navnode.node.create.entry',
                cmsStructureType: "EditableDropdown",
                qualifier: "itemSuperType",
                placeholder: $translate.instant('se.cms.navigationmanagement.navnode.node.entry.dropdown.title'),
                required: true
            }, {
                cmsStructureType: "EntrySearchSelector",
                qualifier: "itemId",
                editable: false,
                required: true
            }];
            return $q.when(tabStructure);
        };
    }])
    /**
     * @ngdoc service
     * @name navigationNodeEditorCreateEntryModule.service:cmsNavigationEntryDropdownPopulator
     * @description implementation of DropdownPopulatorInterface defined in smartedit
     * for "EditableDropdown" cmsStructureType containing options attribute.
     */
    .factory('cmsNavigationEntryDropdownPopulator', ['DropdownPopulatorInterface', '$q', 'extend', 'navigationNodeEntryTypesService', function(DropdownPopulatorInterface, $q, extend, navigationNodeEntryTypesService) {

        var cmsNavigationEntryDropdownPopulator = function() {};

        cmsNavigationEntryDropdownPopulator = extend(DropdownPopulatorInterface, cmsNavigationEntryDropdownPopulator);

        var _getDropdownOptions = function() {
            return navigationNodeEntryTypesService
                .getNavigationNodeEntryTypes()
                .then(function(supportedEntryTypes) {
                    var options = [];
                    if (supportedEntryTypes) {
                        supportedEntryTypes.forEach(function(entryType) {
                            options.push({
                                id: entryType.itemType,
                                label: 'se.cms.' + entryType.itemType.toLowerCase()
                            });
                        });
                    }
                    return options;
                });
        };


        cmsNavigationEntryDropdownPopulator.prototype.populate = function() {
            _getDropdownOptions();
            return $q.when(_getDropdownOptions());
        };

        return new cmsNavigationEntryDropdownPopulator();
    }])
    .controller("navigationNodeEditorCreateEntryController", ['$timeout', 'NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'GENERIC_EDITOR_LOADED_EVENT', 'NAV_ENTRY_EDITOR_ID', 'systemEventService', 'LINKED_DROPDOWN', 'lodash', 'navigationEntryStructureService', function($timeout, NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, GENERIC_EDITOR_LOADED_EVENT, NAV_ENTRY_EDITOR_ID, systemEventService, LINKED_DROPDOWN, lodash, navigationEntryStructureService) {

        var SAVE_ENTRY_EVENT = 'saveEntry';
        var DELETE_ENTRY_EVENT = 'deleteEntry';

        this.entryTypeCode = "cmsNavigationEntry";

        var editEntry = function(entry) {
            this.entry = entry;
            this.displayEditor = true;
        }.bind(this);

        /*
         * function to make fields editable in structure
         */
        var _enableTabStructureField = function(status) {
            this.tabStructure.forEach(function(structure, index) {
                if (lodash.isMatch(structure, {
                        cmsStructureType: 'EntrySearchSelector'
                    })) {
                    this.tabStructure[index].editable = status;
                }
            }.bind(this));
        }.bind(this);

        /*
         * Hide itemId and search selector widget
         */
        var selectedItemTypeDropdownEvent = function(eventId, data) {
            var entity = {};
            entity.id = this.entry ? this.entry.id : undefined;
            entity[data.qualifier] = data.optionObject ? data.optionObject.id : undefined;

            this.navigationNodeEntryData.prepareEntryNodeEditor(entity);
        }.bind(this);

        this.addNewEntry = function() {
            this.entry = undefined;
            this.displayEditor = true;
        };

        this.saveEntry = function() {
            return this.submit().then(
                function(data) {
                    if (data.itemId) {
                        systemEventService.sendAsynchEvent(SAVE_ENTRY_EVENT, data);
                        this.cancelEntry();
                    } else {
                        systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                            messages: [{
                                "message": "se.cms.navigationmanagement.node.addentry.no.itemid.message",
                                "subject": "itemId",
                                "type": "ValidationError"
                            }]
                        });
                    }
                }.bind(this));
        };

        this.isNewEntry = function() {
            return !this.entry || (this.entry && !this.entry.id);
        };

        this.isDisabled = function() {
            return !this.isDirty() || !this.isValid();
        };

        this.cancelEntry = function() {
            _enableTabStructureField(false);
            this.entry = undefined;
            this.displayEditor = false;
        };

        var _deleteEntryEvent = function(eventId, handler) {
            if (this.entry && handler.id === this.entry.id) {
                this.cancelEntry();
            } else {
                return;
            }
        }.bind(this);



        this.$onInit = function() {

            this.displayEditor = false;
            this.tabId = NAV_ENTRY_EDITOR_ID;

            // event listener to hide itemId and search selector widget
            this.unregisterSelectHandler = systemEventService.registerEventHandler(this.tabId + LINKED_DROPDOWN, selectedItemTypeDropdownEvent);
            this.unregisterDeleteHandler = systemEventService.registerEventHandler(DELETE_ENTRY_EVENT, _deleteEntryEvent);

            /*
             * Reset generic editor before editing a node entry.
             */
            this.navigationNodeEntryData.prepareEntryNodeEditor = function(entry) {
                if (entry && (!this.entry || entry.id !== this.entry.id)) {
                    var clone = lodash.cloneDeep(entry);
                    delete clone.parent;

                    // If reset is available, reset the editor and start editing until
                    // it's done. Else, start editing right away.
                    if (this.entry && this.reset) {
                        this.reset().then(function() {
                            this.cancelEntry();
                            $timeout(function() {
                                editEntry(clone);
                            });
                        }.bind(this));
                    } else {
                        editEntry(clone);
                        _enableTabStructureField(true);
                    }
                }
            }.bind(this);


            // get the generic tab structure
            navigationEntryStructureService.getGenericEditorTabStructure()
                .then(function(genericEditorTabStructure) {
                    this.tabStructure = genericEditorTabStructure;
                    if (this.entry) {
                        this.navigationNodeEntryData.prepareEntryNodeEditor(this.entry);
                    }
                }.bind(this));


        };
        // destroy listener on destroy controller scope
        this.$onDestroy = function() {
            this.unregisterSelectHandler();
            this.unregisterDeleteHandler();
        };


    }])
    .directive('navigationNodeEditorCreateEntry', function() {

        return {
            restrict: 'E',
            transclude: false,
            replace: false,
            templateUrl: 'navigationNodeEditorCreateEntryTemplate.html',
            controller: 'navigationNodeEditorCreateEntryController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                entry: '<?',
                uriContext: '<',
                navigationNodeEntryData: '<',
                isDirty: '&'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name navigationNodeEditorEntryListModule
 * @description
 *
 * The navigation node editor entry list module provides a directive and controller to manage the navigation node entry list.     
 */
angular.module("navigationNodeEditorEntryListModule", ['genericEditorModule', 'seValidationErrorParserModule', 'navigationEntryItemServiceModule', 'eventServiceModule', 'resourceModule', 'yLoDashModule'])

/**
 * @ngdoc controller
 * @name navigationNodeEditorEntryListModule.controller:navigationNodeEditorEntryListController
 *
 * @description
 * The navigation node editor entry list controller is responsible for loading the entry list for a specific node, 
 * updating an entry's position, removing the entry as an entry on the node and enabling the modification of an entry. 
 *
 */
.controller("navigationNodeEditorEntryListController", ['$q', 'lodash', 'navigationEntryItemService', 'systemEventService', 'seValidationErrorParser', 'navigationNodeRestService', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'GENERIC_EDITOR_LOADED_EVENT', 'NAV_ENTRY_EDITOR_ID', function($q, lodash, navigationEntryItemService, systemEventService, seValidationErrorParser, navigationNodeRestService, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, GENERIC_EDITOR_LOADED_EVENT, NAV_ENTRY_EDITOR_ID) {

    this.entries = [];

    this.isInError = function(entry) {
        return this.errorsMap && Object.keys(this.errorsMap).indexOf(entry.id) > -1;
    };

    this.onSelect = function(entry) {
        this.currentEntry = entry;
        this.navigationNodeEntryData.prepareEntryNodeEditor(entry);
    }.bind(this);

    var SAVE_ENTRY_EVENT = 'saveEntry';
    var DELETE_ENTRY_EVENT = 'deleteEntry';
    var MODIFY_ENTRY_SET = 'modifyEntrySet';

    this.dropdownItems = [{
        key: 'se.cms.navigationmanagement.navnode.edit',
        callback: this.onSelect
    }, {
        key: 'se.cms.navigationmanagement.navnode.move.up',
        condition: function(entry) {
            return this.entries.indexOf(entry) > 0;
        }.bind(this),
        callback: function(entry) {
            var pos = this.entries.indexOf(entry);
            var upperEntry = this.entries[pos - 1];
            this.entries.splice(pos - 1, 2, entry, upperEntry);
            systemEventService.sendAsynchEvent(MODIFY_ENTRY_SET, this.entries);
        }.bind(this)
    }, {
        key: 'se.cms.navigationmanagement.navnode.move.down',
        condition: function(entry) {
            return this.entries.indexOf(entry) < this.entries.length - 1;
        }.bind(this),
        callback: function(entry) {
            var pos = this.entries.indexOf(entry);
            var lowerEntry = this.entries[pos + 1];
            this.entries.splice(pos, 2, lowerEntry, entry);
            systemEventService.sendAsynchEvent(MODIFY_ENTRY_SET, this.entries);
        }.bind(this)
    }, {
        key: 'se.cms.navigationmanagement.navnode.removenode',
        callback: function(entry) {

            var updatedEntries = this.entries.filter(function(curEntry) {
                return curEntry.id !== entry.id;
            });

            this.entries = updatedEntries;
            systemEventService.sendAsynchEvent(MODIFY_ENTRY_SET, this.entries);
            systemEventService.sendAsynchEvent(DELETE_ENTRY_EVENT, entry);
        }.bind(this)
    }];

    var _pushEntryToList = function(key, entry) {
        navigationEntryItemService.finalizeNavigationEntries([entry], this.uriContext).then(function() {
            var clone = lodash.cloneDeep(entry);

            this.entries = this.entries.map(function(item) {
                if (item.id === clone.id) {
                    item = clone;
                }
                return item;
            });
            if (!this.entries.some(function(item) {
                    return item.id === clone.id;
                })) {
                this.entries.push(clone);
            }

            systemEventService.sendAsynchEvent(MODIFY_ENTRY_SET, this.entries);
        }.bind(this));

    }.bind(this);

    var saveNodeErrorsEvent = function(key, validationData) {
        var validationErrors = lodash.cloneDeep(validationData);
        if (!validationData.messages.processed) {
            this.errorsMap = validationErrors.messages.filter(function(validationError) {
                return validationError.subject === "entries";
            }).map(function(validationError) {
                //validationError.message contains the "position" of the entry pertaining to the faulty subject
                var error = seValidationErrorParser.parse(validationError.message);
                //error contains: subject, message field and position of the entry property
                validationError.position = error.position;
                validationError.subject = error.field;
                return validationError;
            }).reduce(function(holder, next) {

                var entryUid = this.entries[next.position].id;

                holder[entryUid] = holder[entryUid] || [];
                holder[entryUid].push(next);
                delete next.position;
                return holder;
            }.bind(this), {});

            if (this.errorsMap && this.currentEntry && this.errorsMap[this.currentEntry.id]) {
                this.errorsMap[this.currentEntry.id].processed = true;
                systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                    messages: this.errorsMap[this.currentEntry.id]
                });
            }
        }
        return $q.when();
    }.bind(this);

    var onEditorLoadEvent = function(key, tabId) {
        if (this.currentEntry && this.errorsMap && this.errorsMap[this.currentEntry.id] && tabId === NAV_ENTRY_EDITOR_ID) {
            this.errorsMap[this.currentEntry.id].processed = true;
            systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                messages: this.errorsMap[this.currentEntry.id]
            });
        }
        return $q.when();
    }.bind(this);

    this.$onInit = function() {

        if (this.nodeUid) {
            var payload = angular.extend({
                identifier: this.nodeUid
            }, this.uriContext);

            navigationNodeRestService.get(payload).then(function(response) {
                var entries = response.entries;

                navigationEntryItemService.finalizeNavigationEntries(entries, this.uriContext).then(function() {

                    this.entries = response.entries;
                    if (this.entryIndex !== undefined) {
                        this.entry = this.entries[this.entryIndex];
                        this.onSelect(this.entry);
                    }
                }.bind(this));
            }.bind(this));
        }

        // event listener 
        this.unregisterEntryListListener = systemEventService.registerEventHandler(SAVE_ENTRY_EVENT, _pushEntryToList);
        /* this list directive listens for all errors coming from the main node submission : it may contain errors for multiple entries and multiples fields within it
         * it generates a map of entry index / entry errors
         */
        this.unregisterErrorListener = systemEventService.registerEventHandler(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, saveNodeErrorsEvent);
        this.unregisterEditorLoadListener = systemEventService.registerEventHandler(GENERIC_EDITOR_LOADED_EVENT, onEditorLoadEvent);


    };

    // destroy listener on destroy controller scope
    this.$onDestroy = function() {
        this.unregisterEntryListListener();
        this.unregisterErrorListener();
        this.unregisterEditorLoadListener();
    };

}])

/**
 * @ngdoc directive
 * @name navigationNodeEditorEntryListModule.directive:navigationNodeEditorEntryList
 *
 * @description
 * The navigation node editor entry list directive is used inside the navigation node editor directive, and displays 
 * a list of navigation node entries and actions to edit, move up, move down and delete an entry.
 *
 * The directive expects that the parent, the navigation node editor, passes a navigationNodeEntryData object and a 
 * node object that reflects the information about the navigation node being edited. Both should be on the parent's scope. 
 */
.component('navigationNodeEditorEntryList', {
    transclude: false,
    templateUrl: 'navigationNodeEditorEntryListTemplate.html',
    controller: 'navigationNodeEditorEntryListController',
    controllerAs: 'ctrl',
    bindings: {
        entryIndex: '<?',
        nodeUid: '<?',
        uriContext: '<',
        navigationNodeEntryData: '<'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name navigationNodeEditorModalServiceModule
 * @description
 * # The navigationNodeEditorModalServiceModule
 *
 * The navigation node editor modal service module provides a service that allows opening an editor modal for a given navigation node or an entry. The editor modal is populated with a save and cancel button, and is loaded with the 
 * editorTabset of cmssmarteditContainer as its content, providing a way to edit
 * various fields of the given navigation node.
 */
angular.module('navigationNodeEditorModalServiceModule', ['modalServiceModule', 'confirmationModalServiceModule'])
    /**
     * @ngdoc service
     * @name navigationNodeEditorModalServiceModule.service:navigationNodeEditorModalService
     *
     * @description
     * Convenience service to open an editor modal window for a given navigation node's data.
     *
     */
    .factory('navigationNodeEditorModalService', ['$q', '$log', 'modalService', 'confirmationModalService', 'MODAL_BUTTON_STYLES', 'MODAL_BUTTON_ACTIONS', function($q, $log, modalService, confirmationModalService, MODAL_BUTTON_STYLES, MODAL_BUTTON_ACTIONS) {

        function NavigationNodeEditorModalService() {}


        /**
         * @ngdoc method
         * @name navigationNodeEditorModalServiceModule.service:navigationNodeEditorModalService#open
         * @methodOf navigationNodeEditorModalServiceModule.service:navigationNodeEditorModalService
         *
         * @description
         * Uses the {@link genericEditorModalService.open genericEditorModalService} to open an editor modal.
         *
         * The editor modal is initialized with a title in the format '<TypeName> Editor', ie: 'Paragraph Editor'. The
         * editor modal is also wired with a save and cancel button.
         *
         * The content of the editor modal is the {@link editorTabsetModule.directive:editorTabset editorTabset}.
         *
         * @param {Object} data The data associated to a navigation node as defined in the platform.
         * @param {Object}  parameters the {@link resourceLocationsModule.object:UriContext UriContext} necessary to perform operations.
         *
         * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
         */
        NavigationNodeEditorModalService.prototype.openNodeEditor = function(target, uriContext) {
            return modalService.open({
                title: 'se.cms.navigationmanagement.node.edit.title',
                templateUrl: 'navigationNodeEditorModalTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    this.target = target;
                    this.uriContext = uriContext;

                    this.onCancel = function() {
                        if (this.isDirty()) {
                            return confirmationModalService.confirm({
                                description: 'editor.cancel.confirm'
                            }).then(function() {
                                return $q.when({});
                            }, function() {
                                return $q.reject();
                            });
                        } else {
                            return $q.when({});
                        }
                        return $q.when({});
                    };

                    this.init = function() {
                        modalManager.setDismissCallback(this.onCancel.bind(this));

                        modalManager.setButtonHandler(function(buttonId) {
                            switch (buttonId) {
                                case 'save':
                                    return this.submit().then(function() {
                                        modalManager.close();
                                    });
                                case 'cancel':
                                    return this.onCancel();
                                default:
                                    $log.error('A button callback has not been registered for button with id', buttonId);
                                    break;
                            }
                        }.bind(this));

                        $scope.$watch(function() {
                            var isDirty = typeof this.isDirty === 'function' && this.isDirty();
                            return isDirty;
                        }.bind(this), function(isDirty) {
                            if (typeof isDirty === 'boolean') {
                                if (isDirty) {
                                    modalManager.enableButton('save');
                                } else {
                                    modalManager.disableButton('save');
                                }
                            }
                        }.bind(this));

                    };
                }],
                buttons: [{
                    id: 'cancel',
                    label: 'se.cms.navigationmanagement.node.edit.cancel',
                    style: MODAL_BUTTON_STYLES.SECONDARY,
                    action: MODAL_BUTTON_ACTIONS.DISMISS
                }, {
                    id: 'save',
                    label: 'se.cms.navigationmanagement.node.edit.save',
                    action: MODAL_BUTTON_ACTIONS.NONE,
                    disabled: true
                }]
            });
        };

        return new NavigationNodeEditorModalService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('navigationNodeEntryTypesServiceModule', ['resourceModule'])

/**
 * @ngdoc service
 * @name navigationNodeEntryTypesServiceModule.service:navigationNodeEntryTypesService
 * @description
 * This service manages navigation node entry types data by making REST call to the cmswebservices navigationentrytypes API.
 */
.service('navigationNodeEntryTypesService', ['navigationEntryTypesRestService', function(navigationEntryTypesRestService) {

    /**
     * @ngdoc method
     * @name navigationNodeEntryTypesServiceModule.service:navigationNodeEntryTypesService#getNavigationNodeEntryTypes
     * @methodOf navigationNodeEntryTypesServiceModule.service:navigationNodeEntryTypesService
     *
     * @description
     * Returns the navigation node entry types supported by cmswebservices navigationentrytypes API.
     *
     * @return {Array} all navigation entry types supported.
     */
    this.getNavigationNodeEntryTypes = function() {
        return navigationEntryTypesRestService.get().then(function(response) {
            return response.navigationEntryTypes;
        });
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('restrictionsMenuModule', ['assetsServiceModule', 'restrictionsTableModule', 'restrictionsPageInfoModule', 'eventServiceModule', 'iframeClickDetectionServiceModule', 'pageRestrictionsModule', 'displayConditionsFacadeModule'])
    /**
     * @ngdoc directive
     * @name restrictionsMenuModule.directive:restrictionsMenuToolbarItem
     * @element restrictions-menu-toolbar-item
     * @restrict E
     *
     * @description
     * Component responsible for displaying the restriction menu.
     */
    .component('restrictionsMenuToolbarItem', {
        templateUrl: 'pageRestrictionsMenuToolbarItemTemplate.html',
        controller: ['assetsService', 'componentHandlerService', 'pageListService', 'pageRestrictionsFacade', 'displayConditionsFacade',
            function(assetsService, componentHandlerService, pageListService, pageRestrictionsFacade, displayConditionsFacade) {
                this.$onInit = function() {
                    var pageUID = componentHandlerService.getPageUID();

                    pageRestrictionsFacade.getRestrictionsByPageUID(pageUID).then(function(restrictions) {
                        this.restrictions = restrictions;
                    }.bind(this));

                    pageListService.getPageById(pageUID).then(function(page) {

                        this.pageId = page && page.uid;
                        this.pageNameLabelI18nKey = "se.cms.label.page.name";
                        this.pageName = page && page.name;

                        displayConditionsFacade.isPagePrimary(this.pageId).then(function(isPrimary) {
                            this.pageIsPrimary = isPrimary;
                            if (!this.pageIsPrimary) {
                                displayConditionsFacade.getPrimaryPageForVariationPage(this.pageId).then(function(primaryPageData) {
                                    this.associatedPrimaryPageName = primaryPageData.name;
                                }.bind(this));
                            }
                        }.bind(this));

                        this.restrictionCriteria = pageRestrictionsFacade.getRestrictionCriteriaOptionFromPage(page);

                    }.bind(this));
                };
            }
        ]
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('restrictionsPageInfoModule', ['pageListServiceModule'])

.controller('restrictionsPageInfoController', function() {

    this.associatedPrimaryPageLabelI18nKey = "se.cms.label.page.primary";
    this.displayConditionsI18nKey = {
        primary: {
            description: 'page.displaycondition.primary.description',
            value: 'page.displaycondition.primary'
        },
        variation: {
            description: 'page.displaycondition.variation.description',
            value: 'page.displaycondition.variation'
        }
    };
    this.pageDisplayConditionsLabelI18nKey = "se.cms.label.page.display.conditions";
    this.pageNameLabelI18nKey = "se.cms.label.page.name";


    this.getDisplayConditionsValue = function() {
        return this.pageIsPrimary ? "primary" : "variation";
    };

})

.directive('restrictionsPageInfo', function() {
    return {
        templateUrl: 'pageRestrictionsPageInfoTemplate.html',
        restrict: 'E',
        controller: 'restrictionsPageInfoController',
        controllerAs: 'ctrl',
        scope: {},
        bindToController: {
            pageName: '=',
            pageType: '=',
            pageIsPrimary: '=',
            associatedPrimaryPageName: '='
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('restrictionsPageListIconModule', ['translationServiceModule'])
    .controller('restrictionsPageListIconController', function() {
        this.tooltipI18nKey = 'se.cms.icon.tooltip.visibility';

        this.getIconUrl = function() {
            return this.numberOfRestrictions > 0 ? '/cmssmartedit/images/icon_restriction_small_blue.png' :
                '/cmssmartedit/images/icon_restriction_small_grey.png';
        };

        this.getInterpolationParameters = function() {
            return {
                numberOfRestrictions: this.numberOfRestrictions
            };
        };
    })
    .directive('restrictionsPageListIcon', function() {
        return {
            templateUrl: 'restrictionsPageListIconTemplate.html',
            restrict: 'E',
            controller: 'restrictionsPageListIconController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                numberOfRestrictions: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name newPageDisplayConditionModule
 * @description
 * #newPageDisplayConditionModule
 *
 * The newPageDisplayConditionModule module contains the
 * {@link newPageDisplayConditionModule.directive:newPageDisplayCondition newPageDisplayCondition} component
 *
 */
angular.module('newPageDisplayConditionModule', ['catalogServiceModule', 'pageServiceModule', 'pageDisplayConditionsServiceModule'])

/**
 * @ngdoc object
 * @name newPageDisplayConditionModule.object:newPageDisplayConditionResult
 *
 * @description
 * The (optional) output of the
 * {@link newPageDisplayConditionModule.directive:newPageDisplayCondition newPageDisplayCondition} component
 * ```
 * {
    isPrimary: {Boolean} True if the chosen new page display condition is Primary
    primaryPage: {Object} [Optional] If isPrimary is false (meaning this is a variant page),
            the value is a page object, representing the primary page that this
            new page will be a variant of.
 * }
 * ```
 */


.controller('newPageDisplayConditionController', ['$scope', 'catalogService', 'pageService', 'pageDisplayConditionsService', 'lodash', function($scope, catalogService, pageService, pageDisplayConditionsService, lodash) {

    var cache = {};

    this.$onInit = function() {

        this.previousPageTypeCode = null;
        this.previousTargetCatalogVersion = null;
        this.conditions = null;
        this.conditionSelected = null;
        this.primarySelected = null;
        this.ready = false;
        this.resultFn = this.resultFn || function() {};
        this.initialConditionSelected = this.initialConditionSelected || "page.displaycondition.primary";

        this.showPrimarySelector = function() {
            return !(this.conditionSelected && this.conditionSelected.isPrimary === true);
        };

        this.dataChanged = function() {
            this.resultFn(this._getResults());
        };
    };

    this.$onChanges = function $onChanges(changes) {
        if ((changes.pageTypeCode && this.pageTypeCode !== this.previousPageTypeCode) ||
            (changes.targetCatalogVersion && this.targetCatalogVersion !== this.previousTargetCatalogVersion)) {

            this.previousPageTypeCode = this.pageTypeCode;
            this.previousTargetCatalogVersion = this.targetCatalogVersion;

            if (!this.targetCatalogVersion || this._isUriContextEqualToCatalogVersion(this.uriContext, this.targetCatalogVersion)) {
                this._getPrimaryPages();
            } else {
                this._getOnlyPrimaryDisplayCondition();
            }
        }
    };

    this._getResults = function _getResults() {
        var result = {
            isPrimary: this.conditionSelected.isPrimary
        };
        if (!this.conditionSelected.isPrimary) {
            result.primaryPage = this.primarySelected;
        }
        return result;
    };

    this._getPrimaryPages = function _getPrimaryPages() {

        if (this.pageTypeCode) {
            if (cache[this.pageTypeCode]) {
                this.primaryPageChoices = cache[this.pageTypeCode];
                this._getAllPrimaryDisplayCondition();
            } else {
                pageService.getPrimaryPagesForPageType(this.pageTypeCode, this.uriContext).then(
                        function(primaryPages) {
                            this.primaryPageChoices = primaryPages;
                            cache[this.pageTypeCode] = primaryPages;
                        }.bind(this),
                        function(error) {
                            console.error(error);
                            this.primaryPageChoices = [];
                        }.bind(this))
                    .finally(function() {
                        this._getAllPrimaryDisplayCondition();
                    }.bind(this));
            }
        } else {
            this._getAllPrimaryDisplayCondition();
        }
    };

    this._getAllPrimaryDisplayCondition = function() {
        pageDisplayConditionsService.getNewPageConditions(this.pageTypeCode, this.uriContext).then(function(response) {
            this.conditions = response;
            this.conditionSelected = this.conditions[0];

            if (this.conditions.length > 1) {
                this.conditionSelected = lodash.find(this.conditions, function(condition) {
                    return condition.label === this.initialConditionSelected;
                }.bind(this));
            }

            if (this.primaryPageChoices && this.primaryPageChoices.length > 0) {
                if (this.initialPrimaryPageSelected) {
                    this.primarySelected = this.primaryPageChoices.find(function(page) {
                        return page.label === this.initialPrimaryPageSelected;
                    }.bind(this));
                }
                if (!this.primarySelected) {
                    this.primarySelected = this.primaryPageChoices[0];
                }
            }

            this.ready = true;
        }.bind(this)).finally(function() {
            if (this.targetCatalogVersion && !this._isUriContextEqualToCatalogVersion(this.uriContext, this.targetCatalogVersion)) {
                this._getOnlyPrimaryDisplayCondition();
            } else {
                this.dataChanged();
            }
        }.bind(this));
    };

    this._getOnlyPrimaryDisplayCondition = function() {
        this.conditions = [{
            description: "page.displaycondition.primary.description",
            isPrimary: true,
            label: "page.displaycondition.primary"
        }];

        this.conditionSelected = this.conditions[0];
        this.primarySelected = true;

        this.ready = true;
        this.dataChanged();
    };

    this._isUriContextEqualToCatalogVersion = function(uriContext, catalogVersion) {
        return uriContext && catalogVersion && catalogVersion.siteId === uriContext.CURRENT_CONTEXT_SITE_ID &&
            catalogVersion.catalogId === uriContext.CURRENT_CONTEXT_CATALOG &&
            catalogVersion.version === uriContext.CURRENT_CONTEXT_CATALOG_VERSION;
    };
}])

/**
 * @ngdoc directive
 * @name newPageDisplayConditionModule.directive:newPageDisplayCondition
 * @scope
 * @restrict E
 * @element new-page-display-condition
 *
 * @description
 * Component for selecting the page condition that can be applied to a new page.
 * The component takes a page type and some URI params that it needs to load the necessary information, and outputs
 * a display condition result. See below
 *
 * @param {< String} pageTypeCode The page typeCode of a potential new page
 * @param {< Object} uriContext The uri context containing site/catalog information. This is necessary for the
 * component to determine which display conditions can be applied.
 * @param {String} uriContext.siteUID The site ID for the new page
 * @param {String} uriContext.catalogId The catalog ID for the new page
 * @param {String} uriContext.catalogVersion The catalog version for the new page
 * @param {< Function =} resultFn An optional output function binding. Every time there is a change to the output,
 * or resulting display condition, this function (if it exists) will get executed with a
 * {@link newPageDisplayConditionModule.object:newPageDisplayConditionResult newPageDisplayConditionResult} as the single
 * parameter.
 * @param {< String =} initialPrimaryPageSelected A page label if primary page associated to the variation must be selected by default.
 * @param {< String =} initialConditionSelected An optional string to provide if any display condition must be selected by default.
 * @param {< String =} targetCatalogVersion An optional string to provide if the target catalog version selected
 */
.component('newPageDisplayCondition', {
    controller: 'newPageDisplayConditionController',
    templateUrl: 'newPageDisplayConditionTemplate.html',
    bindings: {
        pageTypeCode: '<',
        uriContext: '<',
        resultFn: '<?',
        initialPrimaryPageSelected: '<?',
        initialConditionSelected: '<?',
        targetCatalogVersion: '<?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name selectPageTypeModule
 * @description
 * #selectPageTypeModule
 *
 * The selectPageTypeModule module contains the {@link selectPageTypeModule.directive:selectPageType selectPageType} component
 *
 */
angular.module('selectPageTemplateModule', ['pageTemplateServiceModule', 'l10nModule'])

/**
 * @ngdoc directive
 * @name selectPageTemplateModule.directive:selectPageTemplate
 * @scope
 * @restrict E
 * @element select-page-template
 *
 * @description
 * Displays a list of all CMS page templates in the system, and allows the user to select one, triggering the on-template-selected callback.
 *
 * @param {Function} onTemplateSelected [Required] A callback function that is called when a template is selected from the list.
 * The function is called with a single argument, an object representing the selected page template.
 */
.component('selectPageTemplate', {
    controller: 'selectPageTemplateController',
    templateUrl: 'selectPageTemplateTemplate.html',
    bindings: {
        uriContext: '<',
        pageTypeCode: '<',
        onTemplateSelected: '<'
    }
})

.controller('selectPageTemplateController', ['pageTemplateService', '$rootScope', function(pageTemplateService) {

    var self = this;
    var cache = {};

    this.templateSelected = function templateSelected(pageTemplate) {
        this.selectedTemplate = pageTemplate;
        this.onTemplateSelected(pageTemplate);
    };

    this.isSelected = function isSelected(pageTemplate) {
        return pageTemplate === this.selectedTemplate;
    };

    this.clearSearch = function clearSearch() {
        this.searchString = "";
    };

    this.$onChanges = function onChanges(changesObj) {
        if (changesObj.pageTypeCode.currentValue) {
            this._onInputUpdated();
        }
    };

    this._onInputUpdated = function _onInputUpdated() {
        this.clearSearch();
        this.selectedTemplate = null;
        if (cache[self.pageTypeCode]) {
            self.pageTemplates = cache[self.pageTypeCode];
        } else {
            self.pageTemplates = [];

            pageTemplateService.getPageTemplatesForType(self.uriContext, self.pageTypeCode).then(function(pageTemplates) {
                cache[self.pageTypeCode] = pageTemplates.templates;
                self.pageTemplates = cache[self.pageTypeCode];
            });
        }
    };
}])

.filter('templateNameFilter', function() {
    return function(templates, criteria) {
        var filterResult = [];
        if (!criteria) {
            return templates;
        }

        criteria = criteria.toLowerCase();
        var criteriaList = criteria.split(" ");

        (templates || []).forEach(function(template) {
            var match = true;
            var term = template.name.toLowerCase();

            criteriaList.forEach(function(item) {
                if (term.indexOf(item) === -1) {
                    match = false;
                    return false;
                }
            });

            if (match && filterResult.indexOf(template) === -1) {
                filterResult.push(template);
            }
        });
        return filterResult;
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name selectPageTypeModule
 * @description
 * #selectPageTypeModule
 *
 * The selectPageTypeModule module contains the {@link selectPageTypeModule.directive:selectPageType selectPageType} component
 *
 */
angular.module('selectPageTypeModule', ['pageTypeServiceModule', 'l10nModule'])

/**
 * @ngdoc directive
 * @name selectPageTypeModule.directive:selectPageType
 * @scope
 * @restrict E
 * @element data-select-page-type
 *
 * @description
 * Displays a list of all CMS page types in the system, and allows the user to select one, triggering the on-type-selected callback.
 *
 * @param {Function} onTypeSelected [Required] A callback function that is called when a type is selected from the list.
 * The function is called with a single argument, an object representing the selected page type.
 */
.component('selectPageType', {
    controller: 'selectPageTypeController',
    templateUrl: 'selectPageTypeTemplate.html',
    bindings: {
        onTypeSelected: '='
    }
})

.controller('selectPageTypeController', ['pageTypeService', function(pageTypeService) {

    this.pageTypes = [];
    this.selectedType = null;

    this.selectType = function typeSelected(pageType) {
        this.selectedType = pageType;
        this.onTypeSelected(pageType);
    };

    this.isSelected = function isSelected(pageType) {
        return pageType === this.selectedType;
    };

    this.$onInit = function $onInit() {
        pageTypeService.getPageTypes().then(function(pageTypes) {
            this.pageTypes = pageTypes;
        }.bind(this));

    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name selectTargetCatalogVersionModule
 * @description
 * #selectTargetCatalogVersionModule
 *
 * The selectTargetCatalogVersionModule module contains the {@link selectTargetCatalogVersionModule.directive:selectTargetCatalogVersion selectTargetCatalogVersion} component
 *
 */
angular.module('selectTargetCatalogVersionModule', [
    'yLoDashModule',
    'pageFacadeModule',
    'catalogVersionRestServiceModule',
    'pageServiceModule',
    'resourceLocationsModule'
])

/**
 * @ngdoc directive
 * @name selectTargetCatalogVersionModule.directive:selectTargetCatalogVersion
 * @scope
 * @restrict E
 * @element select-target-catalog-version
 *
 * @description
 * Displays a list of catalog versions used for selecting the target catalog version that will be applied to the cloned page
 *
 * @param {< Object} uriContext The uri context containing site/catalog information. This is necessary for the
 * component to determine the list of catalog versions to display
 * @param {String} uriContext.siteUID The site ID for the new page
 * @param {String} uriContext.catalogId The catalog ID for the new page
 * @param {String} uriContext.catalogVersion The catalog version for the new page
 * @param {< String} pageTypeCode The page typeCode of a potential new page
 * @param {< String} pageLabel The page label of a potential new page
 * @param {< Function =} onTargetCatalogVersionSelected An optional output function binding. Every time there is a change to the catalogVersion selection,
 * or resulting target catalog version, this function (if it exists) will get executed
 */
.component('selectTargetCatalogVersion', {
    controller: 'selectTargetCatalogVersionController',
    templateUrl: 'selectTargetCatalogVersionTemplate.html',
    bindings: {
        uriContext: '<',
        pageTypeCode: '<',
        pageLabel: '<',
        basePageInfo: '<',
        onTargetCatalogVersionSelected: '&'
    }
})

.controller('selectTargetCatalogVersionController', ['$q', 'lodash', 'pageFacade', 'catalogVersionRestService', 'catalogService', 'pageService', 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION',
    function($q, lodash, pageFacade, catalogVersionRestService, catalogService, pageService, PAGE_CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION) {

        this.catalogVersions = [];
        this.selectedCatalogVersion = null;
        this.catalogVersionContainsPageWithSameLabel = false;
        this.catalogVersionContainsPageWithSameTypeCode = false;

        this.catalogVersionSelectorFetchStrategy = {
            fetchAll: function() {
                return $q.when(lodash.forEach(this.catalogVersions, function(catalogVersion) {
                    catalogVersion.id = catalogVersion.uuid;
                    catalogVersion.label = catalogVersion.name;
                }));
            }.bind(this)
        };

        this.onSelectionChanged = function() {
            if (this.selectedCatalogVersion) {
                catalogService.getCatalogVersionByUuid(this.selectedCatalogVersion).then(function(catalogVersion) {
                    if (this.pageTypeCode === 'ContentPage') {
                        pageFacade.contentPageWithLabelExists(this.pageLabel, catalogVersion.catalogId, catalogVersion.version).then(function(pageExists) {
                            this.catalogVersionContainsPageWithSameLabel = pageExists;
                            this.onTargetCatalogVersionSelected({
                                $catalogVersion: catalogVersion
                            });
                        }.bind(this));
                    } else {
                        var uriContextForSelectedCatalogVersion = {};
                        uriContextForSelectedCatalogVersion[PAGE_CONTEXT_SITE_ID] = catalogVersion.siteId;
                        uriContextForSelectedCatalogVersion[PAGE_CONTEXT_CATALOG] = catalogVersion.catalogId;
                        uriContextForSelectedCatalogVersion[PAGE_CONTEXT_CATALOG_VERSION] = catalogVersion.version;

                        pageService.getPrimaryPagesForPageType(this.pageTypeCode, uriContextForSelectedCatalogVersion).then(function(result) {
                            this.catalogVersionContainsPageWithSameTypeCode = !lodash.isEmpty(result);
                            this.onTargetCatalogVersionSelected({
                                $catalogVersion: catalogVersion
                            });
                        }.bind(this));
                    }
                }.bind(this));
            }
        }.bind(this);

        this.$onInit = function() {
            catalogVersionRestService.getCloneableTargets(this.uriContext).then(function(targets) {
                this.catalogVersions = targets.versions;

                if (!lodash.isEmpty(this.catalogVersions)) {
                    catalogService.getCatalogVersionUUid(this.uriContext).then(function(uuid) {
                        var currentCatalogVersion = lodash.find(this.catalogVersions, function(catalogVersion) {
                            return catalogVersion.uuid === uuid;
                        });

                        if (currentCatalogVersion) {
                            this.selectedCatalogVersion = currentCatalogVersion.uuid;
                        } else {
                            this.selectedCatalogVersion = this.catalogVersions[0].uuid;
                        }
                    }.bind(this));
                }
            }.bind(this));
        };
    }
]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name componentCloneInfoFormModule
 * @description
 * #componentCloneInfoFormModule
 *
 * The componentCloneInfoFormModule module contains the clone page generic editor form fields
 *
 */
angular.module('componentCloneInfoFormModule', ['yLoDashModule', 'catalogServiceModule', 'pageFacadeModule', 'pageServiceModule', 'genericEditorModule', 'eventServiceModule', 'resourceLocationsModule'])

.controller('componentCloneInfoFormController', ['$translate', 'lodash', 'catalogService', 'pageFacade', 'pageService', 'systemEventService', 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', function($translate, lodash, catalogService, pageFacade, pageService, systemEventService, PAGE_CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT) {

    this.$onInit = function() {
        this.pageLabel = null;
        this.catalogVersionContainsPageWithSameTypeCode = false;

        if (this.pageTypeCode !== 'ContentPage' && this.targetCatalogVersion &&
            !this._isUriContextEqualToCatalogVersion(this.uriContext, this.targetCatalogVersion)) {
            var uriContextForSelectedCatalogVersion = {};

            uriContextForSelectedCatalogVersion[PAGE_CONTEXT_SITE_ID] = this.targetCatalogVersion.siteId;
            uriContextForSelectedCatalogVersion[PAGE_CONTEXT_CATALOG] = this.targetCatalogVersion.catalogId;
            uriContextForSelectedCatalogVersion[PAGE_CONTEXT_CATALOG_VERSION] = this.targetCatalogVersion.version;

            pageService.getPrimaryPagesForPageType(this.pageTypeCode, uriContextForSelectedCatalogVersion).then(function(result) {
                this.catalogVersionContainsPageWithSameTypeCode = !lodash.isEmpty(result);
            }.bind(this));
        }

        this.setGenericEditorApi = function(api) {
            this.pageInfoEditorApi = api;
        }.bind(this);
    };

    this.$doCheck = function() {
        if (this.pageTypeCode === 'ContentPage' && this.targetCatalogVersion && this.pageInfoEditorApi &&
            !this._isUriContextEqualToCatalogVersion(this.uriContext, this.targetCatalogVersion)) {
            var content = this.pageInfoEditorApi.getContent();

            if (content && content.label !== this.pageLabel) {
                this.pageLabel = content.label;

                pageFacade.contentPageWithLabelExists(this.pageLabel, this.targetCatalogVersion.catalogId, this.targetCatalogVersion.version).then(function(pageExists) {
                    if (pageExists) {
                        systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                            messages: [{
                                subject: 'label',
                                message: $translate.instant('se.cms.clonepagewizard.pageinfo.targetcatalogversion.label.exists.message'),
                                type: 'Warning'
                            }]
                        });
                    } else {
                        this.pageInfoEditorApi.clearMessages();
                    }
                }.bind(this));
            }
        }
    };

    this._isUriContextEqualToCatalogVersion = function(uriContext, catalogVersion) {
        return uriContext && catalogVersion && catalogVersion.siteId === uriContext.CURRENT_CONTEXT_SITE_ID &&
            catalogVersion.catalogId === uriContext.CURRENT_CONTEXT_CATALOG &&
            catalogVersion.version === uriContext.CURRENT_CONTEXT_CATALOG_VERSION;
    };
}])

/**
 * @ngdoc directive
 * @name componentCloneInfoFormModule.directive:componentCloneInfoForm
 * @scope
 * @restrict E
 * @element component-clone-info-form
 *
 * @description
 * Component for the clone page info form
 *
 * @param {<Object} structure The structure that is passed on to the generic editor
 * @param {<Object} content The model that is passed on to the generic editor
 * @param {Function} submit Function defined in outer scope for saving the form fields
 * @param {Function} reset Function defined in outer scope for reseting all fields in the form
 * @param {Function} isDirty Function defined in outer scope returning if the form is in pristine state or not (e.g., have been modified).
 * @param {Function} isValid Function defined in outer scope returning if all fields in the form are valid
 * @param {<String} pageTemplate The pageTemplate property of the cloned page
 * @param {<String} pageTypeCode The typeCode property of the cloned page
 * @param {<Object} uriContext The uri context containing site/catalog information. This is necessary for the
 * component to determine if the selected target catalog version is different then the current page context
 * @param {String} uriContext.siteUID The site ID of the current page
 * @param {String} uriContext.catalogId The catalog ID of the current page
 * @param {String} uriContext.catalogVersion The catalog version of the current page
 * @param {<Object} targetCatalogVersion The selected catalogVersion containing the catalogId, version, and siteId
 * @param {String} targetCatalogVersion.siteId The selected site ID
 * @param {String} targetCatalogVersion.catalogId The selected catalog ID
 * @param {String} targetCatalogVersion.version The selected catalog version
 */
.component('componentCloneInfoForm', {
    controller: 'componentCloneInfoFormController',
    templateUrl: 'componentCloneInfoTemplate.html',
    bindings: {
        structure: '<',
        content: '<',
        submit: '=',
        reset: '=',
        isDirty: '=',
        isValid: '=',
        pageTemplate: '<',
        pageTypeCode: '<',
        uriContext: '<',
        targetCatalogVersion: '<'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name componentCloneOptionFormModule
 * @description
 * #componentCloneOptionFormModule
 *
 * The componentCloneOptionModule module contains the clone page selection options
 * for choosing between copying child components or referencing them.
 *
 */
angular.module('componentCloneOptionFormModule', ['yHelpModule'])

.controller('componentCloneOptionFormController', ['$translate', function($translate) {

    this.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION = {
        "REFERENCE_EXISTING": "reference",
        "CLONE": "clone"
    };

    this.$onInit = function() {

        this.helpTemplate = "<span>" + $translate.instant('se.cms.clonepagewizard.options.tooltip') + "</span>";

        this.componentInSlotOption = this.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.REFERENCE_EXISTING;
        this.onSelectionChange({
            $cloneOptionData: this.componentInSlotOption
        });
    };

    this.updateComponentInSlotOption = function(option) {
        this.componentInSlotOption = option;
        this.onSelectionChange({
            $cloneOptionData: this.componentInSlotOption
        });
    };
}])

/**
 * @ngdoc directive
 * @name componentCloneOptionModule.directive:componentCloneOptionForm
 * @scope
 * @restrict E
 * @element component-clone-option-form
 *
 * @description
 * Component for selecting the clone page options for child components in content slots.
 * The options are clone existing child components or reference them
 *
 * @param {&Expression} onSelectionChange The parent function to call when a selection is made.
 */
.component('componentCloneOptionForm', {
    controller: 'componentCloneOptionFormController',
    templateUrl: 'componentCloneOptionTemplate.html',
    bindings: {
        onSelectionChange: '&'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsEditorModule', ['displayConditionsEditorControllerModule', 'displayConditionsPageInfoModule', 'displayConditionsPageVariationsModule', 'displayConditionsPrimaryPageModule'])
    .component('displayConditionsEditor', {
        controller: 'displayConditionsEditorController',
        templateUrl: 'displayConditionsEditorTemplate.html',
        bindings: {
            pageUid: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsEditorControllerModule', ['displayConditionsEditorModelModule'])
    .controller('displayConditionsEditorController', ['displayConditionsEditorModel', function(displayConditionsEditorModel) {
        this.$onInit = function() {
            displayConditionsEditorModel.initModel(this.pageUid);
        };

        this.getPageName = function() {
            return displayConditionsEditorModel.pageName;
        };

        this.getPageType = function() {
            return displayConditionsEditorModel.pageType;
        };

        this.isPagePrimary = function() {
            return displayConditionsEditorModel.isPrimary;
        };

        this.getVariations = function() {
            return displayConditionsEditorModel.variations;
        };

        this.getAssociatedPrimaryPage = function() {
            return displayConditionsEditorModel.associatedPrimaryPage;
        };

        this.getIsAssociatedPrimaryReadOnly = function() {
            return displayConditionsEditorModel.isAssociatedPrimaryReadOnly;
        };

        this.getPrimaryPages = function() {
            return displayConditionsEditorModel.primaryPages;
        };

        this.onPrimaryPageSelect = function(primaryPage) {
            displayConditionsEditorModel.setAssociatedPrimaryPage(primaryPage);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPageInfoModule', ['displayConditionsPageInfoControllerModule', 'yHelpModule'])
    .component('displayConditionsPageInfo', {
        controller: 'displayConditionsPageInfoController',
        templateUrl: 'displayConditionsPageInfoTemplate.html',
        bindings: {
            pageName: '<',
            pageType: '<',
            isPrimary: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPageInfoControllerModule', ['translationServiceModule'])
    .controller('displayConditionsPageInfoController', ['$translate', function($translate) {
        this.displayConditionLabelI18nKey = 'se.cms.display.conditions.label';
        this.pageNameI18nKey = 'se.cms.pagelist.headerpagename';
        this.pageTypeI18nKey = 'se.cms.pagelist.headerpagetype';

        this.getPageDisplayConditionI18nKey = function() {
            return this.isPrimary ? 'se.cms.display.conditions.primary.id' : 'se.cms.display.conditions.variation.id';
        };

        this.getPageDisplayConditionDescriptionI18nKey = function() {
            return this.isPrimary ? 'se.cms.display.conditions.primary.description' : 'se.cms.display.conditions.variation.description';
        };

        this.$onChanges = function() {

            $translate(this.getPageDisplayConditionDescriptionI18nKey()).then(function(translation) {
                this.helpTemplate = "<span>" + translation + "</span>";
            }.bind(this));

        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPageVariationsModule', ['displayConditionsPageVariationsControllerModule', 'clientPagedListModule'])
    .component('displayConditionsPageVariations', {
        templateUrl: 'displayConditionsPageVariationsTemplate.html',
        controller: 'displayConditionsPageVariationsController',
        bindings: {
            variations: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPageVariationsControllerModule', [])
    .controller('displayConditionsPageVariationsController', function() {
        this.variationPagesTitleI18nKey = 'se.cms.display.conditions.variation.pages.title';
        this.noVariationsI18nKey = 'se.cms.display.conditions.no.variations';
        this.variationsDescriptionI18nKey = 'se.cms.display.conditions.variations.description';

        this.itemsPerPage = 3;

        this.keys = [{
            property: 'pageName',
            i18n: 'se.cms.display.conditions.header.page.name'
        }, {
            property: 'creationDate',
            i18n: 'se.cms.display.conditions.header.creation.date'
        }, {
            property: 'restrictions',
            i18n: 'se.cms.display.conditions.header.restrictions'
        }];

        this.renderers = {
            creationDate: function() {
                return '<span>{{item.creationDate | date: "short"}}</span>';
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPrimaryPageModule', ['displayConditionsPrimaryPageControllerModule'])
    .component('displayConditionsPrimaryPage', {
        templateUrl: 'displayConditionsPrimaryPageTemplate.html',
        controller: 'displayConditionsPrimaryPageController',
        bindings: {
            readOnly: '<',
            associatedPrimaryPage: '<',
            allPrimaryPages: '<?',
            onPrimaryPageSelect: '&?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsPrimaryPageControllerModule', [])
    .controller('displayConditionsPrimaryPageController', function() {
        this.associatedPrimaryPageLabelI18nKey = 'se.cms.display.conditions.primary.page.label';

        this.triggerOnPrimaryPageSelect = function() {
            this.onPrimaryPageSelect({
                primaryPage: this.associatedPrimaryPage
            });
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageEditorModalServiceModule
 * @description
 * # The pageEditorModalServiceModule
 *
 * The page editor modal service module provides a service that allows opening an editor modal for a given page. The editor modal is populated with a save and cancel button, and is loaded with the
 * editorTabset of cmssmarteditContainer as its content, providing a way to edit
 * various fields of the given page.
 */
angular.module('pageEditorModalServiceModule', ['genericEditorModalServiceModule', 'editPageBasicTabModule', 'editPageVisibilityTabModule', 'displayConditionsTabModule', 'pageServiceModule', 'crossFrameEventServiceModule', 'synchronizationConstantsModule'])
    /**
     * @ngdoc service
     * @name pageEditorModalServiceModule.service:pageEditorModalService
     *
     * @description
     * Convenience service to open an editor modal window for a given page's data.
     *
     */
    .factory('pageEditorModalService', ['genericEditorModalService', 'pageService', 'crossFrameEventService', 'SYNCHRONIZATION_POLLING', function(genericEditorModalService, pageService, crossFrameEventService, SYNCHRONIZATION_POLLING) {

        function PageEditorModalService() {}

        var PAGE_INFO_TAB_CONFIGURATION = {
            id: 'editPageBasicTab',
            title: 'se.cms.pageinfo.information.title',
            templateUrl: 'editPageBasicTabTemplate.html'
        };

        var DISPLAY_CONDITIONS_TAB_CONFIGURATION = {
            id: 'editPageDisplayConditionsTab',
            title: 'se.cms.pageinfo.display.conditions.title',
            templateUrl: 'displayConditionsTabTemplate.html'
        };

        var RESTRICTIONS_TAB_CONFIGURATION = {
            id: 'editPageVisibilityTab',
            title: 'se.cms.restrictions.editor.tab',
            templateUrl: 'editPageVisibilityTabTemplate.html'
        };

        var STATIC_TABS = [PAGE_INFO_TAB_CONFIGURATION, DISPLAY_CONDITIONS_TAB_CONFIGURATION];

        /**
         * @ngdoc method
         * @name pageEditorModalServiceModule.service:pageEditorModalService#open
         * @methodOf pageEditorModalServiceModule.service:pageEditorModalService
         *
         * @description
         * Uses the {@link genericEditorModalService.open genericEditorModalService} to open an editor modal.
         *
         * The editor modal is initialized with a title in the format '<TypeName> Editor', ie: 'Paragraph Editor'. The
         * editor modal is also wired with a save and cancel button.
         *
         * The content of the editor modal is the {@link editorTabsetModule.directive:editorTabset editorTabset}.
         *
         * @param {Object} page The data associated to a page as defined in the platform.
         *
         * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
         */
        PageEditorModalService.prototype.open = function(page) {
            return pageService.isPagePrimary(page.uid).then(function(isPagePrimary) {
                var tabs = STATIC_TABS.concat(isPagePrimary ? [] : [RESTRICTIONS_TAB_CONFIGURATION]);
                var editorModalConfiguration = {
                    uid: page.uid,
                    uuid: page.uuid,
                    typeCode: page.typeCode,
                    uriContext: page.uriContext,
                    title: 'se.cms.pageeditormodal.editpagetab.title',
                    template: page.template,
                    onlyOneRestrictionMustApply: page.onlyOneRestrictionMustApply
                };

                return genericEditorModalService.open(editorModalConfiguration, tabs, function() {
                    crossFrameEventService.publish(SYNCHRONIZATION_POLLING.FETCH_SYNC_STATUS_ONCE, page.uuid);
                });
            });
        };

        return new PageEditorModalService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('editPageBasicTabModule', ['editPageBasicTabControllerModule', 'genericEditorModule'])
    .component('editPageBasicTab', {
        templateUrl: 'editPageBasicTabInnerTemplate.html',
        controller: 'editPageBasicTabController',
        bindings: {
            model: '=',
            saveTab: '=',
            resetTab: '=',
            cancelTab: '=',
            isDirtyTab: '=',
            tabId: '='
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('editPageBasicTabControllerModule', ['pageServiceModule', 'genericEditorModule',
        'contextAwarePageStructureServiceModule', 'cmsitemsRestServiceModule', 'eventServiceModule'
    ])
    .controller('editPageBasicTabController', ['$q', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'pageService', 'contextAwarePageStructureService', 'cmsitemsRestService', 'systemEventService', function($q, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT,
        pageService, contextAwarePageStructureService, cmsitemsRestService, systemEventService) {

        this._updateRestrictions = function(eventId, restrictionsObj) {
            this.onlyOneRestrictionMustApply = restrictionsObj.onlyOneRestrictionMustApply;
            this.restrictions = restrictionsObj.restrictions;
        };

        this._updateAssociatedPrimary = function(eventId, associatedPrimaryPage) {
            this.content.label = associatedPrimaryPage.label;
            this.genericEditorApi.updateContent(this.content);
        };

        this.$onInit = function() {

            var pagePromise = cmsitemsRestService.getById(this.model.uuid);
            var isPagePrimaryPromise = pageService.isPagePrimary(this.model.uid);

            $q.all([pagePromise, isPagePrimaryPromise]).then(function(values) {
                this.content = values[0];

                cmsitemsRestService.getById(values[0].masterTemplate).then(function(templateInfo) {
                    this.content.template = templateInfo.uid;

                    contextAwarePageStructureService.getPageStructureForPageEditing(this.content.typeCode, this.content.uid).then(function(fields) {
                        this.structure = fields;
                    }.bind(this));
                }.bind(this));

            }.bind(this));

            this.saveTab = function() {
                if (this.submitCallback) {
                    return this.submitCallback().then(function(result) {
                        result.onlyOneRestrictionMustApply = this.onlyOneRestrictionMustApply;
                        result.restrictions = this.restrictions;
                        result.identifier = result.uuid;
                        return cmsitemsRestService.update(result).then(function(result) {
                            return result;
                        }, function(failure) {
                            systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                                messages: failure.data.errors
                            });

                            //Handle all restriction errors
                            var restrictionErrors = [];
                            if (failure.data && failure.data.errors) {
                                failure.data.errors.forEach(function(error) {
                                    //If the error subject is restrictions, highlight the restrictions tab
                                    if (error.subject.indexOf('restrictions') === 0) {
                                        error.tabId = 'editPageVisibilityTab';
                                    }
                                    if (error.subject === 'restrictions') {
                                        restrictionErrors.push(error);
                                    }
                                });
                            }

                            //Fire an event so the restrictions tab can process and display the errors accordingly
                            if (restrictionErrors.length !== 0) {
                                systemEventService.sendAsynchEvent("EDIT_PAGE_RESTRICTIONS_ERRORS_EVENT", restrictionErrors);
                            }

                            return $q.reject(failure);
                        });
                    }.bind(this));
                } else {
                    var errorMsg = 'saveTab: Save callback not defined';
                    return $q.reject(errorMsg);
                }
            }.bind(this);

            this.setGenericEditorApi = function(api) {
                this.genericEditorApi = api;
            }.bind(this);

            this.unregFn = systemEventService.registerEventHandler("EDIT_PAGE_RESTRICTIONS_UPDATED_EVENT", this._updateRestrictions.bind(this));
            this.associatePrimaryunregFn = systemEventService.registerEventHandler("EDIT_PAGE_ASSOCIATED_PRIMARY_UPDATED_EVENT", this._updateAssociatedPrimary.bind(this));

        };

        this.$onDestroy = function() {
            this.unregFn();
            this.associatePrimaryunregFn();
        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsTabModule', ['displayConditionsTabControllerModule', 'displayConditionsEditorModule'])
    .component('displayConditionsTab', {
        templateUrl: 'displayConditionsTabInnerTemplate.html',
        controller: 'displayConditionsTabController',
        bindings: {
            model: '=',
            save: '=',
            reset: '=',
            cancel: '=',
            isDirty: '=',
            tabId: '='
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsTabControllerModule', ['displayConditionsEditorModelModule'])
    .controller('displayConditionsTabController', ['displayConditionsEditorModel', '$q', function(displayConditionsEditorModel, $q) {
        this.$onInit = function() {
            this.pageUid = this.model.uid;

            this.save = function() {
                return displayConditionsEditorModel.save();
            };

            this.reset = function() {
                return true;
            };

            this.cancel = function() {
                return $q.when(true);
            };

            this.isDirty = function() {
                return displayConditionsEditorModel.isDirty();
            };
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('editPageVisibilityTabModule', ['eventServiceModule', 'pageRestrictionsModule', 'restrictionsServiceModule', 'pageRestrictionsInfoMessageModule', 'cmsitemsRestServiceModule'])
    .controller('EditPageVisibilityTabCtrl', ['systemEventService', 'pageRestrictionsFacade', 'restrictionsService', 'cmsitemsRestService', function(systemEventService, pageRestrictionsFacade, restrictionsService, cmsitemsRestService) {

        this.restrictionsResult = function(onlyOneRestrictionMustApply, restrictions) {
            var restrictionsObj = {
                onlyOneRestrictionMustApply: onlyOneRestrictionMustApply,
                restrictions: restrictions
            };

            this.restrictions = restrictions;
            systemEventService.sendAsynchEvent("EDIT_PAGE_RESTRICTIONS_UPDATED_EVENT", restrictionsObj);

            //Clear all errors when we make a change to page restrictions
            this.errors = [];
        }.bind(this);

        this.getRestrictionTypes = function() {
            return pageRestrictionsFacade.getRestrictionTypesByPageType(this.page.typeCode);
        }.bind(this);

        this.getSupportedRestrictionTypes = function() {
            return restrictionsService.getSupportedRestrictionTypeCodes();
        };

        this.unregFn = systemEventService.registerEventHandler("EDIT_PAGE_RESTRICTIONS_ERRORS_EVENT", function(event, errors) {
            this.errors = errors;
        }.bind(this));

        this.$onDestroy = function() {
            this.unregFn();
        };

        this.$onInit = function() {
            this.isReady = false;
            this.page = this.model;

            cmsitemsRestService.getById(this.page.uuid).then(function(itemData) {
                this.page.restrictions = itemData.restrictions;
                this.errors = [];
                this.restrictions = this.page.restrictions;
                this.isReady = true;
            }.bind(this));
        };
    }])
    .directive('editPageVisibilityTab', function() {
        return {
            restrict: 'E',
            scope: {},
            templateUrl: 'editPageVisibilityTabInnerTemplate.html',
            controller: 'EditPageVisibilityTabCtrl',
            controllerAs: 'ctrl',
            bindToController: {
                model: '=',
                saveTab: '=',
                resetTab: '=',
                cancelTab: '=',
                isDirtyTab: '=',
                tabId: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageInfoContainerModule
 * @description
 * This module contains pageInfoContainer component.
 */
angular.module('pageInfoContainerModule', ['catalogServiceModule', 'pageInfoDetailsModule', 'pageInfoHeaderModule', 'componentHandlerServiceModule', 'pageListServiceModule', 'contextAwarePageStructureServiceModule', 'pageEditorModalServiceModule', 'cmsitemsRestServiceModule', 'pageServiceModule'])

.controller('pageInfoContainerController', ['catalogService', 'componentHandlerService', 'pageListService', 'contextAwarePageStructureService', 'pageEditorModalService', 'cmsitemsRestService', 'pageService', function(catalogService, componentHandlerService, pageListService, contextAwarePageStructureService, pageEditorModalService, cmsitemsRestService, pageService) {
    this.$onInit = function() {
        pageService.getCurrentPageInfo().then(function(pageInfo) {
            this.pageUid = pageInfo.uid;
            this.pageUuid = pageInfo.uuid;
            this.pageTemplateUuid = pageInfo.masterTemplate;
            this.pageTypeCode = pageInfo.typeCode;
            this.pageContent = pageInfo;
            catalogService.retrieveUriContext().then(function(uriContext) {
                this.pageContent = pageInfo;
                this.pageContent.uriContext = uriContext;
            }.bind(this));
        }.bind(this)).then(function() {
            cmsitemsRestService.getById(this.pageTemplateUuid).then(function(templateInfo) {
                this.pageTemplate = templateInfo.uid;
            }.bind(this));
        }.bind(this)).then(function() {
            contextAwarePageStructureService.getPageStructureForViewing(this.pageTypeCode).then(function(pageStructure) {
                this.pageStructure = pageStructure;
            }.bind(this));
        }.bind(this));
    }.bind(this);

    this.onEditClick = function() {
        pageEditorModalService.open(this.pageContent);
    };
}])


/**
 * @ngdoc directive
 * @name pageInfoContainerModule.pageInfoContainer
 * @description
 * Directive that can render current storefront page's information and provides a callback function triggered on opening the editor.
 */
.component('pageInfoContainer', {
    templateUrl: 'pageInfoContainerTemplate.html',
    controller: 'pageInfoContainerController'
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageInfoDetailsModule
 * @description
 * This module contains pageInfoDetails component.
 */
angular.module('pageInfoDetailsModule', ['hasOperationPermissionModule'])

/**
 * @ngdoc directive
 * @name pageInfoDetailsModule.pageInfoDetails
 * @description
 * Directive that can render page information, using an underlying generic editor. Additionally, provides an edit button
 * tied to a parameterized on click callback.
 *
 * @param {String} pageUid The UID of the page of which to render information
 * @param {String} pageTypeCode The type code of the page of which to render information
 * @param {Object} pageStructure The structure used to render the generic editor
 * @param {Object} pageContent The content of the fields of the generic editor
 * @param {Function} onEditClick The callback triggered when clicking the Edit button
 */
.component('pageInfoDetails', {
    templateUrl: 'pageInfoDetailsTemplate.html',
    bindings: {
        pageUid: '<',
        pageTypeCode: '<',
        pageStructure: '<',
        pageContent: '<',
        onEditClick: '&'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageInfoHeaderModule
 * @description
 * This module contains pageInfoHeader component.
 */
angular.module('pageInfoHeaderModule', [])

/**
 * @ngdoc directive
 * @name pageInfoHeaderModule.pageInfoHeader
 * @description
 * Directive that can render page template and type code.
 *
 * @param {String} pageTypeCode The type code of the page of which to render information
 * @param {String} pageTemplate The template of the page of which to render information
 */
.component('pageInfoHeader', {
    templateUrl: 'pageInfoHeaderTemplate.html',
    bindings: {
        pageTypeCode: '<',
        pageTemplate: '<'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageInfoMenuModule
 * @description
 *
 * The page info menu module contains the directive and controller necessary to view the page information menu from the white ribbon..
 *
 * Use the {@link pageInfoMenuModule.directive:pageInfoMenuToolbarItem pageInfoMenuToolbarItem} add this page info toolbar menu.
 *
 */
angular.module('pageInfoMenuModule', ['assetsServiceModule', 'eventServiceModule', 'iframeClickDetectionServiceModule', 'componentHandlerServiceModule', 'pageListServiceModule', 'pageInfoContainerModule', 'languageServiceModule', 'yLoDashModule', 'sharedDataServiceModule'])
    /**
     * @ngdoc directive
     * @name pageInfoMenuModule.directive:pageInfoMenuToolbarItem
     * @scope
     * @restrict E
     * @element page-info-menu-toolbar-item
     *
     * @description
     * Component responsible for displaying the current page's meta data.
     *
     * The component also allows access to the page editor modal.
     *
     */
    .component('pageInfoMenuToolbarItem', {
        templateUrl: 'pageInfoMenuToolbarItemTemplate.html'
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageListModule
 * @description
 *
 * The page list module contains the controller associated to the page list view.
 *
 * This view displays the list of pages for a specific catalog and allows the user
 * to search and sort the list.
 *
 * Use the {@link pageListServiceModule.service:pageListService pageListService}
 * to call backend API in order to get the list of pages for a specific catalog
 *
 */
angular.module('pageListControllerModule', [
    'addPageServiceModule',
    'clonePageWizardServiceModule',
    'eventServiceModule',
    'experienceServiceModule',
    'functionsModule',
    'pageEditorModalServiceModule',
    'pageListServiceModule',
    'pageListSyncIconModule',
    'pageRestrictionsServiceModule',
    'recompileDomModule',
    'resourceLocationsModule',
    'restrictionsPageListIconModule',
    'syncPageModalServiceModule',
    'urlServiceModule',
    'hasOperationPermissionModule',
    'yLoDashModule'
])

/**
 * @ngdoc controller
 * @name pageListModule.controller:pageListController
 *
 * @description
 * The page list controller fetches pages for a specified catalog using the {@link pageListServiceModule.service:pageListService pageListService}
 */
.controller('pageListController', ['$scope', '$routeParams', '$location', '$q', '$timeout', 'clonePageWizardService', 'urlService', 'lodash', 'pageListService', 'catalogService', 'addPageWizardService', 'experienceService', 'sharedDataService', 'systemEventService', 'pageEditorModalService', 'syncPageModalService', 'pageRestrictionsService', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'STOREFRONT_PATH_WITH_PAGE_ID', 'LANDING_PAGE_PATH', 'CONTEXT_SITE_ID', function(
    $scope, $routeParams, $location, $q, $timeout, clonePageWizardService,
    urlService, lodash, pageListService, catalogService, addPageWizardService, experienceService,
    sharedDataService, systemEventService, pageEditorModalService, syncPageModalService, pageRestrictionsService,
    CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, STOREFRONT_PATH_WITH_PAGE_ID, LANDING_PAGE_PATH, CONTEXT_SITE_ID) {

    this.siteUID = $routeParams.siteId;
    this.catalogId = $routeParams.catalogId;
    this.catalogVersion = $routeParams.catalogVersion;
    this.uriContext = urlService.buildUriContext(this.siteUID, this.catalogId, this.catalogVersion);
    this.pageUriContext = urlService.buildPageUriContext(this.siteUID, this.catalogId, this.catalogVersion);

    catalogService.isContentCatalogVersionNonActive().then(function(isNonActive) {

        this.pages = [];
        this.catalogName = "";
        this.itemsReady = false;
        this.searchKeys = ['name', 'uid', 'typeCode', 'template'];
        this.query = {
            value: ""
        };
        this.keys = [{
            property: 'name',
            i18n: 'se.cms.pagelist.headerpagename'
        }, {
            property: 'uid',
            i18n: 'se.cms.pagelist.headerpageid'
        }, {
            property: 'typeCode',
            i18n: 'se.cms.pagelist.headerpagetype'
        }, {
            property: 'template',
            i18n: 'se.cms.pagelist.headerpagetemplate'
        }, {
            property: 'numberOfRestrictions',
            i18n: 'se.cms.pagelist.headerrestrictions'
        }];
        if (isNonActive) {
            this.keys.push({
                property: 'syncStatus',
                i18n: 'se.cms.pagelist.dropdown.sync'
            });
        }

        this.reset = function() {
            this.query.value = '';
        };

        this.reloadPages = function() {
            var uriContext = {};
            uriContext[CONTEXT_SITE_ID] = this.siteUID;
            uriContext[CONTEXT_CATALOG] = this.catalogId;
            uriContext[CONTEXT_CATALOG_VERSION] = this.catalogVersion;
            $q.all([
                pageListService.getPageListForCatalog(uriContext),
                pageRestrictionsService.getPageRestrictionsCountMapForCatalogVersion(this.siteUID, this.catalogId, this.catalogVersion)
            ]).then(function(values) {
                var pages = values[0];
                var pageRestrictionsCountMap = values[1];

                this.pages = pages.map(function(page) {
                    return {
                        name: page.name,
                        uid: page.uid,
                        uuid: page.uuid,
                        typeCode: page.typeCode,
                        template: page.template,
                        onlyOneRestrictionMustApply: page.onlyOneRestrictionMustApply,
                        numberOfRestrictions: pageRestrictionsCountMap[page.uid] || 0
                    };
                });
                this.itemsReady = true;
            }.bind(this));
        };

        this.reloadPages();

        catalogService.getContentCatalogsForSite(this.siteUID).then(function(catalogs) {
            this.catalogName = catalogs.filter(function(catalog) {
                return catalog.catalogId === this.catalogId;
            }.bind(this))[0].name;

        }.bind(this));

        // renderers Object that contains custom HTML renderers for a given key
        this.renderers = {
            name: function() {
                return '<a data-ng-click="injectedContext.onLink( item.uid )">{{ item.name }}</a>';
            },
            numberOfRestrictions: function() {
                return '<restrictions-page-list-icon data-number-of-restrictions="item.numberOfRestrictions"/>';
            },
            syncStatus: function() {
                return '<div data-recompile-dom="item.reloadSynchIcon"><page-list-sync-icon data-uri-context="injectedContext.uriContext" data-page-id="item.uuid" /></div>';
            }
        };

        this.reloadUpdatedPage = function(uid, updatedUid) {

            var search = this.query.value; // if search value exists, save it, delete from scope and reset it after pages list is updated
            delete this.query.value;

            updatedUid = updatedUid || uid;
            return $q.all([
                pageListService.getPageById(updatedUid),
                pageRestrictionsService.getPageRestrictionsCountForPageUID(updatedUid)
            ]).then(function(values) {
                var updatedPage = values[0];
                updatedPage.numberOfRestrictions = values[1];
                this.pages = this.pages.map(function(page) {
                    if (page.uid === uid) {
                        if (page.reloadSynchIcon) { // from recompile-dom in page-list-sync-icon
                            page.reloadSynchIcon();
                        }
                        return updatedPage;
                    }
                    return page;
                }.bind(this));

                this.query.value = search;
            }.bind(this));
        };

        this.dropdownItems = [{
            key: 'se.cms.pagelist.dropdown.edit',
            callback: function(item) {

                item.uriContext = this.uriContext;

                pageEditorModalService.open(item).then(function(response) {
                    //in this case uid is not editable so the item.uid is equal to updated uid
                    if (!response.uid) {
                        response.uid = item.uid;
                    }
                    this.reloadUpdatedPage(item.uid, response.uid);
                }.bind(this));
            }.bind(this)
        }];

        if (isNonActive) {
            this.dropdownItems.push({
                key: 'se.cms.pagelist.dropdown.sync',
                callback: function(item) {
                    syncPageModalService.open(item, this.injectedContext.uriContext);
                }.bind(this)
            });
        }

        this.dropdownItems.push({
            key: 'se.cms.pagelist.dropdown.clone',
            callback: function(item) {
                clonePageWizardService.openClonePageWizard(item);
            }.bind(this)
        });

        // injectedContext Object. This object is passed to the client-paged-list directive.
        this.injectedContext = {
            onLink: function(uid) {
                if (uid) {
                    var experiencePath = this._buildExperiencePath(uid);
                    //iFrameManager.setCurrentLocation(link);
                    $location.path(experiencePath);
                }
            }.bind(this),

            uriContext: lodash.merge(this.uriContext, this.pageUriContext),

        };

        this._buildExperiencePath = function(uid) {
            return STOREFRONT_PATH_WITH_PAGE_ID
                .replace(":siteId", this.siteUID)
                .replace(":catalogId", this.catalogId)
                .replace(":catalogVersion", this.catalogVersion)
                .replace(":pageId", uid);
        };

        this.openAddPageWizard = function() {
            addPageWizardService.openAddPageWizard().then(function() {
                this.reloadPages();
            }.bind(this));
        };

    }.bind(this));
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name pageListLinkModule
     * @description
     * # The pageListLinkModule
     *
     * The pageListLinkModule provides a directive to display a link to the list of pages of a catalogue
     *
     */
    angular.module('pageListLinkModule', [])
        /**
         * @ngdoc directive
         * @name pageListLinkModule.directive:pageListLink
         * @scope
         * @restrict E
         * @element <page-list-link></page-list-link>
         *
         * @description
         * Directive that displays a link to the list of pages of a catalogue. It can only be used if catalog.catalogVersions is in the current scope.
         * 
         * @param {< Object} catalog Object representing the provided catalog. 
         * @param {< Boolean} catalogVersion Object representing the provided catalog version. 
         * @param {< String} siteId The ID of the site the provided catalog is associated with.  
         */
        .component('pageListLink', {
            templateUrl: 'pageListLinkDirectiveTemplate.html',
            bindings: {
                catalog: '<',
                catalogVersion: '<',
                siteId: '<'
            }
        });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageRestrictionsInfoMessageModule', [])
    .component('pageRestrictionsInfoMessage', {
        templateUrl: 'pageRestrictionsInfoMessageTemplate.html',
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionManagementEditModule
 * @requires alertServiceModule
 * @requires restrictionsServiceModule
 * @description
 * This module defines the {@link restrictionManagementEditModule.directive:restrictionManagementEdit restrictionManagementEdit} component.
 **/
angular.module('restrictionManagementEditModule', [
    'alertServiceModule',
    'restrictionsServiceModule',
    'functionsModule',
    'componentHandlerServiceModule'
])

.controller('RestrictionManagementEditController', ['$q', 'alertService', 'restrictionsService', 'URIBuilder', 'cmsitemsUri', function(
    $q,
    alertService,
    restrictionsService,
    URIBuilder,
    cmsitemsUri
) {
    this._internalInit = function(isRestrictionTypeSupported) {
        this.isTypeSupported = isRestrictionTypeSupported;
        if (isRestrictionTypeSupported) {
            this.submitFn = function() {
                return this.submitInternal().then(function(itemResponse) {
                    return $q.when(itemResponse);
                }.bind(this));
            }.bind(this);
        } else {
            // type not supported, disable the save button always
            this.submitFn = function() {};
            this.isDirtyFn = function() {
                return false;
            };
        }
        this.ready = true;
    }.bind(this);

    this.$onInit = function $onInit() {
        this.ready = false;
        this.restriction = this.restriction || {};
        this.itemManagementMode = 'edit';
        var dryRunCmsItemsUri = cmsitemsUri + '/:identifier?dryRun=true';
        this.contentApi = new URIBuilder(dryRunCmsItemsUri).replaceParams(this.uriContext).build();
        this.structureApi = restrictionsService.getStructureApiUri(this.itemManagementMode);

        if (typeof this.getSupportedRestrictionTypesFn !== 'undefined') {
            return this.getSupportedRestrictionTypesFn().then(function(supportedTypes) {
                this._internalInit(supportedTypes.indexOf(this.restriction.itemtype) >= 0);
            }.bind(this));
        } else {
            return this._internalInit(true);
        }
    };

}])

/**
 * @ngdoc directive
 * @name restrictionManagementEditModule.directive:restrictionManagementEdit
 * @restrict E
 * @scope
 * @param {? Function=} isDirtyFn Function returning the dirtiness status of the component.
 * @param {< String} restriction Restriction object.
 * @param {? Function=} submitFn Function defined in outer scope to validate restriction edit.
 * @param {< Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
 * @param {< Function=} getSupportedRestrictionTypesFn A function that returns list of restriction types that are supported for editing for a given item.
 * @description
 * The restrictionManagementEdit Angular component is designed to be able to edit restrictions.
 */
.component('restrictionManagementEdit', {
    controller: 'RestrictionManagementEditController',
    templateUrl: 'restrictionManagementEditTemplate.html',
    bindings: {
        isDirtyFn: '=?',
        restriction: '<',
        getSupportedRestrictionTypesFn: '<?',
        submitFn: '=?',
        uriContext: '<'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionManagementSelectModule
 * @requires alertServiceModule
 * @requires restrictionsModule
 * @requires restrictionsServiceModule
 * @requires yActionableSearchItemModule
 * @requires ySelectModule
 * @description
 * This module defines the {@link restrictionManagementSelectModule.directive:restrictionManagementSelect restrictionManagementSelect} component
 **/
angular.module('restrictionManagementSelectModule', [
    'alertServiceModule',
    'eventServiceModule',
    'restrictionsModule',
    'restrictionsServiceModule',
    'yActionableSearchItemModule',
    'ySelectModule',
    'yMessageModule',
    'cmsitemsRestServiceModule',
    'catalogServiceModule',
    'functionsModule',
    'yLoDashModule'
])

.controller('RestrictionManagementSelectController', ['$q', 'alertService', 'restrictionManagementSelectModel', 'restrictionsService', 'systemEventService', 'cmsitemsUri', 'URIBuilder', function(
    $q,
    alertService,
    restrictionManagementSelectModel,
    restrictionsService,
    systemEventService,
    cmsitemsUri,
    URIBuilder
) {

    var RESTRICTION_CREATE_BUTTON_PRESSED_EVENT_ID = "RESTRICTION_CREATE_BUTTON_PRESSED_EVENT_ID";

    this.resultsHeaderTemplate = "<y-actionable-search-item data-event-id='" + RESTRICTION_CREATE_BUTTON_PRESSED_EVENT_ID + "'></y-actionable-search-item>";
    this.resultsHeaderLabel = "se.cms.restrictionmanagement.restrictionresults.header";
    this.itemTemplateUrl = "restrictionManagementItemNameTemplate.html";
    this.editorHeader = "";

    this.getResultsHeaderTemplate = function getResultsHeaderTemplate() {
        return this.selectModel.isTypeSupported() ? this.resultsHeaderTemplate : "";
    };

    this.selectRestrictionType = function selectRestrictionType() {
        if (this.selectModel.restrictionTypeSelected()) {
            if (this.controllerModel.showRestrictionSelector) {
                this.resetSelector();
            } else {
                this.controllerModel.showRestrictionSelector = true;
            }
            this.controllerModel.showRestrictionEditor = false;
        }
    }.bind(this);

    this.selectRestriction = function selectRestriction() {
        if (this.selectModel.restrictionSelected()) {
            this.editorHeader = 'se.cms.restriction.management.select.editor.header.add';
            this.controllerModel.mode = 'add';
            this.controllerModel.structureApi = restrictionsService.getStructureApiUri(this.controllerModel.mode);

            if (this.controllerModel.showRestrictionEditor) {
                this.resetEditor();
            } else {
                this.controllerModel.showRestrictionEditor = true;
            }
        }
    }.bind(this);

    this.createButtonEventHandler = function(eventId, name) {
        this.createRestriction(name);
    }.bind(this);

    this.createRestriction = function createRestriction(name) {

        this.selectModel.createRestrictionSelected(name, this.uriContext);
        this.editorHeader = 'se.cms.restriction.management.select.editor.header.create';
        this.controllerModel.mode = 'create';
        this.controllerModel.structureApi = restrictionsService.getStructureApiUri(this.controllerModel.mode);
        if (this.controllerModel.showRestrictionEditor) {
            this.resetEditor();
        } else {
            this.controllerModel.showRestrictionEditor = true;
        }
    }.bind(this);

    this.disableRestrictionChoice = function(restriction) {
        var existingIndex = this.existingRestrictions.findIndex(function(existingRestriction) {
            return restriction.uid === existingRestriction.uid;
        });
        return existingIndex !== -1;
    }.bind(this);

    this.$onDestroy = function() {
        systemEventService.unRegisterEventHandler(RESTRICTION_CREATE_BUTTON_PRESSED_EVENT_ID, this.createButtonEventHandler);
    }.bind(this);

    this.$onInit = function $onInit() {

        this.selectModel = restrictionManagementSelectModel.createRestrictionManagementSelectModel(this.getRestrictionTypesFn, this.getSupportedRestrictionTypesFn);

        // bound by the recompile dom directive
        this.resetEditor = function resetEditor() {};
        this.resetSelector = function resetSelector() {};
        var dryRunCmsItemsUri = cmsitemsUri + '/:identifier?dryRun=true';

        this.controllerModel = {
            showRestrictionSelector: false,
            showRestrictionEditor: false,
            mode: 'add',
            contentApi: new URIBuilder(dryRunCmsItemsUri).replaceParams(this.uriContext).build()
        };

        this.isDirtyFn = function() {
            if (this.controllerModel.mode === 'add') {
                // if we're in adding mode and an editor is displayed then a restriction has been picked
                return this.controllerModel.showRestrictionEditor;
            } else if (this.isDirtyInternal) {
                // if we're creating a new restriction the use isDirty from GE
                return this.isDirtyInternal();
            }
            return false;
        }.bind(this);


        this.fetchOptions = {
            fetchPage: this.selectModel.getRestrictionsPaged,
            fetchEntity: this.selectModel.getRestrictionFromBackend
        };

        this.submitFn = function() {
            if (this.selectModel.isTypeSupported()) {
                return this.submitInternal().then(function(value) {
                    return value;
                }.bind(this));
            } else {
                return $q.when(this.selectModel.getRestriction());
            }
        }.bind(this);

        systemEventService.registerEventHandler(RESTRICTION_CREATE_BUTTON_PRESSED_EVENT_ID, this.createButtonEventHandler);


    }.bind(this);
}])

.factory('restrictionManagementSelectModel', ['$q', 'lodash', 'restrictionsFacade', 'cmsitemsRestService', 'catalogService', function(
    $q, lodash,
    restrictionsFacade,
    cmsitemsRestService,
    catalogService
) {

    function RestrictionManagementSelectModel(getRestrictionTypesFn, getSupportedRestrictionTypesFn) {

        var model = {};
        var restrictions;
        var selectedRestriction;
        var supportedRestrictionTypes = [];

        getRestrictionTypesFn().then(function(restrictionTypesResponse) {
            model.restrictionTypes = restrictionTypesResponse;
            var ctr = 0;
            model.restrictionTypes.forEach(function(type) {
                type.id = ctr++;
            });

            if (typeof(getSupportedRestrictionTypesFn) !== 'undefined') {
                getSupportedRestrictionTypesFn().then(function(result) {
                    supportedRestrictionTypes = result;
                });
            } else {
                supportedRestrictionTypes = lodash.map(model.restrictionTypes, 'code');
            }

        }.bind(this));

        this.selectedIds = {
            // restriction
            // restrictionType
        };

        this.getRestrictionsPaged = function(mask, pageSize, currentPage) {

            var requestParams = {
                pageSize: pageSize,
                currentPage: currentPage,
                typeCode: model.selectedRestrictionType.code,
                mask: mask
            };

            return cmsitemsRestService.get(requestParams).then(function(pagedRestrictionsResult) {
                restrictions = pagedRestrictionsResult.response;
                var ctr = 0;
                pagedRestrictionsResult.response.forEach(function(restriction) {
                    restriction.id = ctr++;
                }.bind(this));
                pagedRestrictionsResult.results = pagedRestrictionsResult.response;
                delete pagedRestrictionsResult.response;
                return pagedRestrictionsResult;
            }.bind(this));

        }.bind(this);

        this.getRestrictionFromBackend = function() {
            return {};
        };

        this.getRestrictions = function() {
            if (restrictions) {
                return $q.when(restrictions);
            }
            return restrictionsFacade.getAllRestrictions().then(function(restrictionsResult) {
                restrictions = restrictionsResult.restrictions;
                var ctr = 0;
                restrictions.forEach(function(restriction) {
                    restriction.id = ctr++;
                });
                return $q.when(restrictions);
            }.bind(this));
        }.bind(this);

        this.getRestrictionTypes = function() {
            return $q.when(model.restrictionTypes);
        }.bind(this);

        this.restrictionSelected = function() {
            if (this.selectedIds.restriction || this.selectedIds.restriction === 0) {
                selectedRestriction = restrictions.find(function(restriction) {
                    return restriction.id === this.selectedIds.restriction;
                }.bind(this));
                return true;
            }
            return false;
        }.bind(this);

        this.restrictionTypeSelected = function() {
            delete this.selectedIds.restriction;
            model.selectedRestrictionType = model.restrictionTypes.find(function(restrictionType) {
                return restrictionType.id === this.selectedIds.restrictionType;
            }.bind(this));
            if (model.selectedRestrictionType) {
                selectedRestriction = {
                    typeCode: model.selectedRestrictionType.code
                };
                return true;
            }
            return false;
        }.bind(this);

        this.createRestrictionSelected = function(name, uriContext) {
            selectedRestriction = {
                itemtype: model.selectedRestrictionType.code,
                name: name
            };
            catalogService.getCatalogVersionUUid(uriContext).then(function(catalogVersionUuid) {
                selectedRestriction.catalogVersion = catalogVersionUuid;
            });
        }.bind(this);

        this.getRestrictionTypeCode = function() {
            return model.selectedRestrictionType.code;
        };

        this.getRestriction = function() {
            return selectedRestriction;
        };

        this.isTypeSupported = function() {
            if (model.selectedRestrictionType && model.selectedRestrictionType.code) {
                return supportedRestrictionTypes.indexOf(model.selectedRestrictionType.code) >= 0;
            }
            return false;
        };
    }


    return {
        createRestrictionManagementSelectModel: function(getRestrictionTypesFn, getSupportedRestrictionTypesFn) {
            return new RestrictionManagementSelectModel(getRestrictionTypesFn, getSupportedRestrictionTypesFn);
        }
    };


}])

/**
 * @ngdoc directive
 * @name restrictionManagementSelectModule.directive:restrictionManagementSelect
 * @restrict E
 * @scope
 * @param {< Array=} existingRestrictions Array of existing restrictions, that will be not be selectable.
 * @param {? Function=} isDirtyFn Function returning the dirtiness status of the component.
 * @param {? Function=} submitFn Function defined in outer scope to validate restriction edit.
 * @param {< Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
 * @param {& Expression} getRestrictionTypesFn A function that returns list of restriction types for a given item.
 * @param {< Function=} getSupportedRestrictionTypesFn A function that returns list of restriction types that are supported for editing for a given item. If not provided, all types are assumed to be supported.
 * @description
 * The restrictionManagementSelect Angular component is designed to be able to create or display restrictions.
 */
.component('restrictionManagementSelect', {
    controller: 'RestrictionManagementSelectController',
    templateUrl: 'restrictionManagementSelectTemplate.html',
    bindings: {
        //in
        uriContext: '<',
        existingRestrictions: '<?',
        getRestrictionTypesFn: '&',
        getSupportedRestrictionTypesFn: '<?',
        //out
        submitFn: '=?',
        isDirtyFn: '=?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionPickerModule
 * @requires itemManagementModule
 * @requires recompileDomModule
 * @requires restrictionManagementEditModule
 * @requires restrictionManagementSelectModule
 * @requires yLoDashModule
 * @description
 * This module defines the {@link restrictionPickerModule.directive:restrictionManagement restrictionManagement} component
 **/
angular.module('restrictionPickerModule', [
    'itemManagementModule',
    'recompileDomModule',
    'restrictionManagementEditModule',
    'restrictionManagementSelectModule',
    'yLoDashModule'
])

/**
 * @ngdoc service
 * @name restrictionPickerModule.service:restrictionPickerConfig
 * @requires lodash
 * @description
 * The Generic Editor Modal Service is used to open an editor modal window that contains a tabset.
 */
.service('restrictionPickerConfig', ['lodash', function(lodash) {

    this.MODE_EDITING = 'editing';
    this.MODE_SELECT = 'select';

    /**
     * @ngdoc method
     * @name restrictionPickerModule.service:restrictionPickerConfig#getConfigForEditing
     * @methodOf restrictionPickerModule.service:restrictionPickerConfig
     * @param {Object} existingRestriction The restriction to be edited.
     * @param {String} existingRestriction.uuid The UUID of the restriction to be edited.
     * @param {Number} existingRestriction.index The index of the restriction in restrictions list.
     * @param {Function} getSupportedRestrictionTypesFn A function that returns list of restriction types that are supported for editing for a given item.
     * @returns {Object} A config object to used with the {@link restrictionPickerModule.directive:restrictionManagement restrictionManagement}
     * component in edit mode.
     */
    this.getConfigForEditing = function getConfigForEditing(existingRestriction, getSupportedRestrictionTypesFn) {
        return {
            mode: this.MODE_EDITING,
            restriction: existingRestriction,
            getSupportedRestrictionTypesFn: getSupportedRestrictionTypesFn
        };
    }.bind(this);

    /**
     * @ngdoc method
     * @name restrictionPickerModule.service:restrictionPickerConfig#getConfigForSelecting
     * @methodOf restrictionPickerModule.service:restrictionPickerConfig
     * @param {Array=} existingRestrictions An array of existing restrictions, that will be not be selectable.
     * @param {Function} getRestrictionTypesFn A function that returns list of restriction types for a given item.
     * @param {Function} getSupportedRestrictionTypesFn A function that returns list of restriction types that are supported for editing for a given item.
     * @returns {Object} A config object to used with the {@link restrictionPickerModule.directive:restrictionManagement restrictionManagement}
     * component in select/create mode.
     */
    this.getConfigForSelecting = function getConfigForSelecting(existingRestrictions, getRestrictionTypesFn, getSupportedRestrictionTypesFn) {
        return {
            mode: this.MODE_SELECT,
            getRestrictionTypesFn: getRestrictionTypesFn,
            getSupportedRestrictionTypesFn: getSupportedRestrictionTypesFn,
            existingRestrictions: existingRestrictions
        };
    }.bind(this);

    /**
     * @ngdoc method
     * @name restrictionPickerModule.service:restrictionPickerConfig#isEditingMode
     * @methodOf restrictionPickerModule.service:restrictionPickerConfig
     * @param {Object} config A config to check.
     * @returns {Boolean} True if the config param is a config object created with
     * {@link restrictionPickerModule.service:restrictionPickerConfig#methods_getConfigForEditing getConfigForEditing()}.
     */
    this.isEditingMode = function isEditingMode(config) {
        return config.mode === this.MODE_EDITING;
    }.bind(this);

    /**
     * @ngdoc method
     * @name restrictionPickerModule.service:restrictionPickerConfig#isSelectMode
     * @methodOf restrictionPickerModule.service:restrictionPickerConfig
     * @param {Object} config A config to check.
     * @returns {Boolean} True if the config param is a config object created with
     * {@link restrictionPickerModule.service:restrictionPickerConfig#methods_getConfigForSelecting getConfigForSelecting()}.
     */
    this.isSelectMode = function isSelectMode(config) {
        return config.mode === this.MODE_SELECT;
    }.bind(this);

    /**
     * @ngdoc method
     * @name restrictionPickerModule.service:restrictionPickerConfig#isValidConfig
     * @methodOf restrictionPickerModule.service:restrictionPickerConfig
     * @param {Object} config A config to check.
     * @returns {Boolean} True if the config object was created with proper params.
     */
    this.isValidConfig = function isValidConfig(config) {
        switch (config.mode) {
            case this.MODE_EDITING:
                return lodash.isObject(config.restriction);

            case this.MODE_SELECT:
                if (config.getSupportedRestrictionTypesFn) {
                    return lodash.isFunction(config.getRestrictionTypesFn) && lodash.isFunction(config.getSupportedRestrictionTypesFn);
                } else {
                    return lodash.isFunction(config.getRestrictionTypesFn);
                }
        }
    }.bind(this);

}])

.controller('RestrictionManagementController', ['$q', 'restrictionPickerConfig', function($q, restrictionPickerConfig) {

    this.$onInit = function() {
        this.submitFn = function() {
            return this.submitInternal().then(function(value) {
                return value;
            });
        }.bind(this);
    };

    this.$onChanges = function $onChanges() {
        if (restrictionPickerConfig.isValidConfig(this.config)) {
            this.editMode = restrictionPickerConfig.isEditingMode(this.config);
            this.getSupportedRestrictionTypesFn = this.config.getSupportedRestrictionTypesFn;
            if (this.editMode) {
                this.restriction = this.config.restriction;
            } else {
                this.getRestrictionTypesFn = this.config.getRestrictionTypesFn;
                this.existingRestrictions = this.config.existingRestrictions;
            }
        } else {
            throw "restrictionManagementController - invalid restrictionPickerConfig";
        }
    };
}])

/**
 * @ngdoc directive
 * @name restrictionPickerModule.directive:restrictionManagement
 * @restrict E
 * @param {< Object} Config Object created by {@link restrictionPickerModule.service:restrictionPickerConfig restrictionPickerConfig}.
 * @param {Array=} Config.existingRestrictions Array of existing restrictions, that will be not be selectable.<br /><i>(only on select mode)</i>.
 * @param {String} Config.mode Constant indicating whether the restriction picker is displayed in edit or select mode.
 * @param {String} Config.restrictionId Unique identifier for the processed restriction.
 * @param {< Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
 * @param {= Function=} submitFn A function defined internally. After binding is complete, the caller may execute this.
 * function to trigger the POST/PUT depending on the config. Returns a promise resolving to a restriction object.
 * @param {= Function=} isDirtyFn A function defined internally. After binding is complete, the caller may execute this.
 * function, which return a boolean True if the generic edit use tor represent the restriction is in a dirty state.
 * @description
 * The restrictionManagement Angular component is designed to be able to create new restrictions, editing existing
 * restrictions, or search for restrictions, depending on the config provided.
 */
.component('restrictionManagement', {
    controller: 'RestrictionManagementController',
    templateUrl: 'restrictionManagementTemplate.html',
    bindings: {
        config: '<',
        uriContext: '<',
        submitFn: '=?',
        isDirtyFn: '=?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionsEditorModule
 * @description
 * This module contains the {@link restrictionsEditorModule.restrictionsEditor restrictionsEditor} component.
 */
angular.module('restrictionsEditorModule', [
    'cmssmarteditContainerTemplates',
    "eventServiceModule",
    'restrictionsTableModule',
    'restServiceFactoryModule',
    "restrictionPickerModule",
    "sliderPanelModule",
    "yMessageModule",
    "yLoDashModule",
    'cmsitemsRestServiceModule',
    'restrictionsCriteriaServiceModule',
    'itemManagementModule',
    'functionsModule'
])

.controller('restrictionsEditorController', ['$q', '$log', 'lodash', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'GENERIC_EDITOR_LOADED_EVENT', 'ITEM_MANAGEMENT_EDITOR_ID', 'restServiceFactory', 'systemEventService', 'restrictionPickerConfig', 'cmsitemsRestService', 'restrictionsCriteriaService', 'isBlank', function(
    $q, $log, lodash,
    GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT,
    GENERIC_EDITOR_LOADED_EVENT,
    ITEM_MANAGEMENT_EDITOR_ID,
    restServiceFactory,
    systemEventService,
    restrictionPickerConfig,
    cmsitemsRestService,
    restrictionsCriteriaService, isBlank) {

    this.setSliderConfigForAddOrCreate = function() {
        this.sliderPanelConfiguration.modal.title = "se.cms.restriction.management.panel.title.add";
        this.sliderPanelConfiguration.modal.save.label = "se.cms.restriction.management.panel.button.add";
        this.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
            if (this.restrictionManagement.isDirtyFn) {
                return !this.restrictionManagement.isDirtyFn();
            }
            return true; // disable save until save FN is bound byt restriction management component
        }.bind(this);
        this.sliderPanelConfiguration.modal.save.onClick = function() {
            this.restrictionManagement.submitFn().then(function(restriction) {
                this.restrictions.push(restriction);
                this.sliderPanelHide();
            }.bind(this));
        }.bind(this);
    }.bind(this);

    this.setSliderConfigForEditing = function() {
        this.sliderPanelConfiguration.modal.title = "se.cms.restriction.management.panel.title.edit";
        this.sliderPanelConfiguration.modal.save.label = "se.cms.restriction.management.panel.button.save";
        this.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
            if (this.restrictionManagement.isDirtyFn) {
                return !this.restrictionManagement.isDirtyFn();
            }
            return true; // disable save until save FN is bound byt restriction management component
        }.bind(this);
        this.sliderPanelConfiguration.modal.save.onClick = function() {
            this.restrictionManagement.submitFn().then(function(restrictionEdited) {
                if (this.restrictionManagement.operation) {
                    var payloadRestriction = this.restrictionManagement.operation.restriction;
                    //Copy index back because of the backend returns response without one.
                    restrictionEdited.$restrictionIndex = payloadRestriction.$restrictionIndex;
                }

                var restrictionIndex = restrictionEdited.$restrictionIndex;
                if (restrictionIndex !== -1) {
                    this.restrictions[restrictionIndex] = restrictionEdited;
                } else {
                    throw "restrictionsEditorController - edited restriction not found in list: " + restrictionEdited;
                }
                this.sliderPanelHide();
            }.bind(this));
        }.bind(this);
    }.bind(this);

    this.sliderPanelConfiguration = {
        modal: {
            showDismissButton: true,
            cancel: {
                label: "se.cms.restriction.management.panel.button.cancel",
                onClick: function() {
                    this.sliderPanelHide();
                }.bind(this)
            },
            save: {}
        },
        cssSelector: "#y-modal-dialog"
    };

    this.onClickOnAdd = function() {
        this.setSliderConfigForAddOrCreate();
        this.restrictionManagement.operation = restrictionPickerConfig.getConfigForSelecting(lodash.clone(this.restrictions), this.getRestrictionTypes, this.getSupportedRestrictionTypes);
        this.sliderPanelShow();
    }.bind(this);

    this.onClickOnEdit = function(restriction) {
        this.setSliderConfigForEditing();
        this.restrictionManagement.operation = restrictionPickerConfig.getConfigForEditing(lodash.clone(restriction), this.getSupportedRestrictionTypes);
        this.sliderPanelShow();
    }.bind(this);

    this.matchCriteriaChanged = function(criteriaSelected) {
        this.criteria = criteriaSelected;
        this.matchCriteriaIsDirty = this.criteria !== this.orrigCriteria;
        this.updateRestrictionsData();
    }.bind(this);

    this.setupResults = function(results) {
        this.restrictions = results;
        this.restrictions = this._indexRestrictions(this.restrictions);
        this.oldRestrictions = this._cloneRestrictions(this.restrictions);
        this.originalRestrictions = this._cloneRestrictions(this.restrictions);
        this.updateRestrictionsData();
        this.isRestrictionsReady = true;
    };

    this.updateRestrictionsData = function() {
        if (this.onRestrictionsChanged) {
            this.onRestrictionsChanged({
                $onlyOneRestrictionMustApply: this.criteria.value,
                $restrictions: this.restrictions
            });
        }
    };

    this._prepareRestrictionsCriteria = function() {
        this.criteriaOptions = restrictionsCriteriaService.getRestrictionCriteriaOptions();

        if (!!this.item.onlyOneRestrictionMustApply) {
            this.criteria = this.criteriaOptions[1];
            this.orrigCriteria = this.criteriaOptions[1];
        } else {
            this.criteria = this.criteriaOptions[0];
            this.orrigCriteria = this.criteriaOptions[0];
        }
    };

    this._isRestrictionRelatedError = function(validationError) {
        return lodash.includes(validationError.subject, 'restrictions');
    };

    this._formatRestrictionRelatedError = function(validationError) {
        var cloned = lodash.clone(validationError);
        if (!isBlank(cloned.position)) {
            cloned.position = parseInt(cloned.position);
        }
        if (!isBlank(cloned.subject)) {
            cloned.subject = cloned.subject.split('.').pop();
        }
        return cloned;
    };

    // Restriction Editor can be a part of a generic editor.
    // Whenever generic editor propagates unrelated errors, restriction editor
    // can extract errors related to itself.
    this._handleUnrelatedValidationErrors = function(key, validationData) {
        this.errors = validationData.messages.filter(function(error) {
            return this._isRestrictionRelatedError(error);
        }.bind(this)).map(function(error) {
            return this._formatRestrictionRelatedError(error);
        }.bind(this));
    };

    // Whenever restriction editor opens a form to edit restriction,
    // errors related to this particular restriction are propagated to it.
    this._propagateErrors = function(eventId, genericEditorId) {
        var restrictionInEditMode = this.restrictionManagement.operation &&
            this.restrictionManagement.operation.mode === restrictionPickerConfig.MODE_EDITING &&
            genericEditorId === ITEM_MANAGEMENT_EDITOR_ID;
        if (restrictionInEditMode) {
            var restrictionIndex = this.restrictionManagement.operation.restriction.$restrictionIndex;

            var errorsToPropagate = this.errors.filter(function(error) {
                return error.position === restrictionIndex;
            });

            // Clear and reinitialize events so they do not interfere with GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT
            this._clearEvents();
            systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                messages: errorsToPropagate,
                targetGenericEditorId: genericEditorId
            }).then(function() {
                this._initEvents();
            }.bind(this));
        }
    };

    // Index is provided for each restriction so any restriction without
    // uuid can be easily identified for error propagation
    this._indexRestrictions = function(restrictions) {
        restrictions.forEach(function(element, index) {
            element.$restrictionIndex = index;
        });
        return restrictions;
    };

    this._cloneRestrictions = function(restrictions) {
        return lodash.cloneDeep(restrictions);
    };

    this._initEvents = function() {
        this.unregisterErrorListener = systemEventService.registerEventHandler(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, this._handleUnrelatedValidationErrors.bind(this));
        this.unregisterErrorPropagationEvent = systemEventService.registerEventHandler(GENERIC_EDITOR_LOADED_EVENT, this._propagateErrors.bind(this));
    };

    this._clearEvents = function() {
        this.unregisterErrorListener();
        this.unregisterErrorPropagationEvent();
    };

    this.$onInit = function() {

        this.showRestrictionPicker = false;
        this.restrictions = this.restrictions || [];
        this.originalRestrictions = [];
        this.restrictionsArrayIsDirty = false;
        this.matchCriteriaIsDirty = false;
        this.oldRestrictions = [];
        this.isRestrictionsReady = false;
        this.errors = [];

        //setting restrictions criteria
        this._prepareRestrictionsCriteria();

        //setting restrictions
        this.setupResults(this.restrictions);

        //prepare restriction management module
        this.restrictionManagement = {
            uriContext: this.item.uriContext
        };

        // It is necessary to put this function inside $onInit. Otherwise, the editor is never marked as dirty.
        this.isDirtyFn = function() {
            return this.restrictionsArrayIsDirty || (this.matchCriteriaIsDirty && this.restrictions.length >= 2);
        }.bind(this);

        this.resetFn = function() {
            return true;
        };

        this.cancelFn = function() {
            return $q.when(true);
        };

        this._initEvents();
    };

    this.$onDestroy = function() {
        this._clearEvents();
    };

    this.$doCheck = function() {
        this.restrictionsArrayIsDirty = angular.toJson(this.originalRestrictions) !== angular.toJson(this.restrictions);

        if (angular.toJson(this.oldRestrictions) !== angular.toJson(this.restrictions)) {
            this.restrictions = this._indexRestrictions(this.restrictions);
            this.oldRestrictions = this._cloneRestrictions(this.restrictions);
            this.updateRestrictionsData();
        }
    };
}])

/**
 * @ngdoc directive
 * @name restrictionsEditorModule.restrictionsEditor
 * @restrict E
 * @scope
 * @description
 * The purpose of this directive is to allow the user to manage the restrictions for a given item. The restrictionsEditor has an editable and non-editable mode.
 * It uses the restrictionsTable to display the list of restrictions and it uses the restrictionsPicker to add or remove the restrictions.
 * 
 * @param {= Object} item The object for the item you want to manage restrictions.
 * @param {Boolean} item.onlyOneRestrictionMustApply The restriction criteria for the item.
 * @param {String} item.uuid The uuid of the item. Required if not passing initialRestrictions. Used to fetch and update restrictions for the item.
 * @param {Object} item.uriContext the {@link resourceLocationsModule.object:UriContext uriContext}
 * @param {= Boolean} editable Boolean to determine whether the editor is enabled.
 * @param {< Array=} restrictions An array of initial restriction objects to be loaded in the restrictions editor. If restrictions is not provided, it is used else it is assumed that there are no restrictions.
 * @param {= Function=} resetFn Function that returns true. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {= Function=} cancelFn Function that returns a promise. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {= Function=} isDirtyFn Function that returns a boolean. This function is defined in the restrictionsEditor controller and exists only to provide an external callback.
 * @param {& Expression=} onRestrictionsChanged Function that passes '$onlyOneRestrictionMustApply' boolean and an array of '$restrictions' as arguments. The invoker can bind this to a custom function to fetch these values and perform other operations.
 * @param {& Expression} getRestrictionTypes Function that return list of restriction types. The invoker can bind this to a custom function to fetch a list of restriction types.
 * @param {& Expression=} getSupportedRestrictionTypes Function that returns an arry of supported restriction types. The invoker can bind this to a custom function to fetch these values and perform other operations. If not provide, all types are assumed to be supported.
 */
.component('restrictionsEditor', {
    templateUrl: 'restrictionsEditorTemplate.html',
    controller: 'restrictionsEditorController',
    scope: {},
    bindings: {
        item: '<',
        editable: '=',
        restrictions: '<?',
        resetFn: '=?',
        cancelFn: '=?',
        isDirtyFn: '=?',
        onRestrictionsChanged: '&?',
        getRestrictionTypes: '&',
        getSupportedRestrictionTypes: '&?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name restrictionsTableModule
 * @requires l10nModule
 * @requires restrictionsCriteriaServiceModule
 * @description
 * This module defines the {@link restrictionsTableModule.directive:restrictionsTable restrictionsTable} component
 */
angular.module('restrictionsTableModule', [
    'l10nModule',
    'restrictionsCriteriaServiceModule',
    'yLoDashModule'
])

.controller('restrictionsTableController', ['restrictionsCriteriaService', 'lodash', function(restrictionsCriteriaService, lodash) {
    this.resetRestrictionCriteria = function() {

        if (!this.restrictions || this.restrictions.length < 2) { //default if none is provided or restrictions less than 2
            this.restrictionCriteria = this.criteriaOptions[0];
        }
    }.bind(this);

    this.removeRestriction = function(restriction) {
        var restrictionIndex = this.restrictions.indexOf(restriction);
        this.restrictions.splice(restrictionIndex, 1);
        this._removeUnnecessaryError(restrictionIndex);
        this._modifyErrorPositions(restrictionIndex);
    }.bind(this);

    this._removeUnnecessaryError = function(removedRestrictionIndex) {
        var errorIndex = this.errors.findIndex(function(error) {
            return error.position === removedRestrictionIndex;
        });

        if (errorIndex > -1) {
            this.errors.splice(errorIndex, 1);
        }
    }.bind(this);

    this._modifyErrorPositions = function(removedRestrictionIndex) {
        this.errors.forEach(function(error) {
            if (error.position >= removedRestrictionIndex) {
                error.position = error.position - 1;
            }
        });
    }.bind(this);

    this.editRestriction = function(restriction) {
        this.onClickOnEdit(restriction);
    }.bind(this);


    this.criteriaClicked = function() {
        this.onCriteriaSelected(this.restrictionCriteria);
    };

    this.removeAllRestrictions = function() {
        this.restrictions = [];
    };

    this.showRemoveAllButton = function() {
        return this.restrictions && this.restrictions.length > 0 && this.editable;
    };

    this.isInError = function(index) {
        return !!this.errors && lodash.some(this.errors, function(error) {
            return error.position === index;
        });
    };

    this.$onInit = function() {

        this.criteriaOptions = restrictionsCriteriaService.getRestrictionCriteriaOptions();
        this.resetRestrictionCriteria();

        this.actions = [{
            key: 'se.cms.restrictions.item.edit',
            callback: this.editRestriction
        }, {
            key: 'se.cms.restrictions.item.remove',
            callback: this.removeRestriction
        }];
    };

    this.$doCheck = function() {
        this.resetRestrictionCriteria();
    };

}])

/**
 * @ngdoc directive
 * @name restrictionsTableModule.directive:restrictionsTable
 * @restrict E
 * @scope
 * @param {= String} customClass The name of the CSS class.
 * @param {= Boolean} editable States whether the restrictions table could be modified.
 * @param {< Function=} onClickOnEdit Triggers the custom on edit event.
 * @param {= Function} onCriteriaSelected Function that accepts the selected value of criteria.
 * @param {= Function} onSelect Triggers the custom on select event.
 * @param {= Object} restrictions The object of restrictions.
 * @param {= Object =} restrictionCriteria The object that contains information about criteria.
 * @param {< Array =} errors The list of errors.
 * @description
 * Directive that can render a list of restrictions and provides callback functions such as onSelect and onCriteriaSelected. *
 */
.component('restrictionsTable', {
    templateUrl: 'restrictionsTableTemplate.html',
    controller: 'restrictionsTableController',
    controllerAs: '$ctrl',
    bindings: {
        customClass: '=',
        editable: '=',
        onClickOnEdit: '<?',
        onCriteriaSelected: '=',
        onSelect: '=',
        restrictions: '=',
        restrictionCriteria: '=?',
        errors: '<?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name synchronizeCatalogModule
 * @description
 *
 * The synchronization module contains the service and the directives necessary 
 * to perform catalog synchronization.
 *
 * The {@link synchronizationServiceModule.service:synchronizationService synchronizationService} 
 * calls backend API in order to get synchronization status or trigger a catalog synchronaization.
 *
 * The {@link synchronizationServiceModule.directive:synchronizeCatalog synchronizeCatalog} directive is used to display
 * the synchronization area in the landing page for each store.
 *
 */
angular.module('synchronizeCatalogModule', ['confirmationModalServiceModule', 'synchronizationServiceModule', 'l10nModule', 'hasOperationPermissionModule'])

.controller('synchronizeCatalogController', ['$scope', 'synchronizationService', '$q', 'confirmationModalService', 'l10nFilter', function($scope, synchronizationService, $q, confirmationModalService, l10nFilter) {

    // Constants
    var JOB_STATUS = {
        RUNNING: 'RUNNING',
        ERROR: 'ERROR',
        FAILURE: 'FAILURE',
        FINISHED: 'FINISHED',
        UNKNOWN: 'UNKNOWN'
    };

    this.$onInit = function() {
        this.syncJobStatus = {
            syncStartTime: '',
            syncEndTime: '',
            status: '',
            source: '',
            target: ''
        };
        this.targetCatalogVersion = this.activeCatalogVersion.version;
        this.sourceCatalogVersion = (!this.catalogVersion.active) ? this.catalogVersion.version : null;
        this.syncCatalogPermission = [{
            names: ['se.sync.catalog'],
            context: {
                catalogId: this.catalog.catalogId,
                catalogVersion: this.sourceCatalogVersion
            }
        }];

        // Catalog works as a DTO. Thus it needs the target and source catalog versions. 
        this.catalogDto = {
            catalogId: this.catalog.catalogId,
            targetCatalogVersion: this.targetCatalogVersion,
            sourceCatalogVersion: this.sourceCatalogVersion
        };

        // on init, start auto updating synchronization data
        synchronizationService.startAutoGetSyncData(this.catalogDto, this._updateSyncStatusData.bind(this));

        // call the update for the first time. 
        this._invokeGetSyncData();
    };

    this.$onDestroy = function() {
        synchronizationService.stopAutoGetSyncData(this.catalogDto);
    };

    // Catalog Syncing
    this.syncCatalog = function() {
        var catalogName = l10nFilter(this.catalog.name);
        confirmationModalService.confirm({
            description: 'se.sync.confirm.msg',
            title: 'se.sync.confirmation.title',
            descriptionPlaceholders: {
                catalogName: catalogName
            }
        }).then(function() {
            synchronizationService.updateCatalogSync(this.catalogDto).then(function(response) {
                this._updateSyncStatusData(response);
            }.bind(this));
        }.bind(this));
    };

    // Auto Get
    this._invokeGetSyncData = function() {
        synchronizationService
            .getCatalogSyncStatus(this.catalogDto)
            .then(function(response) {
                this._updateSyncStatusData(response);
            }.bind(this));
    };

    this._updateSyncStatusData = function(response) {
        this.syncJobStatus = {
            syncStartTime: response.creationDate,
            syncEndTime: response.endDate,
            status: response.syncStatus,
            source: (response.sourceCatalogVersion) ? response.sourceCatalogVersion : '',
            target: (response.targetCatalogVersion) ? response.targetCatalogVersion : '',
        };
    };

    // Status 
    this.isSyncJobFinished = function() {
        return this.syncJobStatus.status === JOB_STATUS.FINISHED;
    };

    this.isSyncJobInProgress = function() {
        return (this.syncJobStatus.status === "RUNNING" || this.syncJobStatus.status === "UNKNOWN");
    };

    this.isSyncJobFailed = function() {
        return this.syncJobStatus.status === JOB_STATUS.ERROR || this.syncJobStatus.status === JOB_STATUS.FAILURE;
    };

    this.isButtonEnabled = function() {
        return !this.isSyncJobInProgress();
    };

    this.getSyncFromLabels = function() {
        var returnValue = {
            sourceCatalogVersion: this.syncJobStatus.source
        };
        return returnValue;
    };

}])

/**
 * @ngdoc directive
 * @name synchronizationServiceModule.directive:synchronizeCatalog
 * @restrict E
 * @element synchronize-catalog
 *
 * @description
 * 
 * The synchronize catalog directive is used to display catalog synchronization information in the landing page. 
 * 
 * For the active-catalog version, this directive displays the information of the last sync job affecting that catalog version.
 * 
 * For non-active catalog versions it provides information about the last sync job from that catalog version towards the catalog's active version. 
 * Also, it displays a button to trigger a new synchronization job, going from the current catalog version to the catalog's active version. 
 *
 * @param {Object} catalog An object that contains the catalog details. 
 * @param {Object} catalogVersion An object representing the current catalog version. 
 * @param {Object} activeCatalogVersion An object representing the active catalog version of the provided catalog. 
 * 
 */
.directive('synchronizeCatalog', function() {
    return {
        templateUrl: 'synchronizeCatalogTemplate.html',
        restrict: 'E',
        controller: 'synchronizeCatalogController',
        controllerAs: 'ctrl',
        bindToController: {
            catalog: '<',
            catalogVersion: '<',
            activeCatalogVersion: '<'
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageListSyncIconModule', ['pageSynchronizationServiceModule', 'catalogServiceModule', 'synchronizationConstantsModule', 'crossFrameEventServiceModule'])

.controller('pageListSyncIconController', ['pageSynchronizationService', 'catalogService', 'SYNCHRONIZATION_STATUSES', 'SYNCHRONIZATION_POLLING', 'crossFrameEventService', function(pageSynchronizationService, catalogService, SYNCHRONIZATION_STATUSES, SYNCHRONIZATION_POLLING, crossFrameEventService) {

    this.classes = {};
    this.classes[SYNCHRONIZATION_STATUSES.UNAVAILABLE] = "hyicon-sync se-sync-button__sync__sync-not";
    this.classes[SYNCHRONIZATION_STATUSES.IN_SYNC] = "hyicon-done se-sync-button__sync__done";
    this.classes[SYNCHRONIZATION_STATUSES.NOT_SYNC] = "hyicon-sync se-sync-button__sync__sync-not";
    this.classes[SYNCHRONIZATION_STATUSES.IN_PROGRESS] = "hyicon-sync se-sync-button__sync__sync-not";
    this.classes[SYNCHRONIZATION_STATUSES.SYNC_FAILED] = "hyicon-sync se-sync-button__sync__sync-not";

    this.fetchSyncStatus = function() {
        return pageSynchronizationService.getSyncStatus(this.pageId, this.uriContext).then(function(response) {
            this.syncStatus = response;
        }.bind(this), function() {
            this.syncStatus.status = SYNCHRONIZATION_STATUSES.UNAVAILABLE;
        }.bind(this));
    }.bind(this);


    this.triggerFetch = function(eventId, eventData) {
        if (eventData.itemId === this.pageId) {
            this.fetchSyncStatus();
        }
    };

    this.$onInit = function() {
        catalogService.isContentCatalogVersionNonActive(this.uriContext).then(function(isNonActive) {

            if (isNonActive) {
                // set initial sync status to unavailable
                this.syncStatus = {
                    status: SYNCHRONIZATION_STATUSES.UNAVAILABLE
                };

                this.unRegisterSyncPolling = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, this.triggerFetch.bind(this));

                // the first sync fetch is done manually
                this.fetchSyncStatus();
            }
        }.bind(this));
    };

    this.$onDestroy = function() {
        this.unRegisterSyncPolling();
    };

}])

/**
 * @ngdoc directive
 * @name pageListSyncIconModule.directive:pageListSyncIcon
 * @restrict E
 * @element sync-icon
 *
 * @description
 * The Page Synchronization Icon component is used to display the icon that describes the synchronization status of a page.
 *
 * @param {string} pageId The identifier of the page for which the synchronzation status must be displayed.
 * 
 */
.component('pageListSyncIcon', {
    templateUrl: 'pageListSyncIconTemplate.html',
    controller: 'pageListSyncIconController',
    controllerAs: '$ctrl',
    bindings: {
        pageId: '<',
        uriContext: '<'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageSynchronizationHeaderModule', ['translationServiceModule'])
    .component('pageSynchronizationHeader', {
        templateUrl: 'pageSynchronizationHeaderTemplate.html',
        bindings: {
            lastSyncTime: '<'
        },
        controller: ['$translate', function($translate) {
            $translate('se.cms.synchronization.page.header.help').then(function(translation) {
                this.helpTemplate = "<span>" + translation + "</span>";
            }.bind(this));
        }],
        controllerAs: 'pageSync'

    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageSynchronizationPanelModule', ['functionsModule', 'synchronizationPanelModule', 'pageSynchronizationServiceModule', 'pageSynchronizationHeaderModule', 'componentHandlerServiceModule'])
    .constant("SYNCHRONIZATION_PAGE_SELECT_ALL_SLOTS_LABEL", "se.cms.synchronization.page.select.all.slots")
    .controller('PageSynchronizationPanelController', ['$attrs', 'SYNCHRONIZATION_PAGE_SELECT_ALL_SLOTS_LABEL', 'isBlank', 'pageSynchronizationService', 'componentHandlerService', function($attrs, SYNCHRONIZATION_PAGE_SELECT_ALL_SLOTS_LABEL, isBlank, pageSynchronizationService, componentHandlerService) {


        this.getSyncStatus = function() {
            return pageSynchronizationService.getSyncStatus(this.itemId, this.uriContext).then(function(syncStatus) {
                syncStatus.selectAll = SYNCHRONIZATION_PAGE_SELECT_ALL_SLOTS_LABEL;
                return syncStatus;
            });
        }.bind(this);

        this.performSync = function(array) {
            return pageSynchronizationService.performSync(array, this.uriContext);
        }.bind(this);

        this.headerTemplateUrl = "pageSynchronizationHeaderWrapperTemplate.html";

        this.$postLink = function() {
            this.showSyncButton = isBlank($attrs.syncItems);
            this.itemId = this.itemId || componentHandlerService.getPageUID();
        };

    }])
    .component('pageSynchronizationPanel', {

        templateUrl: 'pageSynchronizationPanelTemplate.html',
        controller: 'PageSynchronizationPanelController',
        controllerAs: 'pageSync',
        bindings: {
            syncItems: '=?',
            itemId: '=?',
            uriContext: '=?',
            onSelectedItemsUpdate: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageSyncMenuToolbarItemModule', ['crossFrameEventServiceModule', 'catalogServiceModule', 'pageSynchronizationPanelModule'])
    .controller('PageSyncMenuToolbarItemController', ['crossFrameEventService', 'catalogService', 'assetsService', 'systemEventService', 'iframeClickDetectionService', 'componentHandlerService', 'pageSynchronizationService', 'SYNCHRONIZATION_STATUSES', 'SYNCHRONIZATION_POLLING', '$scope', function(crossFrameEventService, catalogService, assetsService, systemEventService, iframeClickDetectionService, componentHandlerService, pageSynchronizationService, SYNCHRONIZATION_STATUSES, SYNCHRONIZATION_POLLING, $scope) {

        this.fetchSyncStatus = function() {
            pageSynchronizationService.getSyncStatus(componentHandlerService.getPageUUID()).then(function(syncStatus) {
                this.isNotInSync = syncStatus.status !== SYNCHRONIZATION_STATUSES.IN_SYNC;
            }.bind(this));
        }.bind(this);


        this.$onInit = function() {

            this.isContentCatalogVersionNonActive = false;

            catalogService.isContentCatalogVersionNonActive().then(function(isNonActive) {
                if (isNonActive) {

                    this.icons = {
                        open: assetsService.getAssetsRoot() + "/images/icon_info_white.png",
                        closed: assetsService.getAssetsRoot() + "/images/icon_info_blue.png"
                    };

                    this.menuIcon = this.icons.closed;
                    this.isNotInSync = false;
                    this.isContentCatalogVersionNonActive = true;

                    var unRegisterSyncPolling = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, this.fetchSyncStatus);

                    this.fetchSyncStatus();

                    $scope.$on('$destroy', function() {
                        unRegisterSyncPolling();
                    });

                }
            }.bind(this));
        };

    }])
    .component('pageSyncMenuToolbarItem', {
        templateUrl: 'pageSyncMenuToolbarItemTemplate.html',
        controller: 'PageSyncMenuToolbarItemController',
        controllerAs: '$ctrl',
        bindings: {
            toolbarItem: '<item'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name syncPageModalServiceModule
 * @description
 * # The syncPageModalServiceModule
 *
 * The page synchronization modal service module provides a service that allows opening an synchronization modal for a given page. The synchronization modal contains a 
 * {@link synchronizationPanelModule.component:synchronizationPanel}.
 */
angular.module('syncPageModalServiceModule', ['modalServiceModule', 'pageSynchronizationPanelModule'])
    /**
     * @ngdoc service
     * @name syncPageModalServiceModule.service:syncPageModalService
     *
     * @description
     * Convenience service to open n synchronization modal window for a given page's data.
     *
     */
    .factory('syncPageModalService', ['modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', function(modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES) {

        function SyncModalService() {}

        /**
         * @ngdoc method
         * @name pageEditorModalServiceModule.service:pageEditorModalService#open
         * @methodOf syncPageModalServiceModule.service:syncPageModalService
         *
         * @description
         * Uses the {@link modalService.open modalService} to open a modal.
         *
         * The editor modal is wired with a save and cancel button.
         *
         * The content of the synchronization modal is the {@link synchronizationPanelModule.component:synchronizationPanel}.
         *
         * @param {Object} page The data associated to a page as defined in the platform.
         * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext uriContext}
         *
         * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
         */
        SyncModalService.prototype.open = function(page, uriContext) {

            return modalService.open({
                title: 'se.cms.synchronization.pagelist.modal.title.prefix',
                titleSuffix: 'se.cms.pageeditormodal.editpagetab.title',
                templateInline: '<page-synchronization-panel data-on-selected-items-update="modalController.onSelectedItemsUpdate" data-sync-items="modalController.sync" data-item-id="modalController.itemId" data-uri-context="modalController.uriContext"></page-synchronization-panel>',
                controller: ['syncPageModalService', 'modalManager', '$log',
                    function(syncPageModalService, modalManager, $log) {

                        this.showSyncButton = false;
                        this.modalManager = modalManager;
                        this.itemId = page.uuid;

                        this.onSelectedItemsUpdate = function(selectedItems) {
                            if (selectedItems.length === 0) {
                                modalManager.disableButton('sync');
                            } else {
                                modalManager.enableButton('sync');
                            }
                        };

                        this.uriContext = uriContext;
                        this.init = function() {
                            modalManager.setDismissCallback(this.onCancel);

                            modalManager.setButtonHandler(function(buttonId) {
                                if (buttonId !== 'close') {
                                    switch (buttonId) {
                                        case 'sync':
                                            return this.sync();
                                        default:
                                            $log.error('A button callback has not been registered for button with id', buttonId);
                                            break;
                                    }
                                }
                            }.bind(this));
                        };
                    }
                ],
                buttons: [{
                    id: 'close',
                    label: 'se.cms.component.confirmation.modal.close',
                    style: MODAL_BUTTON_STYLES.SECONDARY,
                    action: MODAL_BUTTON_ACTIONS.DISMISS
                }, {
                    id: 'sync',
                    label: 'se.cms.pagelist.dropdown.sync',
                    action: MODAL_BUTTON_ACTIONS.NONE,
                    disabled: true
                }]
            });
        };

        return new SyncModalService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name catalogVersionRestServiceModule
 * @description
 * # The catalogVersionRestServiceModule
 *
 * The catalogVersionRestServiceModule provides REST services for the CMS catalog version endpoint
 *
 */
angular.module('catalogVersionRestServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule'])

/**
 * @ngdoc service
 * @name catalogVersionRestServiceModule.service:catalogVersionRestService
 *
 * @description
 * The catalogVersionRestService provides core REST functionality for the CMS catalog version endpoint
 */
.factory('catalogVersionRestService', ['restServiceFactory', 'CONTEXT_SITE_ID', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', function(restServiceFactory, CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION) {

    var URI = '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION;

    return {

        /**
         * @ngdoc method
         * @name catalogVersionRestServiceModule.service:catalogVersionRestService#get
         * @methodOf catalogVersionRestServiceModule.service:catalogVersionRestService
         *
         * @description
         * Fetches catalog information for a given site+catalog+catalogversion
         *
         * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
         *
         * @returns {Object} A JSON object representing the current catalog version
         */
        get: function(uriContext) {
            var rest = restServiceFactory.get(URI);
            return rest.get(uriContext);
        },

        /**
         * @ngdoc method
         * @name catalogVersionRestServiceModule.service:catalogVersionRestService#getCloneableTargets
         * @methodOf catalogVersionRestServiceModule.service:catalogVersionRestService
         *
         * @description
         * Fetches all cloneable target catalog versions for a given site+catalog+catalogversion
         *
         * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
         *
         * @returns {Object} A JSON object with a single field 'versions' containing a list of catalog versions, or an empty list.
         */
        getCloneableTargets: function(uriContext) {
            var rest = restServiceFactory.get(URI + '/targets?mode=cloneableTo');
            return rest.get(uriContext);
        }
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name createPageServiceModule
 * @description
 * # The createPageServiceModule
 *
 * The create page service module provides a service that allows creating new pages.
 *
 */
angular.module('createPageServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule'])

/**
 * @ngdoc service
 * @name createPageServiceModule.service:createPageService
 *
 * @description
 * The createPageService allows creating new pages.
 *
 */
.factory('createPageService', ['restServiceFactory', 'PAGES_LIST_RESOURCE_URI', function(restServiceFactory, PAGES_LIST_RESOURCE_URI) {
    var pageRestService = restServiceFactory.get(PAGES_LIST_RESOURCE_URI);

    return {
        /**
         * @ngdoc method
         * @name createPageServiceModule.service:createPageService#createPage
         * @methodOf createPageServiceModule.service:createPageService
         *
         * @description
         * When called this service creates a new page based on the information provided.
         *
         * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
         * @param {String} page The payload containing the information necessary to create the new page.
         * NOTE: The payload must at least provide the following fields.
         * - type: The type of the page.
         * - typeCode: The type code of the page.
         * - template: The uid of the page template to use.
         *
         * @returns {Promise} A promise that will resolve after saving the page in the backend.
         */
        createPage: function(uriContext, page) {
            var payload = angular.extend({}, page, uriContext);
            return pageRestService.save(payload);
        }
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageRestrictionsRestServiceModule
 * @requires restServiceFactoryModule
 * @description
 * This module defines the {@link pageRestrictionsRestServiceModule.service:pageRestrictionsRestService pageRestrictionsRestService} REST service for page restrictions API.
 */
angular.module('pageRestrictionsRestServiceModule', [
    'restServiceFactoryModule'
])

/**
 * @ngdoc service
 * @name pageRestrictionsRestServiceModule.service:pageRestrictionsRestService
 * @requires CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI
 * @requires PAGES_RESTRICTIONS_RESOURCE_URI
 * @requires restServiceFactory
 * @requires UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI
 * @description 
 * Service that handles REST requests for the pagesRestrictions CMS API endpoint.
 */
.service('pageRestrictionsRestService', ['CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI', 'PAGES_RESTRICTIONS_RESOURCE_URI', 'restServiceFactory', 'UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI', function(
    CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI,
    PAGES_RESTRICTIONS_RESOURCE_URI,
    restServiceFactory,
    UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI
) {

    var contextualPageRestrictionsRestService = restServiceFactory.get(CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI);
    var pageRestrictionsRestService = restServiceFactory.get(PAGES_RESTRICTIONS_RESOURCE_URI);

    var updatePageRestrictionsRestService = restServiceFactory.get(UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI, "pageid");

    /**
     * @ngdoc method
     * @name pageRestrictionsRestServiceModule.service:pageRestrictionsRestService#update
     * @methodOf pageRestrictionsRestServiceModule.service:pageRestrictionsRestService
     * @param {Object} payload The pageRestriction object containing the pageRestrictionList.
     * @return {Array} An array of pageRestrictions.
     * @deprecated since 6.5
     * 
     * @description
     * It will do a PUT to the pageRestrictions endpoint.
     */
    this.update = function(payload) {
        return updatePageRestrictionsRestService.update(payload);
    };

    /**
     * @ngdoc method
     * @name  pageRestrictionsRestServiceModule.service:pageRestrictionsRestService#getPagesRestrictionsForPageId
     * @methodOf pageRestrictionsRestServiceModule.service:pageRestrictionsRestService
     * @param {String} pageId The unique page identifier for which to fetch pages-restrictions relation.
     * @return {Array} An array of pageRestrictions for the given page.
     */
    this.getPagesRestrictionsForPageId = function(pageId) {
        return contextualPageRestrictionsRestService.get({
            pageId: pageId
        });
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsRestServiceModule.service:pageRestrictionsRestService#getPagesRestrictionsForCatalogVersion
     * @methodOf pageRestrictionsRestServiceModule.service:pageRestrictionsRestService
     * @param {String} siteUID The unique identifier for site.
     * @param {String} catalogUID The unique identifier for catalog.
     * @param {String} catalogVersionUID The unique identifier for catalog version.
     * @return {Array} An array of all pageRestrictions for the given catalog.
     */
    this.getPagesRestrictionsForCatalogVersion = function(siteUID, catalogUID, catalogVersionUID) {
        return pageRestrictionsRestService.get({
            siteUID: siteUID,
            catalogId: catalogUID,
            catalogVersion: catalogVersionUID
        });
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pageSynchronizationServiceModule', ['synchronizationPollingServiceModule'])
    .service('pageSynchronizationService', ['syncPollingService', function(syncPollingService) {

        this.getSyncStatus = function(itemId, uriContext) {
            return syncPollingService.getSyncStatus(itemId, uriContext).then(function(syncStatus) {
                return syncStatus;
            });
        };

        this.performSync = function(array, uriContext) {
            return syncPollingService.performSync(array, uriContext);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageTypeServiceModule
 *
 * @description
 * The Page Type Service module provides a service that retrieves all supported page types
 *
 */
angular.module('pageTypeServiceModule', ['restServiceFactoryModule'])

/**
 * @ngdoc service
 * @name pageTypeServiceModule.pageTypeService
 *
 * @description
 * Service that concerns business logic tasks related to CMS page types in the SAP Hybris platform.
 * This service retrieves all supported page types configured on the platform, and caches them for the duration of the session.
 */
.service('pageTypeService', ['restServiceFactory', 'PAGE_TYPES_URI', function(restServiceFactory, PAGE_TYPES_URI) {
    var pageTypeRestService = restServiceFactory.get(PAGE_TYPES_URI);
    var pageTypes;

    /**
     * @ngdoc method
     * @name pageTypeServiceModule.pageTypeService.getPageTypes
     * @methodOf pageTypeServiceModule.pageTypeService
     *
     * @description
     * Returns a list of page type descriptor objects. The page type descriptor object
     * returned is structured as follows:
     *
     * ```js
     *  {
     *      code {String} The unique identifier for the page type.
     *      name {Object} A map between language ISO code and localized name of the page type.
     *      description {Object} A map between language ISO code and localized description of the page type.
     *  };
     * ```
     *
     * @returns {Array} An array of page type objects
     */
    this.getPageTypes = function() {
        pageTypes = pageTypes || pageTypeRestService.get();
        return pageTypes.then(function(pageTypesResponse) {
            return pageTypesResponse.pageTypes;
        });
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageTypesRestrictionTypesRestServiceModule
 * @requires restServiceFactoryModule
 * @description
 * This module defines the {@link pageRestrictionsRestServiceModule.service:pageRestrictionsRestService pageRestrictionsRestService} REST service for pageTypes restrictionTypes API.
 */
angular.module('pageTypesRestrictionTypesRestServiceModule', [
    'restServiceFactoryModule'
])


/**
 * @ngdoc service
 * @name pageTypesRestrictionTypesRestServiceModule.service:pageTypesRestrictionTypesRestService
 * @requires languageService
 * @requires PAGE_TYPES_RESTRICTION_TYPES_URI
 * @requires restServiceFactory
 * @description
 * Service that handles REST requests for the pageTypes restrictionTypes CMS API endpoint.
 */
.service('pageTypesRestrictionTypesRestService', ['languageService', 'PAGE_TYPES_RESTRICTION_TYPES_URI', 'restServiceFactory', function(
    languageService,
    PAGE_TYPES_RESTRICTION_TYPES_URI,
    restServiceFactory
) {

    var rest = restServiceFactory.get(PAGE_TYPES_RESTRICTION_TYPES_URI);

    /**
     * @ngdoc method
     * @name pageTypesRestrictionTypesRestServiceModule.service:pageTypesRestrictionTypesRestService#getPageTypesRestrictionTypes
     * @methodOf pageTypesRestrictionTypesRestServiceModule.service:pageTypesRestrictionTypesRestService
     * 
     * @return {Array} An array of all pageType-restrictionType in the system.
     */
    this.getPageTypesRestrictionTypes = function() {
        return rest.get().then(function(pageTypesRestrictionTypesArray) {
            return pageTypesRestrictionTypesArray;
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pagesFallbacksRestServiceModule', ['resourceLocationsModule', 'restServiceFactoryModule', 'yLoDashModule'])
    .service('pagesFallbacksRestService', ['restServiceFactory', 'lodash', 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION', function(restServiceFactory, lodash, PAGE_CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION) {
        var PAGE_FALLBACKS_URI = '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pages/:pageId/fallbacks';

        this.getFallbacksForPageId = function(pageId, params) {
            this._resource = this._resource || restServiceFactory.get(PAGE_FALLBACKS_URI);
            var extendedParams = lodash.assign({
                pageId: pageId
            }, params || {});

            return this._resource.get(extendedParams).then(function(response) {
                return response.uids;
            });
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pagesRestServiceModule
 * @description
 * # The pagesRestServiceModule
 *
 * The pagesRestServiceModule provides REST services for the CMS pages rest endpoint
 *
 */
angular.module('pagesRestServiceModule', ['restServiceFactoryModule', 'yLoDashModule', 'resourceLocationsModule', 'functionsModule'])

/**
 * @ngdoc service
 * @name pagesRestServiceModule.service:pagesRestService
 *
 * @description
 * The pagesRestService provides core REST functionality for the CMS pages rest endpoint
 */
.service('pagesRestService', ['restServiceFactory', 'lodash', 'URIBuilder', 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION', 'CONTEXT_SITE_ID', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', function(restServiceFactory, lodash, URIBuilder, PAGE_CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION, CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION) {
    var URI = '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pages/:pageUid';

    /**
     * @ngdoc method
     * @name pagesRestServiceModule.service:pagesRestService#get
     * @methodOf pagesRestServiceModule.service:pagesRestService
     *
     * @description
     * Fetches a list of pages for a given site, catalog, and catalog version. If the site, catalog, or catalog version
     * is not defined, those used contextually in the session will be used instead.
     *
     * @param {Object} params A JSON object containing catalog context and/or any additional request parameters
     * @param {String} params.siteUID A side ID
     * @param {String} params.catalogId A catalog ID
     * @param {String} params.catalogVersion A catalog version ID
     *
     * Example:
     * ```
     * {
     *      siteUID: 'supershoes',
     *      catalogId: 'shoes',
     *      catalogVersion: 'online',
     *      anOptionalQueryParamName: 'paramValue'
     * }
     * ```
     *
     * @returns {Promise<Array>} A promise resolving to a list of pages, or an empty list.
     */
    this.get = function(params) {
        var _params = lodash.cloneDeep(params);
        var uri = new URIBuilder(URI).replaceParams(_params).build();
        delete _params[CONTEXT_SITE_ID];
        delete _params[CONTEXT_CATALOG];
        delete _params[CONTEXT_CATALOG_VERSION];
        delete _params[PAGE_CONTEXT_SITE_ID];
        delete _params[PAGE_CONTEXT_CATALOG];
        delete _params[PAGE_CONTEXT_CATALOG_VERSION];
        return restServiceFactory.get(uri, 'pageUid').get(_params).then(function(response) {
            return response.pages;
        });
    };

    /**
     * @ngdoc method
     * @name pagesRestServiceModule.service:pagesRestService#getById
     * @methodOf pagesRestServiceModule.service:pagesRestService
     *
     * @description
     * Fetches a page for a given site, catalog, and catalog version. If the site, catalog, or catalog version is not
     * defined, those used contextually in the session will be used instead.
     *
     * @param {String} pageUid A page UID of the page to fetch
     * @param {Object} params A JSON object containing catalog context and/or any additional request parameters
     * @param {String} params.siteUID A side ID
     * @param {String} params.catalogId A catalog ID
     * @param {String} params.catalogVersion A catalog version ID
     *
     * Example:
     * ```
     * {
     *      siteUID: 'supershoes',
     *      catalogId: 'shoes',
     *      catalogVersion: 'online',
     *      anOptionalQueryParamName: 'paramValue'
     * }
     * ```
     *
     * @returns {Promise<Object>} A promise that resolves to a JSON object representing the page.
     */
    this.getById = function(pageUid, params) {
        var uri = new URIBuilder(URI).replaceParams(params).build();
        var extendedParams = lodash.assign({
            pageUid: pageUid
        }, params || {});
        delete extendedParams[PAGE_CONTEXT_SITE_ID];
        delete extendedParams[PAGE_CONTEXT_CATALOG];
        delete extendedParams[PAGE_CONTEXT_CATALOG_VERSION];
        return restServiceFactory.get(uri, 'pageUid').get(extendedParams);
    };

    /**
     * @ngdoc method
     * @name pagesRestServiceModule.service:pagesRestService#update
     * @methodOf pagesRestServiceModule.service:pagesRestService
     *
     * @description
     * Updates a page for a given site, catalog, and catalog version. If the site, catalog, or catalog version is not
     * defined, those used contextually in the session will be used instead.
     *
     * @param {String} pageUid The page UID of the page to update
     * @param {Object} payload The page object to be applied to the page resource as it exists on the backend
     *
     * @returns {Promise<Object>} A promise that resolves to a JSON object representing the updated page.
     */
    this.update = function(pageUid, payload) {
        var uri = new URIBuilder(URI).replaceParams(payload).build();
        var extendedParams = lodash.assign({
            pageUid: pageUid
        }, payload || {});
        delete extendedParams[PAGE_CONTEXT_SITE_ID];
        delete extendedParams[PAGE_CONTEXT_CATALOG];
        delete extendedParams[PAGE_CONTEXT_CATALOG_VERSION];
        return restServiceFactory.get(uri, 'pageUid').update(extendedParams);
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('pagesVariationsRestServiceModule', [
    'resourceLocationsModule',
    'restServiceFactoryModule',
    'yLoDashModule'
])

.service('pagesVariationsRestService', ['restServiceFactory', 'lodash', 'CONTEXT_SITE_ID', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', function(restServiceFactory, lodash, CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION) {

    this._uri = '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID +
        '/catalogs/' + CONTEXT_CATALOG +
        '/versions/' + CONTEXT_CATALOG_VERSION +
        '/pages/:pageId/variations';

    this._resource = restServiceFactory.get(this._uri);

    this.getVariationsForPrimaryPageId = function(pageId, params) {

        var extendedParams = lodash.assign({
            pageId: pageId
        }, params || {});

        return this._resource.get(extendedParams).then(function(response) {
            return response.uids;
        });
    }.bind(this);
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionTypesRestServiceModule
 * @requires restServiceFactoryModule
 * @description
 * This module contains the {@link restrictionTypesRestServiceModule.service:restrictionTypesRestService restrictionTypesRestService} REST service for restrictionTypes API.
 */
angular.module('restrictionTypesRestServiceModule', [
    'restServiceFactoryModule'
])


/**
 * @ngdoc service
 * @name restrictionTypesRestServiceModule.service:restrictionTypesRestService
 * @requires languageService
 * @requires RESTRICTION_TYPES_URI
 * @requires restServiceFactory
 * @description
 * Service that handles REST requests for the restrictionTypes CMS API endpoint.
 */
.service('restrictionTypesRestService', ['languageService', 'RESTRICTION_TYPES_URI', 'restServiceFactory', function(
    languageService,
    RESTRICTION_TYPES_URI,
    restServiceFactory
) {

    var restrictionTypesRestService = restServiceFactory.get(RESTRICTION_TYPES_URI);

    /**
     * @ngdoc method
     * @name restrictionTypesRestServiceModule.service:restrictionTypesRestService#getRestrictionTypes
     * @methodOf restrictionTypesRestServiceModule.service:restrictionTypesRestService
     * @returns {Array} An array of all restriction types in the system.
     */
    this.getRestrictionTypes = function() {
        return restrictionTypesRestService.get().then(function(restrictionTypesResponse) {
            return restrictionTypesResponse;
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionsRestServiceModule
 * @requires functionsModule
 * @requires restServiceFactoryModule
 * @description
 * This module defines the {@link restrictionsRestServiceModule.service:restrictionsRestService restrictionsRestService} REST service for restrictions API.
 */

angular.module('restrictionsRestServiceModule', [
    'functionsModule',
    'restServiceFactoryModule'
])

/**
 * @ngdoc service
 * @name restrictionsRestServiceModule.service:restrictionsRestService
 * @requires languageService
 * @requires RESTRICTIONS_RESOURCE_URI
 * @requires restServiceFactory
 * @requires URIBuilder
 * @description
 * Service that handles REST requests for the restrictions CMS API endpoint.
 */
.service('restrictionsRestService', ['languageService', 'RESTRICTIONS_RESOURCE_URI', 'restServiceFactory', 'URIBuilder', function(
    languageService,
    RESTRICTIONS_RESOURCE_URI,
    restServiceFactory,
    URIBuilder
) {

    var restrictionsRestService = restServiceFactory.get(RESTRICTIONS_RESOURCE_URI);

    /**
     * @ngdoc method
     * @name restrictionsRestServiceModule.service:restrictionsRestService#get
     * @methodOf restrictionsRestServiceModule.service:restrictionsRestService
     * @param {Object} params Object containing parameters passed to the method.
     * @return {Array} An array of all restrictions in the system.
     */
    this.get = function(params) {
        return restrictionsRestService.get(params);
    };

    /**
     * @ngdoc method
     * @name restrictionsRestServiceModule.service:restrictionsRestService#getById
     * @methodOf restrictionsRestServiceModule.service:restrictionsRestService
     * @param {String} restrictionId Identifier for a given restriction.
     * @return {Object} The restriction matching the identifier passed as parameter.
     */
    this.getById = function getById(restrictionId) {
        return restrictionsRestService.getById(restrictionId).then(function(restriction) {
            return restriction;
        });
    };

    /**
     * @ngdoc method
     * @name restrictionsRestServiceModule.service:restrictionsRestService#getContentApiUri
     * @methodOf restrictionsRestServiceModule.service:restrictionsRestService
     * @param {Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
     * @return {String} A URI for CRUD of restrictions on a specific site/catalog/version
     */
    this.getContentApiUri = function(uriContext) {
        return new URIBuilder(RESTRICTIONS_RESOURCE_URI).replaceParams(uriContext).build();
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('structuresRestServiceModule', ['yLoDashModule', 'operationContextServiceModule', 'resourceLocationsModule'])

.service('structureModeManagerFactory', ['lodash', function(lodash) {

    function ModeManager(supportedModes) {

        if (!lodash.isArray(supportedModes)) {
            throw "ModeManager initialization error: supportedModes must be an array of strings";
        }

        var modes = supportedModes;

        this.getSupportedModes = function getSupportedModes() {
            return lodash.clone(modes);
        };
    }

    ModeManager.prototype.isModeSupported = function isModeSupported(mode) {
        return this.getSupportedModes().indexOf(mode) !== -1;
    };

    ModeManager.prototype.validateMode = function validateMode(mode) {
        if (this.getSupportedModes().indexOf(mode) === -1) {
            throw "ModeManager.validateMode() - mode [" + mode + "] not in list of supported modes: " + this.getSupportedModes();
        }
        return true;
    };

    return {
        createModeManager: function(modes) {
            return new ModeManager(modes);
        }
    };
}])

.service('structuresRestService', ['operationContextService', 'OPERATION_CONTEXT', 'TYPES_RESOURCE_URI', function(operationContextService, OPERATION_CONTEXT, TYPES_RESOURCE_URI) {

    var URI = TYPES_RESOURCE_URI + '/';
    var TYPE_PLACEHOLDER = ':smarteditComponentType';

    operationContextService.register(URI, OPERATION_CONTEXT.CMS);

    this.getUriForContext = function getUriForContext(mode, type) {
        var uri = TYPES_RESOURCE_URI;

        if (mode) {
            uri = uri + '?code=' + (type ? type : TYPE_PLACEHOLDER) + '&mode=' + mode.toUpperCase();
        } else {
            uri = uri + '/' + (type ? type : TYPE_PLACEHOLDER);
        }
        return uri;
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name typeStructureRestServiceModule
 * @description
 * # The typeStructureRestServiceModule
 *
 * The typeStructureRestServiceModule provides REST services for the CMS page structure
 *
 */
angular.module('typeStructureRestServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule'])

/**
 * @ngdoc service
 * @name typeStructureRestServiceModule.service:typeStructureRestService
 *
 * @description
 * The typeStructureRestService provides functionality for fetching page structures
 */
.service('typeStructureRestService', ['TYPES_RESOURCE_URI', 'restServiceFactory', function(TYPES_RESOURCE_URI, restServiceFactory) {

    var structureRestService = restServiceFactory.get(TYPES_RESOURCE_URI);

    /**
     * @ngdoc method
     * @name typeStructureRestServiceModule.service:typeStructureRestService#getStructureByType
     * @methodOf typeStructureRestServiceModule.service:typeStructureRestService
     *
     * @description
     * Fetches the type structure (fields) for CMS pages for a give type
     *
     * @param {String} typeCode The type code of type structure to be fetched
     * @param {Object} [options={}] an optional object to control which part of the structure is being returned
     * @returns {Array} An array of fields, representing the type structure for the generic editor
     */
    this.getStructureByType = function(typeCode, options) {
        var opts = (options) ? options : {};

        return structureRestService.getById(typeCode).then(function(structure) {
            return (opts.getWholeStructure) ? structure : structure.attributes;
        });
    };

    /**
     * @ngdoc method
     * @name typeStructureRestServiceModule.service:typeStructureRestService#getStructureByType
     * @methodOf typeStructureRestServiceModule.service:typeStructureRestService
     *
     * @description
     * Fetches the type structure (fields) for CMS pages for a give type
     *
     * @param {String} typeCode The type code of type structure to be fetched
     * @param {String} mode The mode to fetch the structure
     * @param {Object} [options={}] an optional object to control which part of the structure is being returned
     * @returns {Array} An array of fields, representing the type structure for the generic editor
     */
    this.getStructureByTypeAndMode = function(typeCode, mode, options) {

        var structureByModeRestService = restServiceFactory.get(TYPES_RESOURCE_URI + '?code=' + typeCode + '?mode=' + mode);

        var opts = (options) ? options : {};

        return structureByModeRestService.get().then(function(structure) {
            return (opts.getWholeStructure) ? structure : structure.attributes;
        });
    };

    /**
     * @ngdoc method
     * @name typeStructureRestServiceModule.service:typeStructureRestService#getStructuresByCategory
     * @methodOf typeStructureRestServiceModule.service:typeStructureRestService
     *
     * @param {String} category The componentType category of structures you wish to retrieve.
     * Ex: 'RESTRICTION', or 'COMPONENT'
     *
     * @returns {Array} An array of supported structures supported in this category.
     */
    this.getStructuresByCategory = function(category) {
        return structureRestService.get({
            category: category
        }).then(function(result) {
            return result.componentTypes;
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name displayConditionsFacadeModule
 * @description
 * 
 * This module provides a facade module for page display conditions.
 * 
 */
angular.module('displayConditionsFacadeModule', ['pageServiceModule', 'pageRestrictionsServiceModule', 'pageDisplayConditionsServiceModule'])

/**
 * @ngdoc service
 * @name displayConditionsFacadeModule.service:displayConditionsFacade
 * @description
 * 
 * Service defined to retrieve information related to the display conditions of a given page: 
 * - Whether the page is either of 'primary' or 'variation' display type.
 * - The name of the primary page associated to a variation one.
 * - The name of the display type of a given page ("primary" or "variant").
 * - The description of the display type of a given page ("primary" or "variant").
 * 
 */
.service('displayConditionsFacade', ['pageService', 'pageRestrictionsService', 'pageDisplayConditionsService', '$q', function(pageService, pageRestrictionsService, pageDisplayConditionsService, $q) {

    this.getPageInfoForPageUid = function(pageId) {
        var pagePromise = pageService.getPageById(pageId);
        var displayConditionsPromise = pageService.isPagePrimary(pageId);

        var allPromises = $q.all([pagePromise, displayConditionsPromise]);
        return allPromises.then(function(values) {
            return {
                pageName: values[0].name,
                pageType: values[0].typeCode,
                isPrimary: values[1]
            };
        });
    };

    this.getVariationsForPageUid = function(primaryPageId) {
        return pageService.getVariationPages(primaryPageId).then(function(variationPages) {
            if (variationPages.length === 0) {
                return $q.when([]);
            }

            var restrictionsCountsPromise = $q.all(variationPages.map(function(variationPage) {
                return pageRestrictionsService.getPageRestrictionsCountForPageUID(variationPage.uid);
            }));

            return restrictionsCountsPromise.then(function(restrictionCounts) {
                return variationPages.map(function(variationPage, index) {
                    return {
                        pageName: variationPage.name,
                        creationDate: variationPage.creationtime,
                        restrictions: restrictionCounts[index]
                    };
                });
            });
        });
    };

    this.getPrimaryPagesForVariationPageType = function(variationPageType) {
        return pageService.getPrimaryPagesForPageType(variationPageType).then(function(primaryPages) {
            return primaryPages.map(function(primaryPage) {
                return {
                    uid: primaryPage.uid,
                    uuid: primaryPage.uuid,
                    name: primaryPage.name,
                    label: primaryPage.label
                };
            });
        });
    };

    this.updatePage = function(pageId, pageData) {
        return pageService.updatePageById(pageId, pageData);
    };

    /**
     * @ngdoc method
     * @name displayConditionsFacadeModule.service:displayConditionsFacade#isPrimaryPage
     * @methodOf displayConditionsFacadeModule.service:displayConditionsFacade
     *
     * @description
     * Check whether the tested page is of type 'primary'.
     *
     * @param {String} The identifier of the tested page
     * @return {Promise} Promise resolving in a boolean indicated whether the tested page is of type 'primary'
     */
    this.isPagePrimary = function(pageId) {
        return pageService.isPagePrimary(pageId);
    };

    /**
     * @ngdoc method
     * @name displayConditionsFacadeModule.service:displayConditionsFacade#getPrimaryPageForVariationPage
     * @methodOf displayConditionsFacadeModule.service:displayConditionsFacade
     *
     * @description
     * Returns data related to the 'primary' page associated with the tested 'variation' page.
     *
     * @param {String} The identifier of the tested 'variation' page
     * @return {Promise} Promise resolving in an object containing uid, name and label of the associated primary page
     */
    this.getPrimaryPageForVariationPage = function(variationPageId) {
        return pageService.getPrimaryPage(variationPageId).then(function(primaryPage) {
            return {
                uid: primaryPage.uid,
                name: primaryPage.name,
                label: primaryPage.label
            };
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageFacadeModule
 * @description
 * 
 * This module provides a facade module for pages
 */
angular.module('pageFacadeModule', ['yLoDashModule', 'cmsitemsRestServiceModule', 'sharedDataServiceModule', 'urlServiceModule', 'restServiceFactoryModule', 'catalogServiceModule'])

/**
 * @ngdoc service
 * @name pageFacadeModule.service:pageFacade
 * @requires cmsitemsRestService
 * @description
 * A facade that exposes only the business logic necessary for features that need to work with pages
 */
.service('pageFacade', ['lodash', 'cmsitemsRestService', 'sharedDataService', 'urlService', 'restServiceFactory', 'catalogService', 'cmsitemsUri', function(lodash, cmsitemsRestService, sharedDataService, urlService, restServiceFactory, catalogService, cmsitemsUri) {

    /**
     * @ngdoc method
     * @name pageFacadeModule.service:pageFacade#contentPageWithLabelExists
     * @methodOf pageFacadeModule.service:pageFacade
     *
     * @description
     * Determines if a ContentPage with a given label exists in the given catalog and catalog version
     *
     * @param {String} label The label to search for
     * @param {String} catalogId The catalog ID to search in for the ContentPage
     * @param {String} catalogVersion The catalog version to search in for the ContentPage
     * @return {Promise} Promise resolving to a boolean determining if the ContentPage exists
     */
    this.contentPageWithLabelExists = function(label, catalogId, catalogVersion) {
        var requestParams = {
            pageSize: 10,
            currentPage: 0,
            typeCode: 'ContentPage',
            itemSearchParams: 'label:' + label,
            catalogId: catalogId,
            catalogVersion: catalogVersion
        };

        return cmsitemsRestService.get(requestParams).then(function(result) {
            return result && !lodash.isEmpty(result.response);
        }.bind(this));
    };

    /**
     * @ngdoc method
     * @name pageFacadeModule.service:pageFacade#retrievePageUriContext
     * @methodOf pageFacadeModule.service:pageFacade
     *
     * @description
     * Retrieves the experience and builds a uri context based on its page context
     *
     * @return {Object} the page uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
     */
    this.retrievePageUriContext = function() {
        return sharedDataService.get('experience').then(function(experience) {
            if (!experience) {
                throw "pageFacade could not retrieve an experience from sharedDataService";
            }
            return urlService.buildUriContext(experience.pageContext.siteId, experience.pageContext.catalogId, experience.pageContext.catalogVersion);
        });
    };

    /**
     * @ngdoc method
     * @name pageFacadeModule.service:pageFacade#createPage
     * @methodOf pageFacadeModule.service:pageFacade
     *
     * @description
     * Creates a new CMS page item
     *
     * @param {Object} page) The object representing the CMS page item to create
     * @return {Promise} If request is successful, it returns a promise that resolves with the CMS page item object. If
     * the request fails, it resolves with errors from the backend.
     */
    this.createPage = function(page) {
        var uri = page.siteId ? '/cmswebservices/v1/sites/' + page.siteId + '/cmsitems' : cmsitemsUri;

        return catalogService.getCatalogVersionUUid().then(function(catalogVersionUUid) {
            page.catalogVersion = page.catalogVersion || catalogVersionUUid;

            if (page.onlyOneRestrictionMustApply === undefined) {
                page.onlyOneRestrictionMustApply = false;
            }

            return restServiceFactory.get(uri).save(page);
        });
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageRestrictionsModule
 * @requires pageRestrictionsCriteriaModule
 * @requires pageRestrictionsServiceModule
 * @requires restrictionTypesServiceModule
 * @description
 * This module defines the {@link pageRestrictionsModule.factory:pageRestrictionsFacade pageRestrictionsFacade} facade module for page restrictions.
 */
angular.module('pageRestrictionsModule', [
    'pageRestrictionsCriteriaModule',
    'pageRestrictionsServiceModule',
    'restrictionTypesServiceModule'
])

/**
 * @ngdoc service
 * @name pageRestrictionsModule.factory:pageRestrictionsFacade
 * @requires pageRestrictionsCriteriaService
 * @requires pageRestrictionsService
 * @requires restrictionTypesService
 * @description
 * A facade that exposes only the business logic necessary for features that need to work with page restrictions.
 */
.factory('pageRestrictionsFacade', ['pageRestrictionsCriteriaService', 'pageRestrictionsService', 'restrictionTypesService', function(
    pageRestrictionsCriteriaService,
    pageRestrictionsService,
    restrictionTypesService
) {

    return {

        // pageRestrictionsCriteriaService
        getRestrictionCriteriaOptions: pageRestrictionsCriteriaService.getRestrictionCriteriaOptions,
        getRestrictionCriteriaOptionFromPage: pageRestrictionsCriteriaService.getRestrictionCriteriaOptionFromPage,

        // restrictionTypesService
        getRestrictionTypesByPageType: restrictionTypesService.getRestrictionTypesByPageType,

        // pageRestrictionsService
        getRestrictionsByPageUID: pageRestrictionsService.getRestrictionsByPageUID, //@deprecated since 6.4
        isRestrictionTypeSupported: pageRestrictionsService.isRestrictionTypeSupported,
        updateRestrictionsByPageUID: pageRestrictionsService.updateRestrictionsByPageUID, //@deprecated since 6.5

        //new API's
        getRestrictionsByPageUUID: pageRestrictionsService.getRestrictionsByPageUUID,

    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionsModule
 * @requires restrictionsServiceModule
 * @description
 * This module defines the {@link restrictionsModule.restrictionsFacade restrictionsFacade} factory managing all restrictions.
 */
angular.module('restrictionsModule', [
    'restrictionsServiceModule'
])

/**
 * @ngdoc service
 * @name restrictionsModule.restrictionsFacade
 * @requires restrictionsService
 * @description
 * A facade that exposes only the business logic necessary for features that need to work with restrictions.
 */
.factory('restrictionsFacade', ['restrictionsService', function(
    restrictionsService
) {

    return {

        // restrictionsService
        getAllRestrictions: restrictionsService.getAllRestrictions,
        getPagedRestrictionsForType: restrictionsService.getPagedRestrictionsForType

    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name actionableAlertModule
 *
 * @description
 * This module defines the actionableAlert Angular component and its
 * associated constant and service. It uses the alertServiceModule to render an
 * Alert component structured around a description, an hyperlink label and a
 * custom controller.
 */
angular.module("actionableAlertModule", [
    "alertServiceModule"
])

/**
 * @ngdoc object
 * @name actionableAlertModule.object:actionableAlertConstants
 *
 * @description
 * This object defines injectable Angular constant used by the {@link actionableAlertModule.service:actionableAlertService#methods_displayActionableAlert displayActionableAlert}
 * method to render the content of the actionableAlert.
 */
.constant('actionableAlertConstants', {

    /**
     * @ngdoc property
     * @name ALERT_TEMPLATE {String}
     * @propertyOf actionableAlertModule.object:actionableAlertConstants
     *
     * @description
     * Lodash template defining the HTML content inserted within the
     * actionableAlert.
     * Below are listed the placeholders you can use which will get substituted
     * at run-time:
     *  - {String} description Text related to the associated cmsItem
     *  - {String} descriptionDetails Map of parameters passed to the translated description
     *  - {String} hyperlinkLabel Label for the hyperlink rendered within the
     *  contextual alert
     **/
    ALERT_TEMPLATE: "<div><p>{{ $alertInjectedCtrl.description | translate: $alertInjectedCtrl.descriptionDetails }}</p><div><a data-ng-click='alert.hide(); $alertInjectedCtrl.onClick();'>{{ $alertInjectedCtrl.hyperlinkLabel | translate }}</a></div></div>",

    /**
     * @ngdoc object
     * @name TIMEOUT_DURATION {Integer}
     * @propertyOf actionableAlertModule.object:actionableAlertConstants
     *
     * @description
     * The timeout duration of the cms alert item, in milliseconds.
     */
    TIMEOUT_DURATION: 20000

})

/**
 * @ngdoc service
 * @name actionableAlertModule.service:actionableAlertService
 *
 * @description
 * The actionableAlertService is used by external modules to render an
 * Alert structured around a description, an hyperlink label and a custom
 * controller.
 **/
.service('actionableAlertService', ['alertService', 'actionableAlertConstants', function(
    alertService,
    actionableAlertConstants
) {

    /**
     * @ngdoc method
     * @name actionableAlertModule.service:actionableAlertService#displayActionableAlert
     * @methodOf actionableAlertModule.service:actionableAlertService
     *
     * @description
     *
     * @param {Object} alertContent A JSON object containing the specific configuration to be applied on the actionableAlert.
     * @param {Function} alertContent.controller Function defining Angular controller consumed by the contextual alert.
     * @param {String} alertContent.description Description displayed within the contextual alert.
     * @param {String} alertContent.hyperlinkLabel Label for the hyperlink displayed within the contextual alert.
     */
    this.displayActionableAlert = function(alertContent) {
        return alertService.showInfo({
            closeable: true,
            controller: alertContent.controller,
            template: actionableAlertConstants.ALERT_TEMPLATE,
            timeout: actionableAlertConstants.TIMEOUT_DURATION
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name clonePageAlertServiceModule
 *
 * @description
 * This module defines the clonePageAlertService Angular service used
 * in a multi-country context to display an actionable alert whenever a page got
 * successfully cloned to a catalog different than the one of the page being
 * displayed. This alert contains an hyperlink allowing for the user to navigate
 * to the newly cloned page.
 */
angular.module("clonePageAlertServiceModule", [
    "actionableAlertModule",
    "experienceServiceModule",
    "catalogServiceModule",
    "functionsModule",
    "l10nModule"
])

/**
 * @ngdoc service
 * @name clonePageAlertServiceModule.service:clonePageAlertService
 *
 * @description
 * The clonePageAlertService is used by external modules to display of an
 * actionable alert anytime a page got cloned to a catalog different than the
 * one of the page being displayed. This alert contains an hyperlink allowing
 * for the user to navigate to the newly cloned page.
 */
.service('clonePageAlertService', ['$translate', 'actionableAlertService', 'experienceService', 'catalogService', 'l10nFilter', 'isBlank', function(
    $translate,
    actionableAlertService,
    experienceService,
    catalogService,
    l10nFilter,
    isBlank
) {

    /**
     * @ngdoc method
     * @name clonePageAlertServiceModule.service:clonePageAlertService#displayClonePageAlert
     * @methodOf clonePageAlertServiceModule.service:clonePageAlertService
     *
     * @description
     * Method triggering the 'actionableAlertService.displayActionableAlert()'
     * method to display an alert containing an hyperlink allowing for the user
     * to navigate to the newly cloned page.
     *
     * @param {Object} clonedPageInfo A JSON object containing the uid of the
     * newly cloned page.
     * @param {String} clonedPageInfo.uid Uid of the newly cloned page.
     */
    this.displayClonePageAlert = function(clonedPageInfo) {
        return catalogService.getCatalogVersionByUuid(clonedPageInfo.catalogVersion).then(function(catalogVersion) {
            return actionableAlertService.displayActionableAlert({
                controller: ['experienceService', 'l10nFilter', function(experienceService) {
                    this.description = "se.cms.clonepage.alert.info.description";
                    this.descriptionDetails = {
                        catalogName: l10nFilter(catalogVersion.catalogName),
                        catalogVersion: catalogVersion.version
                    };
                    this.hyperlinkLabel = "se.cms.clonepage.alert.info.hyperlink";

                    this.onClick = function() {
                        if (isBlank(clonedPageInfo.uid)) {
                            throw "clonePageAlertService.checkAndAlertOnClonePage - missing required parameter 'uid'";
                        }

                        experienceService.loadExperience({
                            siteId: catalogVersion.siteId,
                            catalogId: catalogVersion.catalogId,
                            catalogVersion: catalogVersion.version,
                            pageUid: clonedPageInfo.uid
                        });
                    };
                }]
            });
        }.bind(this));
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module("componentVisibilityAlertServiceModule", [
    "alertServiceModule",
    "actionableAlertModule",
    "sharedDataServiceModule",
    "componentVisibilityAlertServiceInterfaceModule",
    "editorModalServiceModule",
    "gatewayProxyModule"
])

.factory('componentVisibilityAlertService', ['alertService', 'actionableAlertService', 'sharedDataService', 'ComponentVisibilityAlertServiceInterface', 'editorModalService', 'extend', 'isBlank', 'gatewayProxy', function(
    alertService,
    actionableAlertService,
    sharedDataService,
    ComponentVisibilityAlertServiceInterface,
    editorModalService,
    extend,
    isBlank,
    gatewayProxy
) {

    var ComponentVisibilityAlertService = function() {
        this.gatewayId = 'ComponentVisibilityAlertService';
        gatewayProxy.initForService(this, ["checkAndAlertOnComponentVisibility"]);
    };

    ComponentVisibilityAlertService = extend(ComponentVisibilityAlertServiceInterface, ComponentVisibilityAlertService);

    ComponentVisibilityAlertService.prototype.checkAndAlertOnComponentVisibility = function(component) {

        var I18N = {
            ITEM_ALERT_HIDDEN: 'se.cms.component.visibility.alert.description.hidden',
            ITEM_ALERT_RESTRICTED: 'se.cms.component.visibility.alert.description.restricted'
        };

        if (!component.visible || component.restricted) {
            sharedDataService.get('experience').then(function(experience) {
                var message = (!component.visible) ? I18N.ITEM_ALERT_HIDDEN : I18N.ITEM_ALERT_RESTRICTED;
                var isExternal = component.catalogVersion !== experience.pageContext.catalogVersionUuid;

                if (isExternal) {
                    alertService.showAlert({
                        message: message
                    });
                } else {
                    actionableAlertService.displayActionableAlert({
                        controller: ["componentVisibilityAlertService", "editorModalService", function(componentVisibilityAlertService, editorModalService) {

                            this.onClick = function() {

                                var checkProvidedArguments = function() {
                                    var checkedArguments = [component.itemId, component.itemType, component.slotId];
                                    if (checkedArguments.filter(function(value) {
                                            return value && !isBlank(value);
                                        }).length !== checkedArguments.length) {
                                        throw "componentVisibilityAlertService.checkAndAlertOnComponentVisibility - missing properly typed parameters";
                                    }
                                };

                                checkProvidedArguments();

                                editorModalService.openAndRerenderSlot(
                                    component.itemType,
                                    component.itemId,
                                    component.slotId,
                                    "visibilityTab"
                                ).then(function(item) {

                                    if (!item.visible || item.restricted) {
                                        componentVisibilityAlertService.checkAndAlertOnComponentVisibility({
                                            itemId: item.uuid,
                                            itemType: item.itemtype,
                                            catalogVersion: item.catalogVersion,
                                            restricted: item.restricted,
                                            slotId: component.slotId,
                                            visible: item.visible
                                        });
                                    }
                                });

                            };

                            this.description = message;

                            this.hyperlinkLabel = "se.cms.component.visibility.alert.hyperlink";
                        }]
                    });
                }
            }.bind(this));
        }
    };

    return new ComponentVisibilityAlertService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name clonePageWizardServiceModule
     * @description
     * # The clonePageWizardServiceModule
     *
     * The clone page service module provides the functionality necessary to enable the cloning of pages through a modal wizard.
     *
     * Use the {@link clonePageWizardServiceModule.service:clonePageWizardService clonePageWizardService} to open the add page wizard modal.
     *
     */
    angular.module('clonePageWizardServiceModule', ['wizardServiceModule', 'catalogServiceModule',
        'createPageServiceModule', 'newPageDisplayConditionModule', 'yLoDashModule', 'experienceServiceModule',
        'selectPageTypeModule', 'selectPageTemplateModule', 'contextAwarePageStructureServiceModule',
        'confirmationModalServiceModule', 'resourceLocationsModule', 'typeStructureRestServiceModule',
        'componentHandlerServiceModule', 'pageDisplayConditionsServiceModule',
        'componentCloneOptionFormModule', 'componentCloneInfoFormModule', 'restrictionsStepHandlerFactoryModule',
        'cmsitemsRestServiceModule', 'genericEditorModule', 'eventServiceModule', 'pageRestrictionsModule',
        'restrictionsServiceModule', 'pageRestrictionsInfoMessageModule', 'selectTargetCatalogVersionModule',
        'sharedDataServiceModule', 'clonePageAlertServiceModule', 'alertServiceModule', 'pageFacadeModule'
    ])


    /**
     * @ngdoc service
     * @name clonePageWizardServiceModule.service:clonePageWizardService
     *
     * @description
     * The clone page wizard service allows opening a modal wizard to clone a page.
     */
    .service('clonePageWizardService', ['modalWizard', 'catalogService', 'pageFacade', function(modalWizard, catalogService, pageFacade) {

        /**
         * @ngdoc method
         * @name clonePageWizardServiceModule.service:clonePageWizardService#openClonePageWizard
         * @methodOf clonePageWizardServiceModule.service:clonePageWizardService
         *
         * @description
         * When called, this method opens a modal window containing a wizard to clone an existing page.
         *
         * @param {Object} pageData An object containing the pageData when the clone page wizard is opened from the page list.
         * @returns {Promise} A promise that will resolve when the modal wizard is closed or reject if it's canceled.
         *
         */
        this.openClonePageWizard = function openClonePageWizard(pageData) {
            var promise = pageData ? catalogService.retrieveUriContext() : pageFacade.retrievePageUriContext();
            return promise.then(function(uriContext) {
                return modalWizard.open({
                    controller: 'clonePageWizardController',
                    controllerAs: 'clonePageWizardCtrl',
                    properties: {
                        uriContext: uriContext,
                        basePageUUID: pageData ? pageData.uuid : undefined
                    }
                });
            });
        };
    }])

    .factory('ClonePageBuilderFactory', ['$q', 'lodash', 'contextAwarePageStructureService', 'componentHandlerService', 'typeStructureRestService', 'cmsitemsRestService', 'catalogService', function($q, lodash, contextAwarePageStructureService, componentHandlerService, typeStructureRestService, cmsitemsRestService, catalogService) {

        var ClonePageBuilder = function(restrictionsStepHandler, basePageUUID, uriContext) {
            this.restrictionsStepHandler = restrictionsStepHandler;

            this.pageInfoStructure = [];
            this.basePage = {}; // the page being cloned
            this.pageData = {}; // holds current clone page tabs data
            this.componentCloneOption = null;

            var pageUUID = basePageUUID || componentHandlerService.getPageUUID();
            var promises = $q.all([cmsitemsRestService.getById(pageUUID), catalogService.getCatalogVersionUUid(uriContext)]);
            promises.then(function(values) {
                this.basePage = values[0];
                this.pageData = lodash.cloneDeep(this.basePage);
                this.pageData.catalogVersion = values[1];
                this.pageData.pageUuid = this.basePage.uuid;
                delete this.pageData.uuid;

                cmsitemsRestService.getById(this.basePage.masterTemplate).then(function(templateInfo) {                
                    this.pageData.template = templateInfo.uid;                
                }.bind(this));

                typeStructureRestService.getStructureByTypeAndMode(this.pageData.typeCode, 'DEFAULT', {
                    getWholeStructure: true
                }).then(function(structure) {
                    this.pageData.type = structure.type;
                }.bind(this));
            }.bind(this));
        };

        ClonePageBuilder.prototype._updatePageInfoFields = function() {

            if (typeof this.pageData.defaultPage !== 'undefined') {
                if (this.pageData.typeCode) {
                    contextAwarePageStructureService.getPageStructureForNewPage(this.pageData.typeCode, this.pageData.defaultPage).then(function(pageInfoFields) {
                        this.pageInfoStructure = pageInfoFields;
                    }.bind(this));
                } else {
                    this.pageInfoStructure = [];
                }
            }
        };

        ClonePageBuilder.prototype.getPageTypeCode = function() {
            return this.pageData.typeCode;
        };

        ClonePageBuilder.prototype.getPageTemplate = function() {
            return this.pageData.template;
        };

        ClonePageBuilder.prototype.getPageLabel = function() {
            return this.pageData.label;
        };

        ClonePageBuilder.prototype.getPageInfo = function() {
            return this.pageData;
        };

        ClonePageBuilder.prototype.getBasePageInfo = function() {
            return this.basePage;
        };

        ClonePageBuilder.prototype.getPageProperties = function() {
            var pageProperties = {};
            pageProperties.type = this.pageData.type;
            pageProperties.typeCode = this.pageData.typeCode;
            pageProperties.template = this.pageData.template;
            pageProperties.onlyOneRestrictionMustApply = this.pageData.onlyOneRestrictionMustApply;
            pageProperties.catalogVersion = this.pageData.catalogVersion;

            return pageProperties;
        };

        ClonePageBuilder.prototype.getPageInfoStructure = function() {
            return this.pageInfoStructure;
        };

        ClonePageBuilder.prototype.getPageRestrictions = function() {
            return this.pageData.restrictions || [];
        };

        ClonePageBuilder.prototype.getComponentCloneOption = function() {
            return this.componentCloneOption;
        };

        ClonePageBuilder.prototype.displayConditionSelected = function(displayConditionResult) {
            var isPrimaryPage = displayConditionResult.isPrimary;
            this.pageData.defaultPage = isPrimaryPage;
            if (isPrimaryPage) {
                this.pageData.label = this.basePage.label;

                if (this.pageData.restrictions) {
                    delete this.pageData.restrictions;
                }
                this.restrictionsStepHandler.hideStep();
            } else {
                this.pageData.label = displayConditionResult.primaryPage ? displayConditionResult.primaryPage.label : '';
                this.restrictionsStepHandler.showStep();
            }
            this.pageData.uid = '';
            this._updatePageInfoFields();
        };

        ClonePageBuilder.prototype.onTargetCatalogVersionSelected = function(targetCatalogVersion) {
            this.targetCatalogVersion = targetCatalogVersion;
            this.pageData.catalogVersion = targetCatalogVersion.uuid;
        };

        ClonePageBuilder.prototype.componentCloneOptionSelected = function(cloneOptionResult) {
            this.componentCloneOption = cloneOptionResult;
        };

        ClonePageBuilder.prototype.restrictionsSelected = function(onlyOneRestrictionMustApply, restrictions) {
            this.pageData.onlyOneRestrictionMustApply = onlyOneRestrictionMustApply;
            this.pageData.restrictions = restrictions;
        };

        ClonePageBuilder.prototype.getTargetCatalogVersion = function() {
            return this.targetCatalogVersion;
        };

        return ClonePageBuilder;
    }])

    /**
     * @ngdoc controller
     * @name clonePageWizardServiceModule.controller:clonePageWizardController
     *
     * @description
     * The clone page wizard controller manages the operation of the wizard used to create new pages.
     */
    .controller('clonePageWizardController', ['$q', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'lodash', 'wizardManager', 'ClonePageBuilderFactory', 'restrictionsStepHandlerFactory', 'experienceService', 'confirmationModalService', 'cmsitemsRestService', 'systemEventService', 'pageRestrictionsFacade', 'restrictionsService', 'sharedDataService', 'clonePageAlertService', 'alertService', 'pageFacade', function($q, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, lodash, wizardManager, ClonePageBuilderFactory, restrictionsStepHandlerFactory,
        experienceService, confirmationModalService, cmsitemsRestService, systemEventService, pageRestrictionsFacade, restrictionsService, sharedDataService, clonePageAlertService, alertService, pageFacade) {

        this.uriContext = wizardManager.properties.uriContext;
        this.callbacks = {};
        this.basePageUUID = wizardManager.properties.basePageUUID;
        var cloneInprogress = false;

        var self = this;

        this.restrictionStepProperties = {
            id: 'restrictionsStepId',
            name: 'se.cms.restrictions.editor.tab',
            title: 'se.cms.clonepagewizard.pageclone.title',
            templateUrl: 'clonePageRestrictionsStepTemplate.html'
        };

        var restrictionsEditorFunctionBindingsClosure = {}; // bound in the view for restrictions step
        var restrictionsStepHandler = restrictionsStepHandlerFactory.createRestrictionsStepHandler(wizardManager, restrictionsEditorFunctionBindingsClosure, this.restrictionStepProperties);

        // Constants
        var CLONE_PAGE_WIZARD_STEPS = {
            PAGE_CLONE_OPTIONS: 'cloneOptions',
            PAGE_INFO: 'pageInfo',
            PAGE_RESTRICTIONS: this.restrictionStepProperties.id
        };

        this.pageBuilder = new ClonePageBuilderFactory(restrictionsStepHandler, this.basePageUUID, this.uriContext);

        this.restrictionsEditorFunctionBindings = restrictionsEditorFunctionBindingsClosure;
        this.typeChanged = true;
        this.infoChanged = true;
        this.model = {
            page: {},
            sharedPage: {}
        };

        // Wizard Configuration
        this.getWizardConfig = function() {
            var wizardConfig = {
                isFormValid: this.isFormValid.bind(this),
                onNext: this.onNext.bind(this),
                onDone: this.onDone.bind(this),
                onCancel: this.onCancel,
                steps: [{
                    id: CLONE_PAGE_WIZARD_STEPS.PAGE_CLONE_OPTIONS,
                    name: 'se.cms.clonepagewizard.pagecloneoptions.tabname',
                    title: 'se.cms.clonepagewizard.pageclone.title',
                    templateUrl: 'clonePageOptionsStepTemplate.html'
                }, {
                    id: CLONE_PAGE_WIZARD_STEPS.PAGE_INFO,
                    name: 'se.cms.clonepagewizard.pageinfo.tabname',
                    title: 'se.cms.clonepagewizard.pageclone.title',
                    templateUrl: 'clonePageInfoStepTemplate.html'
                }]
            };

            return wizardConfig;
        }.bind(this);

        this.onCancel = function onCancel() {
            return confirmationModalService.confirm({
                description: 'se.editor.cancel.confirm'
            });
        };

        // Wizard Navigation
        this.isFormValid = function(stepId) {
            switch (stepId) {
                case CLONE_PAGE_WIZARD_STEPS.PAGE_CLONE_OPTIONS:
                    return true;

                case CLONE_PAGE_WIZARD_STEPS.PAGE_INFO:
                    return !cloneInprogress && (self.callbacks.isDirtyPageInfo && self.callbacks.isValidPageInfo && self.callbacks.isValidPageInfo());

                case CLONE_PAGE_WIZARD_STEPS.PAGE_RESTRICTIONS:
                    return !cloneInprogress && this.pageBuilder.getPageRestrictions().length > 0;
            }
            return false;
        };

        this.onNext = function() {
            return $q.when(true);
        };

        this.onDone = function() {
            cloneInprogress = true;
            return this.callbacks.savePageInfo().then(function(page) {
                var payload = this._preparePagePayload(page);

                if (this.pageBuilder.getTargetCatalogVersion()) {
                    payload.siteId = this.pageBuilder.getTargetCatalogVersion().siteId;
                }
                return sharedDataService.get('experience').then(function(experience) {
                    return pageFacade.createPage(payload).then(function(response) {

                        if (experience.catalogDescriptor.catalogVersionUuid === response.catalogVersion) {
                            experienceService.updateExperiencePageId(response.uid);
                        } else {
                            clonePageAlertService.displayClonePageAlert(response);
                        }
                        return alertService.showSuccess({
                            message: "se.cms.clonepage.alert.success"
                        });

                    }, function(failure) {
                        cloneInprogress = false; // re-enable the button
                        systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                            messages: failure.data.errors
                        });
                        wizardManager.goToStepWithId(CLONE_PAGE_WIZARD_STEPS.PAGE_INFO);
                        return $q.reject();
                    });
                }.bind(this));
            }.bind(this), function() {
                cloneInprogress = false; // re-enable the button
            });
        };

        this._preparePagePayload = function(page) {
            var newClonePage = lodash.cloneDeep(page);

            lodash.merge(newClonePage, this.pageBuilder.getPageProperties()); // set page info properties

            newClonePage.cloneComponents = this.pageBuilder.getComponentCloneOption() === 'clone'; // set clone option
            newClonePage.itemtype = page.typeCode;

            if (this.isRestrictionsActive()) { // set restrictions
                newClonePage.restrictions = this.pageBuilder.getPageRestrictions();
            }

            return newClonePage;
        }.bind(this);

        this.getPageTypeCode = function() {
            return this.pageBuilder.getPageTypeCode();
        }.bind(this);

        this.getPageLabel = function() {
            return this.pageBuilder.getPageLabel();
        }.bind(this);

        this.getPageTemplate = function() {
            return this.pageBuilder.getPageTemplate();
        }.bind(this);

        this.getPageInfo = function getPageInfo() {
            var page = this.pageBuilder.getPageInfo();
            page.uriContext = this.uriContext;
            return page;
        }.bind(this);

        this.getBasePageInfo = function getBasePageInfo() {
            var page = this.pageBuilder.getBasePageInfo();
            page.uriContext = this.uriContext;
            return page;
        }.bind(this);

        this.getPageRestrictions = function() {
            return this.pageBuilder.getPageRestrictions();
        }.bind(this);

        this.variationResult = function(displayConditionResult) {
            this.infoChanged = true;
            this.pageBuilder.displayConditionSelected(displayConditionResult);
        }.bind(this);

        this.onTargetCatalogVersionSelected = function($catalogVersion) {
            this.pageBuilder.onTargetCatalogVersionSelected($catalogVersion);
        }.bind(this);

        this.triggerUpdateCloneOptionResult = function(cloneOptionResult) {
            this.pageBuilder.componentCloneOptionSelected(cloneOptionResult);
        }.bind(this);

        this.getPageInfoStructure = function getPageInfoStructure() {
            return this.pageBuilder.getPageInfoStructure();
        }.bind(this);

        this.restrictionsResult = function(onlyOneRestrictionMustApply, restrictions) {
            this.pageBuilder.restrictionsSelected(onlyOneRestrictionMustApply, restrictions);
        }.bind(this);

        this.isRestrictionsActive = function isRestrictionsActive() {
            if (!this.typeChanged || wizardManager.getCurrentStepId() === CLONE_PAGE_WIZARD_STEPS.PAGE_RESTRICTIONS) {
                this.typeChanged = false;
                return true;
            }
            return false;
        }.bind(this);

        this.isPageInfoActive = function isPageInfoActive() {
            if (!this.infoChanged || wizardManager.getCurrentStepId() === CLONE_PAGE_WIZARD_STEPS.PAGE_INFO) {
                this.infoChanged = false;
                return true;
            }
            return false;
        }.bind(this);

        this.resetQueryFilter = function() {
            this.query.value = '';
        }.bind(this);

        this.getRestrictionTypes = function() {
            return pageRestrictionsFacade.getRestrictionTypesByPageType(this.getPageTypeCode());
        }.bind(this);

        this.getSupportedRestrictionTypes = function() {
            return restrictionsService.getSupportedRestrictionTypeCodes();
        };

        this.getTargetCatalogVersion = function() {
            return this.pageBuilder.getTargetCatalogVersion();
        }.bind(this);
    }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('contextAwareCatalogModule', ['catalogServiceModule', 'resourceLocationsModule', 'sharedDataServiceModule'])
    .service('contextAwareCatalogService', ['$q', 'catalogService', 'PRODUCT_CATEGORY_SEARCH_RESOURCE_URI', 'PRODUCT_CATEGORY_RESOURCE_URI', 'PRODUCT_LIST_RESOURCE_API', 'PRODUCT_RESOURCE_API', 'PAGES_LIST_RESOURCE_URI', 'sharedDataService', function($q, catalogService, PRODUCT_CATEGORY_SEARCH_RESOURCE_URI, PRODUCT_CATEGORY_RESOURCE_URI, PRODUCT_LIST_RESOURCE_API, PRODUCT_RESOURCE_API, PAGES_LIST_RESOURCE_URI, sharedDataService) {

        this._getSearchUriByProductCatalogIdAndUriConstant = function(productCatalogId, uriConstant) {
            return catalogService.getActiveProductCatalogVersionByCatalogId(productCatalogId).then(function(catalogVersion) {
                var uri = uriConstant
                    .replace(/:catalogId/gi, productCatalogId)
                    .replace(/:catalogVersion/gi, catalogVersion);
                return $q.when(uri);
            });
        };

        this._getItemUriByUriConstant = function(uriConstant) {
            var uri = uriConstant.replace(/:siteUID/gi, 'CURRENT_CONTEXT_SITE_ID');
            return $q.when(uri);
        };

        this._getContentPageUri = function() {
            return sharedDataService.get('experience').then(function(data) {
                var catalogId = data.catalogDescriptor.catalogId;
                return catalogService.getActiveContentCatalogVersionByCatalogId(catalogId).then(function(catalogVersion) {
                    var uri = PAGES_LIST_RESOURCE_URI
                        .replace(/:catalogId/gi, catalogId)
                        .replace(/:catalogVersion/gi, catalogVersion);
                    return $q.when(uri);
                });
            });
        };

        this.getProductCategorySearchUri = function(productCatalogId) {
            return this._getSearchUriByProductCatalogIdAndUriConstant(productCatalogId, PRODUCT_CATEGORY_SEARCH_RESOURCE_URI);
        };

        this.getProductCategoryItemUri = function() {
            return this._getItemUriByUriConstant(PRODUCT_CATEGORY_RESOURCE_URI);
        };

        this.getProductSearchUri = function(productCatalogId) {
            return this._getSearchUriByProductCatalogIdAndUriConstant(productCatalogId, PRODUCT_LIST_RESOURCE_API);
        };

        this.getProductItemUri = function() {
            return this._getItemUriByUriConstant(PRODUCT_RESOURCE_API);
        };

        this.getContentPageSearchUri = function() {
            return this._getContentPageUri().then(function(uri) {
                return $q.when(uri + '?typeCode=ContentPage');
            });
        };

        this.getContentPageItemUri = function() {
            return this._getContentPageUri();
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name contextAwarePageStructureServiceModule
 * @description
 * # The contextAwarePageStructureServiceModule
 *
 * The contextAwarePageStructureServiceModule provides page structure functionality that is context aware,
 * ie: that changes depending on front-end needs.
 *
 */
angular.module('contextAwarePageStructureServiceModule', ['typeStructureRestServiceModule', 'pageServiceModule'])

/**
 * @ngdoc object
 * @name contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_PRE_ORDER
 *
 * @description
 * Injectable angular array of strings, that are page structure field qualifiers.<br/>
 * After {@link contextAwarePageStructureServiceModule.service:contextAwarePageStructureService contextAwarePageStructureService}
 * fetches the page structure, it will rearrange the fields based on this pre-order. Any fields matching the qualifiers specified
 * in this array will be positioned at the beginning of the returned structure, and matching the order specified.
 *
 */
.value('PAGE_STRUCTURE_PRE_ORDER', ['name', 'label', 'uid', 'title'])

/**
 * @ngdoc object
 * @name contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_POST_ORDER
 *
 * @description
 * Injectable angular array of strings, that are page structure field qualifiers.<br/>
 * After {@link contextAwarePageStructureServiceModule.service:contextAwarePageStructureService contextAwarePageStructureService}
 * fetches the page structure, it will rearrange the fields based on this post-order. Any fields matching the qualifiers specified
 * in this array will be positioned at the end of the returned structure, and matching the order specified.
 *
 */
.value('PAGE_STRUCTURE_POST_ORDER', ['creationtime', 'modifiedtime'])


/**
 * @ngdoc service
 * @name contextAwarePageStructureServiceModule.service:contextAwarePageStructureService
 *
 * @description
 * The contextAwarePageStructureServiceModule is a layer on top of the
 * {@link typeStructureRestServiceModule.service:typeStructureRestService typeStructureRestService}
 * that allows for modifying the structure for front-end needs, such as disabling fields or changing field order.
 *
 * Note: The field order is defined by {@link contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_POST_ORDER PAGE_STRUCTURE_POST_ORDER}
 * and {@link contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_POST_ORDER PAGE_STRUCTURE_POST_ORDER}, and can
 * be replaced/overriden.
 *
 */
.service('contextAwarePageStructureService', ['$q', 'typeStructureRestService', 'pageService', 'PAGE_STRUCTURE_PRE_ORDER', 'PAGE_STRUCTURE_POST_ORDER', function($q, typeStructureRestService, pageService, PAGE_STRUCTURE_PRE_ORDER, PAGE_STRUCTURE_POST_ORDER) {

    function moveElement(array, oldPosition, newPosition) {
        if (newPosition >= array.length) {
            newPosition = Math.max(0, array.length - 1);
        }
        array.splice(newPosition, 0, array.splice(oldPosition, 1)[0]);
        return array;
    }

    function getOrderedFields(unorderedFields) {

        var i, index;

        function isPreMatching(field) {
            return field.qualifier === PAGE_STRUCTURE_PRE_ORDER[i];
        }

        function isPostMatching(field) {
            return field.qualifier === PAGE_STRUCTURE_POST_ORDER[i];
        }

        if (PAGE_STRUCTURE_PRE_ORDER) {
            for (i = PAGE_STRUCTURE_PRE_ORDER.length - 1; i >= 0; i--) {
                index = unorderedFields.findIndex(isPreMatching);
                moveElement(unorderedFields, index, 0);
            }
        }
        if (PAGE_STRUCTURE_POST_ORDER) {
            for (i = 0; i < PAGE_STRUCTURE_POST_ORDER.length; i++) {
                index = unorderedFields.findIndex(isPostMatching);
                moveElement(unorderedFields, index, unorderedFields.length);
            }
        }
        return unorderedFields;
    }

    function setLabelEditability(fields, isPrimary) {
        var labelFieldIndex = fields.findIndex(function(field) {
            return field.qualifier === 'label';
        });
        if (labelFieldIndex !== -1) {
            fields[labelFieldIndex].editable = isPrimary;
        }
    }

    function getFields(pageTypeCode) {
        return typeStructureRestService.getStructureByType(pageTypeCode);
    }

    function removeField(fields, fieldQualifier) {
        var index = fields.findIndex(
            function(field) {
                return field.qualifier === fieldQualifier;
            });
        if (index !== -1) {
            fields.splice(index, 1);
        }
    }

    /**
     * @ngdoc method
     * @name contextAwarePageStructureServiceModule.service:contextAwarePageStructureService#getPageStructureForNewPage
     * @methodOf contextAwarePageStructureServiceModule.service:contextAwarePageStructureService
     *
     * @description
     * Return the CMS page structure with some modifications for the context of creating a new page.
     * The field order is modified, the created/modified time fields are removed, and the label field for variation content pages is disabled.
     *
     * @param {String} pageTypeCode The page type of the new page to be created
     * @param {Boolean} isPrimary Flag indicating if the new page will be a primary or variation page
     *
     * @returns {Array} A modified page structure
     */
    this.getPageStructureForNewPage = function getPageStructureForNewPage(pageTypeCode, isPrimary) {
        return getFields(pageTypeCode).then(function(fields) {
            if (pageTypeCode === 'ContentPage') {
                setLabelEditability(fields, isPrimary);
            }
            removeField(fields, 'creationtime');
            removeField(fields, 'modifiedtime');
            return getOrderedFields(fields);
        });
    };

    /**
     * @ngdoc method
     * @name contextAwarePageStructureServiceModule.service:contextAwarePageStructureService#getPageStructureForPageEditing
     * @methodOf contextAwarePageStructureServiceModule.service:contextAwarePageStructureService
     *
     * @description
     * Return the CMS page structure with some modifications for the context of editing the info of an existing page.
     * The field order is modified, and the label field for variation content pages is disabled.
     *
     * @param {String} pageTypeCode The page type of the page to be edited
     * @param {String} pageId The ID of the existing page to be modified
     *
     * @returns {Array} A modified page structure
     */
    this.getPageStructureForPageEditing = function getPageStructureForPageEditing(pageTypeCode, pageId) {
        return getFields(pageTypeCode).then(function(fields) {
            var readOnlyFieldNames = ['uid', 'creationtime', 'modifiedtime'];
            fields.filter(function(field) {
                return readOnlyFieldNames.indexOf(field.qualifier) >= 0;
            }).forEach(function(field) {
                field.editable = false;
            });

            if (pageTypeCode === 'ContentPage') {
                return pageService.isPagePrimary(pageId).then(function(isPrimary) {
                    setLabelEditability(fields, isPrimary);
                    return getOrderedFields(fields);
                });
            } else {
                return getOrderedFields(fields);
            }
        });
    };

    /**
     * @ngdoc method
     * @name contextAwarePageStructureServiceModule.service:contextAwarePageStructureService#getPageStructureForViewing
     * @methodOf contextAwarePageStructureServiceModule.service:contextAwarePageStructureService
     *
     * @description
     * Return the CMS page structure with some modifications for the context of viewing the info of an existing page.
     * The field order is modified, and the label field for variation content pages is disabled.
     *
     * @param {String} pageTypeCode The page type of the existing page
     *
     * @returns {Array} A modified page structure
     */
    this.getPageStructureForViewing = function getPageStructureForViewing(pageTypeCode) {
        return getFields(pageTypeCode).then(function(fields) {
            fields.forEach(function(field) {
                field.editable = false;
            });
            return getOrderedFields(fields);
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name genericEditorModalServiceModule
 * @description
 * # The genericEditorModalServiceModule
 *
 * The generic editor modal service module provides a service to open an editor modal window with a tabset inside. The editor modal is populated with a save
 * and cancel button, and allows specifying the different editor tabs.
 *
 */
angular.module('genericEditorModalServiceModule', [
    'confirmationModalServiceModule',
    'coretemplates',
    'editorTabsetModule',
    'gatewayProxyModule',
    'modalServiceModule',
    'yLoDashModule'
])

/**
 * @ngdoc service
 * @name genericEditorModalServiceModule.service:genericEditorModalService
 *
 * @description
 * The Generic Editor Modal Service is used to open an editor modal window that contains a tabset.
 *
 */
.factory('genericEditorModalService', ['modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', 'systemEventService', 'gatewayProxy', 'confirmationModalService', function(modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, systemEventService, gatewayProxy, confirmationModalService) {
    function GenericEditorModalService() {}

    /**
     * @ngdoc method
     * @name genericEditorModalServiceModule.service:genericEditorModalService#open
     * @methodOf genericEditorModalServiceModule.service:genericEditorModalService
     *
     * @description
     * Function that opens an editor modal. For this method, you must specify the list of tabs to be displayed in the tabset, an object to contain the edited information, and a save
     * callback that will be triggered once the Save button is clicked.
     *
     * @param {Object} data The object that contains the information to be displayed and edited in the modal.
     * @param {Object} tabs The list of tabs to be displayed in the tabset. Note that each of the tabs must follow the contract specified by the {@link editorTabsetModule.directive:editorTabset editor tabset} .
     * @param {String} tabs.id The ID of the current tab.
     * @param {String} tabs.title The localization key of the title to be displayed for the current tab.
     * @param {String} tabs.templateUrl The URL of the HTML template to be displayed within the current tab.
     * @param {Object} saveCallback a function executed if the user clicks the Save button and the modal closes successfully.
     *
     * @returns {Promise} A promise that resolves to the data returned by the modal when it is closed.
     */
    GenericEditorModalService.prototype.open = function(data, tabs, saveCallback) {

        return modalService.open({
            title: data.title,
            titleSuffix: 'se.cms.editor.title.suffix',
            templateInline: '<editor-tabset control="modalController.tabsetControl" model="modalController.data" tabs="modalController.tabs"></editor-tabset>',
            controller: ['genericEditorModalService', 'modalManager', 'systemEventService', 'hitch', '$scope', '$log', '$q', 'lodash',
                function(genericEditorModalService, modalManager, systemEventService, hitch, $scope, $log, $q, lodash) {
                    this.isDirty = false;

                    this.data = lodash.cloneDeep(data);

                    this.tabs = lodash.cloneDeep(tabs);

                    this.onSave = function() {
                        return this.tabsetControl.saveTabs().then(function(item) {
                            saveCallback();
                            return item;
                        });
                    };

                    this.onCancel = function() {
                        var deferred = $q.defer();
                        if (this.isDirty) {
                            confirmationModalService.confirm({
                                description: 'se.editor.cancel.confirm'
                            }).then(hitch(this, function() {
                                this.tabsetControl.cancelTabs().then(function() {
                                    deferred.resolve();
                                }, function() {
                                    deferred.reject();
                                });
                            }), function() {
                                deferred.reject();
                            });
                        } else {
                            deferred.resolve();
                        }

                        return deferred.promise;
                    };

                    this.init = function() {
                        modalManager.setDismissCallback(hitch(this, this.onCancel));

                        modalManager.setButtonHandler(hitch(this, function(buttonId) {
                            switch (buttonId) {
                                case 'save':
                                    return this.onSave();
                                case 'cancel':
                                    return this.onCancel();
                                default:
                                    $log.error('A button callback has not been registered for button with id', buttonId);
                                    break;
                            }
                        }));

                        $scope.$watch(hitch(this, function() {
                            var isDirty = this.tabsetControl && typeof this.tabsetControl.isDirty === 'function' && this.tabsetControl.isDirty();
                            return isDirty;
                        }), hitch(this, function(isDirty) {
                            if (typeof isDirty === 'boolean') {
                                if (isDirty) {
                                    this.isDirty = true;
                                    modalManager.enableButton('save');
                                } else {
                                    this.isDirty = false;
                                    modalManager.disableButton('save');
                                }
                            }
                        }));
                    };
                }
            ],
            buttons: [{
                id: 'cancel',
                label: 'se.cms.component.confirmation.modal.cancel',
                style: MODAL_BUTTON_STYLES.SECONDARY,
                action: MODAL_BUTTON_ACTIONS.DISMISS
            }, {
                id: 'save',
                label: 'se.cms.component.confirmation.modal.save',
                action: MODAL_BUTTON_ACTIONS.CLOSE,
                disabled: true
            }]
        });
    };

    return new GenericEditorModalService();

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('displayConditionsEditorModelModule', ['displayConditionsFacadeModule', 'eventServiceModule'])
    .service('displayConditionsEditorModel', ['displayConditionsFacade', '$q', 'systemEventService', function(displayConditionsFacade, $q, systemEventService) {
        this._initModelForPrimary = function(pageUid) {
            displayConditionsFacade.getVariationsForPageUid(pageUid).then(function(variations) {
                this.variations = variations;
            }.bind(this));
        };

        this._initModelForVariation = function(pageUid) {
            displayConditionsFacade.getPrimaryPageForVariationPage(pageUid).then(function(associatedPrimaryPage) {
                this.associatedPrimaryPage = associatedPrimaryPage;
                this.originalPrimaryPage = associatedPrimaryPage;
            }.bind(this));

            this.isAssociatedPrimaryReadOnly = this.pageType !== 'ContentPage';
            if (!this.isAssociatedPrimaryReadOnly) {
                displayConditionsFacade.getPrimaryPagesForVariationPageType(this.pageType).then(function(primaryPages) {
                    this.primaryPages = primaryPages;
                }.bind(this));
            }
        };

        this.initModel = function(pageUid) {
            this.pageUid = pageUid;
            displayConditionsFacade.getPageInfoForPageUid(pageUid).then(function(page) {
                this.pageName = page.pageName;
                this.pageType = page.pageType;
                this.isPrimary = page.isPrimary;

                if (this.isPrimary) {
                    this._initModelForPrimary(pageUid);
                } else {
                    this._initModelForVariation(pageUid);
                }
            }.bind(this));
        };

        this.setAssociatedPrimaryPage = function(associatedPrimaryPage) {
            systemEventService.sendAsynchEvent("EDIT_PAGE_ASSOCIATED_PRIMARY_UPDATED_EVENT", associatedPrimaryPage);
            this.associatedPrimaryPage = associatedPrimaryPage;
        };

        this.save = function() {
            return this.isDirty() ? displayConditionsFacade.updatePage(this.pageUid, {
                label: this.associatedPrimaryPage.label
            }) : $q.when(true);
        };

        this.isDirty = function() {
            return !!this.originalPrimaryPage && !!this.associatedPrimaryPage && !angular.equals(this.originalPrimaryPage, this.associatedPrimaryPage);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageDisplayConditionsServiceModule
 * @description
 * # The pageDisplayConditionsServiceModule
 *
 * The pageDisplayConditionsServiceModule provides services for working with page Display Conditions
 *
 */
angular.module('pageDisplayConditionsServiceModule', ['catalogVersionRestServiceModule'])

/**
 * @ngdoc service
 * @name pageDisplayConditionsServiceModule.service:pageDisplayConditionsService
 *
 * @description
 * The pageDisplayConditionsService provides an abstraction layer for the business logic of
 * primary/variant display conditions of a page
 */
.service('pageDisplayConditionsService', ['catalogVersionRestService', function(catalogVersionRestService) {

    /**
     * @ngdoc object
     * @name pageDisplayConditionsServiceModule.object:CONDITION
     *
     * @description
     * An object representing a page display condition<br/>
     * Structure:<br/>
     * ```
     * {
        label: [string] key to be localized to render this condition on a webpage
        description: [string] key to be localized to render this condition description on a webpage
        isPrimary: [boolean]
     * }
     * ```
     */

    function fetchDisplayConditionsForPageType(pageType, uriContext) {
        return catalogVersionRestService.get(uriContext).then(function(response) {
            return response.pageDisplayConditions.find(function(condition) {
                return condition.typecode === pageType;
            });
        });
    }

    function getPageDisplayConditionsByPageType(pageType, uriContext) {
        var conditions = [];

        return fetchDisplayConditionsForPageType(pageType, uriContext).then(function(obj) {
            if (!obj || !obj.options) {
                return [];
            }
            obj.options.forEach(function(option) {
                var displayCondition = {
                    label: option.label,
                    description: option.label + '.description',
                    isPrimary: option.id === 'PRIMARY'
                };

                conditions.push(displayCondition);
            });

            return conditions;
        });
    }


    /**
     * @ngdoc method
     * @name pageDisplayConditionsServiceModule.service:pageDisplayConditionsService#getNewPageConditions
     * @methodOf pageDisplayConditionsServiceModule.service:pageDisplayConditionsService
     *
     * @param {String} pageTypeCode The page typeCode of a potential new page
     * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
     *
     * @returns {Array} An array of {@link pageDisplayConditionsServiceModule.object:CONDITION page conditions} that are the
     * possible conditions if you wish to create a new page of the given pagetype that has the given possible primary
     * pages
     */
    this.getNewPageConditions = function(pageTypeCode, uriContext) {
        return getPageDisplayConditionsByPageType(pageTypeCode, uriContext);
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageRestrictionsCriteriaModule
 * @description
 * This module defines the {@link pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService pageRestrictionsCriteriaService} service used to consolidate business logic for restriction criteria.
 */
angular.module('pageRestrictionsCriteriaModule', [])

/**
 * @ngdoc service
 * @name pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService
 * @description
 * A service for working with restriction criteria.
 */
.service('pageRestrictionsCriteriaService', function() {

    var ALL = {},
        ANY = {};

    function setupCriteria(criteria, id, boolValue) {
        Object.defineProperty(criteria, 'id', {
            writable: false,
            value: id
        });
        Object.defineProperty(criteria, 'label', {
            writable: false,
            value: 'se.cms.restrictions.criteria.' + id
        });
        Object.defineProperty(criteria, 'editLabel', {
            writable: false,
            value: 'se.cms.restrictions.criteria.select.' + id
        });
        Object.defineProperty(criteria, 'value', {
            writable: false,
            value: boolValue
        });
    }
    setupCriteria(ALL, 'all', false);
    setupCriteria(ANY, 'any', true);

    var restrictionCriteriaOptions = [ALL, ANY];

    /**
     * @ngdoc method
     * @name pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService#getMatchCriteriaLabel
     * @methodOf pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService
     * @param {Boolean} onlyOneRestrictionMustApply A boolean to determine whether one restriction should be applied.
     * @return {String} The i18n key of the restriction criteria.
     */
    this.getMatchCriteriaLabel = function(onlyOneRestrictionMustApply) {
        if (onlyOneRestrictionMustApply) {
            return ANY.label;
        }
        return ALL.label;
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService#getRestrictionCriteriaOptions
     * @methodOf pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService
     * @return {Array} An array of criteria options.
     */
    this.getRestrictionCriteriaOptions = function() {
        return restrictionCriteriaOptions;
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService#getRestrictionCriteriaOptionFromPage
     * @methodOf pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService
     * @return {Object} An object of the restriction criteria for the given page.
     */
    this.getRestrictionCriteriaOptionFromPage = function(page) {
        if (page && typeof page.onlyOneRestrictionMustApply === 'boolean') {
            if (page.onlyOneRestrictionMustApply) {
                return ANY;
            }
        }
        return ALL;
    };

});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageRestrictionsServiceModule
 * @requires pageRestrictionsRestServiceModule
 * @requires restrictionsServiceModule
 * @requires restrictionTypesServiceModule
 * @requires yLoDashModule
 * @description
 * This module defines the {@link pageRestrictionsServiceModule.service:pageRestrictionsService pageRestrictionsService} service used to consolidate business logic for SAP Hybris platform CMS restrictions for pages.
 */
angular.module('pageRestrictionsServiceModule', [
    'pageRestrictionsRestServiceModule',
    'restrictionsServiceModule',
    'restrictionTypesServiceModule',
    'yLoDashModule',
    'cmsitemsRestServiceModule'
])

/**
 * @ngdoc service
 * @name pageRestrictionsServiceModule.service:pageRestrictionsService
 * @requires $q
 * @requires yLoDashModule
 * @requires pageRestrictionsRestService
 * @requires restrictionsService
 * @requires restrictionTypesService
 * @requires typeStructureRestService
 * @description
 * Service that concerns business logic tasks related to CMS restrictions for CMS pages.
 */
.service('pageRestrictionsService', ['$q', 'lodash', 'pageRestrictionsRestService', 'restrictionsService', 'restrictionTypesService', 'cmsitemsRestService', function(
    $q,
    lodash,
    pageRestrictionsRestService,
    restrictionsService,
    restrictionTypesService,
    cmsitemsRestService
) {

    function getOriginalRestrictionsByPageUID(pageId) {
        return $q.all([pageRestrictionsRestService.getPagesRestrictionsForPageId(pageId), restrictionsService.getAllRestrictions()]).then(function(values) {
            var pagesRestrictionsResponse = values[0];
            var restrictionsResponse = values[1];

            var restrictionUIDs = pagesRestrictionsResponse.pageRestrictionList.map(function(pageRestriction) {
                return pageRestriction.restrictionId;
            });

            return restrictionsResponse.restrictions.filter(function(restriction) {
                return restrictionUIDs.indexOf(restriction.uid) >= 0;
            });
        });
    }

    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#getRestrictionsByPageUID
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @deprecated since 6.4
     * @param {String} pageId The unique page identifier for which to fetch the restrictions.
     * @returns {Array} An array of all restrictions applied to the page with the given page ID
     */
    this.getRestrictionsByPageUID = function(pageId) {
        return getOriginalRestrictionsByPageUID(pageId).then(function(restrictions) {
            return $q.all(restrictions.map(function(restriction) {
                return restrictionTypesService.getRestrictionTypeForTypeCode(restriction.typeCode).then(function(restrictionType) {
                    return angular.extend({}, restriction, {
                        typeName: restrictionType.name
                    });
                });
            }));
        });
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#updateRestrictionsByPageUID
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @param {String} pageUid The unique page identifier for the page to be updated.
     * @param {Array} restrictionsArray An array of restrictions to be applied to the page.
     * @returns {Array} All restrictions for the given pageUid.
     * @deprecated since 6.5
     * 
     * @description
     * Update the list of restrictions for a page. The provided list of restrictions replaces any/all restrictions
     * that are currently on the page.
     */
    this.updateRestrictionsByPageUID = function(pageUid, restrictionsArray) {
        var payload = {
            pageid: pageUid,
            pageRestrictionList: []
        };
        restrictionsArray.forEach(function(restriction) {
            payload.pageRestrictionList.push({
                restrictionId: restriction.uid,
                pageId: pageUid
            });
        });
        return pageRestrictionsRestService.update(payload);
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#getPageRestrictionsCountMapForCatalogVersion
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @param {String} siteUID The site Id.
     * @param {String} catalogUID The catalog Id.
     * @param {String} catalogVersionUID The catalog version.
     * @returns {Object} A map of all pageId as keys, and the number of restrictions applied to that page as the values.
     */
    this.getPageRestrictionsCountMapForCatalogVersion = function getPageRestrictionsCountMapForCatalogVersion(siteUID, catalogUID, catalogVersionUID) {
        return pageRestrictionsRestService.getPagesRestrictionsForCatalogVersion(siteUID, catalogUID, catalogVersionUID).then(function(relations) {
            return lodash.countBy(relations.pageRestrictionList, 'pageId');
        });
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#getPageRestrictionsCountForPageUID
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @param {String} pageUID The page Id.
     * @returns {Number} The number of restrictions applied to the page with the give page UID.
     */
    this.getPageRestrictionsCountForPageUID = function getPageRestrictionsCountForPageUID(pageUID) {
        return pageRestrictionsRestService.getPagesRestrictionsForPageId(pageUID).then(function(response) {
            return response.pageRestrictionList.length;
        });
    };

    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#isRestrictionTypeSupported
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @param {String} restrictionTypeCode Code for the restriction type.
     * @returns {Boolean} True if smartedit supports editing or creating restrictions of this type.
     */
    this.isRestrictionTypeSupported = function isRestrictionTypeSupported(restrictionTypeCode) {
        return this.getSupportedRestrictionTypeCodes().then(function(supportedTypes) {
            return supportedTypes.indexOf(restrictionTypeCode) >= 0;
        });
    };


    /**
     * @ngdoc method
     * @name pageRestrictionsServiceModule.service:pageRestrictionsService#getRestrictionsByPageUID
     * @methodOf pageRestrictionsServiceModule.service:pageRestrictionsService
     * @param {String} pageUuid The uuid of the page for which to fetch the restrictions.
     * @returns {Array} An array of all restrictions applied to the page with the given page ID
     */
    this.getRestrictionsByPageUUID = function(pageUuid) {
        return cmsitemsRestService.getById(pageUuid).then(function(pageData) {
            return cmsitemsRestService.get({
                uuids: pageData.restrictions.join(',')
            });
        });
    };


}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageTypesRestrictionTypesServiceModule
 * @requires pageTypesRestrictionTypesRestServiceModule
 * @description
 * This module defines the {@link pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService
 * pageTypesRestrictionTypesService} REST service used to consolidate business logic for SAP Hybris platform CMS
 * pageTypes-restrictionTypes relations.
 */
angular.module('pageTypesRestrictionTypesServiceModule', [
    'pageTypesRestrictionTypesRestServiceModule'
])

/**
 * @ngdoc service
 * @name pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService
 * @requires pageTypesRestrictionTypesRestService
 * @description
 * Service that concerns business logic tasks related to CMS pageTypes-restrictionTypes relations.
 */
.service('pageTypesRestrictionTypesService', ['pageTypesRestrictionTypesRestService', function(
    pageTypesRestrictionTypesRestService
) {

    var cache = null;
    var self = this;

    /**
     * @ngdoc method
     * @name pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService#getRestrictionTypeCodesForPageType
     * @methodOf pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService
     * @param {String} pageType The page type for which the restriction type codes can be applied.
     * @returns {Array} An array of restriction type codes that can be applied to the given page type.
     */
    this.getRestrictionTypeCodesForPageType = function(pageType) {
        return self.getPageTypesRestrictionTypes().then(function(pageTypesRestrictionTypes) {
            pageTypesRestrictionTypes = pageTypesRestrictionTypes.filter(function(pageTypeRestrictionType) {
                return pageTypeRestrictionType.pageType === pageType;
            });
            return pageTypesRestrictionTypes.map(function(pageTypeRestrictionType) {
                return pageTypeRestrictionType.restrictionType;
            });
        });
    };

    /**
     * @ngdoc method
     * @name pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService#getPageTypesRestrictionTypes
     * @methodOf pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService
     * @returns {Array} An array of all of the pageType-restrictionType relations in the system.
     */
    this.getPageTypesRestrictionTypes = function() {
        if (cache && cache.$$state.status !== 2) { // if the get fails, allow it to be retried
            return cache;
        } else {
            cache = pageTypesRestrictionTypesRestService.getPageTypesRestrictionTypes().then(function(response) {
                return response.pageTypeRestrictionTypeList;
            });
        }
        return cache;
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionTypesServiceModule
 * @requires pageTypesRestrictionTypesServiceModule
 * @requires restrictionTypesRestServiceModule
 * @description
 * This module defines the {@link restrictionTypesServiceModule.service:restrictionTypesService restrictionTypesService} REST service used to consolidate business logic for SAP Hybris platform CMS restriction types for
 * both pages and components.
 */
angular.module('restrictionTypesServiceModule', [
    'pageTypesRestrictionTypesServiceModule',
    'restrictionTypesRestServiceModule'
])

/**
 * @ngdoc service
 * @name restrictionTypesServiceModule.service:restrictionTypesService
 * @requires $q
 * @requires languageService
 * @requires pageTypesRestrictionTypesService
 * @requires restrictionTypesRestService
 * @description
 * Service that concerns business logic tasks related to CMS page and component restriction types in the SAP Hybris platform.
 * This service fetches all restriction types configured on the platform, and caches them for the duration of the session.
 */
.service('restrictionTypesService', ['$q', 'languageService', 'pageTypesRestrictionTypesService', 'restrictionTypesRestService', function(
    $q,
    languageService,
    pageTypesRestrictionTypesService,
    restrictionTypesRestService) {

    var cache = null;
    var self = this;

    /**
     * @ngdoc method
     * @name restrictionTypesServiceModule.service:restrictionTypesService#getRestrictionTypesByPageType
     * @methodOf restrictionTypesServiceModule.service:restrictionTypesService
     * @param {String} pageType The page type for which the restrictions types can be applied
     * @returns {Array} All types of restriction that can be applied to the given page type.
     */
    this.getRestrictionTypesByPageType = function(pageType) {
        return self.getRestrictionTypes().then(function(restrictionTypes) {
            return pageTypesRestrictionTypesService.getRestrictionTypeCodesForPageType(pageType).then(function(restrictionTypeCodes) {
                return restrictionTypes.filter(function(restrictionType) {
                    return restrictionTypeCodes.indexOf(restrictionType.code) >= 0;
                });
            });
        });
    };

    /**
     * @ngdoc method
     * @name restrictionTypesServiceModule.service:restrictionTypesService#getRestrictionTypes
     * @methodOf restrictionTypesServiceModule.service:restrictionTypesService
     * @returns {Array} All restriction types in the system
     */
    this.getRestrictionTypes = function() {
        if (cache && cache.$$state.status !== 2) { // if the get fails, allow it to be retried
            return cache;
        } else {
            cache = restrictionTypesRestService.getRestrictionTypes().then(function(response) {
                return response.restrictionTypes;
            });
        }
        return cache;
    };

    /**
     * @ngdoc method
     * @name restrictionTypesServiceModule.service:restrictionTypesService#getRestrictionTypeForTypeCode
     * @methodOf restrictionTypesServiceModule.service:restrictionTypesService
     * @param {String} The typeCode of a restriction
     * @returns {Object} The restriction type object, who's code property matches the given restriction typeCode property
     */
    this.getRestrictionTypeForTypeCode = function(typeCode) {
        return self.getRestrictionTypes().then(function(restrictionTypes) {
            return restrictionTypes.find(function(restrictionType) {
                return restrictionType.code === typeCode;
            });
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name pageTemplateServiceModule
     * @description
     * # The pageTemplateServiceModule
     *
     * The page template service module provides a service that allows the retrieval of page templates associated to a page type.
     *
     */
    angular.module('pageTemplateServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'yLoDashModule'])
        .constant('NON_SUPPORTED_TEMPLATES', [
            "layout/landingLayout1Page",
            "layout/landingLayout3Page",
            "layout/landingLayout4Page",
            "layout/landingLayout5Page",
            "layout/landingLayout6Page",
            "layout/landingLayoutPage",
            "account/accountRegisterPage",
            "checkout/checkoutRegisterPage"
        ])
        /**
         * @ngdoc service
         * @name pageTemplateServiceModule.service:pageTemplateService
         *
         * @description
         * This service allows the retrieval of page templates associated to a page type.
         *
         */
        .factory('pageTemplateService', ['restServiceFactory', 'PAGE_TEMPLATES_URI', 'NON_SUPPORTED_TEMPLATES', 'lodash', function(restServiceFactory, PAGE_TEMPLATES_URI, NON_SUPPORTED_TEMPLATES, lodash) {
            var pageTemplateRestService = restServiceFactory.get(PAGE_TEMPLATES_URI);

            return {
                /**
                 * @ngdoc method
                 * @name pageTemplateServiceModule.service:pageTemplateService#getPageTemplatesForType
                 * @methodOf pageTemplateServiceModule.service:pageTemplateService
                 *
                 * @description
                 * When called, this method retrieves the page templates associated to the provided page type.
                 *
                 * @param {Object} uriContext A {@link resourceLocationsModule.object:UriContext UriContext}
                 * @param {String} pageType The page type for which to retrieve its associated page templates.
                 *
                 * @returns {Promise} A promise that will resolve with the page templates retrieved for the provided page type.
                 *
                 */
                getPageTemplatesForType: function(uriContext, pageType) {

                    var params = lodash.assign({
                            pageTypeCode: pageType,
                            active: true
                        },
                        uriContext);

                    return pageTemplateRestService.get(params).then(function(pageTemplates) {
                        return {
                            templates: pageTemplates.templates.filter(function(pageTemplate) {
                                return NON_SUPPORTED_TEMPLATES.indexOf(pageTemplate.frontEndName) === -1;
                            })
                        };
                    });
                }
            };
        }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name addPageServiceModule
     * @description
     * # The addPageServiceModule
     *
     * The add page service module provides the functionality necessary to enable the creation of pages through a modal wizard.
     *
     * Use the {@link addPageServiceModule.service:addPageWizardService addPageWizardService} to open the add page wizard modal.
     *
     */
    angular.module('addPageServiceModule', ['eventServiceModule', 'genericEditorModule', 'wizardServiceModule', 'functionsModule', 'catalogServiceModule', 'pageTypeServiceModule',
        'languageServiceModule', 'cmsitemsRestServiceModule', 'pageRestrictionsModule', 'restrictionsServiceModule',
        'newPageDisplayConditionModule', 'yLoDashModule', 'experienceServiceModule',
        'selectPageTypeModule', 'selectPageTemplateModule', 'contextAwarePageStructureServiceModule',
        'confirmationModalServiceModule', 'resourceLocationsModule', 'restrictionsStepHandlerFactoryModule',
        'pageRestrictionsInfoMessageModule'
    ])


    /**
     * @ngdoc service
     * @name addPageServiceModule.service:addPageWizardService
     *
     * @description
     * The add page wizard service allows opening a modal wizard to create a page.
     */
    .service('addPageWizardService', ['modalWizard', 'catalogService', function(modalWizard, catalogService) {

        /**
         * @ngdoc method
         * @name addPageServiceModule.service:addPageWizardService#openAddPageWizard
         * @methodOf addPageServiceModule.service:addPageWizardService
         *
         * @description
         * When called, this method opens a modal window containing a wizard to create new pages.
         *
         * @returns {Promise} A promise that will resolve when the modal wizard is closed or reject if it's canceled.
         *
         */
        this.openAddPageWizard = function openAddPageWizard() {
            return catalogService.retrieveUriContext().then(function(uriContext) {
                return modalWizard.open({
                    controller: 'addPageWizardController',
                    controllerAs: 'addPageWizardCtl',
                    properties: {
                        uriContext: uriContext
                    }
                });
            });
        };
    }])

    .factory('pageBuilderFactory', ['contextAwarePageStructureService', 'catalogService', function(contextAwarePageStructureService, catalogService) {

        function PageBuilder(restrictionsStepHandler, uriContext) {

            var model = {};
            var page = {};

            catalogService.getCatalogVersionUUid(uriContext).then(function(catalogVersionUuid) {
                page.catalogVersion = catalogVersionUuid;
            });

            function updatePageInfoFields() {

                if (page.defaultPage !== undefined) {
                    if (model.pageType) {
                        contextAwarePageStructureService.getPageStructureForNewPage(model.pageType.code, page.defaultPage).then(
                            function(pageInfoFields) {
                                model.pageInfoFields = pageInfoFields;
                            }
                        );
                    } else {
                        model.pageInfoFields = [];
                    }
                }
            }

            this.pageTypeSelected = function pageTypeSelected(pageTypeObject) {
                model.pageType = pageTypeObject;
                model.pageTemplate = null;
                updatePageInfoFields();
            };

            this.pageTemplateSelected = function pageTemplateSelected(pageTemplateObject) {
                model.pageTemplate = pageTemplateObject;
            };

            this.getPageTypeCode = function getPageType() {
                return model.pageType ? model.pageType.code : null;
            };

            this.getTemplateId = function getTemplateId() {
                return model.pageTemplate ? model.pageTemplate.uuid : "";
            };

            this.getPage = function getPage() {
                page.typeCode = model.pageType ? model.pageType.code : null;
                page.itemtype = page.typeCode;
                page.type = model.pageType ? model.pageType.type : null;
                page.masterTemplate = model.pageTemplate ? model.pageTemplate.uuid : null;
                return page;
            };

            this.setPageUid = function setPageUid(uid) {
                page.uid = uid;
            };

            this.setRestrictions = function(onlyOneRestrictionMustApply, restrictions) {
                page.onlyOneRestrictionMustApply = onlyOneRestrictionMustApply;
                page.restrictions = restrictions;
            };

            this.getPageInfoStructure = function getPageInfoStructure() {
                return model.pageInfoFields;
            };

            this.displayConditionSelected = function displayConditionSelected(displayConditionResult) {
                var isPrimaryPage = displayConditionResult.isPrimary;
                page.defaultPage = isPrimaryPage;
                if (isPrimaryPage) {
                    page.label = null;
                    restrictionsStepHandler.hideStep();
                } else {
                    page.label = displayConditionResult.primaryPage ? displayConditionResult.primaryPage.label : "";
                    restrictionsStepHandler.showStep();
                }
                updatePageInfoFields();
            };

        }

        return {
            createPageBuilder: function(restrictionsStepHandler, uriContext) {
                return new PageBuilder(restrictionsStepHandler, uriContext);
            }
        };
    }])


    /**
     * @ngdoc controller
     * @name addPageServiceModule.controller:addPageWizardController
     *
     * @description
     * The add page wizard controller manages the operation of the wizard used to create new pages.
     */
    .controller('addPageWizardController', ['$q', '$scope', '$timeout', 'wizardManager', 'pageTypeService', 'pageBuilderFactory', 'languageService', 'cmsitemsRestService', 'lodash', 'restrictionsStepHandlerFactory', 'experienceService', 'confirmationModalService', 'systemEventService', 'pageRestrictionsFacade', 'restrictionsService', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', function($q, $scope, $timeout, wizardManager, pageTypeService, pageBuilderFactory,
        languageService, cmsitemsRestService, lodash, restrictionsStepHandlerFactory, experienceService, confirmationModalService,
        systemEventService, pageRestrictionsFacade, restrictionsService, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT) {

        this.uriContext = wizardManager.properties.uriContext;
        this.callbacks = {};

        this.restrictionStepProperties = {
            id: 'restrictionsStepId',
            name: 'se.cms.restrictions.editor.tab',
            title: 'se.cms.addpagewizard.pagetype.title',
            templateUrl: 'pageRestrictionsStepTemplate.html'
        };

        var self = this;

        var restrictionsEditorFunctionBindingsClosure = {}; // bound in the view for restrictions step
        var restrictionsStepHandler = restrictionsStepHandlerFactory.createRestrictionsStepHandler(wizardManager, restrictionsEditorFunctionBindingsClosure, this.restrictionStepProperties);

        // Constants
        var ADD_PAGE_WIZARD_STEPS = {
            PAGE_TYPE: 'pageType',
            PAGE_TEMPLATE: 'pageTemplate',
            PAGE_DISPLAY_CONDITION: 'pageDisplayCondition',
            PAGE_INFO: 'pageInfo',
            PAGE_RESTRICTIONS: restrictionsStepHandler.getStepId()
        };

        this.pageBuilder = pageBuilderFactory.createPageBuilder(restrictionsStepHandler, this.uriContext);

        this.restrictionsEditorFunctionBindings = restrictionsEditorFunctionBindingsClosure;
        this.typeChanged = true;
        this.infoChanged = true;
        this.model = {
            page: {},
            sharedPage: {}
        };

        // Wizard Configuration
        this.getWizardConfig = function() {
            var wizardConfig = {
                isFormValid: this.isFormValid.bind(this),
                onNext: this.onNext.bind(this),
                onDone: this.onDone.bind(this),
                onCancel: this.onCancel,
                steps: [{
                    id: ADD_PAGE_WIZARD_STEPS.PAGE_TYPE,
                    name: 'se.cms.addpagewizard.pagetype.tabname',
                    title: 'se.cms.addpagewizard.pagetype.title',
                    templateUrl: 'pageTypeStepTemplate.html'
                }, {
                    id: ADD_PAGE_WIZARD_STEPS.PAGE_TEMPLATE,
                    name: 'se.cms.addpagewizard.pagetemplate.tabname',
                    title: 'se.cms.addpagewizard.pagetype.title',
                    templateUrl: 'pageTemplateStepTemplate.html'
                }, {
                    id: ADD_PAGE_WIZARD_STEPS.PAGE_DISPLAY_CONDITION,
                    name: 'se.cms.addpagewizard.pageconditions.tabname',
                    title: 'se.cms.addpagewizard.pagetype.title',
                    templateUrl: 'pageDisplayConditionStepTemplate.html'
                }, {
                    id: ADD_PAGE_WIZARD_STEPS.PAGE_INFO,
                    name: 'se.cms.addpagewizard.pageinfo.tabname',
                    title: 'se.cms.addpagewizard.pagetype.title',
                    templateUrl: 'pageInfoStepTemplate.html'
                }]
            };

            return wizardConfig;
        }.bind(this);

        this.onCancel = function onCancel() {
            return confirmationModalService.confirm({
                description: 'se.editor.cancel.confirm'
            });
        };

        // Wizard Navigation
        this.isFormValid = function(stepId) {
            switch (stepId) {
                case ADD_PAGE_WIZARD_STEPS.PAGE_TYPE:
                    return !!self.pageBuilder.getPageTypeCode();

                case ADD_PAGE_WIZARD_STEPS.PAGE_TEMPLATE:
                    return !!self.pageBuilder.getTemplateId();

                case ADD_PAGE_WIZARD_STEPS.PAGE_DISPLAY_CONDITION:
                    return true;

                case ADD_PAGE_WIZARD_STEPS.PAGE_INFO:
                    return (self.callbacks.isDirtyPageInfo && self.callbacks.isDirtyPageInfo() && self.callbacks.isValidPageInfo && self.callbacks.isValidPageInfo());

                case ADD_PAGE_WIZARD_STEPS.PAGE_RESTRICTIONS:
                    return restrictionsStepHandler.isStepValid();

            }

            return false;
        };

        this.onNext = function() {
            return $q.when(true);
        };

        this.restrictionsResult = function(onlyOneRestrictionMustApply, restrictions) {
            this.pageBuilder.setRestrictions(onlyOneRestrictionMustApply, restrictions);
        }.bind(this);

        this.onDone = function() {

            return self.callbacks.savePageInfo().then(function(page) {
                lodash.defaultsDeep(page, self.pageBuilder.getPage());
                return cmsitemsRestService.create(page).then(function(pageCreated) {
                    self.pageBuilder.setPageUid(pageCreated.uid);
                    return experienceService.updateExperiencePageId(self.pageBuilder.getPage().uid);
                }, function(failure) {
                    systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                        messages: failure.data.errors
                    });
                    wizardManager.goToStepWithId(ADD_PAGE_WIZARD_STEPS.PAGE_INFO);
                    return $q.reject();
                });
            });
        };

        this.typeSelected = function typeSelected(pageType) {
            self.infoChanged = true;
            self.typeChanged = true;
            self.pageBuilder.pageTypeSelected(pageType);
        };

        this.templateSelected = function templateSelected(pageTemplate) {
            self.pageBuilder.pageTemplateSelected(pageTemplate);
        };

        this.getPageTypeCode = function getPageTypeCode() {
            return self.pageBuilder.getPageTypeCode();
        };

        this.variationResult = function(displayConditionResult) {
            self.infoChanged = true;
            self.pageBuilder.displayConditionSelected(displayConditionResult);
        };

        this.getPageInfo = function getPageInfo() {
            var page = self.pageBuilder.getPage();
            page.uriContext = self.uriContext;
            return page;
        }.bind(this);

        this.getPageInfoStructure = function getPageInfoStructure() {
            return self.pageBuilder.getPageInfoStructure();
        };

        this.isRestrictionsActive = function isRestrictionsActive() {
            if (!self.typeChanged || wizardManager.getCurrentStepId() === ADD_PAGE_WIZARD_STEPS.PAGE_RESTRICTIONS) {
                self.typeChanged = false;
                return true;
            }
            return false;
        };

        this.isPageInfoActive = function isPageInfoActive() {
            if (!self.infoChanged || wizardManager.getCurrentStepId() === ADD_PAGE_WIZARD_STEPS.PAGE_INFO) {
                self.infoChanged = false;
                return true;
            }
            return false;
        };

        this.resetQueryFilter = function() {
            this.query.value = '';
        };

        this.getRestrictionTypes = function() {
            return pageRestrictionsFacade.getRestrictionTypesByPageType(this.getPageTypeCode());
        }.bind(this);

        this.getSupportedRestrictionTypes = function() {
            return restrictionsService.getSupportedRestrictionTypeCodes();
        };


    }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageListServiceModule
 * @description
 * # The pageListServiceModule
 *
 * The Page List Service module provides a service that fetches pages for a specified catalog
 *
 */
angular.module('pageListServiceModule', ['pagesRestServiceModule'])

/**
 * @ngdoc service
 * @name pageListServiceModule.service:pageListService
 *
 * @description
 * The Page List Service fetches pages for a specified catalog using REST calls to the cmswebservices pages API.
 */
.service('pageListService', ['pagesRestService', function(pagesRestService) {

    /**
     * @ngdoc method
     * @name pageListServiceModule.service:pageListService#getPageListForCatalog
     * @methodOf pageListServiceModule.service:pageListService
     *
     * @description
     * Fetches a list of pages for the catalog that corresponds to the specified site UID, catalogId and catalogVersion. The pages are
     * retrieved using REST calls to the cmswebservices pages API.
     *
     * @param {Object} uriParams A {@link resourceLocationsModule.object:UriContext UriContext}
     *
     * @returns {Array} An array of pages descriptors. Each descriptor provides the following pages properties:
     * creationtime, modifiedtime, pk, template, title, typeCode, uid.
     */
    this.getPageListForCatalog = function(uriParams) {
        return pagesRestService.get(uriParams);
    };

    /**
     * @ngdoc method
     * @name pageListServiceModule.service:pageListService#getPageById
     * @methodOf pageListServiceModule.service:pageListService
     *
     * @description
     * Fetches the page that matches the provided page UID
     *
     * @param {String} pageUID The UID of the page to be fetched.
     *
     * @returns {Object} A page information object. Contains the following properties:
     * creationtime, modifiedtime, pk, template, title, typeCode, uid
     */
    this.getPageById = function(pageUID) {
        return pagesRestService.getById(pageUID);
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name pageServiceModule
 * @description
 * # The pageServiceModule
 *
 * The pageServiceModule provides services for dealing with CMS page objects
 *
 */
angular.module('pageServiceModule', [
    'cmsitemsRestServiceModule',
    'componentHandlerServiceModule',
    'pagesFallbacksRestServiceModule',
    'pagesRestServiceModule',
    'pagesVariationsRestServiceModule',
    'yLoDashModule'
])

/**
 * @ngdoc service
 * @name pageServiceModule.service:pageService
 *
 * @description
 * The pageService provides low level functionality for CMS pages
 */
.service('pageService', ['cmsitemsRestService', 'componentHandlerService', 'pagesFallbacksRestService', 'pagesRestService', 'pagesVariationsRestService', 'lodash', '$q', function(
    cmsitemsRestService,
    componentHandlerService,
    pagesFallbacksRestService,
    pagesRestService,
    pagesVariationsRestService,
    lodash,
    $q
) {

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#getPrimaryPagesForPageType
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Fetches a list of pages for for a given site+catalog+catalogversion
     *
     * @param {String} pageTypeCode A page type code to filter pages by
     * @param {Object} uriParams A {@link resourceLocationsModule.object:UriContext UriContext}
     *
     * @returns {Object} A promise that resolves to an array of page objects, or empty array
     */
    this.getPrimaryPagesForPageType = function(pageTypeCode, uriParams) {
        var extendedParams = lodash.assign({}, uriParams || {}, {
            typeCode: pageTypeCode,
            defaultPage: true
        });
        return pagesRestService.get(extendedParams);
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#isPagePrimary
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Determines if a page belonging to the current contextual site+catalog+catalogversion is primary.
     *
     * @param {String} pageId The UID of the page.
     *
     * @returns {Promise} A promise that resolves to true if the page is primary, false otherwise.
     */
    this.isPagePrimary = function(pageId) {
        return pagesFallbacksRestService.getFallbacksForPageId(pageId).then(function(fallbacks) {
            return fallbacks.length === 0;
        });
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#isPagePrimary
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Retrieves the primary page for the current site+catalog+catalogversion.
     *
     * @param {String} variationPageId The UID of the page.
     *
     * @returns {Promise} A promise that resolves to the page object or undefined if no primary page was found.
     */
    this.getPrimaryPage = function(variationPageId) {
        return pagesFallbacksRestService.getFallbacksForPageId(variationPageId).then(function(fallbacks) {
            return fallbacks[0] ? pagesRestService.getById(fallbacks[0]) : $q.when();
        });
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#isPagePrimary
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Retrieves the variation pages for the current site+catalog+catalogversion.
     *
     * @param {String} primaryPageId The UID of the primary page.
     *
     * @returns {Promise} A promise that resolves an array of variation pages or an empty list if none are found.
     */
    this.getVariationPages = function(primaryPageId) {
        return pagesVariationsRestService.getVariationsForPrimaryPageId(primaryPageId).then(function(variationPageIds) {
            return variationPageIds.length > 0 ? pagesRestService.get({
                uids: variationPageIds
            }) : $q.when([]);
        });
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#updatePageById
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Updates the page corresponding to the given page UID with the payload provided for the current site+catalog+catalogversion.
     *
     * @param {String} pageUid The UID of the page
     * @param {String} payload The JSON object to PUT to the pages API
     *
     * @returns {Promise} A promise that resolves to the JSON page object as it now exists in the backend
     */
    this.updatePageById = function(pageUid, payload) {
        return pagesRestService.getById(pageUid).then(function(originalPage) {
            delete originalPage.$promise;
            delete originalPage.$resolved;

            payload = lodash.assign({}, originalPage, payload);
            return pagesRestService.update(pageUid, payload);
        });
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#updatePageById
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Retrieves the page corresponding to the given page UID.
     *
     * @param {String} pageUid The UID of the page
     *
     * @returns {Promise} A promise that resolves to the JSON page object
     * corresponding to this page UID
     */
    this.getPageById = function(pageUid) {
        return pagesRestService.getById(pageUid);
    };

    /**
     * @ngdoc method
     * @name pageServiceModule.service:pageService#getCurrentPageInfo
     * @methodOf pageServiceModule.service:pageService
     *
     * @description
     * Retrieves the page information of the page that is currently loaded.
     *
     * @returns {Promise} A promise that resolves to a CMS Item object containing
     * information related to the current page
     */
    this.getCurrentPageInfo = function() {
        return cmsitemsRestService.getById(componentHandlerService.getPageUUID());
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('restrictionsStepHandlerFactoryModule', [])
    .factory('restrictionsStepHandlerFactory', ['$q', function($q) {

        function RestrictionsStepHandler(wizardManager, restrictionsEditorFunctionBindings, stepProperties) {

            var stepDetails = stepProperties;

            var isStepOnWizard = function() {
                return wizardManager.containsStep(this.getStepId());
            }.bind(this);

            this.hideStep = function hideStep() {
                if (isStepOnWizard()) {
                    wizardManager.removeStepById(this.getStepId());
                }
            };

            this.showStep = function showStep() {
                if (!isStepOnWizard()) {
                    wizardManager.addStep(stepDetails, wizardManager.getStepsCount());
                }
            };

            this.isStepValid = function isStepValid() {
                return restrictionsEditorFunctionBindings.isDirty && restrictionsEditorFunctionBindings.isDirty();
            };

            this.save = function save() {
                return restrictionsEditorFunctionBindings.save && restrictionsEditorFunctionBindings.save() || $q.when();
            };

            this.getStepId = function getStepId() {
                return stepDetails.id;
            };

            this.goToStep = function goToStep() {
                wizardManager.goToStepWithId(this.getStepId());
            };
        }

        return {
            createRestrictionsStepHandler: function(wizardManager, restrictionsEditorFunctionBindings, stepProperties) {
                return new RestrictionsStepHandler(wizardManager, restrictionsEditorFunctionBindings, stepProperties);
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('removeComponentServiceModule', ['removeComponentServiceInterfaceModule', 'gatewayProxyModule'])

.factory('removeComponentService', ['$q', '$log', 'extend', 'gatewayProxy', 'RemoveComponentServiceInterface', function($q, $log, extend, gatewayProxy, RemoveComponentServiceInterface) {

    var REMOVE_COMPONENT_CHANNEL_ID = "RemoveComponent";

    var removeComponentService = function(gatewayId) {
        this.gatewayId = gatewayId;

        gatewayProxy.initForService(this, ["removeComponent"]);
    };

    removeComponentService = extend(RemoveComponentServiceInterface, removeComponentService);

    return new removeComponentService(REMOVE_COMPONENT_CHANNEL_ID);

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('restrictionsCriteriaServiceModule', [])
    .service('restrictionsCriteriaService', function() {

        var ALL = {},
            ANY = {};

        function setupCriteria(criteria, id, boolValue) {
            Object.defineProperty(criteria, 'id', {
                writable: false,
                value: id
            });
            Object.defineProperty(criteria, 'label', {
                writable: false,
                value: 'se.cms.restrictions.criteria.' + id
            });
            Object.defineProperty(criteria, 'editLabel', {
                writable: false,
                value: 'se.cms.restrictions.criteria.select.' + id
            });
            Object.defineProperty(criteria, 'value', {
                writable: false,
                value: boolValue
            });
        }
        setupCriteria(ALL, 'all', false);
        setupCriteria(ANY, 'any', true);

        var restrictionCriteriaOptions = [ALL, ANY];

        this.getRestrictionCriteriaOptions = function() {
            return restrictionCriteriaOptions;
        };

    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restrictionsServiceModule
 * @requires restrictionsRestServiceModule
 * @requires structuresRestServiceModule
 * @requires typeStructureRestServiceModule
 * @description
 * This module provides the {@link restrictionsServiceModule.service:restrictionsService restrictionsService} service used to consolidate business logic for SAP Hybris platform CMS restrictions.
 */
angular.module('restrictionsServiceModule', [
    'restrictionsRestServiceModule',
    'structuresRestServiceModule',
    'typeStructureRestServiceModule',
    'yLoDashModule'
])

/**
 * @ngdoc service
 * @name restrictionsServiceModule.service:restrictionsService
 * @requires restrictionsRestService
 * @requires structureModeManagerFactory
 * @requires structuresRestService
 * @description
 * Service that concerns business logic tasks related to CMS restrictions in the SAP Hybris platform.
 */
.service('restrictionsService', ['lodash', 'restrictionsRestService', 'structureModeManagerFactory', 'structuresRestService', 'typeStructureRestService', function(lodash,
    restrictionsRestService,
    structureModeManagerFactory,
    structuresRestService,
    typeStructureRestService
) {

    var modeManager = structureModeManagerFactory.createModeManager(["add", "edit", "create"]);

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getAllRestrictions
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * 
     * @deprecated since 6.4
     * 
     * @returns {Array} All restrictions in the system (any type of restriction).
     */
    this.getAllRestrictions = function() {
        return restrictionsRestService.get();
    };

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getStructureApiUri
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * @param {String} mode The structure mode.
     * @param {String} mode Optional typecode, if omited will leave a placeholder in URI that will be replaced with the item.typeCode.
     * @returns {String} A URI for the structure of restrictions, given a structure mode
     */
    this.getStructureApiUri = function getStructureApiUri(mode, typeCode) {
        modeManager.validateMode(mode);
        return structuresRestService.getUriForContext(mode, typeCode);
    };

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getContentApiUri
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * @param {Object} uriContext The {@link resourceLocationsModule.object:UriContext uriContext}, as defined on the resourceLocationModule.
     * @returns {String} A URI to CRUD restrictions, to a given site/catalog/version
     */
    this.getContentApiUri = function getContentApiUri(uriContext) {
        return restrictionsRestService.getContentApiUri(uriContext);
    };

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getById
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * 
     * @deprecated since 6.4
     * 
     * @param {String} restrictionId Identifier for a given restriction.
     * @return {Object} The restriction matching the given ID.
     */
    this.getById = function getById(restrictionId) {
        return restrictionsRestService.getById(restrictionId).then(function(restriction) {
            return restriction;
        });
    };

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getPagedRestrictionsForType
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * @param {String} restrictionTypeCode Code for the restriction type.
     * @param {String} mask A string value sent to the server upon fetching a page to further restrict the search, it is sent as query string "mask".
     * @param {String} pageSize The maximum size of each page requested from the backend.
     * @param {String} currentPage Current page number.
     * @return {Object} The restriction matching the identifier passed as parameter.
     */
    this.getPagedRestrictionsForType = function(restrictionTypeCode, mask, pageSize, currentPage) {
        return restrictionsRestService.get({
            pageSize: pageSize,
            currentPage: currentPage,
            mask: mask,
            sort: 'name:ASC',
            params: "typeCode:" + restrictionTypeCode
        });
    };

    /**
     * @ngdoc method
     * @name restrictionsServiceModule.service:restrictionsService#getPagedRestrictionsForType
     * @methodOf restrictionsServiceModule.service:restrictionsService
     * @returns {Array} An array of restriction TypeCodes that are supported by SmartEdit.
     */
    this.getSupportedRestrictionTypeCodes = function getSupportedRestrictionTypeCodes() {
        return typeStructureRestService.getStructuresByCategory('RESTRICTION').then(function(structures) {
            return lodash.map(structures, function(structure) {
                return structure.code;
            });
        });
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('rulesAndPermissionsRegistrationModule', [
    'catalogServiceModule',
    'catalogVersionPermissionModule',
    'catalogVersionRestServiceModule',
    'pageServiceModule',
    'permissionServiceModule',
    'sharedDataServiceModule',
    'siteServiceModule'
])

.run(['catalogService', 'catalogVersionPermissionService', 'catalogVersionRestService', 'pageService', 'permissionService', 'sharedDataService', 'siteService', '$q', function(
    catalogService,
    catalogVersionPermissionService,
    catalogVersionRestService,
    pageService,
    permissionService,
    sharedDataService,
    siteService,
    $q
) {

    // Rules
    permissionService.registerRule({
        names: ['se.write.page', 'se.write.slot', 'se.write.component'],
        verify: function(permissionNameObjs) {
            var promises = permissionNameObjs.map(function(permissionNameObject) {
                if (permissionNameObject.context) {
                    return catalogVersionPermissionService.hasWritePermission(
                        permissionNameObject.context.catalogId,
                        permissionNameObject.context.catalogVersion
                    );
                } else {
                    return catalogVersionPermissionService.hasWritePermissionOnCurrent();
                }
            });

            var onSuccess = function(result) {
                return result.reduce(function(acc, val) {
                    return acc && val;
                }, true) === true;
            };

            var onError = function() {
                return false;
            };

            return $q.all(promises).then(onSuccess, onError);
        }
    });

    permissionService.registerRule({
        names: ['se.read.page', 'se.read.slot', 'se.read.component'],
        verify: function() {
            return catalogVersionPermissionService.hasReadPermissionOnCurrent();
        }
    });

    permissionService.registerRule({
        names: ['se.page.belongs.to.experience'],
        verify: function() {
            return sharedDataService.get('experience').then(function(experience) {
                return experience.pageContext.catalogVersionUuid === experience.catalogDescriptor.catalogVersionUuid;
            });
        }
    });

    /**
     * Show the clone icon:
     * - If a page belonging to an active catalog version is a primary page, whose copyToCatalogsDisabled flag is set to false and has at-least one clonable target.
     * - If a page belonging to a non active catalog version has at-least one clonable target.
     */
    permissionService.registerRule({
        names: ['se.cloneable.page'],
        verify: function() {

            return sharedDataService.get('experience').then(function(experience) {

                var pageUriContext = {
                    CURRENT_CONTEXT_SITE_ID: experience.pageContext.siteId,
                    CURRENT_CONTEXT_CATALOG: experience.pageContext.catalogId,
                    CURRENT_CONTEXT_CATALOG_VERSION: experience.pageContext.catalogVersion
                };

                return pageService.getCurrentPageInfo().then(function(pageInfo) {
                    return catalogVersionRestService.getCloneableTargets(pageUriContext).then(function(targets) {

                        if (experience.pageContext.active) {
                            return targets.versions.length > 0 && pageInfo.defaultPage && !pageInfo.copyToCatalogsDisabled;
                        }

                        return targets.versions.length > 0;

                    });
                });
            });

        }
    });

    // Permissions
    permissionService.registerPermission({
        aliases: ['se.add.component'],
        rules: ['se.write.slot', 'se.write.component', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.read.restriction', 'se.read.page'],
        rules: ['se.read.page']
    });

    permissionService.registerPermission({
        aliases: ['se.edit.page'],
        rules: ['se.write.page']
    });

    permissionService.registerPermission({
        aliases: ['se.sync.catalog'],
        rules: ['se.write.page', 'se.write.slot', 'se.write.component']
    });

    permissionService.registerPermission({
        aliases: ['se.sync.slot.context.menu', 'se.sync.page', 'se.sync.slot.indicator'],
        rules: ['se.write.page', 'se.write.slot', 'se.write.component', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.edit.navigation'],
        rules: ['se.write.component']
    });

    permissionService.registerPermission({
        aliases: ['se.context.menu.remove.component'],
        rules: ['se.write.slot', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.slot.context.menu.shared.icon'],
        rules: ['se.read.slot']
    });

    permissionService.registerPermission({
        aliases: ['se.slot.context.menu.visibility'],
        rules: ['se.read.slot', 'se.write.component', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.clone.page'],
        rules: ['se.cloneable.page']
    });

    permissionService.registerPermission({
        aliases: ['se.context.menu.edit.component'],
        rules: ['se.write.component', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.context.menu.drag.and.drop.component'],
        rules: ['se.write.slot', 'se.write.component', 'se.page.belongs.to.experience']
    });

    permissionService.registerPermission({
        aliases: ['se.edit.page.link'],
        rules: ['se.write.page', 'se.page.belongs.to.experience']
    });
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('slotRestrictionsServiceModule', ['yLoDashModule', 'gatewayProxyModule'])
    .service('slotRestrictionsService', ['gatewayProxy', function(gatewayProxy) {

        this.getAllComponentTypesSupportedOnPage = function() {};

        this.getSlotRestrictions = function() {};

        gatewayProxy.initForService(this, ['getAllComponentTypesSupportedOnPage', 'getSlotRestrictions'], "SLOT_RESTRICTIONS");

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('synchronizationPollingServiceModule', ['renderServiceInterfaceModule', 'componentHandlerServiceModule', 'eventServiceModule', 'timerModule', 'gatewayProxyModule', 'crossFrameEventServiceModule', 'resourceModule', 'synchronizationConstantsModule', 'catalogServiceModule'])
    .run(['syncPollingService', function(syncPollingService) {
        syncPollingService.initSyncPolling();
    }])
    .factory('syncPollingService', ['$q', 'SYNCHRONIZATION_POLLING', 'EVENTS', 'OVERLAY_RERENDERED_EVENT', 'gatewayProxy', 'componentHandlerService', 'timerService', 'crossFrameEventService', 'systemEventService', 'synchronizationResource', 'catalogService', function($q, SYNCHRONIZATION_POLLING, EVENTS, OVERLAY_RERENDERED_EVENT, gatewayProxy, componentHandlerService, timerService, crossFrameEventService, systemEventService, synchronizationResource, catalogService) {

        var SyncPollingService = function(gatewayId) {
            this.gatewayId = gatewayId;

            gatewayProxy.initForService(this);
        };

        SyncPollingService.prototype.getSyncStatus = function(pageUUID, uriContext) {
            if (this.syncStatus[pageUUID] && pageUUID === this.syncStatus[pageUUID].itemId) {
                return $q.when(this.syncStatus[pageUUID]);
            } else {
                try {
                    if (componentHandlerService.getPageUUID()) {
                        this.syncPollingTimer.restart(this.refreshInterval);
                        return this._fetchSyncStatus(componentHandlerService.getPageUUID(), uriContext).then(function(syncStatus) {
                            this.syncStatus[syncStatus.itemId] = syncStatus;
                            return syncStatus;
                        }.bind(this));
                    }
                } catch (e) {
                    if (e.name === "InvalidStorefrontPageError") {
                        this.syncPollingTimer.stop();
                        return this._fetchSyncStatus(pageUUID, uriContext);
                    } else {
                        throw e;
                    }
                }

            }
        };

        SyncPollingService.prototype._fetchSyncStatus = function(_pageUUID, uriContext) {
            try {
                var pageUUID = _pageUUID || componentHandlerService.getPageUUID();
                if (pageUUID) {
                    return catalogService.isContentCatalogVersionNonActive().then(function(isContentCatalogVersionNonActive) {
                        if (isContentCatalogVersionNonActive) {

                            return catalogService.getContentCatalogActiveVersion(uriContext).then(function(activeVersion) {
                                return synchronizationResource.getPageSynchronizationGetRestService(uriContext).get({
                                    pageUid: pageUUID,
                                    target: activeVersion
                                }).then(function(syncStatus) {
                                    if (JSON.stringify(syncStatus) !== JSON.stringify(this.syncStatus[syncStatus.itemId])) {
                                        crossFrameEventService.publish(SYNCHRONIZATION_POLLING.FAST_FETCH, syncStatus);
                                    }
                                    this.syncStatus[syncStatus.itemId] = syncStatus;

                                    return syncStatus;
                                }.bind(this));
                            }.bind(this));

                        } else {
                            return $q.reject();
                        }
                    }.bind(this));
                }
            } catch (e) {
                if (e.name === "InvalidStorefrontPageError") {
                    this.syncPollingTimer.stop();
                    return $q.reject();
                } else {
                    throw e;
                }
            }
            return $q.when({});
        };

        SyncPollingService.prototype.changePollingSpeed = function(eventId, itemId) {
            if (eventId === SYNCHRONIZATION_POLLING.SPEED_UP) {
                this.syncStatus = {};
                if (itemId && this.triggers.indexOf(itemId) === -1) {
                    this.triggers.push(itemId);
                }

                this.refreshInterval = SYNCHRONIZATION_POLLING.FAST_POLLING_TIME;
            } else {
                if (itemId) {
                    this.triggers.splice(this.triggers.indexOf(itemId), 1);
                }
                if (this.triggers.length === 0) {
                    this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
                }
            }

            this.syncPollingTimer.restart(this.refreshInterval);

        };

        SyncPollingService.prototype.initSyncPolling = function() {
            this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
            this.triggers = [];
            this.syncStatus = {};

            var changePolling = this.changePollingSpeed.bind(this);

            systemEventService.registerEventHandler(SYNCHRONIZATION_POLLING.SPEED_UP, changePolling);
            systemEventService.registerEventHandler(SYNCHRONIZATION_POLLING.SLOW_DOWN, changePolling);

            crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FETCH_SYNC_STATUS_ONCE, function(eventId, pageUUID) {
                this._fetchSyncStatus.bind(this)(pageUUID);
            }.bind(this));

            crossFrameEventService.subscribe(OVERLAY_RERENDERED_EVENT, function() {
                if (this.syncPollingTimer.isActive()) {
                    this._fetchSyncStatus.bind(this)();
                }
            }.bind(this));

            crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
                if (this.syncPollingTimer.isActive()) {
                    this.syncPollingTimer.stop();
                }
            }.bind(this));

            this.syncPollingTimer = timerService.createTimer(this._fetchSyncStatus.bind(this), this.refreshInterval);
        };


        SyncPollingService.prototype.performSync = function(array, uriContext) {
            return catalogService.isContentCatalogVersionNonActive(uriContext).then(function(isNonActive) {
                if (isNonActive) {
                    return catalogService.getContentCatalogActiveVersion(uriContext).then(function(activeVersion) {
                        return synchronizationResource.getPageSynchronizationPostRestService(uriContext).save({
                            target: activeVersion,
                            items: array
                        });
                    });
                } else {
                    return $q.reject();
                }
            });
        };


        return new SyncPollingService('syncPollingService');

    }]);

angular.module('cmssmarteditContainerTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemDropdownTemplate.html',
    "<se-dropdown data-field=\"cmsItemDropdownCtrl.field\"\n" +
    "    data-qualifier=\"cmsItemDropdownCtrl.qualifier\"\n" +
    "    data-model=\"cmsItemDropdownCtrl.model\"\n" +
    "    data-id=\"cmsItemDropdownCtrl.id\"\n" +
    "    data-item-template-url=\"cmsItemDropdownCtrl.itemTemplateUrl\"></se-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemDropdownWrapperTemplate.html',
    "<cms-item-dropdown data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></cms-item-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemSearchTemplate.html',
    "<div class=\"y-select-item-printer\">\n" +
    "    <div>{{item.name}}</div>\n" +
    "    <div>{{item.uid}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/componentMenuTemplate.html',
    "<div class=\"se-component-menu\"\n" +
    "    data-recompile-dom=\"$ctrl._recompileDom\"\n" +
    "    data-ng-class=\"{ 'se-component-menu__localized': $ctrl.hasMultipleContentCatalogs }\">\n" +
    "\n" +
    "    <y-tabset class=\"se-component-menu--tabs\"\n" +
    "        data-tabs-list=\"$ctrl.tabsList\"\n" +
    "        data-model=\"$ctrl.tabsList\"\n" +
    "        data-num-tabs-displayed=\"2\">\n" +
    "    </y-tabset>\n" +
    "\n" +
    "    <div class=\"se-component-menu--help-tip\"\n" +
    "        data-translate=\"se.cms.componentmenu.label.draganddrop\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/componentMenuWrapperTemplate.html',
    "<component-menu action-item=\"item\"></component-menu>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/catalogVersionTemplate.html',
    "<div class=\"se-component-menu--select-local\">\n" +
    "    <span class=\"hyicon hyicon-globe se-component-menu--select-globe\"\n" +
    "        data-ng-if=\"item.isCurrentCatalog == false\"></span>\n" +
    "    <span class=\"se-component-menu--select-text\">{{item.catalogName | l10n}} - {{item.catalogVersionId}}</span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentInfoPopoverTemplate.html',
    "<div class=\"se-component-item--visibility-tooltip\"\n" +
    "    data-translate=\"se.cms.component.display.off.tooltip\"></div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentItemTemplate.html',
    "<div class=\"se-component-item\">\n" +
    "    <div class=\"smartEditComponent se-component-item--image\"\n" +
    "        data-smartedit-component-id=\"{{$ctrl.componentInfo.uid}}\"\n" +
    "        data-smartedit-component-uuid=\"{{$ctrl.componentInfo.uuid}}\"\n" +
    "        data-smartedit-component-type=\"{{$ctrl.componentInfo.typeCode}}\">\n" +
    "        <img data-ng-src=\"{{$ctrl.imageRoot}}/images/component_default.png\"\n" +
    "            class=\"se-component-item--image-element\"\n" +
    "            alt=\"{{$ctrl.componentInfo.name}}\"\n" +
    "            title=\"{{$ctrl.componentInfo.name}} - {{$ctrl.componentInfo.typeCode}}\" />\n" +
    "    </div>\n" +
    "    <div class=\"se-component-item--details-container\">\n" +
    "        <div class=\"se-component-item--details\"\n" +
    "            title=\"{{$ctrl.componentInfo.name}} - {{$ctrl.componentInfo.typeCode}}\">\n" +
    "            <div class=\"se-component-item--details-name\">{{$ctrl.componentInfo.name}}</div>\n" +
    "            <div class=\"se-component-item--details-type\">{{$ctrl.componentInfo.typeCode}}</div>\n" +
    "        </div>\n" +
    "        <div data-ng-if=\"!$ctrl.componentInfo.visible\"\n" +
    "            class=\"se-component-item--visibility\"\n" +
    "            data-y-popover\n" +
    "            data-trigger=\"'hover'\"\n" +
    "            data-placement=\"'bottom'\"\n" +
    "            data-template-url=\"'componentInfoPopoverTemplate.html'\">\n" +
    "            <span class=\"hyicon hyicon-unpowered\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentSearchTemplate.html',
    "<div class=\"se-input-group se-component-search\">\n" +
    "    <div class=\"se-input-group--addon se-component-search--addon\">\n" +
    "        <span class=\"hyicon hyicon-search se-component-search--search-icon\"></span>\n" +
    "    </div>\n" +
    "    <input type=\"text\"\n" +
    "        class=\"se-input-group--input se-component-search--input\"\n" +
    "        name=\"search-term\"\n" +
    "        data-ng-model=\"$ctrl.searchTerm\"\n" +
    "        data-ng-model-options=\"{debounce: 500}\"\n" +
    "        placeholder=\"{{$ctrl.placeholder | translate}}\">\n" +
    "    <div data-ng-show=\"$ctrl.showResetButton\"\n" +
    "        class=\"se-input-group--addon se-component-search--addon\"\n" +
    "        data-ng-click=\"$ctrl._resetSearchBox()\">\n" +
    "        <span class=\"glyphicon glyphicon-remove-sign se-component-search--reset-icon\"></span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentTypeTemplate.html',
    "<div class=\"se-component-item\">\n" +
    "\n" +
    "    <div class=\"smartEditComponent se-component-item--image\"\n" +
    "        data-smartedit-component-type=\"{{$ctrl.typeInfo.code}}\">\n" +
    "        <img class=\"se-component-item--image-element\"\n" +
    "            data-ng-src=\"{{$ctrl.imageRoot}}/images/component_default.png\"\n" +
    "            alt=\"{{$ctrl.typeInfo.i18nKey | translate}}\"\n" +
    "            title=\"{{$ctrl.typeInfo.i18nKey | translate}}\" />\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-component-item--details-container\">\n" +
    "        <div class=\"se-component-item--details\">\n" +
    "            <div class=\"se-component-item--details-type\"\n" +
    "                title=\"{{$ctrl.typeInfo.i18nKey | translate}}\"\n" +
    "                data-translate=\"{{$ctrl.typeInfo.i18nKey}}\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentTypesTabTemplate.html',
    "<component-search data-on-change=\"$ctrl.onSearchTermChanged(searchTerm)\"\n" +
    "    data-placeholder=\"se.cms.componentmenu.search.type.placeholder\"></component-search>\n" +
    "<div class=\"se-component-menu--item-container\">\n" +
    "    <div class=\"se-component-menu--overflow\">\n" +
    "        <div class=\"se-component-menu--results se-component-menu--results__types\">\n" +
    "            <div class=\"se-component-menu--item-wrap\"\n" +
    "                data-ng-repeat=\"componentType in $ctrl.componentTypes | nameFilter:$ctrl.searchTerm track by $id(componentType)\">\n" +
    "                <component-type data-type-info=\"componentType\"></component-type>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentTypesTabWrapperTemplate.html',
    "<component-types-tab></component-types-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentsTabTemplate.html',
    "<div data-ng-if=\"$ctrl.hasMultipleContentCatalogs == true\">\n" +
    "    <y-select class=\"se-component-menu--select\"\n" +
    "        data-id=\"uuid\"\n" +
    "        data-ng-model=\"$ctrl.selectedCatalogVersionId\"\n" +
    "        data-on-change=\"$ctrl.onCatalogVersionChange\"\n" +
    "        data-fetch-strategy=\"$ctrl.catalogVersionsFetchStrategy\"\n" +
    "        data-item-template=\"::$ctrl.catalogVersionTemplate\"\n" +
    "        data-search-enabled=\"false\" />\n" +
    "</div>\n" +
    "\n" +
    "<component-search data-on-change=\"$ctrl.onSearchTermChanged(searchTerm)\"\n" +
    "    data-placeholder=\"se.cms.componentmenu.search.placeholder\"></component-search>\n" +
    "\n" +
    "<div class=\"se-component-menu--item-container\"\n" +
    "    recompile-dom=\"$ctrl.resetComponentsList\">\n" +
    "    <y-infinite-scrolling data-ng-if=\"$ctrl.isActive\"\n" +
    "        class=\"se-component-menu--infinite-scroll\"\n" +
    "        data-drop-down-class=\"ySEComponents\"\n" +
    "        data-page-size=\"10\"\n" +
    "        data-mask=\"$ctrl.searchTerm\"\n" +
    "        data-fetch-page=\"$ctrl.loadComponentItems\"\n" +
    "        data-context=\"$ctrl\">\n" +
    "        <div class=\"se-component-menu--results se-component-menu--results__custom\">\n" +
    "            <component-item class=\"se-component-menu--item-wrap\"\n" +
    "                data-component-info=\"item\"\n" +
    "                data-ng-repeat=\"item in $ctrl.items track by item.uid\"></component-item>\n" +
    "\n" +
    "        </div>\n" +
    "    </y-infinite-scrolling>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentsTabWrapperTemplate.html',
    "<div>\n" +
    "    <components-tab has-multiple-content-catalogs=\"model.componentsTab.hasMultipleContentCatalogs\"\n" +
    "        is-active=\"model.componentsTab.active\"></components-tab>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/componentEditor/componentEditorTemplate.html',
    "<generic-editor data-id=\"compCtrl.tabId\"\n" +
    "    data-smartedit-component-id=\"compCtrl.componentId\"\n" +
    "    data-smartedit-component-type=\"compCtrl.componentType\"\n" +
    "    data-structure=\"compCtrl.tabStructure\"\n" +
    "    data-structure-api=\"compCtrl.structureApi\"\n" +
    "    data-content=\"compCtrl.content\"\n" +
    "    data-submit=\"compCtrl.saveTab\"\n" +
    "    data-reset=\"compCtrl.resetTab\"\n" +
    "    data-custom-on-submit=\"compCtrl.onSubmit\"\n" +
    "    data-is-dirty=\"compCtrl.isDirtyTab\"\n" +
    "    data-get-api=\"compCtrl.getApi($api);\">\n" +
    "</generic-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/componentEditor/componentEditorWrapperTemplate.html',
    "<y-message data-ng-if=\"$ctrl.showDisclaimer\"\n" +
    "    data-type=\"info\"\n" +
    "    data-message-id=\"VisibilityTab.DisplayComponentOffDisclaimer\">\n" +
    "    <message-description>\n" +
    "        <translate>se.cms.editortabset.visibilitytab.disclaimer</translate>\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "\n" +
    "<component-editor data-tab-id=\"$ctrl.tabId\"\n" +
    "    data-component-id=\"$ctrl.componentId\"\n" +
    "    data-component-type=\"$ctrl.componentType\"\n" +
    "    data-tab-structure=\"$ctrl.tabStructure\"\n" +
    "    data-structure-api=\"$ctrl.structureApi\"\n" +
    "    data-save-tab=\"$ctrl.saveTab\"\n" +
    "    data-reset-tab=\"$ctrl.resetTab\"\n" +
    "    data-cancel-tab=\"$ctrl.cancelTab\"\n" +
    "    data-component-info=\"$ctrl.componentInfo\"\n" +
    "    data-is-dirty-tab=\"$ctrl.isDirtyTab\"\n" +
    "    data-content=\"$ctrl.content\">\n" +
    "</component-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/adminTabTemplate.html',
    "<admin-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</admin-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/basicTabTemplate.html',
    "<basic-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</basic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/genericTabTemplate.html',
    "<generic-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</generic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/visibilityTabTemplate.html',
    "<visibility-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</visibility-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/restrictionEditor/componentRestrictionsEditorTemplate.html',
    "<restrictions-editor data-editable=\"true\"\n" +
    "    data-on-restrictions-changed=\"componentRestrictionsEditorCtrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "    data-get-restriction-types=\"componentRestrictionsEditorCtrl.getRestrictionTypes()\"\n" +
    "    data-get-supported-restriction-types=\"componentRestrictionsEditorCtrl.getSupportedRestrictionTypes()\"\n" +
    "    data-item=\"componentRestrictionsEditorCtrl.model\"\n" +
    "    data-restrictions=\"componentRestrictionsEditorCtrl.model.restrictions\">\n" +
    "</restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/restrictionEditor/componentRestrictionsEditorWrapperTemplate.html',
    "<component-restrictions-editor data-model=\"model\"></component-restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/editorTabset/editorTabsetTemplate.html',
    "<y-tabset data-model=\"model\"\n" +
    "    tabs-list=\"tabsList\"\n" +
    "    data-num-tabs-displayed=\"{{numTabsDisplayed}}\"\n" +
    "    tab-control=\"tabControl\">\n" +
    "</y-tabset>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/catalogAwareSelector/catalogAwareSelectorTemplate.html',
    "<div data-ng-if=\"ctrl.listIsEmpty()\">\n" +
    "    <!--\n" +
    "        It's important to add the type=button. Otherwise HTML 5 will default it to type=submit, causing the generic\n" +
    "        editor to submit the form whenever the panel opens.\n" +
    "    -->\n" +
    "    <button id=\"catalog-aware-selector-add-item\"\n" +
    "        type=\"button\"\n" +
    "        data-ng-if=\"ctrl.editable\"\n" +
    "        class=\"y-add-btn\"\n" +
    "        data-ng-click=\"ctrl.openEditingPanel()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>\n" +
    "        <span translate=\"se.cms.catalogaware.newbutton.title\"\n" +
    "            translate-values=\"{catalogItemType: ctrl.catalogItemType}\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "\n" +
    "<div data-ng-if=\"!ctrl.listIsEmpty()\"\n" +
    "    class=\"se-catalog-aware-selector__content\">\n" +
    "    <div class=\"se-catalog-aware-selector__content__btn-wrapper\"\n" +
    "        data-ng-if=\"ctrl.editable\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"btn btn-link se-catalog-aware-selector__content__btn\"\n" +
    "            data-ng-click=\"ctrl.openEditingPanel()\">\n" +
    "            {{'se.cms.catalogaware.list.addmore' | translate}}\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <y-editable-list data-id=\"{{ctrl.id + '_list'}}\"\n" +
    "        data-item-template-url=\"ctrl.itemTemplate\"\n" +
    "        data-items=\"ctrl.itemsList\"\n" +
    "        data-on-change=\"ctrl.onListChange\"\n" +
    "        data-editable=\"ctrl.editable\"\n" +
    "        data-refresh=\"ctrl._refreshListWidget\"\n" +
    "        class=\"se-catalog-aware-selector__list\">\n" +
    "    </y-editable-list>\n" +
    "</div>\n" +
    "\n" +
    "<se-item-selector-panel data-panel-title=\"ctrl.currentOptions.panelTitle\"\n" +
    "    data-items-selected=\"ctrl.model\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-item-template=\"ctrl.itemTemplate\"\n" +
    "    data-on-change=\"ctrl.onPanelChange\"\n" +
    "    data-show-panel=\"ctrl.showPanel\"\n" +
    "    data-hide-panel=\"ctrl.closePanel\"\n" +
    "    data-catalog-item-type=\"ctrl.catalogItemType\"\n" +
    "    data-max-num-items=\"ctrl.maxNumItems\">\n" +
    "</se-item-selector-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/categorySelectorTemplate.html',
    "<se-catalog-aware-selector data-id=\"{{ctrl.id}}\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-item-template=\"ctrl.categoryTemplate\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-catalog-item-type-key=\"se.cms.catalogaware.catalogitemtype.category\"\n" +
    "    data-editable=\"ctrl.editable\"></se-catalog-aware-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/categoryTemplate.html',
    "<div class=\"row se-product-row\">\n" +
    "    <div ng-if=\"item !== undefined\">\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.name | l10n}}\">{{item.name | l10n}}</div>\n" +
    "        <div class=\"col-md-6 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.catalogId}} - {{item.catalogVersion}}\">{{item.catalogId}} - {{item.catalogVersion}}</div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"node !== undefined\">\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.name | l10n}}\">{{node.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.catalogId}} - {{node.catalogVersion}}\">{{node.catalogId}} - {{node.catalogVersion}}</div>\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <y-drop-down-menu data-ng-if=\"ctrl.treeOptions.dragEnabled\"\n" +
    "                data-dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "                data-selected-item=\"this\"\n" +
    "                class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/multiCategorySelectorTemplate.html',
    "<se-category-selector data-id=\"{{field.qualifier}}\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-editable=\"field.editable\"></se-category-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/itemSelectorPanel/catalogItemTemplate.html',
    "<span data-ng-bind-html=\"item.name | l10n\"></span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/itemSelectorPanel/itemSelectorPanelTemplate.html',
    "<y-slider-panel data-slider-panel-show=\"ctrl._internalShowPanel\"\n" +
    "    data-slider-panel-hide=\"ctrl._internalHidePanel\"\n" +
    "    data-slider-panel-configuration=\"ctrl.sliderConfiguration\"\n" +
    "    class=\"se-item-selector-panel\">\n" +
    "    <content recompile-dom=\"ctrl.resetSelector\"\n" +
    "        class=\"se-item-selector-panel__content\">\n" +
    "        <!-- Show the dropdown only if there's more than one catalogs to choose from -->\n" +
    "        <div data-ng-if=\"ctrl.catalogs.length > 1\"\n" +
    "            class=\"se-item-selector-panel__content__item\">\n" +
    "            <label class=\"control-label\">{{'se.cms.catalogaware.panel.catalogs.label' | translate}}</label>\n" +
    "            <y-select data-id=\"se-catalog-selector-dropdown\"\n" +
    "                data-ng-model=\"ctrl.catalogInfo.catalogId\"\n" +
    "                data-fetch-strategy=\"ctrl.catalogSelectorFetchStrategy\"\n" +
    "                data-item-template=\"ctrl.catalogItemTemplate\"\n" +
    "                data-on-change=\"ctrl._onCatalogSelectorChange\"\n" +
    "                data-reset=\"ctrl.resetCatalogSelector\"\n" +
    "                class=\"se-item-selector-panel__content__catalogs__yselect\">\n" +
    "            </y-select>\n" +
    "            <label data-ng-if=\"ctrl.catalogs.length !== 1\">{{ctrl.catalogInfo.name | l10n}}</label>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"se-item-selector-panel__content__item\">\n" +
    "            <label class=\"control-label\">{{'se.cms.catalogaware.panel.catalogsversion.label' | translate}}</label>\n" +
    "            <y-select data-id=\"se-catalog-version-selector-dropdown\"\n" +
    "                data-ng-model=\"ctrl.catalogInfo.catalogVersion\"\n" +
    "                data-fetch-strategy=\"ctrl.catalogVersionSelectorFetchStrategy\"\n" +
    "                data-on-change=\"ctrl._onCatalogVersionSelectorChange\"\n" +
    "                data-search-enabled=false\n" +
    "                data-reset=\"ctrl.resetCatalogVersionSelector\"\n" +
    "                class=\"se-item-selector-panel__content__catalog-version__yselect\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "        <!-- Show the multi-select-->\n" +
    "        <div class=\"se-item-selector-panel__content__item\"\n" +
    "            data-ng-show=\"ctrl.catalogInfo.catalogVersion\">\n" +
    "            <label class=\"control-label\">{{ctrl.catalogItemType}}</label>\n" +
    "            <y-select id=\"se-items-selector-dropdown\"\n" +
    "                multi-select=\"'true'\"\n" +
    "                controls=\"'true'\"\n" +
    "                ng-model=\"ctrl._internalItemsSelected\"\n" +
    "                on-change=\"ctrl._onItemsSelectorChange\"\n" +
    "                fetch-strategy=\"ctrl._internalItemsFetchStrategy\"\n" +
    "                reset=\"ctrl.resetItemsListSelector\"\n" +
    "                item-template=\"ctrl.itemTemplate\"\n" +
    "                is-read-only=\"!ctrl._isItemSelectorEnabled()\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "    </content>\n" +
    "</y-slider-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/multiProductSelectorTemplate.html',
    "<se-product-selector data-id=\"{{field.qualifier}}\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-editable=\"field.editable\"></se-product-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/productSelectorTemplate.html',
    "<se-catalog-aware-selector data-id=\"{{ctrl.id}}\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-item-template=\"ctrl.productTemplate\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-catalog-item-type-key=\"se.cms.catalogaware.catalogitemtype.product\"\n" +
    "    data-max-num-items=\"ctrl.maxNumItems\"></se-catalog-aware-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/productTemplate.html',
    "<div class=\"row se-product-row\">\n" +
    "    <div ng-if=\"item !== undefined\">\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <div class=\"se-default-product-img\">\n" +
    "                <div style=\"background-image: url('{{item.thumbnail.url}}');\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.name | l10n}}\">{{item.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog\">\n" +
    "            <div class=\"se-nowrap-ellipsis\"\n" +
    "                title=\"{{item.catalogId}} - {{item.catalogVersion}}\">{{item.catalogId}} - {{item.catalogVersion}}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"node !== undefined\">\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <div class=\"se-default-product-img\">\n" +
    "                <div style=\"background-image: url('{{node.thumbnail.url}}');\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-5 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.name | l10n}}\">{{node.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.catalogId}} - {{node.catalogVersion}}\">{{node.catalogId}} - {{node.catalogVersion}}</div>\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <y-drop-down-menu data-ng-if=\"ctrl.treeOptions.dragEnabled\"\n" +
    "                data-dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "                data-selected-item=\"this\"\n" +
    "                class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/cmsLinkToSelect/cmsLinkToSelectTemplate.html',
    "<se-dropdown data-field=\"ctrl.field\"\n" +
    "    data-qualifier=\"ctrl.qualifier\"\n" +
    "    data-model=\"ctrl.model\"\n" +
    "    data-id=\"ctrl.id\"></se-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/cmsLinkToSelect/cmsLinkToSelectWrapperTemplate.html',
    "<cms-link-to-select data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></cms-link-to-select>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/linkToggle/linkToggleTemplate.html',
    "<div class=\"se-link-toggle\">\n" +
    "\n" +
    "    <div class=\"radio se-link-toggle__radio\">\n" +
    "        <input type=\"radio\"\n" +
    "            name=\"linktoggle\"\n" +
    "            id=\"external-link\"\n" +
    "            data-ng-model=\"$ctrl.model.linkToggle.external\"\n" +
    "            data-ng-value=\"true\"\n" +
    "            data-ng-change=\"$ctrl.emptyUrlLink()\" />\n" +
    "        <label class=\"control-label\"\n" +
    "            for=\"external-link\">{{ 'se.editor.linkto.external.label' | translate}}</label>\n" +
    "    </div>\n" +
    "    <div class=\"radio se-link-toggle__radio\">\n" +
    "        <input type=\"radio\"\n" +
    "            name=\"linktoggle\"\n" +
    "            id=\"internal-link\"\n" +
    "            data-ng-model=\"$ctrl.model.linkToggle.external\"\n" +
    "            data-ng-value=\"false\"\n" +
    "            data-ng-change=\"$ctrl.emptyUrlLink()\" />\n" +
    "        <label class=\"control-label\"\n" +
    "            for=\"internal-link\">{{ 'se.editor.linkto.internal.label' | translate}}</label>\n" +
    "    </div>\n" +
    "\n" +
    "    <input type=\"text\"\n" +
    "        id=\"urlLink\"\n" +
    "        name=\"urlLink\"\n" +
    "        data-ng-model=\"$ctrl.model.linkToggle.urlLink\"\n" +
    "        data-ng-class=\"{'has-error':$ctrl.field.hasErrors}\"\n" +
    "        class=\"form-control\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/linkToggle/linkToggleWrapperTemplate.html',
    "<link-toggle data-field=\"field\"\n" +
    "    data-model=\"model\">\n" +
    "</link-toggle>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/errorsList/seErrorsListTemplate.html',
    "<div class=\"field-errors\">\n" +
    "    <div data-ng-repeat=\"error in ctrl.getSubjectErrors()\">{{ error.message | translate }}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/fileSelector/seFileSelectorTemplate.html',
    "<div class=\"seFileSelector {{::ctrl.customClass}}\">\n" +
    "    <label class=\" btn__fileSelector\"\n" +
    "        data-ng-show=\"!ctrl.disabled\">\n" +
    "        <img class=\"img-upload\"\n" +
    "            data-ng-src=\"{{::ctrl.uploadIcon}}\" />\n" +
    "        <p class=\"label label__fileUpload label__fileUpload-link\">{{::ctrl.labelI18nKey | translate}}</p>\n" +
    "        <input type=\"file\"\n" +
    "            class=\"hide\"\n" +
    "            accept=\"{{ctrl.buildAcceptedFileTypesList()}}\">\n" +
    "    </label>\n" +
    "    <label class=\" btn__fileSelector\"\n" +
    "        data-ng-show=\"ctrl.disabled\">\n" +
    "        <img class=\"img-upload\"\n" +
    "            data-ng-src=\"{{::ctrl.uploadIcon}}\" />\n" +
    "        <p class=\"label label__fileUpload\">{{::ctrl.labelI18nKey | translate}}</p>\n" +
    "    </label>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesCondensedTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    data-popover-append-to-body=\"true\"\n" +
    "    data-popover-placement=\"bottom\"\n" +
    "    data-popover-trigger=\"'mouseenter'\"\n" +
    "    tabindex=\"0\"\n" +
    "    class=\"media-container-advanced-information\">\n" +
    "    <img class=\"media-advanced-information--image\"\n" +
    "        data-ng-src=\"{{ctrl.advInfoIcon}}\"\n" +
    "        alt=\"{{ctrl.i18nKeys.INFORMATION | translate}}\"\n" +
    "        title=\"{{ctrl.i18nKeys.INFORMATION | translate}}\">\n" +
    "    <p class=\"media-advanced-information--p\">{{ctrl.i18nKeys.INFORMATION | translate}}</p>\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesContentTemplate.html',
    "<div class=\"se-adv-media-info\">\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-description\"\n" +
    "        data-ng-if=\"ctrl.description\">\n" +
    "        {{ctrl.i18nKeys.DESCRIPTION | translate}}: {{ctrl.description}}\n" +
    "    </span>\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-code\">\n" +
    "        {{ctrl.i18nKeys.CODE | translate}}: {{ctrl.code}}\n" +
    "    </span>\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-alt-text\"\n" +
    "        data-ng-if=\"ctrl.altText\">\n" +
    "        {{ctrl.i18nKeys.ALT_TEXT | translate}}: {{ctrl.altText}}\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    popover-append-to-body=\"true\"\n" +
    "    popover-placement=\"left\"\n" +
    "    popover-trigger=\"click\"\n" +
    "    tabindex=\"0\"\n" +
    "    class=\"media-advanced-information btn btn-subordinate\">\n" +
    "    {{ctrl.i18nKeys.INFORMATION | translate}}\n" +
    "    <img class=\"media-selector--preview--icon\" data-ng-src=\"{{ctrl.advInfoIcon}}\">\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaContainerField/seMediaContainerFieldTemplate.html',
    "<div class=\"se-media-container-field\">\n" +
    "    <div class=\"row\">\n" +
    "        <se-media-format class=\"col-sm-6 col-md-4 col-lg-3 se-media-container-cell\"\n" +
    "            ng-repeat=\"option in ctrl.field.options\"\n" +
    "            data-media-uuid=\"ctrl.model[ctrl.qualifier][option.id]\"\n" +
    "            data-is-under-edit=\"ctrl.isFormatUnderEdit(option.id)\"\n" +
    "            data-media-format=\"option.id\"\n" +
    "            data-errors=\"ctrl.field.messages\"\n" +
    "            data-on-file-select=\"ctrl.fileSelected(files, option.id)\"\n" +
    "            data-on-delete=\"ctrl.imageDeleted(option.id)\">\n" +
    "        </se-media-format>\n" +
    "\n" +
    "    </div>\n" +
    "    <se-media-upload-form data-ng-if=\"ctrl.image.file\"\n" +
    "        data-image=\"ctrl.image\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-on-upload-callback=\"ctrl.imageUploaded(uuid)\"\n" +
    "        data-on-cancel-callback=\"ctrl.resetImage()\"\n" +
    "        data-on-select-callback=\"ctrl.fileSelected(files)\">\n" +
    "    </se-media-upload-form>\n" +
    "    <se-errors-list data-errors=\"ctrl.fileErrors\"></se-errors-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaField/seMediaFieldTemplate.html',
    "<div class=\"media-field\">\n" +
    "    <se-media-selector ng-if=\"!ctrl.image.file\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-editor=\"ctrl.editor\"\n" +
    "        data-qualifier=\"ctrl.qualifier\">\n" +
    "    </se-media-selector>\n" +
    "    <se-file-selector ng-if=\"ctrl.showFileSelector()\"\n" +
    "        data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD_IMAGE_TO_LIBRARY\"\n" +
    "        data-accepted-file-types=\"ctrl.acceptedFileTypes\"\n" +
    "        data-on-file-select=\"ctrl.fileSelected(files)\"\n" +
    "        data-upload-icon=\"ctrl.uploadIconUrl\"\n" +
    "        data-image-root=\"ctrl.imageRoot\"></se-file-selector>\n" +
    "    <se-media-upload-form ng-if=\"ctrl.image.file\"\n" +
    "        data-image=\"ctrl.image\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-on-upload-callback=\"ctrl.imageUploaded(uuid)\"\n" +
    "        data-on-cancel-callback=\"ctrl.resetImage()\"\n" +
    "        data-on-select-callback=\"ctrl.fileSelected(files)\"></se-media-upload-form>\n" +
    "    <se-errors-list ng-if=\"ctrl.fileErrors.length > 0\"\n" +
    "        data-errors=\"ctrl.fileErrors\"></se-errors-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaFormat/seMediaFormatTemplate.html',
    "<div class=\"{{ctrl.mediaFormat}} se-media-format\">\n" +
    "    <div class=\"se-media-format-screentype\">\n" +
    "        {{ctrl.mediaFormatI18NKey | translate}}\n" +
    "    </div>\n" +
    "    <!-- when the image is already uploaded -->\n" +
    "    <div class=\"media-present\"\n" +
    "        data-ng-if=\"ctrl.isMediaPreviewEnabled()\">\n" +
    "        <div class=\"media-present-preview\">\n" +
    "            <se-media-preview data-image-url=\"ctrl.media.url\"></se-media-preview>\n" +
    "            <div class=\"se-media-preview-image-wrapper  se-media-format-image-wrapper\">\n" +
    "                <img class=\"thumbnail thumbnail--image-preview\"\n" +
    "                    data-ng-src=\"{{ctrl.media.url}}\">\n" +
    "            </div>\n" +
    "            <span class=\"media-preview-code se-media-format--code\">{{ctrl.media.code}}</span>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <se-media-advanced-properties-condensed data-adv-info-icon=\"ctrl.advInfoIconUrl\"\n" +
    "            data-code=\"ctrl.media.code\"\n" +
    "            data-description=\"ctrl.media.description\"\n" +
    "            data-alt-text=\"ctrl.media.altText\"></se-media-advanced-properties-condensed>\n" +
    "\n" +
    "        <se-file-selector data-custom-class=\" 'media-format-present-replace' \"\n" +
    "            data-label-i18n-key=\"ctrl.i18nKeys.REPLACE \"\n" +
    "            data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "            data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "            data-upload-icon=\"ctrl.replaceIconUrl \"\n" +
    "            data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "\n" +
    "        <a class=\"media-selector--preview__left remove-image btn btn-subordinate \"\n" +
    "            data-ng-click=\"ctrl.onDelete({format: ctrl.mediaFormat}) \">\n" +
    "            <img class=\"media-selector--preview--icon \"\n" +
    "                data-ng-src=\"{{ctrl.deleteIconUrl}} \"\n" +
    "                alt=\"{{ctrl.i18nKeys.REMOVE | translate}} \" />\n" +
    "            <p class=\"media-selector--preview__left--p media-selector--preview__left--p__error \">{{ctrl.i18nKeys.REMOVE | translate}}</p>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- when the image is not yet uploaded -->\n" +
    "    <div class=\"media-absent \"\n" +
    "        data-ng-if=\"ctrl.isMediaEditEnabled()\">\n" +
    "        <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD \"\n" +
    "            data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "            data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "            data-upload-icon=\"ctrl.uploadIconUrl \"\n" +
    "            data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- when the image is under edit -->\n" +
    "    <div data-ng-if=\"ctrl.isUnderEdit \"\n" +
    "        class=\"media-under-edit-parent\">\n" +
    "        <div class=\"media-is-under-edit \">\n" +
    "            <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD \"\n" +
    "                data-disabled=\"true\"\n" +
    "                data-custom-class=\" 'file-selector-disabled' \"\n" +
    "                data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "                data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "                data-upload-icon=\"ctrl.uploadIconDisabledUrl \"\n" +
    "                data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "        </div>\n" +
    "        <span class=\"media-preview-under-edit \">{{ctrl.i18nKeys.UNDER_EDIT | translate}}</span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"error-input help-block \"\n" +
    "        data-ng-repeat=\"error in ctrl.getErrors() \">\n" +
    "        {{error}}\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaPreview/seMediaPreviewContentTemplate.html',
    "<img class=\"preview-image\"\n" +
    "    data-ng-src=\"{{ctrl.imageUrl}}\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaPreview/seMediaPreviewTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    popover-append-to-body=\"true\"\n" +
    "    popover-placement=\"right\"\n" +
    "    popover-trigger=\"'outsideClick'\"\n" +
    "    tabindex=\"1\"\n" +
    "    class=\"media-preview\">\n" +
    "    <span class=\"hyicon hyicon-search media-preview-icon\"></span>\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaPrinterTemplate.html',
    "<div class=\"media-selector--preview\"\n" +
    "    data-ng-if=\"selected\">\n" +
    "    <div class=\"row\">\n" +
    "        <span class=\"col-xs-6\">\n" +
    "            <se-media-preview data-image-url=\"item.url\"></se-media-preview>{{printer.imagePreviewUrl}}\n" +
    "            <div class=\"se-media-preview-image-wrapper\">\n" +
    "                <img class=\"thumbnail thumbnail--image-preview\"\n" +
    "                    data-ng-src={{item.url}}>\n" +
    "            </div>\n" +
    "            <span class=\"media-preview-code se-media-printer--code\">{{item.code}}</span>\n" +
    "        </span>\n" +
    "        <span class=\"col-xs-6 media-selector--preview--2\">\n" +
    "            <se-media-advanced-properties class=\"media-selector--preview__right\"\n" +
    "                data-code=\"item.code\"\n" +
    "                data-description=\"item.description\"\n" +
    "                data-alt-text=\"item.altText\"\n" +
    "                data-adv-info-icon=\"printer.advInfoIcon\"></se-media-advanced-properties>\n" +
    "\n" +
    "            <a class=\"media-selector--preview__right replace-image btn btn-subordinate\"\n" +
    "                data-ng-click=\"ySelect.clear($select, $event)\">\n" +
    "                <p class=\"media-selector--preview__right--p\"\n" +
    "                    data-translate=\"se.upload.image.replace\"></p>\n" +
    "                <img class=\"media-selector--preview--icon\"\n" +
    "                    data-ng-src=\"{{printer.replaceIcon}}\" />\n" +
    "            </a>\n" +
    "            <a class=\"media-selector--preview__right remove-image btn btn-subordinate\"\n" +
    "                data-ng-click=\"ySelect.clear($select, $event)\">\n" +
    "                <p class=\"media-selector--preview__right--p\"\n" +
    "                    data-translate=\"se.media.format.remove\"></p>\n" +
    "                <img class=\"media-selector--preview--icon\"\n" +
    "                    data-ng-src=\"{{printer.deleteIcon}}\" />\n" +
    "            </a>\n" +
    "        </span>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"ySEMediaSearch\"\n" +
    "    data-ng-if=\"!selected\">\n" +
    "    <div class=\"row ySEMediaSearch-row\">\n" +
    "        <div class=\"ySEMediaSearchName col-xs-8\">{{item.code}}</div>\n" +
    "        <div class=\"ySEMediaSearchImage col-xs-4\">\n" +
    "            <img data-ng-src={{item.url}}\n" +
    "                class=\"pull-right\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaPrinterWrapperTemplate.html',
    "<se-media-printer data-item=\"item\"\n" +
    "    data-selected=\"selected\">"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaSelectorTemplate.html',
    "<div class=\"media-selector\">\n" +
    "    <y-select data-ng-disabled=\"!ctrl.field.editable\"\n" +
    "        data-id=\"{{ctrl.field.qualifier}}\"\n" +
    "        data-placeholder=\"ctrl.dropdownProperties.placeHolderI18nKey\"\n" +
    "        data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "        data-fetch-strategy=\"ctrl.fetchStrategy\"\n" +
    "        data-item-template=\"ctrl.mediaTemplate\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaUploadForm/seMediaUploadFieldTemplate.html',
    "<div class=\"form-control se-mu--fileinfo--wrapper\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"se-mu--fileinfo--wrapper-col1 col-xs-10\">\n" +
    "            <input type=\"text\"\n" +
    "                data-ng-class=\"{'se-mu--fileinfo--field--input': true, 'se-mu--fileinfo--field__error': ctrl.error}\"\n" +
    "                class=\"se-mu--fileinfo--field--input\"\n" +
    "                name=\"{{ctrl.field}}\"\n" +
    "                data-ng-model=\"ctrl.model[ctrl.field]\">\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"se-mu--fileinfo--wrapper-col2 col-xs-2\">\n" +
    "            <img ng-src=\"{{ctrl.assetsRoot}}/images/edit_icon.png\"\n" +
    "                class=\"se-mu--fileinfo--field--icon \"\n" +
    "                data-ng-if=\"ctrl.displayImage && !ctrl.error\" />\n" +
    "            <img ng-src=\"{{ctrl.assetsRoot}}/images/edit_icon_error.png\"\n" +
    "                class=\"se-mu--fileinfo--field--icon__error \"\n" +
    "                data-ng-if=\"ctrl.error\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaUploadForm/seMediaUploadFormTemplate.html',
    "<div class=\"se-media-upload-form\">\n" +
    "\n" +
    "    <div class=\"container-fluid se-media-upload-form--toolbar\">\n" +
    "        <div class=\"navbar-left se-media-upload-form--toolbar--title\">\n" +
    "            <h4 class=\"se-media-upload-form--toolbar--title--h4\">{{ctrl.i18nKeys.UPLOAD_IMAGE_TO_LIBRARY | translate}}</h4>\n" +
    "        </div>\n" +
    "        <div class=\"navbar-right se-media-upload-form--toolbar--buttons\">\n" +
    "            <div class=\"y-toolbar\">\n" +
    "                <div class=\"y-toolbar-cell\">\n" +
    "                    <button class=\"btn btn-subordinate btn-lg navbar-btn se-media-upload-btn__cancel\"\n" +
    "                        type=\"button\"\n" +
    "                        data-ng-click=\"ctrl.onCancel()\">{{ctrl.i18nKeys.UPLOAD_IMAGE_CANCEL | translate}}</button>\n" +
    "                </div>\n" +
    "                <div class=\"y-toolbar-cell\">\n" +
    "                    <button class=\"btn btn-default btn-lg navbar-btn se-media-upload-btn__submit\"\n" +
    "                        type=\"button\"\n" +
    "                        data-ng-click=\"ctrl.onMediaUploadSubmit()\">{{ctrl.i18nKeys.UPLOAD_IMAGE_SUBMIT | translate}}</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"container-fluid se-media-upload--filename\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"se-media-upload--filename-col1 col-xs-10\">\n" +
    "                <img ng-src=\"{{assetsRoot}}/images/image_placeholder.png\">\n" +
    "                <div class=\"se-media-upload--fn--name\">{{ctrl.getTruncatedName()}}</div>\n" +
    "            </div>\n" +
    "            <div class=\"se-media-upload--filename-col2 col-xs-2\">\n" +
    "                <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD_IMAGE_REPLACE\"\n" +
    "                    data-accepted-file-types=\"ctrl.acceptedFileTypes\"\n" +
    "                    data-on-file-select=\"ctrl.onSelectCallback({files: files})\"></se-file-selector>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <form>\n" +
    "        <div class=\"se-media-upload--fileinfo\">\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-description\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('description'), 'se-media-upload--fileinfo--label': true }\">{{ctrl.i18nKeys.DESCRIPTION | translate}}</label>\n" +
    "                <se-media-upload-field data-field=\"'description'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-description\"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('description')\">{{error}}</span>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-code\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('code')}\">{{ctrl.i18nKeys.CODE | translate}}*</label>\n" +
    "                <se-media-upload-field data-error=\"ctrl.hasError('code')\"\n" +
    "                    data-field=\"'code'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-code\"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('code')\">{{error}}</span>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-alt-text\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('altText') }\">{{ctrl.i18nKeys.ALT_TEXT | translate}}</label>\n" +
    "                <se-media-upload-field data-field=\"'altText'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-alt-text \"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('altText')\">{{error}}</span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </form>\n" +
    "    <span data-ng-if=\"ctrl.isUploading\"\n" +
    "        class=\"upload-image-in-progress\">\n" +
    "        <!--<img src=\"static-resources/images/spinner.png\"> {{ctrl.i18nKeys.UPLOADING | translate}}-->\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/templates/mediaContainerTemplate.html',
    "<se-media-container-field data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"\n" +
    "    data-qualifier=\"qualifier\"></se-media-container-field>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/templates/mediaTemplate.html',
    "<se-media-field data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"\n" +
    "    data-qualifier=\"qualifier\"></se-media-field>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/breadcrumb/seBreadcrumbTemplate.html',
    "<div class=\"ySEBreadcrumb\">\n" +
    "    <div data-ng-repeat=\"node in bc.breadcrumb\"\n" +
    "        class=\"ySEBreadcrumbNode\">\n" +
    "        <div class=\"ySEBreadcrumbInfo\"\n" +
    "            data-ng-class=\"{'ySEBreadcrumbInfo__last':$last}\">\n" +
    "            <span class=\"yNodeLevel\">{{node.formattedLevel | translate:node}}</span>\n" +
    "            <span class=\"yNodeName\">{{node.name}}</span>\n" +
    "        </div>\n" +
    "        <div class=\"ySEBreadcrumbDivider\"\n" +
    "            data-ng-if=\"$index < bc.breadcrumb.length - 1\">\n" +
    "            <img data-ng-src=\"{{bc.arrowIconUrl}}\"\n" +
    "                alt=\"\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodePicker/navigationNodePickerRenderTemplate.html',
    "<div class=\"col-sm-6 pull-right tree-node yNavigationPickerRenderer\"\n" +
    "    data-ng-click=\"ctrl.pick(this)\">\n" +
    "    <a data-translate=\"se.cms.navigationcomponent.management.node.selection.select.action\"\n" +
    "        class=\"btn btn-link yNavigationPickerRenderer__btn\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodePicker/seNavigationNodePickerTemplate.html',
    "<div class=\"categoryTable\">\n" +
    "\n" +
    "    <div class=\"tablehead clearfix hidden-xs ySENavigationTree-head\">\n" +
    "        <div data-translate=\"se.ytree.template.header.name\"\n" +
    "            class=\"col-md-offset-1 col-sm-5\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ytree data-node-uri=\"ctrl.nodeURI\"\n" +
    "        data-root-node-uid=\"ctrl.rootNodeUid\"\n" +
    "        data-node-template-url=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-node-actions=\"ctrl.actions\" />\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodeSelector/seNavigationNodeSelectorTemplate.html',
    "<div data-ng-if=\"nav.isReady()\">\n" +
    "    <div data-ng-if=\"nav.model[nav.qualifier]\"\n" +
    "        class=\"se-navigation-mode\">\n" +
    "\n" +
    "        <div class=\"se-navigation--node\">\n" +
    "            <se-breadcrumb class=\"se-navigation--node-breadcrumb\"\n" +
    "                data-node-uuid=\"nav.model[nav.qualifier]\"\n" +
    "                data-uri-context=\"nav.uriContext\"></se-breadcrumb>\n" +
    "            <div class=\"se-navigation--node-button\">\n" +
    "                <button class=\"btn btn-link btn-block se-navigation--button\"\n" +
    "                    data-ng-click=\"nav.remove($event)\">\n" +
    "                    <p data-translate=\"se.cms.navigationcomponent.management.node.selection.remove.action\"></p>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <navigation-editor data-uri-context=\"nav.uriContext\"\n" +
    "            data-read-only=\"true\"\n" +
    "            data-root-node-uid=\"nav.nodeUid\"></navigation-editor>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!nav.model[nav.qualifier]\">\n" +
    "\n" +
    "        <label data-translate=\"se.cms.navigationcomponent.management.node.selection.invite.action\"></label>\n" +
    "        <se-navigation-picker data-uri-context=\"nav.uriContext\"\n" +
    "            data-model=\"nav.model\"\n" +
    "            data-qualifier=\"nav.qualifier\"></se-navigation-picker>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/templates/navigationNodeSelectorWrapperTemplate.html',
    "<se-navigation-node-selector data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"></se-navigation-node-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/singleActiveCatalogAwareSelector/singleActiveCatalogAwareItemSelector/singleActiveCatalogAwareItemSelectorTemplate.html',
    "<!-- Product catalog selector -->\n" +
    "<div id=\"product-catalog\">\n" +
    "    <label class=\"control-label\"\n" +
    "        data-ng-if=\"ctrl.catalogs.length === 1\">{{ctrl.catalogName | l10n}}</label>\n" +
    "    <se-dropdown data-ng-if=\"ctrl.catalogs.length > 1\"\n" +
    "        data-field=\"ctrl.productCatalogField\"\n" +
    "        data-qualifier=\"'productCatalog'\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-id=\"se-catalog-selector-dropdown\"></se-dropdown>\n" +
    "</div>\n" +
    "\n" +
    "<!-- Item selector -->\n" +
    "<div>\n" +
    "    <label class=\"control-label\">{{ctrl.mainDropDownI18nKey | translate}}</label>\n" +
    "    <se-dropdown data-field=\"ctrl.field\"\n" +
    "        data-qualifier=\"ctrl.qualifier\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-id=\"se-items-selector-dropdown\"></se-dropdown>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/singleActiveCatalogAwareSelector/singleActiveCatalogAwareItemSelector/singleActiveCatalogAwareItemSelectorWrapperTemplate.html',
    "<single-active-catalog-aware-item-selector data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></single-active-catalog-aware-item-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/itemManagement/itemManagementTemplate.html',
    "<div>\n" +
    "    <generic-editor data-id=\"$ctrl.editorId\"\n" +
    "        data-smartedit-component-id=\"$ctrl.itemId\"\n" +
    "        data-smartedit-component-type=\"$ctrl.componentType\"\n" +
    "        data-structure-api=\"$ctrl.structureApi\"\n" +
    "        data-content=\"$ctrl.item\"\n" +
    "        data-content-api=\"$ctrl.contentApi\"\n" +
    "        data-is-dirty=\"$ctrl.isDirtyInternal\"\n" +
    "        data-submit=\"$ctrl.submit\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-reset=\"$ctrl.reset\">\n" +
    "    </generic-editor>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationEditorLinkTemplate.html',
    "<div class=\"nav-management-link-container\">\n" +
    "    <a class=\"nav-management-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-href=\"#!/navigations/{{$ctrl.siteId}}/{{$ctrl.catalog.catalogId}}/{{$ctrl.catalogVersion.version}}\"\n" +
    "        data-translate=\"se.cms.cataloginfo.navigationmanagement\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationEditorTemplate.html',
    "<div class=\"ySEAdd-Nav-button\"\n" +
    "    data-ng-if=\"!nav.readOnly\">\n" +
    "    <button class=\"y-add-btn\"\n" +
    "        data-ng-click=\"nav.actions.addTopLevelNode()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>\n" +
    "        <span data-translate=\"se.cms.navigationmanagement.add.top.level.node\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "<div class=\"ySEAdd-Nav-tree categoryTable\">\n" +
    "\n" +
    "    <div class=\"tablehead clearfix hidden-xs ySENavigationTree-head\">\n" +
    "        <div data-translate=\"se.ytree.template.header.name\"\n" +
    "            class=\"col-md-offset-1 col-sm-5\"></div>\n" +
    "        <div data-translate=\"se.ytree.template.header.type\"\n" +
    "            class=\"col-sm-6\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ytree data-node-uri=\"nav.nodeURI\"\n" +
    "        data-root-node-uid=\"nav.rootNodeUid\"\n" +
    "        data-node-template-url=\"nav.nodeTemplateUrl\"\n" +
    "        data-node-actions=\"nav.actions\"\n" +
    "        data-drag-options=\"nav.dragOptions\"></ytree>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationNodeRenderTemplate.html',
    "<div class=\"col-sm-1 pull-right dropdown tree-node ySENodeTemplate\"\n" +
    "    data-ng-show=\"!ctrl.isReadOnly()\">\n" +
    "    <y-drop-down-menu dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "        selected-item=\"this\"\n" +
    "        class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "</div>\n" +
    "<div class=\"col-sm-5 pull-right tree-node ySENodeTemplateTree\"> {{node.itemType || 'se.cms.navigationmanagement.navnode.objecttype.node' | translate}} </div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationTemplate.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"navigationEditorWrapper\"\n" +
    "    ng-if=\"nav.readOnly !== undefined\">\n" +
    "    <div class=\"navEditorHeaderWrapper\">\n" +
    "        <h1 class=\"ySEPage-list-title\"\n" +
    "            data-translate='se.cms.navigationmanagement.title'></h1>\n" +
    "        <h4 class=\"ySEPage-list-label\">{{nav.catalogName | l10n}} - {{nav.catalogVersion}}</h4>\n" +
    "    </div>\n" +
    "    <navigation-editor data-uri-context=\"nav.uriContext\"\n" +
    "        data-read-only=\"nav.readOnly\"></navigation-editor>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/entrySearchSelectorDropdownTemplate.html',
    "<y-select data-ng-if=\"ctrl.initialized\"\n" +
    "    data-id=\"{{ctrl.field.qualifier}}\"\n" +
    "    data-placeholder=\"ctrl.dropdownProperties.placeHolderI18nKey\"\n" +
    "    data-controls=\"'true'\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-fetch-strategy=\"ctrl.fetchStrategy\"\n" +
    "    data-reset=\"ctrl.reset\"\n" +
    "    data-item-template=\"ctrl.dropdownProperties.templateUrl\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/entrySearchSelectorTemplate.html',
    "<entry-search-selector data-model=\"model\"\n" +
    "    data-id=\"editor.id\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-field=\"field\"\n" +
    "    data-editor=\"editor\"></entry-search-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/handlerTemplates/itemSearchHandlerTemplate.html',
    "<div class=\"ySENavigationItemUid\">{{item.id}}</div>\n" +
    "<div class=\"ySENavigationItemTypeCode\">{{item.typeCode}}</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/handlerTemplates/mediaSearchHandlerTemplate.html',
    "<div class=\"ySENavigationMediaSearch\">\n" +
    "    <div class=\"row ySENavigationMediaSearch-row\">\n" +
    "        <div class=\"ySENavigationMediaSearchName\"\n" +
    "            title=\"{{item.code}}\">{{item.code}}</div>\n" +
    "        <div class=\"ySENavigationMediaSearchImage\">\n" +
    "            <img data-ng-src={{item.downloadUrl}}\n" +
    "                class=\"pull-right\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorAttributesTemplate.html',
    "<div class=\"ySENavigationNodeEditorAttributes\">\n" +
    "    <generic-editor data-ng-if=\"ctrl.isContentLoaded\"\n" +
    "        data-id=\"ctrl.tabId\"\n" +
    "        data-smartedit-component-id=\"ctrl.nodeUid\"\n" +
    "        data-smartedit-component-type=\"ctrl.typeCode\"\n" +
    "        data-structure=\"ctrl.tabStructure\"\n" +
    "        data-content-api=\"ctrl.contentApi\"\n" +
    "        data-content=\"ctrl.content\"\n" +
    "        data-uri-context=\"ctrl.uriContext\"\n" +
    "        data-submit=\"ctrl.submit\"\n" +
    "        data-reset=\"ctrl.reset\"\n" +
    "        data-is-dirty=\"ctrl.isDirty\"\n" +
    "        data-update-callback=\"ctrl.updateCallback\">\n" +
    "    </generic-editor>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorCreateEntryTemplate.html',
    "<div class=\"ySENavigationNodeEditorAddEntry\">\n" +
    "    <div data-ng-if=\"!ctrl.displayEditor\"\n" +
    "        class=\"ySEAdd-node-button\">\n" +
    "        <button id=\"navigation-node-editor-add-entry\"\n" +
    "            class=\"y-add-btn\"\n" +
    "            data-ng-click=\"ctrl.addNewEntry()\">\n" +
    "            <span class=\"hyicon hyicon-add\"></span>\n" +
    "            <span data-translate=\"se.cms.navigationmanagement.navnode.node.entry.add.new.message\"></span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.displayEditor\"\n" +
    "        class=\"ySEEntryData\">\n" +
    "        <generic-editor data-id=\"ctrl.tabId\"\n" +
    "            data-smartedit-component-type=\"ctrl.entryTypeCode\"\n" +
    "            data-structure=\"ctrl.tabStructure\"\n" +
    "            data-uri-context=\"ctrl.uriContext\"\n" +
    "            data-content=\"ctrl.entry\"\n" +
    "            data-is-dirty=\"ctrl.isDirty\"\n" +
    "            data-is-valid=\"ctrl.isValid\"\n" +
    "            data-submit=\"ctrl.submit\"\n" +
    "            data-reset=\"ctrl.reset\">\n" +
    "        </generic-editor>\n" +
    "\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"ySEEntryData--buttons\">\n" +
    "                <button id=\"navigation-node-editor-save-entry\"\n" +
    "                    class=\"btn btn-default btn-lg\"\n" +
    "                    data-ng-click=\"ctrl.saveEntry()\"\n" +
    "                    data-ng-disabled=\"ctrl.isDisabled()\">\n" +
    "                    <span data-ng-if=\"ctrl.isNewEntry()\"\n" +
    "                        data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.add\"></span>\n" +
    "                    <span data-ng-if=\"!ctrl.isNewEntry()\"\n" +
    "                        data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.update\"></span>\n" +
    "                </button>\n" +
    "                <button id=\"navigation-node-editor-cancel\"\n" +
    "                    class=\"btn btn-subordinate btn-lg\"\n" +
    "                    data-ng-click=\"ctrl.cancelEntry()\"\n" +
    "                    data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.cancel\"></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorEntryListTemplate.html',
    "<div class=\"ySENavigationNodeEditorEntryList\">\n" +
    "    <div>\n" +
    "        <label data-translate=\"se.cms.navigationmanagement.navnode.node.entries\"></label>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-repeat=\"entry in ctrl.entries track by $index\"\n" +
    "        class=\"nav-node-editor-entry-item\">\n" +
    "        <div class=\"row-fluid\">\n" +
    "            <div class=\"col-xs-10 nav-node-editor-entry-item__text\"\n" +
    "                data-ng-click=\"ctrl.onSelect(entry)\">\n" +
    "                <div class=\"nav-node-editor-entry-item__name\">\n" +
    "                    <span class=\"ng-class:{'error-input':ctrl.isInError(entry)}\">{{entry.title | l10n }}</span>\n" +
    "                </div>\n" +
    "                <div class=\"nav-node-editor-entry-item__type\">\n" +
    "                    <span class=\"ng-class:{'error-input':ctrl.isInError(entry)}\">{{entry.itemType }}</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 nav-node-editor-entry-item__menu\">\n" +
    "                <y-drop-down-menu dropdown-items=\"ctrl.dropdownItems\"\n" +
    "                    selected-item=\"entry\"\n" +
    "                    class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.entries.length === 0\">{{'se.cms.navigationmanagement.node.noentry.message'| translate}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorModalTemplate.html',
    "<navigation-node-editor data-submit=\"modalController.submit\"\n" +
    "    data-reset=\"modalController.reset\"\n" +
    "    data-is-dirty=\"modalController.isDirty\"\n" +
    "    data-uri-context=\"modalController.uriContext\"\n" +
    "    data-node-uid=\"modalController.target.nodeUid\"\n" +
    "    data-parent-uid=\"modalController.target.parentUid\"\n" +
    "    data-entry-index=\"modalController.target.entryIndex\">\n" +
    "</navigation-node-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorTemplate.html',
    "<div class=\"ySENavigationNodeEditor\">\n" +
    "    <se-breadcrumb data-node-uid=\"ctrl.nodeUid\"\n" +
    "        data-uri-context=\"ctrl.uriContext\"\n" +
    "        data-ng-if=\"ctrl.nodeUid\"></se-breadcrumb>\n" +
    "    <div class=\"row\">\n" +
    "        <navigation-node-editor-attributes data-submit=\"ctrl.submit\"\n" +
    "            data-reset=\"ctrl.reset\"\n" +
    "            data-is-dirty=\"ctrl.isDirty\"\n" +
    "            data-node-uid=\"ctrl.nodeUid\"\n" +
    "            data-parent-uid=\"ctrl.parentUid\"\n" +
    "            data-uri-context=\"ctrl.uriContext\" />\n" +
    "    </div>\n" +
    "    <div class=\"ySENavigationDivider\"></div>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-6 ySENavigationNodeEditor__nodelist\">\n" +
    "            <navigation-node-editor-entry-list data-navigation-node-entry-data=\"ctrl.navigationNodeEntryData\"\n" +
    "                data-node-uid=\"ctrl.nodeUid\"\n" +
    "                data-entry-index=\"ctrl.entryIndex\"\n" +
    "                data-uri-context=\"ctrl.uriContext\" />\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-6 ySENavigationNodeEditor__nodedata\">\n" +
    "            <navigation-node-editor-create-entry data-navigation-node-entry-data=\"ctrl.navigationNodeEntryData\"\n" +
    "                data-uri-context=\"ctrl.uriContext\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsMenuToolbarItemTemplate.html',
    "<div class=\"ySEVisibilityMenu\"\n" +
    "    data-ng-if=\"$ctrl.pageIsPrimary !== undefined\">\n" +
    "    <div class=\"ySEVisibilityMenu__header\">\n" +
    "        <restrictions-page-info data-page-id=\"$ctrl.pageId\"\n" +
    "            data-page-name=\"$ctrl.pageName\"\n" +
    "            data-page-type=\"$ctrl.pageType\"\n" +
    "            data-page-is-primary=\"$ctrl.pageIsPrimary\"\n" +
    "            data-associated-primary-page-name=\"$ctrl.associatedPrimaryPageName\">\n" +
    "        </restrictions-page-info>\n" +
    "    </div>\n" +
    "    <div class=\"ySEVisibilityMenu__restrictions\"\n" +
    "        data-ng-if=\"!$ctrl.pageIsPrimary\">\n" +
    "        <restrictions-table data-editable=\"false\"\n" +
    "            data-restrictions=\"$ctrl.restrictions\"\n" +
    "            data-restriction-criteria=\"$ctrl.restrictionCriteria\"\n" +
    "            data-custom-class=\"'ySERestrictionMenu'\">\n" +
    "        </restrictions-table>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsMenuToolbarItemWrapperTemplate.html',
    "<restrictions-menu-toolbar-item></restrictions-menu-toolbar-item>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsPageInfoTemplate.html',
    "<div class=\"ySERestrictionsPageInfoContainer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.pageNameLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--page-name\"\n" +
    "                data-ng-bind=\"ctrl.pageName\"></h3>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.pageDisplayConditionsLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--displayconditions-value\">{{ ctrl.displayConditionsI18nKey[ctrl.getDisplayConditionsValue()].value | translate }}</h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"ctrl.pageIsPrimary\">\n" +
    "        <p class=\"ySERestrictonMenuLabel ySERestrictionsPageInfoContainer--displayconditions-description\">{{ ctrl.displayConditionsI18nKey[ctrl.getDisplayConditionsValue()].description | translate }}</p>\n" +
    "    </div>\n" +
    "    <div class=\"row\"\n" +
    "        ng-if=\"!ctrl.pageIsPrimary\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.associatedPrimaryPageLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--associatedprimarypage-name\"\n" +
    "                data-ng-bind=\"ctrl.associatedPrimaryPageName\"></h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/restrictionsPageListIcon/restrictionsPageListIconTemplate.html',
    "<span>\n" +
    "    <img class=\"restrictionPageListIcon\"\n" +
    "        data-ng-src=\"{{ctrl.getIconUrl()}}\"\n" +
    "        data-uib-tooltip-placement=\"bottom\"\n" +
    "        data-uib-tooltip=\"{{ ctrl.tooltipI18nKey | translate: ctrl.getInterpolationParameters() }}\" />\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/newPageDisplayCondition/newPageDisplayConditionTemplate.html',
    "<div data-ng-show=\"$ctrl.ready === true\"\n" +
    "    class=\"ySENewPageDisplayCondition\">\n" +
    "\n" +
    "    <div class=\"page-condition-selector-wrapper form-group\">\n" +
    "\n" +
    "        <label for=\"page-condition-selector-id\"\n" +
    "            class=\"control-label\">{{ 'se.cms.page.condition.selection.label' | translate }}</label>\n" +
    "        <ui-select id=\"page-condition-selector-id\"\n" +
    "            data-ng-model=\"$ctrl.conditionSelected\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"false\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.dataChanged()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.conditionSelected.label | translate }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"c in $ctrl.conditions\">\n" +
    "                {{ c.label | translate }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ $ctrl.conditionSelected.description | translate }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.showPrimarySelector()\"\n" +
    "        class=\"page-condition-primary-selector-wrapper form-group\">\n" +
    "        <label for=\"page-condition-primary-selector-id\"\n" +
    "            class=\"control-label\">{{ 'se.cms.page.condition.primary.association.label' | translate }}</label>\n" +
    "        <ui-select id=\"page-condition-primary-selector-id\"\n" +
    "            data-ng-model=\"$ctrl.primarySelected\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"true\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.dataChanged()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.primarySelected.name }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"page in $ctrl.primaryPageChoices | filter:$select.search\">\n" +
    "                {{ page.name }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span data-ng-show=\"$ctrl.primarySelected.label\"\n" +
    "            class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ 'se.cms.page.label.label' | translate }}: {{ $ctrl.primarySelected.label }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectPageTemplate/selectPageTemplateTemplate.html',
    "<div class=\"page-type-step-template\">\n" +
    "    <div class=\"se-input-group page-type-step-template-list-search\">\n" +
    "        <span class=\"se-input-group--addon\">\n" +
    "            <span class=\"hyicon hyicon-search ySEPage-list-search-icon\"></span>\n" +
    "        </span>\n" +
    "        <input type=\"text\"\n" +
    "            class=\"se-input-group--input ySEPage-list-search-input\"\n" +
    "            placeholder=\"{{ 'se.cms.pagewizard.templatestep.searchplaceholder' | translate }}\"\n" +
    "            data-ng-model=\"$ctrl.searchString\"\n" +
    "            name=\"query\" />\n" +
    "        <span data-ng-if=\"$ctrl.searchString\"\n" +
    "            class=\"se-input-group--addon ySESearchIcon\"\n" +
    "            data-ng-click=\"$ctrl.clearSearch()\">\n" +
    "            <span class=\"glyphicon glyphicon-remove-sign\"></span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"page-wizard-list\">\n" +
    "        <div data-ng-repeat=\"template in $ctrl.pageTemplates | templateNameFilter:$ctrl.searchString track by $id(template)\"\n" +
    "            data-ng-class=\"{ 'page-type-step-template__item__selected': $ctrl.isSelected(template)}\"\n" +
    "            class=\"page-type-step-template__item\"\n" +
    "            data-ng-click=\"$ctrl.templateSelected(template)\">\n" +
    "            <span class=\"hyicon hyicon-checked page-type-step-template__item__icon\"></span>\n" +
    "            <div class=\"page-type-step-template__item--info\">\n" +
    "                <div class=\"page-type-step-template__item--info__title\">{{ template.name }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectPageType/selectPageTypeTemplate.html',
    "<div class=\"page-type-step-template\">\n" +
    "    <div class=\"page-type-step-template__text ySEText\">{{ 'se.cms.addpagewizard.pagetype.description' | translate}}</div>\n" +
    "    <div data-ng-repeat=\"pageType in $ctrl.pageTypes\"\n" +
    "        data-ng-class=\"{ 'page-type-step-template__item__selected': $ctrl.isSelected(pageType)}\"\n" +
    "        class=\"page-type-step-template__item\"\n" +
    "        data-ng-click=\"$ctrl.selectType(pageType)\">\n" +
    "        <span class=\"hyicon hyicon-checked page-type-step-template__item__icon\"></span>\n" +
    "        <div class=\"page-type-step-template__item--info\">\n" +
    "            <div class=\"page-type-step-template__item--info__title\">{{ pageType.name | l10n }}</div>\n" +
    "            <div class=\"page-type-step-template__item--info__description\">{{ pageType.description | l10n }}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectTargetCatalogVersion/selectTargetCatalogVersionTemplate.html',
    "<div class=\"target-catalog-version-selector-wrapper form-group\">\n" +
    "    <label for=\"target-catalog-version-selector-id\"\n" +
    "        class=\"control-label\"\n" +
    "        data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.label\"></label>\n" +
    "\n" +
    "    <y-select data-ng-if=\"$ctrl.catalogVersions.length\"\n" +
    "        data-id=\"se-catalog-version-selector-dropdown\"\n" +
    "        data-ng-model=\"$ctrl.selectedCatalogVersion\"\n" +
    "        data-fetch-strategy=\"$ctrl.catalogVersionSelectorFetchStrategy\"\n" +
    "        data-on-change=\"$ctrl.onSelectionChanged\"\n" +
    "        data-search-enabled=\"false\">\n" +
    "    </y-select>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameLabel\">\n" +
    "        <span class=\"warning-input help-block\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.label.exists.message\"></span>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameTypeCode\">\n" +
    "        <span class=\"warning-input help-block\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.pagetype.exists.message\"></span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageDisplayConditionStepTemplate.html',
    "<new-page-display-condition data-page-type-code=\"addPageWizardCtl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"addPageWizardCtl.uriContext\"\n" +
    "    data-result-fn=\"addPageWizardCtl.variationResult\">\n" +
    "</new-page-display-condition>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageInfoStepTemplate.html',
    "<span data-ng-if=\"addPageWizardCtl.isPageInfoActive()\">\n" +
    "    <generic-editor data-structure=\"addPageWizardCtl.getPageInfoStructure()\"\n" +
    "        data-content=\"addPageWizardCtl.getPageInfo()\"\n" +
    "        data-submit=\"addPageWizardCtl.callbacks.savePageInfo\"\n" +
    "        data-reset=\"addPageWizardCtl.callbacks.resetPageInfo\"\n" +
    "        data-is-dirty=\"addPageWizardCtl.callbacks.isDirtyPageInfo\"\n" +
    "        data-is-valid=\"addPageWizardCtl.callbacks.isValidPageInfo\">\n" +
    "    </generic-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageRestrictionsStepTemplate.html',
    "<span data-ng-if=\"addPageWizardCtl.isRestrictionsActive()\">\n" +
    "\n" +
    "    <div data-ng-if=\"addPageWizardCtl.getPageInfo().restrictions.length > 1\">\n" +
    "        <page-restrictions-info-message />\n" +
    "    </div>\n" +
    "\n" +
    "    <restrictions-editor data-editable=\"true\"\n" +
    "        data-save-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.save\"\n" +
    "        data-reset-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.reset\"\n" +
    "        data-cancel-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.cancel\"\n" +
    "        data-is-dirty-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.isDirty\"\n" +
    "        data-on-restrictions-changed=\"addPageWizardCtl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "        data-get-restriction-types=\"addPageWizardCtl.getRestrictionTypes()\"\n" +
    "        data-get-supported-restriction-types=\"addPageWizardCtl.getSupportedRestrictionTypes()\"\n" +
    "        data-item=\"addPageWizardCtl.getPageInfo()\">\n" +
    "    </restrictions-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageTemplateStepTemplate.html',
    "<select-page-template data-uri-context=\"addPageWizardCtl.uriContext\"\n" +
    "    data-page-type-code=\"addPageWizardCtl.getPageTypeCode()\"\n" +
    "    data-on-template-selected=\"addPageWizardCtl.templateSelected\">\n" +
    "</select-page-template>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageTypeStepTemplate.html',
    "<select-page-type data-on-type-selected=\"addPageWizardCtl.typeSelected\">\n" +
    "</select-page-type>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/components/clonePageInfo/componentCloneInfoTemplate.html',
    "<y-message data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameTypeCode\"\n" +
    "    data-type=\"warning\">\n" +
    "    <message-description translate=\"se.cms.clonepagewizard.pageinfo.targetcatalogversion.pagetype.exists.message\"\n" +
    "        translate-value-type-code=\"{{$ctrl.pageTypeCode}}\">\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "\n" +
    "<div class=\"edit-page-basic-tab--header\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"se.cms.pageinfo.page.type\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.pageTypeCode}}</div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"se.cms.pageinfo.page.template\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.pageTemplate}}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<generic-editor data-structure=\"$ctrl.structure\"\n" +
    "    data-content=\"$ctrl.content\"\n" +
    "    data-submit=\"$ctrl.submit\"\n" +
    "    data-reset=\"$ctrl.reset\"\n" +
    "    data-is-dirty=\"$ctrl.isDirty\"\n" +
    "    data-is-valid=\"$ctrl.isValid\"\n" +
    "    data-get-api=\"$ctrl.setGenericEditorApi($api)\">\n" +
    "</generic-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/components/clonePageOptions/componentCloneOptionTemplate.html',
    "<label for=\"components-cloned-selector-id\"\n" +
    "    class=\"control-label\"\n" +
    "    data-translate=\"se.cms.clonepagewizard.options.title\" />\n" +
    "<y-help data-title=\"$ctrl.helpTitle\"\n" +
    "    data-template=\"$ctrl.helpTemplate\"></y-help>\n" +
    "<div id=\"components-cloned-selector-id\">\n" +
    "    <div class=\"radio\">\n" +
    "        <input class=\"components-cloned-option-id\"\n" +
    "            type=\"radio\"\n" +
    "            name=\"componentsclone\"\n" +
    "            id=\"reference-cloning\"\n" +
    "            ng-model=\"$ctrl.componentInSlotOption\"\n" +
    "            ng-click=\"$ctrl.updateComponentInSlotOption($ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.REFERENCE_EXISTING)\"\n" +
    "            ng-value=\"$ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.REFERENCE_EXISTING\">\n" +
    "        <label for=\"reference-cloning\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.existing\" />\n" +
    "    </div>\n" +
    "    <div class=\"radio\">\n" +
    "        <input class=\"components-cloned-option-id\"\n" +
    "            type=\"radio\"\n" +
    "            id=\"deep-cloning\"\n" +
    "            name=\"componentsclone\"\n" +
    "            ng-model=\"$ctrl.componentInSlotOption\"\n" +
    "            ng-click=\"$ctrl.updateComponentInSlotOption($ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.CLONE)\"\n" +
    "            ng-value=\"$ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.CLONE\">\n" +
    "        <label for=\"deep-cloning\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.copies\" />\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageInfoStepTemplate.html',
    "<component-clone-info-form data-ng-if=\"clonePageWizardCtrl.isPageInfoActive()\"\n" +
    "    data-structure=\"clonePageWizardCtrl.getPageInfoStructure()\"\n" +
    "    data-content=\"clonePageWizardCtrl.getPageInfo()\"\n" +
    "    data-submit=\"clonePageWizardCtrl.callbacks.savePageInfo\"\n" +
    "    data-reset=\"clonePageWizardCtrl.callbacks.resetPageInfo\"\n" +
    "    data-is-dirty=\"clonePageWizardCtrl.callbacks.isDirtyPageInfo\"\n" +
    "    data-is-valid=\"clonePageWizardCtrl.callbacks.isValidPageInfo\"\n" +
    "    data-page-template=\"clonePageWizardCtrl.getPageTemplate()\"\n" +
    "    data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-target-catalog-version=\"clonePageWizardCtrl.getTargetCatalogVersion()\">\n" +
    "</component-clone-info-form>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageOptionsStepTemplate.html',
    "<select-target-catalog-version data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-page-label=\"clonePageWizardCtrl.getPageLabel()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-base-page-info=\"clonePageWizardCtrl.getBasePageInfo()\"\n" +
    "    data-on-target-catalog-version-selected=\"clonePageWizardCtrl.onTargetCatalogVersionSelected($catalogVersion)\">\n" +
    "</select-target-catalog-version>\n" +
    "\n" +
    "<new-page-display-condition data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-result-fn=\"clonePageWizardCtrl.variationResult\"\n" +
    "    data-initial-condition-selected=\"'page.displaycondition.variation'\"\n" +
    "    data-initial-primary-page-selected=\"clonePageWizardCtrl.getPageLabel()\"\n" +
    "    data-target-catalog-version=\"clonePageWizardCtrl.getTargetCatalogVersion()\">\n" +
    "</new-page-display-condition>\n" +
    "\n" +
    "<component-clone-option-form data-on-selection-change=\"clonePageWizardCtrl.triggerUpdateCloneOptionResult($cloneOptionData)\"></component-clone-option-form>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageRestrictionsStepTemplate.html',
    "<span data-ng-if=\"clonePageWizardCtrl.isRestrictionsActive()\">\n" +
    "    <div data-ng-if=\"clonePageWizardCtrl.getPageRestrictions().length > 1\">\n" +
    "        <page-restrictions-info-message />\n" +
    "    </div>\n" +
    "\n" +
    "    <restrictions-editor data-editable=\"true\"\n" +
    "        data-save-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.save\"\n" +
    "        data-reset-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.reset\"\n" +
    "        data-cancel-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.cancel\"\n" +
    "        data-is-dirty-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.isDirty\"\n" +
    "        data-on-restrictions-changed=\"clonePageWizardCtrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "        data-get-restriction-types=\"clonePageWizardCtrl.getRestrictionTypes()\"\n" +
    "        data-get-supported-restriction-types=\"clonePageWizardCtrl.getSupportedRestrictionTypes()\"\n" +
    "        data-item=\"clonePageWizardCtrl.getBasePageInfo()\"\n" +
    "        data-restrictions=\"clonePageWizardCtrl.getBasePageInfo().restrictions\">\n" +
    "    </restrictions-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsEditor/displayConditionsEditorTemplate.html',
    "<div>\n" +
    "    <display-conditions-page-info data-page-name=\"$ctrl.getPageName()\"\n" +
    "        data-page-type=\"$ctrl.getPageType()\"\n" +
    "        data-is-primary=\"$ctrl.isPagePrimary()\"></display-conditions-page-info>\n" +
    "    <div data-ng-if=\"$ctrl.isPagePrimary()\">\n" +
    "        <display-conditions-page-variations data-variations=\"$ctrl.getVariations()\"></display-conditions-page-variations>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.isPagePrimary()\">\n" +
    "        <display-conditions-primary-page data-read-only=\"$ctrl.getIsAssociatedPrimaryReadOnly()\"\n" +
    "            data-associated-primary-page=\"$ctrl.getAssociatedPrimaryPage()\"\n" +
    "            data-all-primary-pages=\"$ctrl.getPrimaryPages()\"\n" +
    "            data-on-primary-page-select=\"$ctrl.onPrimaryPageSelect(primaryPage)\"></display-conditions-primary-page>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPageInfo/displayConditionsPageInfoTemplate.html',
    "<div class=\"ySEDisplayConditionsPageInfo\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"{{ $ctrl.pageNameI18nKey }}\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight dc-page-name\"\n" +
    "                data-ng-bind=\"$ctrl.pageName\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"{{ $ctrl.pageTypeI18nKey }}\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight dc-page-type\"\n" +
    "                data-ng-bind=\"$ctrl.pageType\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <label class=\"control-label\">\n" +
    "            <span data-translate=\"{{ $ctrl.displayConditionLabelI18nKey }}\"></span>\n" +
    "            <y-help data-title=\"pageSync.helpTitle\"\n" +
    "                data-template=\"$ctrl.helpTemplate\"></y-help>\n" +
    "        </label>\n" +
    "        <p class=\"form-readonly-text form-readonly-text__tight dc-page-display-condition\">{{ $ctrl.getPageDisplayConditionI18nKey() | translate }}</p>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPageVariations/displayConditionsPageVariationsTemplate.html',
    "<div class=\"form-group dc-page-variations\">\n" +
    "    <label class=\"control-label control-label__margin\">{{ $ctrl.variationPagesTitleI18nKey | translate }}</label>\n" +
    "    <span data-ng-if=\"$ctrl.variations.length > 0\"\n" +
    "        class=\"help-control\"\n" +
    "        data-toggle=\"tooltip\"\n" +
    "        data-placement=\"top\"\n" +
    "        title=\"{{ $ctrl.variationsDescriptionI18nKey | translate }}\">\n" +
    "        <span class=\"hyicon hyicon-help-icon\"></span>\n" +
    "    </span>\n" +
    "    <div data-ng-if=\"$ctrl.variations.length === 0\"\n" +
    "        class=\"dc-no-variations form-readonly-text form-readonly-text__tight form-readonly-text__inline\">{{ $ctrl.noVariationsI18nKey | translate }}</div>\n" +
    "    <div data-ng-if=\"$ctrl.variations.length > 0\">\n" +
    "        <client-paged-list data-items=\"$ctrl.variations\"\n" +
    "            data-keys=\"$ctrl.keys\"\n" +
    "            data-renderers=\"$ctrl.renderers\"\n" +
    "            data-items-per-page=\"$ctrl.itemsPerPage\"\n" +
    "            class=\"dc-page-variations-list\">\n" +
    "        </client-paged-list>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPrimaryPage/displayConditionsPrimaryPageTemplate.html',
    "<div class=\"form-group form-group__inline\">\n" +
    "    <label class=\"control-label control-label__margin\">{{ $ctrl.associatedPrimaryPageLabelI18nKey | translate }}</label>\n" +
    "    <div data-ng-if=\"$ctrl.readOnly\"\n" +
    "        class=\"dc-associated-primary-page form-readonly-text form-readonly-text__tight\">{{ ::$ctrl.associatedPrimaryPage.name }}\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.readOnly\">\n" +
    "        <ui-select id=\"display-conditions-primary-association-selector\"\n" +
    "            data-ng-model=\"$ctrl.associatedPrimaryPage\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"true\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.triggerOnPrimaryPageSelect()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.associatedPrimaryPage.name }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"primaryPage in $ctrl.allPrimaryPages | filter:$select.search\">\n" +
    "                {{ primaryPage.name }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span data-ng-show=\"$ctrl.associatedPrimaryPage.name\"\n" +
    "            class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ 'page.label.label' | translate }}: {{ $ctrl.associatedPrimaryPage.label }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/basic/editPageBasicTabInnerTemplate.html',
    "<div class=\"edit-page-basic-tab\">\n" +
    "\n" +
    "    <div class=\" edit-page-basic-tab--header\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-md-6 form-group\">\n" +
    "                <label class=\"control-label\">{{ \"se.cms.pageinfo.page.type\" | translate }}</label>\n" +
    "                <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.content.typeCode}}</div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6 form-group\">\n" +
    "                <label class=\"control-label\">{{ \"se.cms.pageinfo.page.template\" | translate }}</label>\n" +
    "                <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.content.template}}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.structure\">\n" +
    "        <generic-editor data-id=\"$ctrl.tabId\"\n" +
    "            data-smartedit-component-id=\"$ctrl.model.uuid\"\n" +
    "            data-smartedit-component-type=\"$ctrl.model.typeCode\"\n" +
    "            data-structure=\"$ctrl.structure\"\n" +
    "            data-content=\"$ctrl.content\"\n" +
    "            data-submit=\"$ctrl.submitCallback\"\n" +
    "            data-reset=\"$ctrl.resetTab\"\n" +
    "            data-is-dirty=\"$ctrl.isDirtyTab\"\n" +
    "            data-get-api=\"$ctrl.setGenericEditorApi($api)\">\n" +
    "        </generic-editor>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/basic/editPageBasicTabTemplate.html',
    "<edit-page-basic-tab save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab='onCancel'\n" +
    "    is-dirty-tab='isDirty'\n" +
    "    data-tab-id='tabId'\n" +
    "    model='model'>\n" +
    "</edit-page-basic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/displayConditions/displayConditionsTabInnerTemplate.html',
    "<display-conditions-editor data-page-uid=\"$ctrl.pageUid\"></display-conditions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/displayConditions/displayConditionsTabTemplate.html',
    "<display-conditions-tab data-save='onSave'\n" +
    "    data-reset='onReset'\n" +
    "    data-cancel='onCancel'\n" +
    "    data-is-dirty='isDirty'\n" +
    "    data-data-tab-id='tabId'\n" +
    "    data-model='model'>\n" +
    "</display-conditions-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/visibility/editPageVisibilityTabInnerTemplate.html',
    "<y-message data-type=\"danger\"\n" +
    "    class=\"se-restrictions-editor--y-message--modal-adjusted\"\n" +
    "    ng-repeat=\"error in ctrl.errors\">\n" +
    "    <message-description>\n" +
    "        <translate>{{error.message}}</translate>\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "<div data-ng-if=\"ctrl.restrictions.length > 1\">\n" +
    "    <page-restrictions-info-message />\n" +
    "</div>\n" +
    "\n" +
    "<restrictions-editor data-ng-if=\"ctrl.isReady\"\n" +
    "    data-editable=\"true\"\n" +
    "    data-reset-fn=\"ctrl.resetTab\"\n" +
    "    data-cancel-fn=\"ctrl.cancelTab\"\n" +
    "    data-is-dirty-fn=\"ctrl.isDirtyTab\"\n" +
    "    data-on-restrictions-changed=\"ctrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "    data-item=\"ctrl.page\"\n" +
    "    data-restrictions=\"ctrl.page.restrictions\"\n" +
    "    data-get-restriction-types=\"ctrl.getRestrictionTypes()\"\n" +
    "    data-get-supported-restriction-types=\"ctrl.getSupportedRestrictionTypes()\">\n" +
    "</restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/visibility/editPageVisibilityTabTemplate.html',
    "<edit-page-visibility-tab save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    model=\"model\">\n" +
    "</edit-page-visibility-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoContainerTemplate.html',
    "<div class=\"ySEPageInfoMenu__header\">\n" +
    "    <page-info-header data-page-type-code=\"$ctrl.pageTypeCode\"\n" +
    "        data-page-template=\"$ctrl.pageTemplate\"></page-info-header>\n" +
    "</div>\n" +
    "<div class=\"ySEPageInfoMenu__content\">\n" +
    "    <page-info-details data-page-uid=\"$ctrl.pageUid\"\n" +
    "        data-page-type-code=\"$ctrl.pageTypeCode\"\n" +
    "        data-page-structure=\"$ctrl.pageStructure\"\n" +
    "        data-page-content=\"$ctrl.pageContent\"\n" +
    "        data-on-edit-click=\"$ctrl.onEditClick()\"></page-info-details>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoDetailsTemplate.html',
    "<div class=\"ySEPageInfoStaticInfo\">\n" +
    "    <div class=\"ySEPageInfoStaticInfoContainer\">\n" +
    "        <div class=\"row ySEPageIngoRow\">\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <p class=\"ySEHeaderText\">{{'se.cms.pageinfo.information.title' | translate}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <button class=\"btn btn-link pull-right ySEPageInfo__edit-btn\"\n" +
    "                    has-operation-permission=\"'se.edit.page.link'\"\n" +
    "                    data-ng-click=\"$ctrl.onEditClick()\">\n" +
    "                    {{'se.cms.contextmenu.title.edit' | translate}}\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"$ctrl.pageStructure\"\n" +
    "        class=\"ySEPageInfoEditInfoContainer\">\n" +
    "        <generic-editor data-smartedit-component-id=\"$ctrl.pageUid\"\n" +
    "            data-smartedit-component-type=\"$ctrl.pageTypeCode\"\n" +
    "            data-structure=\"$ctrl.pageStructure\"\n" +
    "            data-content=\"$ctrl.pageContent\">\n" +
    "        </generic-editor>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoHeaderTemplate.html',
    "<div class=\"ySEPageInfoHeaderContainer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p>{{\"se.cms.pageinfo.page.type\" | translate}}</p>\n" +
    "            <h3 class=\"page-type-code\">{{$ctrl.pageTypeCode}}</h3>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p>{{\"se.cms.pageinfo.page.template\" | translate}}</p>\n" +
    "            <h3 class=\"page-template\">{{$ctrl.pageTemplate}}</h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfoMenuToolbarItemTemplate.html',
    "<div class=\"ySEPageInfoMenu\">\n" +
    "    <page-info-container></page-info-container>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfoMenuToolbarItemWrapperTemplate.html',
    "<page-info-menu-toolbar-item></page-info-menu-toolbar-item>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListLinkDirectiveTemplate.html',
    "<div class=\"page-list-link-container\">\n" +
    "    <a class=\"page-list-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-href=\"#!/pages/{{$ctrl.siteId}}/{{$ctrl.catalog.catalogId}}/{{$ctrl.catalogVersion.version}}\"\n" +
    "        data-translate=\"se.cms.cataloginfo.pagelist\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListLinkTemplate.html',
    "<page-list-link data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-site-id=\"$ctrl.siteId\"></page-list-link>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListTemplate.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"pageListWrapper\">\n" +
    "    <div class=\"ySEPageListTitle\">\n" +
    "        <h1 class=\"ySEPage-list-title\"\n" +
    "            data-translate='se.cms.pagelist.title'></h1>\n" +
    "        <h4 class=\"ySEPage-list-label\">{{pageListCtl.catalogName | l10n}} - {{pageListCtl.catalogVersion}}</h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-input-group ySEPage-list-search\">\n" +
    "        <div class=\"se-input-group--addon\">\n" +
    "            <span class=\"hyicon hyicon-search ySEPage-list-search-icon\"></span>\n" +
    "        </div>\n" +
    "        <input type=\"text\"\n" +
    "            class=\"se-input-group--input ySEPage-list-search-input\"\n" +
    "            placeholder=\"{{ 'se.cms.pagelist.searchplaceholder' | translate }}\"\n" +
    "            data-ng-model=\"pageListCtl.query.value\"\n" +
    "            name=\"query\">\n" +
    "        <div class=\"se-input-group--addon ySESearchIcon\"\n" +
    "            data-ng-show=\"pageListCtl.query.value\"\n" +
    "            data-ng-click=\"pageListCtl.reset()\">\n" +
    "            <span class=\"glyphicon glyphicon-remove-sign\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"ySEAdd-Page-button\"\n" +
    "        has-operation-permission=\"'se.edit.page'\">\n" +
    "        <button class=\"y-add-btn\"\n" +
    "            data-ng-click=\"pageListCtl.openAddPageWizard()\">\n" +
    "            <span class=\"hyicon hyicon-add\"></span>\n" +
    "            <span class=\"\">{{'se.cms.addpagewizard.addpage' | translate}}</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "    <client-paged-list data-ng-if=\"pageListCtl.itemsReady\"\n" +
    "        data-items=\"pageListCtl.pages\"\n" +
    "        data-keys=\"pageListCtl.keys\"\n" +
    "        data-renderers=\"pageListCtl.renderers\"\n" +
    "        data-injected-context=\"pageListCtl.injectedContext\"\n" +
    "        data-sort-by=\"'name'\"\n" +
    "        data-reversed=\"false\"\n" +
    "        data-items-per-page=\"10\"\n" +
    "        data-query=\"pageListCtl.query.value\"\n" +
    "        data-display-count=\"true\"\n" +
    "        data-dropdown-items=pageListCtl.dropdownItems\n" +
    "        data-item-filter-keys=\"pageListCtl.searchKeys\">\n" +
    "    </client-paged-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageRestrictionsInfoMessage/pageRestrictionsInfoMessageTemplate.html',
    "<y-message data-message-id=\"yMsgInfoId\"\n" +
    "    data-type=\"info\"\n" +
    "    class=\"se-restrictions-editor--y-message--modal-adjusted\">\n" +
    "    <message-title>{{ 'se.cms.restrictions.editor.ymessage.title' | translate }}</message-title>\n" +
    "    <message-description>{{ 'se.cms.restrictions.editor.ymessage.description' | translate }}</message-description>\n" +
    "</y-message>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementEditTemplate.html',
    "<div data-ng-if=\"$ctrl.ready\"\n" +
    "    class=\"se-restriction-management-edit\">\n" +
    "    <div data-ng-if=\"$ctrl.isTypeSupported\"\n" +
    "        class=\"se-restriction-management-supported-edit\">\n" +
    "        <div class=\"se-restriction-management-edit__header\">\n" +
    "            <div class=\"se-restriction-management-edit__name\">\n" +
    "                {{ $ctrl.restriction.name }}\n" +
    "            </div>\n" +
    "            <div class=\"se-restriction-management-edit__code\">\n" +
    "                {{ $ctrl.restriction.typeCode }}\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"se-restriction-management-edit__data\">\n" +
    "            <item-manager data-item=\"$ctrl.restriction\"\n" +
    "                data-mode=\"$ctrl.itemManagementMode\"\n" +
    "                data-structure-api=\"$ctrl.structureApi\"\n" +
    "                data-content-api=\"$ctrl.contentApi\"\n" +
    "                data-uri-context=\"$ctrl.uriContext\"\n" +
    "                data-component-type=\"$ctrl.restriction.itemtype\"\n" +
    "                data-submit-function=\"$ctrl.submitInternal\"\n" +
    "                data-is-dirty=\"$ctrl.isDirtyFn\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.isTypeSupported\">\n" +
    "        <div class=\"se-restrictions-list--content\">\n" +
    "            <div>\n" +
    "                {{ 'se.cms.restriction.management.select.type.not.supported.warning' | translate }}\n" +
    "            </div>\n" +
    "            <p class=\"ySERestrictionsNameHeader\">{{ $ctrl.restriction.name }}</p>\n" +
    "            <div class=\"ySERestrictionsDescription\">\n" +
    "                {{ $ctrl.restriction.description }}\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementItemNameTemplate.html',
    "<span class=\"se-restriction-management-item-name\"\n" +
    "    data-ng-bind-html=\"item.name | l10n\">\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementSelectTemplate.html',
    "<div class=\"se-restriction-management-select\">\n" +
    "    <y-message data-ng-show=\"$ctrl.selectModel.getRestriction() && !$ctrl.selectModel.isTypeSupported()\"\n" +
    "        data-message-id=\"yMsgWarningId\"\n" +
    "        data-type=\"warning\"\n" +
    "        class=\"se-restriction-management-select--y-message--modal-adjusted\">\n" +
    "        <message-title>{{ 'se.cms.restriction.management.select.type.not.supported.warning' | translate }}</message-title>\n" +
    "    </y-message>\n" +
    "\n" +
    "    <div class=\"se-restriction-management-select__y-select\">\n" +
    "        <label for=\"\"\n" +
    "            class=\"control-label\">{{ 'se.cms.restriction.management.select.type.label' | translate }}</label>\n" +
    "\n" +
    "        <y-select data-id=\"restriction-type\"\n" +
    "            data-fetch-strategy=\"{ fetchAll: $ctrl.selectModel.getRestrictionTypes }\"\n" +
    "            data-item-template=\"::$ctrl.itemTemplateUrl\"\n" +
    "            data-placeholder=\"'se.cms.restriction.management.select.type.placeholder'\"\n" +
    "            data-ng-model=\"$ctrl.selectModel.selectedIds.restrictionType\"\n" +
    "            data-on-change=\"$ctrl.selectRestrictionType\"\n" +
    "            data-search-enabled=\"false\">\n" +
    "        </y-select>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-recompile-dom=\"$ctrl.resetSelector\"\n" +
    "        class=\"se-restriction-management-select__restriction\">\n" +
    "        <div data-ng-if=\"$ctrl.controllerModel.showRestrictionSelector\"\n" +
    "            class=\"se-restriction-management-select__restriction__info\">\n" +
    "            <label class=\"control-label se-restriction-management-select__restriction__info__label\">\n" +
    "                {{ 'se.cms.restriction.management.select.restriction.label' | translate }}\n" +
    "            </label>\n" +
    "            <y-select data-id=\"restriction-name\"\n" +
    "                data-fetch-strategy=\"$ctrl.fetchOptions\"\n" +
    "                data-ng-model=\"$ctrl.selectModel.selectedIds.restriction\"\n" +
    "                data-placeholder=\"'se.cms.restriction.management.select.restriction.placeholder'\"\n" +
    "                data-on-change=\"$ctrl.selectRestriction\"\n" +
    "                data-disable-choice-fn=\"$ctrl.disableRestrictionChoice\"\n" +
    "                data-item-template=\"::$ctrl.itemTemplateUrl\"\n" +
    "                data-results-header-template=\"$ctrl.getResultsHeaderTemplate()\"\n" +
    "                data-results-header-label=\"::$ctrl.resultsHeaderLabel\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "        <div data-recompile-dom=\"$ctrl.resetEditor\"\n" +
    "            class=\"se-restriction-management-select__restriction__editor\"\n" +
    "            data-ng-if=\"$ctrl.controllerModel.showRestrictionEditor\">\n" +
    "            <p class=\"se-restriction-management-select__restriction__editor__instr\">\n" +
    "                {{ $ctrl.editorHeader | translate }}\n" +
    "            </p>\n" +
    "            <div data-ng-if=\"$ctrl.selectModel.isTypeSupported()\">\n" +
    "                <item-manager data-item=\"$ctrl.selectModel.getRestriction()\"\n" +
    "                    data-mode=\"$ctrl.controllerModel.mode\"\n" +
    "                    data-structure-api=\"$ctrl.controllerModel.structureApi\"\n" +
    "                    data-content-api=\"$ctrl.controllerModel.contentApi\"\n" +
    "                    data-uri-context=\"$ctrl.uriContext\"\n" +
    "                    data-component-type=\"$ctrl.selectModel.getRestrictionTypeCode()\"\n" +
    "                    data-submit-function=\"$ctrl.submitInternal\"\n" +
    "                    data-is-dirty=\"$ctrl.isDirtyInternal\" />\n" +
    "            </div>\n" +
    "            <div class=\"ySERestrictionsList-item\"\n" +
    "                data-ng-if=\"!$ctrl.selectModel.isTypeSupported()\">\n" +
    "                <p class=\"ySERestrictionsNameHeader\">{{ $ctrl.selectModel.getRestriction().name }}</p>\n" +
    "                <div class=\"ySERestrictionsDescription\">\n" +
    "                    {{ $ctrl.selectModel.getRestriction().description }}\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/restrictionManagementTemplate.html',
    "<div data-ng-if=\"$ctrl.editMode\"\n" +
    "    class=\"se-restriction-management se-restriction-management-edit\">\n" +
    "    <restriction-management-edit data-restriction=\"$ctrl.restriction\"\n" +
    "        data-get-supported-restriction-types-fn=\"$ctrl.getSupportedRestrictionTypesFn\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-is-dirty-fn=\"$ctrl.isDirtyFn\"\n" +
    "        data-submit-fn=\"$ctrl.submitInternal\">\n" +
    "    </restriction-management-edit>\n" +
    "</div>\n" +
    "<div data-ng-if=\"!$ctrl.editMode\"\n" +
    "    class=\"se-restriction-management se-restriction-management-select\">\n" +
    "    <restriction-management-select data-existing-restrictions=\"$ctrl.existingRestrictions\"\n" +
    "        data-get-restriction-types-fn=\"$ctrl.getRestrictionTypesFn()\"\n" +
    "        data-get-supported-restriction-types-fn=\"$ctrl.getSupportedRestrictionTypesFn\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-is-dirty-fn=\"$ctrl.isDirtyFn\"\n" +
    "        data-submit-fn=\"$ctrl.submitInternal\">\n" +
    "    </restriction-management-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionsEditor/restrictionsEditorTemplate.html',
    "<div class=\"ySEVisbilityModalContainer\">\n" +
    "    <button data-ng-show=\"!$ctrl.showRestrictionPicker\"\n" +
    "        class=\"y-add-btn\"\n" +
    "        data-ng-click=\"$ctrl.onClickOnAdd()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>{{ 'se.cms.restrictions.editor.button.add.new ' | translate }}\n" +
    "    </button>\n" +
    "\n" +
    "    <label data-ng-if=\"$ctrl.restrictions.length > 0\"\n" +
    "        class=\"ySEVisbilityModalContainer-leftPane-title\">{{ 'se.cms.restrictions.list.title' | translate }}</label>\n" +
    "\n" +
    "    <restrictions-table data-ng-if=\"$ctrl.isRestrictionsReady\"\n" +
    "        data-editable=\"true\"\n" +
    "        data-restrictions=\"$ctrl.restrictions\"\n" +
    "        data-on-click-on-edit='$ctrl.onClickOnEdit'\n" +
    "        data-on-criteria-selected=\"$ctrl.matchCriteriaChanged\"\n" +
    "        data-restriction-criteria=\"$ctrl.criteria\"\n" +
    "        data-errors=\"$ctrl.errors\"\n" +
    "        data-custom-class=\"'ySERestrictionListLink'\">\n" +
    "    </restrictions-table>\n" +
    "\n" +
    "    <span data-ng-show=\"$ctrl.showRestrictionPicker\"\n" +
    "        class=\"ySEVisbilityModalContainer-rightPane__restriction\">\n" +
    "        <y-slider-panel data-slider-panel-configuration=\"$ctrl.sliderPanelConfiguration\"\n" +
    "            data-slider-panel-hide=\"$ctrl.sliderPanelHide\"\n" +
    "            class=\"se-add-restriction-panel\"\n" +
    "            data-slider-panel-show=\"$ctrl.sliderPanelShow\">\n" +
    "            <restriction-management data-config=\"$ctrl.restrictionManagement.operation\"\n" +
    "                data-uri-context=\"$ctrl.restrictionManagement.uriContext\"\n" +
    "                class=\"se-se-add-restriction-panel__restriction-management\"\n" +
    "                data-submit-fn=\"$ctrl.restrictionManagement.submitFn\"\n" +
    "                data-is-dirty-fn=\"$ctrl.restrictionManagement.isDirtyFn\">\n" +
    "            </restriction-management>\n" +
    "        </y-slider-panel>\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionsTable/restrictionsTableTemplate.html',
    "<div class=\"ySERestrictionsContainer ySERestrictionsContainer-left {{$ctrl.customClass}}\">\n" +
    "    <div data-ng-if=\"$ctrl.restrictions.length > 1\">\n" +
    "        <div data-ng-if=\"$ctrl.editable\">\n" +
    "            <ui-select on-select=\"$ctrl.criteriaClicked($item)\"\n" +
    "                data-ng-model=\"$ctrl.restrictionCriteria\"\n" +
    "                class=\"form-control ySERestriction-select\"\n" +
    "                search-enabled=\"false\"\n" +
    "                theme=\"select2\"\n" +
    "                data-dropdown-auto-width=\"false\">\n" +
    "                <ui-select-match placeholder=\"{{ 'se.cms.restrictionspicker.type.placeholder' | translate }}\"\n" +
    "                    class=\"ySERestriction-select__placeholder\">\n" +
    "                    {{$select.selected.editLabel | translate}}\n" +
    "                </ui-select-match>\n" +
    "                <ui-select-choices repeat=\"criteriaOption in $ctrl.criteriaOptions\"\n" +
    "                    class=\"ySERestriction-select__choices\"\n" +
    "                    position=\"down \">\n" +
    "                    {{ criteriaOption.editLabel | translate }}\n" +
    "                </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "        </div>\n" +
    "        <div class=\"ySERestrictionsCriteria\"\n" +
    "            data-ng-if=\"!$ctrl.editable\">\n" +
    "            <span class=\"ySERestrictionsCriteriaLabel\">{{ 'se.cms.restrictions.criteria' | translate }} {{ $ctrl.restrictionCriteria.label | translate }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.showRemoveAllButton()\"\n" +
    "        class=\"ySERestrictionsContainer__clear_all\">\n" +
    "        <button class=\"btn btn-link cms-clean-btn ySERestrictionsContainer__clear_all__btn\"\n" +
    "            data-ng-click=\"$ctrl.removeAllRestrictions()\"\n" +
    "            data-translate=\"se.cms.restrictions.list.clear.all\">\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-restrictions-list\"\n" +
    "        data-ng-if=\"$ctrl.restrictions.length > 0\">\n" +
    "        <div id=\"restriction-{{$index+1}}\"\n" +
    "            data-ng-repeat=\"restriction in $ctrl.restrictions\"\n" +
    "            class=\"se-restrictions-list--item\">\n" +
    "            <div class=\"se-restrictions-list--content\"\n" +
    "                data-ng-click=\"$ctrl.onSelect(restriction)\">\n" +
    "                <p class=\"ySERestrictionsNameHeader ng-class:{'error-input':$ctrl.isInError($index)}\">{{ restriction.name }}</p>\n" +
    "                <div class=\"ySERestrictionsTypeAndID ng-class:{'error-input':$ctrl.isInError($index)}\">{{ restriction.typeCode }}</div>\n" +
    "                <div class=\"ySERestrictionsDescription ng-class:{'error-input':$ctrl.isInError($index)}\"\n" +
    "                    title=\"{{ restriction.description }}\">{{ restriction.description }}</div>\n" +
    "            </div>\n" +
    "            <div class=\"se-restrictions-list--options\"\n" +
    "                data-ng-if=\"$ctrl.editable\">\n" +
    "                <y-drop-down-menu dropdown-items=\"$ctrl.actions\"\n" +
    "                    selected-item=\"restriction\"\n" +
    "                    class=\"y-dropdown ySERestrictionsList-item-btn__more pull-right\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/catalogs/catalogDetailsSyncTemplate.html',
    "<synchronize-catalog data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-active-catalog-version=\"$ctrl.activeCatalogVersion\">\n" +
    "</synchronize-catalog>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/catalogs/synchronizeCatalogTemplate.html',
    "<div class=\"se-synchronize-catalog\">\n" +
    "    <!-- Status -->\n" +
    "    <div class=\"se-synchronize-catalog__item se-synchronize-catalog__sync-info\">\n" +
    "        <div data-ng-if=\"ctrl.isSyncJobFinished()\">\n" +
    "            <label class=\"se-synchronize-catalog__sync-info__sync-label\"\n" +
    "                data-ng-if=\"ctrl.catalogVersion.active\">\n" +
    "                {{ 'se.cms.cataloginfo.lastsyncedfrom' | translate: ctrl.getSyncFromLabels() }}\n" +
    "            </label>\n" +
    "            <label class=\"se-synchronize-catalog__sync-info__sync-label\"\n" +
    "                data-ng-if=\"!ctrl.catalogVersion.active\"\n" +
    "                data-translate=\"se.cms.cataloginfo.lastsynced\"></label>\n" +
    "            <span class=\"catalog-last-synced\">{{ctrl.syncJobStatus.syncEndTime| date:'short'}}</span>\n" +
    "        </div>\n" +
    "        <span data-ng-if=\"ctrl.isSyncJobInProgress()\"\n" +
    "            class=\"se-synchronize-catalog__in-progress\"\n" +
    "            data-translate=\"se.sync.status.synced.inprogress\"></span>\n" +
    "        <span data-ng-if=\"ctrl.isSyncJobFailed()\"\n" +
    "            class=\"label-error se-synchronize-catalog__sync-failed\"\n" +
    "            data-translate=\"se.sync.status.synced.syncfailed\"></span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-synchronize-catalog__item se-synchronize-catalog__sync-btn\"\n" +
    "        data-ng-if=\"!ctrl.catalogVersion.active\"\n" +
    "        data-has-operation-permission=\"ctrl.syncCatalogPermission\">\n" +
    "        <button class=\"btn btn-default\"\n" +
    "            data-ng-disabled=\"!ctrl.isButtonEnabled()\"\n" +
    "            data-ng-click=\"ctrl.syncCatalog()\">{{ 'se.cms.cataloginfo.btn.sync' | translate }}</button>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageListSyncIcon/pageListSyncIconTemplate.html',
    "<span class=\"hyicon se-sync-button__sync \"\n" +
    "    data-ng-class=\"$ctrl.classes[$ctrl.syncStatus.status]\"\n" +
    "    data-sync-status=\"{{$ctrl.syncStatus.status}}\">\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationHeaderTemplate.html',
    "<span class=\"se-sync-panel-header__label\"\n" +
    "    data-translate=\"se.cms.synchronization.panel.lastsync.text\"></span>\n" +
    "<span class=\"se-sync-panel-header__timestamp\">{{pageSync.lastSyncTime | date:'short'}}</span>\n" +
    "<div class=\"se-sync-panel-header__text\">\n" +
    "    <span data-translate=\"se.cms.synchronization.page.header\"></span>\n" +
    "    <y-help data-ng-if=\"pageSync.helpTemplate\"\n" +
    "        data-title=\"pageSync.helpTitle\"\n" +
    "        data-template=\"pageSync.helpTemplate\"></y-help>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationHeaderWrapperTemplate.html',
    "<page-synchronization-header data-last-sync-time=\"sync.syncStatus.lastSyncStatus\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationPanelTemplate.html',
    "<synchronization-panel data-ng-if=\"!pageSync.showSyncButton\"\n" +
    "    data-item-id=\"pageSync.itemId\"\n" +
    "    data-get-sync-status=\"pageSync.getSyncStatus\"\n" +
    "    data-perform-sync=\"pageSync.performSync\"\n" +
    "    data-header-template-url=\"pageSync.headerTemplateUrl\"\n" +
    "    data-sync-items=\"pageSync.syncItems\"\n" +
    "    data-on-selected-items-update=\"pageSync.onSelectedItemsUpdate\">\n" +
    "</synchronization-panel>\n" +
    "<synchronization-panel data-ng-if=\"pageSync.showSyncButton\"\n" +
    "    data-item-id=\"pageSync.itemId\"\n" +
    "    data-get-sync-status=\"pageSync.getSyncStatus\"\n" +
    "    data-perform-sync=\"pageSync.performSync\"\n" +
    "    data-header-template-url=\"pageSync.headerTemplateUrl\">\n" +
    "</synchronization-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/syncMenu/pageSyncMenuToolbarItemTemplate.html',
    "<div class=\"toolbar-action\"\n" +
    "    data-uib-dropdown\n" +
    "    data-auto-close=\"outsideClick\"\n" +
    "    data-is-open=\"$ctrl.toolbarItem.isOpen\"\n" +
    "    data-item-key=\"{{ $ctrl.toolbarItem.key }}\">\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn btn-default toolbar-action--button\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        aria-pressed=\"false\">\n" +
    "        <span class=\"hyicon hyicon-sync se-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <div class=\"toolbar-action--button--txt\">\n" +
    "            <span data-ng-class=\"{'se-toolbar-menu-ddlb--button__txt': $ctrl.isNotInSync }\">\n" +
    "                {{ $ctrl.toolbarItem.name | translate }}\n" +
    "                <span data-ng-if=\"$ctrl.isNotInSync\"\n" +
    "                    class=\"hyicon hyicon-caution se-toolbar-menu-ddlb--button__caution\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </button>\n" +
    "    <div data-uib-dropdown-menu\n" +
    "        class=\"dropdown-menu-left btn-block toolbar-action--include\">\n" +
    "        <div ng-if=\"$ctrl.toolbarItem.isOpen\">\n" +
    "            <ul class=\"se-toolbar-menu-content se-toolbar-menu-content__page-sync\"\n" +
    "                role=\"menu\">\n" +
    "                <li role=\"menuitem\">\n" +
    "                    <div class=\"se-toolbar-menu-content--wrapper\">\n" +
    "                        <div class=\"se-toolbar-menu-content--header\">\n" +
    "                            <div class=\"se-toolbar-menu-content--header__title\"\n" +
    "                                data-translate=\"se.cms.synchronization.page.title\">\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"se-toolbar-menu-content--body\">\n" +
    "                            <page-synchronization-panel></page-synchronization-panel>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/syncMenu/pageSyncMenuToolbarItemWrapperTemplate.html',
    "<page-sync-menu-toolbar-item data-item=\"item\"></page-sync-menu-toolbar-item>"
  );

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */


/**
 * @ngdoc overview
 * @name synchronizationConstantsModule
 * @description
 * Contains various constants used by the synchronization modules.
 *
 * * {@link synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES SYNCHRONIZATION_STATUSES}
 * * {@link synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING SYNCHRONIZATION_POLLING}
 *
 */
angular.module('synchronizationConstantsModule', [])

/**
 * @ngdoc object
 * @name synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES
 *
 * @description
 * Constant containing the different sync statuses
 * * UNAVAILABLE
 * * IN_SYNC
 * * NOT_SYNC
 * * IN_PROGRESS
 * * SYNC_FAILED
 * 
 */
.constant(
    "SYNCHRONIZATION_STATUSES", {
        "UNAVAILABLE": "UNAVAILABLE",
        "IN_SYNC": "IN_SYNC",
        "NOT_SYNC": "NOT_SYNC",
        "IN_PROGRESS": "IN_PROGRESS",
        "SYNC_FAILED": "SYNC_FAILED"
    }
)


/**
 * @ngdoc object
 * @name synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING
 *
 * @description
 * Constant containing polling related values
 * * `SLOW_POLLING_TIME` : the slow polling time in milliseconds (60000ms)
 * * `FAST_POLLING_TIME` : the slow polling time in milliseconds (60000ms)
 * * `SPEED_UP` : event used to speed up polling (`syncPollingSpeedUp`)
 * * `SLOW_DOWN` : event used to slow down polling (`syncPollingSlowDown`)
 * * `FAST_FETCH` : event used to trigger a sync fetch (`syncFastFetch`)
 * * `FETCH_SYNC_STATUS_ONCE`: event used to trigger a one time sync (`fetchSyncStatusOnce`)
 *
 */
.constant(
    "SYNCHRONIZATION_POLLING", {
        "SLOW_POLLING_TIME": 20000,
        "FAST_POLLING_TIME": 2000,
        "SPEED_UP": "syncPollingSpeedUp",
        "SLOW_DOWN": "syncPollingSlowDown",
        "FAST_FETCH": "syncFastFetch",
        "FETCH_SYNC_STATUS_ONCE": "fetchSyncStatusOnce"
    }
);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('synchronizationPanelModule', [
        'yHelpModule',
        'translationServiceModule',
        'alertServiceModule',
        'timerModule',
        'alertsBoxModule',
        'eventServiceModule',
        'synchronizationPollingServiceModule',
        'crossFrameEventServiceModule',
        'waitDialogServiceModule',
        'synchronizationConstantsModule',
        'yMessageModule',
        'sharedDataServiceModule',
        'catalogServiceModule',
        'l10nModule'
    ])
    .controller('SynchronizationPanelController', ['$attrs', '$translate', 'isBlank', 'SYNCHRONIZATION_POLLING', 'SYNCHRONIZATION_STATUSES', 'alertService', 'systemEventService', 'timerService', 'crossFrameEventService', 'waitDialogService', 'catalogService', 'l10nFilter', 'sharedDataService', function($attrs, $translate, isBlank, SYNCHRONIZATION_POLLING, SYNCHRONIZATION_STATUSES, alertService, systemEventService, timerService, crossFrameEventService, waitDialogService, catalogService, l10nFilter, sharedDataService) {

        this.syncPendingItems = [];
        this.selectedItems = [];
        this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
        this.getRows = function() {
            return this.syncStatus ? [this.syncStatus].concat(this.syncStatus.selectedDependencies) : [];
        };
        this.selectionChange = function(index) {

            this.syncStatus.selectedDependencies.forEach(function(item) {
                item.selected = (index === 0 && item.status !== SYNCHRONIZATION_STATUSES.IN_SYNC && !item.isExternal) ? true : item.selected;
            }.bind(this));

            this.selectedItems = this.getRows().filter(function(item) {
                return item.selected;
            });

            if (this.onSelectedItemsUpdate) {
                this.onSelectedItemsUpdate(this.selectedItems);
            }

        };
        this.preserveSelection = function(selectedItems) {

            var itemIds = (selectedItems || []).map(function(item) {
                return item.itemId;
            }, []);

            this.getRows().forEach(function(item) {
                item.selected = itemIds.indexOf(item.itemId) > -1 && item.status !== SYNCHRONIZATION_STATUSES.IN_SYNC;
            });
        };
        this.isDisabled = function(item) {
            return (item !== this.syncStatus && this.syncStatus.selected) || item.status === SYNCHRONIZATION_STATUSES.IN_SYNC;
        };

        this.isSyncButtonDisabled = function() {
            return (this.selectedItems.length === 0 || this.syncPendingItems.length !== 0);
        };
        this.isInSync = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.IN_SYNC;
        };
        this.isOutOfSync = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.NOT_IN_SYNC;
        };
        this.isInProgress = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.IN_PROGRESS;
        };
        this.isSyncFailed = function(dependency) {
            return dependency.status === SYNCHRONIZATION_STATUSES.SYNC_FAILED;
        };
        this.hasHelp = function(dependency) {
            return dependency.dependentItemTypesOutOfSync && dependency.dependentItemTypesOutOfSync.length > 0;
        };
        this.buildInfoTemplate = function(dependency) {
            if (dependency && dependency.dependentItemTypesOutOfSync && !dependency.isExternal) {
                var infoTemplate = dependency.dependentItemTypesOutOfSync.reduce(function(accumulator, item) {
                    accumulator += ("<br/>" + $translate.instant(item.i18nKey));
                    return accumulator;
                }, "");
                return "<div class='se-synchronization-info-template'>" + infoTemplate + "</div>";
            } else if (dependency.isExternal) {
                return dependency.catalogVersionNameTemplate;
            }
        };

        this.fetchSyncStatus = function(eventId, eventData) {
            if (eventData && eventData.itemId !== this.itemId) {
                return;
            }

            return this.getSyncStatus(this.itemId).then(function(syncStatus) {
                sharedDataService.get('experience').then(function(experience) {
                    var targetCatalogVersion = syncStatus.catalogVersionUuid;
                    this.syncStatus = syncStatus;
                    this.preserveSelection(this.selectedItems);
                    this.selectionChange();
                    this._updateStatus();
                    this.markExternalComponents(targetCatalogVersion, this.getRows());
                    this.setTemplateExternalCatalogVersionName(experience, this.getRows());
                    if (this.syncPendingItems.length === 0) {
                        this.refreshInterval = SYNCHRONIZATION_POLLING.SLOW_POLLING_TIME;
                        this.resynchTimer.restart(this.refreshInterval);
                        systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SLOW_DOWN, this.itemId);
                    }
                }.bind(this));
            }.bind(this));
        };

        this.markExternalComponents = function(targetCatalogVersion, syncStatuses) {
            syncStatuses.forEach(function(syncStatus) {
                syncStatus.isExternal = syncStatus.catalogVersionUuid !== targetCatalogVersion;
            });
        };

        this.getInfoTitle = function(dependency) {
            if (!dependency.isExternal) {
                return "se.cms.synchronization.panel.update.title";
            }
        };

        this.getTemplateInfoForExternalComponent = function() {
            return "<div class='se-sync-panel--popover'>" + $translate.instant('se.cms.synchronization.slot.external.component') + "</div>";
        };

        this.setTemplateExternalCatalogVersionName = function(experience, syncStatuses) {
            syncStatuses.forEach(function(syncStatus) {
                syncStatus.catalogVersionNameTemplate = "<span></span>";
                catalogService.getCatalogVersionByUuid(syncStatus.catalogVersionUuid, experience.siteDescriptor.uid).then(function(catalogVersion) {
                    syncStatus.catalogVersionNameTemplate = "<div class='se-sync-panel--popover'>" + l10nFilter(catalogVersion.catalogName) + "</div>";
                });
            });
        };

        this._updateStatus = function() {
            var anyFailures = false;

            var itemsInErrors = '';
            var preNbSyncPendingItems = this.syncPendingItems.length;
            this.getRows().forEach(function(item) {
                if (this.syncPendingItems.indexOf(item.itemId) > -1 && item.status === SYNCHRONIZATION_STATUSES.SYNC_FAILED) {
                    itemsInErrors = ' ' + itemsInErrors + item.itemId;
                    anyFailures = true;
                }
                if (this.syncPendingItems.indexOf(item.itemId) > -1 && item.status !== SYNCHRONIZATION_STATUSES.IN_PROGRESS) {
                    this.syncPendingItems.splice(this.syncPendingItems.indexOf(item.itemId), 1);
                }

                // if there was at least one item in the sync queue, and the last item has been resolved
                // or if there wasn't any item in the sync queue
                if ((preNbSyncPendingItems && this.syncPendingItems.length === 0) || preNbSyncPendingItems === 0) {
                    preNbSyncPendingItems = 0;
                    waitDialogService.hideWaitModal();
                }
                if (item.status === SYNCHRONIZATION_STATUSES.IN_PROGRESS && this.syncPendingItems.indexOf(item.itemId) === -1) {
                    this.syncPendingItems.push(item.itemId);
                }
            }.bind(this));

            if (anyFailures) {
                alertService.showDanger({
                    message: $translate.instant('se.cms.synchronization.panel.failure.message', {
                        items: itemsInErrors
                    })
                });
            }

        };

        this.$onInit = function() {
            this.syncItems = function() {

                var syncPayload = this.getRows().filter(function(item) {
                    return item.selected;
                }).map(function(item) {
                    return {
                        itemId: item.itemId,
                        itemType: item.itemType
                    };
                });
                Array.prototype.push.apply(this.syncPendingItems, syncPayload.map(function(el) {
                    return el.itemId;
                }));

                if (this.selectedItems.length > 0) {
                    waitDialogService.showWaitModal("se.sync.synchronizing");
                    this.performSync(syncPayload).then(function() {
                        this.refreshInterval = SYNCHRONIZATION_POLLING.FAST_POLLING_TIME;
                        this.resynchTimer.restart(this.refreshInterval);
                        systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SPEED_UP, this.itemId);
                    }.bind(this));
                }
            }.bind(this);

            this.unSubscribeFastFetch = crossFrameEventService.subscribe(SYNCHRONIZATION_POLLING.FAST_FETCH, this.fetchSyncStatus.bind(this));
            this.fetchSyncStatus();

            //start timer polling
            this.resynchTimer = timerService.createTimer(this.fetchSyncStatus.bind(this), this.refreshInterval);
            this.resynchTimer.start();
        };

        this.$onDestroy = function() {
            this.resynchTimer.teardown();
            this.unSubscribeFastFetch();
            systemEventService.sendAsynchEvent(SYNCHRONIZATION_POLLING.SLOW_DOWN, this.itemId);
        };

        this.$postLink = function() {
            this.showSyncButton = isBlank($attrs.syncItems);
        };
    }])
    /**
     * @ngdoc directive
     * @name synchronizationPanelModule.component:synchronizationPanel
     * @scope
     * @restrict E
     * @element synchronization-panel
     *
     * @description
     * This component reads and performs synchronization for a main object and its dependencies (from a synchronization perspective).
     * The component will list statuses of backend-configured dependent types.
     * The main object and the listed dependencies will display as well lists of logical grouping of dependencies (in a popover)
     * that cause the main object or the listed dependency to be out of sync.
     * @param {String} itemId the unique identifier of the main object of the synchronization panel
     * @param {Function} getSyncStatus the callback, invoked with itemId that returns a promise of the aggregated sync status required for the component to initialize.
     * the expected format of the aggregated status is the following:
     * ````
     *{
     *  itemId:'someUid',
     *  itemType:'AbstractPage',
     *  name:'Page1',
     *  lastSyncTime: long,
     *  selectAll: 'some.key.for.first.row.item' // Some i18nKey to display the title for the first item
     *  status:{name: 'IN_SYNC', i18nKey: 'some.key.for.in.sync'}  //'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *  dependentItemTypesOutOfSync:[
     *                          {code: 'MetaData', i18nKey: 'some.key.for.MetaData'},
     *                          {code: 'Restrictions', i18nKey: 'some.key.for.Restrictions'},
     *                          {code: 'Slot', i18nKey: 'some.key.for.Slot'},
     *                          {code: 'Component', i18nKey: 'some.key.for.Component'},
     *                          {code: 'Navigation', i18nKey: 'some.key.for.Navigation'},
     *                          {code: 'Customization', i18nKey: 'some.key.for.Customization'}
     *                          ],
     *  selectedDependencies:[
     *  {
     *      itemId:'someUid',
     *      itemType:'ContentSlot',
     *      name:'Slot1',
     *      lastSyncTime: long,
     *      selectAll: 'some.key.for.first.row.item' // Some i18nKey to display the title for the first item
     *      status:{name: 'IN_SYNC', i18nKey: 'some.key.for.in.sync'}  //'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *      dependentItemTypesOutOfSync:[
     *                              {code: 'Component', i18nKey: 'some.key.for.Component'},
     *                              {code: 'Navigation', i18nKey: 'some.key.for.Navigation'},
     *                              {code: 'Customization', i18nKey: 'some.key.for.Customization'}
     *                              ]
     *  }
     *  ]
     *}
     * ````
     *  @param {Function} performSync the function invoked with the list of item unique identifiers to perform synchronization. It returns a promise of the resulting sync status with the following values : 'IN_SYNC'|'NOT_IN_SYNC'|'NOT_AVAILABLE'|'IN_PROGRESS'|'SYNC_FAILED'
     *  @param {String} headerTemplateUrl the path to an HTML template to customize the header of the synchronization panel. Optional.
     *  @param {Function} syncItems enables parent directive/component to invoke the internal synchronization. Optional, if not set, the panel will display a sync button for manual synchronization.
     *  @param {Function} [onSelectedItemsUpdate] callback function invoked when the the list of selected items change
     */
    .component('synchronizationPanel', {

        templateUrl: 'synchronizationPanelTemplate.html',
        controller: 'SynchronizationPanelController',
        controllerAs: 'sync',
        bindings: {
            itemId: '<',
            getSyncStatus: '<',
            performSync: '<',
            headerTemplateUrl: '<?',
            syncItems: '=?',
            onSelectedItemsUpdate: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("configModule", [])
    .config(['$locationProvider', '$qProvider', function($locationProvider, $qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name assetsServiceModule
 * @description
 * # The assetsServiceModule
 *
 * The assetsServiceModule provides methods to handle assets such as images
 *
 */
angular.module('assetsServiceModule', [])
    /**
     * @ngdoc object
     * @name cmsConstantsModule.service:assetsService
     *
     * @description
     * returns the assets resources root depending whether or not we are in test mode
     */
    .factory('assetsService', ['$injector', '$window', function($injector, $window) {
        return {
            getAssetsRoot: function() {
                var testAssets = false;

                if ($injector.has('testAssets')) {
                    testAssets = $injector.get('testAssets');
                }

                return (testAssets || $window.testAssets) ? '/web/webroot' : '/cmssmartedit';
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name cmsItemsRestServiceModule
 * 
 * @description
 * The cmsitemsRestServiceModule provides a service to CRUD operations on CMS Items API.
 */
angular.module('cmsitemsRestServiceModule', [
    'functionsModule',
    'resourceLocationsModule',
    'restServiceFactoryModule',
    'catalogServiceModule',
    'yLoDashModule'
])

.provider('cmsitemsUri', ['CONTEXT_SITE_ID', function(CONTEXT_SITE_ID) {

    this.uri = '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/cmsitems';

    this.$get = function() {
        return this.uri;
    }.bind(this);

}])

/**
 * @ngdoc service
 * @name cmsitemsRestServiceModule.cmsitemsRestService
 * 
 * @description
 * Service to deal with CMS Items related CRUD operations.
 */
.service('cmsitemsRestService', ['lodash', 'restServiceFactory', 'cmsitemsUri', 'catalogService', 'operationContextService', 'OPERATION_CONTEXT', function(lodash, restServiceFactory, cmsitemsUri, catalogService, operationContextService, OPERATION_CONTEXT) {

    var resource = restServiceFactory.get(cmsitemsUri);

    operationContextService.register(cmsitemsUri.replace(/CONTEXT_SITE_ID/, ':CONTEXT_SITE_ID'), OPERATION_CONTEXT.CMS);

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#create
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Create a new CMS Item.
     * 
     * @param {Object} cmsitem The object representing the CMS Item to create
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.create = function(cmsitem) {
        return catalogService.getCatalogVersionUUid().then(function(catalogVersionUUid) {
            cmsitem.catalogVersion = cmsitem.catalogVersion || catalogVersionUUid;
            if (cmsitem.onlyOneRestrictionMustApply === undefined) {
                cmsitem.onlyOneRestrictionMustApply = false;
            }
            return resource.save(cmsitem);
        });
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#getById
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Get the CMS Item that matches the given item uuid (Universally Unique Identifier).
     * 
     * @param {Number} cmsitemUuid The CMS Item uuid
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.getById = function(cmsitemUuid) {
        return resource.getById(cmsitemUuid);
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#get
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Fetch CMS Items serach result by making a REST call to the CMS Items API.
     * A search can be performed by a typeCode (optionnaly in combination of a mask parameter), or by providing a list of cms items uuid.
     *
     * @param {Object} queryParams The object representing the query params
     * @param {String} queryParams.pageSize number of items in the page
     * @param {String} queryParams.currentPage current page number
     * @param {String =} queryParams.typeCode for filtering on the cms item typeCode
     * @param {String =} queryParams.mask for filtering the search
     * @param {String =} queryParams.itemSearchParams search on additional fields using a comma separated list of field name and value
     * pairs which are separated by a colon. Exact matches only.
     * @param {Array =} queryParams.uuids list of cms items uuids
     * @param {String =} queryParams.catalogId the catalog to search items in. If empty, the current context catalog will be used.
     * @param {String =} queryParams.catalogVersion the catalog version to search items in. If empty, the current context catalog version will be used.
     *
     * @returns {Promise} If request is successful, it returns a promise that resolves with the paged search result. If the
     * request fails, it resolves with errors from the backend.
     */
    this.get = function(queryParams) {
        return catalogService.retrieveUriContext().then(function(uriContext) {

            var catalogDetailsParams = {
                catalogId: queryParams.catalogId || uriContext.CURRENT_CONTEXT_CATALOG,
                catalogVersion: queryParams.catalogVersion || uriContext.CURRENT_CONTEXT_CATALOG_VERSION
            };

            queryParams = lodash.merge(catalogDetailsParams, queryParams);

            return resource.get(queryParams);
        });
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#update
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Update a CMS Item.
     * 
     * @param {Object} cmsitem The object representing the CMS Item to update
     * @param {String} cmsitem.identifier The cms item identifier (uuid)
     * 
     * @returns {Promise} If request is successful, it returns a promise that resolves with the updated CMS Item object. If the
     * request fails, it resolves with errors from the backend.
     */
    this.update = function(cmsitem) {
        return resource.update(cmsitem);
    };

    /**
     * @ngdoc method
     * @name cmsitemsRestServiceModule.service:cmsitemsRestService#delete
     * @methodOf cmsitemsRestServiceModule.cmsitemsRestService
     * 
     * @description
     * Remove a CMS Item.
     * 
     * @param {Number} cmsitemUuid The CMS Item uuid
     */
    this.delete = function(cmsitemUuid) {
        return resource.delete(cmsitemUuid);
    };


    this._getByUIdAndType = function(uid, typeCode) {
        return this.get({
            pageSize: 100,
            currentPage: 0,
            mask: uid,
            typeCode: typeCode
        }).then(function(response) {
            return response.response.find(function(element) {
                return element.uid === uid;
            });
        });
    };


}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name resourceModule
     *
     * @description
     * The resource module provides $resource factories.
     */
    angular.module('resourceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'functionsModule'])
        /**
         * @ngdoc service
         * @name resourceModule.service:itemsResource
         *
         * @description
         * This service is used to retrieve the $resource factor for retrieving component items.
         */
        .factory('itemsResource', ['restServiceFactory', 'ITEMS_RESOURCE_URI', 'URIBuilder', function(restServiceFactory, ITEMS_RESOURCE_URI, URIBuilder) {

            return {
                /**
                 * @ngdoc method
                 * @name resourceModule.service:itemsResource#getItemResourceByContext
                 * @methodOf resourceModule.service:itemsResource
                 * 
                 * @description
                 * Returns  the resource of the custom components REST service by replacing the placeholders with the currently selected catalog version.
                 */
                getItemResource: function() {
                    return restServiceFactory.get(ITEMS_RESOURCE_URI);
                },

                /**
                 * @ngdoc method
                 * @name resourceModule.service:itemsResource#getItemResourceByContext
                 * @methodOf resourceModule.service:itemsResource
                 * 
                 * @description
                 * Returns  the resource of the custom components REST service by providing the current uri context as the input object.
                 * 
                 * The input object contains the necessary site and catalog information to retrieve the items.
                 * 
                 * @param {Object} uriContext A  {@link resourceLocationsModule.object:UriContext uriContext}
                 */
                getItemResourceByContext: function(context) {
                    return restServiceFactory.get(new URIBuilder(ITEMS_RESOURCE_URI).replaceParams(context).build());
                }

            };

        }])
        .factory('navigationNodeEntriesResource', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI, 'uid');
        }])
        .factory('pagesContentSlotsComponentsResource', ['restServiceFactory', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', function(restServiceFactory, PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI) {
            return restServiceFactory.get(PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI);
        }])
        // @deprecated since 6.5
        .factory('pageInfoRestService', ['restServiceFactory', 'PAGEINFO_RESOURCE_URI', function(restServiceFactory, PAGEINFO_RESOURCE_URI) {
            return restServiceFactory.get(PAGEINFO_RESOURCE_URI);
        }])
        .factory('navigationNodeRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_RESOURCE_URI);
        }])
        .factory('navigationEntriesRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI);
        }])
        .factory('navigationEntryTypesRestService', ['restServiceFactory', 'NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI', function(restServiceFactory, NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI) {
            return restServiceFactory.get(NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI);
        }])
        .factory('synchronizationResource', ['restServiceFactory', 'URIBuilder', 'GET_PAGE_SYNCHRONIZATION_RESOURCE_URI', 'POST_PAGE_SYNCHRONIZATION_RESOURCE_URI', function(restServiceFactory, URIBuilder, GET_PAGE_SYNCHRONIZATION_RESOURCE_URI, POST_PAGE_SYNCHRONIZATION_RESOURCE_URI) {

            return {

                getPageSynchronizationGetRestService: function(uriContext) {
                    var getURI = new URIBuilder(GET_PAGE_SYNCHRONIZATION_RESOURCE_URI).replaceParams(uriContext).build();
                    return restServiceFactory.get(getURI);
                },

                getPageSynchronizationPostRestService: function(uriContext) {
                    var postURI = new URIBuilder(POST_PAGE_SYNCHRONIZATION_RESOURCE_URI).replaceParams(uriContext).build();
                    return restServiceFactory.get(postURI);
                }

            };

        }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    var CONTEXT_CATALOG = 'CURRENT_CONTEXT_CATALOG',
        CONTEXT_CATALOG_VERSION = 'CURRENT_CONTEXT_CATALOG_VERSION',
        CONTEXT_SITE_ID = 'CURRENT_CONTEXT_SITE_ID';

    var PAGE_CONTEXT_CATALOG = 'CURRENT_PAGE_CONTEXT_CATALOG',
        PAGE_CONTEXT_CATALOG_VERSION = 'CURRENT_PAGE_CONTEXT_CATALOG_VERSION',
        PAGE_CONTEXT_SITE_ID = 'CURRENT_PAGE_CONTEXT_SITE_ID';

    angular.module('resourceLocationsModule')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_SITE_ID
     *
     * @description
     * Constant containing the name of the site uid placeholder in URLs
     */
    .constant('CONTEXT_SITE_ID', CONTEXT_SITE_ID)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_CATALOG
     *
     * @description
     * Constant containing the name of the catalog uid placeholder in URLs
     */
    .constant('CONTEXT_CATALOG', CONTEXT_CATALOG)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_CATALOG_VERSION
     *
     * @description
     * Constant containing the name of the catalog version placeholder in URLs
     */
    .constant('CONTEXT_CATALOG_VERSION', CONTEXT_CATALOG_VERSION)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID
     *
     * @description
     * Constant containing the name of the current page site uid placeholder in URLs
     */
    .constant('PAGE_CONTEXT_SITE_ID', PAGE_CONTEXT_SITE_ID)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG
     *
     * @description
     * Constant containing the name of the current page catalog uid placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG', PAGE_CONTEXT_CATALOG)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION
     *
     * @description
     * Constant containing the name of the current page catalog version placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG_VERSION', PAGE_CONTEXT_CATALOG_VERSION)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:UriContext
     *
     * @description
     * A map that contains the necessary site and catalog information for CMS services and directives.
     * It contains the following keys:
     * {@link resourceLocationsModule.object:CONTEXT_SITE_ID CONTEXT_SITE_ID} for the site uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG CONTEXT_CATALOG} for the catalog uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG_VERSION CONTEXT_CATALOG_VERSION} for the catalog version.
     */

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:TYPES_RESOURCE_URI
     *
     * @description
     * Resource URI of the component types REST service.
     */
    .constant('TYPES_RESOURCE_URI', '/cmswebservices/v1/types')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:STRUCTURES_RESOURCE_URI
     *
     * @description
     * Resource URI of the component structures REST service.
     */
    .constant('STRUCTURES_RESOURCE_URI', '/cmssmarteditwebservices/v1/structures')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:ITEMS_RESOURCE_URI
     *
     * @description
     * Resource URI of the custom components REST service.
     */
    .constant('ITEMS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/items')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages content slot component REST service.
     */
    .constant('PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagescontentslotscomponents')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI
     *
     * @description
     * Resource URI of the content slot type restrictions REST service.
     */
    .constant('CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI', '/cmswebservices/v1/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pages/:pageUid/contentslots/:slotUid/typerestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsMod`ule.object:PAGES_LIST_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages REST service.
     */
    .constant('PAGES_LIST_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_LIST_PATH
     *
     * @description
     * Path of the page list
     */
    .constant('PAGE_LIST_PATH', '/pages/:siteId/:catalogId/:catalogVersion')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGES_CONTENT_SLOT_RESOURCE_URI
     *
     * @description
     * Resource URI of the page content slots REST service
     */
    .constant('PAGES_CONTENT_SLOT_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagescontentslots')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_TEMPLATES_URI
     *
     * @description
     * Resource URI of the page templates REST service
     */
    .constant('PAGE_TEMPLATES_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/pagetemplates')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_PAGE_PATH
     *
     * @description
     * Path to the Navigation Management
     */
    .constant('NAVIGATION_MANAGEMENT_PAGE_PATH', '/navigations/:siteId/:catalogId/:catalogVersion')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigations REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_RESOURCE_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/navigations')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigations REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI', '/cmswebservices/v1/sites/:' + CONTEXT_SITE_ID + '/catalogs/:' + CONTEXT_CATALOG + '/versions/:' + CONTEXT_CATALOG_VERSION + '/navigations/:navigationUid/entries')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI
     *
     * @description
     * Resource URI of the navigation entry types REST service.
     */
    .constant('NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI', '/cmswebservices/v1/navigationentrytypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the pages REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('CONTEXTUAL_PAGES_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/pagesrestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/:siteUID/catalogs/:catalogId/versions/:catalogVersion/pagesrestrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pagesrestrictions/pages')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTIONS_RESOURCE_URI
     *
     * @description
     * Resource URI of the restrictions REST service, with placeholders to be replaced by the currently selected catalog version.
     */
    .constant('RESTRICTIONS_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/restrictions')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTION_TYPES_URI
     *
     * @description
     * Resource URI of the restriction types REST service.
     */
    .constant('RESTRICTION_TYPES_URI', '/cmswebservices/v1/restrictiontypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.RESTRICTION_TYPES_URI
     *
     * @description
     * Resource URI of the pageTypes-restrictionTypes relationship REST service.
     */
    .constant('PAGE_TYPES_RESTRICTION_TYPES_URI', '/cmswebservices/v1/pagetypesrestrictiontypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGEINFO_RESOURCE_URI
     * @deprecated since 6.5
     *
     * @description
     * Resource URI of the page info REST service.
     */
    .constant('PAGEINFO_RESOURCE_URI', '/cmswebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/pages/:pageUid')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.PAGE_TYPES_URI
     *
     * @description
     * Resource URI of the page types REST service.
     */
    .constant('PAGE_TYPES_URI', '/cmswebservices/v1/pagetypes')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:GET_PAGE_SYNCHRONIZATION_RESOURCE_URI
     *
     * @description
     * Resource URI to retrieve the full synchronization status of page related items
     */
    .constant('GET_PAGE_SYNCHRONIZATION_RESOURCE_URI', '/cmssmarteditwebservices/v1/sites/' + PAGE_CONTEXT_SITE_ID + '/catalogs/' + PAGE_CONTEXT_CATALOG + '/versions/' + PAGE_CONTEXT_CATALOG_VERSION + '/synchronizations/versions/:target/pages/:pageUid')


    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:POST_PAGE_SYNCHRONIZATION_RESOURCE_URI
     *
     * @description
     * Resource URI to perform synchronization of page related items
     */
    .constant('POST_PAGE_SYNCHRONIZATION_RESOURCE_URI', '/cmssmarteditwebservices/v1/sites/' + CONTEXT_SITE_ID + '/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/synchronizations/versions/:target');
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('componentServiceModule', [
    'restServiceFactoryModule',
    'resourceLocationsModule',
    'cmsitemsRestServiceModule',
    'catalogServiceModule',
    'slotRestrictionsServiceModule'
])

/**
 * @ngdoc service
 * @name componentMenuModule.ComponentService
 *
 * @description
 * Service which manages component types and items
 */
.service('ComponentService', ['restServiceFactory', 'TYPES_RESOURCE_URI', 'PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'cmsitemsRestService', 'catalogService', 'slotRestrictionsService', function(
    restServiceFactory,
    TYPES_RESOURCE_URI,
    PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI,
    CONTEXT_CATALOG,
    CONTEXT_CATALOG_VERSION,
    cmsitemsRestService,
    catalogService,
    slotRestrictionsService) {

    var restServiceForTypes = restServiceFactory.get(TYPES_RESOURCE_URI);
    var restServiceForItems = cmsitemsRestService;
    var restServiceForAddExistingComponent = restServiceFactory.get(PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI);

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#createNewComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description given a component info and the component payload, a new componentItem is created and added to a slot
     *
     * @param {Object} componentInfo The basic information of the ComponentType to be created and added to the slot.
     * @param {String} componentInfo.componenCode componenCode of the ComponentType to be created and added to the slot.
     * @param {String} componentInfo.name name of the new component to be created.
     * @param {String} componentInfo.pageId pageId used to identify the current page template.
     * @param {String} componentInfo.slotId slotId used to identify the slot in the current template.
     * @param {String} componentInfo.position position used to identify the position in the slot in the current template.
     * @param {String} componentInfo.type type of the component being created.
     * @param {Object} componentPayload payload of the new component to be created.
     */
    this.createNewComponent = function(componentInfo, componentPayload) {

        //FIXME fix naming of slotId
        var _payload = {};
        _payload.name = componentInfo.name;
        _payload.slotId = componentInfo.targetSlotId;
        _payload.pageId = componentInfo.pageId;
        _payload.position = componentInfo.position;
        _payload.typeCode = componentInfo.componentType;
        _payload.itemtype = componentInfo.componentType;
        _payload.catalogVersion = componentInfo.catalogVersionUuid;

        if (typeof componentPayload === "object") {
            for (var property in componentPayload) {
                _payload[property] = componentPayload[property];
            }
        } else if (componentPayload) {
            throw "ComponentService.createNewComponent() - Illegal componentPayload - [" + componentPayload + "]";
        }

        return restServiceForItems.create(_payload);

    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#updateComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description Given a component info and the payload related to an existing component, the latter will be updated with the new supplied values.
     *
     * @param {Object} componentPayload of the new component to be created, including the info.
     * @param {String} componentPayload.componenCode of the ComponentType to be created and added to the slot.
     * @param {String} componentPayload.name of the new component to be created.
     * @param {String} componentPayload.pageId used to identify the current page template.
     * @param {String} componentPayload.slotId used to identify the slot in the current template.
     * @param {String} componentPayload.position used to identify the position in the slot in the current template.
     * @param {String} componentPayload.type of the component being created.
     */
    this.updateComponent = function(componentPayload) {
        return restServiceForItems.update(componentPayload);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#addExistingComponent
     * @methodOf componentMenuModule.ComponentService
     *
     * @description add an existing component item to a slot
     *
     * @param {String} pageId used to identify the page containing the slot in the current template.
     * @param {String} componentId used to identify the existing component which will be added to the slot.
     * @param {String} slotId used to identify the slot in the current template.
     * @param {String} position used to identify the position in the slot in the current template.
     */
    this.addExistingComponent = function(pageId, componentId, slotId, position) {

        var _payload = {};
        _payload.pageId = pageId;
        _payload.slotId = slotId;
        _payload.componentId = componentId;
        _payload.position = position;

        return restServiceForAddExistingComponent.save(_payload);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadComponentTypes
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all component types are retrieved
     */
    this.loadComponentTypes = function() {
        return restServiceForTypes.get({
            category: 'COMPONENT'
        });
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#getSupportedComponentTypesForCurrentPage
     * @methodOf componentMenuModule.ComponentService
     *
     * @description Fetches all component types supported by the system, then filters this list
     * using the restricted component types for all the slots on the current page.
     *
     * @returns {Array} A promise resolving to the component types that can be added to the current page
     */
    this.getSupportedComponentTypesForCurrentPage = function() {
        return this.loadComponentTypes().then(function(response) {
            return slotRestrictionsService.getAllComponentTypesSupportedOnPage().then(function(supportedTypes) {
                return response.componentTypes.filter(function(componentType) {
                    return supportedTypes.indexOf(componentType.code) > -1;
                });
            });
        });
    }.bind(this);

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadComponentItem
     * @methodOf componentMenuModule.ComponentService
     *
     * @description load a component identified by its id
     */
    this.loadComponentItem = function(id) {
        return restServiceForItems.getById(id);
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadPagedComponentItems
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all existing component items for the current catalog are retrieved in the form of pages
     * used for pagination especially when the result set is very large.
     * 
     * @param {String} mask the search string to filter the results.
     * @param {String} pageSize the number of elements that a page can contain.
     * @param {String} page the current page number.
     */
    this.loadPagedComponentItems = function(mask, pageSize, page) {

        return catalogService.retrieveUriContext().then(function(uriContext) {

            var requestParams = {
                pageSize: pageSize,
                currentPage: page,
                mask: mask,
                sort: 'name',
                typeCode: 'AbstractCMSComponent',
                catalogId: uriContext[CONTEXT_CATALOG],
                catalogVersion: uriContext[CONTEXT_CATALOG_VERSION]
            };

            return restServiceForItems.get(requestParams);
        });
    };

    /**
     * @ngdoc method
     * @name componentMenuModule.ComponentService#loadPagedComponentItemsByCatalogVersion
     * @methodOf componentMenuModule.ComponentService
     *
     * @description all existing component items for the provided content catalog are retrieved in the form of pages
     * used for pagination especially when the result set is very large.
     * 
     * @param {Object} payload The payload that contains the information of the page of components to load
     * @param {String} payload.catalogId the id of the catalog for which to retrieve the component items. 
     * @param {String} payload.catalogVersion the id of the catalog version for which to retrieve the component items. 
     * @param {String} payload.mask the search string to filter the results.
     * @param {String} payload.pageSize the number of elements that a page can contain.
     * @param {String} payload.page the current page number.
     * 
     * @returns {Promise} A promise resolving to a page of component items retrieved from the provided catalog version. 
     */
    this.loadPagedComponentItemsByCatalogVersion = function(payload) {
        var requestParams = {
            pageSize: payload.pageSize,
            currentPage: payload.page,
            mask: payload.mask,
            sort: 'name',
            typeCode: 'AbstractCMSComponent',
            catalogId: payload.catalogId,
            catalogVersion: payload.catalogVersion
        };

        return restServiceForItems.get(requestParams);
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name componentVisibilityAlertServiceInterfaceModule
 *
 * @description
 * This module defines the componentVisibilityAlertServiceInterfaceModule Angular component
 * and its associated service. It uses the cmsContextualAlertModule to display
 * a contextual alert whenever a component get either hidden or restricted.
 */
angular.module("componentVisibilityAlertServiceInterfaceModule", [])

/**
 * @ngdoc service
 * @name componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface
 *
 * @description
 * The componentVisibilityAlertServiceInterface is used by external modules to check
 * on a component visibility and trigger the display of a contextual alert when
 * the component is either hidden or restricted.
 */
.factory('ComponentVisibilityAlertServiceInterface', function() {

    var ComponentVisibilityAlertServiceInterface = function() {};

    /**
     * @ngdoc method
     * @name componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface#checkAndAlertOnComponentVisibility
     * @methodOf componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface
     *
     * @description
     * Method checks on a component visibility and triggering the display of a
     * contextual alert when the component is either hidden or restricted. This
     * method defines a custom Angular controller which will get passed to and
     * consumed by the contextual alert.
     *
     * @param {Object} component A JSON object containing the specific configuration to be applied on the actionableAlert.
     * @param {String} component.componentId Uuid of the cmsItem
     * @param {String} component.componentType Type of the cmsItem
     * @param {String} component.catalogVersion CatalogVersion uuid of the cmsItem
     * @param {String} component.restricted Boolean stating whether a restriction is applied to the cmsItem.
     * @param {String} component.slotId Id of the slot where the cmsItem was added or modified.
     * @param {String} component.visibility Boolean stating whether the cmsItem is rendered.
     */
    ComponentVisibilityAlertServiceInterface.prototype.checkAndAlertOnComponentVisibility = function() {};

    return ComponentVisibilityAlertServiceInterface;

});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name removeComponentServiceInterfaceModule
 * @description
 * # The removeComponentServiceInterfaceModule
 *
 * Provides a service with the ability to remove a component from a slot.
 */
angular.module('removeComponentServiceInterfaceModule', [])
    /**
     * @ngdoc service
     * @name removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface
     * @description
     * Service interface specifying the contract used to remove a component from a slot.
     *
     * This class serves as an interface and should be extended, not instantiated.
     */
    .factory('RemoveComponentServiceInterface', function() {
        function RemoveComponentServiceInterface() {}

        /**
         * @ngdoc method
         * @name removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface#removeComponent
         * @methodOf removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface
         *
         * @description
         * Removes the component specified by the given ID from the component specified by the given ID.
         *
         * @param {String} slotId The ID of the slot from which to remove the component.
         * @param {String} componentId The ID of the component to remove from the slot.
         */
        RemoveComponentServiceInterface.prototype.removeComponent = function() {};

        return RemoveComponentServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name slotVisibilityServiceModule
 * @description
 *
 * The slot visibility service module provides factories and services to manage all backend calls and loads an internal
 * structure that provides the necessary data to the slot visibility button and slot visibility component.
 */
angular.module('slotVisibilityServiceModule', ['resourceModule', 'componentHandlerServiceModule', 'cmsitemsRestServiceModule', 'crossFrameEventServiceModule'])
    /**
     * @ngdoc service
     * @name slotVisibilityServiceModule.service:SlotVisibilityService
     * @description
     *
     * The SlotVisibilityService class provides methods to interact with the backend.
     * The definition of the class is not instantiated immediately, whereas the service instance of
     * this same class (@see slotVisibilityService) returns an instance of this service definition.
     *
     * @param {Object} cmsitemsRestService Gets all components based on their UUIDs.
     * @param {Object} pagesContentSlotsComponentsResource Gets content slots and components based on their page IDs.
     * @param {Object} componentHandlerService Gets the current page ID.
     */
    .service('SlotVisibilityService', ['$q', 'cmsitemsRestService', 'pagesContentSlotsComponentsResource', 'componentHandlerService', function($q, cmsitemsRestService, pagesContentSlotsComponentsResource, componentHandlerService) {

        var SlotVisibilityService = function() {};

        /**
         * Function that filters the given SlotsToComponentsMap to return only those components that are hidden in the storefront.
         * @param {Object} allSlotsToComponentsMap object containing slotId - components list.
         *
         * @return {Object} allSlotsToComponentsMap object containing slotId - components list.
         */
        SlotVisibilityService.prototype._filterVisibleComponents = function(allSlotsToComponentsMap) {

            //filter allSlotsToComponentsMap to show only hidden components
            Object.keys(allSlotsToComponentsMap).forEach(function(slotId) {

                var componentsOnDOM = [];
                componentHandlerService.getOriginalComponentsWithinSlot(slotId).get().forEach(function(component) {
                    componentsOnDOM.push(componentHandlerService.getId(component));
                });

                var hiddenComponents = allSlotsToComponentsMap[slotId].filter(function(component) {
                    return componentsOnDOM.indexOf(component.uid) === -1;
                });

                allSlotsToComponentsMap[slotId] = hiddenComponents;

            });

            return allSlotsToComponentsMap;
        };

        /**
         * Converts the provided list of pageContentSlotsComponents to slotId - components list map.
         * @param {Object} pageContentSlotsComponents object containing list of slots and components for the page in context
         */
        SlotVisibilityService.prototype._loadSlotsToComponentsMap = function(pageContentSlotsComponents) {
            var componentUuids = (pageContentSlotsComponents.pageContentSlotComponentList || [])
                .map(function(pageContentSlotComponent) {
                    return pageContentSlotComponent.componentUuid;
                });
            return cmsitemsRestService.get({
                uuids: componentUuids.join(',')
            }).then(function(components) {

                //load all components as ComponentUuid-Component map
                var allComponentsMap = (components.response || []).reduce(function(map, component) {
                    map[component.uuid] = component;
                    return map;
                }, {});

                //load all components as SlotUuid-Component[] map
                var allSlotsToComponentsMap = (pageContentSlotsComponents.pageContentSlotComponentList || [])
                    .reduce(function(map, pageContentSlotComponent) {
                        map[pageContentSlotComponent.slotId] = map[pageContentSlotComponent.slotId] || [];
                        if (allComponentsMap[pageContentSlotComponent.componentUuid]) {
                            map[pageContentSlotComponent.slotId].push(allComponentsMap[pageContentSlotComponent.componentUuid]);
                        }
                        return map;
                    }, {});

                return allSlotsToComponentsMap;
            });
        };

        SlotVisibilityService.prototype._getPagesContentSlotsComponentsPromise = function() {
            return this.hiddenComponentsMapPromise || this.reloadSlotsInfo();
        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#reloadSlotInfo
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Reloads and cache's the pagesContentSlotsComponents for the current page in context.
         * this method can be called when ever a component is added or modified to the slot so that the pagesContentSlotsComponents is re-evalated.
         *
         * @return {Promise} A promise that resolves to the pagesContentSlotsComponents for the page in context.
         */
        SlotVisibilityService.prototype.reloadSlotsInfo = function() {
            try {
                var currentPageId = componentHandlerService.getPageUID();
                this.hiddenComponentsMapPromise = pagesContentSlotsComponentsResource.get({
                    pageId: currentPageId
                });
                return this.hiddenComponentsMapPromise;
            } catch (e) {
                if (e.name === "InvalidStorefrontPageError") {
                    return $q.when({});
                } else {
                    throw e;
                }
            }

        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#getSlotsForComponent
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Returns an array of slotId's in which the provided component (identified by its componentUuid) exists.
         *
         * @param {String} componentUuid The uuid of the component.
         *
         * @return {Promise} A promise that resolves to a array of slotId's for the given componnet uuid.
         */
        SlotVisibilityService.prototype.getSlotsForComponent = function(componentUuid) {
            var slotIds = [];
            return this._getSlotToComponentsMap(true).then(function(allSlotsToComponentsMap) {
                Object.keys(allSlotsToComponentsMap).forEach(function(slotId) {
                    if (allSlotsToComponentsMap[slotId].find(function(component) {
                            return component.uuid === componentUuid;
                        })) {
                        slotIds.push(slotId);
                    }
                });

                return slotIds;
            });
        };

        /**
         * Function to load slot to component map for the current page in context
         * @param reload boolean to specify if the pagesContentSlotsComponents resource needs to be re-called.
         */
        SlotVisibilityService.prototype._getSlotToComponentsMap = function(reload) {
            return (reload ? this.reloadSlotsInfo() : this._getPagesContentSlotsComponentsPromise()).then(this._loadSlotsToComponentsMap);
        };

        /**
         * @ngdoc method
         * @name slotVisibilityServiceModule.service:SlotVisibilityService#getHiddenComponents
         * @methodOf slotVisibilityServiceModule.service:SlotVisibilityService
         *
         * @description
         * Returns the list of hidden components for a given slotId
         *
         * @param {String} slotId the slot id
         *
         * @return {Promise} A promise that resolves to a list of hidden components for the slotId
         */
        SlotVisibilityService.prototype.getHiddenComponents = function(slotId) {
            return this._getSlotToComponentsMap(false).then(this._filterVisibleComponents).then(function(hiddenComponentsMap) {
                return hiddenComponentsMap[slotId] || [];
            }, function() {
                return [];
            });
        };

        return SlotVisibilityService;

    }])
    .service('slotVisibilityService', ['SlotVisibilityService', 'crossFrameEventService', 'EVENTS', function(SlotVisibilityService, crossFrameEventService, EVENTS) {
        var instance = new SlotVisibilityService();
        crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
            instance._getSlotToComponentsMap(true);
        });
        return instance;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name synchronizationServiceModule
 * @description
 *
 * The synchronization module contains the service necessary to perform catalog synchronization.
 *
 * The {@link synchronizationServiceModule.service:synchronizationService synchronizationService} 
 * calls backend API in order to get synchronization status or trigger a catalog synchronization between two catalog versions.
 *
 */
angular.module('synchronizationServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'alertServiceModule', 'authenticationModule', 'timerModule'])
    /**
     * @ngdoc object
     * @name synchronizationServiceModule.object:CATALOG_SYNC_INTERVAL_IN_MILLISECONDS
     * @description
     * This object defines an injectable Angular constant that determines the frequency to update catalog synchronization information. 
     * Value given in milliseconds. 
     */
    .constant('CATALOG_SYNC_INTERVAL_IN_MILLISECONDS', 5000)
    /**
     * @ngdoc service
     * @name synchronizationServiceModule.service:synchronizationService
     * @description
     *
     * The synchronization service manages RESTful calls to the synchronization service's backend API.
     * 
     */
    .service('synchronizationService', ['restServiceFactory', 'timerService', '$q', '$translate', 'alertService', 'authenticationService', 'operationContextService', 'OPERATION_CONTEXT', 'CATALOG_SYNC_INTERVAL_IN_MILLISECONDS', function(restServiceFactory, timerService, $q, $translate, alertService, authenticationService, operationContextService, OPERATION_CONTEXT, CATALOG_SYNC_INTERVAL_IN_MILLISECONDS) {

        // Constants
        var BASE_URL = "/cmswebservices";
        var SYNC_JOB_INFO_BY_TARGET_URI = '/cmswebservices/v1/catalogs/:catalog/synchronizations/targetversions/:target';
        var SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI = '/cmswebservices/v1/catalogs/:catalog/versions/:source/synchronizations/versions/:target';

        // Variables
        var intervalHandle = {};
        var syncJobInfoByTargetRestService = restServiceFactory.get(SYNC_JOB_INFO_BY_TARGET_URI);
        var syncJobInfoBySourceAndTargetRestService = restServiceFactory.get(SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI, 'catalog');

        operationContextService.register(SYNC_JOB_INFO_BY_TARGET_URI, OPERATION_CONTEXT.CMS);
        operationContextService.register(SYNC_JOB_INFO_BY_SOURCE_AND_TARGET_URI, OPERATION_CONTEXT.CMS);

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#updateCatalogSync
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to synchronize a catalog between two catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.updateCatalogSync = function(catalog) {
            return syncJobInfoBySourceAndTargetRestService.update({
                'catalog': catalog.catalogId,
                'source': catalog.sourceCatalogVersion,
                'target': catalog.targetCatalogVersion
            }).then(function(response) {
                return response;
            }.bind(this), function(reason) {
                var translationErrorMsg = $translate.instant('sync.running.error.msg', {
                    catalogName: catalog.name
                });
                if (reason.statusText === 'Conflict') {
                    alertService.showDanger({
                        message: translationErrorMsg
                    });
                }
                return false;
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job between two catalog versions. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getCatalogSyncStatus = function(catalog) {
            if (catalog.sourceCatalogVersion) {
                return this.getSyncJobInfoBySourceAndTarget(catalog);
            } else {
                return this.getLastSyncJobInfoByTarget(catalog);
            }
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job between two catalog versions. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getSyncJobInfoBySourceAndTarget = function(catalog) {
            return syncJobInfoBySourceAndTargetRestService.get({
                'catalog': catalog.catalogId,
                'source': catalog.sourceCatalogVersion,
                'target': catalog.targetCatalogVersion
            });
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#getCatalogSyncStatus
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method is used to get the status of the last synchronization job. 
         * 
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.getLastSyncJobInfoByTarget = function(catalog) {
            return syncJobInfoByTargetRestService.get({
                'catalog': catalog.catalogId,
                'target': catalog.targetCatalogVersion
            });
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#stopAutoGetSyncData
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method starts the auto synchronization status update in a catalog between two given catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.startAutoGetSyncData = function(catalog, callback) {
            var catalogId = catalog.catalogId;
            var sourceCatalogVersion = catalog.sourceCatalogVersion;
            var targetCatalogVersion = catalog.targetCatalogVersion;

            var jobKey = this._getJobKey(catalogId, sourceCatalogVersion, targetCatalogVersion);

            var syncJobTimer = timerService.createTimer(this._autoSyncCallback.bind(this, catalog, callback, jobKey), CATALOG_SYNC_INTERVAL_IN_MILLISECONDS);
            syncJobTimer.start();

            intervalHandle[jobKey] = syncJobTimer;
        };

        this._autoSyncCallback = function(catalog, callback, jobKey) {
            authenticationService.isAuthenticated(BASE_URL).then(function(response) {
                if (!response) {
                    this.stopAutoGetSyncData(catalog);
                }
                this.getCatalogSyncStatus(catalog)
                    .then(callback)
                    .then(function() {
                        if (!intervalHandle[jobKey]) {
                            this.startAutoGetSyncData(catalog, callback);
                        }
                    }.bind(this));
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name synchronizationServiceModule.service:synchronizationService#stopAutoGetSyncData
         * @methodOf synchronizationServiceModule.service:synchronizationService
         *
         * @description
         * This method stops the auto synchronization status update in a catalog between two given catalog versions.
         *
         * @param {Object} catalog An object that contains the information about the catalog to be synchronized.
         * @param {String} catalog.catalogId The ID of the catalog to synchronize. 
         * @param {String=} catalog.sourceCatalogVersion The name of the source catalog version. 
         * @param {String} catalog.targetCatalogVersion The name of the target catalog version.
         */
        this.stopAutoGetSyncData = function(catalog) {
            var jobKey = this._getJobKey(catalog.catalogId, catalog.sourceCatalogVersion, catalog.targetCatalogVersion);
            if (intervalHandle[jobKey]) {
                intervalHandle[jobKey].stop();
                intervalHandle[jobKey] = undefined;
            }
        };

        this._getJobKey = function(catalogId, sourceCatalogVersion, targetCatalogVersion) {
            return catalogId + "_" + sourceCatalogVersion + "_" + targetCatalogVersion;
        };
    }]);

angular.module('cmscommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmscommons/components/synchronize/synchronizationPanelTemplate.html',
    "<div class=\"se-sync-panel\">\n" +
    "    <y-message data-ng-if=\"false\"\n" +
    "        data-message-id=\"yMsgWarningId\"\n" +
    "        data-type=\"warning\"\n" +
    "        data-ng-class=\"!sync.showSyncButton ? 'se-sync-panel--y-message--modal-adjusted' : 'se-sync-panel--y-message--toolbar-adjusted'\">\n" +
    "        <message-title>{{ 'se.cms.synchronization.panel.live.recent.notice' | translate }}</message-title>\n" +
    "        <message-description>{{ 'se.cms.synchronization.panel.live.override.warning' | translate }}</message-description>\n" +
    "    </y-message>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-status\"\n" +
    "        data-ng-if=\"sync.headerTemplateUrl\"\n" +
    "        data-ng-include=\"sync.headerTemplateUrl\">\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-info\">\n" +
    "        <div data-ng-repeat=\"dependency in sync.getRows()\"\n" +
    "            data-ng-class=\"{active: $index==0, 'se-sync-panel--item__external': dependency.isExternal}\"\n" +
    "            class=\"se-sync-panel__sync-info__row\">\n" +
    "\n" +
    "            <div class=\"checkbox se-sync-panel__sync-info__checkbox se-nowrap-ellipsis\">\n" +
    "                <input type=\"checkbox\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-model=\"dependency.selected\"\n" +
    "                    data-ng-disabled=\"sync.isDisabled(dependency)\"\n" +
    "                    data-ng-change=\"sync.selectionChange($index)\"\n" +
    "                    id=\"sync-info__checkbox_{{$index}}\">\n" +
    "                <label data-ng-if=\"$index===0\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.selectAll | translate}}\">\n" +
    "                    {{::dependency.selectAll | translate}}</label>\n" +
    "\n" +
    "                <label data-ng-if=\"$index!==0 && !dependency.isExternal\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.name | translate}}\">\n" +
    "                    {{::dependency.name | translate}}</label>\n" +
    "\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    data-y-popover\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-template=\"sync.getTemplateInfoForExternalComponent()\">\n" +
    "                    <label data-ng-if=\"$index!==0\"\n" +
    "                        for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                        class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                        title=\"{{::dependency.name | translate}}\">\n" +
    "                        {{::dependency.name | translate}}</label>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "            <span data-ng-if=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-y-popover\n" +
    "                data-trigger=\"'hover'\"\n" +
    "                data-title=\"sync.getInfoTitle(dependency)\"\n" +
    "                data-template=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-ng-class=\"{'pull-right se-sync-panel__sync-info__right-icon': true, 'se-sync-panel--icon-globe': dependency.isExternal} \">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    class=\"hyicon hyicon-globe\"></span>\n" +
    "            </span>\n" +
    "\n" +
    "            <span data-ng-if=\"!sync.buildInfoTemplate(dependency)\"\n" +
    "                class=\"pull-right se-sync-panel__sync-info__right-icon\">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__footer\"\n" +
    "        data-ng-if=\"sync.showSyncButton\">\n" +
    "        <button class=\"btn btn-lg btn-primary se-sync-panel__footer__btn\"\n" +
    "            data-ng-disabled=\"sync.isSyncButtonDisabled()\"\n" +
    "            data-ng-click=\"sync.syncItems()\"\n" +
    "            data-translate=\"se.cms.pagelist.dropdown.sync\"></button>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );

}]);
