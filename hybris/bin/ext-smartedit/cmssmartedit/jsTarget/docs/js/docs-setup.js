NG_DOCS={
  "sections": {
    "cmssmartedit": "SmartEdit CMS",
    "cmssmarteditContainer": "SmartEdit CMS Container"
  },
  "pages": [
    {
      "section": "cmssmartedit",
      "id": "assetsServiceModule",
      "shortName": "assetsServiceModule",
      "type": "overview",
      "moduleName": "assetsServiceModule",
      "shortDescription": "The assetsServiceModule",
      "keywords": "assets assetsservicemodule cmssmartedit handle images methods overview"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsConstantsModule.service:assetsService",
      "shortName": "assetsService",
      "type": "object",
      "moduleName": "cmsConstantsModule",
      "shortDescription": "returns the assets resources root depending whether or not we are in test mode",
      "keywords": "assets cmsconstantsmodule cmssmartedit depending mode object resources returns root service test"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsDragAndDropServiceModule",
      "shortName": "cmsDragAndDropServiceModule",
      "type": "overview",
      "moduleName": "cmsDragAndDropServiceModule",
      "shortDescription": "The cmsDragAndDropServiceModule",
      "keywords": "cms cmsdraganddropservicemodule cmssmartedit drag drop experience operations overview rich service tailored"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsDragAndDropServiceModule.object:DRAG_AND_DROP_EVENTS",
      "shortName": "DRAG_AND_DROP_EVENTS",
      "type": "object",
      "moduleName": "cmsDragAndDropServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "angular cms cmsdraganddropservicemodule cmssmartedit constant constants drag drag_started drag_stopped drop event events executed identifying injectable object ondragleave ondragover property starts stops triggered"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsDragAndDropServiceModule.service:cmsDragAndDropService",
      "shortName": "cmsDragAndDropService",
      "type": "service",
      "moduleName": "cmsDragAndDropServiceModule",
      "shortDescription": "This service provides a rich drag and drop experience tailored for CMS operations.",
      "keywords": "applies apply cms cmsdraganddropservicemodule cmssmartedit current dom drag draggable drop droppable element execute executed experience instance method operation operations register registers removed rich service smartedit start tailored time unregister unregisters update updates user"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsItemsRestServiceModule",
      "shortName": "cmsItemsRestServiceModule",
      "type": "overview",
      "moduleName": "cmsItemsRestServiceModule",
      "shortDescription": "The cmsitemsRestServiceModule provides a service to CRUD operations on CMS Items API.",
      "keywords": "api cms cmsitemsrestservicemodule cmssmartedit crud items operations overview service"
    },
    {
      "section": "cmssmartedit",
      "id": "cmsitemsRestServiceModule.cmsitemsRestService",
      "shortName": "cmsitemsRestServiceModule.cmsitemsRestService",
      "type": "service",
      "moduleName": "cmsitemsRestServiceModule",
      "shortDescription": "Service to deal with CMS Items related CRUD operations.",
      "keywords": "additional api backend call catalog catalogid catalogversion cms cmsitem cmsitemsrestservice cmsitemsrestservicemodule cmsitemuuid cmssmartedit colon combination comma context create crud current currentpage deal delete empty errors exact fails fetch field fields filtering getbyid identifier item items itemsearchparams list making mask matches method number object operations paged pagesize pairs parameter params performed promise providing query queryparams remove representing request resolves rest result returns search separated serach service successful typecode unique update updated uuid uuids version"
    },
    {
      "section": "cmssmartedit",
      "id": "componentEditingFacadeModule",
      "shortName": "componentEditingFacadeModule",
      "type": "overview",
      "moduleName": "componentEditingFacadeModule",
      "shortDescription": "The componentEditingFacadeModule",
      "keywords": "adding allow cmssmartedit componenteditingfacademodule components methods overview removing service"
    },
    {
      "section": "cmssmartedit",
      "id": "componentEditingFacadeModule.service:componentEditingFacade",
      "shortName": "componentEditingFacade",
      "type": "service",
      "moduleName": "componentEditingFacadeModule",
      "shortDescription": "This service provides methods that allow adding or removing components in the page.",
      "keywords": "add addexistingcomponenttoslot adding addnewcomponenttoslot adds alert allow catalog catalogversionuuid cmssmartedit component componenteditingfacademodule componentid components componenttype componentuuid create display draginfo drop edit existing hidden initially located method methods modal movecomponent moves object opens position properties removing restricted service slot slotinfo slots sourceslotid target targetslotid targetslotuuid type uid uuid version"
    },
    {
      "section": "cmssmartedit",
      "id": "componentMenuModule.ComponentService",
      "shortName": "componentMenuModule.ComponentService",
      "type": "service",
      "moduleName": "componentMenuModule",
      "shortDescription": "Service which manages component types and items",
      "keywords": "add addexistingcomponent basic catalog catalogid catalogversion cmssmartedit componencode component componentid componentinfo componentitem componentmenumodule componentpayload components componentservice componenttype content created createnewcomponent current elements existing fetches filter filters form getsupportedcomponenttypesforcurrentpage identified identify including info item items large list load loadcomponentitem loadcomponenttypes loadpagedcomponentitems loadpagedcomponentitemsbycatalogversion manages mask method number pageid pagesize pagination payload position promise provided resolving restricted result retrieve retrieved search service set slot slotid slots string supplied supported system template type types updatecomponent updated values version"
    },
    {
      "section": "cmssmartedit",
      "id": "componentVisibilityAlertServiceInterfaceModule",
      "shortName": "componentVisibilityAlertServiceInterfaceModule",
      "type": "overview",
      "moduleName": "componentVisibilityAlertServiceInterfaceModule",
      "shortDescription": "This module defines the componentVisibilityAlertServiceInterfaceModule Angular component",
      "keywords": "alert angular associated cmscontextualalertmodule cmssmartedit component componentvisibilityalertserviceinterfacemodule contextual defines display hidden module overview restricted service"
    },
    {
      "section": "cmssmartedit",
      "id": "componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface",
      "shortName": "componentVisibilityAlertServiceInterface",
      "type": "service",
      "moduleName": "componentVisibilityAlertServiceInterfaceModule",
      "shortDescription": "The componentVisibilityAlertServiceInterface is used by external modules to check",
      "keywords": "actionablealert alert angular applied boolean catalogversion check checkandalertoncomponentvisibility checks cmsitem cmssmartedit component componentid componenttype componentvisibilityalertserviceinterface componentvisibilityalertserviceinterfacemodule configuration consumed contextual controller custom defines display external hidden json method modified modules object passed rendered restricted restriction service slot slotid specific stating trigger triggering type uuid visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "contextualMenuDropdownServiceModule.contextualMenuDropdownService",
      "shortName": "contextualMenuDropdownServiceModule.contextualMenuDropdownService",
      "type": "overview",
      "moduleName": "contextualMenuDropdownServiceModule.contextualMenuDropdownService",
      "shortDescription": "contextualMenuDropdownService is an internal service that provides methods for interaction between",
      "keywords": "cmssmartedit contextual contextualmenudropdownservice contextualmenudropdownservicemodule drag drop interaction internal menu methods overview service"
    },
    {
      "section": "cmssmartedit",
      "id": "editorEnablerServiceModule",
      "shortName": "editorEnablerServiceModule",
      "type": "overview",
      "moduleName": "editorEnablerServiceModule",
      "shortDescription": "The editorEnablerServiceModule",
      "keywords": "ability admin allows cms cmssmartedit component contextual edit editor editorenablerservicemodule enabler enabling item menu module overview properties providing service smartedit"
    },
    {
      "section": "cmssmartedit",
      "id": "editorEnablerServiceModule.service:editorEnablerService",
      "shortName": "editorEnablerService",
      "type": "service",
      "moduleName": "editorEnablerServiceModule",
      "shortDescription": "Convenience service to attach the open editor modal action to the contextual menu of a given component type, or",
      "keywords": "action angular app attach bannercomponent button cmsparagraphcomponent cmssmartedit component componenttypes contextual convenience corresponding defined edit editor editorenablerservice editorenablerservicemodule enable enableforcomponents enables example item list menu method modal module open platform postfixed regex selection service type types"
    },
    {
      "section": "cmssmartedit",
      "id": "editorModalServiceModule",
      "shortName": "editorModalServiceModule",
      "type": "overview",
      "moduleName": "editorModalServiceModule",
      "shortDescription": "The editorModalServiceModule",
      "keywords": "allows button cancel cmssmartedit cmssmarteditcontainer component content edit editor editormodalservicemodule editortabset fields loaded modal module opening overview populated providing save service type"
    },
    {
      "section": "cmssmartedit",
      "id": "editorModalServiceModule.service:editorModalService",
      "shortName": "editorModalService",
      "type": "service",
      "moduleName": "editorModalServiceModule",
      "shortDescription": "Convenience service to open an editor modal window for a given component type and component ID.",
      "keywords": "$scope add angular app bound button catalog catalogversionuuid clicking closed cmsparagraphcomponent cmssmartedit component componentattributes componentid componenttype container controller convenience created data database default defined delegates details directive editor editormodalservice editormodalservicemodule example function i18n key method modal module ng-click onclick open opening platform position promise proxy resolves returned selected selectedtabtitle service slot smartedit smarteditcomponentid smarteditcomponenttype smarteditcomponentuuid somecontroller tab termsandconditionsparagraph trigger type unique universally uuid version window"
    },
    {
      "section": "cmssmartedit",
      "id": "removeComponentService.removeComponentService",
      "shortName": "removeComponentService.removeComponentService",
      "type": "service",
      "moduleName": "removeComponentService",
      "shortDescription": "Service to remove a component from a slot",
      "keywords": "cmssmartedit component remove removecomponentservice service slot"
    },
    {
      "section": "cmssmartedit",
      "id": "removeComponentServiceInterfaceModule",
      "shortName": "removeComponentServiceInterfaceModule",
      "type": "overview",
      "moduleName": "removeComponentServiceInterfaceModule",
      "shortDescription": "The removeComponentServiceInterfaceModule",
      "keywords": "ability cmssmartedit component overview remove removecomponentserviceinterfacemodule service slot"
    },
    {
      "section": "cmssmartedit",
      "id": "removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface",
      "shortName": "RemoveComponentServiceInterface",
      "type": "service",
      "moduleName": "removeComponentServiceInterfaceModule",
      "shortDescription": "Service interface specifying the contract used to remove a component from a slot.",
      "keywords": "class cmssmartedit component componentid contract extended instantiated interface method remove removecomponent removecomponentserviceinterfacemodule removes serves service slot slotid"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsMod`ule.object:PAGES_LIST_RESOURCE_URI",
      "shortName": "PAGES_LIST_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsMod`ule",
      "shortDescription": "Resource URI of the pages REST service.",
      "keywords": "cmssmartedit object resource resourcelocationsmod rest service ule uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI",
      "shortName": "resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmartedit contextual_pages_resource_uri currently object placeholders replaced resource resourcelocationsmodule rest selected service uri version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmartedit contextual_pages_restrictions_resource_uri currently object placeholders replaced resource resourcelocationsmodule rest restrictions selected service uri version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI",
      "shortName": "CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the content slot type restrictions REST service.",
      "keywords": "cmssmartedit content object resource resourcelocationsmodule rest restrictions service slot type uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG",
      "shortName": "CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog uid placeholder in URLs",
      "keywords": "catalog cmssmartedit constant object placeholder resourcelocationsmodule uid urls"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG_VERSION",
      "shortName": "CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog version placeholder in URLs",
      "keywords": "catalog cmssmartedit constant object placeholder resourcelocationsmodule urls version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:CONTEXT_SITE_ID",
      "shortName": "CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the site uid placeholder in URLs",
      "keywords": "cmssmartedit constant object placeholder resourcelocationsmodule site uid urls"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:GET_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "shortName": "GET_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to retrieve the full synchronization status of page related items",
      "keywords": "cmssmartedit full items object resource resourcelocationsmodule retrieve status synchronization uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:ITEMS_RESOURCE_URI",
      "shortName": "ITEMS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the custom components REST service.",
      "keywords": "cmssmartedit components custom object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigations REST service.",
      "keywords": "cmssmartedit navigations object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigation entry types REST service.",
      "keywords": "cmssmartedit entry navigation object resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_PAGE_PATH",
      "shortName": "NAVIGATION_MANAGEMENT_PAGE_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to the Navigation Management",
      "keywords": "cmssmartedit management navigation object path resourcelocationsmodule"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigations REST service.",
      "keywords": "cmssmartedit navigations object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG",
      "shortName": "PAGE_CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog uid placeholder in URLs",
      "keywords": "catalog cmssmartedit constant current object placeholder resourcelocationsmodule uid urls"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION",
      "shortName": "PAGE_CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog version placeholder in URLs",
      "keywords": "catalog cmssmartedit constant current object placeholder resourcelocationsmodule urls version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID",
      "shortName": "PAGE_CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page site uid placeholder in URLs",
      "keywords": "cmssmartedit constant current object placeholder resourcelocationsmodule site uid urls"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGE_LIST_PATH",
      "shortName": "PAGE_LIST_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the page list",
      "keywords": "cmssmartedit list object path resourcelocationsmodule"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGE_TEMPLATES_URI",
      "shortName": "PAGE_TEMPLATES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page templates REST service",
      "keywords": "cmssmartedit object resource resourcelocationsmodule rest service templates uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI",
      "shortName": "PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages content slot component REST service.",
      "keywords": "cmssmartedit component content object resource resourcelocationsmodule rest service slot uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:PAGES_CONTENT_SLOT_RESOURCE_URI",
      "shortName": "PAGES_CONTENT_SLOT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page content slots REST service",
      "keywords": "cmssmartedit content object resource resourcelocationsmodule rest service slots uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:POST_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "shortName": "POST_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to perform synchronization of page related items",
      "keywords": "cmssmartedit items object perform resource resourcelocationsmodule synchronization uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:STRUCTURES_RESOURCE_URI",
      "shortName": "STRUCTURES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component structures REST service.",
      "keywords": "cmssmartedit component object resource resourcelocationsmodule rest service structures uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:TYPES_RESOURCE_URI",
      "shortName": "TYPES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component types REST service.",
      "keywords": "cmssmartedit component object resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.object:UriContext",
      "shortName": "UriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives.",
      "keywords": "catalog cms cmssmartedit context_catalog context_catalog_version context_site_id directives map object resourcelocationsmodule services site uid version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.PAGE_TYPES_URI",
      "shortName": "resourceLocationsModule.PAGE_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page types REST service.",
      "keywords": "cmssmartedit object page_types_uri resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.PAGEINFO_RESOURCE_URI",
      "shortName": "resourceLocationsModule.PAGEINFO_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page info REST service.",
      "keywords": "cmssmartedit info object pageinfo_resource_uri resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmartedit currently object pages_restrictions_resource_uri placeholders replaced resource resourcelocationsmodule rest restrictions selected service uri version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "shortName": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pageTypes-restrictionTypes relationship REST service.",
      "keywords": "cmssmartedit object pagetypes-restrictiontypes relationship resource resourcelocationsmodule rest restriction_types_uri service uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "shortName": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the restriction types REST service.",
      "keywords": "cmssmartedit object resource resourcelocationsmodule rest restriction restriction_types_uri service types uri"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmartedit currently object placeholders replaced resource resourcelocationsmodule rest restrictions restrictions_resource_uri selected service uri version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmartedit currently object placeholders replaced resource resourcelocationsmodule rest restrictions selected service update_pages_restrictions_resource_uri uri version"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceModule",
      "shortName": "resourceModule",
      "type": "overview",
      "moduleName": "resourceModule",
      "shortDescription": "The resource module provides $resource factories.",
      "keywords": "$resource cmssmartedit factories module overview resource resourcemodule"
    },
    {
      "section": "cmssmartedit",
      "id": "resourceModule.service:itemsResource",
      "shortName": "itemsResource",
      "type": "service",
      "moduleName": "resourceModule",
      "shortDescription": "This service is used to retrieve the $resource factor for retrieving component items.",
      "keywords": "$resource catalog cmssmartedit component components context current currently custom factor getitemresourcebycontext input items method object placeholders providing replacing resource resourcelocationsmodule resourcemodule rest retrieve retrieving returns selected service site uri uricontext version"
    },
    {
      "section": "cmssmartedit",
      "id": "slotRestrictionsServiceModule",
      "shortName": "slotRestrictionsServiceModule",
      "type": "overview",
      "moduleName": "slotRestrictionsServiceModule",
      "shortDescription": "The slotRestrictionsServiceModule",
      "keywords": "allowed caches cmssmartedit component determine forbidden overview restrictions returns service slot slotrestrictionsservicemodule type"
    },
    {
      "section": "cmssmartedit",
      "id": "slotRestrictionsServiceModule.service:slotRestrictionsService",
      "shortName": "slotRestrictionsService",
      "type": "service",
      "moduleName": "slotRestrictionsServiceModule",
      "shortDescription": "This service provides methods that cache and return the restrictions of a slot in a page. This restrictions determine",
      "keywords": "allowed allows applied array boolean cache checked cmssmartedit component componentid components componenttype contained current determine determines dragged draginfo droppable editable flag forbidden getallcomponenttypessupportedonpage getslotrestrictions identified iscomponentallowedinslot issloteditable list method methods object originates promise property provided restrictions retrieve retrieves return service slot slotid slotrestrictionsservicemodule slots smartedit type types verify"
    },
    {
      "section": "cmssmartedit",
      "id": "slotSharedServiceModule.slotSharedService",
      "shortName": "slotSharedServiceModule.slotSharedService",
      "type": "overview",
      "moduleName": "slotSharedServiceModule.slotSharedService",
      "shortDescription": "SlotSharedService provides methods to interact with the backend for shared slot information. ",
      "keywords": "backend based button case checks cmssmartedit disablesharedslot false feature fetches function hidden interact issharedslotdisabled isslotshared method methods overview pageuid property reloadsharedslotmap retrieves returns service sets setsharedslotenablementstatus shared slot slotid slots slotshared slotsharedservice slotsharedservicemodule status true"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityButtonModule",
      "shortName": "slotVisibilityButtonModule",
      "type": "overview",
      "moduleName": "slotVisibilityButtonModule",
      "shortDescription": "The slot visibility button module provides a directive and controller to manage the button within the slot contextual menu ",
      "keywords": "associated button cmssmartedit component contextual controller directive dropdown hidden list manage menu module overview slot slotvisibilitybuttonmodule template visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityButtonModule.controller:slotVisibilityButtonController",
      "shortName": "slotVisibilityButtonController",
      "type": "controller",
      "moduleName": "slotVisibilityButtonModule",
      "shortDescription": "The slot visibility button controller is responsible for enabling and disabling the hidden components button, ",
      "keywords": "$scope button close cmssmartedit component components controller current disabling displaying enabling functions hidden instance list open responsible scope service slot slotvisibilitybuttonmodule slotvisibilityservice visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityButtonModule.directive:slotVisibilityButton",
      "shortName": "slotVisibilityButton",
      "type": "directive",
      "moduleName": "slotVisibilityButtonModule",
      "shortDescription": "The slot visibility button component is used inside the slot contextual menu and provides a button ",
      "keywords": "button cmssmartedit command component components contextual directive displays dropdown expects function hidden image inside leave menu number open parent scope send setremainopen slot slotid slotvisibilitybuttonmodule visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityComponentModule",
      "shortName": "slotVisibilityComponentModule",
      "type": "overview",
      "moduleName": "slotVisibilityComponentModule",
      "shortDescription": "The slot visibility component module provides a directive and controller to display the hidden components of a specified content slot.",
      "keywords": "cmssmartedit component components componentvisibilityalertservicemodule content controller directive display editormodalservicemodule hidden module overview slot slotvisibilitycomponentmodule visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityComponentModule.directive:slotVisibilityComponent",
      "shortName": "slotVisibilityComponent",
      "type": "directive",
      "moduleName": "slotVisibilityComponentModule",
      "shortDescription": "The slot visibility component directive is used to display information about a specified hidden component.",
      "keywords": "binds cmssmartedit component controller directive display hidden receives scope slot slotvisibilitycomponentmodule visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityServiceModule",
      "shortName": "slotVisibilityServiceModule",
      "type": "overview",
      "moduleName": "slotVisibilityServiceModule",
      "shortDescription": "The slot visibility service module provides factories and services to manage all backend calls and loads an internal",
      "keywords": "backend button calls cmssmartedit component data factories internal loads manage module overview service services slot slotvisibilityservicemodule structure visibility"
    },
    {
      "section": "cmssmartedit",
      "id": "slotVisibilityServiceModule.service:SlotVisibilityService",
      "shortName": "SlotVisibilityService",
      "type": "service",
      "moduleName": "slotVisibilityServiceModule",
      "shortDescription": "The SlotVisibilityService class provides methods to interact with the backend.",
      "keywords": "array backend based cache called class cmsitemsrestservice cmssmartedit component componenthandlerservice components componentuuid componnet content context current definition exists gethiddencomponents getslotsforcomponent hidden ids instance instantiated interact list method methods modified pagescontentslotscomponents pagescontentslotscomponentsresource promise provided re-evalated reloads reloadslotinfo resolves returns service slot slotid slots slotvisibilityservice slotvisibilityservicemodule uuid uuids"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationConstantsModule",
      "shortName": "synchronizationConstantsModule",
      "type": "overview",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Contains various constants used by the synchronization modules.",
      "keywords": "cmssmartedit constants modules object overview synchronization synchronization_polling synchronization_statuses synchronizationconstantsmodule"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING",
      "shortName": "SYNCHRONIZATION_POLLING",
      "type": "object",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Constant containing polling related values",
      "keywords": "cmssmartedit constant event fast_fetch fast_polling_time fetch fetch_sync_status_once fetchsyncstatusonce milliseconds object polling slow slow_down slow_polling_time speed speed_up sync syncfastfetch synchronizationconstantsmodule syncpollingslowdown syncpollingspeedup time trigger values"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES",
      "shortName": "SYNCHRONIZATION_STATUSES",
      "type": "object",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Constant containing the different sync statuses",
      "keywords": "cmssmartedit constant in_progress in_sync not_sync object statuses sync sync_failed synchronizationconstantsmodule unavailable"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationPanelModule.component:synchronizationPanel",
      "shortName": "synchronizationPanel",
      "type": "directive",
      "moduleName": "synchronizationPanelModule",
      "shortDescription": "This component reads and performs synchronization for a main object and its dependencies (from a synchronization perspective).",
      "keywords": "abstractpage aggregated backend-configured button callback change cmssmartedit component contentslot customization customize dependencies dependency dependent dependentitemtypesoutofsync directive display enables expected format function getsyncstatus grouping header headertemplateurl html i18nkey identifier identifiers in_progress in_sync initialize internal invoke invoked item itemid items itemtype key lastsynctime list listed lists logical long main manual metadata navigation not_available not_in_sync object optional page1 panel parent path perform performs performsync perspective popover promise reads required restrictions returns row selectall selected selecteddependencies set slot slot1 someuid status statuses sync sync_failed synchronization synchronization-panel synchronizationpanelmodule syncitems template title types unique values"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationServiceModule",
      "shortName": "synchronizationServiceModule",
      "type": "overview",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "The synchronization module contains the service necessary to perform catalog synchronization.",
      "keywords": "api backend calls catalog cmssmartedit module order overview perform service status synchronization synchronizationservice synchronizationservicemodule trigger versions"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationServiceModule.object:CATALOG_SYNC_INTERVAL_IN_MILLISECONDS",
      "shortName": "CATALOG_SYNC_INTERVAL_IN_MILLISECONDS",
      "type": "object",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "This object defines an injectable Angular constant that determines the frequency to update catalog synchronization information. ",
      "keywords": "angular catalog cmssmartedit constant defines determines frequency injectable milliseconds object synchronization synchronizationservicemodule update"
    },
    {
      "section": "cmssmartedit",
      "id": "synchronizationServiceModule.service:synchronizationService",
      "shortName": "synchronizationService",
      "type": "service",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "The synchronization service manages RESTful calls to the synchronization service&#39;s backend API.",
      "keywords": "api auto backend calls catalog catalogid cmssmartedit getcatalogsyncstatus job manages method object restful service source sourcecatalogversion starts status stopautogetsyncdata stops synchronization synchronizationservicemodule synchronize synchronized target targetcatalogversion update updatecatalogsync version versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "actionableAlertModule",
      "shortName": "actionableAlertModule",
      "type": "overview",
      "moduleName": "actionableAlertModule",
      "shortDescription": "This module defines the actionableAlert Angular component and its",
      "keywords": "actionablealert actionablealertmodule alert alertservicemodule angular associated cmssmarteditcontainer component constant controller custom defines description hyperlink label module overview render service structured"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "actionableAlertModule.object:actionableAlertConstants",
      "shortName": "actionableAlertConstants",
      "type": "object",
      "moduleName": "actionableAlertModule",
      "shortDescription": "This object defines injectable Angular constant used by the displayActionableAlert",
      "keywords": "actionablealert actionablealertmodule alert alert_template angular associated cms cmsitem cmssmarteditcontainer constant content contextual defines defining description descriptiondetails displayactionablealert duration html hyperlink hyperlinklabel injectable inserted item label listed lodash map method methods_displayactionablealert milliseconds object parameters passed placeholders property render rendered run-time service substituted template text timeout timeout_duration translated"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "actionableAlertModule.service:actionableAlertService",
      "shortName": "actionableAlertService",
      "type": "service",
      "moduleName": "actionableAlertModule",
      "shortDescription": "The actionableAlertService is used by external modules to render an",
      "keywords": "actionablealert actionablealertmodule actionablealertservice alert alertcontent angular applied cmssmarteditcontainer configuration consumed contextual controller custom defining description displayactionablealert displayed external function hyperlink hyperlinklabel json label method modules object render service specific structured"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "addPageServiceModule",
      "shortName": "addPageServiceModule",
      "type": "overview",
      "moduleName": "addPageServiceModule",
      "shortDescription": "The addPageServiceModule",
      "keywords": "add addpageservicemodule addpagewizardservice cmssmarteditcontainer creation enable functionality modal module open overview service wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "addPageServiceModule.controller:addPageWizardController",
      "shortName": "addPageWizardController",
      "type": "controller",
      "moduleName": "addPageServiceModule",
      "shortDescription": "The add page wizard controller manages the operation of the wizard used to create new pages.",
      "keywords": "add addpageservicemodule cmssmarteditcontainer controller create manages operation wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "addPageServiceModule.service:addPageWizardService",
      "shortName": "addPageWizardService",
      "type": "service",
      "moduleName": "addPageServiceModule",
      "shortDescription": "The add page wizard service allows opening a modal wizard to create a page.",
      "keywords": "add addpageservicemodule allows called canceled closed cmssmarteditcontainer create method modal openaddpagewizard opening opens promise reject resolve service window wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "assetsServiceModule",
      "shortName": "assetsServiceModule",
      "type": "overview",
      "moduleName": "assetsServiceModule",
      "shortDescription": "The assetsServiceModule",
      "keywords": "assets assetsservicemodule cmssmarteditcontainer handle images methods overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogAwareSelectorModule",
      "shortName": "catalogAwareSelectorModule",
      "type": "overview",
      "moduleName": "catalogAwareSelectorModule",
      "shortDescription": "The catalogAwareSelectorModule",
      "keywords": "allows catalog catalogawareselectormodule cmssmarteditcontainer depend directive items overview select users"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogAwareSelectorModule.directive:seCatalogAwareSelector",
      "shortName": "seCatalogAwareSelector",
      "type": "directive",
      "moduleName": "catalogAwareSelectorModule",
      "shortDescription": "A component that allows users to select items from one or more catalogs. This component is catalog aware; the list",
      "keywords": "allows arguments aware call called catalog catalogawareselectormodule catalogitemsfetchstrategy catalogitemtypekey catalogs choose chosen cmssmarteditcontainer component control default defaults defines dependent directive display displayed editable edited false fashion fetchall fetchentities fetchentity fetchpage flag function functions getcatalogs hand identifier item items itemsfetchstrategy itemtemplate key list localized maximum maxnumitems modified number object pagination performed property provide provided read-only reside retrieve retrieved scenarios select selected selection selector specifies strategies template three track treated type url user users version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogInformationServiceModule",
      "shortName": "catalogInformationServiceModule",
      "type": "overview",
      "moduleName": "catalogInformationServiceModule",
      "shortDescription": "The catalogInformationServiceModule contains a service with helper methods used by catalog aware selector components.",
      "keywords": "aware catalog cataloginformationservicemodule cmssmarteditcontainer components helper methods overview selector service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogInformationServiceModule.object:categoriesFetchStrategy",
      "shortName": "categoriesFetchStrategy",
      "type": "object",
      "moduleName": "catalogInformationServiceModule",
      "shortDescription": "This object contains the strategy necessary to display categories in a paged way; it contains a method to retrieve pages of categories and another method to",
      "keywords": "cataloginformationservicemodule categories cmssmarteditcontainer display individual method object paged retrieve strategy work yselect"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogInformationServiceModule.object:productsFetchStrategy",
      "shortName": "productsFetchStrategy",
      "type": "object",
      "moduleName": "catalogInformationServiceModule",
      "shortDescription": "This object contains the strategy necessary to display products in a paged way; it contains a method to retrieve pages of products and another method to",
      "keywords": "cataloginformationservicemodule cmssmarteditcontainer display individual method object paged products retrieve strategy work yselect"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogInformationServiceModule.service:seCatalogInformationService",
      "shortName": "seCatalogInformationService",
      "type": "service",
      "moduleName": "catalogInformationServiceModule",
      "shortDescription": "This service contains helper methods used by catalog aware components.",
      "keywords": "array aware catalog cataloginformationservicemodule catalogs cmssmarteditcontainer components current getproductcatalogsinformation helper method methods product promise resolves retrieves service site"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogVersionRestServiceModule",
      "shortName": "catalogVersionRestServiceModule",
      "type": "overview",
      "moduleName": "catalogVersionRestServiceModule",
      "shortDescription": "The catalogVersionRestServiceModule",
      "keywords": "catalog catalogversionrestservicemodule cms cmssmarteditcontainer endpoint overview rest services version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "catalogVersionRestServiceModule.service:catalogVersionRestService",
      "shortName": "catalogVersionRestService",
      "type": "service",
      "moduleName": "catalogVersionRestServiceModule",
      "shortDescription": "The catalogVersionRestService provides core REST functionality for the CMS catalog version endpoint",
      "keywords": "catalog catalogversionrestservice catalogversionrestservicemodule cloneable cms cmssmarteditcontainer core current empty endpoint fetches field functionality getcloneabletargets json list method object representing resourcelocationsmodule rest service single site target uricontext version versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "categorySelectorModule",
      "shortName": "categorySelectorModule",
      "type": "overview",
      "moduleName": "categorySelectorModule",
      "shortDescription": "The categorySelectorModule contains a directive that allows selecting categories from a catalog.",
      "keywords": "allows catalog categories categoryselectormodule cmssmarteditcontainer directive overview selecting"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "categorySelectorModule.directive:seCategorySelector",
      "shortName": "seCategorySelector",
      "type": "directive",
      "moduleName": "categorySelectorModule",
      "shortDescription": "A component that allows users to select categories from one or more catalogs. This component is catalog aware; the list of categories displayed is dependent on",
      "keywords": "allows array aware catalog catalogs categories categoryselectormodule cmssmarteditcontainer component dependent directive displayed editable flag hold identifier key list model modified object product property qualifier read-only select selected specifies store stored track type uids user users version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "categorySelectorModule.object:CATEGORY_TEMPLATE_FOR_SELECTOR",
      "shortName": "CATEGORY_TEMPLATE_FOR_SELECTOR",
      "type": "object",
      "moduleName": "categorySelectorModule",
      "shortDescription": "Constant that specifies the URL of the template used to display a category.",
      "keywords": "category categoryselectormodule cmssmarteditcontainer constant display object specifies template url"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "clonePageAlertServiceModule",
      "shortName": "clonePageAlertServiceModule",
      "type": "overview",
      "moduleName": "clonePageAlertServiceModule",
      "shortDescription": "This module defines the clonePageAlertService Angular service used",
      "keywords": "actionable alert allowing angular catalog cloned clonepagealertservice clonepagealertservicemodule cmssmarteditcontainer context defines display displayed hyperlink module multi-country navigate newly overview service user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "clonePageAlertServiceModule.service:clonePageAlertService",
      "shortName": "clonePageAlertService",
      "type": "service",
      "moduleName": "clonePageAlertServiceModule",
      "shortDescription": "The clonePageAlertService is used by external modules to display of an",
      "keywords": "actionable actionablealertservice alert allowing anytime catalog cloned clonedpageinfo clonepagealertservice clonepagealertservicemodule cmssmarteditcontainer display displayactionablealert displayclonepagealert displayed external hyperlink json method modules navigate newly object service triggering uid user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "clonePageWizardServiceModule",
      "shortName": "clonePageWizardServiceModule",
      "type": "overview",
      "moduleName": "clonePageWizardServiceModule",
      "shortDescription": "The clonePageWizardServiceModule",
      "keywords": "add clone clonepagewizardservice clonepagewizardservicemodule cloning cmssmarteditcontainer enable functionality modal module open overview service wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "clonePageWizardServiceModule.controller:clonePageWizardController",
      "shortName": "clonePageWizardController",
      "type": "controller",
      "moduleName": "clonePageWizardServiceModule",
      "shortDescription": "The clone page wizard controller manages the operation of the wizard used to create new pages.",
      "keywords": "clone clonepagewizardservicemodule cmssmarteditcontainer controller create manages operation wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "clonePageWizardServiceModule.service:clonePageWizardService",
      "shortName": "clonePageWizardService",
      "type": "service",
      "moduleName": "clonePageWizardServiceModule",
      "shortDescription": "The clone page wizard service allows opening a modal wizard to clone a page.",
      "keywords": "allows called canceled clone clonepagewizardservicemodule closed cmssmarteditcontainer existing list method modal object openclonepagewizard opened opening opens pagedata promise reject resolve service window wizard"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "cmsConstantsModule.service:assetsService",
      "shortName": "assetsService",
      "type": "object",
      "moduleName": "cmsConstantsModule",
      "shortDescription": "returns the assets resources root depending whether or not we are in test mode",
      "keywords": "assets cmsconstantsmodule cmssmarteditcontainer depending mode object resources returns root service test"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "cmsItemsRestServiceModule",
      "shortName": "cmsItemsRestServiceModule",
      "type": "overview",
      "moduleName": "cmsItemsRestServiceModule",
      "shortDescription": "The cmsitemsRestServiceModule provides a service to CRUD operations on CMS Items API.",
      "keywords": "api cms cmsitemsrestservicemodule cmssmarteditcontainer crud items operations overview service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "cmsitemsRestServiceModule.cmsitemsRestService",
      "shortName": "cmsitemsRestServiceModule.cmsitemsRestService",
      "type": "service",
      "moduleName": "cmsitemsRestServiceModule",
      "shortDescription": "Service to deal with CMS Items related CRUD operations.",
      "keywords": "additional api backend call catalog catalogid catalogversion cms cmsitem cmsitemsrestservice cmsitemsrestservicemodule cmsitemuuid cmssmarteditcontainer colon combination comma context create crud current currentpage deal delete empty errors exact fails fetch field fields filtering getbyid identifier item items itemsearchparams list making mask matches method number object operations paged pagesize pairs parameter params performed promise providing query queryparams remove representing request resolves rest result returns search separated serach service successful typecode unique update updated uuid uuids version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentCloneInfoFormModule",
      "shortName": "componentCloneInfoFormModule",
      "type": "overview",
      "moduleName": "componentCloneInfoFormModule",
      "shortDescription": "#componentCloneInfoFormModule",
      "keywords": "clone cmssmarteditcontainer componentcloneinfoformmodule editor fields form generic module overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentCloneInfoFormModule.directive:componentCloneInfoForm",
      "shortName": "componentCloneInfoForm",
      "type": "directive",
      "moduleName": "componentCloneInfoFormModule",
      "shortDescription": "Component for the clone page info form",
      "keywords": "catalog catalogid catalogversion clone cloned cmssmarteditcontainer component component-clone-info-form componentcloneinfoformmodule content context current defined determine directive editor fields form function generic info isdirty isvalid model modified outer pagetemplate pagetypecode passed pristine property reset reseting returning saving scope selected site siteid siteuid structure submit target targetcatalogversion typecode uri uricontext valid version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentCloneOptionFormModule",
      "shortName": "componentCloneOptionFormModule",
      "type": "overview",
      "moduleName": "componentCloneOptionFormModule",
      "shortDescription": "#componentCloneOptionFormModule",
      "keywords": "child choosing clone cmssmarteditcontainer componentcloneoptionformmodule componentcloneoptionmodule components copying module options overview referencing selection"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentCloneOptionModule.directive:componentCloneOptionForm",
      "shortName": "componentCloneOptionForm",
      "type": "directive",
      "moduleName": "componentCloneOptionModule",
      "shortDescription": "Component for selecting the clone page options for child components in content slots.",
      "keywords": "call child clone cmssmarteditcontainer component component-clone-option-form componentcloneoptionmodule components content directive existing function onselectionchange options parent reference selecting selection slots"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentEditorModule.COMPONENT_EDITOR_CHANGED",
      "shortName": "componentEditorModule.COMPONENT_EDITOR_CHANGED",
      "type": "object",
      "moduleName": "componentEditorModule",
      "shortDescription": "Event is triggered whenever the isDirty state changes. The payload is object &#123;tabId, component&#125;.",
      "keywords": "changes cmssmarteditcontainer component component_editor_changed componenteditormodule event isdirty object payload triggered"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentEditorModule.directive:componentEditor",
      "shortName": "componentEditor",
      "type": "directive",
      "moduleName": "componentEditorModule",
      "shortDescription": "Angular component responsible for loading the structure and the content of the tab that invokes this component into the generic-editor directive.",
      "keywords": "acts angular api basic binding bridge button call callback calls cancel canceltab canceltabs clicked cmssmarteditcontainer cmsstructuretype componencode component component-editor componentcode componenteditor componenteditormodule componentid componentinfo componenttype consolidate content contract corresponding created current data delegate described determine directive display displayed editable editor editormodalservice editortabset editortabsetmodule endpoint error execute false field form fulfills generic generic-editor genericeditor holds i18nkey icon identifier identify independent info instructs invoke invokes isdirtytab label labeltext load loading model modified oncancel onreset onsave optional order pageid parameter passed position prefixtext pristine property provided qualifier relevant requires reset resettab resettabs responsible rest save savetab savetabs service set single slot slotid structure structureapi tab tabid tabs tabstructure template text thrown type typecode uneditable unique universally widget"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentEditorModule.service:componentEditorService",
      "shortName": "componentEditorService",
      "type": "service",
      "moduleName": "componentEditorModule",
      "shortDescription": "Service that aggregates calls from multiple tabs of the editorModalService",
      "keywords": "aggregate aggregates api associated association asynchronous basic call called calling calls changes cmssmarteditcontainer collecting compared component componenteditor componenteditormodule componenteditorservice componentid componentinfo components content corresponds created current data depending described destroyinstance directive dirty discard discarded edited editormodalservice editortabset editortabsetmodule event exposes fetch fetchtabscontent fields genericeditor getinstance holds identifier initial initialization instance instances invoke invoked isdirty items load loaded loading manage matching method methods modified multiple order parameter partial passed post ready receives recorded register registered registertab relevant reset responsible returned save savetabdata savetabs scope send service settabdirtystate supplied tab tabid tabs time trigger triggered update wait"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentMenuModule.ComponentService",
      "shortName": "componentMenuModule.ComponentService",
      "type": "service",
      "moduleName": "componentMenuModule",
      "shortDescription": "Service which manages component types and items",
      "keywords": "add addexistingcomponent basic catalog catalogid catalogversion cmssmarteditcontainer componencode component componentid componentinfo componentitem componentmenumodule componentpayload components componentservice componenttype content created createnewcomponent current elements existing fetches filter filters form getsupportedcomponenttypesforcurrentpage identified identify including info item items large list load loadcomponentitem loadcomponenttypes loadpagedcomponentitems loadpagedcomponentitemsbycatalogversion manages mask method number pageid pagesize pagination payload position promise provided resolving restricted result retrieve retrieved search service set slot slotid slots string supplied supported system template type types updatecomponent updated values version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentVisibilityAlertServiceInterfaceModule",
      "shortName": "componentVisibilityAlertServiceInterfaceModule",
      "type": "overview",
      "moduleName": "componentVisibilityAlertServiceInterfaceModule",
      "shortDescription": "This module defines the componentVisibilityAlertServiceInterfaceModule Angular component",
      "keywords": "alert angular associated cmscontextualalertmodule cmssmarteditcontainer component componentvisibilityalertserviceinterfacemodule contextual defines display hidden module overview restricted service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "componentVisibilityAlertServiceInterfaceModule.service:componentVisibilityAlertServiceInterface",
      "shortName": "componentVisibilityAlertServiceInterface",
      "type": "service",
      "moduleName": "componentVisibilityAlertServiceInterfaceModule",
      "shortDescription": "The componentVisibilityAlertServiceInterface is used by external modules to check",
      "keywords": "actionablealert alert angular applied boolean catalogversion check checkandalertoncomponentvisibility checks cmsitem cmssmarteditcontainer component componentid componenttype componentvisibilityalertserviceinterface componentvisibilityalertserviceinterfacemodule configuration consumed contextual controller custom defines display external hidden json method modified modules object passed rendered restricted restriction service slot slotid specific stating trigger triggering type uuid visibility"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "contextAwarePageStructureServiceModule",
      "shortName": "contextAwarePageStructureServiceModule",
      "type": "overview",
      "moduleName": "contextAwarePageStructureServiceModule",
      "shortDescription": "The contextAwarePageStructureServiceModule",
      "keywords": "aware changes cmssmarteditcontainer context contextawarepagestructureservicemodule depending front-end functionality overview structure"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_POST_ORDER",
      "shortName": "PAGE_STRUCTURE_POST_ORDER",
      "type": "object",
      "moduleName": "contextAwarePageStructureServiceModule",
      "shortDescription": "Injectable angular array of strings, that are page structure field qualifiers.",
      "keywords": "angular array based cmssmarteditcontainer contextawarepagestructureservice contextawarepagestructureservicemodule fetches field fields injectable matching object order positioned post-order qualifiers rearrange returned service strings structure"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "contextAwarePageStructureServiceModule.object:PAGE_STRUCTURE_PRE_ORDER",
      "shortName": "PAGE_STRUCTURE_PRE_ORDER",
      "type": "object",
      "moduleName": "contextAwarePageStructureServiceModule",
      "shortDescription": "Injectable angular array of strings, that are page structure field qualifiers.",
      "keywords": "angular array based cmssmarteditcontainer contextawarepagestructureservice contextawarepagestructureservicemodule fetches field fields injectable matching object order positioned pre-order qualifiers rearrange returned service strings structure"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "contextAwarePageStructureServiceModule.service:contextAwarePageStructureService",
      "shortName": "contextAwarePageStructureService",
      "type": "service",
      "moduleName": "contextAwarePageStructureServiceModule",
      "shortDescription": "The contextAwarePageStructureServiceModule is a layer on top of the",
      "keywords": "allows changing cms cmssmarteditcontainer content context contextawarepagestructureservicemodule created creating defined disabled disabling edited editing existing field fields flag front-end getpagestructurefornewpage getpagestructureforpageediting getpagestructureforviewing indicating info isprimary label layer method modifications modified modifying note object order page_structure_post_order pageid pagetypecode primary removed replaced return service structure time top type typestructurerestservice typestructurerestservicemodule variation viewing"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "createPageServiceModule",
      "shortName": "createPageServiceModule",
      "type": "overview",
      "moduleName": "createPageServiceModule",
      "shortDescription": "The createPageServiceModule",
      "keywords": "allows cmssmarteditcontainer create createpageservicemodule creating module overview service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "createPageServiceModule.service:createPageService",
      "shortName": "createPageService",
      "type": "service",
      "moduleName": "createPageServiceModule",
      "shortDescription": "The createPageService allows creating new pages.",
      "keywords": "allows backend based called cmssmarteditcontainer code create createpage createpageservice createpageservicemodule creates creating fields method note object payload promise provide provided resolve resourcelocationsmodule saving service template type typecode uid uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "displayConditionsFacadeModule",
      "shortName": "displayConditionsFacadeModule",
      "type": "overview",
      "moduleName": "displayConditionsFacadeModule",
      "shortDescription": "This module provides a facade module for page display conditions.",
      "keywords": "cmssmarteditcontainer conditions display displayconditionsfacademodule facade module overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "displayConditionsFacadeModule.service:displayConditionsFacade",
      "shortName": "displayConditionsFacade",
      "type": "service",
      "moduleName": "displayConditionsFacadeModule",
      "shortDescription": "Service defined to retrieve information related to the display conditions of a given page: ",
      "keywords": "associated boolean check cmssmarteditcontainer conditions data defined description display displayconditionsfacademodule getprimarypageforvariationpage identifier indicated isprimarypage label method object primary promise resolving retrieve returns service tested type uid variant variation"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "editorTabsetModule",
      "shortName": "editorTabsetModule",
      "type": "overview",
      "moduleName": "editorTabsetModule",
      "shortDescription": "The editor tabset module contains the service and the directives necessary to organize and display editing forms",
      "keywords": "cmssmarteditcontainer directive directives display editing editor editortabset editortabsetmodule editortabsetservice forms list manage module organize organized overview registered service tabs tabset"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "editorTabsetModule.directive:editorTabset",
      "shortName": "editorTabset",
      "type": "directive",
      "moduleName": "editorTabsetModule",
      "shortDescription": "Directive responsible for displaying and organizing editing forms as tabs within a tabset.",
      "keywords": "allows callback called canceltabs changes clean clears cmssmarteditcontainer communication complete component componentid componenttype control corresponding custom data delegate directive directives discard display displaying editing editor editortabsetmodule enables error errors execute exposes forms header individual instructs internal internally isdirty methods modal modifications modified object oncancel onreset onsave operation organizing pass persist pristine represented resettabs responsible return save savetabs smartedit smartedit-editor-tabset tab tabs tabset unable validate ytab"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "editorTabsetModule.service:editorTabsetService",
      "shortName": "editorTabsetService",
      "type": "service",
      "moduleName": "editorTabsetModule",
      "shortDescription": "The Editor Tabset Service keeps track of a list of tabs and their configuration, which includes the id, title,",
      "keywords": "admin allows basic cmssmarteditcontainer configuration content default deletetab determine directive displayed editing editor editortabset editortabsetmodule fragment generic header html includes list listing method note override overriden path previous registered registering registertab removed removes removing service string tab tabid tabs tabset tabtemplateurl tabtitle template title track unique url"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "entrySearchStrategyInterfaceModule.service:EntrySearchStrategyInterface",
      "shortName": "EntrySearchStrategyInterface",
      "type": "service",
      "moduleName": "entrySearchStrategyInterfaceModule",
      "shortDescription": "Interface describing the contract for entry search in navigation node editor and is part of strategy for the search of EditableDropdown widget which is configured by name.",
      "keywords": "appropriate call choices cmssmarteditcontainer components configured contract creation current currentpage describing documentation dropdown editabledropdown editor entry entrysearchstrategyinterfacemodule example fetch filtering find getitem getpage getsearchdropdownproperties getsearchresults handler handlerservice identified identifier interface item items itemtype itemtypeselected key making manage mask method navigation node number object operations paged pagesize parameters perform placeholder point properties render required resourcelocationsmodule rest result return returns search select service smartedit strategy templateurl type uricontext widget"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "genericEditorModalServiceModule",
      "shortName": "genericEditorModalServiceModule",
      "type": "overview",
      "moduleName": "genericEditorModalServiceModule",
      "shortDescription": "The genericEditorModalServiceModule",
      "keywords": "allows button cancel cmssmarteditcontainer editor generic genericeditormodalservicemodule inside modal module open overview populated save service tabs tabset window"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "genericEditorModalServiceModule.service:genericEditorModalService",
      "shortName": "genericEditorModalService",
      "type": "service",
      "moduleName": "genericEditorModalServiceModule",
      "shortDescription": "The Generic Editor Modal Service is used to open an editor modal window that contains a tabset.",
      "keywords": "button callback clicked clicks closed closes cmssmarteditcontainer contract current data directive displayed edited editor editortabsetmodule executed follow function generic genericeditormodalservicemodule html key list localization method modal note object open opens promise resolves returned save savecallback service tab tabs tabset template templateurl title triggered url user window"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "itemManagementModule",
      "shortName": "itemManagementModule",
      "type": "overview",
      "moduleName": "itemManagementModule",
      "shortDescription": "This module contains the itemManager component.",
      "keywords": "cmssmarteditcontainer component itemmanagementmodule itemmanager module overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "itemManagementModule.itemManager",
      "shortName": "itemManagementModule.itemManager",
      "type": "directive",
      "moduleName": "itemManagementModule",
      "shortDescription": "The purpose of this component is handle the logic of displaying the fields and PUT/POST logic",
      "keywords": "add adding call cms cmssmarteditcontainer code component componenttype content contentapi create created creating defined directive displaying edit editing editor existing fetching fields function generic handle invoke isdirty item itemmanagementmodule itemmanager items logic mode non-empty object operations preset promise purpose resourcelocationsmodule returning returns structure structureapi submit submitfunction triggering true type underlying uri uricontext wrapped"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "itemSelectorPanelModule",
      "shortName": "itemSelectorPanelModule",
      "type": "overview",
      "moduleName": "itemSelectorPanelModule",
      "shortDescription": "The itemSelectorPanelModule",
      "keywords": "add catalogs cmssmarteditcontainer directive display items itemselectorpanelmodule module overview panel user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "itemSelectorPanelModule.directive:seItemSelectorPanel",
      "shortName": "seItemSelectorPanel",
      "type": "directive",
      "moduleName": "itemSelectorPanelModule",
      "shortDescription": "Directive used to create a panel where a user can add items from one or more catalogs. Note that this directive is intended to be used within a",
      "keywords": "add array binding call called catalog catalogawareselectormodule catalogitemsfetchstrategy catalogitemtype catalogs change changes choose cmssmarteditcontainer component create designed directive display displayed double elements executed fashion fetchall fetchentities fetchentity fetchpage function functions getcatalogs hand hidepanel intended items itemselectorpanelmodule itemsfetchstrategy itemsselected itemtemplate localization maximum maxnumitems note number object onchange opened pagination panel paneltitle parent passed performed provide purposes retrieve retrieved scenarios se-item-selector-panel secatalogawareselector select selected selection showpanel strategy string template three title top type uids update url user work"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "itemStrategyInterfaceModule.service:ItemTitleStrategyInterface",
      "shortName": "ItemTitleStrategyInterface",
      "type": "service",
      "moduleName": "itemStrategyInterfaceModule",
      "shortDescription": "Interface describing the contract to retrieve an item in navigation editor.",
      "keywords": "api call cmssmarteditcontainer cmswebservices contract describing display editor entry finding getitembyid handler handlerservice identifier interface item itemid itemstrategyinterfacemodule itemtypeselected making method navigation object operations parameters perform resourcelocationsmodule retrieve service title type unique uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "mediaServiceModule.service:mediaService",
      "shortName": "mediaService",
      "type": "service",
      "moduleName": "mediaServiceModule",
      "shortDescription": "Service to deal with media related CRUD operations",
      "keywords": "cmssmarteditcontainer crud deal media mediaservicemodule operations service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationEditorModule.directive:navigationEditor",
      "shortName": "navigationEditor",
      "type": "directive",
      "moduleName": "navigationEditorModule",
      "shortDescription": "Navigation Editor directive used to display navigation editor tree",
      "keywords": "cmssmarteditcontainer crud default directive display editor facility false navigation navigationeditormodule node object operations optional perform readonly resourcelocationsmodule root rootnodeuid tree true uid uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationEditorNodeServiceModule.service:navigationEditorNodeService",
      "shortName": "navigationEditorNodeService",
      "type": "service",
      "moduleName": "navigationEditorNodeServiceModule",
      "shortDescription": "This service updates the navigation node by making REST call to the cmswebservices navigations API.",
      "keywords": "ancestry api array belonging call catalogid catalogversion cmssmarteditcontainer cmswebservices corresponds getnavigationnodeancestry identified includes list making method navigation navigationeditornodeservicemodule navigations node nodes object operations perform queried request resourcelocationsmodule rest returns service site specific treemodule uid updated updatenavigationnode updates uricontext uriparams"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationEntryItemServiceModule.service:navigationEntryItemService",
      "shortName": "navigationEntryItemService",
      "type": "service",
      "moduleName": "navigationEntryItemServiceModule",
      "shortDescription": "This service is used to retrieve component items by making a REST call to the items API.",
      "keywords": "api assigns associated based calculated call catalogid catalogversion cmssmarteditcontainer component default entries false finalizenavigationentries form generated item items list making method navigationentryitemservicemodule node object property respective rest retrieve service set setname site specific strategies super title titles true type uid uriparams"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorCreateEntryModule.service:cmsNavigationEntryDropdownPopulator",
      "shortName": "cmsNavigationEntryDropdownPopulator",
      "type": "service",
      "moduleName": "navigationNodeEditorCreateEntryModule",
      "shortDescription": "implementation of DropdownPopulatorInterface defined in smartedit",
      "keywords": "attribute cmssmarteditcontainer cmsstructuretype defined dropdownpopulatorinterface editabledropdown implementation navigationnodeeditorcreateentrymodule options service smartedit"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorEntryListModule",
      "shortName": "navigationNodeEditorEntryListModule",
      "type": "overview",
      "moduleName": "navigationNodeEditorEntryListModule",
      "shortDescription": "The navigation node editor entry list module provides a directive and controller to manage the navigation node entry list.     ",
      "keywords": "cmssmarteditcontainer controller directive editor entry list manage module navigation navigationnodeeditorentrylistmodule node overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorEntryListModule.controller:navigationNodeEditorEntryListController",
      "shortName": "navigationNodeEditorEntryListController",
      "type": "controller",
      "moduleName": "navigationNodeEditorEntryListModule",
      "shortDescription": "The navigation node editor entry list controller is responsible for loading the entry list for a specific node, ",
      "keywords": "cmssmarteditcontainer controller editor enabling entry list loading modification navigation navigationnodeeditorentrylistmodule node position removing responsible specific updating"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorEntryListModule.directive:navigationNodeEditorEntryList",
      "shortName": "navigationNodeEditorEntryList",
      "type": "directive",
      "moduleName": "navigationNodeEditorEntryListModule",
      "shortDescription": "The navigation node editor entry list directive is used inside the navigation node editor directive, and displays ",
      "keywords": "actions cmssmarteditcontainer delete directive displays edit edited editor entries entry expects inside list move navigation navigationnodeeditorentrylistmodule navigationnodeentrydata node object parent passes reflects scope"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorModalServiceModule",
      "shortName": "navigationNodeEditorModalServiceModule",
      "type": "overview",
      "moduleName": "navigationNodeEditorModalServiceModule",
      "shortDescription": "The navigationNodeEditorModalServiceModule",
      "keywords": "allows button cancel cmssmarteditcontainer content edit editor editortabset entry fields loaded modal module navigation navigationnodeeditormodalservicemodule node opening overview populated providing save service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorModalServiceModule.service:navigationNodeEditorModalService",
      "shortName": "navigationNodeEditorModalService",
      "type": "service",
      "moduleName": "navigationNodeEditorModalServiceModule",
      "shortDescription": "Convenience service to open an editor modal window for a given navigation node&#39;s data.",
      "keywords": "associated button cancel closed cmssmarteditcontainer content convenience data defined directive editor editortabset editortabsetmodule format genericeditormodalservice initialized method modal navigation navigationnodeeditormodalservicemodule node object open operations paragraph parameters perform platform promise resolves resourcelocationsmodule returned save service title uricontext window wired"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorModule",
      "shortName": "navigationNodeEditorModule",
      "type": "overview",
      "moduleName": "navigationNodeEditorModule",
      "shortDescription": "The navigation node editor module provides a directive and controller to manage the navigation node selected on the parent&#39;s scope.     ",
      "keywords": "cmssmarteditcontainer controller directive editor manage module navigation navigationnodeeditormodule node overview parent scope selected"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorModule.controller:navigationNodeEditorController",
      "shortName": "navigationNodeEditorController",
      "type": "controller",
      "moduleName": "navigationNodeEditorModule",
      "shortDescription": "The navigation node editor controller is responsible for initializing the shared data that will be managed by other ",
      "keywords": "cmssmarteditcontainer controller data directives editor initializing managed navigation navigationnodeeditormodule nested node responsible shared"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEditorModule.directive:navigationNodeEditor",
      "shortName": "navigationNodeEditor",
      "type": "directive",
      "moduleName": "navigationNodeEditorModule",
      "shortDescription": "The navigation node editor directive is used to edit the navigation node, and displays essentially the entry list directive, ",
      "keywords": "attribute cmssmarteditcontainer creation directive displays edit edited editor entry essentially expects list navigation navigationnodeeditormodule node object parent passes reflects"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "navigationNodeEntryTypesServiceModule.service:navigationNodeEntryTypesService",
      "shortName": "navigationNodeEntryTypesService",
      "type": "service",
      "moduleName": "navigationNodeEntryTypesServiceModule",
      "shortDescription": "This service manages navigation node entry types data by making REST call to the cmswebservices navigationentrytypes API.",
      "keywords": "api call cmssmarteditcontainer cmswebservices data entry getnavigationnodeentrytypes making manages method navigation navigationentrytypes navigationnodeentrytypesservicemodule node rest returns service supported types"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "newPageDisplayConditionModule",
      "shortName": "newPageDisplayConditionModule",
      "type": "overview",
      "moduleName": "newPageDisplayConditionModule",
      "shortDescription": "#newPageDisplayConditionModule",
      "keywords": "cmssmarteditcontainer component directive module newpagedisplaycondition newpagedisplayconditionmodule overview"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "newPageDisplayConditionModule.directive:newPageDisplayCondition",
      "shortName": "newPageDisplayCondition",
      "type": "directive",
      "moduleName": "newPageDisplayConditionModule",
      "shortDescription": "Component for selecting the page condition that can be applied to a new page.",
      "keywords": "applied associated binding catalog catalogid catalogversion change cmssmarteditcontainer component condition conditions context default determine directive display executed exists function initialconditionselected initialprimarypageselected label load new-page-display-condition newpagedisplayconditionmodule newpagedisplayconditionresult object optional output outputs pagetypecode parameter params potential primary provide result resultfn selected selecting single site siteuid string takes target targetcatalogversion time type typecode uri uricontext variation version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "newPageDisplayConditionModule.object:newPageDisplayConditionResult",
      "shortName": "newPageDisplayConditionResult",
      "type": "object",
      "moduleName": "newPageDisplayConditionModule",
      "shortDescription": "The (optional) output of the",
      "keywords": "chosen cmssmarteditcontainer component condition directive display false isprimary newpagedisplaycondition newpagedisplayconditionmodule object output primary primarypage representing true variant"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageDisplayConditionsServiceModule",
      "shortName": "pageDisplayConditionsServiceModule",
      "type": "overview",
      "moduleName": "pageDisplayConditionsServiceModule",
      "shortDescription": "The pageDisplayConditionsServiceModule",
      "keywords": "cmssmarteditcontainer conditions display overview pagedisplayconditionsservicemodule services working"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageDisplayConditionsServiceModule.object:CONDITION",
      "shortName": "CONDITION",
      "type": "object",
      "moduleName": "pageDisplayConditionsServiceModule",
      "shortDescription": "An object representing a page display condition",
      "keywords": "cmssmarteditcontainer condition description display isprimary key label localized object pagedisplayconditionsservicemodule render representing structure webpage"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageDisplayConditionsServiceModule.service:pageDisplayConditionsService",
      "shortName": "pageDisplayConditionsService",
      "type": "service",
      "moduleName": "pageDisplayConditionsServiceModule",
      "shortDescription": "The pageDisplayConditionsService provides an abstraction layer for the business logic of",
      "keywords": "abstraction array business cmssmarteditcontainer conditions create display getnewpageconditions layer logic method object pagedisplayconditionsservice pagedisplayconditionsservicemodule pagetype pagetypecode potential primary resourcelocationsmodule service typecode uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageEditorModalServiceModule",
      "shortName": "pageEditorModalServiceModule",
      "type": "overview",
      "moduleName": "pageEditorModalServiceModule",
      "shortDescription": "The pageEditorModalServiceModule",
      "keywords": "allows button cancel cmssmarteditcontainer content edit editor editortabset fields loaded modal module opening overview pageeditormodalservicemodule populated providing save service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageEditorModalServiceModule.service:pageEditorModalService",
      "shortName": "pageEditorModalService",
      "type": "service",
      "moduleName": "pageEditorModalServiceModule",
      "shortDescription": "Convenience service to open an editor modal window for a given page&#39;s data.",
      "keywords": "associated button cancel closed cmssmarteditcontainer content convenience data defined directive editor editortabset editortabsetmodule format genericeditormodalservice initialized method modal open pageeditormodalservicemodule paragraph platform promise resolves returned save service title window wired"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageFacadeModule",
      "shortName": "pageFacadeModule",
      "type": "overview",
      "moduleName": "pageFacadeModule",
      "shortDescription": "This module provides a facade module for pages",
      "keywords": "cmssmarteditcontainer facade module overview pagefacademodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageFacadeModule.service:pageFacade",
      "shortName": "pageFacade",
      "type": "service",
      "moduleName": "pageFacadeModule",
      "shortDescription": "A facade that exposes only the business logic necessary for features that need to work with pages",
      "keywords": "backend based boolean builds business catalog catalogid catalogversion cms cmsitemsrestservice cmssmarteditcontainer contentpage contentpagewithlabelexists context create createpage creates determines determining errors exists experience exposes facade fails features item label logic method object pagefacademodule promise representing request resolves resolving resourcelocationsmodule retrievepageuricontext retrieves returns search service successful uri uricontext version work"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoContainerModule",
      "shortName": "pageInfoContainerModule",
      "type": "overview",
      "moduleName": "pageInfoContainerModule",
      "shortDescription": "This module contains pageInfoContainer component.",
      "keywords": "cmssmarteditcontainer component module overview pageinfocontainer pageinfocontainermodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoContainerModule.pageInfoContainer",
      "shortName": "pageInfoContainerModule.pageInfoContainer",
      "type": "directive",
      "moduleName": "pageInfoContainerModule",
      "shortDescription": "Directive that can render current storefront page&#39;s information and provides a callback function triggered on opening the editor.",
      "keywords": "callback cmssmarteditcontainer current directive editor function opening pageinfocontainer pageinfocontainermodule render storefront triggered"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoDetailsModule",
      "shortName": "pageInfoDetailsModule",
      "type": "overview",
      "moduleName": "pageInfoDetailsModule",
      "shortDescription": "This module contains pageInfoDetails component.",
      "keywords": "cmssmarteditcontainer component module overview pageinfodetails pageinfodetailsmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoDetailsModule.pageInfoDetails",
      "shortName": "pageInfoDetailsModule.pageInfoDetails",
      "type": "directive",
      "moduleName": "pageInfoDetailsModule",
      "shortDescription": "Directive that can render page information, using an underlying generic editor. Additionally, provides an edit button",
      "keywords": "additionally button callback click clicking cmssmarteditcontainer code content directive edit editor fields generic oneditclick pagecontent pageinfodetails pageinfodetailsmodule pagestructure pagetypecode pageuid parameterized render structure tied triggered type uid underlying"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoHeaderModule",
      "shortName": "pageInfoHeaderModule",
      "type": "overview",
      "moduleName": "pageInfoHeaderModule",
      "shortDescription": "This module contains pageInfoHeader component.",
      "keywords": "cmssmarteditcontainer component module overview pageinfoheader pageinfoheadermodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoHeaderModule.pageInfoHeader",
      "shortName": "pageInfoHeaderModule.pageInfoHeader",
      "type": "directive",
      "moduleName": "pageInfoHeaderModule",
      "shortDescription": "Directive that can render page template and type code.",
      "keywords": "cmssmarteditcontainer code directive pageinfoheader pageinfoheadermodule pagetemplate pagetypecode render template type"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoMenuModule",
      "shortName": "pageInfoMenuModule",
      "type": "overview",
      "moduleName": "pageInfoMenuModule",
      "shortDescription": "The page info menu module contains the directive and controller necessary to view the page information menu from the white ribbon..",
      "keywords": "add cmssmarteditcontainer controller directive info menu module overview pageinfomenumodule pageinfomenutoolbaritem ribbon toolbar view white"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageInfoMenuModule.directive:pageInfoMenuToolbarItem",
      "shortName": "pageInfoMenuToolbarItem",
      "type": "directive",
      "moduleName": "pageInfoMenuModule",
      "shortDescription": "Component responsible for displaying the current page&#39;s meta data.",
      "keywords": "access allows cmssmarteditcontainer component current data directive displaying editor meta modal page-info-menu-toolbar-item pageinfomenumodule responsible"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListLinkModule",
      "shortName": "pageListLinkModule",
      "type": "overview",
      "moduleName": "pageListLinkModule",
      "shortDescription": "The pageListLinkModule",
      "keywords": "catalogue cmssmarteditcontainer directive display link list overview pagelistlinkmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListLinkModule.directive:pageListLink",
      "shortName": "pageListLink",
      "type": "directive",
      "moduleName": "pageListLinkModule",
      "shortDescription": "Directive that displays a link to the list of pages of a catalogue. It can only be used if catalog.catalogVersions is in the current scope.",
      "keywords": "associated boolean catalog catalogue catalogversion catalogversions cmssmarteditcontainer current directive displays link list object pagelistlinkmodule provided representing scope site siteid string version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListModule",
      "shortName": "pageListModule",
      "type": "overview",
      "moduleName": "pageListModule",
      "shortDescription": "The page list module contains the controller associated to the page list view.",
      "keywords": "allows api associated backend call catalog cmssmarteditcontainer controller displays list module order overview pagelistmodule pagelistservice pagelistservicemodule search service sort specific user view"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListModule.controller:pageListController",
      "shortName": "pageListController",
      "type": "controller",
      "moduleName": "pageListModule",
      "shortDescription": "The page list controller fetches pages for a specified catalog using the pageListService",
      "keywords": "catalog cmssmarteditcontainer controller fetches list pagelistmodule pagelistservice pagelistservicemodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListServiceModule",
      "shortName": "pageListServiceModule",
      "type": "overview",
      "moduleName": "pageListServiceModule",
      "shortDescription": "The pageListServiceModule",
      "keywords": "catalog cmssmarteditcontainer fetches list module overview pagelistservicemodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListServiceModule.service:pageListService",
      "shortName": "pageListService",
      "type": "service",
      "moduleName": "pageListServiceModule",
      "shortDescription": "The Page List Service fetches pages for a specified catalog using REST calls to the cmswebservices pages API.",
      "keywords": "api array calls catalog catalogid catalogversion cmssmarteditcontainer cmswebservices corresponds creationtime descriptor descriptors fetched fetches getpagebyid getpagelistforcatalog list matches method modifiedtime object pagelistservicemodule pageuid pk properties provided resourcelocationsmodule rest retrieved service site template title typecode uid uricontext uriparams"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageListSyncIconModule.directive:pageListSyncIcon",
      "shortName": "pageListSyncIcon",
      "type": "directive",
      "moduleName": "pageListSyncIconModule",
      "shortDescription": "The Page Synchronization Icon component is used to display the icon that describes the synchronization status of a page.",
      "keywords": "cmssmarteditcontainer component describes directive display displayed icon identifier pageid pagelistsynciconmodule status sync-icon synchronization synchronzation"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsCriteriaModule",
      "shortName": "pageRestrictionsCriteriaModule",
      "type": "overview",
      "moduleName": "pageRestrictionsCriteriaModule",
      "shortDescription": "This module defines the pageRestrictionsCriteriaService service used to consolidate business logic for restriction criteria.",
      "keywords": "business cmssmarteditcontainer consolidate criteria defines logic module overview pagerestrictionscriteriamodule pagerestrictionscriteriaservice restriction service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsCriteriaModule.service:pageRestrictionsCriteriaService",
      "shortName": "pageRestrictionsCriteriaService",
      "type": "service",
      "moduleName": "pageRestrictionsCriteriaModule",
      "shortDescription": "A service for working with restriction criteria.",
      "keywords": "applied array boolean cmssmarteditcontainer criteria determine getmatchcriterialabel getrestrictioncriteriaoptionfrompage getrestrictioncriteriaoptions i18n key method object onlyonerestrictionmustapply options pagerestrictionscriteriamodule restriction service working"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsModule",
      "shortName": "pageRestrictionsModule",
      "type": "overview",
      "moduleName": "pageRestrictionsModule",
      "shortDescription": "This module defines the pageRestrictionsFacade facade module for page restrictions.",
      "keywords": "cmssmarteditcontainer defines facade factory module overview pagerestrictionscriteriamodule pagerestrictionsfacade pagerestrictionsmodule pagerestrictionsservicemodule restrictions restrictiontypesservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsModule.factory:pageRestrictionsFacade",
      "shortName": "pageRestrictionsFacade",
      "type": "service",
      "moduleName": "pageRestrictionsModule",
      "shortDescription": "A facade that exposes only the business logic necessary for features that need to work with page restrictions.",
      "keywords": "business cmssmarteditcontainer exposes facade factory features logic pagerestrictionscriteriaservice pagerestrictionsmodule pagerestrictionsservice restrictions restrictiontypesservice service work"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsRestServiceModule",
      "shortName": "pageRestrictionsRestServiceModule",
      "type": "overview",
      "moduleName": "pageRestrictionsRestServiceModule",
      "shortDescription": "This module defines the pageRestrictionsRestService REST service for page restrictions API.",
      "keywords": "api cmssmarteditcontainer defines module overview pagerestrictionsrestservice pagerestrictionsrestservicemodule rest restrictions restservicefactorymodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsRestServiceModule.service:pageRestrictionsRestService",
      "shortName": "pageRestrictionsRestService",
      "type": "service",
      "moduleName": "pageRestrictionsRestServiceModule",
      "shortDescription": "Service that handles REST requests for the pagesRestrictions CMS API endpoint.",
      "keywords": "api array catalog cataloguid catalogversionuid cms cmssmarteditcontainer contextual_pages_restrictions_resource_uri endpoint fetch getpagesrestrictionsforcatalogversion getpagesrestrictionsforpageid handles identifier method object pageid pagerestriction pagerestrictionlist pagerestrictions pagerestrictionsrestservicemodule pages-restrictions pages_restrictions_resource_uri pagesrestrictions payload relation requests rest restservicefactory service site siteuid unique update update_pages_restrictions_resource_uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsServiceModule",
      "shortName": "pageRestrictionsServiceModule",
      "type": "overview",
      "moduleName": "pageRestrictionsServiceModule",
      "shortDescription": "This module defines the pageRestrictionsService service used to consolidate business logic for SAP Hybris platform CMS restrictions for pages.",
      "keywords": "business cms cmssmarteditcontainer consolidate defines hybris logic module overview pagerestrictionsrestservicemodule pagerestrictionsservice pagerestrictionsservicemodule platform restrictions restrictionsservicemodule restrictiontypesservicemodule sap service ylodashmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageRestrictionsServiceModule.service:pageRestrictionsService",
      "shortName": "pageRestrictionsService",
      "type": "service",
      "moduleName": "pageRestrictionsServiceModule",
      "shortDescription": "Service that concerns business logic tasks related to CMS restrictions for CMS pages.",
      "keywords": "$q applied array business catalog cataloguid catalogversionuid cms cmssmarteditcontainer code concerns creating currently editing fetch getpagerestrictionscountforpageuid getpagerestrictionscountmapforcatalogversion getrestrictionsbypageuid identifier isrestrictiontypesupported list logic map method number pageid pagerestrictionsrestservice pagerestrictionsservicemodule pageuid pageuuid provided replaces restriction restrictions restrictionsarray restrictionsservice restrictiontypecode restrictiontypesservice service site siteuid smartedit supports tasks true type typestructurerestservice uid unique update updated updaterestrictionsbypageuid uuid values version ylodashmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageServiceModule",
      "shortName": "pageServiceModule",
      "type": "overview",
      "moduleName": "pageServiceModule",
      "shortDescription": "The pageServiceModule",
      "keywords": "cms cmssmarteditcontainer dealing objects overview pageservicemodule services"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageServiceModule.service:pageService",
      "shortName": "pageService",
      "type": "service",
      "moduleName": "pageServiceModule",
      "shortDescription": "The pageService provides low level functionality for CMS pages",
      "keywords": "api array backend belonging cms cmssmarteditcontainer code contextual corresponding current currently determines empty exists false fetches filter functionality getcurrentpageinfo getprimarypagesforpagetype ispageprimary item json level list loaded low method object objects pageid pageservice pageservicemodule pagetypecode pageuid payload primary primarypageid promise provided resolves resourcelocationsmodule retrieves service site true type uid undefined updatepagebyid updates uricontext uriparams variation variationpageid"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pagesRestServiceModule",
      "shortName": "pagesRestServiceModule",
      "type": "overview",
      "moduleName": "pagesRestServiceModule",
      "shortDescription": "The pagesRestServiceModule",
      "keywords": "cms cmssmarteditcontainer endpoint overview pagesrestservicemodule rest services"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pagesRestServiceModule.service:pagesRestService",
      "shortName": "pagesRestService",
      "type": "service",
      "moduleName": "pagesRestServiceModule",
      "shortDescription": "The pagesRestService provides core REST functionality for the CMS pages rest endpoint",
      "keywords": "additional anoptionalqueryparamname applied backend catalog catalogid catalogversion cms cmssmarteditcontainer context contextually core defined empty endpoint example exists fetch fetches functionality getbyid json list method object online pagesrestservice pagesrestservicemodule pageuid parameters params paramvalue payload promise representing request resolves resolving resource rest service session shoes side site siteuid supershoes uid update updated updates version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTemplateServiceModule",
      "shortName": "pageTemplateServiceModule",
      "type": "overview",
      "moduleName": "pageTemplateServiceModule",
      "shortDescription": "The pageTemplateServiceModule",
      "keywords": "allows associated cmssmarteditcontainer module overview pagetemplateservicemodule retrieval service template templates type"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTemplateServiceModule.service:pageTemplateService",
      "shortName": "pageTemplateService",
      "type": "service",
      "moduleName": "pageTemplateServiceModule",
      "shortDescription": "This service allows the retrieval of page templates associated to a page type.",
      "keywords": "allows associated called cmssmarteditcontainer getpagetemplatesfortype method object pagetemplateservicemodule pagetype promise provided resolve resourcelocationsmodule retrieval retrieve retrieved retrieves service templates type uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypeServiceModule",
      "shortName": "pageTypeServiceModule",
      "type": "overview",
      "moduleName": "pageTypeServiceModule",
      "shortDescription": "The Page Type Service module provides a service that retrieves all supported page types",
      "keywords": "cmssmarteditcontainer module overview pagetypeservicemodule retrieves service supported type types"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypeServiceModule.pageTypeService",
      "shortName": "pageTypeServiceModule.pageTypeService",
      "type": "service",
      "moduleName": "pageTypeServiceModule",
      "shortDescription": "Service that concerns business logic tasks related to CMS page types in the SAP Hybris platform.",
      "keywords": "array business caches cms cmssmarteditcontainer code concerns configured description descriptor duration getpagetypes hybris identifier iso js language list localized logic map method object objects pagetypeservice pagetypeservicemodule platform retrieves returned returns sap service session structured supported tasks type types unique"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypesRestrictionTypesRestServiceModule",
      "shortName": "pageTypesRestrictionTypesRestServiceModule",
      "type": "overview",
      "moduleName": "pageTypesRestrictionTypesRestServiceModule",
      "shortDescription": "This module defines the pageRestrictionsRestService REST service for pageTypes restrictionTypes API.",
      "keywords": "api cmssmarteditcontainer defines module overview pagerestrictionsrestservice pagerestrictionsrestservicemodule pagetypes pagetypesrestrictiontypesrestservicemodule rest restrictiontypes restservicefactorymodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypesRestrictionTypesRestServiceModule.service:pageTypesRestrictionTypesRestService",
      "shortName": "pageTypesRestrictionTypesRestService",
      "type": "service",
      "moduleName": "pageTypesRestrictionTypesRestServiceModule",
      "shortDescription": "Service that handles REST requests for the pageTypes restrictionTypes CMS API endpoint.",
      "keywords": "api array cms cmssmarteditcontainer endpoint getpagetypesrestrictiontypes handles languageservice method page_types_restriction_types_uri pagetype-restrictiontype pagetypes pagetypesrestrictiontypesrestservicemodule requests rest restrictiontypes restservicefactory service system"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypesRestrictionTypesServiceModule",
      "shortName": "pageTypesRestrictionTypesServiceModule",
      "type": "overview",
      "moduleName": "pageTypesRestrictionTypesServiceModule",
      "shortDescription": "This module defines the pageTypesRestrictionTypesService REST service used to consolidate business logic for SAP Hybris platform CMS",
      "keywords": "business cms cmssmarteditcontainer consolidate defines hybris logic module overview pagetypes-restrictiontypes pagetypesrestrictiontypesrestservicemodule pagetypesrestrictiontypesservice pagetypesrestrictiontypesservicemodule platform relations rest sap service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "pageTypesRestrictionTypesServiceModule.service:pageTypesRestrictionTypesService",
      "shortName": "pageTypesRestrictionTypesService",
      "type": "service",
      "moduleName": "pageTypesRestrictionTypesServiceModule",
      "shortDescription": "Service that concerns business logic tasks related to CMS pageTypes-restrictionTypes relations.",
      "keywords": "applied array business cms cmssmarteditcontainer codes concerns getpagetypesrestrictiontypes getrestrictiontypecodesforpagetype logic method pagetype pagetype-restrictiontype pagetypes-restrictiontypes pagetypesrestrictiontypesrestservice pagetypesrestrictiontypesservicemodule relations restriction service system tasks type"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "productSelectorModule",
      "shortName": "productSelectorModule",
      "type": "overview",
      "moduleName": "productSelectorModule",
      "shortDescription": "The productSelectorModule contains a directive that allows selecting products from a catalog.",
      "keywords": "allows catalog cmssmarteditcontainer directive overview products productselectormodule selecting"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "productSelectorModule.directive:seProductSelector",
      "shortName": "seProductSelector",
      "type": "directive",
      "moduleName": "productSelectorModule",
      "shortDescription": "A component that allows users to select products from one or more catalogs. This component is catalog aware; the list of products displayed is dependent on",
      "keywords": "allows array aware catalog catalogs cmssmarteditcontainer component dependent directive displayed hold identifier key list model object product products productselectormodule property qualifier select selected store stored track type uids user users version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "productSelectorModule.object:PRODUCT_TEMPLATE_FOR_SELECTOR",
      "shortName": "PRODUCT_TEMPLATE_FOR_SELECTOR",
      "type": "object",
      "moduleName": "productSelectorModule",
      "shortDescription": "Constant that specifies the maximum number of products a user can select.",
      "keywords": "cmssmarteditcontainer constant maximum number object products productselectormodule select specifies user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "productSelectorModule.object:PRODUCT_TEMPLATE_FOR_SELECTOR",
      "shortName": "PRODUCT_TEMPLATE_FOR_SELECTOR",
      "type": "object",
      "moduleName": "productSelectorModule",
      "shortDescription": "Constant that specifies the URL of the template used to display a product.",
      "keywords": "cmssmarteditcontainer constant display object product productselectormodule specifies template url"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "removeComponentServiceInterfaceModule",
      "shortName": "removeComponentServiceInterfaceModule",
      "type": "overview",
      "moduleName": "removeComponentServiceInterfaceModule",
      "shortDescription": "The removeComponentServiceInterfaceModule",
      "keywords": "ability cmssmarteditcontainer component overview remove removecomponentserviceinterfacemodule service slot"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "removeComponentServiceInterfaceModule.service:RemoveComponentServiceInterface",
      "shortName": "RemoveComponentServiceInterface",
      "type": "service",
      "moduleName": "removeComponentServiceInterfaceModule",
      "shortDescription": "Service interface specifying the contract used to remove a component from a slot.",
      "keywords": "class cmssmarteditcontainer component componentid contract extended instantiated interface method remove removecomponent removecomponentserviceinterfacemodule removes serves service slot slotid"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsMod`ule.object:PAGES_LIST_RESOURCE_URI",
      "shortName": "PAGES_LIST_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsMod`ule",
      "shortDescription": "Resource URI of the pages REST service.",
      "keywords": "cmssmarteditcontainer object resource resourcelocationsmod rest service ule uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI",
      "shortName": "resourceLocationsModule.CONTEXTUAL_PAGES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmarteditcontainer contextual_pages_resource_uri currently object placeholders replaced resource resourcelocationsmodule rest selected service uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.CONTEXTUAL_PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmarteditcontainer contextual_pages_restrictions_resource_uri currently object placeholders replaced resource resourcelocationsmodule rest restrictions selected service uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI",
      "shortName": "CONTENT_SLOT_TYPE_RESTRICTION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the content slot type restrictions REST service.",
      "keywords": "cmssmarteditcontainer content object resource resourcelocationsmodule rest restrictions service slot type uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG",
      "shortName": "CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog uid placeholder in URLs",
      "keywords": "catalog cmssmarteditcontainer constant object placeholder resourcelocationsmodule uid urls"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG_VERSION",
      "shortName": "CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog version placeholder in URLs",
      "keywords": "catalog cmssmarteditcontainer constant object placeholder resourcelocationsmodule urls version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_SITE_ID",
      "shortName": "CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the site uid placeholder in URLs",
      "keywords": "cmssmarteditcontainer constant object placeholder resourcelocationsmodule site uid urls"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:GET_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "shortName": "GET_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to retrieve the full synchronization status of page related items",
      "keywords": "cmssmarteditcontainer full items object resource resourcelocationsmodule retrieve status synchronization uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:ITEMS_RESOURCE_URI",
      "shortName": "ITEMS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the custom components REST service.",
      "keywords": "cmssmarteditcontainer components custom object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_ENTRIES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigations REST service.",
      "keywords": "cmssmarteditcontainer navigations object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_ENTRY_TYPES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigation entry types REST service.",
      "keywords": "cmssmarteditcontainer entry navigation object resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_PAGE_PATH",
      "shortName": "NAVIGATION_MANAGEMENT_PAGE_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to the Navigation Management",
      "keywords": "cmssmarteditcontainer management navigation object path resourcelocationsmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:NAVIGATION_MANAGEMENT_RESOURCE_URI",
      "shortName": "NAVIGATION_MANAGEMENT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the navigations REST service.",
      "keywords": "cmssmarteditcontainer navigations object resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG",
      "shortName": "PAGE_CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog uid placeholder in URLs",
      "keywords": "catalog cmssmarteditcontainer constant current object placeholder resourcelocationsmodule uid urls"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION",
      "shortName": "PAGE_CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog version placeholder in URLs",
      "keywords": "catalog cmssmarteditcontainer constant current object placeholder resourcelocationsmodule urls version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID",
      "shortName": "PAGE_CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page site uid placeholder in URLs",
      "keywords": "cmssmarteditcontainer constant current object placeholder resourcelocationsmodule site uid urls"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGE_LIST_PATH",
      "shortName": "PAGE_LIST_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the page list",
      "keywords": "cmssmarteditcontainer list object path resourcelocationsmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGE_TEMPLATES_URI",
      "shortName": "PAGE_TEMPLATES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page templates REST service",
      "keywords": "cmssmarteditcontainer object resource resourcelocationsmodule rest service templates uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI",
      "shortName": "PAGES_CONTENT_SLOT_COMPONENT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages content slot component REST service.",
      "keywords": "cmssmarteditcontainer component content object resource resourcelocationsmodule rest service slot uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:PAGES_CONTENT_SLOT_RESOURCE_URI",
      "shortName": "PAGES_CONTENT_SLOT_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page content slots REST service",
      "keywords": "cmssmarteditcontainer content object resource resourcelocationsmodule rest service slots uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:POST_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "shortName": "POST_PAGE_SYNCHRONIZATION_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to perform synchronization of page related items",
      "keywords": "cmssmarteditcontainer items object perform resource resourcelocationsmodule synchronization uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:STRUCTURES_RESOURCE_URI",
      "shortName": "STRUCTURES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component structures REST service.",
      "keywords": "cmssmarteditcontainer component object resource resourcelocationsmodule rest service structures uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:TYPES_RESOURCE_URI",
      "shortName": "TYPES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component types REST service.",
      "keywords": "cmssmarteditcontainer component object resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.object:UriContext",
      "shortName": "UriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives.",
      "keywords": "catalog cms cmssmarteditcontainer context_catalog context_catalog_version context_site_id directives map object resourcelocationsmodule services site uid version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.PAGE_TYPES_URI",
      "shortName": "resourceLocationsModule.PAGE_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page types REST service.",
      "keywords": "cmssmarteditcontainer object page_types_uri resource resourcelocationsmodule rest service types uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.PAGEINFO_RESOURCE_URI",
      "shortName": "resourceLocationsModule.PAGEINFO_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the page info REST service.",
      "keywords": "cmssmarteditcontainer info object pageinfo_resource_uri resource resourcelocationsmodule rest service uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmarteditcontainer currently object pages_restrictions_resource_uri placeholders replaced resource resourcelocationsmodule rest restrictions selected service uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "shortName": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the restriction types REST service.",
      "keywords": "cmssmarteditcontainer object resource resourcelocationsmodule rest restriction restriction_types_uri service types uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "shortName": "resourceLocationsModule.RESTRICTION_TYPES_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pageTypes-restrictionTypes relationship REST service.",
      "keywords": "cmssmarteditcontainer object pagetypes-restrictiontypes relationship resource resourcelocationsmodule rest restriction_types_uri service uri"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmarteditcontainer currently object placeholders replaced resource resourcelocationsmodule rest restrictions restrictions_resource_uri selected service uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI",
      "shortName": "resourceLocationsModule.UPDATE_PAGES_RESTRICTIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the pages restrictions REST service, with placeholders to be replaced by the currently selected catalog version.",
      "keywords": "catalog cmssmarteditcontainer currently object placeholders replaced resource resourcelocationsmodule rest restrictions selected service update_pages_restrictions_resource_uri uri version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceModule",
      "shortName": "resourceModule",
      "type": "overview",
      "moduleName": "resourceModule",
      "shortDescription": "The resource module provides $resource factories.",
      "keywords": "$resource cmssmarteditcontainer factories module overview resource resourcemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "resourceModule.service:itemsResource",
      "shortName": "itemsResource",
      "type": "service",
      "moduleName": "resourceModule",
      "shortDescription": "This service is used to retrieve the $resource factor for retrieving component items.",
      "keywords": "$resource catalog cmssmarteditcontainer component components context current currently custom factor getitemresourcebycontext input items method object placeholders providing replacing resource resourcelocationsmodule resourcemodule rest retrieve retrieving returns selected service site uri uricontext version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionManagementEditModule",
      "shortName": "restrictionManagementEditModule",
      "type": "overview",
      "moduleName": "restrictionManagementEditModule",
      "shortDescription": "This module defines the restrictionManagementEdit component.",
      "keywords": "alertservicemodule cmssmarteditcontainer component defines directive module overview restrictionmanagementedit restrictionmanagementeditmodule restrictionsservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionManagementEditModule.directive:restrictionManagementEdit",
      "shortName": "restrictionManagementEdit",
      "type": "directive",
      "moduleName": "restrictionManagementEditModule",
      "shortDescription": "The restrictionManagementEdit Angular component is designed to be able to edit restrictions.",
      "keywords": "angular cmssmarteditcontainer component defined designed directive dirtiness edit editing function getsupportedrestrictiontypesfn isdirtyfn item list object outer resourcelocationmodule resourcelocationsmodule restriction restrictionmanagementedit restrictionmanagementeditmodule restrictions returning returns scope status string submitfn supported types uricontext validate"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionManagementSelectModule",
      "shortName": "restrictionManagementSelectModule",
      "type": "overview",
      "moduleName": "restrictionManagementSelectModule",
      "shortDescription": "This module defines the restrictionManagementSelect component",
      "keywords": "alertservicemodule cmssmarteditcontainer component defines directive module overview restrictionmanagementselect restrictionmanagementselectmodule restrictionsmodule restrictionsservicemodule yactionablesearchitemmodule yselectmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionManagementSelectModule.directive:restrictionManagementSelect",
      "shortName": "restrictionManagementSelect",
      "type": "directive",
      "moduleName": "restrictionManagementSelectModule",
      "shortDescription": "The restrictionManagementSelect Angular component is designed to be able to create or display restrictions.",
      "keywords": "angular array assumed cmssmarteditcontainer component create defined designed directive dirtiness display edit editing existing existingrestrictions expression function getrestrictiontypesfn getsupportedrestrictiontypesfn isdirtyfn item list object outer provided resourcelocationmodule resourcelocationsmodule restriction restrictionmanagementselect restrictionmanagementselectmodule restrictions returning returns scope selectable status submitfn supported types uricontext validate"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionPickerModule",
      "shortName": "restrictionPickerModule",
      "type": "overview",
      "moduleName": "restrictionPickerModule",
      "shortDescription": "This module defines the restrictionManagement component",
      "keywords": "cmssmarteditcontainer component defines directive itemmanagementmodule module overview recompiledommodule restrictionmanagement restrictionmanagementeditmodule restrictionmanagementselectmodule restrictionpickermodule ylodashmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionPickerModule.directive:restrictionManagement",
      "shortName": "restrictionManagement",
      "type": "directive",
      "moduleName": "restrictionPickerModule",
      "shortDescription": "The restrictionManagement Angular component is designed to be able to create new restrictions, editing existing",
      "keywords": "angular array binding boolean caller cmssmarteditcontainer complete component config constant create created defined depending designed directive dirty displayed edit editing execute existing existingrestrictions function generic identifier indicating internally isdirtyfn mode object picker post processed promise provided represent resolving resourcelocationmodule resourcelocationsmodule restriction restrictionid restrictionmanagement restrictionpickerconfig restrictionpickermodule restrictions return returns search select selectable service submitfn tor trigger true unique uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionPickerModule.service:restrictionPickerConfig",
      "shortName": "restrictionPickerConfig",
      "type": "service",
      "moduleName": "restrictionPickerModule",
      "shortDescription": "The Generic Editor Modal Service is used to open an editor modal window that contains a tabset.",
      "keywords": "array check cmssmarteditcontainer component config created directive edit edited editing editor existing existingrestriction existingrestrictions function generic getconfigforediting getconfigforselecting getrestrictiontypesfn getsupportedrestrictiontypesfn iseditingmode isselectmode isvalidconfig item list lodash method methods_getconfigforediting methods_getconfigforselecting modal mode object open param params proper restriction restrictionmanagement restrictionpickermodule restrictions returns select selectable service supported tabset true types uuid window"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsEditorModule",
      "shortName": "restrictionsEditorModule",
      "type": "overview",
      "moduleName": "restrictionsEditorModule",
      "shortDescription": "This module contains the restrictionsEditor component.",
      "keywords": "cmssmarteditcontainer component module overview restrictionseditor restrictionseditormodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsEditorModule.restrictionsEditor",
      "shortName": "restrictionsEditorModule.restrictionsEditor",
      "type": "directive",
      "moduleName": "restrictionsEditorModule",
      "shortDescription": "The purpose of this directive is to allow the user to manage the restrictions for a given item. The restrictionsEditor has an editable and non-editable mode.",
      "keywords": "$onlyonerestrictionmustapply $restrictions add allow arguments array arry assumed bind boolean callback cancelfn cmssmarteditcontainer controller criteria custom defined determine directive display editable editor enabled exists expression external fetch function getrestrictiontypes getsupportedrestrictiontypes initial initialrestrictions invoker isdirtyfn item list loaded manage mode non-editable object objects onlyonerestrictionmustapply onrestrictionschanged operations passes passing perform promise provide provided purpose remove required resetfn resourcelocationsmodule restriction restrictions restrictionseditor restrictionseditormodule restrictionspicker restrictionstable return returns supported true types update uricontext user uuid values"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsMenuModule.directive:restrictionsMenuToolbarItem",
      "shortName": "restrictionsMenuToolbarItem",
      "type": "directive",
      "moduleName": "restrictionsMenuModule",
      "shortDescription": "Component responsible for displaying the restriction menu.",
      "keywords": "cmssmarteditcontainer component directive displaying menu responsible restriction restrictions-menu-toolbar-item restrictionsmenumodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsModule",
      "shortName": "restrictionsModule",
      "type": "overview",
      "moduleName": "restrictionsModule",
      "shortDescription": "This module defines the restrictionsFacade factory managing all restrictions.",
      "keywords": "cmssmarteditcontainer defines factory managing module overview restrictions restrictionsfacade restrictionsmodule restrictionsservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsModule.restrictionsFacade",
      "shortName": "restrictionsModule.restrictionsFacade",
      "type": "service",
      "moduleName": "restrictionsModule",
      "shortDescription": "A facade that exposes only the business logic necessary for features that need to work with restrictions.",
      "keywords": "business cmssmarteditcontainer exposes facade features logic restrictions restrictionsfacade restrictionsmodule restrictionsservice service work"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsRestServiceModule",
      "shortName": "restrictionsRestServiceModule",
      "type": "overview",
      "moduleName": "restrictionsRestServiceModule",
      "shortDescription": "This module defines the restrictionsRestService REST service for restrictions API.",
      "keywords": "api cmssmarteditcontainer defines functionsmodule module overview rest restrictions restrictionsrestservice restrictionsrestservicemodule restservicefactorymodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsRestServiceModule.service:restrictionsRestService",
      "shortName": "restrictionsRestService",
      "type": "service",
      "moduleName": "restrictionsRestServiceModule",
      "shortDescription": "Service that handles REST requests for the restrictions CMS API endpoint.",
      "keywords": "api array cms cmssmarteditcontainer crud defined endpoint getbyid getcontentapiuri handles identifier languageservice matching method object parameter parameters params passed requests resourcelocationmodule resourcelocationsmodule rest restriction restrictionid restrictions restrictions_resource_uri restrictionsrestservicemodule restservicefactory service site specific system uri uribuilder uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsServiceModule",
      "shortName": "restrictionsServiceModule",
      "type": "overview",
      "moduleName": "restrictionsServiceModule",
      "shortDescription": "This module provides the restrictionsService service used to consolidate business logic for SAP Hybris platform CMS restrictions.",
      "keywords": "business cms cmssmarteditcontainer consolidate hybris logic module overview platform restrictions restrictionsrestservicemodule restrictionsservice restrictionsservicemodule sap service structuresrestservicemodule typestructurerestservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsServiceModule.service:restrictionsService",
      "shortName": "restrictionsService",
      "type": "service",
      "moduleName": "restrictionsServiceModule",
      "shortDescription": "Service that concerns business logic tasks related to CMS restrictions in the SAP Hybris platform.",
      "keywords": "array backend business cms cmssmarteditcontainer code concerns crud current currentpage defined fetching getallrestrictions getbyid getcontentapiuri getpagedrestrictionsfortype getstructureapiuri hybris identifier item leave logic mask matching maximum method mode number object omited optional pagesize parameter passed placeholder platform query replaced requested resourcelocationmodule resourcelocationsmodule restrict restriction restrictionid restrictions restrictionsrestservice restrictionsservicemodule restrictiontypecode sap search server service site size smartedit string structure structuremodemanagerfactory structuresrestservice supported system tasks type typecode typecodes uri uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsTableModule",
      "shortName": "restrictionsTableModule",
      "type": "overview",
      "moduleName": "restrictionsTableModule",
      "shortDescription": "This module defines the restrictionsTable component",
      "keywords": "cmssmarteditcontainer component defines directive l10nmodule module overview restrictionscriteriaservicemodule restrictionstable restrictionstablemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionsTableModule.directive:restrictionsTable",
      "shortName": "restrictionsTable",
      "type": "directive",
      "moduleName": "restrictionsTableModule",
      "shortDescription": "Directive that can render a list of restrictions and provides callback functions such as onSelect and onCriteriaSelected. *",
      "keywords": "accepts array boolean callback class cmssmarteditcontainer criteria css custom customclass directive edit editable errors event function functions list modified object onclickonedit oncriteriaselected onselect render restrictioncriteria restrictions restrictionstablemodule select selected string table triggers"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionTypesRestServiceModule",
      "shortName": "restrictionTypesRestServiceModule",
      "type": "overview",
      "moduleName": "restrictionTypesRestServiceModule",
      "shortDescription": "This module contains the restrictionTypesRestService REST service for restrictionTypes API.",
      "keywords": "api cmssmarteditcontainer module overview rest restrictiontypes restrictiontypesrestservice restrictiontypesrestservicemodule restservicefactorymodule service"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionTypesRestServiceModule.service:restrictionTypesRestService",
      "shortName": "restrictionTypesRestService",
      "type": "service",
      "moduleName": "restrictionTypesRestServiceModule",
      "shortDescription": "Service that handles REST requests for the restrictionTypes CMS API endpoint.",
      "keywords": "api array cms cmssmarteditcontainer endpoint getrestrictiontypes handles languageservice method requests rest restriction restriction_types_uri restrictiontypes restrictiontypesrestservicemodule restservicefactory service system types"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionTypesServiceModule",
      "shortName": "restrictionTypesServiceModule",
      "type": "overview",
      "moduleName": "restrictionTypesServiceModule",
      "shortDescription": "This module defines the restrictionTypesService REST service used to consolidate business logic for SAP Hybris platform CMS restriction types for",
      "keywords": "business cms cmssmarteditcontainer components consolidate defines hybris logic module overview pagetypesrestrictiontypesservicemodule platform rest restriction restrictiontypesrestservicemodule restrictiontypesservice restrictiontypesservicemodule sap service types"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "restrictionTypesServiceModule.service:restrictionTypesService",
      "shortName": "restrictionTypesService",
      "type": "service",
      "moduleName": "restrictionTypesServiceModule",
      "shortDescription": "Service that concerns business logic tasks related to CMS page and component restriction types in the SAP Hybris platform.",
      "keywords": "$q applied business caches cms cmssmarteditcontainer code component concerns configured duration fetches getrestrictiontypefortypecode getrestrictiontypes getrestrictiontypesbypagetype hybris languageservice logic matches method object pagetype pagetypesrestrictiontypesservice platform property restriction restrictions restrictiontypesrestservice restrictiontypesservicemodule sap service session system tasks type typecode types"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seBackendValidationHandlerModule",
      "shortName": "seBackendValidationHandlerModule",
      "type": "overview",
      "moduleName": "seBackendValidationHandlerModule",
      "shortDescription": "This module provides the seBackendValidationHandler service, which handles standard OCC validation errors received",
      "keywords": "backend cmssmarteditcontainer errors handles module occ overview received sebackendvalidationhandler sebackendvalidationhandlermodule service standard validation"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seBackendValidationHandlerModule.seBackendValidationHandler",
      "shortName": "seBackendValidationHandlerModule.seBackendValidationHandler",
      "type": "service",
      "moduleName": "seBackendValidationHandlerModule",
      "shortDescription": "The seBackendValidationHandler service handles validation errors received from the backend.",
      "keywords": "appended appends array backend cmssmarteditcontainer consisting context contextual contract data details error errors errorscontext example exception expected extracts format handleresponse handles list matches message method mysubject occurred originally output parameter provided received response sebackendvalidationhandler sebackendvalidationhandlermodule service someothererror subject type validation validationerror var"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seBreadcrumbModule.directive:seBreadcrumb",
      "shortName": "seBreadcrumb",
      "type": "directive",
      "moduleName": "seBreadcrumbModule",
      "shortDescription": "Directive that will build a navigation breadcrumb for the Node identified by either uuid or uid.",
      "keywords": "breadcrumb build cmssmarteditcontainer directive identified navigation node nodeuid nodeuuid object operations perform resourcelocationsmodule sebreadcrumbmodule uid uricontext uuid"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seFileValidationServiceModule",
      "shortName": "seFileValidationServiceModule",
      "type": "overview",
      "moduleName": "seFileValidationServiceModule",
      "shortDescription": "This module provides the seFileValidationService service, which validates if a specified file meets the required file",
      "keywords": "cmssmarteditcontainer commerce constraints file hybris meets module overview required sap sefilevalidationservice sefilevalidationservicemodule service size type validates"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seFileValidationServiceModule.seFileObjectValidators",
      "shortName": "seFileValidationServiceModule.seFileObjectValidators",
      "type": "object",
      "moduleName": "seFileValidationServiceModule",
      "shortDescription": "A list of file validators, that includes a validator for file-size constraints and a validator for file-type",
      "keywords": "cmssmarteditcontainer constraints file file-size file-type includes list object sefileobjectvalidators sefilevalidationservicemodule validator validators"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seFileValidationServiceModule.seFileValidationService",
      "shortName": "seFileValidationServiceModule.seFileValidationService",
      "type": "service",
      "moduleName": "seFileValidationServiceModule",
      "shortDescription": "The seFileValidationService validates that the file provided is of a specified file type and that the file does not",
      "keywords": "api append appends array buildacceptedfiletypeslist cmssmarteditcontainer comma- comma-separated context contextual creates error errors exceed extensions file header limit list maxium method object output parameter promise provided rejected resolves sefilemimetypeservice sefilemimetypeservicemodule sefileobjectvalidators sefilevalidationservice sefilevalidationserviceconstants sefilevalidationservicemodule separated service size supported transforms type types valid validate validated validates validator web"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seFileValidationServiceModule.seFileValidationServiceConstants",
      "shortName": "seFileValidationServiceModule.seFileValidationServiceConstants",
      "type": "object",
      "moduleName": "seFileValidationServiceModule",
      "shortDescription": "The constants provided by the file validation service.",
      "keywords": "bytes cmssmarteditcontainer constants file internationalization list map maximum object platform provided sefilevalidationserviceconstants sefilevalidationservicemodule service size supported types uploaded validation"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectPageTemplateModule.directive:selectPageTemplate",
      "shortName": "selectPageTemplate",
      "type": "directive",
      "moduleName": "selectPageTemplateModule",
      "shortDescription": "Displays a list of all CMS page templates in the system, and allows the user to select one, triggering the on-template-selected callback.",
      "keywords": "allows argument callback called cms cmssmarteditcontainer directive displays function list object on-template-selected ontemplateselected representing select select-page-template selected selectpagetemplatemodule single system template templates triggering user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectPageTypeModule",
      "shortName": "selectPageTypeModule",
      "type": "overview",
      "moduleName": "selectPageTypeModule",
      "shortDescription": "#selectPageTypeModule",
      "keywords": "cmssmarteditcontainer component directive module overview selectpagetype selectpagetypemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectPageTypeModule",
      "shortName": "selectPageTypeModule",
      "type": "overview",
      "moduleName": "selectPageTypeModule",
      "shortDescription": "#selectPageTypeModule",
      "keywords": "cmssmarteditcontainer component directive module overview selectpagetype selectpagetypemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectPageTypeModule.directive:selectPageType",
      "shortName": "selectPageType",
      "type": "directive",
      "moduleName": "selectPageTypeModule",
      "shortDescription": "Displays a list of all CMS page types in the system, and allows the user to select one, triggering the on-type-selected callback.",
      "keywords": "allows argument callback called cms cmssmarteditcontainer data-select-page-type directive displays function list object on-type-selected ontypeselected representing select selected selectpagetypemodule single system triggering type types user"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectTargetCatalogVersionModule",
      "shortName": "selectTargetCatalogVersionModule",
      "type": "overview",
      "moduleName": "selectTargetCatalogVersionModule",
      "shortDescription": "#selectTargetCatalogVersionModule",
      "keywords": "cmssmarteditcontainer component directive module overview selecttargetcatalogversion selecttargetcatalogversionmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "selectTargetCatalogVersionModule.directive:selectTargetCatalogVersion",
      "shortName": "selectTargetCatalogVersion",
      "type": "directive",
      "moduleName": "selectTargetCatalogVersionModule",
      "shortDescription": "Displays a list of catalog versions used for selecting the target catalog version that will be applied to the cloned page",
      "keywords": "applied binding catalog catalogid catalogversion change cloned cmssmarteditcontainer component context determine directive display displays executed exists function label list object ontargetcatalogversionselected optional output pagelabel pagetypecode potential select-target-catalog-version selecting selection selecttargetcatalogversionmodule site siteuid string target time typecode uri uricontext version versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seMediaServiceModule",
      "shortName": "seMediaServiceModule",
      "type": "overview",
      "moduleName": "seMediaServiceModule",
      "shortDescription": "The media service module provides a service to create an image file for a catalog through AJAX calls. This module",
      "keywords": "$resource ajax calls catalog cmssmarteditcontainer create data dedicated file form image media mediaservicemodule module multipart overview posts request semediaservicemodule service transformed"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seMediaServiceModule.seMediaResource",
      "shortName": "seMediaServiceModule.seMediaResource",
      "type": "service",
      "moduleName": "seMediaServiceModule",
      "shortDescription": "A $resource that makes REST calls to the default",
      "keywords": "$resource angularjs api calls catalog cms cmssmarteditcontainer collection content-type default file formdata http https media mediaservice method methods mozilla multipart object org pojo post request required rest retrieve semediaresource semediaservicemodule service supports transform transformation uploads"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seMediaServiceModule.seMediaResourceService",
      "shortName": "seMediaServiceModule.seMediaResourceService",
      "type": "service",
      "moduleName": "seMediaServiceModule",
      "shortDescription": "This service provides an interface to the $resource that makes REST ",
      "keywords": "$resource angularjs api calls catalog cms cmssmarteditcontainer default http https interface media method org rest retrieve returning semediaresourceservice semediaservicemodule service single supports"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seMediaServiceModule.seMediaService",
      "shortName": "seMediaServiceModule.seMediaService",
      "type": "service",
      "moduleName": "seMediaServiceModule",
      "shortDescription": "This service provides an interface to the $resource provided by the seMediaResource service and ",
      "keywords": "$resource alternate alttext angularjs backend catalog catalog-catalog cmssmarteditcontainer code combination corresponding corresponds description errors exists fails fetch fetches file functionality getmediabycode https identifier images interface media method mozilla object org pojo promise provided request resolves returns selected semediaresource semediaresourceservice semediaservice semediaservicemodule service specific successful text unique upload uploaded uploadmedia uploads version"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seNavigationNodePickerModule.directive:seNavigationPicker",
      "shortName": "seNavigationPicker",
      "type": "directive",
      "moduleName": "seNavigationNodePickerModule",
      "shortDescription": "Directive that will build a navigation node picker and assign the uid of the selected node to model[qualifier].",
      "keywords": "assign build cmssmarteditcontainer directive model navigation node object operations perform picker property qualifier resourcelocationsmodule selected senavigationnodepickermodule set uid uricontext"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seObjectValidatorFactoryModule",
      "shortName": "seObjectValidatorFactoryModule",
      "type": "overview",
      "moduleName": "seObjectValidatorFactoryModule",
      "shortDescription": "This module provides the seObjectValidatorFactory service, which is used to build a validator for a specified list of",
      "keywords": "build cmssmarteditcontainer list module objects overview seobjectvalidatorfactory seobjectvalidatorfactorymodule service validator"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "seObjectValidatorFactoryModule.seObjectValidatorFactory",
      "shortName": "seObjectValidatorFactoryModule.seObjectValidatorFactory",
      "type": "service",
      "moduleName": "seObjectValidatorFactoryModule",
      "shortDescription": "This service provides a factory method to build a validator for a specified list of validator objects.",
      "keywords": "append associate beause block build builds case cmssmarteditcontainer code consist consists contextual described error errors errorscontext example factory fail failed false function invalid isvalid list message method object objects objectundervalidation parameter parameters predicate result return seobjectvalidatorfactory seobjectvalidatorfactorymodule service single subject takes validate validating validator validators var"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "slotVisibilityServiceModule",
      "shortName": "slotVisibilityServiceModule",
      "type": "overview",
      "moduleName": "slotVisibilityServiceModule",
      "shortDescription": "The slot visibility service module provides factories and services to manage all backend calls and loads an internal",
      "keywords": "backend button calls cmssmarteditcontainer component data factories internal loads manage module overview service services slot slotvisibilityservicemodule structure visibility"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "slotVisibilityServiceModule.service:SlotVisibilityService",
      "shortName": "SlotVisibilityService",
      "type": "service",
      "moduleName": "slotVisibilityServiceModule",
      "shortDescription": "The SlotVisibilityService class provides methods to interact with the backend.",
      "keywords": "array backend based cache called class cmsitemsrestservice cmssmarteditcontainer component componenthandlerservice components componentuuid componnet content context current definition exists gethiddencomponents getslotsforcomponent hidden ids instance instantiated interact list method methods modified pagescontentslotscomponents pagescontentslotscomponentsresource promise provided re-evalated reloads reloadslotinfo resolves returns service slot slotid slots slotvisibilityservice slotvisibilityservicemodule uuid uuids"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationConstantsModule",
      "shortName": "synchronizationConstantsModule",
      "type": "overview",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Contains various constants used by the synchronization modules.",
      "keywords": "cmssmarteditcontainer constants modules object overview synchronization synchronization_polling synchronization_statuses synchronizationconstantsmodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationConstantsModule.object:SYNCHRONIZATION_POLLING",
      "shortName": "SYNCHRONIZATION_POLLING",
      "type": "object",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Constant containing polling related values",
      "keywords": "cmssmarteditcontainer constant event fast_fetch fast_polling_time fetch fetch_sync_status_once fetchsyncstatusonce milliseconds object polling slow slow_down slow_polling_time speed speed_up sync syncfastfetch synchronizationconstantsmodule syncpollingslowdown syncpollingspeedup time trigger values"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationConstantsModule.object:SYNCHRONIZATION_STATUSES",
      "shortName": "SYNCHRONIZATION_STATUSES",
      "type": "object",
      "moduleName": "synchronizationConstantsModule",
      "shortDescription": "Constant containing the different sync statuses",
      "keywords": "cmssmarteditcontainer constant in_progress in_sync not_sync object statuses sync sync_failed synchronizationconstantsmodule unavailable"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationPanelModule.component:synchronizationPanel",
      "shortName": "synchronizationPanel",
      "type": "directive",
      "moduleName": "synchronizationPanelModule",
      "shortDescription": "This component reads and performs synchronization for a main object and its dependencies (from a synchronization perspective).",
      "keywords": "abstractpage aggregated backend-configured button callback change cmssmarteditcontainer component contentslot customization customize dependencies dependency dependent dependentitemtypesoutofsync directive display enables expected format function getsyncstatus grouping header headertemplateurl html i18nkey identifier identifiers in_progress in_sync initialize internal invoke invoked item itemid items itemtype key lastsynctime list listed lists logical long main manual metadata navigation not_available not_in_sync object optional page1 panel parent path perform performs performsync perspective popover promise reads required restrictions returns row selectall selected selecteddependencies set slot slot1 someuid status statuses sync sync_failed synchronization synchronization-panel synchronizationpanelmodule syncitems template title types unique values"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationServiceModule",
      "shortName": "synchronizationServiceModule",
      "type": "overview",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "The synchronization module contains the service necessary to perform catalog synchronization.",
      "keywords": "api backend calls catalog cmssmarteditcontainer module order overview perform service status synchronization synchronizationservice synchronizationservicemodule trigger versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationServiceModule.directive:synchronizeCatalog",
      "shortName": "synchronizeCatalog",
      "type": "directive",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "The synchronize catalog directive is used to display catalog synchronization information in the landing page. ",
      "keywords": "active active-catalog activecatalogversion button catalog catalogversion cmssmarteditcontainer current details directive display displays going job landing non-active object provided representing sync synchronization synchronizationservicemodule synchronize synchronize-catalog trigger version versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationServiceModule.object:CATALOG_SYNC_INTERVAL_IN_MILLISECONDS",
      "shortName": "CATALOG_SYNC_INTERVAL_IN_MILLISECONDS",
      "type": "object",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "This object defines an injectable Angular constant that determines the frequency to update catalog synchronization information. ",
      "keywords": "angular catalog cmssmarteditcontainer constant defines determines frequency injectable milliseconds object synchronization synchronizationservicemodule update"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizationServiceModule.service:synchronizationService",
      "shortName": "synchronizationService",
      "type": "service",
      "moduleName": "synchronizationServiceModule",
      "shortDescription": "The synchronization service manages RESTful calls to the synchronization service&#39;s backend API.",
      "keywords": "api auto backend calls catalog catalogid cmssmarteditcontainer getcatalogsyncstatus job manages method object restful service source sourcecatalogversion starts status stopautogetsyncdata stops synchronization synchronizationservicemodule synchronize synchronized target targetcatalogversion update updatecatalogsync version versions"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "synchronizeCatalogModule",
      "shortName": "synchronizeCatalogModule",
      "type": "overview",
      "moduleName": "synchronizeCatalogModule",
      "shortDescription": "The synchronization module contains the service and the directives necessary ",
      "keywords": "api area backend calls catalog cmssmarteditcontainer directive directives display landing module order overview perform service status store synchronaization synchronization synchronizationservice synchronizationservicemodule synchronizecatalog synchronizecatalogmodule trigger"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "syncPageModalServiceModule",
      "shortName": "syncPageModalServiceModule",
      "type": "overview",
      "moduleName": "syncPageModalServiceModule",
      "shortDescription": "The syncPageModalServiceModule",
      "keywords": "allows cmssmarteditcontainer component modal module opening overview service synchronization synchronizationpanelmodule syncpagemodalservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "syncPageModalServiceModule.service:syncPageModalService",
      "shortName": "syncPageModalService",
      "type": "service",
      "moduleName": "syncPageModalServiceModule",
      "shortDescription": "Convenience service to open n synchronization modal window for a given page&#39;s data.",
      "keywords": "associated button cancel closed cmssmarteditcontainer component content convenience data defined editor method modal modalservice object open pageeditormodalservicemodule platform promise resolves resourcelocationsmodule returned save service synchronization synchronizationpanelmodule syncpagemodalservicemodule uricontext window wired"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "typeStructureRestServiceModule",
      "shortName": "typeStructureRestServiceModule",
      "type": "overview",
      "moduleName": "typeStructureRestServiceModule",
      "shortDescription": "The typeStructureRestServiceModule",
      "keywords": "cms cmssmarteditcontainer overview rest services structure typestructurerestservicemodule"
    },
    {
      "section": "cmssmarteditContainer",
      "id": "typeStructureRestServiceModule.service:typeStructureRestService",
      "shortName": "typeStructureRestService",
      "type": "service",
      "moduleName": "typeStructureRestServiceModule",
      "shortDescription": "The typeStructureRestService provides functionality for fetching page structures",
      "keywords": "array category cms cmssmarteditcontainer code component componenttype control editor fetch fetched fetches fetching fields functionality generic getstructurebytype getstructuresbycategory method mode object optional representing restriction retrieve returned service structure structures supported type typecode typestructurerestservice typestructurerestservicemodule"
    }
  ],
  "apis": {
    "cmssmartedit": true,
    "cmssmarteditContainer": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/#/cmssmartedit",
  "scripts": [
    "angular.min.js"
  ]
};