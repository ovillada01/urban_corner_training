angular.module('cmscommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmscommons/components/synchronize/synchronizationPanelTemplate.html',
    "<div class=\"se-sync-panel\">\n" +
    "    <y-message data-ng-if=\"false\"\n" +
    "        data-message-id=\"yMsgWarningId\"\n" +
    "        data-type=\"warning\"\n" +
    "        data-ng-class=\"!sync.showSyncButton ? 'se-sync-panel--y-message--modal-adjusted' : 'se-sync-panel--y-message--toolbar-adjusted'\">\n" +
    "        <message-title>{{ 'se.cms.synchronization.panel.live.recent.notice' | translate }}</message-title>\n" +
    "        <message-description>{{ 'se.cms.synchronization.panel.live.override.warning' | translate }}</message-description>\n" +
    "    </y-message>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-status\"\n" +
    "        data-ng-if=\"sync.headerTemplateUrl\"\n" +
    "        data-ng-include=\"sync.headerTemplateUrl\">\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__sync-info\">\n" +
    "        <div data-ng-repeat=\"dependency in sync.getRows()\"\n" +
    "            data-ng-class=\"{active: $index==0, 'se-sync-panel--item__external': dependency.isExternal}\"\n" +
    "            class=\"se-sync-panel__sync-info__row\">\n" +
    "\n" +
    "            <div class=\"checkbox se-sync-panel__sync-info__checkbox se-nowrap-ellipsis\">\n" +
    "                <input type=\"checkbox\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-model=\"dependency.selected\"\n" +
    "                    data-ng-disabled=\"sync.isDisabled(dependency)\"\n" +
    "                    data-ng-change=\"sync.selectionChange($index)\"\n" +
    "                    id=\"sync-info__checkbox_{{$index}}\">\n" +
    "                <label data-ng-if=\"$index===0\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.selectAll | translate}}\">\n" +
    "                    {{::dependency.selectAll | translate}}</label>\n" +
    "\n" +
    "                <label data-ng-if=\"$index!==0 && !dependency.isExternal\"\n" +
    "                    for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                    class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                    title=\"{{::dependency.name | translate}}\">\n" +
    "                    {{::dependency.name | translate}}</label>\n" +
    "\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    data-y-popover\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-template=\"sync.getTemplateInfoForExternalComponent()\">\n" +
    "                    <label data-ng-if=\"$index!==0\"\n" +
    "                        for=\"sync-info__checkbox_{{$index}}\"\n" +
    "                        class=\"se-sync-panel__sync-info__checkbox-label se-nowrap-ellipsis\"\n" +
    "                        title=\"{{::dependency.name | translate}}\">\n" +
    "                        {{::dependency.name | translate}}</label>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "            <span data-ng-if=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-y-popover\n" +
    "                data-trigger=\"'hover'\"\n" +
    "                data-title=\"sync.getInfoTitle(dependency)\"\n" +
    "                data-template=\"sync.buildInfoTemplate(dependency)\"\n" +
    "                data-ng-class=\"{'pull-right se-sync-panel__sync-info__right-icon': true, 'se-sync-panel--icon-globe': dependency.isExternal} \">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-if=\"!dependency.isExternal\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "                <span data-ng-if=\"dependency.isExternal\"\n" +
    "                    class=\"hyicon hyicon-globe\"></span>\n" +
    "            </span>\n" +
    "\n" +
    "            <span data-ng-if=\"!sync.buildInfoTemplate(dependency)\"\n" +
    "                class=\"pull-right se-sync-panel__sync-info__right-icon\">\n" +
    "                <span data-status=\"{{dependency.status}}\"\n" +
    "                    data-ng-class=\"{'hyicon hyicon__se-sync-panel__sync-info':true, 'hyicon-done hyicon__se-sync-panel__sync-done':sync.isInSync(dependency), 'hyicon-sync hyicon__se-sync-panel__sync-not':!sync.isInSync(dependency)}\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-sync-panel__footer\"\n" +
    "        data-ng-if=\"sync.showSyncButton\">\n" +
    "        <button class=\"btn btn-lg btn-primary se-sync-panel__footer__btn\"\n" +
    "            data-ng-disabled=\"sync.isSyncButtonDisabled()\"\n" +
    "            data-ng-click=\"sync.syncItems()\"\n" +
    "            data-translate=\"se.cms.pagelist.dropdown.sync\"></button>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );

}]);
