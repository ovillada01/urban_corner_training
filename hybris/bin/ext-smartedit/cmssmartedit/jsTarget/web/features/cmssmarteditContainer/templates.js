angular.module('cmssmarteditContainerTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemDropdownTemplate.html',
    "<se-dropdown data-field=\"cmsItemDropdownCtrl.field\"\n" +
    "    data-qualifier=\"cmsItemDropdownCtrl.qualifier\"\n" +
    "    data-model=\"cmsItemDropdownCtrl.model\"\n" +
    "    data-id=\"cmsItemDropdownCtrl.id\"\n" +
    "    data-item-template-url=\"cmsItemDropdownCtrl.itemTemplateUrl\"></se-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemDropdownWrapperTemplate.html',
    "<cms-item-dropdown data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></cms-item-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/cmsItemDropdown/cmsItemSearchTemplate.html',
    "<div class=\"y-select-item-printer\">\n" +
    "    <div>{{item.name}}</div>\n" +
    "    <div>{{item.uid}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/componentMenuTemplate.html',
    "<div class=\"se-component-menu\"\n" +
    "    data-recompile-dom=\"$ctrl._recompileDom\"\n" +
    "    data-ng-class=\"{ 'se-component-menu__localized': $ctrl.hasMultipleContentCatalogs }\">\n" +
    "\n" +
    "    <y-tabset class=\"se-component-menu--tabs\"\n" +
    "        data-tabs-list=\"$ctrl.tabsList\"\n" +
    "        data-model=\"$ctrl.tabsList\"\n" +
    "        data-num-tabs-displayed=\"2\">\n" +
    "    </y-tabset>\n" +
    "\n" +
    "    <div class=\"se-component-menu--help-tip\"\n" +
    "        data-translate=\"se.cms.componentmenu.label.draganddrop\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/componentMenuWrapperTemplate.html',
    "<component-menu action-item=\"item\"></component-menu>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/catalogVersionTemplate.html',
    "<div class=\"se-component-menu--select-local\">\n" +
    "    <span class=\"hyicon hyicon-globe se-component-menu--select-globe\"\n" +
    "        data-ng-if=\"item.isCurrentCatalog == false\"></span>\n" +
    "    <span class=\"se-component-menu--select-text\">{{item.catalogName | l10n}} - {{item.catalogVersionId}}</span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentInfoPopoverTemplate.html',
    "<div class=\"se-component-item--visibility-tooltip\"\n" +
    "    data-translate=\"se.cms.component.display.off.tooltip\"></div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentItemTemplate.html',
    "<div class=\"se-component-item\">\n" +
    "    <div class=\"smartEditComponent se-component-item--image\"\n" +
    "        data-smartedit-component-id=\"{{$ctrl.componentInfo.uid}}\"\n" +
    "        data-smartedit-component-uuid=\"{{$ctrl.componentInfo.uuid}}\"\n" +
    "        data-smartedit-component-type=\"{{$ctrl.componentInfo.typeCode}}\">\n" +
    "        <img data-ng-src=\"{{$ctrl.imageRoot}}/images/component_default.png\"\n" +
    "            class=\"se-component-item--image-element\"\n" +
    "            alt=\"{{$ctrl.componentInfo.name}}\"\n" +
    "            title=\"{{$ctrl.componentInfo.name}} - {{$ctrl.componentInfo.typeCode}}\" />\n" +
    "    </div>\n" +
    "    <div class=\"se-component-item--details-container\">\n" +
    "        <div class=\"se-component-item--details\"\n" +
    "            title=\"{{$ctrl.componentInfo.name}} - {{$ctrl.componentInfo.typeCode}}\">\n" +
    "            <div class=\"se-component-item--details-name\">{{$ctrl.componentInfo.name}}</div>\n" +
    "            <div class=\"se-component-item--details-type\">{{$ctrl.componentInfo.typeCode}}</div>\n" +
    "        </div>\n" +
    "        <div data-ng-if=\"!$ctrl.componentInfo.visible\"\n" +
    "            class=\"se-component-item--visibility\"\n" +
    "            data-y-popover\n" +
    "            data-trigger=\"'hover'\"\n" +
    "            data-placement=\"'bottom'\"\n" +
    "            data-template-url=\"'componentInfoPopoverTemplate.html'\">\n" +
    "            <span class=\"hyicon hyicon-unpowered\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentSearchTemplate.html',
    "<div class=\"se-input-group se-component-search\">\n" +
    "    <div class=\"se-input-group--addon se-component-search--addon\">\n" +
    "        <span class=\"hyicon hyicon-search se-component-search--search-icon\"></span>\n" +
    "    </div>\n" +
    "    <input type=\"text\"\n" +
    "        class=\"se-input-group--input se-component-search--input\"\n" +
    "        name=\"search-term\"\n" +
    "        data-ng-model=\"$ctrl.searchTerm\"\n" +
    "        data-ng-model-options=\"{debounce: 500}\"\n" +
    "        placeholder=\"{{$ctrl.placeholder | translate}}\">\n" +
    "    <div data-ng-show=\"$ctrl.showResetButton\"\n" +
    "        class=\"se-input-group--addon se-component-search--addon\"\n" +
    "        data-ng-click=\"$ctrl._resetSearchBox()\">\n" +
    "        <span class=\"glyphicon glyphicon-remove-sign se-component-search--reset-icon\"></span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/components/componentTypeTemplate.html',
    "<div class=\"se-component-item\">\n" +
    "\n" +
    "    <div class=\"smartEditComponent se-component-item--image\"\n" +
    "        data-smartedit-component-type=\"{{$ctrl.typeInfo.code}}\">\n" +
    "        <img class=\"se-component-item--image-element\"\n" +
    "            data-ng-src=\"{{$ctrl.imageRoot}}/images/component_default.png\"\n" +
    "            alt=\"{{$ctrl.typeInfo.i18nKey | translate}}\"\n" +
    "            title=\"{{$ctrl.typeInfo.i18nKey | translate}}\" />\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-component-item--details-container\">\n" +
    "        <div class=\"se-component-item--details\">\n" +
    "            <div class=\"se-component-item--details-type\"\n" +
    "                title=\"{{$ctrl.typeInfo.i18nKey | translate}}\"\n" +
    "                data-translate=\"{{$ctrl.typeInfo.i18nKey}}\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentTypesTabTemplate.html',
    "<component-search data-on-change=\"$ctrl.onSearchTermChanged(searchTerm)\"\n" +
    "    data-placeholder=\"se.cms.componentmenu.search.type.placeholder\"></component-search>\n" +
    "<div class=\"se-component-menu--item-container\">\n" +
    "    <div class=\"se-component-menu--overflow\">\n" +
    "        <div class=\"se-component-menu--results se-component-menu--results__types\">\n" +
    "            <div class=\"se-component-menu--item-wrap\"\n" +
    "                data-ng-repeat=\"componentType in $ctrl.componentTypes | nameFilter:$ctrl.searchTerm track by $id(componentType)\">\n" +
    "                <component-type data-type-info=\"componentType\"></component-type>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentTypesTabWrapperTemplate.html',
    "<component-types-tab></component-types-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentsTabTemplate.html',
    "<div data-ng-if=\"$ctrl.hasMultipleContentCatalogs == true\">\n" +
    "    <y-select class=\"se-component-menu--select\"\n" +
    "        data-id=\"uuid\"\n" +
    "        data-ng-model=\"$ctrl.selectedCatalogVersionId\"\n" +
    "        data-on-change=\"$ctrl.onCatalogVersionChange\"\n" +
    "        data-fetch-strategy=\"$ctrl.catalogVersionsFetchStrategy\"\n" +
    "        data-item-template=\"::$ctrl.catalogVersionTemplate\"\n" +
    "        data-search-enabled=\"false\" />\n" +
    "</div>\n" +
    "\n" +
    "<component-search data-on-change=\"$ctrl.onSearchTermChanged(searchTerm)\"\n" +
    "    data-placeholder=\"se.cms.componentmenu.search.placeholder\"></component-search>\n" +
    "\n" +
    "<div class=\"se-component-menu--item-container\"\n" +
    "    recompile-dom=\"$ctrl.resetComponentsList\">\n" +
    "    <y-infinite-scrolling data-ng-if=\"$ctrl.isActive\"\n" +
    "        class=\"se-component-menu--infinite-scroll\"\n" +
    "        data-drop-down-class=\"ySEComponents\"\n" +
    "        data-page-size=\"10\"\n" +
    "        data-mask=\"$ctrl.searchTerm\"\n" +
    "        data-fetch-page=\"$ctrl.loadComponentItems\"\n" +
    "        data-context=\"$ctrl\">\n" +
    "        <div class=\"se-component-menu--results se-component-menu--results__custom\">\n" +
    "            <component-item class=\"se-component-menu--item-wrap\"\n" +
    "                data-component-info=\"item\"\n" +
    "                data-ng-repeat=\"item in $ctrl.items track by item.uid\"></component-item>\n" +
    "\n" +
    "        </div>\n" +
    "    </y-infinite-scrolling>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/componentMenu/tabs/componentsTabWrapperTemplate.html',
    "<div>\n" +
    "    <components-tab has-multiple-content-catalogs=\"model.componentsTab.hasMultipleContentCatalogs\"\n" +
    "        is-active=\"model.componentsTab.active\"></components-tab>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/componentEditor/componentEditorTemplate.html',
    "<generic-editor data-id=\"compCtrl.tabId\"\n" +
    "    data-smartedit-component-id=\"compCtrl.componentId\"\n" +
    "    data-smartedit-component-type=\"compCtrl.componentType\"\n" +
    "    data-structure=\"compCtrl.tabStructure\"\n" +
    "    data-structure-api=\"compCtrl.structureApi\"\n" +
    "    data-content=\"compCtrl.content\"\n" +
    "    data-submit=\"compCtrl.saveTab\"\n" +
    "    data-reset=\"compCtrl.resetTab\"\n" +
    "    data-custom-on-submit=\"compCtrl.onSubmit\"\n" +
    "    data-is-dirty=\"compCtrl.isDirtyTab\"\n" +
    "    data-get-api=\"compCtrl.getApi($api);\">\n" +
    "</generic-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/componentEditor/componentEditorWrapperTemplate.html',
    "<y-message data-ng-if=\"$ctrl.showDisclaimer\"\n" +
    "    data-type=\"info\"\n" +
    "    data-message-id=\"VisibilityTab.DisplayComponentOffDisclaimer\">\n" +
    "    <message-description>\n" +
    "        <translate>se.cms.editortabset.visibilitytab.disclaimer</translate>\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "\n" +
    "<component-editor data-tab-id=\"$ctrl.tabId\"\n" +
    "    data-component-id=\"$ctrl.componentId\"\n" +
    "    data-component-type=\"$ctrl.componentType\"\n" +
    "    data-tab-structure=\"$ctrl.tabStructure\"\n" +
    "    data-structure-api=\"$ctrl.structureApi\"\n" +
    "    data-save-tab=\"$ctrl.saveTab\"\n" +
    "    data-reset-tab=\"$ctrl.resetTab\"\n" +
    "    data-cancel-tab=\"$ctrl.cancelTab\"\n" +
    "    data-component-info=\"$ctrl.componentInfo\"\n" +
    "    data-is-dirty-tab=\"$ctrl.isDirtyTab\"\n" +
    "    data-content=\"$ctrl.content\">\n" +
    "</component-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/adminTabTemplate.html',
    "<admin-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</admin-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/basicTabTemplate.html',
    "<basic-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</basic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/genericTabTemplate.html',
    "<generic-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</generic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/editorModal/tabs/visibilityTabTemplate.html',
    "<visibility-tab class='sm-tab-content'\n" +
    "    save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    data-component-info=\"model\"\n" +
    "    component-id=\"model.componentUuid\"\n" +
    "    component-type=\"model.componentType\">\n" +
    "</visibility-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/restrictionEditor/componentRestrictionsEditorTemplate.html',
    "<restrictions-editor data-editable=\"true\"\n" +
    "    data-on-restrictions-changed=\"componentRestrictionsEditorCtrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "    data-get-restriction-types=\"componentRestrictionsEditorCtrl.getRestrictionTypes()\"\n" +
    "    data-get-supported-restriction-types=\"componentRestrictionsEditorCtrl.getSupportedRestrictionTypes()\"\n" +
    "    data-item=\"componentRestrictionsEditorCtrl.model\"\n" +
    "    data-restrictions=\"componentRestrictionsEditorCtrl.model.restrictions\">\n" +
    "</restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/cmsComponents/restrictionEditor/componentRestrictionsEditorWrapperTemplate.html',
    "<component-restrictions-editor data-model=\"model\"></component-restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/editorTabset/editorTabsetTemplate.html',
    "<y-tabset data-model=\"model\"\n" +
    "    tabs-list=\"tabsList\"\n" +
    "    data-num-tabs-displayed=\"{{numTabsDisplayed}}\"\n" +
    "    tab-control=\"tabControl\">\n" +
    "</y-tabset>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/catalogAwareSelector/catalogAwareSelectorTemplate.html',
    "<div data-ng-if=\"ctrl.listIsEmpty()\">\n" +
    "    <!--\n" +
    "        It's important to add the type=button. Otherwise HTML 5 will default it to type=submit, causing the generic\n" +
    "        editor to submit the form whenever the panel opens.\n" +
    "    -->\n" +
    "    <button id=\"catalog-aware-selector-add-item\"\n" +
    "        type=\"button\"\n" +
    "        data-ng-if=\"ctrl.editable\"\n" +
    "        class=\"y-add-btn\"\n" +
    "        data-ng-click=\"ctrl.openEditingPanel()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>\n" +
    "        <span translate=\"se.cms.catalogaware.newbutton.title\"\n" +
    "            translate-values=\"{catalogItemType: ctrl.catalogItemType}\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "\n" +
    "<div data-ng-if=\"!ctrl.listIsEmpty()\"\n" +
    "    class=\"se-catalog-aware-selector__content\">\n" +
    "    <div class=\"se-catalog-aware-selector__content__btn-wrapper\"\n" +
    "        data-ng-if=\"ctrl.editable\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"btn btn-link se-catalog-aware-selector__content__btn\"\n" +
    "            data-ng-click=\"ctrl.openEditingPanel()\">\n" +
    "            {{'se.cms.catalogaware.list.addmore' | translate}}\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <y-editable-list data-id=\"{{ctrl.id + '_list'}}\"\n" +
    "        data-item-template-url=\"ctrl.itemTemplate\"\n" +
    "        data-items=\"ctrl.itemsList\"\n" +
    "        data-on-change=\"ctrl.onListChange\"\n" +
    "        data-editable=\"ctrl.editable\"\n" +
    "        data-refresh=\"ctrl._refreshListWidget\"\n" +
    "        class=\"se-catalog-aware-selector__list\">\n" +
    "    </y-editable-list>\n" +
    "</div>\n" +
    "\n" +
    "<se-item-selector-panel data-panel-title=\"ctrl.currentOptions.panelTitle\"\n" +
    "    data-items-selected=\"ctrl.model\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-item-template=\"ctrl.itemTemplate\"\n" +
    "    data-on-change=\"ctrl.onPanelChange\"\n" +
    "    data-show-panel=\"ctrl.showPanel\"\n" +
    "    data-hide-panel=\"ctrl.closePanel\"\n" +
    "    data-catalog-item-type=\"ctrl.catalogItemType\"\n" +
    "    data-max-num-items=\"ctrl.maxNumItems\">\n" +
    "</se-item-selector-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/categorySelectorTemplate.html',
    "<se-catalog-aware-selector data-id=\"{{ctrl.id}}\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-item-template=\"ctrl.categoryTemplate\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-catalog-item-type-key=\"se.cms.catalogaware.catalogitemtype.category\"\n" +
    "    data-editable=\"ctrl.editable\"></se-catalog-aware-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/categoryTemplate.html',
    "<div class=\"row se-product-row\">\n" +
    "    <div ng-if=\"item !== undefined\">\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.name | l10n}}\">{{item.name | l10n}}</div>\n" +
    "        <div class=\"col-md-6 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.catalogId}} - {{item.catalogVersion}}\">{{item.catalogId}} - {{item.catalogVersion}}</div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"node !== undefined\">\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.name | l10n}}\">{{node.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.catalogId}} - {{node.catalogVersion}}\">{{node.catalogId}} - {{node.catalogVersion}}</div>\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <y-drop-down-menu data-ng-if=\"ctrl.treeOptions.dragEnabled\"\n" +
    "                data-dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "                data-selected-item=\"this\"\n" +
    "                class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/categorySelector/multiCategorySelectorTemplate.html',
    "<se-category-selector data-id=\"{{field.qualifier}}\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-editable=\"field.editable\"></se-category-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/itemSelectorPanel/catalogItemTemplate.html',
    "<span data-ng-bind-html=\"item.name | l10n\"></span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/itemSelectorPanel/itemSelectorPanelTemplate.html',
    "<y-slider-panel data-slider-panel-show=\"ctrl._internalShowPanel\"\n" +
    "    data-slider-panel-hide=\"ctrl._internalHidePanel\"\n" +
    "    data-slider-panel-configuration=\"ctrl.sliderConfiguration\"\n" +
    "    class=\"se-item-selector-panel\">\n" +
    "    <content recompile-dom=\"ctrl.resetSelector\"\n" +
    "        class=\"se-item-selector-panel__content\">\n" +
    "        <!-- Show the dropdown only if there's more than one catalogs to choose from -->\n" +
    "        <div data-ng-if=\"ctrl.catalogs.length > 1\"\n" +
    "            class=\"se-item-selector-panel__content__item\">\n" +
    "            <label class=\"control-label\">{{'se.cms.catalogaware.panel.catalogs.label' | translate}}</label>\n" +
    "            <y-select data-id=\"se-catalog-selector-dropdown\"\n" +
    "                data-ng-model=\"ctrl.catalogInfo.catalogId\"\n" +
    "                data-fetch-strategy=\"ctrl.catalogSelectorFetchStrategy\"\n" +
    "                data-item-template=\"ctrl.catalogItemTemplate\"\n" +
    "                data-on-change=\"ctrl._onCatalogSelectorChange\"\n" +
    "                data-reset=\"ctrl.resetCatalogSelector\"\n" +
    "                class=\"se-item-selector-panel__content__catalogs__yselect\">\n" +
    "            </y-select>\n" +
    "            <label data-ng-if=\"ctrl.catalogs.length !== 1\">{{ctrl.catalogInfo.name | l10n}}</label>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"se-item-selector-panel__content__item\">\n" +
    "            <label class=\"control-label\">{{'se.cms.catalogaware.panel.catalogsversion.label' | translate}}</label>\n" +
    "            <y-select data-id=\"se-catalog-version-selector-dropdown\"\n" +
    "                data-ng-model=\"ctrl.catalogInfo.catalogVersion\"\n" +
    "                data-fetch-strategy=\"ctrl.catalogVersionSelectorFetchStrategy\"\n" +
    "                data-on-change=\"ctrl._onCatalogVersionSelectorChange\"\n" +
    "                data-search-enabled=false\n" +
    "                data-reset=\"ctrl.resetCatalogVersionSelector\"\n" +
    "                class=\"se-item-selector-panel__content__catalog-version__yselect\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "        <!-- Show the multi-select-->\n" +
    "        <div class=\"se-item-selector-panel__content__item\"\n" +
    "            data-ng-show=\"ctrl.catalogInfo.catalogVersion\">\n" +
    "            <label class=\"control-label\">{{ctrl.catalogItemType}}</label>\n" +
    "            <y-select id=\"se-items-selector-dropdown\"\n" +
    "                multi-select=\"'true'\"\n" +
    "                controls=\"'true'\"\n" +
    "                ng-model=\"ctrl._internalItemsSelected\"\n" +
    "                on-change=\"ctrl._onItemsSelectorChange\"\n" +
    "                fetch-strategy=\"ctrl._internalItemsFetchStrategy\"\n" +
    "                reset=\"ctrl.resetItemsListSelector\"\n" +
    "                item-template=\"ctrl.itemTemplate\"\n" +
    "                is-read-only=\"!ctrl._isItemSelectorEnabled()\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "    </content>\n" +
    "</y-slider-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/multiProductSelectorTemplate.html',
    "<se-product-selector data-id=\"{{field.qualifier}}\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-editable=\"field.editable\"></se-product-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/productSelectorTemplate.html',
    "<se-catalog-aware-selector data-id=\"{{ctrl.id}}\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-item-template=\"ctrl.productTemplate\"\n" +
    "    data-get-catalogs=\"ctrl.getCatalogs\"\n" +
    "    data-items-fetch-strategy=\"ctrl.itemsFetchStrategy\"\n" +
    "    data-catalog-item-type-key=\"se.cms.catalogaware.catalogitemtype.product\"\n" +
    "    data-max-num-items=\"ctrl.maxNumItems\"></se-catalog-aware-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/catalog/components/productSelector/productTemplate.html',
    "<div class=\"row se-product-row\">\n" +
    "    <div ng-if=\"item !== undefined\">\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <div class=\"se-default-product-img\">\n" +
    "                <div style=\"background-image: url('{{item.thumbnail.url}}');\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{item.name | l10n}}\">{{item.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog\">\n" +
    "            <div class=\"se-nowrap-ellipsis\"\n" +
    "                title=\"{{item.catalogId}} - {{item.catalogVersion}}\">{{item.catalogId}} - {{item.catalogVersion}}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"node !== undefined\">\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <div class=\"se-default-product-img\">\n" +
    "                <div style=\"background-image: url('{{node.thumbnail.url}}');\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-5 se-product-row__product se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.name | l10n}}\">{{node.name | l10n}}</div>\n" +
    "        <div class=\"col-md-5 se-product-row__catalog se-nowrap-ellipsis\"\n" +
    "            title=\"{{node.catalogId}} - {{node.catalogVersion}}\">{{node.catalogId}} - {{node.catalogVersion}}</div>\n" +
    "        <div class=\"col-md-1\">\n" +
    "            <y-drop-down-menu data-ng-if=\"ctrl.treeOptions.dragEnabled\"\n" +
    "                data-dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "                data-selected-item=\"this\"\n" +
    "                class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/cmsLinkToSelect/cmsLinkToSelectTemplate.html',
    "<se-dropdown data-field=\"ctrl.field\"\n" +
    "    data-qualifier=\"ctrl.qualifier\"\n" +
    "    data-model=\"ctrl.model\"\n" +
    "    data-id=\"ctrl.id\"></se-dropdown>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/cmsLinkToSelect/cmsLinkToSelectWrapperTemplate.html',
    "<cms-link-to-select data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></cms-link-to-select>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/linkToggle/linkToggleTemplate.html',
    "<div class=\"se-link-toggle\">\n" +
    "\n" +
    "    <div class=\"radio se-link-toggle__radio\">\n" +
    "        <input type=\"radio\"\n" +
    "            name=\"linktoggle\"\n" +
    "            id=\"external-link\"\n" +
    "            data-ng-model=\"$ctrl.model.linkToggle.external\"\n" +
    "            data-ng-value=\"true\"\n" +
    "            data-ng-change=\"$ctrl.emptyUrlLink()\" />\n" +
    "        <label class=\"control-label\"\n" +
    "            for=\"external-link\">{{ 'se.editor.linkto.external.label' | translate}}</label>\n" +
    "    </div>\n" +
    "    <div class=\"radio se-link-toggle__radio\">\n" +
    "        <input type=\"radio\"\n" +
    "            name=\"linktoggle\"\n" +
    "            id=\"internal-link\"\n" +
    "            data-ng-model=\"$ctrl.model.linkToggle.external\"\n" +
    "            data-ng-value=\"false\"\n" +
    "            data-ng-change=\"$ctrl.emptyUrlLink()\" />\n" +
    "        <label class=\"control-label\"\n" +
    "            for=\"internal-link\">{{ 'se.editor.linkto.internal.label' | translate}}</label>\n" +
    "    </div>\n" +
    "\n" +
    "    <input type=\"text\"\n" +
    "        id=\"urlLink\"\n" +
    "        name=\"urlLink\"\n" +
    "        data-ng-model=\"$ctrl.model.linkToggle.urlLink\"\n" +
    "        data-ng-class=\"{'has-error':$ctrl.field.hasErrors}\"\n" +
    "        class=\"form-control\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/linkToggle/linkToggleWrapperTemplate.html',
    "<link-toggle data-field=\"field\"\n" +
    "    data-model=\"model\">\n" +
    "</link-toggle>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/errorsList/seErrorsListTemplate.html',
    "<div class=\"field-errors\">\n" +
    "    <div data-ng-repeat=\"error in ctrl.getSubjectErrors()\">{{ error.message | translate }}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/fileSelector/seFileSelectorTemplate.html',
    "<div class=\"seFileSelector {{::ctrl.customClass}}\">\n" +
    "    <label class=\" btn__fileSelector\"\n" +
    "        data-ng-show=\"!ctrl.disabled\">\n" +
    "        <img class=\"img-upload\"\n" +
    "            data-ng-src=\"{{::ctrl.uploadIcon}}\" />\n" +
    "        <p class=\"label label__fileUpload label__fileUpload-link\">{{::ctrl.labelI18nKey | translate}}</p>\n" +
    "        <input type=\"file\"\n" +
    "            class=\"hide\"\n" +
    "            accept=\"{{ctrl.buildAcceptedFileTypesList()}}\">\n" +
    "    </label>\n" +
    "    <label class=\" btn__fileSelector\"\n" +
    "        data-ng-show=\"ctrl.disabled\">\n" +
    "        <img class=\"img-upload\"\n" +
    "            data-ng-src=\"{{::ctrl.uploadIcon}}\" />\n" +
    "        <p class=\"label label__fileUpload\">{{::ctrl.labelI18nKey | translate}}</p>\n" +
    "    </label>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesCondensedTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    data-popover-append-to-body=\"true\"\n" +
    "    data-popover-placement=\"bottom\"\n" +
    "    data-popover-trigger=\"'mouseenter'\"\n" +
    "    tabindex=\"0\"\n" +
    "    class=\"media-container-advanced-information\">\n" +
    "    <img class=\"media-advanced-information--image\"\n" +
    "        data-ng-src=\"{{ctrl.advInfoIcon}}\"\n" +
    "        alt=\"{{ctrl.i18nKeys.INFORMATION | translate}}\"\n" +
    "        title=\"{{ctrl.i18nKeys.INFORMATION | translate}}\">\n" +
    "    <p class=\"media-advanced-information--p\">{{ctrl.i18nKeys.INFORMATION | translate}}</p>\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesContentTemplate.html',
    "<div class=\"se-adv-media-info\">\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-description\"\n" +
    "        data-ng-if=\"ctrl.description\">\n" +
    "        {{ctrl.i18nKeys.DESCRIPTION | translate}}: {{ctrl.description}}\n" +
    "    </span>\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-code\">\n" +
    "        {{ctrl.i18nKeys.CODE | translate}}: {{ctrl.code}}\n" +
    "    </span>\n" +
    "    <span class=\"se-adv-media-info--data advanced-information-alt-text\"\n" +
    "        data-ng-if=\"ctrl.altText\">\n" +
    "        {{ctrl.i18nKeys.ALT_TEXT | translate}}: {{ctrl.altText}}\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaAdvancedProperties/seMediaAdvancedPropertiesTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    popover-append-to-body=\"true\"\n" +
    "    popover-placement=\"left\"\n" +
    "    popover-trigger=\"click\"\n" +
    "    tabindex=\"0\"\n" +
    "    class=\"media-advanced-information btn btn-subordinate\">\n" +
    "    {{ctrl.i18nKeys.INFORMATION | translate}}\n" +
    "    <img class=\"media-selector--preview--icon\" data-ng-src=\"{{ctrl.advInfoIcon}}\">\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaContainerField/seMediaContainerFieldTemplate.html',
    "<div class=\"se-media-container-field\">\n" +
    "    <div class=\"row\">\n" +
    "        <se-media-format class=\"col-sm-6 col-md-4 col-lg-3 se-media-container-cell\"\n" +
    "            ng-repeat=\"option in ctrl.field.options\"\n" +
    "            data-media-uuid=\"ctrl.model[ctrl.qualifier][option.id]\"\n" +
    "            data-is-under-edit=\"ctrl.isFormatUnderEdit(option.id)\"\n" +
    "            data-media-format=\"option.id\"\n" +
    "            data-errors=\"ctrl.field.messages\"\n" +
    "            data-on-file-select=\"ctrl.fileSelected(files, option.id)\"\n" +
    "            data-on-delete=\"ctrl.imageDeleted(option.id)\">\n" +
    "        </se-media-format>\n" +
    "\n" +
    "    </div>\n" +
    "    <se-media-upload-form data-ng-if=\"ctrl.image.file\"\n" +
    "        data-image=\"ctrl.image\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-on-upload-callback=\"ctrl.imageUploaded(uuid)\"\n" +
    "        data-on-cancel-callback=\"ctrl.resetImage()\"\n" +
    "        data-on-select-callback=\"ctrl.fileSelected(files)\">\n" +
    "    </se-media-upload-form>\n" +
    "    <se-errors-list data-errors=\"ctrl.fileErrors\"></se-errors-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaField/seMediaFieldTemplate.html',
    "<div class=\"media-field\">\n" +
    "    <se-media-selector ng-if=\"!ctrl.image.file\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-editor=\"ctrl.editor\"\n" +
    "        data-qualifier=\"ctrl.qualifier\">\n" +
    "    </se-media-selector>\n" +
    "    <se-file-selector ng-if=\"ctrl.showFileSelector()\"\n" +
    "        data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD_IMAGE_TO_LIBRARY\"\n" +
    "        data-accepted-file-types=\"ctrl.acceptedFileTypes\"\n" +
    "        data-on-file-select=\"ctrl.fileSelected(files)\"\n" +
    "        data-upload-icon=\"ctrl.uploadIconUrl\"\n" +
    "        data-image-root=\"ctrl.imageRoot\"></se-file-selector>\n" +
    "    <se-media-upload-form ng-if=\"ctrl.image.file\"\n" +
    "        data-image=\"ctrl.image\"\n" +
    "        data-field=\"ctrl.field\"\n" +
    "        data-on-upload-callback=\"ctrl.imageUploaded(uuid)\"\n" +
    "        data-on-cancel-callback=\"ctrl.resetImage()\"\n" +
    "        data-on-select-callback=\"ctrl.fileSelected(files)\"></se-media-upload-form>\n" +
    "    <se-errors-list ng-if=\"ctrl.fileErrors.length > 0\"\n" +
    "        data-errors=\"ctrl.fileErrors\"></se-errors-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaFormat/seMediaFormatTemplate.html',
    "<div class=\"{{ctrl.mediaFormat}} se-media-format\">\n" +
    "    <div class=\"se-media-format-screentype\">\n" +
    "        {{ctrl.mediaFormatI18NKey | translate}}\n" +
    "    </div>\n" +
    "    <!-- when the image is already uploaded -->\n" +
    "    <div class=\"media-present\"\n" +
    "        data-ng-if=\"ctrl.isMediaPreviewEnabled()\">\n" +
    "        <div class=\"media-present-preview\">\n" +
    "            <se-media-preview data-image-url=\"ctrl.media.url\"></se-media-preview>\n" +
    "            <div class=\"se-media-preview-image-wrapper  se-media-format-image-wrapper\">\n" +
    "                <img class=\"thumbnail thumbnail--image-preview\"\n" +
    "                    data-ng-src=\"{{ctrl.media.url}}\">\n" +
    "            </div>\n" +
    "            <span class=\"media-preview-code se-media-format--code\">{{ctrl.media.code}}</span>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <se-media-advanced-properties-condensed data-adv-info-icon=\"ctrl.advInfoIconUrl\"\n" +
    "            data-code=\"ctrl.media.code\"\n" +
    "            data-description=\"ctrl.media.description\"\n" +
    "            data-alt-text=\"ctrl.media.altText\"></se-media-advanced-properties-condensed>\n" +
    "\n" +
    "        <se-file-selector data-custom-class=\" 'media-format-present-replace' \"\n" +
    "            data-label-i18n-key=\"ctrl.i18nKeys.REPLACE \"\n" +
    "            data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "            data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "            data-upload-icon=\"ctrl.replaceIconUrl \"\n" +
    "            data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "\n" +
    "        <a class=\"media-selector--preview__left remove-image btn btn-subordinate \"\n" +
    "            data-ng-click=\"ctrl.onDelete({format: ctrl.mediaFormat}) \">\n" +
    "            <img class=\"media-selector--preview--icon \"\n" +
    "                data-ng-src=\"{{ctrl.deleteIconUrl}} \"\n" +
    "                alt=\"{{ctrl.i18nKeys.REMOVE | translate}} \" />\n" +
    "            <p class=\"media-selector--preview__left--p media-selector--preview__left--p__error \">{{ctrl.i18nKeys.REMOVE | translate}}</p>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- when the image is not yet uploaded -->\n" +
    "    <div class=\"media-absent \"\n" +
    "        data-ng-if=\"ctrl.isMediaEditEnabled()\">\n" +
    "        <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD \"\n" +
    "            data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "            data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "            data-upload-icon=\"ctrl.uploadIconUrl \"\n" +
    "            data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "    </div>\n" +
    "\n" +
    "    <!-- when the image is under edit -->\n" +
    "    <div data-ng-if=\"ctrl.isUnderEdit \"\n" +
    "        class=\"media-under-edit-parent\">\n" +
    "        <div class=\"media-is-under-edit \">\n" +
    "            <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD \"\n" +
    "                data-disabled=\"true\"\n" +
    "                data-custom-class=\" 'file-selector-disabled' \"\n" +
    "                data-accepted-file-types=\"ctrl.acceptedFileTypes \"\n" +
    "                data-on-file-select=\"ctrl.onFileSelect({files: files, format: ctrl.mediaFormat}) \"\n" +
    "                data-upload-icon=\"ctrl.uploadIconDisabledUrl \"\n" +
    "                data-image-root=\"ctrl.imageRoot \"></se-file-selector>\n" +
    "        </div>\n" +
    "        <span class=\"media-preview-under-edit \">{{ctrl.i18nKeys.UNDER_EDIT | translate}}</span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"error-input help-block \"\n" +
    "        data-ng-repeat=\"error in ctrl.getErrors() \">\n" +
    "        {{error}}\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaPreview/seMediaPreviewContentTemplate.html',
    "<img class=\"preview-image\"\n" +
    "    data-ng-src=\"{{ctrl.imageUrl}}\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaPreview/seMediaPreviewTemplate.html',
    "<a data-uib-popover-template=\"ctrl.contentUrl\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\"\n" +
    "    popover-append-to-body=\"true\"\n" +
    "    popover-placement=\"right\"\n" +
    "    popover-trigger=\"'outsideClick'\"\n" +
    "    tabindex=\"1\"\n" +
    "    class=\"media-preview\">\n" +
    "    <span class=\"hyicon hyicon-search media-preview-icon\"></span>\n" +
    "</a>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaPrinterTemplate.html',
    "<div class=\"media-selector--preview\"\n" +
    "    data-ng-if=\"selected\">\n" +
    "    <div class=\"row\">\n" +
    "        <span class=\"col-xs-6\">\n" +
    "            <se-media-preview data-image-url=\"item.url\"></se-media-preview>{{printer.imagePreviewUrl}}\n" +
    "            <div class=\"se-media-preview-image-wrapper\">\n" +
    "                <img class=\"thumbnail thumbnail--image-preview\"\n" +
    "                    data-ng-src={{item.url}}>\n" +
    "            </div>\n" +
    "            <span class=\"media-preview-code se-media-printer--code\">{{item.code}}</span>\n" +
    "        </span>\n" +
    "        <span class=\"col-xs-6 media-selector--preview--2\">\n" +
    "            <se-media-advanced-properties class=\"media-selector--preview__right\"\n" +
    "                data-code=\"item.code\"\n" +
    "                data-description=\"item.description\"\n" +
    "                data-alt-text=\"item.altText\"\n" +
    "                data-adv-info-icon=\"printer.advInfoIcon\"></se-media-advanced-properties>\n" +
    "\n" +
    "            <a class=\"media-selector--preview__right replace-image btn btn-subordinate\"\n" +
    "                data-ng-click=\"ySelect.clear($select, $event)\">\n" +
    "                <p class=\"media-selector--preview__right--p\"\n" +
    "                    data-translate=\"se.upload.image.replace\"></p>\n" +
    "                <img class=\"media-selector--preview--icon\"\n" +
    "                    data-ng-src=\"{{printer.replaceIcon}}\" />\n" +
    "            </a>\n" +
    "            <a class=\"media-selector--preview__right remove-image btn btn-subordinate\"\n" +
    "                data-ng-click=\"ySelect.clear($select, $event)\">\n" +
    "                <p class=\"media-selector--preview__right--p\"\n" +
    "                    data-translate=\"se.media.format.remove\"></p>\n" +
    "                <img class=\"media-selector--preview--icon\"\n" +
    "                    data-ng-src=\"{{printer.deleteIcon}}\" />\n" +
    "            </a>\n" +
    "        </span>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"ySEMediaSearch\"\n" +
    "    data-ng-if=\"!selected\">\n" +
    "    <div class=\"row ySEMediaSearch-row\">\n" +
    "        <div class=\"ySEMediaSearchName col-xs-8\">{{item.code}}</div>\n" +
    "        <div class=\"ySEMediaSearchImage col-xs-4\">\n" +
    "            <img data-ng-src={{item.url}}\n" +
    "                class=\"pull-right\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaPrinterWrapperTemplate.html',
    "<se-media-printer data-item=\"item\"\n" +
    "    data-selected=\"selected\">"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaSelector/seMediaSelectorTemplate.html',
    "<div class=\"media-selector\">\n" +
    "    <y-select data-ng-disabled=\"!ctrl.field.editable\"\n" +
    "        data-id=\"{{ctrl.field.qualifier}}\"\n" +
    "        data-placeholder=\"ctrl.dropdownProperties.placeHolderI18nKey\"\n" +
    "        data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "        data-fetch-strategy=\"ctrl.fetchStrategy\"\n" +
    "        data-item-template=\"ctrl.mediaTemplate\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaUploadForm/seMediaUploadFieldTemplate.html',
    "<div class=\"form-control se-mu--fileinfo--wrapper\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"se-mu--fileinfo--wrapper-col1 col-xs-10\">\n" +
    "            <input type=\"text\"\n" +
    "                data-ng-class=\"{'se-mu--fileinfo--field--input': true, 'se-mu--fileinfo--field__error': ctrl.error}\"\n" +
    "                class=\"se-mu--fileinfo--field--input\"\n" +
    "                name=\"{{ctrl.field}}\"\n" +
    "                data-ng-model=\"ctrl.model[ctrl.field]\">\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"se-mu--fileinfo--wrapper-col2 col-xs-2\">\n" +
    "            <img ng-src=\"{{ctrl.assetsRoot}}/images/edit_icon.png\"\n" +
    "                class=\"se-mu--fileinfo--field--icon \"\n" +
    "                data-ng-if=\"ctrl.displayImage && !ctrl.error\" />\n" +
    "            <img ng-src=\"{{ctrl.assetsRoot}}/images/edit_icon_error.png\"\n" +
    "                class=\"se-mu--fileinfo--field--icon__error \"\n" +
    "                data-ng-if=\"ctrl.error\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/components/mediaUploadForm/seMediaUploadFormTemplate.html',
    "<div class=\"se-media-upload-form\">\n" +
    "\n" +
    "    <div class=\"container-fluid se-media-upload-form--toolbar\">\n" +
    "        <div class=\"navbar-left se-media-upload-form--toolbar--title\">\n" +
    "            <h4 class=\"se-media-upload-form--toolbar--title--h4\">{{ctrl.i18nKeys.UPLOAD_IMAGE_TO_LIBRARY | translate}}</h4>\n" +
    "        </div>\n" +
    "        <div class=\"navbar-right se-media-upload-form--toolbar--buttons\">\n" +
    "            <div class=\"y-toolbar\">\n" +
    "                <div class=\"y-toolbar-cell\">\n" +
    "                    <button class=\"btn btn-subordinate btn-lg navbar-btn se-media-upload-btn__cancel\"\n" +
    "                        type=\"button\"\n" +
    "                        data-ng-click=\"ctrl.onCancel()\">{{ctrl.i18nKeys.UPLOAD_IMAGE_CANCEL | translate}}</button>\n" +
    "                </div>\n" +
    "                <div class=\"y-toolbar-cell\">\n" +
    "                    <button class=\"btn btn-default btn-lg navbar-btn se-media-upload-btn__submit\"\n" +
    "                        type=\"button\"\n" +
    "                        data-ng-click=\"ctrl.onMediaUploadSubmit()\">{{ctrl.i18nKeys.UPLOAD_IMAGE_SUBMIT | translate}}</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"container-fluid se-media-upload--filename\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"se-media-upload--filename-col1 col-xs-10\">\n" +
    "                <img ng-src=\"{{assetsRoot}}/images/image_placeholder.png\">\n" +
    "                <div class=\"se-media-upload--fn--name\">{{ctrl.getTruncatedName()}}</div>\n" +
    "            </div>\n" +
    "            <div class=\"se-media-upload--filename-col2 col-xs-2\">\n" +
    "                <se-file-selector data-label-i18n-key=\"ctrl.i18nKeys.UPLOAD_IMAGE_REPLACE\"\n" +
    "                    data-accepted-file-types=\"ctrl.acceptedFileTypes\"\n" +
    "                    data-on-file-select=\"ctrl.onSelectCallback({files: files})\"></se-file-selector>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <form>\n" +
    "        <div class=\"se-media-upload--fileinfo\">\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-description\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('description'), 'se-media-upload--fileinfo--label': true }\">{{ctrl.i18nKeys.DESCRIPTION | translate}}</label>\n" +
    "                <se-media-upload-field data-field=\"'description'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-description\"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('description')\">{{error}}</span>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-code\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('code')}\">{{ctrl.i18nKeys.CODE | translate}}*</label>\n" +
    "                <se-media-upload-field data-error=\"ctrl.hasError('code')\"\n" +
    "                    data-field=\"'code'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-code\"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('code')\">{{error}}</span>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"se-media-upload--fileinfo--field form-group\">\n" +
    "                <label name=\"label-alt-text\"\n" +
    "                    data-ng-class=\"{ 'se-media-upload-has-error': ctrl.hasError('altText') }\">{{ctrl.i18nKeys.ALT_TEXT | translate}}</label>\n" +
    "                <se-media-upload-field data-field=\"'altText'\"\n" +
    "                    data-model=\"ctrl.imageParameters\"></se-media-upload-field>\n" +
    "                <span class=\"upload-field-error upload-field-error-alt-text \"\n" +
    "                    data-ng-repeat=\"error in ctrl.getErrorsForField('altText')\">{{error}}</span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </form>\n" +
    "    <span data-ng-if=\"ctrl.isUploading\"\n" +
    "        class=\"upload-image-in-progress\">\n" +
    "        <!--<img src=\"static-resources/images/spinner.png\"> {{ctrl.i18nKeys.UPLOADING | translate}}-->\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/templates/mediaContainerTemplate.html',
    "<se-media-container-field data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"\n" +
    "    data-qualifier=\"qualifier\"></se-media-container-field>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/media/templates/mediaTemplate.html',
    "<se-media-field data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"\n" +
    "    data-qualifier=\"qualifier\"></se-media-field>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/breadcrumb/seBreadcrumbTemplate.html',
    "<div class=\"ySEBreadcrumb\">\n" +
    "    <div data-ng-repeat=\"node in bc.breadcrumb\"\n" +
    "        class=\"ySEBreadcrumbNode\">\n" +
    "        <div class=\"ySEBreadcrumbInfo\"\n" +
    "            data-ng-class=\"{'ySEBreadcrumbInfo__last':$last}\">\n" +
    "            <span class=\"yNodeLevel\">{{node.formattedLevel | translate:node}}</span>\n" +
    "            <span class=\"yNodeName\">{{node.name}}</span>\n" +
    "        </div>\n" +
    "        <div class=\"ySEBreadcrumbDivider\"\n" +
    "            data-ng-if=\"$index < bc.breadcrumb.length - 1\">\n" +
    "            <img data-ng-src=\"{{bc.arrowIconUrl}}\"\n" +
    "                alt=\"\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodePicker/navigationNodePickerRenderTemplate.html',
    "<div class=\"col-sm-6 pull-right tree-node yNavigationPickerRenderer\"\n" +
    "    data-ng-click=\"ctrl.pick(this)\">\n" +
    "    <a data-translate=\"se.cms.navigationcomponent.management.node.selection.select.action\"\n" +
    "        class=\"btn btn-link yNavigationPickerRenderer__btn\" />\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodePicker/seNavigationNodePickerTemplate.html',
    "<div class=\"categoryTable\">\n" +
    "\n" +
    "    <div class=\"tablehead clearfix hidden-xs ySENavigationTree-head\">\n" +
    "        <div data-translate=\"se.ytree.template.header.name\"\n" +
    "            class=\"col-md-offset-1 col-sm-5\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ytree data-node-uri=\"ctrl.nodeURI\"\n" +
    "        data-root-node-uid=\"ctrl.rootNodeUid\"\n" +
    "        data-node-template-url=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-node-actions=\"ctrl.actions\" />\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/components/navigationNodeSelector/seNavigationNodeSelectorTemplate.html',
    "<div data-ng-if=\"nav.isReady()\">\n" +
    "    <div data-ng-if=\"nav.model[nav.qualifier]\"\n" +
    "        class=\"se-navigation-mode\">\n" +
    "\n" +
    "        <div class=\"se-navigation--node\">\n" +
    "            <se-breadcrumb class=\"se-navigation--node-breadcrumb\"\n" +
    "                data-node-uuid=\"nav.model[nav.qualifier]\"\n" +
    "                data-uri-context=\"nav.uriContext\"></se-breadcrumb>\n" +
    "            <div class=\"se-navigation--node-button\">\n" +
    "                <button class=\"btn btn-link btn-block se-navigation--button\"\n" +
    "                    data-ng-click=\"nav.remove($event)\">\n" +
    "                    <p data-translate=\"se.cms.navigationcomponent.management.node.selection.remove.action\"></p>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <navigation-editor data-uri-context=\"nav.uriContext\"\n" +
    "            data-read-only=\"true\"\n" +
    "            data-root-node-uid=\"nav.nodeUid\"></navigation-editor>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!nav.model[nav.qualifier]\">\n" +
    "\n" +
    "        <label data-translate=\"se.cms.navigationcomponent.management.node.selection.invite.action\"></label>\n" +
    "        <se-navigation-picker data-uri-context=\"nav.uriContext\"\n" +
    "            data-model=\"nav.model\"\n" +
    "            data-qualifier=\"nav.qualifier\"></se-navigation-picker>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/navigationNode/templates/navigationNodeSelectorWrapperTemplate.html',
    "<se-navigation-node-selector data-field=\"field\"\n" +
    "    data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"></se-navigation-node-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/singleActiveCatalogAwareSelector/singleActiveCatalogAwareItemSelector/singleActiveCatalogAwareItemSelectorTemplate.html',
    "<!-- Product catalog selector -->\n" +
    "<div id=\"product-catalog\">\n" +
    "    <label class=\"control-label\"\n" +
    "        data-ng-if=\"ctrl.catalogs.length === 1\">{{ctrl.catalogName | l10n}}</label>\n" +
    "    <se-dropdown data-ng-if=\"ctrl.catalogs.length > 1\"\n" +
    "        data-field=\"ctrl.productCatalogField\"\n" +
    "        data-qualifier=\"'productCatalog'\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-id=\"se-catalog-selector-dropdown\"></se-dropdown>\n" +
    "</div>\n" +
    "\n" +
    "<!-- Item selector -->\n" +
    "<div>\n" +
    "    <label class=\"control-label\">{{ctrl.mainDropDownI18nKey | translate}}</label>\n" +
    "    <se-dropdown data-field=\"ctrl.field\"\n" +
    "        data-qualifier=\"ctrl.qualifier\"\n" +
    "        data-model=\"ctrl.model\"\n" +
    "        data-id=\"se-items-selector-dropdown\"></se-dropdown>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/genericEditor/singleActiveCatalogAwareSelector/singleActiveCatalogAwareItemSelector/singleActiveCatalogAwareItemSelectorWrapperTemplate.html',
    "<single-active-catalog-aware-item-selector data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></single-active-catalog-aware-item-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/itemManagement/itemManagementTemplate.html',
    "<div>\n" +
    "    <generic-editor data-id=\"$ctrl.editorId\"\n" +
    "        data-smartedit-component-id=\"$ctrl.itemId\"\n" +
    "        data-smartedit-component-type=\"$ctrl.componentType\"\n" +
    "        data-structure-api=\"$ctrl.structureApi\"\n" +
    "        data-content=\"$ctrl.item\"\n" +
    "        data-content-api=\"$ctrl.contentApi\"\n" +
    "        data-is-dirty=\"$ctrl.isDirtyInternal\"\n" +
    "        data-submit=\"$ctrl.submit\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-reset=\"$ctrl.reset\">\n" +
    "    </generic-editor>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationEditorLinkTemplate.html',
    "<div class=\"nav-management-link-container\">\n" +
    "    <a class=\"nav-management-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-href=\"#!/navigations/{{$ctrl.siteId}}/{{$ctrl.catalog.catalogId}}/{{$ctrl.catalogVersion.version}}\"\n" +
    "        data-translate=\"se.cms.cataloginfo.navigationmanagement\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationEditorTemplate.html',
    "<div class=\"ySEAdd-Nav-button\"\n" +
    "    data-ng-if=\"!nav.readOnly\">\n" +
    "    <button class=\"y-add-btn\"\n" +
    "        data-ng-click=\"nav.actions.addTopLevelNode()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>\n" +
    "        <span data-translate=\"se.cms.navigationmanagement.add.top.level.node\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "<div class=\"ySEAdd-Nav-tree categoryTable\">\n" +
    "\n" +
    "    <div class=\"tablehead clearfix hidden-xs ySENavigationTree-head\">\n" +
    "        <div data-translate=\"se.ytree.template.header.name\"\n" +
    "            class=\"col-md-offset-1 col-sm-5\"></div>\n" +
    "        <div data-translate=\"se.ytree.template.header.type\"\n" +
    "            class=\"col-sm-6\"></div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ytree data-node-uri=\"nav.nodeURI\"\n" +
    "        data-root-node-uid=\"nav.rootNodeUid\"\n" +
    "        data-node-template-url=\"nav.nodeTemplateUrl\"\n" +
    "        data-node-actions=\"nav.actions\"\n" +
    "        data-drag-options=\"nav.dragOptions\"></ytree>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationNodeRenderTemplate.html',
    "<div class=\"col-sm-1 pull-right dropdown tree-node ySENodeTemplate\"\n" +
    "    data-ng-show=\"!ctrl.isReadOnly()\">\n" +
    "    <y-drop-down-menu dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "        selected-item=\"this\"\n" +
    "        class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "</div>\n" +
    "<div class=\"col-sm-5 pull-right tree-node ySENodeTemplateTree\"> {{node.itemType || 'se.cms.navigationmanagement.navnode.objecttype.node' | translate}} </div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationEditor/navigationTemplate.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"navigationEditorWrapper\"\n" +
    "    ng-if=\"nav.readOnly !== undefined\">\n" +
    "    <div class=\"navEditorHeaderWrapper\">\n" +
    "        <h1 class=\"ySEPage-list-title\"\n" +
    "            data-translate='se.cms.navigationmanagement.title'></h1>\n" +
    "        <h4 class=\"ySEPage-list-label\">{{nav.catalogName | l10n}} - {{nav.catalogVersion}}</h4>\n" +
    "    </div>\n" +
    "    <navigation-editor data-uri-context=\"nav.uriContext\"\n" +
    "        data-read-only=\"nav.readOnly\"></navigation-editor>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/entrySearchSelectorDropdownTemplate.html',
    "<y-select data-ng-if=\"ctrl.initialized\"\n" +
    "    data-id=\"{{ctrl.field.qualifier}}\"\n" +
    "    data-placeholder=\"ctrl.dropdownProperties.placeHolderI18nKey\"\n" +
    "    data-controls=\"'true'\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-fetch-strategy=\"ctrl.fetchStrategy\"\n" +
    "    data-reset=\"ctrl.reset\"\n" +
    "    data-item-template=\"ctrl.dropdownProperties.templateUrl\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/entrySearchSelectorTemplate.html',
    "<entry-search-selector data-model=\"model\"\n" +
    "    data-id=\"editor.id\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-field=\"field\"\n" +
    "    data-editor=\"editor\"></entry-search-selector>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/handlerTemplates/itemSearchHandlerTemplate.html',
    "<div class=\"ySENavigationItemUid\">{{item.id}}</div>\n" +
    "<div class=\"ySENavigationItemTypeCode\">{{item.typeCode}}</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/entrySearchSelector/handlerTemplates/mediaSearchHandlerTemplate.html',
    "<div class=\"ySENavigationMediaSearch\">\n" +
    "    <div class=\"row ySENavigationMediaSearch-row\">\n" +
    "        <div class=\"ySENavigationMediaSearchName\"\n" +
    "            title=\"{{item.code}}\">{{item.code}}</div>\n" +
    "        <div class=\"ySENavigationMediaSearchImage\">\n" +
    "            <img data-ng-src={{item.downloadUrl}}\n" +
    "                class=\"pull-right\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorAttributesTemplate.html',
    "<div class=\"ySENavigationNodeEditorAttributes\">\n" +
    "    <generic-editor data-ng-if=\"ctrl.isContentLoaded\"\n" +
    "        data-id=\"ctrl.tabId\"\n" +
    "        data-smartedit-component-id=\"ctrl.nodeUid\"\n" +
    "        data-smartedit-component-type=\"ctrl.typeCode\"\n" +
    "        data-structure=\"ctrl.tabStructure\"\n" +
    "        data-content-api=\"ctrl.contentApi\"\n" +
    "        data-content=\"ctrl.content\"\n" +
    "        data-uri-context=\"ctrl.uriContext\"\n" +
    "        data-submit=\"ctrl.submit\"\n" +
    "        data-reset=\"ctrl.reset\"\n" +
    "        data-is-dirty=\"ctrl.isDirty\"\n" +
    "        data-update-callback=\"ctrl.updateCallback\">\n" +
    "    </generic-editor>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorCreateEntryTemplate.html',
    "<div class=\"ySENavigationNodeEditorAddEntry\">\n" +
    "    <div data-ng-if=\"!ctrl.displayEditor\"\n" +
    "        class=\"ySEAdd-node-button\">\n" +
    "        <button id=\"navigation-node-editor-add-entry\"\n" +
    "            class=\"y-add-btn\"\n" +
    "            data-ng-click=\"ctrl.addNewEntry()\">\n" +
    "            <span class=\"hyicon hyicon-add\"></span>\n" +
    "            <span data-translate=\"se.cms.navigationmanagement.navnode.node.entry.add.new.message\"></span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.displayEditor\"\n" +
    "        class=\"ySEEntryData\">\n" +
    "        <generic-editor data-id=\"ctrl.tabId\"\n" +
    "            data-smartedit-component-type=\"ctrl.entryTypeCode\"\n" +
    "            data-structure=\"ctrl.tabStructure\"\n" +
    "            data-uri-context=\"ctrl.uriContext\"\n" +
    "            data-content=\"ctrl.entry\"\n" +
    "            data-is-dirty=\"ctrl.isDirty\"\n" +
    "            data-is-valid=\"ctrl.isValid\"\n" +
    "            data-submit=\"ctrl.submit\"\n" +
    "            data-reset=\"ctrl.reset\">\n" +
    "        </generic-editor>\n" +
    "\n" +
    "\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"ySEEntryData--buttons\">\n" +
    "                <button id=\"navigation-node-editor-save-entry\"\n" +
    "                    class=\"btn btn-default btn-lg\"\n" +
    "                    data-ng-click=\"ctrl.saveEntry()\"\n" +
    "                    data-ng-disabled=\"ctrl.isDisabled()\">\n" +
    "                    <span data-ng-if=\"ctrl.isNewEntry()\"\n" +
    "                        data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.add\"></span>\n" +
    "                    <span data-ng-if=\"!ctrl.isNewEntry()\"\n" +
    "                        data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.update\"></span>\n" +
    "                </button>\n" +
    "                <button id=\"navigation-node-editor-cancel\"\n" +
    "                    class=\"btn btn-subordinate btn-lg\"\n" +
    "                    data-ng-click=\"ctrl.cancelEntry()\"\n" +
    "                    data-translate=\"se.cms.navigationmanagement.navnode.node.entry.button.cancel\"></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorComponents/navigationNodeEditorEntryListTemplate.html',
    "<div class=\"ySENavigationNodeEditorEntryList\">\n" +
    "    <div>\n" +
    "        <label data-translate=\"se.cms.navigationmanagement.navnode.node.entries\"></label>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-repeat=\"entry in ctrl.entries track by $index\"\n" +
    "        class=\"nav-node-editor-entry-item\">\n" +
    "        <div class=\"row-fluid\">\n" +
    "            <div class=\"col-xs-10 nav-node-editor-entry-item__text\"\n" +
    "                data-ng-click=\"ctrl.onSelect(entry)\">\n" +
    "                <div class=\"nav-node-editor-entry-item__name\">\n" +
    "                    <span class=\"ng-class:{'error-input':ctrl.isInError(entry)}\">{{entry.title | l10n }}</span>\n" +
    "                </div>\n" +
    "                <div class=\"nav-node-editor-entry-item__type\">\n" +
    "                    <span class=\"ng-class:{'error-input':ctrl.isInError(entry)}\">{{entry.itemType }}</span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 nav-node-editor-entry-item__menu\">\n" +
    "                <y-drop-down-menu dropdown-items=\"ctrl.dropdownItems\"\n" +
    "                    selected-item=\"entry\"\n" +
    "                    class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.entries.length === 0\">{{'se.cms.navigationmanagement.node.noentry.message'| translate}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorModalTemplate.html',
    "<navigation-node-editor data-submit=\"modalController.submit\"\n" +
    "    data-reset=\"modalController.reset\"\n" +
    "    data-is-dirty=\"modalController.isDirty\"\n" +
    "    data-uri-context=\"modalController.uriContext\"\n" +
    "    data-node-uid=\"modalController.target.nodeUid\"\n" +
    "    data-parent-uid=\"modalController.target.parentUid\"\n" +
    "    data-entry-index=\"modalController.target.entryIndex\">\n" +
    "</navigation-node-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/navigation/navigationNodeEditor/navigationNodeEditorTemplate.html',
    "<div class=\"ySENavigationNodeEditor\">\n" +
    "    <se-breadcrumb data-node-uid=\"ctrl.nodeUid\"\n" +
    "        data-uri-context=\"ctrl.uriContext\"\n" +
    "        data-ng-if=\"ctrl.nodeUid\"></se-breadcrumb>\n" +
    "    <div class=\"row\">\n" +
    "        <navigation-node-editor-attributes data-submit=\"ctrl.submit\"\n" +
    "            data-reset=\"ctrl.reset\"\n" +
    "            data-is-dirty=\"ctrl.isDirty\"\n" +
    "            data-node-uid=\"ctrl.nodeUid\"\n" +
    "            data-parent-uid=\"ctrl.parentUid\"\n" +
    "            data-uri-context=\"ctrl.uriContext\" />\n" +
    "    </div>\n" +
    "    <div class=\"ySENavigationDivider\"></div>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-sm-6 ySENavigationNodeEditor__nodelist\">\n" +
    "            <navigation-node-editor-entry-list data-navigation-node-entry-data=\"ctrl.navigationNodeEntryData\"\n" +
    "                data-node-uid=\"ctrl.nodeUid\"\n" +
    "                data-entry-index=\"ctrl.entryIndex\"\n" +
    "                data-uri-context=\"ctrl.uriContext\" />\n" +
    "        </div>\n" +
    "        <div class=\"col-sm-6 ySENavigationNodeEditor__nodedata\">\n" +
    "            <navigation-node-editor-create-entry data-navigation-node-entry-data=\"ctrl.navigationNodeEntryData\"\n" +
    "                data-uri-context=\"ctrl.uriContext\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsMenuToolbarItemTemplate.html',
    "<div class=\"ySEVisibilityMenu\"\n" +
    "    data-ng-if=\"$ctrl.pageIsPrimary !== undefined\">\n" +
    "    <div class=\"ySEVisibilityMenu__header\">\n" +
    "        <restrictions-page-info data-page-id=\"$ctrl.pageId\"\n" +
    "            data-page-name=\"$ctrl.pageName\"\n" +
    "            data-page-type=\"$ctrl.pageType\"\n" +
    "            data-page-is-primary=\"$ctrl.pageIsPrimary\"\n" +
    "            data-associated-primary-page-name=\"$ctrl.associatedPrimaryPageName\">\n" +
    "        </restrictions-page-info>\n" +
    "    </div>\n" +
    "    <div class=\"ySEVisibilityMenu__restrictions\"\n" +
    "        data-ng-if=\"!$ctrl.pageIsPrimary\">\n" +
    "        <restrictions-table data-editable=\"false\"\n" +
    "            data-restrictions=\"$ctrl.restrictions\"\n" +
    "            data-restriction-criteria=\"$ctrl.restrictionCriteria\"\n" +
    "            data-custom-class=\"'ySERestrictionMenu'\">\n" +
    "        </restrictions-table>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsMenuToolbarItemWrapperTemplate.html',
    "<restrictions-menu-toolbar-item></restrictions-menu-toolbar-item>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/pageRestrictionsMenu/pageRestrictionsPageInfoTemplate.html',
    "<div class=\"ySERestrictionsPageInfoContainer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.pageNameLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--page-name\"\n" +
    "                data-ng-bind=\"ctrl.pageName\"></h3>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.pageDisplayConditionsLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--displayconditions-value\">{{ ctrl.displayConditionsI18nKey[ctrl.getDisplayConditionsValue()].value | translate }}</h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div ng-if=\"ctrl.pageIsPrimary\">\n" +
    "        <p class=\"ySERestrictonMenuLabel ySERestrictionsPageInfoContainer--displayconditions-description\">{{ ctrl.displayConditionsI18nKey[ctrl.getDisplayConditionsValue()].description | translate }}</p>\n" +
    "    </div>\n" +
    "    <div class=\"row\"\n" +
    "        ng-if=\"!ctrl.pageIsPrimary\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p class=\"ySERestrictonMenuLabel\">{{ ctrl.associatedPrimaryPageLabelI18nKey | translate }}</p>\n" +
    "            <h3 class=\"restrictionsPageName ySERestrictionsPageInfoContainer--associatedprimarypage-name\"\n" +
    "                data-ng-bind=\"ctrl.associatedPrimaryPageName\"></h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pageRestrictions/restrictionsPageListIcon/restrictionsPageListIconTemplate.html',
    "<span>\n" +
    "    <img class=\"restrictionPageListIcon\"\n" +
    "        data-ng-src=\"{{ctrl.getIconUrl()}}\"\n" +
    "        data-uib-tooltip-placement=\"bottom\"\n" +
    "        data-uib-tooltip=\"{{ ctrl.tooltipI18nKey | translate: ctrl.getInterpolationParameters() }}\" />\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/newPageDisplayCondition/newPageDisplayConditionTemplate.html',
    "<div data-ng-show=\"$ctrl.ready === true\"\n" +
    "    class=\"ySENewPageDisplayCondition\">\n" +
    "\n" +
    "    <div class=\"page-condition-selector-wrapper form-group\">\n" +
    "\n" +
    "        <label for=\"page-condition-selector-id\"\n" +
    "            class=\"control-label\">{{ 'se.cms.page.condition.selection.label' | translate }}</label>\n" +
    "        <ui-select id=\"page-condition-selector-id\"\n" +
    "            data-ng-model=\"$ctrl.conditionSelected\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"false\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.dataChanged()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.conditionSelected.label | translate }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"c in $ctrl.conditions\">\n" +
    "                {{ c.label | translate }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ $ctrl.conditionSelected.description | translate }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.showPrimarySelector()\"\n" +
    "        class=\"page-condition-primary-selector-wrapper form-group\">\n" +
    "        <label for=\"page-condition-primary-selector-id\"\n" +
    "            class=\"control-label\">{{ 'se.cms.page.condition.primary.association.label' | translate }}</label>\n" +
    "        <ui-select id=\"page-condition-primary-selector-id\"\n" +
    "            data-ng-model=\"$ctrl.primarySelected\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"true\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.dataChanged()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.primarySelected.name }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"page in $ctrl.primaryPageChoices | filter:$select.search\">\n" +
    "                {{ page.name }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span data-ng-show=\"$ctrl.primarySelected.label\"\n" +
    "            class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ 'se.cms.page.label.label' | translate }}: {{ $ctrl.primarySelected.label }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectPageTemplate/selectPageTemplateTemplate.html',
    "<div class=\"page-type-step-template\">\n" +
    "    <div class=\"se-input-group page-type-step-template-list-search\">\n" +
    "        <span class=\"se-input-group--addon\">\n" +
    "            <span class=\"hyicon hyicon-search ySEPage-list-search-icon\"></span>\n" +
    "        </span>\n" +
    "        <input type=\"text\"\n" +
    "            class=\"se-input-group--input ySEPage-list-search-input\"\n" +
    "            placeholder=\"{{ 'se.cms.pagewizard.templatestep.searchplaceholder' | translate }}\"\n" +
    "            data-ng-model=\"$ctrl.searchString\"\n" +
    "            name=\"query\" />\n" +
    "        <span data-ng-if=\"$ctrl.searchString\"\n" +
    "            class=\"se-input-group--addon ySESearchIcon\"\n" +
    "            data-ng-click=\"$ctrl.clearSearch()\">\n" +
    "            <span class=\"glyphicon glyphicon-remove-sign\"></span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"page-wizard-list\">\n" +
    "        <div data-ng-repeat=\"template in $ctrl.pageTemplates | templateNameFilter:$ctrl.searchString track by $id(template)\"\n" +
    "            data-ng-class=\"{ 'page-type-step-template__item__selected': $ctrl.isSelected(template)}\"\n" +
    "            class=\"page-type-step-template__item\"\n" +
    "            data-ng-click=\"$ctrl.templateSelected(template)\">\n" +
    "            <span class=\"hyicon hyicon-checked page-type-step-template__item__icon\"></span>\n" +
    "            <div class=\"page-type-step-template__item--info\">\n" +
    "                <div class=\"page-type-step-template__item--info__title\">{{ template.name }}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectPageType/selectPageTypeTemplate.html',
    "<div class=\"page-type-step-template\">\n" +
    "    <div class=\"page-type-step-template__text ySEText\">{{ 'se.cms.addpagewizard.pagetype.description' | translate}}</div>\n" +
    "    <div data-ng-repeat=\"pageType in $ctrl.pageTypes\"\n" +
    "        data-ng-class=\"{ 'page-type-step-template__item__selected': $ctrl.isSelected(pageType)}\"\n" +
    "        class=\"page-type-step-template__item\"\n" +
    "        data-ng-click=\"$ctrl.selectType(pageType)\">\n" +
    "        <span class=\"hyicon hyicon-checked page-type-step-template__item__icon\"></span>\n" +
    "        <div class=\"page-type-step-template__item--info\">\n" +
    "            <div class=\"page-type-step-template__item--info__title\">{{ pageType.name | l10n }}</div>\n" +
    "            <div class=\"page-type-step-template__item--info__description\">{{ pageType.description | l10n }}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/components/selectTargetCatalogVersion/selectTargetCatalogVersionTemplate.html',
    "<div class=\"target-catalog-version-selector-wrapper form-group\">\n" +
    "    <label for=\"target-catalog-version-selector-id\"\n" +
    "        class=\"control-label\"\n" +
    "        data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.label\"></label>\n" +
    "\n" +
    "    <y-select data-ng-if=\"$ctrl.catalogVersions.length\"\n" +
    "        data-id=\"se-catalog-version-selector-dropdown\"\n" +
    "        data-ng-model=\"$ctrl.selectedCatalogVersion\"\n" +
    "        data-fetch-strategy=\"$ctrl.catalogVersionSelectorFetchStrategy\"\n" +
    "        data-on-change=\"$ctrl.onSelectionChanged\"\n" +
    "        data-search-enabled=\"false\">\n" +
    "    </y-select>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameLabel\">\n" +
    "        <span class=\"warning-input help-block\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.label.exists.message\"></span>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameTypeCode\">\n" +
    "        <span class=\"warning-input help-block\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.targetcatalogversion.pagetype.exists.message\"></span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageDisplayConditionStepTemplate.html',
    "<new-page-display-condition data-page-type-code=\"addPageWizardCtl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"addPageWizardCtl.uriContext\"\n" +
    "    data-result-fn=\"addPageWizardCtl.variationResult\">\n" +
    "</new-page-display-condition>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageInfoStepTemplate.html',
    "<span data-ng-if=\"addPageWizardCtl.isPageInfoActive()\">\n" +
    "    <generic-editor data-structure=\"addPageWizardCtl.getPageInfoStructure()\"\n" +
    "        data-content=\"addPageWizardCtl.getPageInfo()\"\n" +
    "        data-submit=\"addPageWizardCtl.callbacks.savePageInfo\"\n" +
    "        data-reset=\"addPageWizardCtl.callbacks.resetPageInfo\"\n" +
    "        data-is-dirty=\"addPageWizardCtl.callbacks.isDirtyPageInfo\"\n" +
    "        data-is-valid=\"addPageWizardCtl.callbacks.isValidPageInfo\">\n" +
    "    </generic-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageRestrictionsStepTemplate.html',
    "<span data-ng-if=\"addPageWizardCtl.isRestrictionsActive()\">\n" +
    "\n" +
    "    <div data-ng-if=\"addPageWizardCtl.getPageInfo().restrictions.length > 1\">\n" +
    "        <page-restrictions-info-message />\n" +
    "    </div>\n" +
    "\n" +
    "    <restrictions-editor data-editable=\"true\"\n" +
    "        data-save-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.save\"\n" +
    "        data-reset-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.reset\"\n" +
    "        data-cancel-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.cancel\"\n" +
    "        data-is-dirty-fn=\"addPageWizardCtl.restrictionsEditorFunctionBindings.isDirty\"\n" +
    "        data-on-restrictions-changed=\"addPageWizardCtl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "        data-get-restriction-types=\"addPageWizardCtl.getRestrictionTypes()\"\n" +
    "        data-get-supported-restriction-types=\"addPageWizardCtl.getSupportedRestrictionTypes()\"\n" +
    "        data-item=\"addPageWizardCtl.getPageInfo()\">\n" +
    "    </restrictions-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageTemplateStepTemplate.html',
    "<select-page-template data-uri-context=\"addPageWizardCtl.uriContext\"\n" +
    "    data-page-type-code=\"addPageWizardCtl.getPageTypeCode()\"\n" +
    "    data-on-template-selected=\"addPageWizardCtl.templateSelected\">\n" +
    "</select-page-template>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/addPageWizard/templates/pageTypeStepTemplate.html',
    "<select-page-type data-on-type-selected=\"addPageWizardCtl.typeSelected\">\n" +
    "</select-page-type>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/components/clonePageInfo/componentCloneInfoTemplate.html',
    "<y-message data-ng-if=\"$ctrl.catalogVersionContainsPageWithSameTypeCode\"\n" +
    "    data-type=\"warning\">\n" +
    "    <message-description translate=\"se.cms.clonepagewizard.pageinfo.targetcatalogversion.pagetype.exists.message\"\n" +
    "        translate-value-type-code=\"{{$ctrl.pageTypeCode}}\">\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "\n" +
    "<div class=\"edit-page-basic-tab--header\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"se.cms.pageinfo.page.type\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.pageTypeCode}}</div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"se.cms.pageinfo.page.template\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.pageTemplate}}</div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<generic-editor data-structure=\"$ctrl.structure\"\n" +
    "    data-content=\"$ctrl.content\"\n" +
    "    data-submit=\"$ctrl.submit\"\n" +
    "    data-reset=\"$ctrl.reset\"\n" +
    "    data-is-dirty=\"$ctrl.isDirty\"\n" +
    "    data-is-valid=\"$ctrl.isValid\"\n" +
    "    data-get-api=\"$ctrl.setGenericEditorApi($api)\">\n" +
    "</generic-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/components/clonePageOptions/componentCloneOptionTemplate.html',
    "<label for=\"components-cloned-selector-id\"\n" +
    "    class=\"control-label\"\n" +
    "    data-translate=\"se.cms.clonepagewizard.options.title\" />\n" +
    "<y-help data-title=\"$ctrl.helpTitle\"\n" +
    "    data-template=\"$ctrl.helpTemplate\"></y-help>\n" +
    "<div id=\"components-cloned-selector-id\">\n" +
    "    <div class=\"radio\">\n" +
    "        <input class=\"components-cloned-option-id\"\n" +
    "            type=\"radio\"\n" +
    "            name=\"componentsclone\"\n" +
    "            id=\"reference-cloning\"\n" +
    "            ng-model=\"$ctrl.componentInSlotOption\"\n" +
    "            ng-click=\"$ctrl.updateComponentInSlotOption($ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.REFERENCE_EXISTING)\"\n" +
    "            ng-value=\"$ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.REFERENCE_EXISTING\">\n" +
    "        <label for=\"reference-cloning\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.existing\" />\n" +
    "    </div>\n" +
    "    <div class=\"radio\">\n" +
    "        <input class=\"components-cloned-option-id\"\n" +
    "            type=\"radio\"\n" +
    "            id=\"deep-cloning\"\n" +
    "            name=\"componentsclone\"\n" +
    "            ng-model=\"$ctrl.componentInSlotOption\"\n" +
    "            ng-click=\"$ctrl.updateComponentInSlotOption($ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.CLONE)\"\n" +
    "            ng-value=\"$ctrl.CLONE_COMPONENTS_IN_CONTENT_SLOTS_OPTION.CLONE\">\n" +
    "        <label for=\"deep-cloning\"\n" +
    "            data-translate=\"se.cms.clonepagewizard.options.copies\" />\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageInfoStepTemplate.html',
    "<component-clone-info-form data-ng-if=\"clonePageWizardCtrl.isPageInfoActive()\"\n" +
    "    data-structure=\"clonePageWizardCtrl.getPageInfoStructure()\"\n" +
    "    data-content=\"clonePageWizardCtrl.getPageInfo()\"\n" +
    "    data-submit=\"clonePageWizardCtrl.callbacks.savePageInfo\"\n" +
    "    data-reset=\"clonePageWizardCtrl.callbacks.resetPageInfo\"\n" +
    "    data-is-dirty=\"clonePageWizardCtrl.callbacks.isDirtyPageInfo\"\n" +
    "    data-is-valid=\"clonePageWizardCtrl.callbacks.isValidPageInfo\"\n" +
    "    data-page-template=\"clonePageWizardCtrl.getPageTemplate()\"\n" +
    "    data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-target-catalog-version=\"clonePageWizardCtrl.getTargetCatalogVersion()\">\n" +
    "</component-clone-info-form>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageOptionsStepTemplate.html',
    "<select-target-catalog-version data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-page-label=\"clonePageWizardCtrl.getPageLabel()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-base-page-info=\"clonePageWizardCtrl.getBasePageInfo()\"\n" +
    "    data-on-target-catalog-version-selected=\"clonePageWizardCtrl.onTargetCatalogVersionSelected($catalogVersion)\">\n" +
    "</select-target-catalog-version>\n" +
    "\n" +
    "<new-page-display-condition data-page-type-code=\"clonePageWizardCtrl.getPageTypeCode()\"\n" +
    "    data-uri-context=\"clonePageWizardCtrl.uriContext\"\n" +
    "    data-result-fn=\"clonePageWizardCtrl.variationResult\"\n" +
    "    data-initial-condition-selected=\"'page.displaycondition.variation'\"\n" +
    "    data-initial-primary-page-selected=\"clonePageWizardCtrl.getPageLabel()\"\n" +
    "    data-target-catalog-version=\"clonePageWizardCtrl.getTargetCatalogVersion()\">\n" +
    "</new-page-display-condition>\n" +
    "\n" +
    "<component-clone-option-form data-on-selection-change=\"clonePageWizardCtrl.triggerUpdateCloneOptionResult($cloneOptionData)\"></component-clone-option-form>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/clonePageWizard/templates/clonePageRestrictionsStepTemplate.html',
    "<span data-ng-if=\"clonePageWizardCtrl.isRestrictionsActive()\">\n" +
    "    <div data-ng-if=\"clonePageWizardCtrl.getPageRestrictions().length > 1\">\n" +
    "        <page-restrictions-info-message />\n" +
    "    </div>\n" +
    "\n" +
    "    <restrictions-editor data-editable=\"true\"\n" +
    "        data-save-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.save\"\n" +
    "        data-reset-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.reset\"\n" +
    "        data-cancel-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.cancel\"\n" +
    "        data-is-dirty-fn=\"clonePageWizardCtrl.restrictionsEditorFunctionBindings.isDirty\"\n" +
    "        data-on-restrictions-changed=\"clonePageWizardCtrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "        data-get-restriction-types=\"clonePageWizardCtrl.getRestrictionTypes()\"\n" +
    "        data-get-supported-restriction-types=\"clonePageWizardCtrl.getSupportedRestrictionTypes()\"\n" +
    "        data-item=\"clonePageWizardCtrl.getBasePageInfo()\"\n" +
    "        data-restrictions=\"clonePageWizardCtrl.getBasePageInfo().restrictions\">\n" +
    "    </restrictions-editor>\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsEditor/displayConditionsEditorTemplate.html',
    "<div>\n" +
    "    <display-conditions-page-info data-page-name=\"$ctrl.getPageName()\"\n" +
    "        data-page-type=\"$ctrl.getPageType()\"\n" +
    "        data-is-primary=\"$ctrl.isPagePrimary()\"></display-conditions-page-info>\n" +
    "    <div data-ng-if=\"$ctrl.isPagePrimary()\">\n" +
    "        <display-conditions-page-variations data-variations=\"$ctrl.getVariations()\"></display-conditions-page-variations>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.isPagePrimary()\">\n" +
    "        <display-conditions-primary-page data-read-only=\"$ctrl.getIsAssociatedPrimaryReadOnly()\"\n" +
    "            data-associated-primary-page=\"$ctrl.getAssociatedPrimaryPage()\"\n" +
    "            data-all-primary-pages=\"$ctrl.getPrimaryPages()\"\n" +
    "            data-on-primary-page-select=\"$ctrl.onPrimaryPageSelect(primaryPage)\"></display-conditions-primary-page>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPageInfo/displayConditionsPageInfoTemplate.html',
    "<div class=\"ySEDisplayConditionsPageInfo\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"{{ $ctrl.pageNameI18nKey }}\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight dc-page-name\"\n" +
    "                data-ng-bind=\"$ctrl.pageName\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-6 form-group\">\n" +
    "            <label class=\"control-label\"\n" +
    "                data-translate=\"{{ $ctrl.pageTypeI18nKey }}\"></label>\n" +
    "            <div class=\"form-readonly-text form-readonly-text__tight dc-page-type\"\n" +
    "                data-ng-bind=\"$ctrl.pageType\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <label class=\"control-label\">\n" +
    "            <span data-translate=\"{{ $ctrl.displayConditionLabelI18nKey }}\"></span>\n" +
    "            <y-help data-title=\"pageSync.helpTitle\"\n" +
    "                data-template=\"$ctrl.helpTemplate\"></y-help>\n" +
    "        </label>\n" +
    "        <p class=\"form-readonly-text form-readonly-text__tight dc-page-display-condition\">{{ $ctrl.getPageDisplayConditionI18nKey() | translate }}</p>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPageVariations/displayConditionsPageVariationsTemplate.html',
    "<div class=\"form-group dc-page-variations\">\n" +
    "    <label class=\"control-label control-label__margin\">{{ $ctrl.variationPagesTitleI18nKey | translate }}</label>\n" +
    "    <span data-ng-if=\"$ctrl.variations.length > 0\"\n" +
    "        class=\"help-control\"\n" +
    "        data-toggle=\"tooltip\"\n" +
    "        data-placement=\"top\"\n" +
    "        title=\"{{ $ctrl.variationsDescriptionI18nKey | translate }}\">\n" +
    "        <span class=\"hyicon hyicon-help-icon\"></span>\n" +
    "    </span>\n" +
    "    <div data-ng-if=\"$ctrl.variations.length === 0\"\n" +
    "        class=\"dc-no-variations form-readonly-text form-readonly-text__tight form-readonly-text__inline\">{{ $ctrl.noVariationsI18nKey | translate }}</div>\n" +
    "    <div data-ng-if=\"$ctrl.variations.length > 0\">\n" +
    "        <client-paged-list data-items=\"$ctrl.variations\"\n" +
    "            data-keys=\"$ctrl.keys\"\n" +
    "            data-renderers=\"$ctrl.renderers\"\n" +
    "            data-items-per-page=\"$ctrl.itemsPerPage\"\n" +
    "            class=\"dc-page-variations-list\">\n" +
    "        </client-paged-list>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/displayConditions/displayConditionsPrimaryPage/displayConditionsPrimaryPageTemplate.html',
    "<div class=\"form-group form-group__inline\">\n" +
    "    <label class=\"control-label control-label__margin\">{{ $ctrl.associatedPrimaryPageLabelI18nKey | translate }}</label>\n" +
    "    <div data-ng-if=\"$ctrl.readOnly\"\n" +
    "        class=\"dc-associated-primary-page form-readonly-text form-readonly-text__tight\">{{ ::$ctrl.associatedPrimaryPage.name }}\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.readOnly\">\n" +
    "        <ui-select id=\"display-conditions-primary-association-selector\"\n" +
    "            data-ng-model=\"$ctrl.associatedPrimaryPage\"\n" +
    "            class=\"form-control\"\n" +
    "            search-enabled=\"true\"\n" +
    "            theme=\"select2\"\n" +
    "            data-dropdown-auto-width=\"false\"\n" +
    "            data-ng-change=\"$ctrl.triggerOnPrimaryPageSelect()\">\n" +
    "            <ui-select-match id=\"select-type\"\n" +
    "                class=\"ySEPageRestr-picker--select__match\">\n" +
    "                {{ $ctrl.associatedPrimaryPage.name }}\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"primaryPage in $ctrl.allPrimaryPages | filter:$select.search\">\n" +
    "                {{ primaryPage.name }}\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "        <span data-ng-show=\"$ctrl.associatedPrimaryPage.name\"\n" +
    "            class=\"help-inline\">\n" +
    "            <span class=\"help-block-inline\">{{ 'page.label.label' | translate }}: {{ $ctrl.associatedPrimaryPage.label }}</span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/basic/editPageBasicTabInnerTemplate.html',
    "<div class=\"edit-page-basic-tab\">\n" +
    "\n" +
    "    <div class=\" edit-page-basic-tab--header\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-md-6 form-group\">\n" +
    "                <label class=\"control-label\">{{ \"se.cms.pageinfo.page.type\" | translate }}</label>\n" +
    "                <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.content.typeCode}}</div>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6 form-group\">\n" +
    "                <label class=\"control-label\">{{ \"se.cms.pageinfo.page.template\" | translate }}</label>\n" +
    "                <div class=\"form-readonly-text form-readonly-text__tight\">{{$ctrl.content.template}}</div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.structure\">\n" +
    "        <generic-editor data-id=\"$ctrl.tabId\"\n" +
    "            data-smartedit-component-id=\"$ctrl.model.uuid\"\n" +
    "            data-smartedit-component-type=\"$ctrl.model.typeCode\"\n" +
    "            data-structure=\"$ctrl.structure\"\n" +
    "            data-content=\"$ctrl.content\"\n" +
    "            data-submit=\"$ctrl.submitCallback\"\n" +
    "            data-reset=\"$ctrl.resetTab\"\n" +
    "            data-is-dirty=\"$ctrl.isDirtyTab\"\n" +
    "            data-get-api=\"$ctrl.setGenericEditorApi($api)\">\n" +
    "        </generic-editor>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/basic/editPageBasicTabTemplate.html',
    "<edit-page-basic-tab save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab='onCancel'\n" +
    "    is-dirty-tab='isDirty'\n" +
    "    data-tab-id='tabId'\n" +
    "    model='model'>\n" +
    "</edit-page-basic-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/displayConditions/displayConditionsTabInnerTemplate.html',
    "<display-conditions-editor data-page-uid=\"$ctrl.pageUid\"></display-conditions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/displayConditions/displayConditionsTabTemplate.html',
    "<display-conditions-tab data-save='onSave'\n" +
    "    data-reset='onReset'\n" +
    "    data-cancel='onCancel'\n" +
    "    data-is-dirty='isDirty'\n" +
    "    data-data-tab-id='tabId'\n" +
    "    data-model='model'>\n" +
    "</display-conditions-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/visibility/editPageVisibilityTabInnerTemplate.html',
    "<y-message data-type=\"danger\"\n" +
    "    class=\"se-restrictions-editor--y-message--modal-adjusted\"\n" +
    "    ng-repeat=\"error in ctrl.errors\">\n" +
    "    <message-description>\n" +
    "        <translate>{{error.message}}</translate>\n" +
    "    </message-description>\n" +
    "</y-message>\n" +
    "<div data-ng-if=\"ctrl.restrictions.length > 1\">\n" +
    "    <page-restrictions-info-message />\n" +
    "</div>\n" +
    "\n" +
    "<restrictions-editor data-ng-if=\"ctrl.isReady\"\n" +
    "    data-editable=\"true\"\n" +
    "    data-reset-fn=\"ctrl.resetTab\"\n" +
    "    data-cancel-fn=\"ctrl.cancelTab\"\n" +
    "    data-is-dirty-fn=\"ctrl.isDirtyTab\"\n" +
    "    data-on-restrictions-changed=\"ctrl.restrictionsResult($onlyOneRestrictionMustApply, $restrictions)\"\n" +
    "    data-item=\"ctrl.page\"\n" +
    "    data-restrictions=\"ctrl.page.restrictions\"\n" +
    "    data-get-restriction-types=\"ctrl.getRestrictionTypes()\"\n" +
    "    data-get-supported-restriction-types=\"ctrl.getSupportedRestrictionTypes()\">\n" +
    "</restrictions-editor>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/editPageModal/tabs/visibility/editPageVisibilityTabTemplate.html',
    "<edit-page-visibility-tab save-tab='onSave'\n" +
    "    reset-tab='onReset'\n" +
    "    cancel-tab=\"onCancel\"\n" +
    "    is-dirty-tab=\"isDirty\"\n" +
    "    data-tab-id=\"tabId\"\n" +
    "    model=\"model\">\n" +
    "</edit-page-visibility-tab>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoContainerTemplate.html',
    "<div class=\"ySEPageInfoMenu__header\">\n" +
    "    <page-info-header data-page-type-code=\"$ctrl.pageTypeCode\"\n" +
    "        data-page-template=\"$ctrl.pageTemplate\"></page-info-header>\n" +
    "</div>\n" +
    "<div class=\"ySEPageInfoMenu__content\">\n" +
    "    <page-info-details data-page-uid=\"$ctrl.pageUid\"\n" +
    "        data-page-type-code=\"$ctrl.pageTypeCode\"\n" +
    "        data-page-structure=\"$ctrl.pageStructure\"\n" +
    "        data-page-content=\"$ctrl.pageContent\"\n" +
    "        data-on-edit-click=\"$ctrl.onEditClick()\"></page-info-details>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoDetailsTemplate.html',
    "<div class=\"ySEPageInfoStaticInfo\">\n" +
    "    <div class=\"ySEPageInfoStaticInfoContainer\">\n" +
    "        <div class=\"row ySEPageIngoRow\">\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <p class=\"ySEHeaderText\">{{'se.cms.pageinfo.information.title' | translate}}</p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-6\">\n" +
    "                <button class=\"btn btn-link pull-right ySEPageInfo__edit-btn\"\n" +
    "                    has-operation-permission=\"'se.edit.page.link'\"\n" +
    "                    data-ng-click=\"$ctrl.onEditClick()\">\n" +
    "                    {{'se.cms.contextmenu.title.edit' | translate}}\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"$ctrl.pageStructure\"\n" +
    "        class=\"ySEPageInfoEditInfoContainer\">\n" +
    "        <generic-editor data-smartedit-component-id=\"$ctrl.pageUid\"\n" +
    "            data-smartedit-component-type=\"$ctrl.pageTypeCode\"\n" +
    "            data-structure=\"$ctrl.pageStructure\"\n" +
    "            data-content=\"$ctrl.pageContent\">\n" +
    "        </generic-editor>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfo/pageInfoHeaderTemplate.html',
    "<div class=\"ySEPageInfoHeaderContainer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p>{{\"se.cms.pageinfo.page.type\" | translate}}</p>\n" +
    "            <h3 class=\"page-type-code\">{{$ctrl.pageTypeCode}}</h3>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6\">\n" +
    "            <p>{{\"se.cms.pageinfo.page.template\" | translate}}</p>\n" +
    "            <h3 class=\"page-template\">{{$ctrl.pageTemplate}}</h3>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfoMenuToolbarItemTemplate.html',
    "<div class=\"ySEPageInfoMenu\">\n" +
    "    <page-info-container></page-info-container>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageInfoMenu/pageInfoMenuToolbarItemWrapperTemplate.html',
    "<page-info-menu-toolbar-item></page-info-menu-toolbar-item>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListLinkDirectiveTemplate.html',
    "<div class=\"page-list-link-container\">\n" +
    "    <a class=\"page-list-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-href=\"#!/pages/{{$ctrl.siteId}}/{{$ctrl.catalog.catalogId}}/{{$ctrl.catalogVersion.version}}\"\n" +
    "        data-translate=\"se.cms.cataloginfo.pagelist\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListLinkTemplate.html',
    "<page-list-link data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-site-id=\"$ctrl.siteId\"></page-list-link>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageList/pageListTemplate.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"pageListWrapper\">\n" +
    "    <div class=\"ySEPageListTitle\">\n" +
    "        <h1 class=\"ySEPage-list-title\"\n" +
    "            data-translate='se.cms.pagelist.title'></h1>\n" +
    "        <h4 class=\"ySEPage-list-label\">{{pageListCtl.catalogName | l10n}} - {{pageListCtl.catalogVersion}}</h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-input-group ySEPage-list-search\">\n" +
    "        <div class=\"se-input-group--addon\">\n" +
    "            <span class=\"hyicon hyicon-search ySEPage-list-search-icon\"></span>\n" +
    "        </div>\n" +
    "        <input type=\"text\"\n" +
    "            class=\"se-input-group--input ySEPage-list-search-input\"\n" +
    "            placeholder=\"{{ 'se.cms.pagelist.searchplaceholder' | translate }}\"\n" +
    "            data-ng-model=\"pageListCtl.query.value\"\n" +
    "            name=\"query\">\n" +
    "        <div class=\"se-input-group--addon ySESearchIcon\"\n" +
    "            data-ng-show=\"pageListCtl.query.value\"\n" +
    "            data-ng-click=\"pageListCtl.reset()\">\n" +
    "            <span class=\"glyphicon glyphicon-remove-sign\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"ySEAdd-Page-button\"\n" +
    "        has-operation-permission=\"'se.edit.page'\">\n" +
    "        <button class=\"y-add-btn\"\n" +
    "            data-ng-click=\"pageListCtl.openAddPageWizard()\">\n" +
    "            <span class=\"hyicon hyicon-add\"></span>\n" +
    "            <span class=\"\">{{'se.cms.addpagewizard.addpage' | translate}}</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "    <client-paged-list data-ng-if=\"pageListCtl.itemsReady\"\n" +
    "        data-items=\"pageListCtl.pages\"\n" +
    "        data-keys=\"pageListCtl.keys\"\n" +
    "        data-renderers=\"pageListCtl.renderers\"\n" +
    "        data-injected-context=\"pageListCtl.injectedContext\"\n" +
    "        data-sort-by=\"'name'\"\n" +
    "        data-reversed=\"false\"\n" +
    "        data-items-per-page=\"10\"\n" +
    "        data-query=\"pageListCtl.query.value\"\n" +
    "        data-display-count=\"true\"\n" +
    "        data-dropdown-items=pageListCtl.dropdownItems\n" +
    "        data-item-filter-keys=\"pageListCtl.searchKeys\">\n" +
    "    </client-paged-list>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/pages/pageRestrictionsInfoMessage/pageRestrictionsInfoMessageTemplate.html',
    "<y-message data-message-id=\"yMsgInfoId\"\n" +
    "    data-type=\"info\"\n" +
    "    class=\"se-restrictions-editor--y-message--modal-adjusted\">\n" +
    "    <message-title>{{ 'se.cms.restrictions.editor.ymessage.title' | translate }}</message-title>\n" +
    "    <message-description>{{ 'se.cms.restrictions.editor.ymessage.description' | translate }}</message-description>\n" +
    "</y-message>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementEditTemplate.html',
    "<div data-ng-if=\"$ctrl.ready\"\n" +
    "    class=\"se-restriction-management-edit\">\n" +
    "    <div data-ng-if=\"$ctrl.isTypeSupported\"\n" +
    "        class=\"se-restriction-management-supported-edit\">\n" +
    "        <div class=\"se-restriction-management-edit__header\">\n" +
    "            <div class=\"se-restriction-management-edit__name\">\n" +
    "                {{ $ctrl.restriction.name }}\n" +
    "            </div>\n" +
    "            <div class=\"se-restriction-management-edit__code\">\n" +
    "                {{ $ctrl.restriction.typeCode }}\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"se-restriction-management-edit__data\">\n" +
    "            <item-manager data-item=\"$ctrl.restriction\"\n" +
    "                data-mode=\"$ctrl.itemManagementMode\"\n" +
    "                data-structure-api=\"$ctrl.structureApi\"\n" +
    "                data-content-api=\"$ctrl.contentApi\"\n" +
    "                data-uri-context=\"$ctrl.uriContext\"\n" +
    "                data-component-type=\"$ctrl.restriction.itemtype\"\n" +
    "                data-submit-function=\"$ctrl.submitInternal\"\n" +
    "                data-is-dirty=\"$ctrl.isDirtyFn\" />\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!$ctrl.isTypeSupported\">\n" +
    "        <div class=\"se-restrictions-list--content\">\n" +
    "            <div>\n" +
    "                {{ 'se.cms.restriction.management.select.type.not.supported.warning' | translate }}\n" +
    "            </div>\n" +
    "            <p class=\"ySERestrictionsNameHeader\">{{ $ctrl.restriction.name }}</p>\n" +
    "            <div class=\"ySERestrictionsDescription\">\n" +
    "                {{ $ctrl.restriction.description }}\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementItemNameTemplate.html',
    "<span class=\"se-restriction-management-item-name\"\n" +
    "    data-ng-bind-html=\"item.name | l10n\">\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/flavours/restrictionManagementSelectTemplate.html',
    "<div class=\"se-restriction-management-select\">\n" +
    "    <y-message data-ng-show=\"$ctrl.selectModel.getRestriction() && !$ctrl.selectModel.isTypeSupported()\"\n" +
    "        data-message-id=\"yMsgWarningId\"\n" +
    "        data-type=\"warning\"\n" +
    "        class=\"se-restriction-management-select--y-message--modal-adjusted\">\n" +
    "        <message-title>{{ 'se.cms.restriction.management.select.type.not.supported.warning' | translate }}</message-title>\n" +
    "    </y-message>\n" +
    "\n" +
    "    <div class=\"se-restriction-management-select__y-select\">\n" +
    "        <label for=\"\"\n" +
    "            class=\"control-label\">{{ 'se.cms.restriction.management.select.type.label' | translate }}</label>\n" +
    "\n" +
    "        <y-select data-id=\"restriction-type\"\n" +
    "            data-fetch-strategy=\"{ fetchAll: $ctrl.selectModel.getRestrictionTypes }\"\n" +
    "            data-item-template=\"::$ctrl.itemTemplateUrl\"\n" +
    "            data-placeholder=\"'se.cms.restriction.management.select.type.placeholder'\"\n" +
    "            data-ng-model=\"$ctrl.selectModel.selectedIds.restrictionType\"\n" +
    "            data-on-change=\"$ctrl.selectRestrictionType\"\n" +
    "            data-search-enabled=\"false\">\n" +
    "        </y-select>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-recompile-dom=\"$ctrl.resetSelector\"\n" +
    "        class=\"se-restriction-management-select__restriction\">\n" +
    "        <div data-ng-if=\"$ctrl.controllerModel.showRestrictionSelector\"\n" +
    "            class=\"se-restriction-management-select__restriction__info\">\n" +
    "            <label class=\"control-label se-restriction-management-select__restriction__info__label\">\n" +
    "                {{ 'se.cms.restriction.management.select.restriction.label' | translate }}\n" +
    "            </label>\n" +
    "            <y-select data-id=\"restriction-name\"\n" +
    "                data-fetch-strategy=\"$ctrl.fetchOptions\"\n" +
    "                data-ng-model=\"$ctrl.selectModel.selectedIds.restriction\"\n" +
    "                data-placeholder=\"'se.cms.restriction.management.select.restriction.placeholder'\"\n" +
    "                data-on-change=\"$ctrl.selectRestriction\"\n" +
    "                data-disable-choice-fn=\"$ctrl.disableRestrictionChoice\"\n" +
    "                data-item-template=\"::$ctrl.itemTemplateUrl\"\n" +
    "                data-results-header-template=\"$ctrl.getResultsHeaderTemplate()\"\n" +
    "                data-results-header-label=\"::$ctrl.resultsHeaderLabel\">\n" +
    "            </y-select>\n" +
    "        </div>\n" +
    "\n" +
    "        <div data-recompile-dom=\"$ctrl.resetEditor\"\n" +
    "            class=\"se-restriction-management-select__restriction__editor\"\n" +
    "            data-ng-if=\"$ctrl.controllerModel.showRestrictionEditor\">\n" +
    "            <p class=\"se-restriction-management-select__restriction__editor__instr\">\n" +
    "                {{ $ctrl.editorHeader | translate }}\n" +
    "            </p>\n" +
    "            <div data-ng-if=\"$ctrl.selectModel.isTypeSupported()\">\n" +
    "                <item-manager data-item=\"$ctrl.selectModel.getRestriction()\"\n" +
    "                    data-mode=\"$ctrl.controllerModel.mode\"\n" +
    "                    data-structure-api=\"$ctrl.controllerModel.structureApi\"\n" +
    "                    data-content-api=\"$ctrl.controllerModel.contentApi\"\n" +
    "                    data-uri-context=\"$ctrl.uriContext\"\n" +
    "                    data-component-type=\"$ctrl.selectModel.getRestrictionTypeCode()\"\n" +
    "                    data-submit-function=\"$ctrl.submitInternal\"\n" +
    "                    data-is-dirty=\"$ctrl.isDirtyInternal\" />\n" +
    "            </div>\n" +
    "            <div class=\"ySERestrictionsList-item\"\n" +
    "                data-ng-if=\"!$ctrl.selectModel.isTypeSupported()\">\n" +
    "                <p class=\"ySERestrictionsNameHeader\">{{ $ctrl.selectModel.getRestriction().name }}</p>\n" +
    "                <div class=\"ySERestrictionsDescription\">\n" +
    "                    {{ $ctrl.selectModel.getRestriction().description }}\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionManagement/restrictionManagementTemplate.html',
    "<div data-ng-if=\"$ctrl.editMode\"\n" +
    "    class=\"se-restriction-management se-restriction-management-edit\">\n" +
    "    <restriction-management-edit data-restriction=\"$ctrl.restriction\"\n" +
    "        data-get-supported-restriction-types-fn=\"$ctrl.getSupportedRestrictionTypesFn\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-is-dirty-fn=\"$ctrl.isDirtyFn\"\n" +
    "        data-submit-fn=\"$ctrl.submitInternal\">\n" +
    "    </restriction-management-edit>\n" +
    "</div>\n" +
    "<div data-ng-if=\"!$ctrl.editMode\"\n" +
    "    class=\"se-restriction-management se-restriction-management-select\">\n" +
    "    <restriction-management-select data-existing-restrictions=\"$ctrl.existingRestrictions\"\n" +
    "        data-get-restriction-types-fn=\"$ctrl.getRestrictionTypesFn()\"\n" +
    "        data-get-supported-restriction-types-fn=\"$ctrl.getSupportedRestrictionTypesFn\"\n" +
    "        data-uri-context=\"$ctrl.uriContext\"\n" +
    "        data-is-dirty-fn=\"$ctrl.isDirtyFn\"\n" +
    "        data-submit-fn=\"$ctrl.submitInternal\">\n" +
    "    </restriction-management-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionsEditor/restrictionsEditorTemplate.html',
    "<div class=\"ySEVisbilityModalContainer\">\n" +
    "    <button data-ng-show=\"!$ctrl.showRestrictionPicker\"\n" +
    "        class=\"y-add-btn\"\n" +
    "        data-ng-click=\"$ctrl.onClickOnAdd()\">\n" +
    "        <span class=\"hyicon hyicon-add\"></span>{{ 'se.cms.restrictions.editor.button.add.new ' | translate }}\n" +
    "    </button>\n" +
    "\n" +
    "    <label data-ng-if=\"$ctrl.restrictions.length > 0\"\n" +
    "        class=\"ySEVisbilityModalContainer-leftPane-title\">{{ 'se.cms.restrictions.list.title' | translate }}</label>\n" +
    "\n" +
    "    <restrictions-table data-ng-if=\"$ctrl.isRestrictionsReady\"\n" +
    "        data-editable=\"true\"\n" +
    "        data-restrictions=\"$ctrl.restrictions\"\n" +
    "        data-on-click-on-edit='$ctrl.onClickOnEdit'\n" +
    "        data-on-criteria-selected=\"$ctrl.matchCriteriaChanged\"\n" +
    "        data-restriction-criteria=\"$ctrl.criteria\"\n" +
    "        data-errors=\"$ctrl.errors\"\n" +
    "        data-custom-class=\"'ySERestrictionListLink'\">\n" +
    "    </restrictions-table>\n" +
    "\n" +
    "    <span data-ng-show=\"$ctrl.showRestrictionPicker\"\n" +
    "        class=\"ySEVisbilityModalContainer-rightPane__restriction\">\n" +
    "        <y-slider-panel data-slider-panel-configuration=\"$ctrl.sliderPanelConfiguration\"\n" +
    "            data-slider-panel-hide=\"$ctrl.sliderPanelHide\"\n" +
    "            class=\"se-add-restriction-panel\"\n" +
    "            data-slider-panel-show=\"$ctrl.sliderPanelShow\">\n" +
    "            <restriction-management data-config=\"$ctrl.restrictionManagement.operation\"\n" +
    "                data-uri-context=\"$ctrl.restrictionManagement.uriContext\"\n" +
    "                class=\"se-se-add-restriction-panel__restriction-management\"\n" +
    "                data-submit-fn=\"$ctrl.restrictionManagement.submitFn\"\n" +
    "                data-is-dirty-fn=\"$ctrl.restrictionManagement.isDirtyFn\">\n" +
    "            </restriction-management>\n" +
    "        </y-slider-panel>\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/restrictions/restrictionsTable/restrictionsTableTemplate.html',
    "<div class=\"ySERestrictionsContainer ySERestrictionsContainer-left {{$ctrl.customClass}}\">\n" +
    "    <div data-ng-if=\"$ctrl.restrictions.length > 1\">\n" +
    "        <div data-ng-if=\"$ctrl.editable\">\n" +
    "            <ui-select on-select=\"$ctrl.criteriaClicked($item)\"\n" +
    "                data-ng-model=\"$ctrl.restrictionCriteria\"\n" +
    "                class=\"form-control ySERestriction-select\"\n" +
    "                search-enabled=\"false\"\n" +
    "                theme=\"select2\"\n" +
    "                data-dropdown-auto-width=\"false\">\n" +
    "                <ui-select-match placeholder=\"{{ 'se.cms.restrictionspicker.type.placeholder' | translate }}\"\n" +
    "                    class=\"ySERestriction-select__placeholder\">\n" +
    "                    {{$select.selected.editLabel | translate}}\n" +
    "                </ui-select-match>\n" +
    "                <ui-select-choices repeat=\"criteriaOption in $ctrl.criteriaOptions\"\n" +
    "                    class=\"ySERestriction-select__choices\"\n" +
    "                    position=\"down \">\n" +
    "                    {{ criteriaOption.editLabel | translate }}\n" +
    "                </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "        </div>\n" +
    "        <div class=\"ySERestrictionsCriteria\"\n" +
    "            data-ng-if=\"!$ctrl.editable\">\n" +
    "            <span class=\"ySERestrictionsCriteriaLabel\">{{ 'se.cms.restrictions.criteria' | translate }} {{ $ctrl.restrictionCriteria.label | translate }}</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.showRemoveAllButton()\"\n" +
    "        class=\"ySERestrictionsContainer__clear_all\">\n" +
    "        <button class=\"btn btn-link cms-clean-btn ySERestrictionsContainer__clear_all__btn\"\n" +
    "            data-ng-click=\"$ctrl.removeAllRestrictions()\"\n" +
    "            data-translate=\"se.cms.restrictions.list.clear.all\">\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-restrictions-list\"\n" +
    "        data-ng-if=\"$ctrl.restrictions.length > 0\">\n" +
    "        <div id=\"restriction-{{$index+1}}\"\n" +
    "            data-ng-repeat=\"restriction in $ctrl.restrictions\"\n" +
    "            class=\"se-restrictions-list--item\">\n" +
    "            <div class=\"se-restrictions-list--content\"\n" +
    "                data-ng-click=\"$ctrl.onSelect(restriction)\">\n" +
    "                <p class=\"ySERestrictionsNameHeader ng-class:{'error-input':$ctrl.isInError($index)}\">{{ restriction.name }}</p>\n" +
    "                <div class=\"ySERestrictionsTypeAndID ng-class:{'error-input':$ctrl.isInError($index)}\">{{ restriction.typeCode }}</div>\n" +
    "                <div class=\"ySERestrictionsDescription ng-class:{'error-input':$ctrl.isInError($index)}\"\n" +
    "                    title=\"{{ restriction.description }}\">{{ restriction.description }}</div>\n" +
    "            </div>\n" +
    "            <div class=\"se-restrictions-list--options\"\n" +
    "                data-ng-if=\"$ctrl.editable\">\n" +
    "                <y-drop-down-menu dropdown-items=\"$ctrl.actions\"\n" +
    "                    selected-item=\"restriction\"\n" +
    "                    class=\"y-dropdown ySERestrictionsList-item-btn__more pull-right\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/catalogs/catalogDetailsSyncTemplate.html',
    "<synchronize-catalog data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-active-catalog-version=\"$ctrl.activeCatalogVersion\">\n" +
    "</synchronize-catalog>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/catalogs/synchronizeCatalogTemplate.html',
    "<div class=\"se-synchronize-catalog\">\n" +
    "    <!-- Status -->\n" +
    "    <div class=\"se-synchronize-catalog__item se-synchronize-catalog__sync-info\">\n" +
    "        <div data-ng-if=\"ctrl.isSyncJobFinished()\">\n" +
    "            <label class=\"se-synchronize-catalog__sync-info__sync-label\"\n" +
    "                data-ng-if=\"ctrl.catalogVersion.active\">\n" +
    "                {{ 'se.cms.cataloginfo.lastsyncedfrom' | translate: ctrl.getSyncFromLabels() }}\n" +
    "            </label>\n" +
    "            <label class=\"se-synchronize-catalog__sync-info__sync-label\"\n" +
    "                data-ng-if=\"!ctrl.catalogVersion.active\"\n" +
    "                data-translate=\"se.cms.cataloginfo.lastsynced\"></label>\n" +
    "            <span class=\"catalog-last-synced\">{{ctrl.syncJobStatus.syncEndTime| date:'short'}}</span>\n" +
    "        </div>\n" +
    "        <span data-ng-if=\"ctrl.isSyncJobInProgress()\"\n" +
    "            class=\"se-synchronize-catalog__in-progress\"\n" +
    "            data-translate=\"se.sync.status.synced.inprogress\"></span>\n" +
    "        <span data-ng-if=\"ctrl.isSyncJobFailed()\"\n" +
    "            class=\"label-error se-synchronize-catalog__sync-failed\"\n" +
    "            data-translate=\"se.sync.status.synced.syncfailed\"></span>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"se-synchronize-catalog__item se-synchronize-catalog__sync-btn\"\n" +
    "        data-ng-if=\"!ctrl.catalogVersion.active\"\n" +
    "        data-has-operation-permission=\"ctrl.syncCatalogPermission\">\n" +
    "        <button class=\"btn btn-default\"\n" +
    "            data-ng-disabled=\"!ctrl.isButtonEnabled()\"\n" +
    "            data-ng-click=\"ctrl.syncCatalog()\">{{ 'se.cms.cataloginfo.btn.sync' | translate }}</button>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageListSyncIcon/pageListSyncIconTemplate.html',
    "<span class=\"hyicon se-sync-button__sync \"\n" +
    "    data-ng-class=\"$ctrl.classes[$ctrl.syncStatus.status]\"\n" +
    "    data-sync-status=\"{{$ctrl.syncStatus.status}}\">\n" +
    "</span>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationHeaderTemplate.html',
    "<span class=\"se-sync-panel-header__label\"\n" +
    "    data-translate=\"se.cms.synchronization.panel.lastsync.text\"></span>\n" +
    "<span class=\"se-sync-panel-header__timestamp\">{{pageSync.lastSyncTime | date:'short'}}</span>\n" +
    "<div class=\"se-sync-panel-header__text\">\n" +
    "    <span data-translate=\"se.cms.synchronization.page.header\"></span>\n" +
    "    <y-help data-ng-if=\"pageSync.helpTemplate\"\n" +
    "        data-title=\"pageSync.helpTitle\"\n" +
    "        data-template=\"pageSync.helpTemplate\"></y-help>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationHeaderWrapperTemplate.html',
    "<page-synchronization-header data-last-sync-time=\"sync.syncStatus.lastSyncStatus\" />"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/pageSynchronizationPanelTemplate.html',
    "<synchronization-panel data-ng-if=\"!pageSync.showSyncButton\"\n" +
    "    data-item-id=\"pageSync.itemId\"\n" +
    "    data-get-sync-status=\"pageSync.getSyncStatus\"\n" +
    "    data-perform-sync=\"pageSync.performSync\"\n" +
    "    data-header-template-url=\"pageSync.headerTemplateUrl\"\n" +
    "    data-sync-items=\"pageSync.syncItems\"\n" +
    "    data-on-selected-items-update=\"pageSync.onSelectedItemsUpdate\">\n" +
    "</synchronization-panel>\n" +
    "<synchronization-panel data-ng-if=\"pageSync.showSyncButton\"\n" +
    "    data-item-id=\"pageSync.itemId\"\n" +
    "    data-get-sync-status=\"pageSync.getSyncStatus\"\n" +
    "    data-perform-sync=\"pageSync.performSync\"\n" +
    "    data-header-template-url=\"pageSync.headerTemplateUrl\">\n" +
    "</synchronization-panel>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/syncMenu/pageSyncMenuToolbarItemTemplate.html',
    "<div class=\"toolbar-action\"\n" +
    "    data-uib-dropdown\n" +
    "    data-auto-close=\"outsideClick\"\n" +
    "    data-is-open=\"$ctrl.toolbarItem.isOpen\"\n" +
    "    data-item-key=\"{{ $ctrl.toolbarItem.key }}\">\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn btn-default toolbar-action--button\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        aria-pressed=\"false\">\n" +
    "        <span class=\"hyicon hyicon-sync se-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <div class=\"toolbar-action--button--txt\">\n" +
    "            <span data-ng-class=\"{'se-toolbar-menu-ddlb--button__txt': $ctrl.isNotInSync }\">\n" +
    "                {{ $ctrl.toolbarItem.name | translate }}\n" +
    "                <span data-ng-if=\"$ctrl.isNotInSync\"\n" +
    "                    class=\"hyicon hyicon-caution se-toolbar-menu-ddlb--button__caution\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "    </button>\n" +
    "    <div data-uib-dropdown-menu\n" +
    "        class=\"dropdown-menu-left btn-block toolbar-action--include\">\n" +
    "        <div ng-if=\"$ctrl.toolbarItem.isOpen\">\n" +
    "            <ul class=\"se-toolbar-menu-content se-toolbar-menu-content__page-sync\"\n" +
    "                role=\"menu\">\n" +
    "                <li role=\"menuitem\">\n" +
    "                    <div class=\"se-toolbar-menu-content--wrapper\">\n" +
    "                        <div class=\"se-toolbar-menu-content--header\">\n" +
    "                            <div class=\"se-toolbar-menu-content--header__title\"\n" +
    "                                data-translate=\"se.cms.synchronization.page.title\">\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"se-toolbar-menu-content--body\">\n" +
    "                            <page-synchronization-panel></page-synchronization-panel>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmarteditContainer/components/synchronize/pages/syncMenu/pageSyncMenuToolbarItemWrapperTemplate.html',
    "<page-sync-menu-toolbar-item data-item=\"item\"></page-sync-menu-toolbar-item>"
  );

}]);
