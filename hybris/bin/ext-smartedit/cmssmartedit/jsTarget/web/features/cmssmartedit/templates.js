angular.module('cmssmarteditTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/cmssmartedit/components/externalComponent/externalComponentButtonTemplate.html',
    "<div class=\"cms-external-component-button\"\n" +
    "    data-ng-if=\"ctrl.isReady\">\n" +
    "    {{ctrl.catalogVersion}}\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/externalComponent/externalComponentDecoratorTemplate.html',
    "<div class=\"cms-external-component-decorator\"\n" +
    "    data-ng-if=\"!ctrl.isExtenalSlot\">\n" +
    "    <div class=\"contextualMenuOverlay\">\n" +
    "        <span class=\"hyicon hyicon-globe hyicon-globe--normal\"\n" +
    "            data-ng-if=\"!ctrl.active\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"yWrapperData\"\n" +
    "        data-ng-transclude>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotShared/slotSharedButtonTemplate.html',
    "<div class=\"shared-slot-button-template\">\n" +
    "    <a type=\"button\"\n" +
    "        data-ng-if=\"ctrl.slotSharedFlag\">\n" +
    "        <div class=\"btn-group se-slot-ctx-menu__btn-group\"\n" +
    "            data-uib-dropdown\n" +
    "            data-dropdown-append-to-body\n" +
    "            data-auto-close=\"outsideClick\"\n" +
    "            data-is-open=\"ctrl.isPopupOpened\">\n" +
    "            <button type=\"button\"\n" +
    "                data-uib-dropdown-toggle\n" +
    "                class=\"btn btn-primary shared-slot-button-template__btn se-slot-ctx-menu__divider\"\n" +
    "                data-ng-class=\"{'ySESharedSlotOpen': ctrl.isPopupOpened}\"\n" +
    "                id=\"sharedSlotButton-{{::ctrl.slotId}}\">\n" +
    "                <span class=\"hyicon hyicon-linked se-slot-ctx-menu__icon-shared\"></span>\n" +
    "            </button>\n" +
    "            <ul class=\"dropdown-menu dropdown-menu-right btn-block shared-slot-button-template__menu\"\n" +
    "                data-uib-dropdown-menu\n" +
    "                role=\"menu\">\n" +
    "                <li role=\"menuitem\">\n" +
    "                    <div class=\"shared-slot-button-template__menu__title__text\">\n" +
    "                        <span class=\"shared-slot-button-template__menu__title__text__msg\"\n" +
    "                            data-translate=\"se.cms.slot.shared.popover.message\"></span>\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </div>\n" +
    "    </a>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotShared/slotSharedTemplate.html',
    "<slot-shared-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"></slot-shared-button>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/externalSlotDisabledDecoratorTemplate.html',
    "<slot-disabled-component data-active=\"ctrl.active\"\n" +
    "    data-component-attributes=\"ctrl.componentAttributes\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/sharedSlotDisabledDecoratorTemplate.html',
    "<slot-disabled-component data-active=\"ctrl.active\"\n" +
    "    data-component-attributes=\"ctrl.componentAttributes\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotSharedDisabled/slotDisabledTemplate.html',
    "<div class=\"disabled-shared-slot popoveredElement\"\n" +
    "    data-ng-class=\"{ 'disabled-shared-slot-hovered': ctrl.active, 'external-shared-slot': ctrl.isExternalSlot }\"\n" +
    "    data-popover-content=\"{{ctrl.getPopoverMessage()}}\">\n" +
    "    <div class=\"disabled-shared-slot__icon--outer\"\n" +
    "        data-ng-class=\"[ctrl.getOuterSlotClass(), { 'disabled-shared-slot__icon--outer-hovered': ctrl.active }]\">\n" +
    "        <span class=\"hyicon shared_slot_disabled_icon disabled-shared-slot__icon--inner\"\n" +
    "            data-ng-class=\"ctrl.getSlotIconClass()\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityButtonTemplate.html',
    "<div class=\"slot-visibility-button-template\">\n" +
    "    <div class=\"btn-group slot-visibility-button-template__btn-group\"\n" +
    "        data-uib-dropdown\n" +
    "        data-dropdown-append-to-body\n" +
    "        data-is-open=\"ctrl.isComponentListOpened\">\n" +
    "        <button type=\"button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            class=\"btn btn-primary slot-visibility-button-template__btn se-slot-ctx-menu__divider\"\n" +
    "            data-ng-if=\"ctrl.buttonVisible\"\n" +
    "            data-ng-class=\"{'ySEVisibilityOpen': ctrl.isComponentListOpened}\"\n" +
    "            id=\"slot-visibility-button-{{::ctrl.slotId}}\">\n" +
    "            <img class=\"slot-visibility-button-template__btn__img \"\n" +
    "                data-ng-src=\"{{ ctrl.eyeImageUrl }}\" />{{ ::ctrl.hiddenComponentCount }}\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu dropdown-menu-right slot-visibility-button-template__menu\"\n" +
    "            data-uib-dropdown-menu\n" +
    "            role=\"menu\"\n" +
    "            id=\"slot-visibility-list-{{::ctrl.slotId}}\">\n" +
    "            <lh class=\"slot-visibility-button-template__menu__title\">\n" +
    "                <p class=\"slot-visibility-button-template__menu__title__text\">{{'se.cms.slotvisibility.list.title' | translate}}</p>\n" +
    "            </lh>\n" +
    "            <li class=\"slot-visibility-component slot-visibility-button-template__menu__component\"\n" +
    "                data-ng-repeat=\"component in ctrl.hiddenComponents track by $index\">\n" +
    "\n" +
    "                <span data-y-popover\n" +
    "                    data-ng-if=\"component.isExternal\"\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-template=\"ctrl.getTemplateInfoForExternalComponent()\">\n" +
    "                    <slot-visibility-component data-component=\"component\"\n" +
    "                        data-slot-id=\"{{::ctrl.slotId}}\"></slot-visibility-component>\n" +
    "                </span>\n" +
    "\n" +
    "                <slot-visibility-component data-component=\"component\"\n" +
    "                    data-ng-if=\"!component.isExternal\"\n" +
    "                    data-slot-id=\"{{::ctrl.slotId}}\"></slot-visibility-component>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityComponentTemplate.html',
    "<div class=\"slot-visiblity-component-template\"\n" +
    "    data-ng-class=\"{'slot-visiblity-component-template__external': ctrl.component.isExternal}\">\n" +
    "    <div class=\"slot-visiblity-component-template__icon\">\n" +
    "        <img data-ng-src=\"{{::ctrl.imageRoot}}/{{ ctrl.component.isExternal ? 'component_external.png': 'component_default.png' }}\"\n" +
    "            class=\"slot-visiblity-component-template__icon__image\"\n" +
    "            alt=\"{{::ctrl.component.typeCode}}\" />\n" +
    "    </div>\n" +
    "    <div class=\"slot-visiblity-component-template__text-wrapper\">\n" +
    "        <a href=\"#\"\n" +
    "            data-ng-if=\"!ctrl.component.isExternal\"\n" +
    "            class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__name\"\n" +
    "            data-ng-click=\"ctrl.openEditorModal()\">{{ ::ctrl.component.name }}</a>\n" +
    "        <a data-ng-if=\"ctrl.component.isExternal\"\n" +
    "            class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__name\">{{ ::ctrl.component.name }}</a>\n" +
    "        <p class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__type\">{{ ::ctrl.component.typeCode }}</p>\n" +
    "        <p class=\"slot-visiblity-component-template__text slot-visiblity-component-template__text__visibility\">{{'se.cms.editortabset.visibilitytab.title' | translate}} {{ctrl.componentVisbilitySwitch | translate}} / {{ctrl.componentRestrictionsCount}} {{'se.cms.restrictions.editor.tab' | translate}}</p>\n" +
    "    </div>\n" +
    "    <span data-ng-if=\"ctrl.component.isExternal === true\"\n" +
    "        class=\"hyicon hyicon-globe slot-visiblity-component--globe-icon\"></span>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/slotVisibility/slotVisibilityWidgetTemplate.html',
    "<slot-visibility-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"\n" +
    "    data-set-remain-open=\"ctrl.setRemainOpen(button, remainOpen)\" />"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSyncButtonTemplate.html',
    "<div class=\"slot-sync-button-template\"\n" +
    "    data-ng-if=\"ctrl.isContentCatalogVersionNonActive\">\n" +
    "    <div class=\"btn-group se-slot-ctx-menu__btn-group\"\n" +
    "        data-uib-dropdown\n" +
    "        data-dropdown-append-to-body\n" +
    "        data-auto-close=\"outsideClick\"\n" +
    "        data-is-open=\"ctrl.isPopupOpened\">\n" +
    "        <button type=\"button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            class=\"btn btn-primary se-slot-ctx-menu__btn se-slot-ctx-menu__divider\"\n" +
    "            data-ng-class=\"{'ySESyncOpen': ctrl.isPopupOpened}\"\n" +
    "            id=\"slot-sync-button-{{::ctrl.slotId}}\">\n" +
    "            <span class=\"hyicon hyicon-sync se-slot-ctx-menu__btn__sync\"></span>\n" +
    "            <span data-ng-if=\"!ctrl.isSlotInSync\"\n" +
    "                class=\"hyicon hyicon-caution se-slot-ctx-menu__btn__caution\"></span>\n" +
    "            </span>\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu dropdown-menu-right btn-block se-toolbar-menu-content se-toolbar-menu-content__page-sync slot-sync-button-template__menu\"\n" +
    "            data-uib-dropdown-menu\n" +
    "            role=\"menu\">\n" +
    "            <li role=\"menuitem\">\n" +
    "                <div class=\"se-toolbar-menu-content--wrapper\">\n" +
    "                    <div class=\"se-toolbar-menu-content--header\">\n" +
    "                        <div class=\"se-toolbar-menu-content--header__title\"\n" +
    "                            data-translate=\"se.cms.synchronization.slot.header\">\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"se-toolbar-menu-content--body\">\n" +
    "                        <slot-synchronization-panel data-ng-if=\"ctrl.isPopupOpened\"\n" +
    "                            data-slot-id=\"ctrl.slotId\"></slot-synchronization-panel>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSyncTemplate.html',
    "<slot-sync-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"\n" +
    "    data-set-remain-open=\"ctrl.setRemainOpen(button, remainOpen)\"></slot-sync-button>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/slotSynchronizationPanelTemplate.html',
    "<div data-page-sensitive>\n" +
    "    <synchronization-panel data-item-id=\"slotSync.slotId\"\n" +
    "        data-get-sync-status=\"slotSync.getSyncStatus\"\n" +
    "        data-perform-sync=\"slotSync.performSync\">\n" +
    "    </synchronization-panel>\n" +
    "</div>"
  );


  $templateCache.put('web/features/cmssmartedit/components/synchronize/slots/syncIndicatorDecorator/syncIndicatorDecoratorTemplate.html',
    "<div class=\"sync-indicator-decorator\"\n" +
    "    data-ng-class=\"ctrl.syncStatus.status\"\n" +
    "    data-sync-status=\"{{ctrl.syncStatus.status}}\">\n" +
    "    <div class=\"yWrapperData\"\n" +
    "        data-ng-transclude></div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<!-- temporary styling -->\n" +
    "<style>\n" +
    ".sync-indicator-decorator:not(.IN_SYNC):not(.UNAVAILABLE) .decorator-slot-border {\n" +
    "    border: 2px dashed #0486e0;\n" +
    "    /* @smartedit-blue */\n" +
    "}\n" +
    "/* or */\n" +
    "/* .sync-indicator-decorator:not([data-sync-status=\"IN_SYNC\"]):not([data-sync-status=\"UNAVAILABLE\"]) .decorator-slot-border{\n" +
    "    border: 2px dashed #0486e0;\n" +
    "}\n" +
    "*/\n" +
    "</style>"
  );

}]);
