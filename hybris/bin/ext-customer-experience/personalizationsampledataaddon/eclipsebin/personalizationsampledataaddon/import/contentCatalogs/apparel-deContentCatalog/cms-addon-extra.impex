# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------

$lang=de
$defaultPromoGrp=apparelDEPromoGrp
$defaultRuleStatus=UNPUBLISHED
$contentCatalog=apparel-deContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=Staged])[default=$contentCatalog:Staged]
$defaultPassword=12341234
$promotionModuleName="promotions-module"
$promotionPreviewModuleName="promotions-preview-module"

INSERT_UPDATE CatalogVersion; catalog(id)[unique=true]; version[unique=true];readPrincipals(uid)[mode=append];writePrincipals(uid)[mode=append];;;;
;apparel-deContentCatalog;Staged;cxmanagergroup,cxreadonlygroup;cxmanagergroup;;;;
;apparel-deContentCatalog;Online;cxmanagergroup,cxreadonlygroup;cxmanagergroup;;;;

INSERT_UPDATE CxSegment;code[unique=true];
;CATEGORY Burton;
;CATEGORY 250600;
;CATEGORY skigear;
;CATEGORY sandals;
;BeachwearLover;
;FlannelShirtsFan;
;FlipFlopsEnthusiast;
;ShadesConnoisseur;
;BeltsOldHand;
;HatsFan;

INSERT_UPDATE Customer;uid[unique=true];groups(uid);password[default=$defaultPassword];name;&userref;
;menshortslover@hybris.com;customergroup;;Men Shorts Lover;;menshortslover@hybris.com;
;shortslover@hybris.com;customergroup;;Shorts Lover;;shortslover@hybris.com;
;burtonlover@hybris.com;customergroup;;Burton Lover;;burtonlover@hybris.com;

INSERT_UPDATE CxUserToSegment;segment(code)[unique=true];user(uid)[unique=true];affinity[default=1];
;Men;menshortslover@hybris.com;
;30-39;menshortslover@hybris.com;
;CATEGORY 250600;menshortslover@hybris.com;
;CATEGORY Burton;womenvipgold@hybris.com;
;CATEGORY 250600;shortslover@hybris.com;
;CATEGORY Burton;burtonlover@hybris.com;
;CATEGORY skigear;vipsilver@hybris.com;

INSERT_UPDATE CxCustomizationsGroup;code;$contentCV[unique=true]
"#% beforeEach: de.hybris.platform.personalizationsampledataaddon.util.CxAddOnSampleDataUtils.verifyIfCxCustomizationsGroupExists(line, 1, impex, 1);"
;apparelDECustomizationGroup;;

INSERT_UPDATE CxCustomization;code[unique=true];name;$contentCV[unique=true];group(code,$contentCV)[default='apparelDECustomizationGroup'];status(code)[default=ENABLED]
;SummerSale;SummerSale;;
;SpringSale;SpringSale;;
;WinterSale;WinterSale;;
;CategoryLover;CategoryLover;;
;VIP;VIP;;

INSERT_UPDATE CxCmsAction;componentId;code[unique=true];type(code);target;&actionRef;$contentCV[unique=true];containerId;componentCatalog[default=$contentCatalog];
"#% beforeEach: de.hybris.platform.personalizationsampledataaddon.util.CxAddOnSampleDataUtils.verifyIfCxActionExists(line, 2, impex, 5);"
#banner
;springSaleMenSplashBannerComponent;actionSpringSaleMenBanner;PLAIN;cxCmsActionPerformable;actionSpringSaleMenBanner;;Section1SlotHomepageCxContainer
;springSaleWomenSplashBannerComponent;actionSpringSaleWomenBanner;PLAIN;cxCmsActionPerformable;actionSpringSaleWomenBanner;;Section1SlotHomepageCxContainer
;springSaleWomenSplashBannerComponent;actionSpringSaleVipSilverWomenBanner;PLAIN;cxCmsActionPerformable;actionSpringSaleVipSilverWomenBanner;;Section1SlotHomepageCxContainer
;summerSaleMenSplashBannerComponent;actionSummerSaleMenBanner;PLAIN;cxCmsActionPerformable;actionSummerSaleMenBanner;;Section1SlotHomepageCxContainer
;summerSaleWomenSplashBannerComponent;actionSummerSaleWomenBanner;PLAIN;cxCmsActionPerformable;actionSummerSaleWomenBanner;;Section1SlotHomepageCxContainer
;springSaleWomenSplashBannerComponent;actionSpringSaleOver30AndShortsLoverAndNotVipGold;PLAIN;cxCmsActionPerformable;actionSpringSaleOver30AndShortsLoverAndNotVipGold;;Section1SlotHomepageCxContainer
;springSaleWomenSplashBannerComponent;actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover;PLAIN;cxCmsActionPerformable;actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover;;Section1SlotHomepageCxContainer
;winterSaleMenSplashBannerComponent;actionWinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;PLAIN;cxCmsActionPerformable;actionWinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;;Section1SlotHomepageCxContainer
;loyalCustomersSplashBannerComponent;actionLoyalCustomers;PLAIN;cxCmsActionPerformable;actionLoyalCustomers;;Section1SlotHomepageCxContainer
;springShopSplashBannerComponent;actionSpringShop;PLAIN;cxCmsActionPerformable;actionSpringShop;;Section1SlotHomepageCxContainer
;summerCornerSplashBannerComponent;actionSummerCorner;PLAIN;cxCmsActionPerformable;actionSummerCorner;;Section1SlotHomepageCxContainer
#content 2A
;burtonLoverContentComponent;actionSpringSaleBurtonLoverContent2A;PLAIN;cxCmsActionPerformable;actionSpringSaleBurtonLoverContent2A;;Section2ASlotHomepageCxContainer
;burtonLoverContentComponent;actionSummerSaleBurtonLoverContent2A;PLAIN;cxCmsActionPerformable;actionSummerSaleBurtonLoverContent2A;;Section2ASlotHomepageCxContainer
;burtonLoverContentComponent;actionBurtonLoverContent2A;PLAIN;cxCmsActionPerformable;actionBurtonLoverContent2A;;Section2ASlotHomepageCxContainer
;shortsLoverContentComponent;actionSummerSaleShortsLoverContent2A;PLAIN;cxCmsActionPerformable;actionSummerSaleShortsLoverContent2A;;Section2ASlotHomepageCxContainer
;shortsLoverContentComponent;actionShortsLoverContent2A;PLAIN;cxCmsActionPerformable;actionShortsLoverContent2A;;Section2ASlotHomepageCxContainer
;shortsLoverContentComponent;actionSpringSaleOver30AndShortsLoverAndNotVipGold2A;PLAIN;cxCmsActionPerformable;actionSpringSaleOver30AndShortsLoverAndNotVipGold2A;;Section2ASlotHomepageCxContainer
#content 2B
;vipGoldContentComponent;actionVipGoldContent2B;PLAIN;cxCmsActionPerformable;actionVipGoldContent2B;;Section2BSlotHomepageCxContainer
;vipGoldContentComponent;actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover2B;PLAIN;cxCmsActionPerformable;actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover2B;;Section2BSlotHomepageCxContainer
;vipGoldContentComponent;actionSpringSaleVipGoldContent2B;PLAIN;cxCmsActionPerformable;actionSpringSaleVipGoldContent2B;;Section2BSlotHomepageCxContainer
;vipGoldContentComponent;actionSummerSaleVipGoldContent2B;PLAIN;cxCmsActionPerformable;actionSummerSaleVipGoldContent2B;;Section2BSlotHomepageCxContainer
;vipSilverContentComponent;actionWinterSaleVipSilverContent2B;PLAIN;cxCmsActionPerformable;actionWinterSaleVipSilverContent2B;;Section2BSlotHomepageCxContainer
;vipSilverContentComponent;actionVipSilverContent2B;PLAIN;cxCmsActionPerformable;actionVipSilverContent2B;;Section2BSlotHomepageCxContainer
;vipSilverContentComponent;actionSpringSaleVipSilverWomenContent2B;PLAIN;cxCmsActionPerformable;actionSpringSaleVipSilverWomenContent2B;;Section2BSlotHomepageCxContainer
;vipBronzeContentComponent;actionVipBronzeContent2B;PLAIN;cxCmsActionPerformable;actionVipBronzeContent2B;;Section2BSlotHomepageCxContainer
;vipBronzeContentComponent;actionSpringSaleVipBronzeContent2B;PLAIN;cxCmsActionPerformable;actionSpringSaleVipBronzeContent2B;;Section2BSlotHomepageCxContainer

INSERT_UPDATE CxVariation;code[unique=true];name;$contentCV[unique=true];actions(&actionRef)[mode=append];enabled[default=true];customization(code,$contentCV)[unique=true];status(code)[default=ENABLED]
;SpringSaleVipBronzeMen;SpringSaleVipBronzeMen;;actionSpringSaleMenBanner,actionSpringSaleVipBronzeContent2B;;SpringSale;
;SpringSaleVipGoldWomen;SpringSaleVipGoldWomen;;actionSpringSaleWomenBanner,actionSpringSaleVipGoldContent2B,actionSpringSaleBurtonLoverContent2A;;SpringSale;
;SpringSaleVipSilverWomen;SpringSaleVipSilverWomen;;actionSpringSaleVipSilverWomenBanner,actionSpringSaleVipSilverWomenContent2B;;SpringSale;
;SummerSaleMenShortsLover;SummerSaleMenShortsLover;;actionSummerSaleMenBanner,actionSummerSaleShortsLoverContent2A;;SummerSale;
;SummerSaleVipGoldWomen;SummerSaleVipGoldWomen;;actionSummerSaleWomenBanner,actionSummerSaleVipGoldContent2B,actionSummerSaleBurtonLoverContent2A;;SummerSale;
;SpringSaleOver30AndShortsLoverAndNotVipGold;SpringSaleOver30AndShortsLoverAndNotVipGold;;actionSpringSaleOver30AndShortsLoverAndNotVipGold,actionSpringSaleOver30AndShortsLoverAndNotVipGold2A;;SpringSale;
;SpringSaleVipGoldAndNotShortsLoverNorSandalsLover;SpringSaleVipGoldAndNotShortsLoverNorSandalsLover;;actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover,actionSpringSaleVipGoldAndNotShortsLoverNorSandalsLover2B;;SpringSale;
;VipGold;VipGold;;actionVipGoldContent2B;;VIP;
;VipSilver;VipSilver;;actionVipSilverContent2B;;VIP;
;VipBronze;VipBronze;;actionVipBronzeContent2B;;VIP;
;ShortsLover;ShortsLover;;actionShortsLoverContent2A;;CategoryLover;
;BurtonLover;BurtonLover;;actionBurtonLoverContent2A;;CategoryLover;
;WinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;WinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;;actionWinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan,actionWinterSaleVipSilverContent2B;;WinterSale;
;VIPDefault;VIPDefault;;actionLoyalCustomers;;VIP;
;SpringSaleDefault;SpringSaleDefault;;actionSpringShop;;SpringSale;DISABLED
;SummerSaleDefault;SummerSaleDefault;;actionSummerCorner;;SummerSale;DISABLED

INSERT_UPDATE CxSegmentTrigger;code[unique=true];variation(code,$contentCV)[unique=true];$contentCV[unique=true];segments(code);groupBy(code)[default='OR'];
;trigger1;SpringSaleVipBronzeMen;;Men,VIPBronze;AND
;trigger2;SpringSaleVipGoldWomen;;Women,VIPGold,CATEGORY Burton;AND
;trigger3;SpringSaleVipSilverWomen;;Women,VIPSilver;AND
;trigger4;SummerSaleMenShortsLover;;Men,CATEGORY 250600;AND
;trigger5;SummerSaleVipGoldWomen;;Women,VIPGold,CATEGORY Burton;AND
;trigger6;VipGold;;VIPGold;
;trigger7;VipSilver;;VIPSilver;
;trigger8;VipBronze;;VIPBronze;
;trigger9;ShortsLover;;CATEGORY 250600;
;trigger10;BurtonLover;;CATEGORY Burton;

INSERT_UPDATE CxExpressionTrigger;code[unique=true];variation(code,$contentCV)[unique=true];$contentCV[unique=true];expression;
;expressionTrigger1;WinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;;"{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""CATEGORY skigear""},{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""VIPSilver""},{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""20-29""},{""type"":""segmentExpression"",""code"":""Men""}],""operator"":""AND""}],""operator"":""OR""}],""operator"":""AND""}"
;expressionTrigger2;SpringSaleOver30AndShortsLoverAndNotVipGold;;"{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""30-39""},{""type"":""segmentExpression"",""code"":""CATEGORY 250600""},{""type"":""negationExpression"",""element"":{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""VIPGold""}],""operator"":""AND""}}],""operator"":""AND""}"
;expressionTrigger3;SpringSaleVipGoldAndNotShortsLoverNorSandalsLover;;"{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""VIPGold""},{""type"":""negationExpression"",""element"":{""type"":""groupExpression"",""elements"":[{""type"":""segmentExpression"",""code"":""CATEGORY 250600""},{""type"":""segmentExpression"",""code"":""CATEGORY sandals""}],""operator"":""AND""}}],""operator"":""AND""}"

INSERT_UPDATE CxDefaultTrigger;code[unique=true];variation(code,$contentCV)[unique=true];$contentCV[unique=true];
;defaultTrigger1;VIPDefault;
;defaultTrigger2;SpringSaleDefault;
;defaultTrigger3;SummerSaleDefault;

UPDATE CxCustomization;code[unique=true];name;$contentCV[unique=true];variations(code,$contentCV)
;SpringSale;SpringSale;;SpringSaleVipBronzeMen,SpringSaleVipGoldWomen,SpringSaleVipSilverWomen,SpringSaleOver30AndShortsLoverAndNotVipGold,SpringSaleVipGoldAndNotShortsLoverNorSandalsLover,SpringSaleDefault;
;SummerSale;SummerSale;;SummerSaleMenShortsLover,SummerSaleVipGoldWomen,SummerSaleDefault;
;WinterSale;WinterSale;;WinterSaleSkiGearLoverAndVIPSilverOrOver20AndMan;
;CategoryLover;CategoryLover;;ShortsLover,BurtonLover;
;VIP;VIP;;VipGold,VipSilver,VipBronze,VIPDefault;

INSERT_UPDATE PromotionSourceRule;code[unique=true];name[lang=$lang];messageFired[lang=$lang];priority;stackable[default=false];status(code)[default=$defaultRuleStatus];website(identifier)[default=$defaultPromoGrp];ruleGroup(code);conditions;actions;excludeFromStorefrontDisplay[default=true];
"#% beforeEach:
if(de.hybris.platform.personalizationsampledataaddon.util.CxAddOnSampleDataUtils.isPromotionSourceRuleUnmodifiable(line.get(new Integer(1)))){
    line.clear();
}";
"#% afterEach: de.hybris.platform.core.Registry.getApplicationContext().getBean(""ruleEngineSystemSetup"").registerSourceRuleForDeployment(impex.getLastImportedItem(), new String[]{$promotionModuleName, $promotionPreviewModuleName});"
;personalized_de_burton_lover_fixed_discount;Burton product category fixed discount;10 Euro Preisnachlass auf Ihre Lieblingsprodukte;80;;;;productPromotionRuleGroup;"[{""definitionId"":""y_qualifying_categories"",""parameters"":{""quantity"":{""type"":""java.lang.Integer"",""value"":1},""categories_operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.CollectionOperator)"",""value"":""CONTAINS_ANY""},""excluded_categories"":{""type"":""List(ItemType(Category))""},""categories"":{""type"":""List(ItemType(Category))"",""value"":[""Burton""]},""excluded_products"":{""type"":""List(ItemType(Product))"",""value"":[]},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_order_entry_fixed_discount"",""parameters"":{""value"":{""type"":""Map(ItemType(Currency),java.math.BigDecimal)"",""value"":{""EUR"":10}}}}]"
;personalized_de_shorts_lover_fixed_discount;Shorts product category fixed discount;10 Euro Preisnachlass auf Ihre Lieblingsprodukte;80;;;;productPromotionRuleGroup;"[{""definitionId"":""y_qualifying_categories"",""parameters"":{""quantity"":{""type"":""java.lang.Integer"",""value"":1},""categories_operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.CollectionOperator)"",""value"":""CONTAINS_ANY""},""excluded_categories"":{""type"":""List(ItemType(Category))""},""categories"":{""type"":""List(ItemType(Category))"",""value"":[""250600""]},""excluded_products"":{""type"":""List(ItemType(Product))"",""value"":[]},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_order_entry_fixed_discount"",""parameters"":{""value"":{""type"":""Map(ItemType(Currency),java.math.BigDecimal)"",""value"":{""EUR"":10}}}}]"
;personalized_de_buy_2_shorts_get_1_shorts_free;Buy 2 shorts get 1 for free;Zwei Artikel zum Preis von einem;90;;;;productPromotionRuleGroup;"[{""definitionId"":""y_container"",""parameters"":{""id"":{""type"":""java.lang.String"",""value"":""CONTAINER_X""}},""children"":[{""definitionId"":""y_group"",""parameters"":{""operator"":{""type"":""Enum(de.hybris.platform.ruleengineservices.definitions.conditions.RuleGroupOperator)"",""value"":""OR""}},""children"":[{""definitionId"":""y_qualifying_categories"",""parameters"":{""quantity"":{""type"":""java.lang.Integer"",""value"":2},""categories_operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.CollectionOperator)"",""value"":""CONTAINS_ANY""},""excluded_categories"":{""type"":""List(ItemType(Category))""},""categories"":{""type"":""List(ItemType(Category))"",""value"":[""250600""]},""excluded_products"":{""type"":""List(ItemType(Product))""},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]}]}]},{""definitionId"":""y_container"",""parameters"":{""id"":{""type"":""java.lang.String"",""value"":""CONTAINER_Y""}},""children"":[{""definitionId"":""y_group"",""parameters"":{""operator"":{""type"":""Enum(de.hybris.platform.ruleengineservices.definitions.conditions.RuleGroupOperator)"",""value"":""OR""}},""children"":[{""definitionId"":""y_qualifying_categories"",""parameters"":{""quantity"":{""type"":""java.lang.Integer"",""value"":1},""categories_operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.CollectionOperator)"",""value"":""CONTAINS_ANY""},""excluded_categories"":{""type"":""List(ItemType(Category))""},""categories"":{""type"":""List(ItemType(Category))"",""value"":[""250600""]},""excluded_products"":{""type"":""List(ItemType(Product))""},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]}]}]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_partner_order_entry_percentage_discount"",""parameters"":{""selection_strategy"":{""type"":""Enum(de.hybris.platform.ruleengineservices.enums.OrderEntrySelectionStrategy)"",""value"":""CHEAPEST""},""qualifying_containers"":{""type"":""Map(java.lang.String,java.lang.Integer)"",""value"":{""CONTAINER_X"":2}},""target_containers"":{""type"":""Map(java.lang.String,java.lang.Integer)"",""value"":{""CONTAINER_Y"":1}},""value"":{""type"":""java.math.BigDecimal"",""value"":100}}}]"
;personalized_de_vip_gold_percentage_discount_for_order_threshold;VIP Gold customer specific percentage discount for order threshold;10 % Preisnachlass für Stammkunden und Bestellungen über 75 Euro;100;true;;;customerPromotionRuleGroup;"[{""definitionId"":""y_cart_total"",""parameters"":{""value"":{""type"":""Map(ItemType(Currency),java.math.BigDecimal)"",""value"":{""EUR"":75}},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_order_percentage_discount"",""parameters"":{""value"":{""type"":""java.math.BigDecimal"",""value"":10}}}]";true
;personalized_de_vip_silver_percentage_discount_for_order_threshold;VIP Silver customer specific percentage discount for order threshold;10 % Preisnachlass für Stammkunden und Bestellungen über 100 Euro;100;true;;;customerPromotionRuleGroup;"[{""definitionId"":""y_cart_total"",""parameters"":{""value"":{""type"":""Map(ItemType(Currency),java.math.BigDecimal)"",""value"":{""EUR"":100}},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_order_percentage_discount"",""parameters"":{""value"":{""type"":""java.math.BigDecimal"",""value"":10}}}]";true
;personalized_de_vip_bronze_percentage_discount_for_order_threshold;VIP Bronze customer specific percentage discount for order threshold;5 % Preisnachlass für Stammkunden und Bestellungen über 150 Euro;100;true;;;customerPromotionRuleGroup;"[{""definitionId"":""y_cart_total"",""parameters"":{""value"":{""type"":""Map(ItemType(Currency),java.math.BigDecimal)"",""value"":{""EUR"":150}},""operator"":{""type"":""Enum(de.hybris.platform.ruledefinitions.AmountOperator)"",""value"":""GREATER_THAN_OR_EQUAL""}},""children"":[]},{""definitionId"":""cx_aware_promotion"",""parameters"":{},""children"":[]}]";"[{""definitionId"":""y_order_percentage_discount"",""parameters"":{""value"":{""type"":""java.math.BigDecimal"",""value"":5}}}]";true

INSERT_UPDATE CxPromotionAction;code[unique=true];promotionId[unique=true];variation(code,$contentCV)[unique=true];target;type(code);$contentCV[unique=true]
"#% beforeEach: de.hybris.platform.personalizationsampledataaddon.util.CxAddOnSampleDataUtils.verifyIfCxActionExists(line, 1, impex, 5);"
;promotionaction_burton_lover_fixed_discount;personalized_de_burton_lover_fixed_discount;BurtonLover;cxPromotionActionPerformable;PLAIN;
;promotionaction_shorts_lover_fixed_discount;personalized_de_shorts_lover_fixed_discount;ShortsLover;cxPromotionActionPerformable;PLAIN;
;promotionaction_buy_2_shorts_get_1_shorts_free;personalized_de_buy_2_shorts_get_1_shorts_free;ShortsLover;cxPromotionActionPerformable;PLAIN;
;promotionaction_shorts_lover_fixed_discount;personalized_de_shorts_lover_fixed_discount;SpringSaleOver30AndShortsLoverAndNotVipGold;cxPromotionActionPerformable;PLAIN;
;promotionaction_buy_2_shorts_get_1_shorts_free;personalized_de_buy_2_shorts_get_1_shorts_free;SpringSaleOver30AndShortsLoverAndNotVipGold;cxPromotionActionPerformable;PLAIN;
;promotionaction_vip_gold_customer_percentage_discount_for_order_threshold;personalized_de_vip_gold_percentage_discount_for_order_threshold;VipGold;cxPromotionActionPerformable;PLAIN;
;promotionaction_vip_silver_customer_percentage_discount_for_order_threshold;personalized_de_vip_silver_percentage_discount_for_order_threshold;VipSilver;cxPromotionActionPerformable;PLAIN;
;promotionaction_vip_bronze_customer_percentage_discount_for_order_threshold;personalized_de_vip_bronze_percentage_discount_for_order_threshold;VipBronze;cxPromotionActionPerformable;PLAIN;

