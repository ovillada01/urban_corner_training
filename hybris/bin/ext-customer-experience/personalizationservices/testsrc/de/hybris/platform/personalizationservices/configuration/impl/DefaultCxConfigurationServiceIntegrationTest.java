package de.hybris.platform.personalizationservices.configuration.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.basecommerce.model.site.BaseSiteModel;
import de.hybris.platform.personalizationservices.AbstractCxServiceTest;
import de.hybris.platform.personalizationservices.configuration.CxConfigurationService;
import de.hybris.platform.personalizationservices.constants.PersonalizationservicesConstants;
import de.hybris.platform.personalizationservices.model.config.CxConfigModel;
import de.hybris.platform.personalizationservices.model.config.CxUrlVoterConfigModel;
import de.hybris.platform.servicelayer.config.ConfigurationService;
import de.hybris.platform.site.BaseSiteService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Sets;


@IntegrationTest
public class DefaultCxConfigurationServiceIntegrationTest extends AbstractCxServiceTest
{
	private static final String CURRENT_BASE_SITE_UID = "testSite";
	private static final BigDecimal CURRENT_BASE_SITE_MIN_AFFINITY = BigDecimal.valueOf(0.5);
	private static final Integer CURRENT_BASE_SITE_ACTION_RESULT_MAX_REPEAT = Integer.valueOf(1);
	private static final Set<String> CURRENT_BASE_SITE_LOGIN_ACTIONS = Sets.newHashSet("RECALCULATE", "ASYNC_PROCESS");
	private static final String CURRENT_BASE_SITE_URL_DEFAULT_CODE = "default";
	private static final String CURRENT_BASE_SITE_URL_DEFAULT_PATTERN = ".*";
	private static final Set<String> CURRENT_BASE_SITE_URL_DEFAULT_ACTIONS = Sets.newHashSet("LOAD");
	private static final String CURRENT_BASE_SITE_URL_CHECKOUT_CODE = "checkout";
	private static final String CURRENT_BASE_SITE_URL_CHECKOUT_PATTERN = ".*/checkout";
	private static final Set<String> CURRENT_BASE_SITE_URL_CHECKOUT_ACTIONS = Sets.newHashSet("ASYNC_PROCESS");
	private static final String CURRENT_BASE_SITE_URL_CART_CODE = "cart";
	private static final String CURRENT_BASE_SITE_URL_CART_PATTERN = ".*/checkout";
	private static final Set<String> CURRENT_BASE_SITE_URL_CART_ACTIONS = Sets.newHashSet("ASYNC_PROCESS", "UPDATE");

	private static final String BASE_SITE_1_UID = "testSite1";
	private static final BigDecimal BASE_SITE_1_MIN_AFFINITY = BigDecimal.valueOf(0.75);
	private static final Integer BASE_SITE_1_ACTION_RESULT_MAX_REPEAT = Integer.valueOf(2);
	private static final Set<String> BASE_SITE_1_LOGIN_ACTIONS = Sets.newHashSet("LOAD");
	private static final String BASE_SITE_1_URL_DEFAULT_CODE = "default";
	private static final String BASE_SITE_1_URL_DEFAULT_PATTERN = ".*";
	private static final Set<String> BASE_SITE_1_URL_DEFAULT_ACTIONS = Sets.newHashSet("LOAD");

	private static final String BASE_SITE_2_UID = "testSite2";

	private static final String BASE_SITE_3_UID = "testSite3";

	private static final String LOAD_ACTION_RESULT_MAX_REPEAT_PARAMETER = "4";
	private static final String MIN_AFFINITY_PARAMETER = "0.90";
	private static final String LOGIN_ACTIONS_PARAMETER = "RECALCULATE\\,ASYNC_PROCESS";

	@Resource
	private ConfigurationService configurationService;

	@Resource
	private CxConfigurationService cxConfigurationService;

	@Resource
	private BaseSiteService baseSiteService;

	@Override
	@Before
	public void setUp() throws Exception
	{
		createCoreData();
		createDefaultCatalog();
		importCsv("/personalizationservices/test/testdata_cxconfig.impex", "utf-8");
	}

	@Test
	public void findConfigurationForCurrentBaseSiteTest()
	{
		//having
		baseSiteService.setCurrentBaseSite(CURRENT_BASE_SITE_UID, false);

		//when
		final Optional<CxConfigModel> configuration = cxConfigurationService.getConfiguration();

		//then
		assertNotNull(configuration);
		assertTrue(configuration.isPresent());
		final CxConfigModel cxConfigModel = configuration.get();
		assertTrue(cxConfigModel.getBaseSites().stream().filter(bs -> StringUtils.equals(bs.getUid(), CURRENT_BASE_SITE_UID))
				.findAny().isPresent());
		assertEquals(CURRENT_BASE_SITE_MIN_AFFINITY, cxConfigModel.getMinAffinity());
		assertEquals(CURRENT_BASE_SITE_ACTION_RESULT_MAX_REPEAT, cxConfigModel.getActionResultMaxRepeat());
		assertEquals(CURRENT_BASE_SITE_LOGIN_ACTIONS, cxConfigModel.getUserChangedActions());
	}

	@Test
	public void findConfigurationForBaseSiteTest()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_1_UID);

		//when
		final Optional<CxConfigModel> configuration = cxConfigurationService.getConfiguration(baseSite);

		//then
		assertNotNull(configuration);
		assertTrue(configuration.isPresent());
		final CxConfigModel cxConfigModel = configuration.get();
		assertTrue(cxConfigModel.getBaseSites().stream().filter(bs -> StringUtils.equals(bs.getUid(), BASE_SITE_1_UID)).findAny()
				.isPresent());
		assertEquals(BASE_SITE_1_MIN_AFFINITY, cxConfigModel.getMinAffinity());
		assertEquals(BASE_SITE_1_ACTION_RESULT_MAX_REPEAT, cxConfigModel.getActionResultMaxRepeat());
		assertEquals(BASE_SITE_1_LOGIN_ACTIONS, cxConfigModel.getUserChangedActions());
	}

	@Test
	public void findNoConfigurationForBaseSiteTest()
	{
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_3_UID);

		final Optional<CxConfigModel> configuration = cxConfigurationService.getConfiguration(baseSite);

		assertNotNull(configuration);
		assertFalse(configuration.isPresent());
	}

	@Test(expected = IllegalArgumentException.class)
	public void findConfigurationForNullBaseSiteTest()
	{
		cxConfigurationService.getConfiguration(null);
	}

	@Test
	public void testGetActionResultMaxRepeatForCurrentBaseSite()
	{
		//having
		baseSiteService.setCurrentBaseSite(CURRENT_BASE_SITE_UID, false);

		//when
		final Integer actionResultMaxRepeat = cxConfigurationService.getActionResultMaxRepeat();

		//then
        assertNotNull(actionResultMaxRepeat);
		assertEquals(CURRENT_BASE_SITE_ACTION_RESULT_MAX_REPEAT, actionResultMaxRepeat);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetActionResultMaxRepeatForNullBaseSite()
	{
		cxConfigurationService.getActionResultMaxRepeat(null);
	}

	@Test
	public void testGetActionResultMaxRepeatForBaseSite()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_1_UID);

		//when
		final Integer actionResultMaxRepeat = cxConfigurationService.getActionResultMaxRepeat(baseSite);

		//then
        assertNotNull(actionResultMaxRepeat);
		assertEquals(BASE_SITE_1_ACTION_RESULT_MAX_REPEAT, actionResultMaxRepeat);
	}


	@Test
	public void testNoActionResultMaxRepeatForBaseSiteShouldFallBack()
	{
		//having
		configurationService.getConfiguration().addProperty(PersonalizationservicesConstants.LOAD_ACTION_RESULT_MAX_REPEAT,
				LOAD_ACTION_RESULT_MAX_REPEAT_PARAMETER);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_3_UID);

		//when
		final Integer actionResultMaxRepeat = cxConfigurationService.getActionResultMaxRepeat(baseSite);

		//then
		assertEquals(Integer.valueOf(LOAD_ACTION_RESULT_MAX_REPEAT_PARAMETER), actionResultMaxRepeat);
	}

	@Test
	public void testGetMinAffinityForCurrentBaseSite()
	{
		//having
		baseSiteService.setCurrentBaseSite(CURRENT_BASE_SITE_UID, false);

		//when
		final BigDecimal minAffinity = cxConfigurationService.getMinAffinity();

		//then
		assertNotNull(minAffinity);
		assertTrue(CURRENT_BASE_SITE_MIN_AFFINITY.equals(minAffinity));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetMinAffinityForNullBaseSite()
	{
		cxConfigurationService.getMinAffinity(null);
	}

	@Test
	public void testGetMinAffinityForBaseSite()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_1_UID);

		//when
		final BigDecimal minAffinity = cxConfigurationService.getMinAffinity(baseSite);

		//then
		assertNotNull(minAffinity);
		assertTrue(BASE_SITE_1_MIN_AFFINITY.equals(minAffinity));
	}


	@Test
	public void testNoMinAffinityForBaseSiteShouldFallBack()
	{
		//having
		configurationService.getConfiguration().addProperty(PersonalizationservicesConstants.MIN_AFFINITY_PROPERTY,
				MIN_AFFINITY_PARAMETER);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_3_UID);

		//when
		final BigDecimal minAffinity = cxConfigurationService.getMinAffinity(baseSite);

		//then
		assertNotNull(minAffinity);
		assertTrue(new BigDecimal(MIN_AFFINITY_PARAMETER).equals(minAffinity));
	}

	@Test
	public void testGetUserChangedActionsForCurrentBaseSite()
	{
		//having
		baseSiteService.setCurrentBaseSite(CURRENT_BASE_SITE_UID, false);

		//when
		final Set<String> userChangedActions = cxConfigurationService.getUserChangedActions();

		//then
		assertNotNull(userChangedActions);
		assertTrue(CURRENT_BASE_SITE_LOGIN_ACTIONS.equals(userChangedActions));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testGetUserChangedActionsForNullBaseSite()
	{
		cxConfigurationService.getUserChangedActions(null);
	}

	@Test
	public void testGetUserChangedActionsForBaseSite()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_1_UID);

		//when
		final Set<String> userChangedActions = cxConfigurationService.getUserChangedActions(baseSite);

		//then
		assertNotNull(userChangedActions);
		assertTrue(BASE_SITE_1_LOGIN_ACTIONS.equals(userChangedActions));
	}


	@Test
	public void testNoUserChangedActionsForBaseSiteShouldFallBack()
	{
		//having
		configurationService.getConfiguration().addProperty(PersonalizationservicesConstants.USER_CHANGED_ACTIONS_PROPERTY,
				LOGIN_ACTIONS_PARAMETER);

		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_3_UID);

		//when
		final Set<String> userChangedActions = cxConfigurationService.getUserChangedActions(baseSite);

		//then
		assertNotNull(userChangedActions);
		final String actions = configurationService.getConfiguration()
				.getString(PersonalizationservicesConstants.USER_CHANGED_ACTIONS_PROPERTY);
		assertTrue(Sets.newHashSet(actions.split(",")).equals(userChangedActions));
	}

	@Test
	public void findUrlVoterConfigurationsForCurrentBaseSiteTest()
	{
		//having
		baseSiteService.setCurrentBaseSite(CURRENT_BASE_SITE_UID, false);

		//when
		final List<CxUrlVoterConfigModel> urlVoterConfigurations = cxConfigurationService.getUrlVoterConfigurations();

		//then
		assertNotNull(urlVoterConfigurations);
		assertEquals(3, urlVoterConfigurations.size());

		final Optional<CxUrlVoterConfigModel> defaultUrlVoterConfigModel = urlVoterConfigurations.stream()
				.filter(c -> StringUtils.equals(CURRENT_BASE_SITE_URL_DEFAULT_CODE, c.getCode())).findAny();
		assertTrue(defaultUrlVoterConfigModel.isPresent());
		assertEquals(CURRENT_BASE_SITE_URL_DEFAULT_PATTERN, defaultUrlVoterConfigModel.get().getUrlRegexp());
		assertEquals(CURRENT_BASE_SITE_URL_DEFAULT_ACTIONS, defaultUrlVoterConfigModel.get().getActions());

		final Optional<CxUrlVoterConfigModel> checkoutUrlVoterConfigModel = urlVoterConfigurations.stream()
				.filter(c -> StringUtils.equals(CURRENT_BASE_SITE_URL_CHECKOUT_CODE, c.getCode())).findAny();
		assertTrue(checkoutUrlVoterConfigModel.isPresent());
		assertEquals(CURRENT_BASE_SITE_URL_CHECKOUT_PATTERN, checkoutUrlVoterConfigModel.get().getUrlRegexp());
		assertEquals(CURRENT_BASE_SITE_URL_CHECKOUT_ACTIONS, checkoutUrlVoterConfigModel.get().getActions());

		final Optional<CxUrlVoterConfigModel> cartUrlVoterConfigModel = urlVoterConfigurations.stream()
				.filter(c -> StringUtils.equals(CURRENT_BASE_SITE_URL_CART_CODE, c.getCode())).findAny();
		assertTrue(cartUrlVoterConfigModel.isPresent());
		assertEquals(CURRENT_BASE_SITE_URL_CART_PATTERN, cartUrlVoterConfigModel.get().getUrlRegexp());
		assertEquals(CURRENT_BASE_SITE_URL_CART_ACTIONS, cartUrlVoterConfigModel.get().getActions());
	}

	@Test
	public void findUrlVoterConfigurationsForBaseSiteTest()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_1_UID);

		//when
		final List<CxUrlVoterConfigModel> urlVoterConfigurations = cxConfigurationService.getUrlVoterConfigurations(baseSite);

		//then
		assertNotNull(urlVoterConfigurations);
		assertEquals(1, urlVoterConfigurations.size());

		final Optional<CxUrlVoterConfigModel> defaultUrlVoterConfigModel = urlVoterConfigurations.stream()
				.filter(c -> StringUtils.equals(BASE_SITE_1_URL_DEFAULT_CODE, c.getCode())).findAny();
		assertTrue(defaultUrlVoterConfigModel.isPresent());
		assertEquals(BASE_SITE_1_URL_DEFAULT_PATTERN, defaultUrlVoterConfigModel.get().getUrlRegexp());
		assertEquals(BASE_SITE_1_URL_DEFAULT_ACTIONS, defaultUrlVoterConfigModel.get().getActions());
	}

	@Test
	public void findNoUrlVoterConfigurationsForBaseSiteTest()
	{
		//having
		final BaseSiteModel baseSite = baseSiteService.getBaseSiteForUID(BASE_SITE_2_UID);

		//when
		final List<CxUrlVoterConfigModel> urlVoterConfigurations = cxConfigurationService.getUrlVoterConfigurations(baseSite);

		//then
		assertNotNull(urlVoterConfigurations);
		assertTrue(urlVoterConfigurations.isEmpty());
	}


	@Test(expected = IllegalArgumentException.class)
	public void findUrlVoterConfigurationsForNullBaseSiteTest()
	{
		cxConfigurationService.getUrlVoterConfigurations(null);
	}
}
