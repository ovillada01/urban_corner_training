/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.personalizationservices.action.impl;

import de.hybris.bootstrap.annotations.UnitTest;

import org.junit.Ignore;


@UnitTest
@Ignore
public class DefaultCxActionServiceTest
{
	//we don't have test here because most of the logic in default serivce is dao based.
	//and because we don't have concrete implementation of CxAbstractAction we weren't able to create valid tests
	//all tests for DefaultCxActionService are in de.hybris.platform.personalizationcms.service.DefaultCxActionServiceIntegrationTest
}
