# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------

type.CxSegment.name=Сегмент
type.CxSegment.description=Сегмент персонализации 
type.CxSegment.code.name=Код
type.CxSegment.code.description=Уникальный код
type.CxSegment.triggers.name=Триггеры
type.CxSegment.triggers.description=Триггеры, указывающие вариант
type.CxSegment.expressionTriggers.name=Триггеры выражения
type.CxSegment.expressionTriggers.description=Триггеры выражения, указывающие вариант
type.CxSegment.users.name=Пользователи
type.CxSegment.users.description=Пользователи в сегменте
type.CxSegment.userToSegments.name=Ссылки на пользователей
type.CxSegment.userToSegments.description=Ссылки на пользователей, которым назначен сегмент

type.CxVariation.name=Вариант
type.CxVariation.description=Вариант клиента
type.CxVariation.code.name=Код
type.CxVariation.code.description=Уникальный код
type.CxVariation.catalogversion.name=Версия каталога
type.CxVariation.catalogversion.description=Версия каталога
type.CxVariation.name.name=Имя
type.CxVariation.name.description=Имя варианта
type.CxVariation.actions.name=Действия
type.CxVariation.actions.description=Действия для варианта
type.CxVariation.active.name=Активно
type.CxVariation.active.description=Вариант активен 
type.CxVariation.enabled.name=Включено
type.CxVariation.enabled.description=Вариант включен 
type.CxVariation.status.name=Статус
type.CxVariation.status.description=Статус варианта 
type.CxVariation.customization.name=Пользовательская настройка
type.CxVariation.customization.description=Пользовательская настройка варианта
type.CxVariation.triggers.name=Триггеры 
type.CxVariation.triggers.description=Триггеры, указывающие вариант
type.CxVariation.rank.name=Ранг (приоритет)
type.CxVariation.rank.description=Ранг (приоритет) варианта

type.CxCustomizationsGroup.name=Группа пользовательских настроек
type.CxCustomizationsGroup.description=Элемент для группирования пользовательских настроек
type.CxCustomizationsGroup.code.name=Код
type.CxCustomizationsGroup.code.description=Код
type.CxCustomizationsGroup.catalogversion.name=Версия каталога
type.CxCustomizationsGroup.catalogversion.description=Версия каталога
type.CxCustomizationsGroup.customizations.name=Пользовательские настройки
type.CxCustomizationsGroup.customizations.description=Пользовательские настройки

type.CxCustomization.name=Пользовательская настройка
type.CxCustomization.code.name=Код
type.CxCustomization.code.description=Уникальный код
type.CxCustomization.catalogversion.name=Версия каталога
type.CxCustomization.catalogversion.description=Версия каталога
type.CxCustomization.name.name=Имя
type.CxCustomization.name.description=Имя пользовательской настройки
type.CxCustomization.description=Элемент для группирования вариантов
type.CxCustomization.description.name=Описание
type.CxCustomization.description.description=Описание пользовательской настройки
type.CxCustomization.variations.name=Варианты
type.CxCustomization.variations.description=Варианты персонализации в этой пользовательской настройке
type.CxCustomization.rank.name=Ранг (приоритет)
type.CxCustomization.rank.description=Ранг (приоритет) пользовательской настройки
type.CxCustomization.group.name=Группа пользовательских настроек
type.CxCustomization.group.description=Элемент для группирования пользовательских настроек
type.CxCustomization.active.name=Активно
type.CxCustomization.active.description=Пользовательская настройка активна 
type.CxCustomization.status.name=Статус
type.CxCustomization.status.description=Статус пользовательской настройки
type.CxCustomization.enabledStartDate.name=Дата начала
type.CxCustomization.enabledStartDate.description=Дата включения пользовательской настройки
type.CxCustomization.enabledEndDate.name=Дата окончания
type.CxCustomization.enabledEndDate.description=Дата выключения пользовательской настройки

type.CxUserToSegment.name=Отношение пользователь-сегмент
type.CxUserToSegment.description=Отношение пользователь-сегмент
type.CxUserToSegment.segment.name=Сегмент
type.CxUserToSegment.segment.description=Сегмент персонализации
type.CxUserToSegment.user.name=Пользователь
type.CxUserToSegment.user.description=Пользователь
type.CxUserToSegment.affinity.name=Близость
type.CxUserToSegment.affinity.description=Степень близости отношения

type.CxAbstractAction.name=Абстрактное действие
type.CxAbstractAction.description=Абстрактное действие персонализации
type.CxAbstractAction.user.name=Пользователь
type.CxAbstractAction.user.description=Пользователь
type.CxAbstractAction.variation.name=Вариант
type.CxAbstractAction.variation.description=Вариант, для которого определено действие
type.CxAbstractAction.affectedObjectKey.name=Ключ соответствующего объекта
type.CxAbstractAction.affectedObjectKey.description=Укажите объект, которого коснется операция
type.CxAbstractAction.catalogversion.name=Версия каталога
type.CxAbstractAction.catalogversion.description=Версия каталога

type.User.userToSegments.name=Ссылки на сегменты персонализации
type.User.userToSegments.description=Ссылки на сегменты, которым назначен пользователь
type.User.cxResults.name=Результаты персонализации
type.User.cxResults.description=Результаты персонализации

type.CxAbstractTrigger.name=Абстрактный триггер варианта
type.CxAbstractTrigger.description=Абстрактный триггер варианта
type.CxAbstractTrigger.code.name=Код
type.CxAbstractTrigger.code.description=Уникальный код
type.CxAbstractTrigger.catalogversion.name=Версия каталога
type.CxAbstractTrigger.catalogversion.description=Версия каталога
type.CxAbstractTrigger.variation.name=Вариант инициирован
type.CxAbstractTrigger.variation.description=Вариант инициирован

type.CxSegmentTrigger.name=Триггер сегмента
type.CxSegmentTrigger.description=Триггер варианта с сегментами
type.CxSegmentTrigger.groupBy.name=Группировать сегменты по
type.CxSegmentTrigger.groupBy.description=Оператор для группирования сегментов
type.CxSegmentTrigger.segments.name=Сегменты, используемые триггером
type.CxSegmentTrigger.segments.description=Сегменты, используемые триггером

type.CxExpressionTrigger.name=Триггер выражения
type.CxExpressionTrigger.description=Триггер выражения, использующий логическое выражение и сегменты
type.CxExpressionTrigger.expression.name=Логическое выражение
type.CxExpressionTrigger.expression.description=Логическое выражение, сохраненное в формате JSON
type.CxExpressionTrigger.segments.name=Сегменты, используемые триггером
type.CxExpressionTrigger.segments.description=Сегменты, используемые триггером

type.CxDefaultTrigger.name=Триггер по умолчанию
type.CxDefaultTrigger.description=Триггер по умолчанию, всегда возвращающий связанный вариант

type.CxGroupingOperator.name=Оператор группирования сегментов
type.CxGroupingOperator.description=Оператор группирования сегментов

type.CxItemStatus.name=Статус элемента CX
type.CxItemStatus.description=Статус элемента CX

type.CxResults.name=Результаты персонализации
type.CxResults.description=Рассчитанные результаты персонализации
type.CxResults.catalogVersion.name=Версия каталога
type.CxResults.catalogVersion.description=Версия каталога для результатов персонализации
type.CxResults.results.name=Сериализованные результаты
type.CxResults.results.description=Сериализованные результаты
type.CxResults.calculationTime.name=Время расчета
type.CxResults.calculationTime.description=Время расчета результатов
type.CxResults.user.name=Пользователь
type.CxResults.user.description=Пользователь
type.CxResults.key.name=Ключ результатов
type.CxResults.key.description=Ключ результатов
type.CxResults.sessionKey.name=Ключ сеанса
type.CxResults.sessionKey.description=Ключ сеанса
type.CxResults.anonymous.name=Результаты рассчитаны для анонимного пользователя
type.CxResults.anonymous.description=Атрибут, указывающий, рассчитаны ли результаты для анонимного пользователя


type.CxPersonalizationProcess.name=Процесс персонализации
type.CxPersonalizationProcess.description=Процесс персонализации
type.CxPersonalizationProcess.key.name=Ключ процесса
type.CxPersonalizationProcess.key.description=Ключ процесса
type.CxPersonalizationProcess.baseSite.name=Текущий базовый сайт
type.CxPersonalizationProcess.baseSite.description=Текущий базовый сайт
type.CxPersonalizationProcess.catalogVersions.name=Версии каталога
type.CxPersonalizationProcess.catalogVersions.description=Версии каталога для процесса

type.CatalogVersion.cxPersonalizationProcesses.name=Процессы расчета персонализации для версии каталога
type.CatalogVersion.cxPersonalizationProcesses.description=Процессы расчета персонализации для версии каталога

type.CxPersonalizationProcessCleanupCronJob.name=Задача Cron для очистки процессов персонализации
type.CxPersonalizationProcessCleanupCronJob.description=Задачи Cron для очистки процессов персонализации
type.CxPersonalizationProcessCleanupCronJob.processStates.name=Статусы процесса персонализации для удаления
type.CxPersonalizationProcessCleanupCronJob.processStates.description=Статусы процесса персонализации, которые будут включены в процесс очистки
type.CxPersonalizationProcessCleanupCronJob.maxProcessAge.name=Максимальный возраст процесса персонализации
type.CxPersonalizationProcessCleanupCronJob.maxProcessAge.description=Максимальный возраст процесса персонализации, по достижении которого такой процесс удаляется

type.CxConfig.name=Конфигурация персонализации
type.CxConfig.description=Конфигурация персонализации
type.CxConfig.code.name=Код
type.CxConfig.code.description=Уникальный код
type.CxConfig.baseSites.name=Набор базовых сайтов
type.CxConfig.baseSites.description=Набор базовых сайтов
type.CxConfig.minAffinity.name=Минимальная аффинитивность
type.CxConfig.minAffinity.description=Минимальная аффинитивность для отношения пользователя и сегмента
type.CxConfig.urlVoterConfigs.name=Конфигурация выбора URL
type.CxConfig.urlVoterConfigs.description=Список конфигураций выбора URL
type.CxConfig.actionResultMaxRepeat.name=Максимум повторов загрузки результатов операции
type.CxConfig.actionResultMaxRepeat.description=Число попыток в случае ошибки загрузки результатов операции из-за ошибки оптимистической блокировки
type.CxConfig.userChangedActions.name=Операции (после изменения события пользователем)
type.CxConfig.userChangedActions.description=Набор операций, вызываемых после изменения события пользователем
type.CxConfig.calculationProcess.name=Имя процесса расчета
type.CxConfig.calculationProcess.description=Имя процесса расчета по умолчанию, запускаемого для асинхронного расчета
type.CxConfig.ignoreRecalcForAnonymous.name=Игнорировать перерасчет для анонимного пользователя
type.CxConfig.ignoreRecalcForAnonymous.description=Игнорировать перерасчет интерфейса для анонимного пользователя

type.CxConfig.anonymousUserDefaultActions.name=Разовые операции
type.CxConfig.anonymousUserDefaultActions.description=Операции, выполняемые при обнаружении анонимного пользователя
type.CxConfig.anonymousUserActions.name=Последующие операции
type.CxConfig.anonymousUserActions.description=Операции, выполняемые для анонимного пользователя при последующем запросе 
type.CxConfig.anonymousUserMinRequestNumber.name=Последующие операции требуют запросов
type.CxConfig.anonymousUserMinRequestNumber.description=Число запросов между последующими операциями [отрицательное число означает отсутствие ограничения]
type.CxConfig.anonymousUserMinTime.name=Интервал последующих операций
type.CxConfig.anonymousUserMinTime.description=Минимальное время [ms] между последующими операциями [отрицательное число означает отсутствие ограничения]
type.CxConfig.anonymousUserIgnoreOtherActions.name=Игнорировать другие операции для анонимного пользователя

type.CxConfig.anonymousUserIgnoreOtherActions.description=Операции из других конфигураций будут игнорироваться для анонимного пользователя

type.CxUrlVoterConfig.name=Конфигурация выбора URL
type.CxUrlVoterConfig.description=Конфигурация выбора URL
type.CxUrlVoterConfig.cxConfig.name=Конфигурация персонализации
type.CxUrlVoterConfig.cxConfig.description=Конфигурация персонализации
type.CxUrlVoterConfig.code.name=Код
type.CxUrlVoterConfig.code.description=Код
type.CxUrlVoterConfig.urlRegexp.name=Регулярное выражение URL
type.CxUrlVoterConfig.urlRegexp.description=Если URL соответствует указанному регулярному выражению, выбор URL возвращает операции  
type.CxUrlVoterConfig.actions.name=Операции
type.CxUrlVoterConfig.actions.description=Набор операций, возвращаемых при выборе в случае соответствия регулярному выражению

type.BaseSite.cxConfig.name=Конфигурация персонализации
type.BaseSite.cxConfig.description=Конфигурация персонализации

type.CxResultsCleaningCronJob.name=Задача Cron для очистки результатов персонализации
type.CxResultsCleaningCronJob.description=Задачи Cron для очистки результатов персонализации
type.CxResultsCleaningCronJob.anonymous.name=Тип результатов для удаления
type.CxResultsCleaningCronJob.anonymous.description=Определите тип результатов для удаления (анонимные или постоянные покупатели) 
type.CxResultsCleaningCronJob.maxResultsAge.name=Максимальный срок хранения результатов
type.CxResultsCleaningCronJob.maxResultsAge.description=Максимальный срок хранения результатов, по истечении которого они удаляются
