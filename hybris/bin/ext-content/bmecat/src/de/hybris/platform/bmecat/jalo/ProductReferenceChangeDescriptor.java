/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 * 
 *  
 */
package de.hybris.platform.bmecat.jalo;

import org.apache.log4j.Logger;


/**
 * ProductReferenceChangeDescriptor
 */
public class ProductReferenceChangeDescriptor extends GeneratedProductReferenceChangeDescriptor
{
	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(ProductReferenceChangeDescriptor.class.getName());
}
