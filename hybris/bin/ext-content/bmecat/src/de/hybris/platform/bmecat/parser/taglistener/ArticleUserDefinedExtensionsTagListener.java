/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 * 
 *  
 */
package de.hybris.platform.bmecat.parser.taglistener;


/**
 * Parses the &lt;ArticleUserDefinedExtensions&gt; tag
 */
public class ArticleUserDefinedExtensionsTagListener extends UserDefinedExtensionsTagListener
{
	// DOCTODO Document reason, why this block is empty
}
