/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
module.exports = function() {

    return {
        loadE2eDependencies: function(browser, e2eDependencyContainer) {
            browser.setStorefrontDelayConfigInSessionStorage = function(delayConfig) {
                browser.get('smarteditcontainerJSTests/e2e/_shared/dummystorefront/fakeAngularEmptyPage.html');
                browser.executeScript('window.sessionStorage.setItem("STOREFRONT_DELAY_STRATEGY", arguments[0])', delayConfig);
            };
        }
    };

}();
