/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
var dropdownObject = require("./componentWithDropdown/dropdownObject.js");

module.exports = (function() {

    var componentObject = {};

    componentObject.actions = {

        openAndBeReady: function(editorType) {
            if (editorType === 'withDropdown') {
                dropdownObject.actions.openAndBeReady();
            }
        }

    };

    return componentObject;

})();
