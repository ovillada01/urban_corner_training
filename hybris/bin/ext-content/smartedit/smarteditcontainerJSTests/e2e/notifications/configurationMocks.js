/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
angular.module('OuterMocksModule', [
        'ngMockE2E',
        'resourceLocationsModule',
        'languageServiceModule'
    ])
    .constant('SMARTEDIT_ROOT', 'web/webroot')
    .value('CONFIGURATION_MOCK', [{
        'value': '{"smartEditContainerLocation":"/smarteditcontainerJSTests/e2e/utils/commonMockedModules/goToCustomView.js"}',
        'key': 'applications.goToCustomView'
    }, {
        'value': '{"smartEditContainerLocation":"/smarteditcontainerJSTests/e2e/notifications/customViewController.js"}',
        'key': 'applications.customViewModule'
    }, {
        'value': '{"smartEditContainerLocation":"/smarteditcontainerJSTests/e2e/notifications/notificationMocks.js"}',
        'key': 'applications.notificationMocksModule'
    }]);

try {
    angular.module('smarteditloader').requires.push('OuterMocksModule');
    angular.module('smarteditcontainer').requires.push('OuterMocksModule');
} catch (exception) {
    console.error('yNotificationMocks - Failed to add OuterMocksModule as a dependency', exception);
}
