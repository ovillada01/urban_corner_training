/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
angular.module('FakeModule', [
        'decoratorServiceModule',
        'componentHandlerServiceModule',
        'featureServiceModule'
    ]) .component('pageChangeTest', {
        template: "<span>{{$ctrl.getPageChangeMessage()}}</span>",
                controller: function($rootScope) {
            this.getPageChangeMessage = function() {
                return $rootScope.pageChangeMessage;
            };
        }
    })
    .directive('deco1', function() {
        return {
            template: "<div style='background: rgba(0, 0, 255, .4)'><div ng-transclude></div></div>",
            restrict: 'C',
            controller: function() {},
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '='
            }
        };
    })
    .directive('deco2', function() {
        return {
            template: "<div style='background: rgba(255, 0, 0, .4)'><div ng-transclude></div></div>",
            restrict: 'C',
            controller: function() {},
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '='
            }
        };
    })
    .directive('deco3', function() {
        return {
            template: "<div style='background: rgba(0, 255, 0, .4)'><div ng-transclude></div></div>",
            restrict: 'C',
            controller: function() {},
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '='
            }
        };
    })
    .directive('deco4', function() {
        return {
            template: "<div style='background: rgba(0, 255, 255, .8)'><div ng-transclude></div>page changed</div>",
            restrict: 'C',
            controller: function() {},
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                active: '='
            }
        };
    })
    .run(function($q, $rootScope, decoratorService, crossFrameEventService, featureService) {
        crossFrameEventService.subscribe("PAGE_CHANGE", function(eventId, experience) {
            $rootScope.pageChangeMessage = "paged changed to " + experience.pageId;
            $rootScope.currentPageId = experience.pageId;
            return $q.when();
        });

        decoratorService.addMappings({
            'componentType1': ['deco1', 'deco4'],
            'componentType2': ['deco2'],
            'ContentSlot': ['deco3']
        });

        decoratorService.enable('deco1');
        decoratorService.enable('deco2');
        decoratorService.enable('deco3');

        featureService.addDecorator({
            key: 'deco4',
            nameI18nKey: 'deco4',
            displayCondition: function() {
                return $q.when($rootScope.currentPageId === 'demo_storefront_page_id');
            }
        });
    })
    .controller('sakExecutorDebugController', function(sakExecutor, yjQuery, COMPONENT_CLASS, resizeListener, positionRegistry) {
        this.$onInit = function() {


            this.getTotalSmartEditComponents = function() {
                return yjQuery("." + COMPONENT_CLASS).length;
            };

            this.getTotalSakExecutorElements = function() {
                return sakExecutor.getScopes().length;
            };

            this.getTotalResizeEventListeners = function() {
                return resizeListener._listenerCount();
            };

            this.getTotalPositionEventListeners = function() {
                return positionRegistry._listenerCount();
            };

        };
    })
    .component('sakExecutorDebug', {
        template: '<h3>sak executor debug:</h3><pre>Total of SmartEdit components in page: <div id="total-store-front-components">{{ctrl.getTotalSmartEditComponents()}}</div>' +
            '</pre><pre>Total of sakExecutor stored elements: <div id="total-sak-executor-elements">{{ctrl.getTotalSakExecutorElements()}}</div></pre>' +
            '<pre>Total of resize event listeners in DOM : <div id="total-resize-listeners">{{ctrl.getTotalResizeEventListeners()}}</div></pre>' +
            '<pre>Total of position event listeners in DOM : <div id="total-reposition-listeners">{{ctrl.getTotalPositionEventListeners()}}</div></pre>',
        controller: 'sakExecutorDebugController',
        controllerAs: 'ctrl'
    });
