/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/* jshint unused:false, undef:false */
describe('smartEditContractChangeListener', function() {
    var $rootScope;
    var $interval;
    var componentHandlerService;
    var yjQuery;
    var smartEditContractChangeListener;
    var mutationObserverMock;
    var mutationObserverCallback;
    var onComponentRepositionedCallback;
    var onComponentResizedCallback;
    var resizeListener;
    var positionRegistry;

    var parent;
    var directParent;
    var component0;
    var component1;
    var component2;
    var component2_1;
    var component3;

    var COMPONENT_CLASS;
    var UUID_ATTRIBUTE;
    var ID_ATTRIBUTE;
    var INITIAL_PAGE_UUID = 'INITIAL_PAGE_UUID';
    var ANY_PAGE_UUID = 'ANY_PAGE_UUID';
    var SMARTEDIT_COMPONENTS;
    var REPROCESS_TIMEOUT = 100;

    beforeEach(customMatchers);

    beforeEach(module('smartEditContractChangeListenerModule', function() {
        window.elementResizeDetectorMaker = function() {
            return {
                uninstall: angular.noop
            };
        };
    }));

    beforeEach(module('componentHandlerServiceModule', function($provide) {
        componentHandlerService = jasmine.createSpyObj('componentHandlerService', ['getFromSelector', 'getPageUUID', 'getClosestSmartEditComponent', 'isSmartEditComponent', 'getFirstSmartEditComponentChildren']);
        $provide.value('componentHandlerService', componentHandlerService);

        resizeListener = jasmine.createSpyObj('resizeListener', ['register', 'unregister', 'dispose']);
        $provide.value('resizeListener', resizeListener);

        positionRegistry = jasmine.createSpyObj('positionRegistry', ['register', 'unregister', 'getRepositionedComponents', 'dispose']);
        $provide.value('positionRegistry', positionRegistry);

        yjQuery = jasmine.createSpyObj('yjQuery', ['contains']);
        yjQuery.contains.and.callFake(function(container, element) {
            if (container !== $document[0]) {
                throw "yjQuery.contains should have been the plain document object";
            }
            return parent === element;
        });
        yjQuery.fn = {
            extend: function() {}
        };

        $provide.value("yjQuery", yjQuery);


    }));

    beforeEach(inject(function(_$document_, _$rootScope_, _$interval_, _smartEditContractChangeListener_, _COMPONENT_CLASS_, _UUID_ATTRIBUTE_, _ID_ATTRIBUTE_) {
        $document = _$document_;
        $rootScope = _$rootScope_;
        $interval = _$interval_;

        smartEditContractChangeListener = _smartEditContractChangeListener_;

        mutationObserverMock = jasmine.createSpyObj('MutationObserver', ['observe', 'disconnect']);
        spyOn(smartEditContractChangeListener, '_newMutationObserver').and.callFake(function(callback) {
            mutationObserverCallback = callback;
            this.observe = angular.noop;
            this.disconnect = angular.noop;
            return mutationObserverMock;
        });

        onComponentRepositionedCallback = jasmine.createSpy('onComponentRepositioned');
        smartEditContractChangeListener.onComponentRepositioned(onComponentRepositionedCallback);

        onComponentResizedCallback = angular.noop;
        smartEditContractChangeListener.onComponentResized(onComponentResizedCallback);

        COMPONENT_CLASS = _COMPONENT_CLASS_;
        UUID_ATTRIBUTE = _UUID_ATTRIBUTE_;
        ID_ATTRIBUTE = _ID_ATTRIBUTE_;
    }));

    beforeEach(function() {

        parent = jasmine.createSpyObj('parent', ['attr']);
        parent.nodeType = 1;
        parent.className = COMPONENT_CLASS;
        parent.attr.and.returnValue('parent');
        parent.name = 'parent';

        directParent = jasmine.createSpyObj('directParent', ['attr']);
        directParent.nodeType = 1;
        directParent.attr.and.returnValue('directParent');
        directParent.name = 'directParent';

        component0 = jasmine.createSpyObj('component0', ['attr']);
        component0.nodeType = 1;
        component0.className = "nonSmartEditComponent";
        component0.attr.and.returnValue('component0');
        component0.name = 'component0';
        component0.contains = function(node) {
            return false;
        };

        component1 = jasmine.createSpyObj('component1', ['attr']);
        component1.nodeType = 1;
        component1.className = COMPONENT_CLASS;
        component1.attr.and.returnValue('component1');
        component1.name = 'component1';
        component1.contains = function(node) {
            return false;
        };

        component2_1 = jasmine.createSpyObj('component2_1', ['attr']);
        component2_1.nodeType = 1;
        component2_1.className = COMPONENT_CLASS;
        component2_1.attr.and.returnValue('component2_1');
        component2_1.name = 'component2_1';
        component2_1.contains = function(node) {
            return false;
        };

        component2 = jasmine.createSpyObj('component2', ['attr']);
        component2.nodeType = 1;
        component2.className = COMPONENT_CLASS;
        component2.attr.and.returnValue('component2');
        component2.name = 'component2';
        component2.contains = function(node) {
            return node === component2_1;
        };
        component3 = jasmine.createSpyObj('component3', ['attr']);
        component3.nodeType = 1;
        component3.className = COMPONENT_CLASS;
        component3.attr.and.returnValue('component3');
        component3.name = 'component3';
        component3.contains = function(node) {
            return false;
        };


        SMARTEDIT_COMPONENTS = [component1];

        var smarteditComponents = jasmine.createSpyObj('smarteditComponents', ['each']);
        smarteditComponents.each.and.callFake(function(callback) {
            SMARTEDIT_COMPONENTS.forEach(function(smarteditComponent, index) {
                callback(index, smarteditComponent);
            });
        });

        var pageUUIDCounter = 0;
        componentHandlerService.getPageUUID.and.callFake(function(arg) {
            pageUUIDCounter++;
            if (pageUUIDCounter === 1) {
                return INITIAL_PAGE_UUID;
            } else if (pageUUIDCounter === 2) {
                return ANY_PAGE_UUID;
            }
            return null;

        });

        componentHandlerService.getFromSelector.and.callFake(function(arg) {
            if (arg === "." + COMPONENT_CLASS) {
                return smarteditComponents;
            }
            return null;

        });

        componentHandlerService.isSmartEditComponent.and.callFake(function(node) {
            return node.className && node.className.split(/[\s]+/).indexOf(COMPONENT_CLASS) > -1;

        });

        componentHandlerService.getClosestSmartEditComponent.and.returnValue([parent]);

        componentHandlerService.getFirstSmartEditComponentChildren.and.callFake(function(node) {
            if (node === component2) {
                return [component2_1];
            } else if (node === component0) {
                return [component2]; //ok to just return array, slice is applied on it
            } else {
                return [];
            }
        });

        smartEditContractChangeListener.initListener();
    });

    beforeEach(function() {
        jasmine.clock().install();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    it('should init the Mutation Observer and observe on body element with the expected configuration', function() {
        var expectedConfig = {
            attributes: true,
            attributeOldValue: true,
            childList: true,
            characterData: false,
            subtree: true
        };
        expect(mutationObserverMock.observe).toHaveBeenCalledWith(document.getElementsByTagName('body')[0], expectedConfig);
    });

    it('should register resize and position listeners on existing smartedit components', function() {

        expect(resizeListener.register.calls.count()).toEqual(1);
        expect(positionRegistry.register.calls.count()).toEqual(1);

        SMARTEDIT_COMPONENTS.forEach(function(component, index) {
            expect(resizeListener.register.calls.argsFor(index)).toEqual([component, jasmine.any(Function)]);
            expect(positionRegistry.register.calls.argsFor(index)).toEqual([component]);
        });
    });

    it('should be able to observe a page change and execute a registered page change callback', function() {
        // GIVEN
        var pageChangedCallback = jasmine.createSpy('callback');
        smartEditContractChangeListener.onPageChanged(pageChangedCallback);

        // WHEN
        var mutations = [{
            attributeName: 'class',
            type: 'attributes',
            target: {
                tagName: 'BODY'
            }
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN
        expect(pageChangedCallback.calls.argsFor(0)[0]).toEqual(ANY_PAGE_UUID);

        mutationObserverCallback(mutations);

        expect(pageChangedCallback.calls.count()).toBe(1);
    });

    it('when a parent and a child are in the same operation (can occur), the child is ignored', function() {

        resizeListener.register.calls.reset();
        positionRegistry.register.calls.reset();

        // GIVEN
        var onComponentAddedCallback = jasmine.createSpy('onComponentAdded');
        smartEditContractChangeListener.onComponentAdded(onComponentAddedCallback);

        // WHEN
        var mutations = [{
            type: 'childList',
            addedNodes: [component3, component2, component2_1]
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN
        expect(onComponentAddedCallback.calls.count()).toBe(2);
        expect(onComponentAddedCallback.calls.argsFor(0)[0]).toEqual(component3);
        expect(onComponentAddedCallback.calls.argsFor(1)[0]).toEqual(component2);
    });


    it('should be able to observe sub tree of smartEditComponent component added', function() {

        resizeListener.register.calls.reset();
        positionRegistry.register.calls.reset();

        // GIVEN
        var onComponentAddedCallback = jasmine.createSpy('onComponentAdded');
        smartEditContractChangeListener.onComponentAdded(onComponentAddedCallback);

        // WHEN
        var mutations = [{
            type: 'childList',
            addedNodes: [component2, component3]
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN

        expect(resizeListener.unregister.calls.count()).toEqual(2);
        expect(resizeListener.unregister.calls.argsFor(0)).toEqual([parent]);
        expect(resizeListener.unregister.calls.argsFor(1)).toEqual([parent]);

        expect(resizeListener.register.calls.count()).toEqual(5);
        expect(resizeListener.register.calls.argsFor(0)).toEqual([parent, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(1)).toEqual([component2, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(2)).toEqual([component2_1, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(3)).toEqual([parent, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(4)).toEqual([component3, jasmine.any(Function)]);

        expect(positionRegistry.register.calls.count()).toEqual(3);
        expect(positionRegistry.register.calls.argsFor(0)).toEqual([component2]);
        expect(positionRegistry.register.calls.argsFor(1)).toEqual([component2_1]);
        expect(positionRegistry.register.calls.argsFor(2)).toEqual([component3]);

        expect(onComponentAddedCallback.calls.count()).toBe(2);
        expect(onComponentAddedCallback.calls.argsFor(0)[0]).toEqual(component2);
        expect(onComponentAddedCallback.calls.argsFor(1)[0]).toEqual(component3);
    });

    it('should be able to observe sub tree of non smartEditComponent component added', function() {

        resizeListener.register.calls.reset();
        positionRegistry.register.calls.reset();

        // GIVEN
        var onComponentAddedCallback = jasmine.createSpy('onComponentAdded');
        smartEditContractChangeListener.onComponentAdded(onComponentAddedCallback);

        // WHEN
        var mutations = [{
            type: 'childList',
            addedNodes: [component0, component3]
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN

        expect(resizeListener.unregister.calls.count()).toEqual(2);
        expect(resizeListener.unregister.calls.argsFor(0)).toEqual([parent]);
        expect(resizeListener.unregister.calls.argsFor(1)).toEqual([parent]);

        expect(resizeListener.register.calls.count()).toEqual(5);
        expect(resizeListener.register.calls.argsFor(0)).toEqual([parent, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(1)).toEqual([component2, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(2)).toEqual([component2_1, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(3)).toEqual([parent, jasmine.any(Function)]);
        expect(resizeListener.register.calls.argsFor(4)).toEqual([component3, jasmine.any(Function)]);

        expect(positionRegistry.register.calls.count()).toEqual(3);
        expect(positionRegistry.register.calls.argsFor(0)).toEqual([component2]);
        expect(positionRegistry.register.calls.argsFor(1)).toEqual([component2_1]);
        expect(positionRegistry.register.calls.argsFor(2)).toEqual([component3]);

        expect(onComponentAddedCallback.calls.count()).toBe(2);
        expect(onComponentAddedCallback.calls.argsFor(0)[0]).toEqual(component2);
        expect(onComponentAddedCallback.calls.argsFor(1)[0]).toEqual(component3);
    });

    it('should be able to observe sub tree of smartEditComponent (and parent) removed', function() {
        // GIVEN
        var onComponentRemovedCallback = jasmine.createSpy('onComponentRemoved');
        smartEditContractChangeListener.onComponentRemoved(onComponentRemovedCallback);

        // WHEN
        var mutations = [{
            type: 'childList',
            target: directParent,
            removedNodes: [component2, component3]
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN
        expect(resizeListener.unregister.calls.count()).toEqual(5);
        expect(resizeListener.unregister.calls.argsFor(0)).toEqual([component2]);
        expect(resizeListener.unregister.calls.argsFor(1)).toEqual([component2_1]);
        expect(resizeListener.unregister.calls.argsFor(2)).toEqual([parent]);
        expect(resizeListener.unregister.calls.argsFor(3)).toEqual([component3]);
        expect(resizeListener.unregister.calls.argsFor(4)).toEqual([parent]);

        expect(positionRegistry.unregister.calls.count()).toEqual(3);
        expect(positionRegistry.unregister.calls.argsFor(0)).toEqual([component2]);
        expect(positionRegistry.unregister.calls.argsFor(1)).toEqual([component2_1]);
        expect(positionRegistry.unregister.calls.argsFor(2)).toEqual([component3]);

        expect(onComponentRemovedCallback.calls.count()).toBe(2);
        expect(onComponentRemovedCallback.calls.argsFor(0)[0]).toEqual(component2);
        expect(onComponentRemovedCallback.calls.argsFor(0)[1]).toEqual(parent);
        expect(onComponentRemovedCallback.calls.argsFor(1)[0]).toEqual(component3);
        expect(onComponentRemovedCallback.calls.argsFor(1)[1]).toEqual(parent);

        expect(componentHandlerService.getClosestSmartEditComponent).toHaveBeenCalledWith(directParent);

    });

    it('should be able to observe sub tree of a non smartEditComponent (and parent) removed', function() {
        // GIVEN
        var onComponentRemovedCallback = jasmine.createSpy('onComponentRemoved');
        smartEditContractChangeListener.onComponentRemoved(onComponentRemovedCallback);

        // WHEN
        var mutations = [{
            type: 'childList',
            target: directParent,
            removedNodes: [component0, component3]
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN
        expect(resizeListener.unregister.calls.count()).toEqual(5);
        expect(resizeListener.unregister.calls.argsFor(0)).toEqual([component2]);
        expect(resizeListener.unregister.calls.argsFor(1)).toEqual([component2_1]);
        expect(resizeListener.unregister.calls.argsFor(2)).toEqual([parent]);
        expect(resizeListener.unregister.calls.argsFor(3)).toEqual([component3]);
        expect(resizeListener.unregister.calls.argsFor(4)).toEqual([parent]);

        expect(positionRegistry.unregister.calls.count()).toEqual(3);
        expect(positionRegistry.unregister.calls.argsFor(0)).toEqual([component2]);
        expect(positionRegistry.unregister.calls.argsFor(1)).toEqual([component2_1]);
        expect(positionRegistry.unregister.calls.argsFor(2)).toEqual([component3]);

        expect(onComponentRemovedCallback.calls.count()).toBe(2);
        expect(onComponentRemovedCallback.calls.argsFor(0)[0]).toEqual(component2);
        expect(onComponentRemovedCallback.calls.argsFor(0)[1]).toEqual(parent);
        expect(onComponentRemovedCallback.calls.argsFor(1)[0]).toEqual(component3);
        expect(onComponentRemovedCallback.calls.argsFor(1)[1]).toEqual(parent);

        expect(componentHandlerService.getClosestSmartEditComponent).toHaveBeenCalledWith(directParent);

    });

    it('should be able to stop all the listeners', function() {
        smartEditContractChangeListener.stopListener();

        expect(mutationObserverMock.disconnect).toHaveBeenCalled();
        expect(resizeListener.dispose).toHaveBeenCalled();
        expect(positionRegistry.dispose).toHaveBeenCalled();
    });

    it('should call the componentRepositionedCallback when a component is repositioned after updating the registry', function() {
        var component1 = SMARTEDIT_COMPONENTS[0];
        positionRegistry.getRepositionedComponents.and.returnValue([component1]);

        $interval.flush(REPROCESS_TIMEOUT);

        expect(positionRegistry.register).toHaveBeenCalledWith(component1);
        expect(onComponentRepositionedCallback.calls.count()).toBe(1);
        expect(onComponentRepositionedCallback).toHaveBeenCalledWith(component1);
    });

    it('should cancel the repositionListener interval when calling stopListener', function() {
        positionRegistry.getRepositionedComponents.and.returnValue([]);

        var cancelSpy = spyOn($interval, 'cancel');

        smartEditContractChangeListener.stopListener();

        expect(cancelSpy.calls.count()).toBe(1);
    });

    it('should be able to observe a component change', function() {
        // GIVEN
        var onComponentChangedCallback = jasmine.createSpy('onComponentChangedCallback');
        smartEditContractChangeListener.onComponentChanged(onComponentChangedCallback);

        // WHEN
        var mutations = [{
            type: 'attributes',
            attributeName: UUID_ATTRIBUTE,
            target: component1,
            oldValue: 'random_uuid'
        }, {
            type: 'attributes',
            attributeName: ID_ATTRIBUTE,
            target: component1,
            oldValue: 'random_id'
        }];
        mutationObserverCallback(mutations);
        $rootScope.$digest();

        // THEN
        var expectedOldAttributes = {};
        expectedOldAttributes[UUID_ATTRIBUTE] = 'random_uuid';
        expectedOldAttributes[ID_ATTRIBUTE] = 'random_id';
        expect(onComponentChangedCallback.calls.argsFor(0)[0]).toEqual(component1, expectedOldAttributes);
    });
});
