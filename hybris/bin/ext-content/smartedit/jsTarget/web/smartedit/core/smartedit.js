/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    angular.module('smartedit', [
            'yjqueryModule',
            'configModule',
            'templateCacheDecoratorModule',
            'sakExecutorDecorator',
            'restServiceFactoryModule',
            'ui.bootstrap',
            'ngResource',
            'decoratorServiceModule',
            'smartEditContractChangeListenerModule',
            'alertsBoxModule',
            'ui.select',
            'httpAuthInterceptorModule',
            'httpErrorInterceptorServiceModule',
            'unauthorizedErrorInterceptorModule',
            'retryInterceptorModule',
            'resourceNotFoundErrorInterceptorModule',
            'experienceInterceptorModule',
            'gatewayFactoryModule',
            'renderServiceModule',
            'iframeClickDetectionServiceModule',
            'sanitizeHtmlInputModule',
            'perspectiveServiceModule',
            'featureServiceModule',
            'resizeComponentServiceModule',
            'languageServiceModule',
            'notificationServiceModule',
            'slotContextualMenuDecoratorModule',
            'contextualMenuDecoratorModule',
            'crossFrameEventServiceModule',
            'pageSensitiveDirectiveModule',
            'seNamespaceModule',
            'experienceServiceModule'
        ])
        .config(['$logProvider', function($logProvider) {
            $logProvider.debugEnabled(false);
        }])
        .directive('html', function() {
            return {
                restrict: "E",
                replace: false,
                transclude: false,
                priority: 1000,
                link: function($scope, element) {
                    element.addClass('smartedit-html-container');
                }
            };
        })
        .controller('SmartEditController', function() {})
        .run(['domain', 'systemEventService', 'EVENTS', 'ID_ATTRIBUTE', 'OVERLAY_RERENDERED_EVENT', 'smartEditContractChangeListener', 'crossFrameEventService', 'perspectiveService', 'languageService', 'restServiceFactory', 'gatewayFactory', 'renderService', 'decoratorService', 'featureService', 'resizeComponentService', 'seNamespace', 'experienceService', 'httpErrorInterceptorService', 'retryInterceptor', 'unauthorizedErrorInterceptor', 'resourceNotFoundErrorInterceptor', function(domain, systemEventService, EVENTS, ID_ATTRIBUTE, OVERLAY_RERENDERED_EVENT,
            smartEditContractChangeListener, crossFrameEventService, perspectiveService,
            languageService, restServiceFactory, gatewayFactory, renderService, decoratorService,
            featureService, resizeComponentService, seNamespace, experienceService, httpErrorInterceptorService,
            retryInterceptor, unauthorizedErrorInterceptor, resourceNotFoundErrorInterceptor) {

            gatewayFactory.initListener();

            httpErrorInterceptorService.addInterceptor(retryInterceptor);
            httpErrorInterceptorService.addInterceptor(unauthorizedErrorInterceptor);
            httpErrorInterceptorService.addInterceptor(resourceNotFoundErrorInterceptor);

            smartEditContractChangeListener.onComponentAdded(function(component) {
                seNamespace.reprocessPage();
                resizeComponentService._resizeComponents(true);
                renderService._resizeSlots();
                renderService._createComponent(component);
                crossFrameEventService.publish(OVERLAY_RERENDERED_EVENT);
            });

            //parent may be null
            smartEditContractChangeListener.onComponentRemoved(function(component, parent) {
                seNamespace.reprocessPage();
                renderService._resizeSlots();
                renderService._destroyComponent(component, parent);
                crossFrameEventService.publish(OVERLAY_RERENDERED_EVENT);
            });

            smartEditContractChangeListener.onComponentResized(function(component) {
                seNamespace.reprocessPage();
                renderService._resizeSlots();
                renderService._updateComponentSizeAndPosition(component);
            });

            smartEditContractChangeListener.onComponentRepositioned(function(component) {
                renderService._updateComponentSizeAndPosition(component);
            });

            smartEditContractChangeListener.onComponentChanged(function(component, oldAttributes) {
                seNamespace.reprocessPage();
                renderService._resizeSlots();
                renderService._destroyComponent(component, component.parent, oldAttributes);
                renderService._createComponent(component);
            });

            smartEditContractChangeListener.onPageChanged(function() {
                experienceService.updateExperiencePageContext();
            });

            systemEventService.registerEventHandler(EVENTS.AUTHORIZATION_SUCCESS, function(evtId, evtData) {
                if (evtData.userHasChanged) {
                    perspectiveService.refreshPerspective();
                }
            });

            crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
                perspectiveService.refreshPerspective();
                languageService.registerSwitchLanguage();
            });

            smartEditContractChangeListener.initListener();

            restServiceFactory.setDomain(domain);

            // Feature registration
            featureService.register({
                key: 'se.emptySlotFix',
                nameI18nKey: 'se.emptyslotfix',
                enablingCallback: function() {
                    resizeComponentService._resizeComponents(true);
                },
                disablingCallback: function() {
                    resizeComponentService._resizeComponents(false);
                }
            });

            featureService.addDecorator({
                key: 'se.contextualMenu',
                nameI18nKey: 'contextualMenu'
            });

            featureService.addDecorator({
                key: 'se.slotContextualMenu',
                nameI18nKey: 'se.slot.contextual.menu'
            });

        }]);
})();
