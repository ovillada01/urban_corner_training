/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("smartEditContractChangeListenerModule", [
        'yjqueryModule',
        'yLoDashModule',
        'timerModule',
        'componentHandlerServiceModule',
        'resizeListenerModule',
        'positionRegistryModule'
    ])
    /*
     * interval at which manual listening/checks are executed
     * So far it is only by repositionListener
     * (resizeListener delegates to a self-contained third-party library and DOM mutations observation is done in native MutationObserver
     */
    .constant("REPROCESS_TIMEOUT", 100)


/*
 *  Fetches the HTML root element to observer for changes
 *  Exposed as a service for extensibility / optimization
 */
.service('smartEditContractChangeListenerObservableRoot', function() {
    this.getObservableRoot = function() {
        return document.getElementsByTagName('body')[0];
    };
})

/*
 * service that allows specifying callbacks for all events affecting elements of the smartEdit storefront contract:
 * - page identifier change
 * - smartEdit components added or removed
 * - contract bound attributes of smartEdit components changes
 * - smartEdit components repositioned
 * - smartEdit components resized
 */
.service("smartEditContractChangeListener", ['$log', '$interval', '$rootScope', '$document', 'yjQuery', 'lodash', 'componentHandlerService', 'resizeListener', 'positionRegistry', 'smartEditContractChangeListenerObservableRoot', 'REPROCESS_TIMEOUT', 'COMPONENT_CLASS', 'TYPE_ATTRIBUTE', 'ID_ATTRIBUTE', 'UUID_ATTRIBUTE', function($log, $interval, $rootScope, $document,
    yjQuery,
    lodash,
    componentHandlerService,
    resizeListener,
    positionRegistry,
    smartEditContractChangeListenerObservableRoot,
    REPROCESS_TIMEOUT,
    COMPONENT_CLASS,
    TYPE_ATTRIBUTE,
    ID_ATTRIBUTE,
    UUID_ATTRIBUTE) {

    /*
     * list of smartEdit component attributes the change of which we observe to trigger an onComponentChanged event
     */
    var smartEditAttributeNames = [TYPE_ATTRIBUTE, ID_ATTRIBUTE, UUID_ATTRIBUTE];

    /*
     * Mutation object (return in a list of mutations in mutation event) can be of different types.
     * We are here only interested in type attributes (used for onPageChanged and onComponentChanged events) and childList (used for onComponentRemoved and onComponentAdded events)
     */
    var MUTATION_TYPES = {
        CHILD_LIST: {
            NAME: "childList",
            REMOVE_OPERATION: "removedNodes",
            ADD_OPERATION: "addedNodes"
        },
        ATTRIBUTES: {
            NAME: "attributes"
        }
    };

    /*
     * Of All the DOM node types we only care for 1 and 2 (Element and Attributes)
     */
    var NODE_TYPES = {
        ELEMENT: 1,
        ATTRIBUTE: 2,
        TEXT: 3
    };

    /*
     * This is the configuration passed to the MutationObserver instance
     */
    var OBSERVER_OPTIONS = {
        /*
         * enables observation of attribute mutations
         */
        attributes: true,
        /*
         * instruct the observer to keep in store the former values of the mutated attributes
         */
        attributeOldValue: true,
        /*
         * enables observation of addition and removal of nodes
         */
        childList: true,
        characterData: false,
        /*
         * enables recursive lookup without which only addition and removal of DIRECT children of the observed DOM root would be collected
         */
        subtree: true
    };

    /*
     * unique instance of a MutationObserver on the body (enough since subtree:true)
     */
    var mutationObserver;
    /*
     * unique instance of a custom listener for repositioning invoking the positionRegistry
     */
    var repositionListener;

    /*
     * holder of the current value of the page since previous onPageChanged event
     */
    var currentPage;

    /*
     * nullable callbacks provided to smartEditContractChangeListener for all the observed events
     */
    this._componentAddedCallback = null;
    this._componentRemovedCallback = null;
    this._componentResizedCallback = null;
    this._componentRepositionedCallback = null;
    this._onComponentChangedCallback = null;
    this._pageChangedCallback = null;

    /*
     * Method used in mutationObserverCallback that extracts from mutations the list of nodes added or removed
     * The nodes are returned within a pair along with their nullable closest smartEditComponent parent
     */
    var aggregateAddOrRemoveNodesAndTheirParents = function(mutations, addOrRemoveOperation) {

        var entries = lodash.flatten(mutations.filter(function(mutation) {
            //only keep mutations of type childList and for the selected addOrRemoveOperation (removedNodes or addedNodes)
            return mutation.type === MUTATION_TYPES.CHILD_LIST.NAME && mutation[addOrRemoveOperation].length;
        }).map(function(mutation) {

            //the mutated child may not be smartEditComponent, in such case we return their first level smartEditComponent children
            var children = lodash.flatten(Array.prototype.slice.call(mutation[addOrRemoveOperation])
                .filter(function(node) {
                    return node.nodeType === NODE_TYPES.ELEMENT;
                }).map(function(child) {
                    return componentHandlerService.isSmartEditComponent(child) ? child : Array.prototype.slice.call(componentHandlerService.getFirstSmartEditComponentChildren(child));
                }));

            // nodes are returned in pairs with their nullable parent
            var parents = componentHandlerService.getClosestSmartEditComponent(mutation.target);
            return children.map(function(node) {
                return {
                    node: node,
                    parent: parents.length ? parents[0] : null
                };
            });
        }));

        /*
         * Despite MutationObserver specifications it so happens that sometimes,
         * depending on the very way a parent node is added with its children,
         * parent AND children will appear in a same mutation. We then must only keep the parent
         * Since the parent will appear first, the filtering lodash.uniqWith will always return the parent as opposed to the child which is what we need
         */

        return lodash.uniqWith(entries, function(entry1, entry2) {
            return entry1.node.contains(entry2.node) || entry2.node.contains(entry1.node);
        });
    };

    /*
     * Method used in mutationObserverCallback that extracts from mutations the list of nodes the smartEdit contract attributes of which have changed
     * The nodes are returned within a pair along with their map of changed attributes
     */
    var aggregateMutationsOnSmartEditAttributes = function(mutations) {
        return mutations.filter(function(mutation) {
            return mutation.target.nodeType === NODE_TYPES.ELEMENT && componentHandlerService.isSmartEditComponent(mutation.target) && mutation.type === MUTATION_TYPES.ATTRIBUTES.NAME && smartEditAttributeNames.indexOf(mutation.attributeName) > -1;
        }).reduce(function(seed, mutation) {
            var targetEntry = seed.find(function(entry) {
                return entry.node === mutation.target;
            });
            if (!targetEntry) {
                targetEntry = {
                    node: mutation.target,
                    oldAttributes: {}
                };
                seed.push(targetEntry);
            }
            targetEntry.oldAttributes[mutation.attributeName] = mutation.oldValue;
            return seed;
        }, []);
    };

    /*
     * Methods used in mutationObserverCallback that determines whether the smartEdit contract page identifier MAY have changed in the DOM
     */
    var mutationsHasPageChange = function(mutations) {
        return mutations.find(function(mutation) {
            return mutation.type === MUTATION_TYPES.ATTRIBUTES.NAME && mutation.target.tagName === "BODY" && mutation.attributeName === "class";
        });
    };

    /*
     * convenience method to invoke a callback on a node and recursively on all its smartEditComponent children
     */
    var applyToSelfAndAllChildren = function(node, callback) {
        callback(node);
        Array.prototype.slice.call(componentHandlerService.getFirstSmartEditComponentChildren(node)).forEach(function(component) {
            applyToSelfAndAllChildren(component, callback);
        });
    };


    var repairParentResizeListener = function(childAndParent) {
        if (childAndParent.parent) {
            //the adding of a component is likely to destroy the DOM added by the resizeListener on the parent, it needs be restored
            resizeListener.unregister(childAndParent.parent);
            if (yjQuery.contains($document[0], childAndParent.parent)) {
                resizeListener.register(childAndParent.parent, this._componentResizedCallback.bind(undefined, childAndParent.parent));
                this._componentResizedCallback(childAndParent.parent);
            }
        }
    }.bind(this);

    /*
     * when a callback is executed we make sure that angular is synchronized since it is occurring outside the life cycle
     */
    var executeCallback = function(callback) {
        $rootScope.$evalAsync(callback);
    };

    /*
     * callback executed by the mutation observer every time mutations occur.
     * repositioning and resizing are not part of this except that every time a smartEditComponent is added,
     * it is registered within the positionRegistry and the resizeListener 
     */
    var mutationObserverCallback = function(mutations) {
        $log.debug(mutations);

        if (this._pageChangedCallback && mutationsHasPageChange(mutations)) {
            var newPageUUID = componentHandlerService.getPageUUID();
            if (currentPage !== newPageUUID) {
                executeCallback(this._pageChangedCallback.bind(undefined, newPageUUID));
            }
            currentPage = newPageUUID;
        }
        if (this._componentRemovedCallback) {
            aggregateAddOrRemoveNodesAndTheirParents(mutations, MUTATION_TYPES.CHILD_LIST.REMOVE_OPERATION).forEach(function(childAndParent) {
                //we unregister repositioning and resizing listeners on all the subtree of the removed smartEditComponent
                if (this._componentRepositionedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, positionRegistry.unregister);
                }
                if (this._componentResizedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, resizeListener.unregister);

                    repairParentResizeListener(childAndParent);
                }
                //the onComponentRemoved is called with the top of smartEditComponent subtree being removed and its nullable closest smartEditComponent parent
                executeCallback(this._componentRemovedCallback.bind(undefined, childAndParent.node, childAndParent.parent));
            }.bind(this));
        }
        if (this._componentAddedCallback) {
            aggregateAddOrRemoveNodesAndTheirParents(mutations, MUTATION_TYPES.CHILD_LIST.ADD_OPERATION).forEach(function(childAndParent) {
                //we register repositioning and resizing listeners on all the subtree of the removed smartEditComponent
                if (this._componentRepositionedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, positionRegistry.register);
                }
                if (this._componentResizedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, function(node) {
                        resizeListener.register(node, this._componentResizedCallback.bind(undefined, node));
                    }.bind(this));

                    repairParentResizeListener(childAndParent);
                }
                //the onComponentAdded is called with the top of smartEditComponent subtree being added
                executeCallback(this._componentAddedCallback.bind(undefined, childAndParent.node));
            }.bind(this));
        }

        if (this._onComponentChangedCallback) {
            aggregateMutationsOnSmartEditAttributes(mutations).forEach(function(entry) {
                //the onComponentChanged is called with the mutated smartEditComponent subtree and the map of old attributes
                executeCallback(this._onComponentChangedCallback.bind(undefined, entry.node, entry.oldAttributes));
            }.bind(this));
        }

    };

    /*
     * wrapping for test purposes
     */
    this._newMutationObserver = function(callback) {
        return new MutationObserver(callback);
    };

    /*
     * initializes and starts all DOM listeners:
     * - DOM mutations on smartEditComponents and page identifier (by Means of native MutationObserver)
     * - smartEditComponents repositioning (by means of querying, with an interval, the list of repositioned components from the positionRegistry)
     * - smartEditComponents resizing (by delegating to the injected resizeListener)
     */
    this.initListener = function() {

        try {
            currentPage = componentHandlerService.getPageUUID();
            if (this._pageChangedCallback) {
                executeCallback(this._pageChangedCallback.bind(undefined, currentPage));
            }
        } catch (e) {
            //case when the page that has just loaded is an asynchronous one
        }

        if (!mutationObserver) {
            mutationObserver = this._newMutationObserver(mutationObserverCallback.bind(this));
            mutationObserver.observe(smartEditContractChangeListenerObservableRoot.getObservableRoot(), OBSERVER_OPTIONS);

            // Start listening for DOM resize and repositioning (for pre-existing nodes as opposed to the ones added on the fly)
            componentHandlerService.getFromSelector("." + COMPONENT_CLASS).each(function(index, component) {
                // TODO: compile pre-existing nodes if and when renderService stops doing it;
                if (this._componentResizedCallback) {
                    resizeListener.register(component, this._componentResizedCallback.bind(undefined, component));
                }
                if (this._componentRepositionedCallback) {
                    positionRegistry.register(component);
                }
            }.bind(this));

            // Initiate polling for DOM repositioning
            if (this._componentRepositionedCallback) {
                repositionListener = $interval(function() {
                    positionRegistry.getRepositionedComponents().forEach(function(component) {
                        this._componentRepositionedCallback(component);
                    }.bind(this));

                }.bind(this), REPROCESS_TIMEOUT);
            }
        }
    };

    /*
     * stops and clean up all listeners
     */
    this.stopListener = function() {
        // Stop listening for DOM mutations
        if (mutationObserver) {
            mutationObserver.disconnect();
        }
        mutationObserver = null;
        // Stop listening for DOM resize
        resizeListener.dispose();
        // Stop listening for DOM repositioning
        if (repositionListener) {
            $interval.cancel(repositionListener);
            repositionListener = null;
        }
        positionRegistry.dispose();
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is added to the DOM
     * it is executed only once per subtree of smarteditComponent nodes being added
     * the callback is invoked with the root node of a subtree
     */
    this.onComponentAdded = function(callback) {
        this._componentAddedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is removed from the DOM
     * it is executed only once per subtree of smarteditComponent nodes being removed
     * the callback is invoked with the root node of a subtree and its parent
     */
    this.onComponentRemoved = function(callback) {
        this._componentRemovedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time at least one of the smartEdit contract attributes of a smarteditComponent node is changed
     * the callback is invoked with the mutated node itself and the map of old attributes
     */
    this.onComponentChanged = function(callback) {
        this._onComponentChangedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is resized in the DOM
     * the callback is invoked with the resized node itself
     */
    this.onComponentResized = function(callback) {
        this._componentResizedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is repositioned (as per Node.getBoundingClientRect()) in the DOM
     * the callback is invoked with the resized node itself
     */
    this.onComponentRepositioned = function(callback) {
        this._componentRepositionedCallback = callback;
    };

    /*
     * registers a unique callback to be executed:
     * - upon bootstrapping smartEdit IF the page identifier is available
     * - every time the page identifier is changed in the DOM (see componentHandlerService.getPageUUID())
     * the callback is invoked with the new page identifier read from componentHandlerService.getPageUUID()
     */
    this.onPageChanged = function(callback) {
        this._pageChangedCallback = callback;
    };

}]);
