/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('seBooleanModule', [])
    .component('seBoolean', {
        templateUrl: 'booleanTemplate.html',
        restrict: 'E',
        controller: function() {
            this.$onInit = function() {
                if (this.model[this.qualifier] === undefined) {
                    this.model[this.qualifier] = false;
                }
            };
        },
        bindings: {
            field: '<',
            qualifier: '<',
            model: '<',
        }
    });
