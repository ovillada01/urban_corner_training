/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yPopupOverlayModule
 * @description
 * This module provides the yPopupOverlay directive, and it's helper services
 */
angular.module('yPopupOverlayModule', [
    'yPopupOverlayUtilsModule',
    'yjqueryModule',
    'yLoDashModule',
    'compileHtmlModule',
    'componentHandlerServiceModule'
])

/**
 * A string, representing the prefix of the generated UUID for each yPopupOverlay.
 * This uuid is added as an attribute to the overlay DOM element.
 */
.constant('yPopupOverlayUuidPrefix', 'ypo-uuid-_')


/**
 *  @ngdoc directive
 *  @name yPopupOverlayModule.directive:yPopupOverlay
 *  @restrict A
 *
 *  @description
 *  The yPopupOverlay is meant to be a directive that allows popups/overlays to be displayed attached to any element.
 *  The element that the directive is applied to is called the anchor element. Once the popup is displayed, it is
 *  positioned relative to the anchor, depending on the configuration provided.<br />
 *  <br />
 *  <h3>Scrolling Limitation</h3>
 *  In this initial implementation, it appends the popup element to the body, and positions itself relative to body.
 *  This means that it can handle default window/body scrolling, but if the anchor is contained within an inner
 *  scrollable DOM element then the positions will not work correctly.
 *
 *  @param {< Object} yPopupOverlay A popup overlay configuration object that must contain either a template or a templateUrl
 *  @param {string} yPopupOverlay.template|templateUrl An html string template or a url to an html file
 *  @param {string =} [yPopupOverlay.halign='right'] Aligns the popup horizontally
 *      relative to the anchor (element). Accepts values: 'left' or 'right'.
 *  @param {string =} [yPopupOverlay.valign='bottom'] Aligns the popup vertically
 *      relative to the anchor (element). Accepts values: 'top' or 'bottom'.
 *  @param {@ string =} yPopupOverlayTrigger 'true'|'false'|'click' Controls when the overlay is displayed.
 *      If yPopupOverlayTrigger is true, the overlay is displayed, if false (or something other then true or click)
 *      then the overlay is hidden.
 *      If yPopupOverlayTrigger is 'click' then the overlay is displayed when the anchor (element) is clicked on
 *  @param {& expression =} yPopupOverlayOnShow An angular expression executed whenever this overlay is displayed
 *  @param {& expression =} yPopupOverlayOnHide An angular expression executed whenever this overlay is hidden
 */
.directive('yPopupOverlay', function() {
    return {
        restrict: 'A',
        scope: false,
        controllerAs: '$yPopupCtrl',
        controller: 'yPopupOverlayController'
    };
})

.controller('yPopupOverlayController', ['$scope', '$element', '$document', '$compile', '$attrs', '$timeout', '$interpolate', 'yjQuery', 'lodash', 'yPopupOverlayUuidPrefix', 'yPopupOverlayUtilsDOMCalculations', 'yPopupOverlayUtilsClickOrderService', 'componentHandlerService', function($scope,
    $element,
    $document,
    $compile,
    $attrs,
    $timeout,
    $interpolate,
    yjQuery,
    lodash,
    yPopupOverlayUuidPrefix,
    yPopupOverlayUtilsDOMCalculations,
    yPopupOverlayUtilsClickOrderService,
    componentHandlerService) {

    /**
     * Check if a yjQuery element contains a child element.
     * @param parentElement
     * @param childElement Click event target
     * @returns {boolean|*} True if parent contains child
     */
    function isChildOfElement(parentElement, childElement) {
        return parentElement[0] === childElement || yjQuery.contains(parentElement[0], childElement);
    }

    /**
     * Namespace to protect the non-isolated scope for this directive
     */
    function SafeController() {

        /**
         * Calculates the size of the popup content and stores it.
         * Returns true if the size has changed since the previous call.
         */
        this.checkPopupSizeChanged = function() {
            if (this.popupElement) {
                var firstChildOfRootPopupElement = this.popupElement.children().first();
                var popupBounds = firstChildOfRootPopupElement[0].getBoundingClientRect();
                var changed = popupBounds.width !== this.popupSize.width || popupBounds.height !== this.popupSize.height;
                this.popupSize = {
                    width: popupBounds.width,
                    height: popupBounds.height
                };
                if (changed) {
                    this.updatePopupElementPositionAndSize();
                }
            }
            return false;
        }.bind(this);

        /**
         *
         */
        this.updatePopupElementPositionAndSize = function() {
            if (this.popupElement) {
                try {
                    // Always calculate based on first child of popup, but apply css to root of popup
                    // otherwise any applied css may harm the content by enforcing size
                    var anchorBounds = $element[0].getBoundingClientRect();
                    var position = yPopupOverlayUtilsDOMCalculations.calculatePreferredPosition(anchorBounds,
                        this.popupSize.width, this.popupSize.height, this.config.valign, this.config.halign);
                    yPopupOverlayUtilsDOMCalculations.adjustHorizontalToBeInViewport(position);
                    this.popupElement.css(position);
                } catch (e) {
                    // There are racing conditions where some of the elements are not ready yet...
                    // Since we're constantly recalculating, this is just an easy way to avoid all these conditions
                }
            }
        }.bind(this);

        this.togglePoppup = function($event) {
            $event.stopPropagation();
            $event.preventDefault();
            if (this.popupDisplayed) {
                this.hide();
            } else {
                this.show();
            }
        }.bind(this);

        this.getTemplateString = function() {
            var outerElement = componentHandlerService.getFromSelector('<div>');
            outerElement.attr('data-uuid', this.uuid);
            outerElement.addClass('y-popover-outer');

            var innerElement;
            if (this.config.template) {
                innerElement = componentHandlerService.getFromSelector('<div>');
                innerElement.html(this.config.template);
            } else if (this.config.templateUrl) {
                innerElement = yjQuery('<data-ng-include>');
                innerElement.attr('src', "'" + this.config.templateUrl + "'");
            } else {
                throw "yPositiongetTemplateString() - Missing template";
            }

            innerElement.addClass('y-popover-inner');
            outerElement.append(innerElement);

            return outerElement[0].outerHTML;
        }.bind(this);


        this.hide = function() {
            if (this.popupDisplayed) {
                yPopupOverlayUtilsClickOrderService.unregister(this);
                if (this.popupElementScope) {
                    this.popupElementScope.$destroy();
                    this.popupElementScope = null;
                }
                if (this.popupElement) {
                    this.popupElement.remove();
                    this.popupElement = null;
                }
                if ($attrs.yPopupOverlayOnHide) {
                    // We want to evaluate this angular expression inside of a digest cycle
                    $timeout(function() {
                        $scope.$eval($attrs.yPopupOverlayOnHide);
                    });
                }
                this.resetPopupSize();
            }
            this.popupDisplayed = false;
        }.bind(this);

        this.show = function() {
            if (!this.popupDisplayed) {
                this.popupElement = this.getTemplateString();
                this.popupElementScope = $scope.$new(false);
                this.popupElement = $compile(this.popupElement)(this.popupElementScope);
                var containerElement = angular.element($document[0].body);
                this.updatePopupElementPositionAndSize();
                this.popupElement.appendTo(containerElement);
                angular.element(function() {
                    this.updatePopupElementPositionAndSize();
                    yPopupOverlayUtilsClickOrderService.register(this);
                    if ($attrs.yPopupOverlayOnShow) {
                        // We want to evaluate this angular expression inside of a digest cycle
                        $timeout(function() {
                            $scope.$eval($attrs.yPopupOverlayOnShow);
                        });
                    }

                }.bind(this));
            }
            this.popupDisplayed = true;
        }.bind(this);

        this.updateTriggers = function(newValue) {
            if (typeof newValue === 'undefined') {
                newValue = 'click';
            }
            if (this.oldTrigger === newValue) {
                return;
            }
            this.oldTrigger = newValue;
            if (this.untrigger) {
                this.untrigger();
            }
            if (newValue === 'click') {
                angular.element($element).on('click', this.togglePoppup);
                this.untrigger = function() {
                    angular.element($element).off('click', this.togglePoppup);
                };
                return;
            }
            if (newValue === "true" || newValue === true) {
                this.show();
            } else {
                this.hide();
            }
        }.bind(this);

        /**
         * Handles click event, triggered by the
         */
        this.onBodyElementClicked = function($event) {
            if (this.popupElement) {
                var isPopupClicked = isChildOfElement(this.popupElement, $event.target);
                var isAnchorClicked = isChildOfElement($element, $event.target);
                if (!isPopupClicked && !isAnchorClicked) {
                    this.hide();
                    $event.stopPropagation();
                    $event.preventDefault();
                    return true;
                }
            }
            return false;
        }.bind(this);

        this.resetPopupSize = function() {
            this.popupSize = {
                width: 0,
                height: 0
            };
        }.bind(this);

        this.$doCheck = function() {
            if (this.active) {
                this.checkPopupSizeChanged();
                var trigger = $interpolate($attrs.yPopupOverlayTrigger)($scope);
                if (trigger !== this.doCheckTrigger) {
                    this.doCheckTrigger = trigger;
                    this.updateTriggers(trigger);
                }
            }
        }.bind(this);

        this.$onInit = function() {
            this.uuid = lodash.uniqueId(yPopupOverlayUuidPrefix);
            this.popupDisplayed = false;
            this.config = $scope.$eval($attrs.yPopupOverlay);
            this.resetPopupSize();
            this.updateTriggers();

            // only activate
            this.active = this.config !== 'undefined';
        };

        this.$onDestroy = function() {
            if (this.untrigger) {
                this.untrigger();
            }
            this.hide();
        }.bind(this);
    }


    // --------------------------------------------
    // --------------- PUBLIC API -----------------
    // --------------------------------------------

    var safeController = new SafeController();

    this.$doCheck = function() {
        safeController.$doCheck();
    };

    this.$onInit = function() {
        safeController.$onInit();
    };

    this.$onDestroy = function() {
        safeController.$onDestroy();
    };

    // EXPOSED Close API for users to programmatically close
    // the popup via their template or templateUrl
    $scope.closePopupOverlay = function() {
        safeController.hide();
    }.bind(this);

}]);
