/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('experienceSelectorModule', ['eventServiceModule', 'genericEditorModule', 'sharedDataServiceModule', 'iframeClickDetectionServiceModule', 'iFrameManagerModule', 'siteServiceModule', 'genericEditorModule', 'resourceLocationsModule', 'previewDataDropdownPopulatorModule', 'experienceServiceModule', 'yLoDashModule', 'functionsModule', 'catalogServiceModule'])
    .constant("ERROR_CATALOG_SERVICE_FAILED", "catalogService received rejected promise: ")
    .constant("WARN_NO_PRODUCT_CATALOGS_FOUND", "No product catalogs we found")
    .controller('ExperienceSelectorController', ['$q', '$scope', '$filter', '$timeout', '$document', 'systemEventService', 'siteService', 'sharedDataService', 'iframeClickDetectionService', 'iFrameManager', 'GenericEditor', 'experienceService', 'EVENTS', 'TYPES_RESOURCE_URI', 'PREVIEW_RESOURCE_URI', 'lodash', 'formatDateAsUtc', 'catalogService', function($q, $scope, $filter, $timeout, $document, systemEventService, siteService, sharedDataService, iframeClickDetectionService, iFrameManager, GenericEditor, experienceService, EVENTS, TYPES_RESOURCE_URI, PREVIEW_RESOURCE_URI, lodash, formatDateAsUtc, catalogService) {
        var selectedExperience = {};
        var siteCatalogs = {};

        this.$onInit = function() {
            this.resetExperienceSelector = function() {

                sharedDataService.get('experience')
                    .then(function(experience) {

                        selectedExperience = lodash.cloneDeep(experience);
                        delete selectedExperience.siteDescriptor;
                        delete selectedExperience.languageDescriptor;
                        selectedExperience.previewCatalog = experience.siteDescriptor.uid + '_' + experience.catalogDescriptor.catalogId + '_' + experience.catalogDescriptor.catalogVersion;
                        selectedExperience.language = experience.languageDescriptor.isocode;
                        selectedExperience.productCatalogVersions = selectedExperience.productCatalogVersions.map(function(productCatalogVersion) {
                            return productCatalogVersion.uuid;
                        });

                        this.setFields();

                    }.bind(this));
            }.bind(this);

            this.reset = function() {
                this.editor.reset($scope.componentForm);
                this.dropdownStatus.isopen = false;
            }.bind(this);

            this.submit = function() {
                return this.editor.submit($scope.componentForm);
            }.bind(this);

            this.isSubmitDisabled = function(componentForm) {
                return !(this.editor.isDirty() && componentForm.$valid);
            }.bind(this);

        };

        this.returnProductCatalogsByUuids = function(versionUuids) {
            var versions = [];
            siteCatalogs.productCatalogs.forEach(function(catalog) {
                if (catalog.versions) {
                    var versionMatch = catalog.versions.find(function(version) {
                        return versionUuids.indexOf(version.uuid) > -1;
                    });
                    versions.push({
                        catalog: catalog.catalogId,
                        catalogVersion: versionMatch.version
                    });
                }
            });
            return versions;
        };

        this.setFields = function() {
            sharedDataService.get('configuration').then(function(configuration) {
                this.editor = new GenericEditor({
                    smarteditComponentType: 'PreviewData',
                    smarteditComponentId: null,
                    structureApi: TYPES_RESOURCE_URI + '/:smarteditComponentType?mode=DEFAULT',
                    contentApi: configuration && configuration.previewTicketURI || PREVIEW_RESOURCE_URI,
                    updateCallback: null,
                    content: selectedExperience
                });

                this.editor.alwaysShowReset = true;
                this.editor.alwaysShowSubmit = true;

                this.editor.init();

                this.editor.preparePayload = function(originalPayload) {
                    siteCatalogs.siteId = originalPayload.previewCatalog.split('_')[0];
                    siteCatalogs.catalogId = originalPayload.previewCatalog.split('_')[1];
                    siteCatalogs.catalogVersion = originalPayload.previewCatalog.split('_')[2];

                    return catalogService.getProductCatalogsForSite(siteCatalogs.siteId).then(function(productCatalogs) {
                        siteCatalogs.productCatalogs = productCatalogs;
                        siteCatalogs.productCatalogVersions = originalPayload.productCatalogVersions;

                        return sharedDataService.get('configuration').then(function(configuration) {

                            return sharedDataService.get('experience').then(function(experience) {

                                return siteService.getSiteById(siteCatalogs.siteId).then(function(siteDescriptor) {

                                    var transformedPayload = lodash.cloneDeep(originalPayload);
                                    delete transformedPayload.previewCatalog;
                                    delete transformedPayload.time;

                                    transformedPayload.catalog = siteCatalogs.catalogId;
                                    transformedPayload.catalogVersion = siteCatalogs.catalogVersion;
                                    transformedPayload.resourcePath = configuration.domain + siteDescriptor.previewUrl;
                                    transformedPayload.pageId = experience.pageId;
                                    transformedPayload.time = originalPayload.time;
                                    transformedPayload.catalogVersions = this.returnProductCatalogsByUuids(originalPayload.productCatalogVersions);

                                    return transformedPayload;

                                }.bind(this));

                            }.bind(this));

                        }.bind(this));

                    }.bind(this));
                }.bind(this);

                this.editor.updateCallback = function(payload, response) {
                    delete this.smarteditComponentId; //to force a permanent POST
                    this.dropdownStatus.isopen = false;
                    sharedDataService.get('configuration').then(function(configuration) {
                        // Then perform the actual update.
                        var experienceParams = lodash.cloneDeep(response);
                        delete experienceParams.catalog;
                        delete experienceParams.time;
                        experienceParams.siteId = siteCatalogs.siteId;
                        experienceParams.catalogId = siteCatalogs.catalogId;
                        experienceParams.time = formatDateAsUtc(payload.time);
                        experienceParams.pageId = response.pageId;
                        experienceParams.productCatalogVersions = siteCatalogs.productCatalogVersions;
                        experienceService.buildDefaultExperience(experienceParams).then(function(experience) {
                            sharedDataService.set('experience', experience).then(function() {
                                systemEventService.sendAsynchEvent(EVENTS.EXPERIENCE_UPDATE);
                                var fullPreviewUrl = configuration.domain + experience.siteDescriptor.previewUrl;
                                iFrameManager.loadPreview(fullPreviewUrl, response.ticketId);
                                var preview = {
                                    previewTicketId: response.ticketId,
                                    resourcePath: fullPreviewUrl
                                };
                                sharedDataService.set('preview', preview);
                            });
                        });

                    }.bind(this));
                }.bind(this);
            }.bind(this));

        };

        this.submitButtonText = 'se.componentform.actions.apply';
        this.cancelButtonText = 'se.componentform.actions.cancel';
        this.modalHeaderTitle = 'se.experience.selector.header';

        this.$postLink = function() {

            //FIXME: unregister
            iframeClickDetectionService.registerCallback('closeExperienceSelector', function() {
                if (this.dropdownStatus && this.dropdownStatus.isopen) {
                    this.editor.reset($scope.componentForm);
                    this.dropdownStatus.isopen = false;
                }
            }.bind(this));

            this.unRegFn = systemEventService.registerEventHandler('OVERLAY_DISABLED', function() {
                if (this.dropdownStatus && this.dropdownStatus.isopen) {
                    this.editor.reset($scope.componentForm);
                    this.dropdownStatus.isopen = false;
                }
            }.bind(this));
        };

        this.$onDestroy = function() {
            this.unRegFn();
        };

    }])
    .component('experienceSelector', {
        templateUrl: 'genericEditorTemplate.html',
        transclude: true,
        controller: 'ExperienceSelectorController',
        controllerAs: 'ge',
        bindings: {
            experience: '=',
            dropdownStatus: '=',
            resetExperienceSelector: '='
        }
    });
