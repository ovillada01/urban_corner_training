NG_DOCS={
  "sections": {
    "smartEdit": "SmartEdit",
    "smartEditContainer": "SmartEdit Container"
  },
  "pages": [
    {
      "section": "smartEdit",
      "id": "alertServiceModule",
      "shortName": "alertServiceModule",
      "type": "overview",
      "moduleName": "alertServiceModule",
      "shortDescription": "Smartedit has only an empty (proxied) implementation of alertService.",
      "keywords": "alertservice alertservicemodule container details empty implementation overview smartedit"
    },
    {
      "section": "smartEdit",
      "id": "alertServiceModule.alertService",
      "shortName": "alertServiceModule.alertService",
      "type": "service",
      "moduleName": "alertServiceModule",
      "shortDescription": "Smartedit has only an empty (proxied) implementation of alertService.",
      "keywords": "alertservice alertservicemodule container details empty implementation service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "authenticationModule",
      "shortName": "authenticationModule",
      "type": "overview",
      "moduleName": "authenticationModule",
      "shortDescription": "The authenticationModule",
      "keywords": "allows application authenticate authentication authenticationmodule entry logout management module overview points resources service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "authenticationModule.object:DEFAULT_AUTH_MAP",
      "shortName": "DEFAULT_AUTH_MAP",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The default authentication map contains the entry points to use before an authentication map",
      "keywords": "authentication authenticationmodule configuration default entry loaded map object points service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "authenticationModule.object:DEFAULT_CREDENTIALS_MAP",
      "shortName": "DEFAULT_CREDENTIALS_MAP",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The default credentials map contains the credentials to use before an authentication map",
      "keywords": "authentication authenticationmodule configuration credentials default loaded map object service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "authenticationModule.service:authenticationService",
      "shortName": "authenticationService",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The authenticationService is used to authenticate and logout from SmartEdit.",
      "keywords": "access allows application assigned associated authenticate authenticated authenticates authentication authenticationmap authenticationmodule authenticationservice compare current currently default default_authentication_entry_point entered entry entrypoint expiry expression filterentrypoints flag flow identifying indicate indicates isauthenticated isauthentrypoint key landing logout management map maps matching method object point points promise provided re-authenticated re-authentication redirects registered regular relevant removes requested resolves resource resourcelocationsmodule resources retrieve returns service setreauthinprogress shareddataservice shareddataservicemodule smartedit stored successful suitable token tokens true uri url user"
    },
    {
      "section": "smartEdit",
      "id": "authorizationModule.authorizationService",
      "shortName": "authorizationModule.authorizationService",
      "type": "overview",
      "moduleName": "authorizationModule.authorizationService",
      "shortDescription": "The authorization module provides a service that checks if the current user was granted certain",
      "keywords": "api authorization authorizationmodule authorizationservice backend checks contacting current global granted module overview permissions poll rest restservicefactorymodule service smartedit user"
    },
    {
      "section": "smartEdit",
      "id": "authorizationModule.AuthorizationService",
      "shortName": "authorizationModule.AuthorizationService",
      "type": "service",
      "moduleName": "authorizationModule",
      "shortDescription": "This service makes calls to the Global Permissions REST API to check if the current user was",
      "keywords": "api array authorizationmodule authorizationservice calls canperformoperation check checks comma current empty error false global granted hasglobalpermissions list method operation parameter perform permissionnames permissions required rest separated service smartedit string throw true user values version"
    },
    {
      "section": "smartEdit",
      "id": "catalogServiceModule",
      "shortName": "catalogServiceModule",
      "type": "overview",
      "moduleName": "catalogServiceModule",
      "shortDescription": "The catalogServiceModule",
      "keywords": "catalog catalogs catalogservicemodule fetches hybris module overview platform registered service site sites smartedit"
    },
    {
      "section": "smartEdit",
      "id": "catalogServiceModule.service:catalogService",
      "shortName": "catalogService",
      "type": "service",
      "moduleName": "catalogServiceModule",
      "shortDescription": "The Catalog Service fetches catalogs for a specified site or for all sites registered on the hybris platform using",
      "keywords": "active api array attempt built caches calls catalog catalogid catalogs catalogservicemodule catalogversion catalogversionuuid clearcache cmswebservices configured content contentcatalogid convenience corresponds default descriptor descriptors details determines empties exception experience fail fetched fetches find finds flagged full getactivecontentcatalogversionbycatalogid getactiveproductcatalogversionbycatalogid getallcatalogsgroupedbyid getallcontentcatalogsgroupedbyid getcatalogbyversion getcatalogsforsite getcatalogversionbyuuid getcontentcatalogactiveversion getcontentcatalogsforsite getdefaultsiteforcontentcatalog getproductcatalogsforsite groupings hybris identified invoker iscontentcatalogversionnonactive list match method object objects optional perform performed permitted platform product productcatalogid promise properties provided registered resolves resourcelocationsmodule rest retrieve retrieveuricontext return returnactivecatalogversionuids returned search service shareddataservice shareddataservicemodule simply site siteid sites siteuid smartedit sorted storing thrown true uid uricontext uuid version versions wrapped"
    },
    {
      "section": "smartEdit",
      "id": "compileHtmlModule",
      "shortName": "compileHtmlModule",
      "type": "overview",
      "moduleName": "compileHtmlModule",
      "shortDescription": "The compileHtmlModule",
      "keywords": "compile compilehtmlmodule directive evaluate html markup overview smartedit"
    },
    {
      "section": "smartEdit",
      "id": "compileHtmlModule.directive:compileHtml",
      "shortName": "compileHtml",
      "type": "directive",
      "moduleName": "compileHtmlModule",
      "shortDescription": "Directive responsible for evaluating and compiling HTML markup.",
      "keywords": "compile-html compiled compilehtmlmodule compiling data-ng-click directive evaluated evaluating html injectedcontext item markup onlink path property responsible smartedit string"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule",
      "shortName": "componentHandlerServiceModule",
      "type": "overview",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "this module aims at handling smartEdit components both on the original storefront and the smartEdit overlay",
      "keywords": "aims componenthandlerservicemodule components handling module original overlay overview smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.COMPONENT_CLASS",
      "shortName": "componentHandlerServiceModule.COMPONENT_CLASS",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the css class of the smartEdit components as per contract with the storefront",
      "keywords": "class component_class componenthandlerservicemodule components contract css object smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.componentHandlerService",
      "shortName": "componentHandlerServiceModule.componentHandlerService",
      "type": "service",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "The service provides convenient methods to get DOM references of smartEdit components both in the original laye rof the storefornt and in the smartEdit overlay",
      "keywords": "api applicable application argument body call catalog catalogversionuuid child children class closest combined component componenthandlerservice componenthandlerservicemodule components contained container container_id_attribute container_type_attribute contentslot contract convenient corresponding css cssclass current deep defaults depths determines direct div dom element encoding evaluates example extracts false fetched flag getoverlaycomponent getoverlaycomponentwithinslot handler id_attribute identified ids iframe invoked laye layer level list loaded matching method methods methodsof_getallcomponentsselector methodsof_getallslotsselector methodsof_getcatalogversionuuid methodsof_getcatalogversionuuidfrompage methodsof_getclosestsmarteditcomponent methodsof_getcomponent methodsof_getcomponentinoverlay methodsof_getcomponentunderslot methodsof_getfirstsmarteditcomponentchildren methodsof_getfromselector methodsof_getid methodsof_getoriginalcomponent methodsof_getoriginalcomponentswithinslot methodsof_getoriginalcomponentwithinslot methodsof_getoverlay methodsof_getoverlaycomponent methodsof_getoverlaycomponentwithinslot methodsof_getpageuid methodsof_getpageuuid methodsof_getparent methodsof_getparentslotforcomponent methodsof_getparentslotuuidforcomponent methodsof_getslotoperationrelatedid methodsof_getslotoperationrelatedtype methodsof_getslotoperationrelateduuid methodsof_gettype methodsof_getuuid methodsof_isexternalcomponent methodsof_isoverlayon methodsof_issmarteditcomponent methodsof_setid methodsof_settype node object operations optional original originalcomponent overlay pageuid pageuuid parameter parent passed perform provided references relevant represents resides restrict retrieves return returned returns rof search searched selector service set sets slot slotid smae-layer smartedit smartedit-component-id smarteditcomponentid smarteditcomponenttype smarteditslotid storefornt storefront string true type type_attribute typically uid uuid version visible wrapper wrapping yjquery"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id attribute of the smartEdit container, when applicable, as per contract with the storefront",
      "keywords": "applicable attribute componenthandlerservicemodule container container_id_attribute contract object smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type attribute of the smartEdit container, when applicable, as per contract with the storefront",
      "keywords": "applicable attribute componenthandlerservicemodule container container_type_attribute contract object smartedit storefront type"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.CONTENT_SLOT_TYPE",
      "shortName": "componentHandlerServiceModule.CONTENT_SLOT_TYPE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type value of the smartEdit slots as per contract with the storefront",
      "keywords": "componenthandlerservicemodule content_slot_type contract object slots smartedit storefront type"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.ID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.ID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract id_attribute object smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.OVERLAY_COMPONENT_CLASS",
      "shortName": "componentHandlerServiceModule.OVERLAY_COMPONENT_CLASS",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the css class of the smartEdit component clones copied to the storefront overlay",
      "keywords": "class clones component componenthandlerservicemodule copied css object overlay overlay_component_class smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.OVERLAY_ID",
      "shortName": "componentHandlerServiceModule.OVERLAY_ID",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the identifier of the overlay placed in front of the storefront to where all smartEdit component decorated clones are copied.",
      "keywords": "clones component componenthandlerservicemodule copied decorated front identifier object overlay overlay_id smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.SMARTEDIT_ATTRIBUTE_PREFIX",
      "shortName": "componentHandlerServiceModule.SMARTEDIT_ATTRIBUTE_PREFIX",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "If the storefront needs to expose more attributes than the minimal contract, these attributes must be prefixed with this constant value",
      "keywords": "attributes componenthandlerservicemodule constant contract expose minimal object prefixed smartedit smartedit_attribute_prefix storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.SMARTEDIT_IFRAME_ID",
      "shortName": "componentHandlerServiceModule.SMARTEDIT_IFRAME_ID",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id of the iframe which contains storefront",
      "keywords": "componenthandlerservicemodule iframe object smartedit smartedit_iframe_id storefront"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.TYPE_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.TYPE_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit storefront type type_attribute"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the uuid attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit storefront uuid uuid_attribute"
    },
    {
      "section": "smartEdit",
      "id": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the uuid attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit storefront uuid uuid_attribute"
    },
    {
      "section": "smartEdit",
      "id": "confirmationModalServiceModule",
      "shortName": "confirmationModalServiceModule",
      "type": "overview",
      "moduleName": "confirmationModalServiceModule",
      "shortDescription": "The confirmationModalServiceModule",
      "keywords": "allows confirmation confirmationmodalservicemodule content dependent modal modalservicemodule module opening overview prompt service smartedit title"
    },
    {
      "section": "smartEdit",
      "id": "confirmationModalServiceModule.service:confirmationModalService",
      "shortName": "confirmationModalService",
      "type": "service",
      "moduleName": "confirmationModalServiceModule",
      "shortDescription": "Service used to open a confirmation modal in which an end-user can confirm or cancel an action. A confirmation modal",
      "keywords": "action actioned button cancel configuration confirm confirmation confirmationmodalservicemodule consists content context default description displayed end-user i18n initialized input key method modal modalservice modalservicemodule object open optional override overrides passed promise property provided rejected required resolved service smartedit text title user"
    },
    {
      "section": "smartEdit",
      "id": "contextualMenuServiceModule.contextualMenuService",
      "shortName": "contextualMenuServiceModule.contextualMenuService",
      "type": "service",
      "moduleName": "contextualMenuServiceModule",
      "shortDescription": "The contextualMenuService returns an instance of",
      "keywords": "add components contextual contextualmenuservice contextualmenuserviceclass contextualmenuservicemodule instance items menus returns service slots smartedit"
    },
    {
      "section": "smartEdit",
      "id": "contextualMenuServiceModule.ContextualMenuServiceClass",
      "shortName": "contextualMenuServiceModule.ContextualMenuServiceClass",
      "type": "service",
      "moduleName": "contextualMenuServiceModule",
      "shortDescription": "The ContextualMenuServiceClass is used to add contextual menu items for each component.",
      "keywords": "action activate activated add additems alert ant-like applicable application applied array arrays assigned banner call callback called classes clicked clicking code compares component component-type componentid components componenttype condition configuration container containerid containertype content contextual contextual_menu contextualmenuitemsmap contextualmenuservice contextualmenuserviceclass contextualmenuservicemodule converts css decorator democlass deprecated determine display displayclass displayed dom edit element enabled entry exactly example excludes executed file full function generated getcontextualmenubytype getcontextualmenuitems holds html i18nkey icon iconidle iconnonidle identified identifies idle ileftbtns includes input invoked item itemkey items key key-value leftmenuitems limit list localize location map mapping maps match matched menu menus message method module moremenuitems names non-idle number object optional options pair pass passing payload performed png promise properties provided refreshmenuitems regex regexpfactory remove removeitembykey removes required resolves responsive retrieve retrieved return returned returns sample selected service set simple size slot slotid smaller smallicon smartedit specific string style template templateurl translated type types url usage valid version visible vlide wildcard wire wrapping"
    },
    {
      "section": "smartEdit",
      "id": "contextualMenuServiceModule.object:REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT",
      "shortName": "REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT",
      "type": "object",
      "moduleName": "contextualMenuServiceModule",
      "shortDescription": "Name of the event triggered whenever SmartEdit decides to update items in contextual menus.",
      "keywords": "contextual contextualmenuservicemodule decides event items menus object smartedit triggered update"
    },
    {
      "section": "smartEdit",
      "id": "crossFrameEventServiceModule.service:CrossFrameEventService",
      "shortName": "CrossFrameEventService",
      "type": "service",
      "moduleName": "crossFrameEventServiceModule",
      "shortDescription": "The Cross Frame Event Service is responsible for publishing and subscribing events within and between frames.",
      "keywords": "call callback cross crossframeeventservicegatway crossframeeventservicemodule data event eventid events eventservice eventservicemodule frame frames function gateway gatewayfactory gatewayfactorymodule handler identifier invoked listening message messagegateway method order payload promise publish publishes publishing register registereventhandler registration resolve responsible send sendevent service smartedit subscribe subscribing systemeventservice transmit unsubscribe"
    },
    {
      "section": "smartEdit",
      "id": "dateFormatterModule",
      "shortName": "dateFormatterModule",
      "type": "overview",
      "moduleName": "dateFormatterModule",
      "shortDescription": "The dateFormatterModule",
      "keywords": "dateformattermodule desired displaying format formatter module overview smartedit"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule",
      "shortName": "dateTimePickerModule",
      "type": "overview",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePickerModule",
      "keywords": "datetimepicker datetimepickerlocalizationservice datetimepickermodule directive displaying localize module open opened overview picker service smartedit time tooling"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule.directive:dateTimePicker",
      "shortName": "dateTimePicker",
      "type": "directive",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePicker",
      "keywords": "attributes data-date-formatter datetimepicker datetimepickermodule default desired directive eg- filter format format-type medium pass short smartedit tag template text type"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule.directive:dateTimePicker",
      "shortName": "dateTimePicker",
      "type": "directive",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePicker",
      "keywords": "datetimepicker datetimepickermodule directive smartedit"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule.object: tooltipsMap",
      "shortName": "tooltipsMap",
      "type": "object",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "Contains a map of all tooltips to be localized in the date time picker",
      "keywords": "datetimepickermodule localized map object picker smartedit time tooltips tooltipsmap"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule.object:resolvedLocaleToMomentLocaleMap",
      "shortName": "resolvedLocaleToMomentLocaleMap",
      "type": "object",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "Contains a map of all inconsistent locales ISOs between SmartEdit and MomentJS",
      "keywords": "datetimepickermodule inconsistent isos locales map momentjs object smartedit"
    },
    {
      "section": "smartEdit",
      "id": "dateTimePickerModule.service:dateTimePickerLocalizationService",
      "shortName": "dateTimePickerLocalizationService",
      "type": "service",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePickerLocalizationService is responsible for both localizing the date time picker as well as the tooltips",
      "keywords": "datetimepickerlocalizationservice datetimepickermodule localizing picker responsible service smartedit time tooltips"
    },
    {
      "section": "smartEdit",
      "id": "decoratorServiceModule.service:decoratorService",
      "shortName": "decoratorService",
      "type": "service",
      "moduleName": "decoratorServiceModule",
      "shortDescription": "This service enables and disables decorators. It also maps decorators to SmartEdit component types–regardless if they are enabled or disabled.",
      "keywords": "activated active addmappings ant-like array boolean bound card component componentid componenttype decorated decorator decorator1 decorator2 decorator3 decorator4 decorator5 decoratorkey decorators decoratorservice decoratorservicemodule depends determines disable disabled disables displaycondition displayed eligible enable enabled enables exact expression full getdecoratorsforcomponent group identified identifies key key-map list map maps matching method methods_addmappings methods_enable myexacttype pattern perspective promise referenced regular resolve resolves retrieved retrieves returns service smartedit type types uniquely wild"
    },
    {
      "section": "smartEdit",
      "id": "dragAndDropServiceModule",
      "shortName": "dragAndDropServiceModule",
      "type": "overview",
      "moduleName": "dragAndDropServiceModule",
      "shortDescription": "The dragAndDropServiceModule",
      "keywords": "drag draganddropservicemodule drop functionality html overview service smartedit wraps"
    },
    {
      "section": "smartEdit",
      "id": "dragAndDropServiceModule.service:dragAndDropService",
      "shortName": "dragAndDropService",
      "type": "service",
      "moduleName": "dragAndDropServiceModule",
      "shortDescription": "The dragAndDropService wraps over the HTML 5 drag and drop functionality to provide a browser independent interface and very",
      "keywords": "applied applies apply applyall area array basic browser callback called clean configuration configurationid configurations configurationsidlist current custom doesn dom drag draganddropservice draganddropservicemodule dragentercallback draggable dragging dragovercallback drop dropcallback droppable dropped element elements enable enablescrolling enters execute executed experience flag forces function functionality helper hovers html identifier ids iframe ignored image images independent instance instances intended interface items leaves locate managed markdragstarted method mouse note operation outcallback parameter prepare provide provided register registered registering registers remove removed removes richer scrolling selector service smartedit sourceselector specifies start startcallback started stopcallback stopped support targetselector time track unique unregister update updates user wraps yjquery"
    },
    {
      "section": "smartEdit",
      "id": "DropdownPopulatorInterfaceModule.DropdownPopulatorInterface",
      "shortName": "DropdownPopulatorInterfaceModule.DropdownPopulatorInterface",
      "type": "service",
      "moduleName": "DropdownPopulatorInterfaceModule",
      "shortDescription": "Interface describing the contract of a DropdownPopulator fetched through dependency injection by the",
      "keywords": "additional append array attribute attributes building call comma contract current currentpage data default depend dependency dependson deprecated describing descriptor determining directive dropdown dropdownpopulator dropdownpopulatorinterface dropdownpopulatorinterfacemodule dropdowns edited false fetch fetchall fetched fetchpage field filter filtered full genericeditor genericeditormodule idattribute include injection interface ispaged item items key label labelattributes list lists making matches meant method mode model modified number object objects opposed option optional options optionsdropdownpopulator optionsdropdownpopulatormodule ordered orderedlabelattributes original paged pagesize params payload populate populateattributes populates populator promise properties property query request resolving rest retrieve returns search searches searchterm sedropdown sedropdownmodule selected selection separated service set setting smartedit specifies string term types uri uridropdownpopulator uridropdownpopulatormodule user work"
    },
    {
      "section": "smartEdit",
      "id": "editorFieldMappingServiceModule.service:editorFieldMappingService",
      "shortName": "editorFieldMappingService",
      "type": "service",
      "moduleName": "editorFieldMappingServiceModule",
      "shortDescription": "The editorFieldMappingServices contains the strategy that the genericEditor directive",
      "keywords": "add addfieldmapping adhere api area based box call check choose class cmsstructureenumtype cmsstructuretype cmsstructuretypes component componenttypename conditions configurable configuration contract created currently custom customsanitize date-time default defined directive discriminator displays dropdown editor editorfieldmappingservicemodule editorfieldmappingservices editors enabled ensure ensures enum field fields filterable form function functionsmodule generic genericeditor genericeditormodule getfieldmapping holder html identified input instructions invoked list mandatory mapping media method multiple note null optional override overrides path payload picker preselected program property propertyeditortemplate provided rendered required rest retrieve retrieved retrieves returning sanitize sedropdown sedropdownmodule select selects service set short shortstringtemplate smartedit smarteditcomponenttype specific specifies strategy structure structuretypename system template templates text transformed type values ways web"
    },
    {
      "section": "smartEdit",
      "id": "editorFieldMappingServiceModule.service:PropertyEditorTemplate",
      "shortName": "PropertyEditorTemplate",
      "type": "object",
      "moduleName": "editorFieldMappingServiceModule",
      "shortDescription": "The purpose of the property editor template is to assign a value to model[qualifier].",
      "keywords": "achieve actual api assign de defined described description edited editor editorfieldmappingservicemodule en entities field full genericeditor genericeditormodule goal identifier language localized map model object parent property purpose qualifier receive scope service smartedit structure template templates"
    },
    {
      "section": "smartEdit",
      "id": "eventServiceModule",
      "shortName": "eventServiceModule",
      "type": "overview",
      "moduleName": "eventServiceModule",
      "shortDescription": "eventServiceModule contains an event service which is supported by the SmartEdit gatewayFactory to propagate events between SmartEditContainer and SmartEdit.",
      "keywords": "event events eventservicemodule gatewayfactory gatewayfactorymodule overview propagate service smartedit smarteditcontainer supported"
    },
    {
      "section": "smartEdit",
      "id": "eventServiceModule.EVENT_SERVICE_MODE_ASYNCH",
      "shortName": "eventServiceModule.EVENT_SERVICE_MODE_ASYNCH",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "A constant used in the constructor of the Event Service to specify asynchronous event transmission.",
      "keywords": "asynchronous constant event event_service_mode_asynch eventservicemodule object service smartedit transmission"
    },
    {
      "section": "smartEdit",
      "id": "eventServiceModule.EVENT_SERVICE_MODE_SYNCH",
      "shortName": "eventServiceModule.EVENT_SERVICE_MODE_SYNCH",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "A constant that is used in the constructor of the Event Service to specify synchronous event transmission.",
      "keywords": "constant event event_service_mode_synch eventservicemodule object service smartedit synchronous transmission"
    },
    {
      "section": "smartEdit",
      "id": "eventServiceModule.EVENTS",
      "shortName": "eventServiceModule.EVENTS",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "Events that are fired/handled in the SmartEdit application",
      "keywords": "application events eventservicemodule fired object smartedit"
    },
    {
      "section": "smartEdit",
      "id": "eventServiceModule.EventService",
      "shortName": "eventServiceModule.EventService",
      "type": "service",
      "moduleName": "eventServiceModule",
      "shortDescription": "The event service is used to transmit events synchronously or asynchronously. It also contains options to send",
      "keywords": "asynchronous asynchronously constant constants data defaultmode depending event event_service_mode_asynch event_service_mode_synch eventid events eventservice eventservicemodule handlers identifier method mode options payload register send sendevent service set sets smartedit synchronous synchronously transmission transmit unregister"
    },
    {
      "section": "smartEdit",
      "id": "ExperienceInterceptorModule.experienceInterceptor",
      "shortName": "ExperienceInterceptorModule.experienceInterceptor",
      "type": "service",
      "moduleName": "ExperienceInterceptorModule",
      "shortDescription": "A HTTP request interceptor which intercepts all &#39;cmswebservices/catalogs&#39; requests and adds the current catalog and version",
      "keywords": "$httpprovider adding adds angularjs array called catalog cmswebservices config configuration context current current_context_catalog current_context_catalog_version data define dependencies details experience experienceinterceptor experienceinterceptormodule factories factory headers holds http https initialization injected interceptor interceptors intercepts loaded method methods note object org page_context_catalog page_context_catalog_version page_context_site_id passed preview promise registered replaced request requests retrieve returns service set shared siteid smartedit stored uri url variables version versions"
    },
    {
      "section": "smartEdit",
      "id": "experienceServiceModule.service:experienceService",
      "shortName": "experienceService",
      "type": "service",
      "moduleName": "experienceServiceModule",
      "shortDescription": "The experience Service deals with building experience objects given a context.",
      "keywords": "building context deals experience experienceservicemodule objects service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "exponentialRetrylModule",
      "shortName": "exponentialRetrylModule",
      "type": "overview",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "This module provides the exponentialRetry service.",
      "keywords": "exponentialretry exponentialretrylmodule module overview service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "exponentialRetrylModule.object:EXPONENTIAL_RETRY_DEFAULT_SETTING",
      "shortName": "EXPONENTIAL_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default exponentialretrylmodule object retry setting smartedit values"
    },
    {
      "section": "smartEdit",
      "id": "exponentialRetrylModule.service:exponentialRetry",
      "shortName": "exponentialRetry",
      "type": "service",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide an exponential delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts calculate calculatenextdelay canretry check current delay exponential exponentialretrylmodule false functionality maxattempt maxbackoff maximum method minbackoff minimum number perform provide request retries retry returns service smartedit strategy time true valid"
    },
    {
      "section": "smartEdit",
      "id": "featureInterfaceModule",
      "shortName": "featureInterfaceModule",
      "type": "overview",
      "moduleName": "featureInterfaceModule",
      "keywords": "featureinterfacemodule overview smartedit"
    },
    {
      "section": "smartEdit",
      "id": "featureInterfaceModule.service:FeatureServiceInterface",
      "shortName": "FeatureServiceInterface",
      "type": "service",
      "moduleName": "featureInterfaceModule",
      "shortDescription": "The interface stipulates how to register features in the SmartEdit application and the SmartEdit container.",
      "keywords": "_registeraliases action activate addcontextualmenubutton adddecorator additems addmappings addtoolbaritem ant-like api applicable application applied bound button buttons callback callbacks classes clicked clicking component componentid components componenttype concrete condition configuration container containerid containertype content contextual contextualmenuservice contextualmenuservicemodule cross css deal decorator decorators decoratorservice decoratorservicemodule default defined delegates description descriptioni18nkey disable disabled disablingcallback displayclass displayed dom element eligible enable enabled enablingcallback entry event exist expression feature featureinterfacemodule featurekey features frame full function functions gateway gatewayfactory gatewayfactorymodule getfeatureproperty handler hold holding html hybrid_action i18n icon iconidle iconnonidle icons identified identifies idle image images implementation include included instance instances interface invocation invoke invoked item items key keythe l18n list location mappings match meant menu method methods methods_additems methods_disable methods_enable methods_register namei18nkey needed non-idle null object optional options order parameter performed permissions perspective perspectiveinterfacemodule perspectiveserviceinterface point properties property propertyname reference regexpkey register registered registers registry regular removed representing represents required respective returns selected selects separate service simplified slot slotid smaller smallicon smartedit smarteditcontainer stand stipulates stores strict style template templates toolbar toolbarid toolbarinterfacemodule toolbarserviceinterface tooltip translated triggered type types unique uniquely url urls user version web wildcard wrapper wrapping"
    },
    {
      "section": "smartEdit",
      "id": "FetchDataHandlerInterfaceModule.FetchDataHandlerInterface",
      "shortName": "FetchDataHandlerInterfaceModule.FetchDataHandlerInterface",
      "type": "service",
      "moduleName": "FetchDataHandlerInterfaceModule",
      "shortDescription": "Interface describing the contract of a fetchDataHandler fetched through dependency injection by the",
      "keywords": "contract defined dependency describing descriptor dropdowns eligible entities entity fetch fetchdatahandler fetchdatahandlerinterface fetchdatahandlerinterfacemodule fetched field findbymask genericeditor genericeditormodule getbyid identifier identifying injection interface list mask matching method populate promise resolving returns search service smartedit type witch"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule",
      "shortName": "functionsModule",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "provides a list of useful functions that can be used as part of the SmartEdit framework.",
      "keywords": "framework functions functionsmodule list service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.closeOpenModalsOnBrowserBack",
      "shortName": "functionsModule.closeOpenModalsOnBrowserBack",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "close any open modal window when a user clicks browser back button",
      "keywords": "$modalstack angular-ui browser button clicks close closeopenmodalsonbrowserback functionsmodule modal modalstack open service smartedit user window"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.convertToArray",
      "shortName": "functionsModule.convertToArray",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "convertToArray will convert the given object to array.",
      "keywords": "array convert converttoarray created elements functionsmodule input inputobject key object original output service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.copy",
      "shortName": "functionsModule.copy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "copy will do a deep copy of the given input object.",
      "keywords": "candidate copied copy deep functionsmodule input javascript object service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.customTimeout",
      "shortName": "functionsModule.customTimeout",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "customTimeout will call the javascrit&#39;s native setTimeout method to execute a given function after a specified period of time.",
      "keywords": "$timeout assert better call customtimeout difficult duration end-to-end execute executed func function functionsmodule javascrit method milliseconds native period service settimeout smartedit testing time"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.escapeHtml",
      "shortName": "functionsModule.escapeHtml",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "escapeHtml will escape &amp;, &lt;, &gt;, &quot; and &#39; characters .",
      "keywords": "characters escape escaped escapehtml functionsmodule service smartedit string"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.extend",
      "shortName": "functionsModule.extend",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "extend provides a convenience to either default a new child or &quot;extend&quot; an existing child with the prototype of the parent",
      "keywords": "child childclass convenience default existing extend extended functionsmodule parent parentclass prototype service set smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.extractFromElement",
      "shortName": "functionsModule.extractFromElement",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object",
      "keywords": "dom element elements extract extracted extractfromelement extractionselector functionsmodule html identifying matching object parent parses queriable selector selectors service smartedit string yjquery"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.generateIdentifier",
      "shortName": "functionsModule.generateIdentifier",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "generateIdentifier will generate a unique string based on system time and a random generator.",
      "keywords": "based functionsmodule generate generateidentifier generator identifier random service smartedit string system time unique"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.getOrigin",
      "shortName": "functionsModule.getOrigin",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "returns document location origin",
      "keywords": "browsers caters document function functionsmodule gap getorigin location origin returns service smartedit support w3c"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.getQueryString",
      "shortName": "functionsModule.getQueryString",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "getQueryString will convert a given object into a query string.",
      "keywords": "code convert functionsmodule getquerystring input key1 key2 key3 list object output params query sample service smartedit snippet string value1 value2 value3 var"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.getURI",
      "shortName": "functionsModule.getURI",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will return the URI part of a URL",
      "keywords": "functionsmodule geturi return returned service smartedit uri url"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.hitch",
      "shortName": "functionsModule.hitch",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "hitch will create a new function that will pass our desired context (scope) to the given function.",
      "keywords": "assigned binded binding context create desired function functionsmodule hitch method parameters pass pre-bind scope service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.injectJS",
      "shortName": "functionsModule.injectJS",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "injectJS will inject script tags into html for a given set of sources.",
      "keywords": "array callback callbacks configuration configurations execute extract functionsmodule html inject injectjs javascript method object potential provided script service set smartedit source sources tag tags triggered wired"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.isBlank",
      "shortName": "functionsModule.isBlank",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "isBlank will check if a given string is undefined or null or empty.",
      "keywords": "check empty false functionsmodule input inputstring isblank null returns service smartedit string true undefined"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.merge",
      "shortName": "functionsModule.merge",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "merge will merge the contents of two objects together into the first object.",
      "keywords": "contents functionsmodule javascript merge object objects result service smartedit source target"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.parseHTML",
      "shortName": "functionsModule.parseHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object, stripping any JavaScript from the HTML.",
      "keywords": "dom functionsmodule html javascript object parse parsehtml parses queriable representation service smartedit string stringhtml stripping"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.parseQuery",
      "shortName": "functionsModule.parseQuery",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parseQuery will convert a given query string to an object.",
      "keywords": "code convert functionsmodule input key1 key2 key3 object output params parsed parsequery query sample service smartedit snippet string value1 value2 value3 var"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.regExpFactory",
      "shortName": "functionsModule.regExpFactory",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "regExpFactory will convert a given pattern into a regular expression.",
      "keywords": "append convert converted expression functionsmodule generated method pattern prepend proper regex regexpfactory regular replaces service smartedit string wildcards"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.sanitize",
      "shortName": "functionsModule.sanitize",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "escapes any harmful scripting from a string, leaves innocuous HTML untouched/b&gt;",
      "keywords": "functionsmodule harmful html innocuous leaves sanitize sanitized scripting service smartedit string untouched"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.sanitizeHTML",
      "shortName": "functionsModule.sanitizeHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "sanitizeHTML will remove breaks and space .",
      "keywords": "breaks escaped functionsmodule html remove sanitized sanitizehtml service smartedit space string"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:formatDateAsUtc",
      "shortName": "formatDateAsUtc",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Formats provided dateTime as utc.",
      "keywords": "datetime format formats formatted functionsmodule provided service smartedit string utc"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:getDataFromResponse",
      "shortName": "getDataFromResponse",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "when provided with a response returned from a backend call, will filter the response",
      "keywords": "array backend call data filter functionsmodule interest provided response retrieve returned returns service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:getKeyHoldingDataFromResponse",
      "shortName": "getKeyHoldingDataFromResponse",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "when provided with a response returned from a backend call, will filter the response",
      "keywords": "array backend call data filter functionsmodule holding interest key provided response retrieve returned returns service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:isAllTruthy",
      "shortName": "isAllTruthy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Iterate on the given array of Functions, return true if each function returns true",
      "keywords": "arguments array function functions functionsmodule iterate return returns service smartedit true"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:isAnyTruthy",
      "shortName": "isAnyTruthy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Iterate on the given array of Functions, return true if at least one function returns true",
      "keywords": "arguments array function functions functionsmodule iterate return returns service smartedit true"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:isFunctionEmpty",
      "shortName": "isFunctionEmpty",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will determine whether a function body is empty",
      "keywords": "body boolean determine empty evaluate fn function functionsmodule service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:isObjectEmptyDeep",
      "shortName": "isObjectEmptyDeep",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will check if the object is empty and will return true if each and every property of the object is empty",
      "keywords": "boolean check empty evaluate functionsmodule object property return service smartedit true"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:resetObject",
      "shortName": "resetObject",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Resets a given object&#39;s properties&#39; values",
      "keywords": "functionsmodule modelobject object properties reset resets returns service smartedit structure targetobject values"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.service:URIBuilder",
      "shortName": "URIBuilder",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "builder or URIs, build() method must be invoked to actually retrieve a URI",
      "keywords": "$modalstack angular-ui build builder functionsmodule invoked map matching method modalstack names params placeholder placeholders replaceparams retrieve service smartedit substitute uri uris values"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.toPromise",
      "shortName": "functionsModule.toPromise",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "toPromise&lt;/&gt; transforms a function into a function that is guaranteed to return a Promise that resolves to the",
      "keywords": "exceptino fails function functionsmodule guaranteed invocation object original promise rejected rejects resolves return service smartedit topromise transforms"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.trim",
      "shortName": "functionsModule.trim",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "trim will remove spaces at the beginning and end of a given string.",
      "keywords": "functionsmodule input inputstring modified newly remove service smartedit spaces string trim"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.uniqueArray",
      "shortName": "functionsModule.uniqueArray",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "uniqueArray will return the first Array argument supplemented with new entries from the second Array argument.",
      "keywords": "argument array array1 array2 entries functionsmodule javascript return second service smartedit supplemented uniquearray"
    },
    {
      "section": "smartEdit",
      "id": "functionsModule.unsafeParseHTML",
      "shortName": "functionsModule.unsafeParseHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object, preserving any JavaScript present in the HTML.",
      "keywords": "dom failure functionsmodule html javascript location note object originating parse parses preserves preserving queriable representation result safe service smartedit string stringhtml strings unsafeparsehtml vulnerability xss"
    },
    {
      "section": "smartEdit",
      "id": "gatewayFactoryModule.gatewayFactory",
      "shortName": "gatewayFactoryModule.gatewayFactory",
      "type": "service",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "The Gateway Factory controls the creation of and access to MessageGateway",
      "keywords": "access application argument caches call calls channel clients construct controls corresponding create created creategateway creates creation dispatches error event events exist factory fail gateway gatewayfactory gatewayfactorymodule gatewayid handle handler handling identifier initializes initlistener instances lifecycle logged message messagegateway method newly null order postmessage prevent provide return returns second service smartedit subsequent"
    },
    {
      "section": "smartEdit",
      "id": "gatewayFactoryModule.MessageGateway",
      "shortName": "gatewayFactoryModule.MessageGateway",
      "type": "service",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "The Message Gateway is a private channel that is used to publish and subscribe to events across iFrame",
      "keywords": "angularjs attempt attempts based benefits boundaries call callback chain channel controlled creation cross-origin current data event eventid events exist failure function gateway gatewayfactory gatewayfactorymodule gatewayid generated identifier iframe implementation implements instance instances interrupted invoked key listener listening message messagegateway messages method number occurs optional order origins parameter payload pk postmessage primary private promise promises publish publishes receiving registers reject rejected resolve retries scenarios send service side smartedit subscribe technology underlying unsubscribe w3c-compliant works"
    },
    {
      "section": "smartEdit",
      "id": "gatewayFactoryModule.object:TIMEOUT_TO_RETRY_PUBLISHING",
      "shortName": "TIMEOUT_TO_RETRY_PUBLISHING",
      "type": "object",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "Period between two retries of a gatewayFactoryModule.MessageGateway to publish an event",
      "keywords": "browser event explorer frames gatewayfactorymodule greater internet messagegateway needed object period postmessage process publish retries smartedit time"
    },
    {
      "section": "smartEdit",
      "id": "gatewayFactoryModule.object:WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY",
      "shortName": "WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY",
      "type": "object",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "the name of the configuration key containing the list of white listed storefront domain names",
      "keywords": "configuration domain gatewayfactorymodule key list listed names object smartedit storefront white"
    },
    {
      "section": "smartEdit",
      "id": "gatewayProxyModule.gatewayProxy",
      "shortName": "gatewayProxyModule.gatewayProxy",
      "type": "service",
      "moduleName": "gatewayProxyModule",
      "shortDescription": "To seamlessly integrate the gateway factory between two services on different frames, you can use a gateway",
      "keywords": "allowing api attaches automatically avoid body call calls communication declared default delegates empty explicit factory forward frames function functions gateway gatewayfactory gatewayfactorymodule gatewayid gatewayproxy gatewayproxymodule initforservice inner instance instances integrate internaly listeners method methods methodssubset module mutate mutates param process promise promises property provide provided providing proxied proxy publish registers registration requires result seamlessly service services set simplifies smartedit string stub trigger turned unique unnecessarily ways wraps"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule",
      "shortName": "genericEditorModule",
      "type": "overview",
      "moduleName": "genericEditorModule",
      "keywords": "genericeditormodule overview smartedit"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.directive:genericEditor",
      "shortName": "genericEditor",
      "type": "directive",
      "moduleName": "genericEditorModule",
      "shortDescription": "Component responsible for generating custom HTML CRUD form for any smarteditComponent type.",
      "keywords": "api arguments binding button callback called calls cancel component content contentapi contract controller controls create created creates crud current custom customonsubmit data delete deleted described directive display dom edited editor element example expected experience exposes extracted find form format forms fulfills function generating generic generic-editor genericeditor genericeditormodule getapi getcomponent html identifier indicates initial inner instance invoked invoker isdirty isvalid json local making method model modified object onsubmit optional original overridden parameter params pass pristine promise read reset response responsible rest return returns scope server service set sets shareddataservice smartedit smarteditcomponent smarteditcomponentid smarteditcomponenttype storefront string structure structureapi submit successful type update updatecallback updated uri uricontext valid widgets"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.object:GENERIC_EDITOR_LOADED_EVENT",
      "shortName": "GENERIC_EDITOR_LOADED_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify subscribers that GenericEditor is loaded.",
      "keywords": "event genericeditor genericeditormodule loaded notify object smartedit subscribers"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT",
      "shortName": "GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify GenericEditor about errors that can be relevant to it.",
      "keywords": "errors event generic_editor_unrelated_validation_messages_event genericeditor genericeditormodule notification notify object receive received relevant situation smartedit validation"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT",
      "shortName": "GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify GenericEditor about messages (errors, warnings) that can be relevant to it.",
      "keywords": "allows delivered editor editors event generic genericeditor genericeditormodule list map messages notification notify object optional optional-id payload properties receive received relevant send sendasynchevent situation smartedit specific systemeventservice target targetgenericeditorid unrelatedvalidationmessages validation warnings"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.object:genericEditorApi",
      "shortName": "genericEditorApi",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "The generic editor&#39;s api object exposing public functionality",
      "keywords": "api changes clearmessages clears component content copy current editor everytime exposing function functionality generic genericeditormodule getcomponent messages method model object oncontentchange public reinitialize replace returns smartedit triggered updatecomponent updates validation"
    },
    {
      "section": "smartEdit",
      "id": "genericEditorModule.service:GenericEditor",
      "shortName": "GenericEditor",
      "type": "service",
      "moduleName": "genericEditorModule",
      "shortDescription": "The Generic Editor is a class that makes it possible for SmartEdit users (CMS managers, editors, etc.) to edit components in the SmartEdit interface.",
      "keywords": "actual advised api array associated attempted attributes based boolean build built call case class cmsstructureenumtype cmsstructuretype code comparing component componentform components componenttypes content convention crud current data datahandler datetime de default defining details determine determined directive dirty display displayed documentation dropdown dropdowns edit editable edited editor editorfieldmappingservice editorfieldmappingservicemodule editors element en english entries entry enum error errors example exception expect expected expects expression extensible fail fallback false fetch fetchdatahandlerinterface fetchdatahandlerinterfacemodule fetches field fields filter filtered form format fr french fulfill function functionality generic genericeditor genericeditorapi genericeditormodule getapi hindi holds html i18nkey i18nkeyforsomequalifier1 i18nkeyforsomequalifier2 i18nkeyforsomequalifier3 i18nkeyforsomequalifier4 i18nkeyforsomequalifier5 i18nkeyforsomequalifier6 i18nkeyforsomequalifier7 identifier implementation implies indicates indicator initialization interface invoked isdirty iso json key label language learn list local localized long longstring manage managers mandatory map mask match mechanism media message messages method methods_refreshoptions modified mypackage naming non-localized object occurs option options orientation original owned pattern payload payloads performing populates post predicate preparepayload pristine property provided public qualified qualifier qualifier1 qualifier2 qualifier3 qualifier4 reads received refreshoptions regular request requested requests required requirements requires reset resource response rest return returned returns richtext saved saves saving search second select selection serialized server service sets shortstring smartedit somequalifier1 somequalifier2 somequalifier3 somequalifier4 somequalifier5 somequalifier6 somequalifier7 sort specific strategy string strings structure structures subject submit support templates throw timestamps transformed transforms translated true type types update updated updates users validation validationerror values warning widget widgets"
    },
    {
      "section": "smartEdit",
      "id": "hasOperationPermissionModule",
      "shortName": "hasOperationPermissionModule",
      "type": "overview",
      "moduleName": "hasOperationPermissionModule",
      "shortDescription": "This module provides a directive used to determine if the current user has permission to perform the action defined",
      "keywords": "action current defined determine directive dom elements hasoperationpermissionmodule key module overview perform permission removes smartedit user"
    },
    {
      "section": "smartEdit",
      "id": "hasOperationPermissionModule.directive:hasOperationPermission",
      "shortName": "hasOperationPermission",
      "type": "directive",
      "moduleName": "hasOperationPermissionModule",
      "shortDescription": "Authorization HTML mark-up that will remove elements from the DOM if the user does not have authorization defined",
      "keywords": "access array authorization called check comma-separated commas context current data defined directive dom elements extra function has-operation-permission hasoperationpermissionmodule html included input list mark-up names object objects parameter permission permissionservice permissionserviceinterface permissionserviceinterfacemodule property remove rule separated service set smartedit string structured takes user validate verify"
    },
    {
      "section": "smartEdit",
      "id": "httpAuthInterceptorModule.httpAuthInterceptor",
      "shortName": "httpAuthInterceptorModule.httpAuthInterceptor",
      "type": "service",
      "moduleName": "httpAuthInterceptorModule",
      "shortDescription": "Makes it possible to perform global authentication by intercepting requests before they are forwarded to the server",
      "keywords": "$http $httpprovider adding adds application array authentication call called code config configuration dependencies factories factory forwarded forwards global holds http httpauthinterceptor httpauthinterceptormodule injected intercepted intercepting interceptor interceptors intercepts method methods object perform registered request requests resource responses rest returns server service smartedit token"
    },
    {
      "section": "smartEdit",
      "id": "httpErrorInterceptorServiceModule",
      "shortName": "httpErrorInterceptorServiceModule",
      "type": "overview",
      "moduleName": "httpErrorInterceptorServiceModule",
      "shortDescription": "This module provides the functionality to add custom HTTP error interceptors.",
      "keywords": "add code custom error execute fails functionality http httperrorinterceptorservicemodule interceptors module overview request smartedit time"
    },
    {
      "section": "smartEdit",
      "id": "httpErrorInterceptorServiceModule.service:httpErrorInterceptorService",
      "shortName": "httpErrorInterceptorService",
      "type": "service",
      "moduleName": "httpErrorInterceptorServiceModule",
      "shortDescription": "The httpErrorInterceptorService provides the functionality to add custom HTTP error interceptors.",
      "keywords": "$q add addinterceptor alertservice alertservicemodule angular associated call called calls controller current custom customerrorinterceptor custominterceptorexample default defined designed error examplecontroller factory fails fulfill function functionality functions generic http httperrorinterceptorservice httperrorinterceptorservicemodule interceptor interceptors iterates iteration js matches message method modified modifies module object override pair predicate registered reject rejected represented request resolved resolves response responseerror return returning sequentially service showdanger smartedit specific status stops time true unregister unregistercustominterceptor var"
    },
    {
      "section": "smartEdit",
      "id": "i18nInterceptorModule.object:I18NAPIROOT",
      "shortName": "I18NAPIROOT",
      "type": "object",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "The I18NAPIroot is a hard-coded URI that is used to initialize the translationServiceModule.",
      "keywords": "hard-coded i18n_resource_uri i18napiroot i18ninterceptor i18ninterceptormodule initialize intercepts methods_request object replaces request resourcelocationsmodule service smartedit translationservicemodule uri"
    },
    {
      "section": "smartEdit",
      "id": "i18nInterceptorModule.object:UNDEFINED_LOCALE",
      "shortName": "UNDEFINED_LOCALE",
      "type": "object",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "The undefined locale set as the preferred language of the translationServiceModule so that",
      "keywords": "browser i18ninterceptor i18ninterceptormodule intercept language locale methods_request object preferred replace request service set smartedit translationservicemodule undefined"
    },
    {
      "section": "smartEdit",
      "id": "i18nInterceptorModule.service:i18nInterceptor",
      "shortName": "i18nInterceptor",
      "type": "service",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "A HTTP request interceptor that intercepts all i18n calls and handles them as required in the i18nInterceptor.request method.",
      "keywords": "$httpprovider adding angularjs appends array called calls config configuration defined dependencies factories factory getresolvelocale handles http https i18n i18n_resource_uri i18napiroot i18ninterceptor i18ninterceptormodule injected interceptor interceptors intercepts invoked languageservice languageservicemodule locale method methods methods_getresolvelocale methods_request object org passed promise provided registered replaces request requests required resourcelocationsmodule retrieved returns service smartedit url"
    },
    {
      "section": "smartEdit",
      "id": "iframeClickDetectionServiceModule",
      "shortName": "iframeClickDetectionServiceModule",
      "type": "overview",
      "moduleName": "iframeClickDetectionServiceModule",
      "shortDescription": "The iframeClickDetectionServiceModule",
      "keywords": "application bind click container contents detection document event events function functionality gatewayproxymodule iframe iframeclickdetectionservicemodule module mousedown overview propagate proxy requires smartedit transmit triggering yjquery"
    },
    {
      "section": "smartEdit",
      "id": "iframeClickDetectionServiceModule.service:iframeClickDetectionService",
      "shortName": "iframeClickDetectionService",
      "type": "service",
      "moduleName": "iframeClickDetectionServiceModule",
      "shortDescription": "The iframe Click Detection service leverages the gatewayProxy service to",
      "keywords": "callback calls click completes container delegates detection events function gatewayproxy gatewayproxymodule iframe iframeclickdetectionservicemodule leverages method mousedown oniframeclick promise propagate proxy resolved service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "interceptorHelperModule.service:interceptorHelper",
      "shortName": "interceptorHelper",
      "type": "service",
      "moduleName": "interceptorHelperModule",
      "shortDescription": "Helper service used to handle request and response in interceptors",
      "keywords": "body callback config error function handle handled handles helper initial interceptor interceptorhelpermodule interceptors method methodsof_handlerequest methodsof_handleresponse methodsof_handleresponseerror object promise rejecting request resolving response return service smartedit success"
    },
    {
      "section": "smartEdit",
      "id": "l10nModule.filter:l10n",
      "shortName": "l10n",
      "type": "filter",
      "moduleName": "l10nModule",
      "shortDescription": "Filter that accepts a localized map as input and returns the value corresponding to the resolvedLocale of languageServiceModule and defaults to the first entry.",
      "keywords": "accepts class corresponding defaults entry extended filter input instantiated interface isocodes l10nmodule language languageservicemodule localized localizedmap map resolvedlocale returns serves smartedit values"
    },
    {
      "section": "smartEdit",
      "id": "languageSelectorModule",
      "shortName": "languageSelectorModule",
      "type": "overview",
      "moduleName": "languageSelectorModule",
      "shortDescription": "The language selector module contains a directive which allow the user to select a language.",
      "keywords": "allow api backend call directive language languages languageselectormodule languageservice languageservicemodule list module order overview select selector service smartedit supported user"
    },
    {
      "section": "smartEdit",
      "id": "languageSelectorModule.directive:languageSelector",
      "shortName": "languageSelector",
      "type": "directive",
      "moduleName": "languageSelectorModule",
      "shortDescription": "Language selector provides a drop-down list which contains a list of supported languages.",
      "keywords": "directive drop-down language languages languageselectormodule list select selector smartedit supported system translate"
    },
    {
      "section": "smartEdit",
      "id": "languageServiceModule",
      "shortName": "languageServiceModule",
      "type": "overview",
      "moduleName": "languageServiceModule",
      "shortDescription": "The languageServiceModule",
      "keywords": "fetches language languages languageservicemodule module overview service site smartedit supported"
    },
    {
      "section": "smartEdit",
      "id": "languageServiceModule.SELECTED_LANGUAGE",
      "shortName": "languageServiceModule.SELECTED_LANGUAGE",
      "type": "object",
      "moduleName": "languageServiceModule",
      "shortDescription": "A constant that is used as key to store the selected language in the storageService",
      "keywords": "constant key language languageservicemodule object selected selected_language smartedit storageservice store"
    },
    {
      "section": "smartEdit",
      "id": "languageServiceModule.service:languageService",
      "shortName": "languageService",
      "type": "service",
      "moduleName": "languageServiceModule",
      "shortDescription": "The Language Service fetches all languages for a specified site using REST service calls to the cmswebservices languages API.",
      "keywords": "active api array browser callback calls check cmswebservices code current currently descriptor descriptors determine determines en en_us english fetched fetches format fr french function gateway getbrowserlanguageisocode getbrowserlocale getlanguagesforsite getresolvelocale getresolvelocaleisocode gettoolinglanguages i18n identifier iso isocode language languages languageservicemodule list locale method nativename object order preference promise properties register registerswitchlanguage required resolve resolved resolves rest retrieves saved selected service set setselectedlanguage site sites siteuid smartedit smarteditwebservices storage storefront supported switch system tooling true uid unique user"
    },
    {
      "section": "smartEdit",
      "id": "languageServiceModule.SWITCH_LANGUAGE_EVENT",
      "shortName": "languageServiceModule.SWITCH_LANGUAGE_EVENT",
      "type": "object",
      "moduleName": "languageServiceModule",
      "shortDescription": "A constant that is used as key to publish and receive events when a language is changed.",
      "keywords": "changed constant events key language languageservicemodule object publish receive smartedit switch_language_event"
    },
    {
      "section": "smartEdit",
      "id": "linearRetrylModule",
      "shortName": "linearRetrylModule",
      "type": "overview",
      "moduleName": "linearRetrylModule",
      "shortDescription": "This module provides the linearRetry service.",
      "keywords": "linearretry linearretrylmodule module overview service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "linearRetrylModule.object:LINEAR_RETRY_DEFAULT_SETTING",
      "shortName": "LINEAR_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "linearRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default linearretrylmodule object retry setting smartedit values"
    },
    {
      "section": "smartEdit",
      "id": "linearRetrylModule.service:linearRetry",
      "shortName": "linearRetry",
      "type": "service",
      "moduleName": "linearRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide a linear delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts base calculate calculatenextdelay canretry check current delay false functionality interval linear linearretrylmodule maxbackoff maximum maxretry method minbackoff minimum number perform provide request retries retry retryinterval returns service smartedit strategy time true valid"
    },
    {
      "section": "smartEdit",
      "id": "loadConfigModule",
      "shortName": "loadConfigModule",
      "type": "overview",
      "moduleName": "loadConfigModule",
      "shortDescription": "The loadConfigModule supplies configuration information to SmartEdit. Configuration is stored in key/value pairs.",
      "keywords": "array configuration exposes functionsmodule key load loadconfigmodule module ngresource object overview pairs resourcelocationsmodule service shareddataservicemodule smartedit stored supplies"
    },
    {
      "section": "smartEdit",
      "id": "loadConfigModule.service:LoadConfigManager",
      "shortName": "LoadConfigManager",
      "type": "service",
      "moduleName": "loadConfigModule",
      "shortDescription": "The LoadConfigManager is used to retrieve configurations stored in configuration API.",
      "keywords": "$log $resource _prettify api array conf configuration configurations converts converttoarray copy create defaulttoolinglanguage example function hitch key loadasarray loadasobject loadconfigmanager loadconfigmanagerservice loadconfigmodule mapped method object pairs promise resourcelocationsmodule retrieve retrieves returns service set shareddataservice smartedit stored values"
    },
    {
      "section": "smartEdit",
      "id": "loadConfigModule.service:loadConfigManagerService",
      "shortName": "loadConfigManagerService",
      "type": "service",
      "moduleName": "loadConfigModule",
      "shortDescription": "A service that is a singleton of loadConfigModule.service:LoadConfigManager  which is used to ",
      "keywords": "configuration entry loadconfigmanager loadconfigmodule module point retrieve service services singleton smartedit values"
    },
    {
      "section": "smartEdit",
      "id": "modalServiceModule",
      "shortName": "modalServiceModule",
      "type": "overview",
      "moduleName": "modalServiceModule",
      "shortDescription": "The modalServiceModule",
      "keywords": "achieving actions adding additionally affect behave button buttons chaning closing components create devoted easy goal manage modal modalmanager modalservice modalservicemodule module object open opened overview providing service smartedit style styles title window windows"
    },
    {
      "section": "smartEdit",
      "id": "modalServiceModule.modalService",
      "shortName": "modalServiceModule.modalService",
      "type": "service",
      "moduleName": "modalServiceModule",
      "shortDescription": "Convenience service to open and style a promise-based templated modal window.",
      "keywords": "$log $q $scope action actions acts addbutton additional angular angularjs animation animations app array button buttonhandlerfn buttons callbacks called caller calling cancel choose classed close closed common complex conf config configuration configurations content controller convenience css cssclasses custom data debug declared defer deferred dependency depending details determines dismiss display errorcallback example explicit explicitly factory feel fragment function functions html https injection inline key label list log logic method methods_addbutton methods_close modal modal_button_actions modal_button_styles modalcontroller modalmanager modalservice modalservicemodule module multiple object open org path piece promise promise-based reject rejected resolve resolved result return separated service setbuttonhandler share simple smartedit someresult someservice space style submit successcallback template templated templateinline templateurl title translated true validatesomething var ways window windows"
    },
    {
      "section": "smartEdit",
      "id": "modalServiceModule.object:MODAL_BUTTON_ACTIONS",
      "shortName": "MODAL_BUTTON_ACTIONS",
      "type": "object",
      "moduleName": "modalServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "action addbutton adding angular angularjs button close close_modal constant defines dismiss example executing existing getbuttons https indicates injectable label methods_getbuttons methods_open modal modal_button_actions modalmanager modalservice modalservicemodule mymodalmanager object open opening org performed promise property rejected resolved returned service smartedit window"
    },
    {
      "section": "smartEdit",
      "id": "modalServiceModule.object:MODAL_BUTTON_STYLES",
      "shortName": "MODAL_BUTTON_STYLES",
      "type": "object",
      "moduleName": "modalServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "addbutton adding angular button cancel cancel_button constant default defines equivalent example existing feel getbuttons indicates injectable label methods_getbuttons methods_open modal modal_button_styles modalmanager modalservice modalservicemodule mymodalmanager object open opening primary property save secondary service smartedit style styled submit window"
    },
    {
      "section": "smartEdit",
      "id": "modalServiceModule.service:ModalManager",
      "shortName": "ModalManager",
      "type": "service",
      "moduleName": "modalServiceModule",
      "shortDescription": "The ModalManager is a service designed to provide easy runtime modification to various aspects of a modal window,",
      "keywords": "$log $q $scope access action addbutton adding additionally allowing allows angularjs applied array aspects avoid button buttonhandlerfn buttonhandlerfunction buttonid buttonpressedcallback buttons callback callbacks called caller cancel cancelled case caution chain clone close closed closing code complexity conf configuration content continue controller corner create data debug default defer deferred designed details disablebutton disabled dismiss dismisscallback displayed don easy empty enable enablebutton enabled enables errorcallback example execute executed exposed fetched fired flag function getbutton getbuttons handler handlers happen header hello https i18n ignore implicitly instance isolated key kind label long manager matching method methods_addbutton methods_setbuttonhandler modal modal_button_actions modal_button_styles modalmanager modalservice modalservicemodule modaltestcontroller modification modifying multiple newly note null object open org parameter parameters pass passed passing press pressed prevent preventing process promise properties provide provided publicly read receives reference registered reject rejected rejecting remove removeallbuttons removebutton removed representing resolve resolved resolving return returned returns runtime sample scenarios scope service setbuttonhandler setdismisscallback setshowheaderdismiss setting showx single smartedit someresult string style submit successcallback suggested title top translated trigger true undefined unique unnecessary update validatesomething validating validation var window"
    },
    {
      "section": "smartEdit",
      "id": "nonvalidationErrorInterceptorModule.service:nonValidationErrorInterceptor",
      "shortName": "nonValidationErrorInterceptor",
      "type": "service",
      "moduleName": "nonvalidationErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 400. It removes all errors of type &#39;ValidationError&#39; and displays alert messages for non-validation errors.",
      "keywords": "alert code displays error errors http messages non-validation nonvalidationerrorinterceptormodule removes service smartedit type validationerror"
    },
    {
      "section": "smartEdit",
      "id": "notificationServiceInterfaceModule",
      "shortName": "notificationServiceInterfaceModule",
      "type": "overview",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The notification module provides a service to display visual cues to inform",
      "keywords": "application container cues display iframed inform module notification notificationserviceinterfacemodule overview service smartedit user visual"
    },
    {
      "section": "smartEdit",
      "id": "notificationServiceInterfaceModule.object:SE_NOTIFICATION_SERVICE_GATEWAY_ID",
      "shortName": "SE_NOTIFICATION_SERVICE_GATEWAY_ID",
      "type": "object",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The gateway UID used to proxy the NotificationService.",
      "keywords": "gateway notificationservice notificationserviceinterfacemodule object proxy smartedit uid"
    },
    {
      "section": "smartEdit",
      "id": "notificationServiceInterfaceModule.service:NotificationServiceInterface",
      "shortName": "NotificationServiceInterface",
      "type": "service",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The interface defines the methods required to manage notifications that are to be displayed to the user.",
      "keywords": "adds based configuration creates defines displayed empty error html identifier interface list manage method methods notification notificationid notifications notificationserviceinterfacemodule pushnotification removeallnotifications removenotification removes required service smartedit string template templateurl top unique url user"
    },
    {
      "section": "smartEdit",
      "id": "notificationServiceModule",
      "shortName": "notificationServiceModule",
      "type": "overview",
      "moduleName": "notificationServiceModule",
      "shortDescription": "The notification module provides a service to display visual cues to inform",
      "keywords": "application container cues display iframed inform module notification notificationservicemodule overview service smartedit user visual"
    },
    {
      "section": "smartEdit",
      "id": "notificationServiceModule.service:notificationService",
      "shortName": "notificationService",
      "type": "service",
      "moduleName": "notificationServiceModule",
      "shortDescription": "The notification service is used to display visual cues to inform the user of the state of the application.",
      "keywords": "application cues display inform notification notificationservicemodule service smartedit user visual"
    },
    {
      "section": "smartEdit",
      "id": "operationContextServiceModule",
      "shortName": "operationContextServiceModule",
      "type": "overview",
      "moduleName": "operationContextServiceModule",
      "shortDescription": "This module provides the functionality to register a set of url with their associated operation contexts.",
      "keywords": "associated contexts functionality module operation operationcontextservicemodule overview register set smartedit url"
    },
    {
      "section": "smartEdit",
      "id": "operationContextServiceModule.object:OPERATION_CONTEXT",
      "shortName": "OPERATION_CONTEXT",
      "type": "object",
      "moduleName": "operationContextServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "angular application constant context enumeration injectable object operation operationcontextservicemodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "operationContextServiceModule.service:operationContextService",
      "shortName": "operationContextService",
      "type": "service",
      "moduleName": "operationContextServiceModule",
      "keywords": "associated context find findoperationcontext matching method operation operationcontext operationcontextservice operationcontextservicemodule register request service smartedit url"
    },
    {
      "section": "smartEdit",
      "id": "optionsDropdownPopulatorModule.service:optionsDropdownPopulator",
      "shortName": "optionsDropdownPopulator",
      "type": "service",
      "moduleName": "optionsDropdownPopulatorModule",
      "shortDescription": "implementation of DropdownPopulatorInterface for &quot;EditableDropdown&quot; cmsStructureType",
      "keywords": "attribute cmsstructuretype dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown implementation method options optionsdropdownpopulatormodule populate service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "Page.object:Page",
      "shortName": "Page",
      "type": "object",
      "moduleName": "Page",
      "shortDescription": "An object representing the backend response to a paged query",
      "keywords": "array backend elements exceed object paged pagination pertaining property query representing requested response returned size smartedit"
    },
    {
      "section": "smartEdit",
      "id": "Page.object:Pagination",
      "shortName": "Pagination",
      "type": "object",
      "moduleName": "Page",
      "shortDescription": "An object representing the returned pagination information from backend",
      "keywords": "backend elements mask matching object pagination property representing returned smartedit total totalcount"
    },
    {
      "section": "smartEdit",
      "id": "pageSensitiveDirectiveModule",
      "shortName": "pageSensitiveDirectiveModule",
      "type": "overview",
      "moduleName": "pageSensitiveDirectiveModule",
      "shortDescription": "This module defines the pageSensitive attribute directive.",
      "keywords": "attribute defines directive module overview pagesensitive pagesensitivedirectivemodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "pageSensitiveDirectiveModule.directive:pageSensitive",
      "shortName": "pageSensitive",
      "type": "directive",
      "moduleName": "pageSensitiveDirectiveModule",
      "shortDescription": "Will cause an Angular re-compilation of the node declaring this directive whenever the page identifier in smartEdit layer changes",
      "keywords": "angular changes declaring directive identifier layer node pagesensitivedirectivemodule re-compilation smartedit"
    },
    {
      "section": "smartEdit",
      "id": "permissionServiceInterfaceModule",
      "shortName": "permissionServiceInterfaceModule",
      "type": "overview",
      "moduleName": "permissionServiceInterfaceModule",
      "shortDescription": "The permissions service resolves user permissions returning true or false.",
      "keywords": "false overview permissions permissionserviceinterfacemodule resolves returning service smartedit true user"
    },
    {
      "section": "smartEdit",
      "id": "permissionServiceInterfaceModule.service:PermissionServiceInterface",
      "shortName": "PermissionServiceInterface",
      "type": "service",
      "moduleName": "permissionServiceInterfaceModule",
      "shortDescription": "The permission service is used to check if a user has been granted certain permissions.",
      "keywords": "accidentally alias aliases appropriate array associated attempting avoid cached caches called calling character check checks clearance clearcache clears combination common configuration configured context convenience data default defined determine empty entry error execute executed fake false function getpermission granted isn ispermitted list logic lookup method names namespace number object objects order overriding parameter permission permissionname permissions permissionserviceinterfacemodule permissionservicemodule prefixed promise property reduces references register registerdefaultrule registered registering registerpermission registerrule registers rejects required reserved resolve resolves responds return returns rule ruleconfiguration rules se service set smartedit spaced structured takes throw thrown true un-registers undefined unregisterdefaultrule user valid verification verified verify"
    },
    {
      "section": "smartEdit",
      "id": "perspectiveInterfaceModule",
      "shortName": "perspectiveInterfaceModule",
      "type": "overview",
      "moduleName": "perspectiveInterfaceModule",
      "keywords": "overview perspectiveinterfacemodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "perspectiveInterfaceModule.service:PerspectiveServiceInterface",
      "shortName": "PerspectiveServiceInterface",
      "type": "service",
      "moduleName": "perspectiveInterfaceModule",
      "keywords": "activated activating active actives allowed application associated bound changed configuration consists cookie crossframeeventservice crossframeeventservicemodule current currently currently-selected data deactivates deactivating default description descriptioni18nkey determine disable disabled disablingcallback enabled enabling enablingcallback event event_perspective_changed exising feature features flag functions hasactiveperspective i18n identified identifies indicates invoked isemptyperspectiveactive key list method methods_register mode namei18nkey names object optional overlay parameter permission permissions permitted perspective perspectiveinterfacemodule perspectives prespective preview published referenced refresh refreshed refreshperspective register registered registers registry replaying represents respective returns se seconstantsmodule selectdefault selected selects service set smartedit smartedit-perspectives stored stores switches switchto system tooltip translated true uniquely user web"
    },
    {
      "section": "smartEdit",
      "id": "previewErrorInterceptorModule.service:previewErrorInterceptor",
      "shortName": "previewErrorInterceptor",
      "type": "service",
      "moduleName": "previewErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 400 from the Preview API when the pageId is not found in the context. The request will be replayed without the pageId.",
      "keywords": "api code context error http pageid preview previewerrorinterceptormodule replayed request service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "previewTicketInterceptorModule.previewTicketInterceptor",
      "shortName": "previewTicketInterceptorModule.previewTicketInterceptor",
      "type": "service",
      "moduleName": "previewTicketInterceptorModule",
      "shortDescription": "A HTTP request interceptor that adds the preview ticket to the HTTP header object before a request is made.",
      "keywords": "$httpprovider adding adds array called config configuration current dependencies extracts factories factory header holds http injected interceptor interceptors method methods modified object preview previewticketinterceptor previewticketinterceptormodule property registered request resource returns service smartedit ticket url x-preview-ticket"
    },
    {
      "section": "smartEdit",
      "id": "recompileDomModule",
      "shortName": "recompileDomModule",
      "type": "overview",
      "moduleName": "recompileDomModule",
      "shortDescription": "This module defines the recompileDom component.",
      "keywords": "component defines directive module overview recompiledom recompiledommodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "recompileDomModule.directive:recompileDom",
      "shortName": "recompileDom",
      "type": "directive",
      "moduleName": "recompileDomModule",
      "shortDescription": "The recompile dom directive accepts a function param, and can be applied to any part of the dom.",
      "keywords": "$timeout accepts angular applied content contents directive dom execution function inner invoked outer param recompile recompiled recompiledom recompiledommodule recompiling scope smartedit transcluded trigger"
    },
    {
      "section": "smartEdit",
      "id": "renderServiceInterfaceModule",
      "shortName": "renderServiceInterfaceModule",
      "type": "overview",
      "moduleName": "renderServiceInterfaceModule",
      "shortDescription": "The renderServiceInterfaceModule",
      "keywords": "abstract accelerator component components data designed displays extensible interface module operation overview performed re-render render renderservice renderserviceinterfacemodule service smartedit update"
    },
    {
      "section": "smartEdit",
      "id": "renderServiceInterfaceModule.service:RenderServiceInterface",
      "shortName": "RenderServiceInterface",
      "type": "service",
      "moduleName": "renderServiceInterfaceModule",
      "shortDescription": "Designed to re-render components after an update component data operation has been performed, according to",
      "keywords": "$compile accelerator backend blocked blockrendering boolean class compilation component componentid components componenttype content correctly current custom customcontent data decorators decoratorservicemodule designed determines displayed displays error executed extended flag frontend html indicates indicator instantiated interface isblocked isrenderingblocked method methods_storeprecompiledcomponent note object operation optional original overlay performed position positioned promise propagate re-render re-rendered re-renders refreshoverlaydimensions reject rejected remove removed removes render rendercomponent rendered rendering renderpage renderremoval renderserviceinterfacemodule renderslots replace requires resets resolve returns saved serves service showoverlay slot slotids slotsids smartedit stack storefront success time toggleoverlay toggles type update updated updates values visibility wrapping"
    },
    {
      "section": "smartEdit",
      "id": "renderServiceModule",
      "shortName": "renderServiceModule",
      "type": "overview",
      "moduleName": "renderServiceModule",
      "shortDescription": "This module provides the renderService, which is responsible for rendering the SmartEdit overlays used for providing",
      "keywords": "cms context functionality module overlays overview providing rendering renderservice renderservicemodule responsible smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "renderServiceModule.renderService",
      "shortName": "renderServiceModule.renderService",
      "type": "service",
      "moduleName": "renderServiceModule",
      "shortDescription": "The renderService is responsible for rendering and resizing component overlays, and re-rendering components and slots",
      "keywords": "_resizeslots appear based body bottom bound bounding calculated children cms comparing component components contextual css default dimensions discover displayed dom dynamic element elements empty encompassing event fetched flag function height implementation inside isvisible manager match menus method minimum original overlap overlay overlays perspective positions provided re-rendering rectangle refresh refreshes refreshoverlaydimensions remain rendering renderservice renderservicemodule resized resizes resizing responsible responsive root search service set size sizes slot slots smartedit specific storefront storefronts sub-components synced toggleoverlay toggles top traverse values visibility window"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CATALOGS_PATH",
      "shortName": "CATALOGS_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the catalogs",
      "keywords": "catalogs object path resourcelocationsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CMSWEBSERVICES_PATH",
      "shortName": "CMSWEBSERVICES_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Regular expression identifying CMS related URIs",
      "keywords": "cms expression identifying object regular resourcelocationsmodule smartedit uris"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CMSWEBSERVICES_RESOURCE_URI",
      "shortName": "CMSWEBSERVICES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant for the cmswebservices API root",
      "keywords": "api cmswebservices constant object resourcelocationsmodule root smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CONFIGURATION_COLLECTION_URI",
      "shortName": "CONFIGURATION_COLLECTION_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "The SmartEdit configuration collection API root",
      "keywords": "api collection configuration object resourcelocationsmodule root smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CONFIGURATION_URI",
      "shortName": "CONFIGURATION_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "the name of the SmartEdit configuration API root",
      "keywords": "api configuration object resourcelocationsmodule root smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG",
      "shortName": "CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog uid placeholder in URLs",
      "keywords": "catalog constant object placeholder resourcelocationsmodule smartedit uid urls"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG_VERSION",
      "shortName": "CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog version placeholder in URLs",
      "keywords": "catalog constant object placeholder resourcelocationsmodule smartedit urls version"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:CONTEXT_SITE_ID",
      "shortName": "CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the site uid placeholder in URLs",
      "keywords": "constant object placeholder resourcelocationsmodule site smartedit uid urls"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:DEFAULT_AUTHENTICATION_CLIENT_ID",
      "shortName": "DEFAULT_AUTHENTICATION_CLIENT_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "The default OAuth 2 client id to use during authentication.",
      "keywords": "authentication client default oauth object resourcelocationsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:DEFAULT_AUTHENTICATION_ENTRY_POINT",
      "shortName": "DEFAULT_AUTHENTICATION_ENTRY_POINT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "When configuration is not available yet to provide authenticationMap, one needs a default authentication entry point to access configuration API itself",
      "keywords": "access api authentication authenticationmap configuration default entry object point provide resourcelocationsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:ENUM_RESOURCE_URI",
      "shortName": "ENUM_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to fetch list of values of a given enum type",
      "keywords": "enum fetch list object path resourcelocationsmodule smartedit type values"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:I18N_LANGUAGE_RESOURCE_URI",
      "shortName": "I18N_LANGUAGE_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to fetch the supported i18n languages.",
      "keywords": "fetch i18n languages object resource resourcelocationsmodule smartedit supported uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:I18N_RESOURCE_URI",
      "shortName": "I18N_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to fetch the i18n initialization map for a given locale.",
      "keywords": "fetch i18n initialization locale map object resource resourcelocationsmodule smartedit uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:LANDING_PAGE_PATH",
      "shortName": "LANDING_PAGE_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the landing page",
      "keywords": "landing object path resourcelocationsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:LANGUAGE_RESOURCE_URI",
      "shortName": "LANGUAGE_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the languages REST service.",
      "keywords": "languages object resource resourcelocationsmodule rest service smartedit uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:MEDIA_PATH",
      "shortName": "MEDIA_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the media",
      "keywords": "media object path resourcelocationsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:MEDIA_RESOURCE_URI",
      "shortName": "MEDIA_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the media REST service.",
      "keywords": "media object resource resourcelocationsmodule rest service smartedit uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG",
      "shortName": "PAGE_CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog uid placeholder in URLs",
      "keywords": "catalog constant current object placeholder resourcelocationsmodule smartedit uid urls"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION",
      "shortName": "PAGE_CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog version placeholder in URLs",
      "keywords": "catalog constant current object placeholder resourcelocationsmodule smartedit urls version"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID",
      "shortName": "PAGE_CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page site uid placeholder in URLs",
      "keywords": "constant current object placeholder resourcelocationsmodule site smartedit uid urls"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PageUriContext",
      "shortName": "PageUriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives for a given page.",
      "keywords": "catalog cms directives map object page_context_catalog page_context_catalog_version page_context_site_id resourcelocationsmodule services site smartedit uid version"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PERMISSIONSWEBSERVICES_RESOURCE_URI",
      "shortName": "PERMISSIONSWEBSERVICES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to fetch permissions of a given type",
      "keywords": "fetch object path permissions resourcelocationsmodule smartedit type"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:PREVIEW_RESOURCE_URI",
      "shortName": "PREVIEW_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the preview ticket API",
      "keywords": "api object path preview resourcelocationsmodule smartedit ticket"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:SITES_RESOURCE_URI",
      "shortName": "SITES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the sites REST service.",
      "keywords": "object resource resourcelocationsmodule rest service sites smartedit uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:SMARTEDIT_RESOURCE_URI_REGEXP",
      "shortName": "SMARTEDIT_RESOURCE_URI_REGEXP",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "to calculate platform domain URI, this regular expression will be used",
      "keywords": "calculate domain expression object platform regular resourcelocationsmodule smartedit uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:SMARTEDIT_ROOT",
      "shortName": "SMARTEDIT_ROOT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "the name of the webapp root context",
      "keywords": "context object resourcelocationsmodule root smartedit webapp"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:STORE_FRONT_CONTEXT",
      "shortName": "STORE_FRONT_CONTEXT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "to fetch the store front context for inflection points.",
      "keywords": "context fetch front inflection object points resourcelocationsmodule smartedit store"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:STOREFRONT_PATH",
      "shortName": "STOREFRONT_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the storefront",
      "keywords": "object path resourcelocationsmodule smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:STOREFRONT_PATH_WITH_PAGE_ID",
      "shortName": "STOREFRONT_PATH_WITH_PAGE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the storefront with a page ID",
      "keywords": "object path resourcelocationsmodule smartedit storefront"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:STRUCTURES_RESOURCE_URI",
      "shortName": "STRUCTURES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component structures REST service.",
      "keywords": "component object resource resourcelocationsmodule rest service smartedit structures uri"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:SYNC_PATH",
      "shortName": "SYNC_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the synchronization service",
      "keywords": "object path resourcelocationsmodule service smartedit synchronization"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.object:UriContext",
      "shortName": "UriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives.",
      "keywords": "catalog cms context_catalog context_catalog_version context_site_id directives map object resourcelocationsmodule services site smartedit uid version"
    },
    {
      "section": "smartEdit",
      "id": "resourceLocationsModule.resourceLocationToRegex",
      "shortName": "resourceLocationsModule.resourceLocationToRegex",
      "type": "service",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Generates a regular expresssion matcher from a given resource location URL, replacing predefined keys by wildcard",
      "keywords": "$httpbackend backend endpoint endpointregex example expresssion generates hits http location match matcher matchers mocked passed predefined regex regular replacing resource resourcelocationsmodule resourcelocationtoregex respond service smartedit someresource somevalue url var whenget wildcard"
    },
    {
      "section": "smartEdit",
      "id": "resourceNotFoundErrorInterceptorModule.service:resourceNotFoundErrorInterceptor",
      "shortName": "resourceNotFoundErrorInterceptor",
      "type": "service",
      "moduleName": "resourceNotFoundErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 404 (Not Found) except for an HTML or a language resource. It will display the response.message in an alert message.",
      "keywords": "alert code display error html http language message resource resourcenotfounderrorinterceptormodule response service smartedit"
    },
    {
      "section": "smartEdit",
      "id": "restServiceFactoryModule",
      "shortName": "restServiceFactoryModule",
      "type": "overview",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "The restServiceFactoryModule",
      "keywords": "factory generate module overview providing resource rest restservicefactorymodule service smartedit url wrapper"
    },
    {
      "section": "smartEdit",
      "id": "restServiceFactoryModule.service:ResourceWrapper",
      "shortName": "ResourceWrapper",
      "type": "service",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "The ResourceWrapper is service used to make REST calls to the wrapped target URI. It is created",
      "keywords": "angularjs appends based call called calls component components created deletes evaluate evaluates factory getbyid https identifier list loads mapped match matching method methods_get object objects org parameters params payload placeholder placeholders promise provided query rejected rejecting rejects remove resolves resolving resource resourcewrapper rest restservicefactory restservicefactorymodule returned returns save saved saves search searchparams server service single smartedit string target unique update updated updates uri verify warpper wrapped wrapper"
    },
    {
      "section": "smartEdit",
      "id": "restServiceFactoryModule.service:restServiceFactory",
      "shortName": "restServiceFactory",
      "type": "service",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "A factory used to generate a REST service wrapper for a given resource URL, providing a means to perform HTTP",
      "keywords": "$resource angularjs appended argument automatically best context-wide create created default delegated domain factory forward generate http https identifier location method multiple object operations opposed optional org parameter paths payload perform placeholder points post prefix prefixed prepended provided providing reference relative remain resource resourceid rest restservicefactory restservicefactorymodule retrieved returned separate service services set setdomain slash smartedit split target uri uris url working wrapped wrapper wraps"
    },
    {
      "section": "smartEdit",
      "id": "retryInterceptorModule",
      "shortName": "retryInterceptorModule",
      "type": "overview",
      "moduleName": "retryInterceptorModule",
      "shortDescription": "This module provides the functionality to retry failing xhr requests through the registration of retry strategies.",
      "keywords": "failing functionality module overview registration requests retry retryinterceptormodule smartedit strategies xhr"
    },
    {
      "section": "smartEdit",
      "id": "retryInterceptorModule.service:retryInterceptor",
      "shortName": "retryInterceptor",
      "type": "service",
      "moduleName": "retryInterceptorModule",
      "shortDescription": "The retryInterceptor provides the functionality to register a set of predicates with their associated retry strategies.",
      "keywords": "angular argument associated attemptcount boolean calculatenextdelay canretry controller custominterceptorexample custompredicate delay determine examplecontroller fails false find firstfastretry function functionality http httpobj instanciated js match matching method module operation_context operationcontext operationcontextservicemodule predicate predicates prototype register remain request response retried retries retry retryinterceptor retryinterceptormodule retrystrategyinterface retrystrategyinterfacemodule return run-time service set smartedit status strategies strategy strategyholder subject takes time tooling true var"
    },
    {
      "section": "smartEdit",
      "id": "RetryStrategyInterface",
      "shortName": "RetryStrategyInterface",
      "type": "overview",
      "moduleName": "RetryStrategyInterface",
      "keywords": "overview retrystrategyinterface smartedit"
    },
    {
      "section": "smartEdit",
      "id": "retryStrategyInterfaceModule.service:RetryStrategyInterface",
      "shortName": "RetryStrategyInterface",
      "type": "service",
      "moduleName": "retryStrategyInterfaceModule",
      "keywords": "canretry current delay function method request retried retry retrystrategyinterfacemodule return returns service smartedit time true"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:ALL_PERSPECTIVE",
      "shortName": "ALL_PERSPECTIVE",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The key of the default All Perspective.",
      "keywords": "default key object perspective seconstantsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_ADDED",
      "shortName": "EVENT_PERSPECTIVE_ADDED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when a new perspective (known as mode for users) is registered.",
      "keywords": "event mode object perspective registered seconstantsmodule smartedit triggered users"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_CHANGED",
      "shortName": "EVENT_PERSPECTIVE_CHANGED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when the perspective (known as mode for users) is changed.",
      "keywords": "changed event mode object perspective seconstantsmodule smartedit triggered users"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_REFRESHED",
      "shortName": "EVENT_PERSPECTIVE_REFRESHED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when the perspective (known as mode for users) is refreshed.",
      "keywords": "event mode object perspective refreshed seconstantsmodule smartedit triggered users"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_UNLOADING",
      "shortName": "EVENT_PERSPECTIVE_UNLOADING",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when a perspective is about to be unloaded.",
      "keywords": "disabled event features object perspective seconstantsmodule smartedit triggered unloaded"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:NONE_PERSPECTIVE",
      "shortName": "NONE_PERSPECTIVE",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The key of the default None Perspective.",
      "keywords": "default key object perspective seconstantsmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "seConstantsModule.object:VALIDATION_MESSAGE_TYPES",
      "shortName": "VALIDATION_MESSAGE_TYPES",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "Validation message types",
      "keywords": "message object seconstantsmodule smartedit types validation"
    },
    {
      "section": "smartEdit",
      "id": "seDropdownModule.directive:seDropdown",
      "shortName": "seDropdown",
      "type": "directive",
      "moduleName": "seDropdownModule",
      "shortDescription": "This directive generates a custom dropdown (standalone or dependent on another one) for the genericEditor.",
      "keywords": "actual angularjs api array associated attribute attributes based boolean call calling changed cmsstructuretype configured contract custom de default defined depend dependent depends dependson described description determine determining directive display dom dropdown dropdownpopulator dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown edited editorfieldmappingservice editorfieldmappingservicemodule element en example exception false fetch field full generated generates genericeditor genericeditormodule i18nkey i18nkeyforsomequalifier1 i18nkeyforsomequalifier2 i18nkeyforsomequalifier3 idattribute identifier implementation item items itemtemplateurl label labelattributes language list localized map maps menu method mode model object opposed option1 option2 option3 options paged parent path populate populated populating populator propertyeditortemplate propertytype qualifier raised recipe rest retrieved retrieving returned se-dropdown sedropdown sedropdownmodule selection service set smartedit somepropertytype somequalifier1 somequalifier2 somequalifier3 string structure structures template uri uridropdownpopulator uridropdownpopulatormodule work"
    },
    {
      "section": "smartEdit",
      "id": "seDropdownModule.service:SEDropdownService",
      "shortName": "SEDropdownService",
      "type": "service",
      "moduleName": "seDropdownModule",
      "shortDescription": "The SEDropdownService handles the initialization and the rendering of the seDropdown Angular component.",
      "keywords": "angular array asynchronous attributes based component configured currently currentpage directive dropdown dropdownpopulatorinterface dropdownpopulatorinterfacemodule event fetch fetchall fetched fetchentity fetchpage field filter getitem handles implementation init initialization initializes instantiating item items list method number object option options pagesize paging populate populated populator promise publishes rendering resolves returned search sedropdown sedropdownmodule sedropdownservice selected service single smartedit triggeraction"
    },
    {
      "section": "smartEdit",
      "id": "seGenericEditorFieldErrorsModule",
      "shortName": "seGenericEditorFieldErrorsModule",
      "type": "overview",
      "moduleName": "seGenericEditorFieldErrorsModule",
      "shortDescription": "This module provides the seGenericEditorFieldErrors component, which is used to show validation errors.",
      "keywords": "component errors module overview segenericeditorfielderrors segenericeditorfielderrorsmodule smartedit validation"
    },
    {
      "section": "smartEdit",
      "id": "seGenericEditorFieldMessagesModule",
      "shortName": "seGenericEditorFieldMessagesModule",
      "type": "overview",
      "moduleName": "seGenericEditorFieldMessagesModule",
      "shortDescription": "This module provides the seGenericEditorFieldMessages component, which is used to show validation messages like errors or warnings.",
      "keywords": "component errors messages module overview segenericeditorfieldmessages segenericeditorfieldmessagesmodule smartedit validation warnings"
    },
    {
      "section": "smartEdit",
      "id": "seGenericEditorFieldMessagesModule.component:seGenericEditorFieldMessages",
      "shortName": "seGenericEditorFieldMessages",
      "type": "directive",
      "moduleName": "seGenericEditorFieldMessagesModule",
      "shortDescription": "Component responsible for displaying validation messages like errors or warnings",
      "keywords": "actual array code component directive displaying errors field iso language localized messages non-localized object qualifier responsible se-generic-editor-field-messages segenericeditorfieldmessagesmodule smartedit string validation warnings"
    },
    {
      "section": "smartEdit",
      "id": "seValidationErrorParserModule",
      "shortName": "seValidationErrorParserModule",
      "type": "overview",
      "moduleName": "seValidationErrorParserModule",
      "shortDescription": "This module provides the validationErrorsParser service, which is used to parse validation errors for parameters",
      "keywords": "errors format language message module overview parameters parse service sevalidationerrorparsermodule smartedit validation validationerrorsparser"
    },
    {
      "section": "smartEdit",
      "id": "seValidationErrorParserModule.seValidationErrorParser",
      "shortName": "seValidationErrorParserModule.seValidationErrorParser",
      "type": "service",
      "moduleName": "seValidationErrorParserModule",
      "shortDescription": "This service provides the functionality to parse validation errors received from the backend.",
      "keywords": "backend details en error errors expects extra final format function functionality language message method object occurred parse parses received service sevalidationerrorparser sevalidationerrorparsermodule smartedit somekey someval stripped validation var widescreen"
    },
    {
      "section": "smartEdit",
      "id": "seValidationMessageParserModule",
      "shortName": "seValidationMessageParserModule",
      "type": "overview",
      "moduleName": "seValidationMessageParserModule",
      "shortDescription": "This module provides the seValidationMessageParser service, which is used to parse validation messages (errors, warnings)",
      "keywords": "format language message messages module overview parameters parse service sevalidationmessageparser sevalidationmessageparsermodule smartedit validation warnings"
    },
    {
      "section": "smartEdit",
      "id": "seValidationMessageParserModule.seValidationMessageParser",
      "shortName": "seValidationMessageParserModule.seValidationMessageParser",
      "type": "service",
      "moduleName": "seValidationMessageParserModule",
      "shortDescription": "This service provides the functionality to parse validation messages (errors, warnings) received from the backend.",
      "keywords": "backend details en expects extra final format function functionality language message messages method object occurred parse parses received service sevalidationmessageparser sevalidationmessageparsermodule smartedit somekey someval stripped validation var warning warnings widescreen"
    },
    {
      "section": "smartEdit",
      "id": "sharedDataServiceInterfaceModule.SharedDataServiceInterface",
      "shortName": "sharedDataServiceInterfaceModule.SharedDataServiceInterface",
      "type": "service",
      "moduleName": "sharedDataServiceInterfaceModule",
      "shortDescription": "Provides an abstract extensible shared data service. Used to store any data to be used either the SmartEdit",
      "keywords": "abstract application callback class container content convenience data extended extensible fed fetch fly instantiated interface key method modify modifyingcallback object retrieve return serves service set shared shareddataserviceinterface shareddataserviceinterfacemodule smartedit store stored update"
    },
    {
      "section": "smartEdit",
      "id": "sharedDataServiceModule",
      "shortName": "sharedDataServiceModule",
      "type": "overview",
      "moduleName": "sharedDataServiceModule",
      "shortDescription": "The sharedDataServiceModule",
      "keywords": "application container data key module overview retrieve service shared shareddataservicemodule smartedit store"
    },
    {
      "section": "smartEdit",
      "id": "sharedDataServiceModule.sharedDataService",
      "shortName": "sharedDataServiceModule.sharedDataService",
      "type": "service",
      "moduleName": "sharedDataServiceModule",
      "shortDescription": "The Shared Data Service is used to store data that is to be shared between the SmartEdit application and the",
      "keywords": "application container data extends gateway gatewayproxy gatewayproxymodule service share shared shareddata shareddataservice shareddataserviceinterface shareddataserviceinterfacemodule shareddataservicemodule smartedit store"
    },
    {
      "section": "smartEdit",
      "id": "simpleRetrylModule",
      "shortName": "simpleRetrylModule",
      "type": "overview",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "This module provides the simpleRetry service.",
      "keywords": "module overview service simpleretry simpleretrylmodule smartedit"
    },
    {
      "section": "smartEdit",
      "id": "simpleRetrylModule.object:SIMPLE_RETRY_DEFAULT_SETTING",
      "shortName": "SIMPLE_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default object retry setting simpleretrylmodule smartedit values"
    },
    {
      "section": "smartEdit",
      "id": "simpleRetrylModule.service:simpleRetry",
      "shortName": "simpleRetry",
      "type": "service",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide a simple fixed delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts base calculate calculatenextdelay canretry check current delay false fixed functionality interval maxattempt maximum method minbackoff minimum number perform provide request retries retry retryinterval returns service simple simpleretrylmodule smartedit strategy time true valid"
    },
    {
      "section": "smartEdit",
      "id": "sliderPanelModule",
      "shortName": "sliderPanelModule",
      "type": "overview",
      "moduleName": "sliderPanelModule",
      "shortDescription": "This module defines the slider panel Angular component and its associated constants and controller.",
      "keywords": "$ctrl add advanced angular applied associated automatically basic btn btn-default calling class component configuration configurations constants content controller data-slider-panel-configuration data-slider-panel-show default define defined defines definition dependency directive display embed example expected function getnewserviceinstance html implementation include instantiate instantiated json merge method methods_getnewserviceinstance modifications module ng-click nganimate object overview overwrite panel panels provide service set settings showsliderpanel slider sliderpanelconfiguration sliderpanelmodule sliderpanelservicemodule sliderpanelshow smartedit specific tag template trigger type update visible yourapp yourcontroller ysliderpanel"
    },
    {
      "section": "smartEdit",
      "id": "sliderPanelModule.directive:ySliderPanel",
      "shortName": "ySliderPanel",
      "type": "directive",
      "moduleName": "sliderPanelModule",
      "shortDescription": "The ySliderPanel Angular component allows for the dynamic display of any HTML content on a sliding panel.",
      "keywords": "allows angular applied binding component configuration content controller datasliderpanelconfiguration datasliderpanelhide datasliderpanelshow directive display dynamic function hiding html json main object panel shared slider sliderpanelmodule sliding smartedit trigger ways ysliderpanel"
    },
    {
      "section": "smartEdit",
      "id": "sliderPanelModule.object:CSS_CLASSNAMES",
      "shortName": "CSS_CLASSNAMES",
      "type": "object",
      "moduleName": "sliderPanelModule",
      "shortDescription": "This object defines injectable Angular constants that store the CSS class names used in the controller to define the",
      "keywords": "action angular animation applied class common constants container content controller css define defines injectable names object panel prefix property rendered rendering slide slider sliderpanel_animated sliderpanel_slideprefix sliderpanelmodule sliding smartedit store trigger"
    },
    {
      "section": "smartEdit",
      "id": "sliderPanelServiceModule",
      "shortName": "sliderPanelServiceModule",
      "type": "overview",
      "moduleName": "sliderPanelServiceModule",
      "shortDescription": "This module provides a service to initialize and control the rendering of the ySliderPanel Angular component.",
      "keywords": "angular component control directive initialize module overview rendering service sliderpanelmodule sliderpanelservicemodule smartedit ysliderpanel"
    },
    {
      "section": "smartEdit",
      "id": "sliderPanelServiceModule.service:sliderPanelService",
      "shortName": "sliderPanelService",
      "type": "service",
      "moduleName": "sliderPanelServiceModule",
      "shortDescription": "The sliderPanelService handles the initialization and the rendering of the ySliderPanel Angular component.",
      "keywords": "angular applied cancel component configuration container covered css cssselector default dimension directive disabled dismiss displayed displayedbydefault element framework getnewserviceinstance greyed-out handles html indicate indicates initialization inline instance javascript json label method modal nogreyedoutoverlay object onclick overlay overlaydimension panel parent pattern pointer position provided rendering renders resize returns save screen screenresized select service set sets showdismissbutton side slidefrom slider sliderpanelmodule sliderpanelservice sliderpanelservicemodule slides smartedit specific specifies styling tag title triggered update updatecontainerinlinestyling values window ysliderpanel z-index zindex"
    },
    {
      "section": "smartEdit",
      "id": "storageServiceModule",
      "shortName": "storageServiceModule",
      "type": "overview",
      "moduleName": "storageServiceModule",
      "shortDescription": "The storageServiceModule",
      "keywords": "allows browser module overview service smartedit storage storageservicemodule storing temporary"
    },
    {
      "section": "smartEdit",
      "id": "storageServiceModule.service:storageService",
      "shortName": "storageService",
      "type": "service",
      "moduleName": "storageServiceModule",
      "shortDescription": "The Storage service is used to store temporary information in the browser. The service keeps track of key/value pairs",
      "keywords": "associated associates authenticate authenticated authentication authtoken authtokens authuri browser cookie creates current determine entry getauthtoken getprincipalidentifier getvaluefromcookie identified identifies indicates initialized isinitialized key login method pairs principal principalnamevalue principaluid properly provided remove removeallauthtokens removeauthtoken removed removeprincipalidentifier removes resource retrieve retrieved retrieves service smartedit smartedit-sessions storage storageservicemodule store storeauthtoken stored storeprincipalidentifier stores temporary token track uid uri uris user"
    },
    {
      "section": "smartEdit",
      "id": "tabsetModule",
      "shortName": "tabsetModule",
      "type": "overview",
      "moduleName": "tabsetModule",
      "shortDescription": "The Tabset module provides the directives required to display a group of tabsets within a tabset. The",
      "keywords": "developers directive directives display displaying group interest module organizing overview required responsible smartedit tabs tabset tabsetmodule tabsets ytabset"
    },
    {
      "section": "smartEdit",
      "id": "tabsetModule.directive:yTab",
      "shortName": "yTab",
      "type": "directive",
      "moduleName": "tabsetModule",
      "shortDescription": "The directive  responsible for wrapping the content of a tab within a",
      "keywords": "allows called caller content contents custom data determine directive displayed executed extra fragment function functionality html match model modify object optional parameter parse path register responsible scope smartedit smartedit-tab smartedit-tabset tab tabcontrol tabid tabs tabset tabsetmodule track wrapping ytabset"
    },
    {
      "section": "smartEdit",
      "id": "tabsetModule.directive:yTabset",
      "shortName": "yTabset",
      "type": "directive",
      "moduleName": "tabsetModule",
      "shortDescription": "The directive responsible for displaying and organizing tabs within a tabset. A specified number of tabs will",
      "keywords": "allows body called caller changes child clicks configuration content contents custom data defined determine directive display displayed displaying drop-down error executed extra flag fragment function functionality grouped haserrors header headers html indicates item list maximum menu model modify note number numtabsdisplayed object optional organizing parameter parse passed path register remaining responsible scope selected smartedit smartedit-tab smartedit-tabset tab tabcontrol tabs tabset tabsetmodule tabslist templateurl title track user visual wrapped ytab"
    },
    {
      "section": "smartEdit",
      "id": "timerModule",
      "shortName": "timerModule",
      "type": "overview",
      "moduleName": "timerModule",
      "shortDescription": "A module that provides a Timer object that can invoke a callback after a certain period of time.",
      "keywords": "callback invoke module object overview period service smartedit time timer timermodule"
    },
    {
      "section": "smartEdit",
      "id": "timerModule.service:Timer",
      "shortName": "Timer",
      "type": "service",
      "moduleName": "timerModule",
      "shortDescription": "A Timer must be instanciated calling timerService.createTimer().",
      "keywords": "$timeout active additional adds angular angularjs callback calling checking clean createtimer current currently dirty duration false function functions https instanciated instruct internal invoke invoked isactive method milliseconds model number object order org parameter prevent references resetduration restart returns running service set sets skip smartedit start starts stops teardown thise timeout timer timermodule timerservice true wait wraps"
    },
    {
      "section": "smartEdit",
      "id": "toolbarInterfaceModule.ToolbarServiceInterface",
      "shortName": "toolbarInterfaceModule.ToolbarServiceInterface",
      "type": "service",
      "moduleName": "toolbarInterfaceModule",
      "shortDescription": "Provides an abstract extensible toolbar service. Used to manage and perform actions to either the SmartEdit",
      "keywords": "abstract action actions additems application array callback class classes clicked close container content default description descriptioni18nkey determines display dom dropdown extended extensible fonts higher html hybrid_action icon iconclassname icons identifier image images include included instantiated interface internal internally item keepaliveonclose key list lower manage maps method middle named namei18nkey number perform position priority properties ranging sections serves service smartedit takes template toolbar toolbarinterfacemodule toolbarserviceinterface translation trigger triggered type unique url urls variable"
    },
    {
      "section": "smartEdit",
      "id": "toolbarModule.ToolbarService",
      "shortName": "toolbarModule.ToolbarService",
      "type": "service",
      "moduleName": "toolbarModule",
      "shortDescription": "The inner toolbar service is used to add toolbar actions that affect the inner application, publish aliases to",
      "keywords": "actions add additems affect aliases application callbacks communication cross function gateway gatewayid gatewayproxy gatewayproxymodule gettoolbarservice identifier iframe inner instance key manage managed methods methods_additems outer private provided proxy publish returns service single singleton smartedit store toolbar toolbar-name toolbarinterfacemodule toolbarmodule toolbarservice toolbarservicefactory toolbarserviceinterface"
    },
    {
      "section": "smartEdit",
      "id": "toolbarModule.toolbarServiceFactory",
      "shortName": "toolbarModule.toolbarServiceFactory",
      "type": "service",
      "moduleName": "toolbarModule",
      "shortDescription": "The toolbar service factory generates instances of the ToolbarService based on",
      "keywords": "based cached communication corresponding created cross exist exists factory gateway gatewayid gatewayproxy gatewayproxymodule generates gettoolbarservice identifier iframe instance instances method provided respect returns service single singleton smartedit toolbar toolbarmodule toolbarservice toolbarservicefactory"
    },
    {
      "section": "smartEdit",
      "id": "translationServiceModule",
      "shortName": "translationServiceModule",
      "type": "service",
      "moduleName": "translationServiceModule",
      "shortDescription": "This module is used to configure the translate service, the filter, and the directives from the &#39;pascalprecht.translate&#39; package. The configuration consists of:",
      "keywords": "appropriate browser combined configuration configure consists constant directives filter getbrowserlocale i18napiroot i18ninterceptor i18ninterceptormodule initializing languageservice languageservicemodule locale map methods_getbrowserlocale methods_request module object package pascalprecht preferredlanguage replace request retrieved runtime service setting smartedit time translate translation translationservicemodule unaccessible undefined_locale uri"
    },
    {
      "section": "smartEdit",
      "id": "treeModule",
      "shortName": "treeModule",
      "type": "overview",
      "moduleName": "treeModule",
      "shortDescription": "This module deals with rendering and management of node trees",
      "keywords": "deals management module node overview rendering smartedit treemodule trees"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.controller:YTreeController",
      "shortName": "YTreeController",
      "type": "controller",
      "moduleName": "treeModule",
      "shortDescription": "Extensible controller of the ytree directive",
      "keywords": "add affect angular-ui-tree boolean causing checking child children collapse collapseall controller determine directive existing expand expandable expanded expanding extensible fetch fetched getnodebyid handle haschildren https identifier initiated method methods_removenode methods_savenode native newchild newsibling node nodes nodeuid object parent referenced refresh refreshparent remove removed removenode return savenode server service sibling smartedit status successful time toggle toggleandfetch tree treemodule treeservice ytree"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.directive:ytree",
      "shortName": "ytree",
      "type": "directive",
      "moduleName": "treeModule",
      "shortDescription": "This directive renders a tree of nodes and manages CRUD operations around the nodes.",
      "keywords": "action actions angular-ui-tree arg1 arg2 behaviour callback closure closure-bound controller crud ctrl customize data-ng-click data-node-actions data-node-template-url data-node-uri defined delete directive drag dragoptions drop enhance entry example expecting exposing function functions hereunder html https included instance invoke library manage manages map methods mymethod newchild node nodeactions nodes nodetemplateurl nodeuri object operations optional order parameter parent party passed point post prebound relies rendering renders rest root scope service smartedit template third tree treemodule treeservice ytree ytreecontroller ytreedndevent"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.object:dragOptions",
      "shortName": "dragOptions",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A JSON object holding callbacks related to nodes drag and drop functionality in the ytree directive.",
      "keywords": "acceptdropcallback allow allows beforedropcallback block callback callbacks confirmation directive drag drop droppable dropped executed exposes false function functionality holding hovering json key modal node nodes object ondropcallback opens property rejects return slots smartedit treemodule true ytree ytreedndevent"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.object:Node",
      "shortName": "Node",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A plain JSON object, representing the node of a tree managed by the ytree directive.",
      "keywords": "boolean catalog children directive haschildren identifier ignored json localized managed node object optional parent parentuid plain posting property read representing required retrieved saving smartedit tree treemodule uid unique ytree"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.object:yTreeDndEvent",
      "shortName": "yTreeDndEvent",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A plain JSON object, representing the event triggered when dragging and dropping nodes in the ytree directive.",
      "keywords": "array children destination destinationnodes directive dragged dragging drop dropped dropping element event handle json location node nodes object parent plain position property representing set siblings smartedit source sourcenode sourceparenthandle targeted targetparenthandle treemodule triggered ui ytree"
    },
    {
      "section": "smartEdit",
      "id": "treeModule.service:TreeService",
      "shortName": "TreeService",
      "type": "service",
      "moduleName": "treeModule",
      "shortDescription": "A class to manage tree nodes through a REST API.",
      "keywords": "api assigned calls child children class create delete entry false fetch fetchchildren final front generated handle haschildren hit initiated list manage manytoone marked method model navigationnodes node node2 node4 nodes nodeuri object parent parentuid payload point post posting property querying re-evaluated reference removenode rest retrieved return returns root save savenode sending server service side smartedit subsequent support takes tree treemodule treeservice true uid verbs wrapped"
    },
    {
      "section": "smartEdit",
      "id": "unauthorizedErrorInterceptorModule.service:unauthorizedErrorInterceptor",
      "shortName": "unauthorizedErrorInterceptor",
      "type": "service",
      "moduleName": "unauthorizedErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 401 (Forbidden). It will display the login modal.",
      "keywords": "code display error http login modal service smartedit unauthorizederrorinterceptormodule"
    },
    {
      "section": "smartEdit",
      "id": "uriDropdownPopulatorModule.service:uriDropdownPopulator",
      "shortName": "uriDropdownPopulator",
      "type": "service",
      "moduleName": "uriDropdownPopulatorModule",
      "shortDescription": "implementation of DropdownPopulatorInterface for &quot;EditableDropdown&quot; cmsStructureType",
      "keywords": "attribute attributes building call cmsstructuretype comma data defined dependson dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown fetch fetchall fetched fetchpage field getitem idattribute implementation include item label labelattributes list method model object option options params payload promise query request resolves rest separated service setting smartedit uri uridropdownpopulatormodule"
    },
    {
      "section": "smartEdit",
      "id": "urlServiceInterfaceModule.UrlServiceInterface",
      "shortName": "urlServiceInterfaceModule.UrlServiceInterface",
      "type": "service",
      "moduleName": "urlServiceInterfaceModule",
      "shortDescription": "Provides an abstract extensible url service, Used to open a given URL",
      "keywords": "abstract authentication browser class extended extensible instantiated interface invocation method navigate navigates open opens openurlinpopup path pop serves service smartedit tab url urlserviceinterface urlserviceinterfacemodule"
    },
    {
      "section": "smartEdit",
      "id": "urlServiceModule",
      "shortName": "urlServiceModule",
      "type": "overview",
      "moduleName": "urlServiceModule",
      "shortDescription": "The urlServiceModule",
      "keywords": "browser container functionality module open overview providing service smartedit url urlservicemodule"
    },
    {
      "section": "smartEdit",
      "id": "wizardServiceModule",
      "shortName": "wizardServiceModule",
      "type": "overview",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The wizardServiceModule",
      "keywords": "add angular called controller create creating dependencies dosomething function getwizardconfig html i18n implement inject methods_open modal modalwizard modalwizardconfig module mymodule myservice mywizardcontroller object open overview passing return returns service services simple smartedit step1 step2 steps templateurl title wizard wizardmanager wizardservicemodule"
    },
    {
      "section": "smartEdit",
      "id": "wizardServiceModule.modalWizard",
      "shortName": "wizardServiceModule.modalWizard",
      "type": "service",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The modalWizard service is used to create wizards that are embedded into the modalService",
      "keywords": "accessible alternate angular angularjs boilerplate cancelled care conf configuration controller controlleras create default embedded feel finished function https implement initialize injected manipulation manual map method modal modalservice modalservicemodule modalwizard modalwizardconfig navigation object open org promise properties rejected resolved returns service simple smartedit step templates underlying wizard wizardcontroller wizardmanager wizards wizardservicemodule"
    },
    {
      "section": "smartEdit",
      "id": "wizardServiceModule.object:ModalWizardConfig",
      "shortName": "ModalWizardConfig",
      "type": "object",
      "moduleName": "wizardServiceModule",
      "shortDescription": "A plain JSON object, representing the configuration options for a modal wizard",
      "keywords": "action array backlabel boolean button callback caller cancel cancellabel close closed configuration continue current default defaults defined donelabel easy enable enabled fired function halt i18n isformvalid json key label load methods_open modal modalwizard nextlabel null object oncancel ondone onnext open opportunity optional options ordered override parameter parameters pass plain promise property receives rejecting representing resolved result resultfn return returned returning returns single smartedit step steps stopping triggered true wizard wizardservicemodule wizardstepconfig"
    },
    {
      "section": "smartEdit",
      "id": "wizardServiceModule.object:WizardStepConfig",
      "shortName": "WizardStepConfig",
      "type": "object",
      "moduleName": "wizardServiceModule",
      "shortDescription": "A plain JSON object, representing the configuration options for a single step in a wizard",
      "keywords": "automatically callback choose configuration displayed easier explicitly generated html i18n identify json key making meaning menu navigation object optional options plain property provide provided reference representing service single smartedit step template templateurl title top triggered unique url wizard wizardservicemodule"
    },
    {
      "section": "smartEdit",
      "id": "wizardServiceModule.WizardManager",
      "shortName": "wizardServiceModule.WizardManager",
      "type": "service",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The Wizard Manager is a wizard management service that can be injected into your wizard controller.",
      "keywords": "additional adds addstep array automatically cancel close containsstep controller controllers creator currently default displayed equal error exists form function generated getcurrentstep getcurrentstepid getcurrentstepindex getstepindexfromid getsteps getstepscount getstepwithid getstepwithindex getwizardconfig gotostepwithid gotostepwithindex injected management manager method methods_getsteps modalwizardconfig navigates newstep note number object passed position promise provided rejected remove removestepbyid removestepbyindex removing resolved result resultfn return returned runtime service size smartedit step steps true wizard wizardmanager wizardservicemodule wizardstepconfig"
    },
    {
      "section": "smartEdit",
      "id": "yActionableSearchItemModule",
      "shortName": "yActionableSearchItemModule",
      "type": "overview",
      "moduleName": "yActionableSearchItemModule",
      "shortDescription": "This module defines the yActionableSearchItem component",
      "keywords": "component defines directive eventservicemodule module overview smartedit yactionablesearchitem yactionablesearchitemmodule"
    },
    {
      "section": "smartEdit",
      "id": "yActionableSearchItemModule.directive:yActionableSearchItem",
      "shortName": "yActionableSearchItem",
      "type": "directive",
      "moduleName": "yActionableSearchItemModule",
      "shortDescription": "The yActionableSearchItem Angular component is designed to work with the ySelect drop down. It allows you to add",
      "keywords": "action add allows angular aread button component create designed directive drop drop-down event i18n key label pressed resultsheader se smartedit systemeventservice trigger triggered user-defined work yactionablesearchitem yactionablesearchitem_action_create yactionablesearchitemmodule yationablesearchitem yselect"
    },
    {
      "section": "smartEdit",
      "id": "yCollapsibleContainerModule",
      "shortName": "yCollapsibleContainerModule",
      "type": "overview",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "This module defines the collapsible container Angular component and its associated constants and controller.",
      "keywords": "$yourctrl add angular applied associated basic binded bootstrap collapsible component configuration configurations constants container content controller customize data-configuration define defines dependency directive embed function html implementation include modifications module overview panel passed rendering settings sliderpanelmodule smartedit tag template title ui variable ycollapsiblecontainer ycollapsiblecontainermodule ylodashmodule yourapp yourcontroller"
    },
    {
      "section": "smartEdit",
      "id": "yCollapsibleContainerModule.directive:yCollapsibleContainer",
      "shortName": "yCollapsibleContainer",
      "type": "directive",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "The yCollapsibleContainer Angular component allows for the dynamic display of any HTML content on a collapsible container.",
      "keywords": "_right_ allows angular applied collapsible component configuration container content default directive display displayed dynamic expand-collapse expanded expandedbydefault header html icon iconalignment iconvisible json object rendered smartedit specifies ycollapsiblecontainer ycollapsiblecontainermodule"
    },
    {
      "section": "smartEdit",
      "id": "yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS",
      "shortName": "COLLAPSIBLE_CONTAINER_CONSTANTS",
      "type": "object",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "This object defines injectable Angular constants that store the default configuration and CSS class names used in the controller to define the rendering and animation of the collapsible container.",
      "keywords": "allowing angular animation applied class classname collapsible configuration constants container controller css css-based default default_configuration define defines defining display displayed expand-collapse expanded expandedbydefault header icon icon_left icon_right iconalignment iconvisible injectable json left names object positioned property rendered rendering smartedit specifies store ycollapsiblecontainermodule"
    },
    {
      "section": "smartEdit",
      "id": "yDropDownMenuModule.directive:yDropDownMenu",
      "shortName": "yDropDownMenu",
      "type": "directive",
      "moduleName": "yDropDownMenuModule",
      "shortDescription": "yDropDownMenu builds a drop-down menu. It has two parameters dropdownItems and selectedItem. The dropdownItems is an array of object which contains an key, condition and callback function. ",
      "keywords": "alert argument array associated bind builds callback called click condition defined directive drop drop-down dropdown dropdownitems edit example false function hide implemented item key menu object open pageeditormodalservice pagelist parameters passed reloadupdatedpage response return returns selecteditem smartedit sync true uid user ydropdownmenu ydropdownmenumodule"
    },
    {
      "section": "smartEdit",
      "id": "yEditableListModule",
      "shortName": "yEditableListModule",
      "type": "overview",
      "moduleName": "yEditableListModule",
      "shortDescription": "The yEditableList module contains a component which allows displaying a list of elements. The items in ",
      "keywords": "allows component displaying elements items list module overview re-ordered removed smartedit yeditablelist yeditablelistmodule"
    },
    {
      "section": "smartEdit",
      "id": "yEditableListModule.directive:yEditableList",
      "shortName": "yEditableList",
      "type": "directive",
      "moduleName": "yEditableListModule",
      "shortDescription": "The yEditableList component allows displaying a list of items. The list can be managed dynamically, by ",
      "keywords": "adding allows called changes collection component content directive display displaying dynamically editable function identify items itemtemplateurl list managed modified onchange path property re-ordering refresh removing smartedit specifies string template track update y-editable-list yeditablelist yeditablelistmodule"
    },
    {
      "section": "smartEdit",
      "id": "yHelpModule.component:yHelp",
      "shortName": "yHelp",
      "type": "directive",
      "moduleName": "yHelpModule",
      "shortDescription": "This component will generate a help button that will show a customizable popover on top of it when hovering.",
      "keywords": "automatically body button component customizable defined directive exactly generate help hovering html location optional popover relies smartedit template templateurl title top trusted y-help yhelpmodule ypopover ypopovermodule"
    },
    {
      "section": "smartEdit",
      "id": "yInfiniteScrollingModule",
      "shortName": "yInfiniteScrollingModule",
      "type": "overview",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "This module holds the base web component to perform infinite scrolling from paged backend",
      "keywords": "backend base component holds infinite module overview paged perform scrolling smartedit web yinfinitescrollingmodule"
    },
    {
      "section": "smartEdit",
      "id": "yInfiniteScrollingModule.directive:yInfiniteScrolling",
      "shortName": "yInfiniteScrolling",
      "type": "directive",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "A component that you can use to implement infinite scrolling for an expanding content (typically with a ng-repeat) nested in it.",
      "keywords": "approaches arguments attached backend bottom call change class close component container content context crosses css currentpage data decide default defaults determined directive distance dropdown dropdownclass dropdowncontainerclass element evaluated example expanding expected expression failure fetch fetched fetching fetchpage fill filters free function handle height implement implementers infinite invoked items large left listens mask max-height maximum meant measured multiples mycontext nested nextpage ng-repeat number object optional overflow overflow-y override pagesize paginated pagination pixels push query re-fetch reach representing requested requests reset resolved restrict return scroll scrolling search server set size smartedit space starts string tall times triggered type typically yinfinitescrollingmodule"
    },
    {
      "section": "smartEdit",
      "id": "yInfiniteScrollingModule.object:THROTTLE_MILLISECONDS",
      "shortName": "THROTTLE_MILLISECONDS",
      "type": "object",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "Configures the yInfiniteScrolling directive to throttle the page fetching with the value provided in milliseconds.",
      "keywords": "configures directive fetching milliseconds object provided smartedit throttle yinfinitescrolling yinfinitescrollingmodule"
    },
    {
      "section": "smartEdit",
      "id": "yjqueryModule",
      "shortName": "yjqueryModule",
      "type": "overview",
      "moduleName": "yjqueryModule",
      "shortDescription": "This module manages the use of the jQuery library in SmartEdit.",
      "keywords": "enables jquery library manages module noconflict overview smartedit storefront version work yjquerymodule"
    },
    {
      "section": "smartEdit",
      "id": "yjqueryModule.yjQuery",
      "shortName": "yjqueryModule.yjQuery",
      "type": "object",
      "moduleName": "yjqueryModule",
      "shortDescription": "Expose a jQuery wrapping factory all the while preserving potentially pre-existing jQuery in storefront and smartEditContainer",
      "keywords": "expose factory jquery object pre-existing preserving smartedit smarteditcontainer storefront wrapping yjquery yjquerymodule"
    },
    {
      "section": "smartEdit",
      "id": "yLoDashModule",
      "shortName": "yLoDashModule",
      "type": "overview",
      "moduleName": "yLoDashModule",
      "shortDescription": "This module manages the use of the lodash library in SmartEdit. It makes sure the library is introduced",
      "keywords": "angular easy introduced library lifecycle lodash manages mock module overview smartedit tests unit ylodashmodule"
    },
    {
      "section": "smartEdit",
      "id": "yLoDashModule.lodash",
      "shortName": "yLoDashModule.lodash",
      "type": "object",
      "moduleName": "yLoDashModule",
      "shortDescription": "Makes the underscore library available to SmartEdit.",
      "keywords": "clash dependency enforce injection libraries library lodash namespace note object order original proper removed smartedit storefront underscore window ylodashmodule"
    },
    {
      "section": "smartEdit",
      "id": "yMessageModule",
      "shortName": "yMessageModule",
      "type": "overview",
      "moduleName": "yMessageModule",
      "shortDescription": "This module provides the yMessage component, which is responsible for rendering contextual",
      "keywords": "actions component contextual feedback messages module overview rendering responsible smartedit user ymessage ymessagemodule"
    },
    {
      "section": "smartEdit",
      "id": "yMessageModule.component:yMessage",
      "shortName": "yMessage",
      "type": "directive",
      "moduleName": "yMessageModule",
      "shortDescription": "This component provides contextual feedback messages for the user actions. To provide title and description for the yMessage",
      "keywords": "actions component contextual default description directive elements feedback info message-description message-title messageid messages provide smartedit success title transcluded type user warning ymessage ymessagemodule"
    },
    {
      "section": "smartEdit",
      "id": "yPopoverModule.directive:yPopover",
      "shortName": "yPopover",
      "type": "directive",
      "moduleName": "yPopoverModule",
      "shortDescription": "This directive attaches a customizable popover on a DOM element.",
      "keywords": "attaches automatically body bottom bottom-right click concatenation customizable default defined directive dom element event exactly format html left location optional outsideclick placement placement1-placement2 popover possibles smartedit target template templateurl title top trigger trusted type values ypopovermodule"
    },
    {
      "section": "smartEdit",
      "id": "yPopupOverlayModule",
      "shortName": "yPopupOverlayModule",
      "type": "overview",
      "moduleName": "yPopupOverlayModule",
      "shortDescription": "This module provides the yPopupOverlay directive, and it&#39;s helper services",
      "keywords": "directive helper module overview services smartedit ypopupoverlay ypopupoverlaymodule"
    },
    {
      "section": "smartEdit",
      "id": "yPopupOverlayModule.directive:yPopupOverlay",
      "shortName": "yPopupOverlay",
      "type": "directive",
      "moduleName": "yPopupOverlayModule",
      "shortDescription": "The yPopupOverlay is meant to be a directive that allows popups/overlays to be displayed attached to any element.",
      "keywords": "accepts aligns allows anchor angular appends applied attached body bottom called click clicked configuration contained controls correctly default depending directive displayed dom element executed expression false file halign handle hidden horizontally html implementation initial inner left limitation meant object overlay popup popups positioned positions provided relative scrollable scrolling smartedit string template templateurl top true url valign values vertically window work ypopupoverlay ypopupoverlaymodule ypopupoverlayonhide ypopupoverlayonshow ypopupoverlaytrigger"
    },
    {
      "section": "smartEdit",
      "id": "yPopupOverlayUtilsModule",
      "shortName": "yPopupOverlayUtilsModule",
      "type": "overview",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "This module provides utility services for the yPopupOverlayModule",
      "keywords": "module overview services smartedit utility ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEdit",
      "id": "yPopupOverlayUtilsModule.service:yPopupOverlayUtilsClickOrderService",
      "shortName": "yPopupOverlayUtilsClickOrderService",
      "type": "service",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "A service that manages the click handlers for all yPopupOverlay overlay DOM elements.",
      "keywords": "case click clicks close delegates directive displayed dom elements execute handlers manages multiple order overlay overlays popup registered reverse service smartedit stack top user ypopupoverlay ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEdit",
      "id": "yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations",
      "shortName": "yPopupOverlayUtilsDOMCalculations",
      "type": "service",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "Contains some yPopupOverlay helper functions for",
      "keywords": "absolutely absposition adjusthorizontaltobeinviewport alignment anchor anchorboundingclientrect based bottom bounding calculatepreferredposition calculates calculating directive dom element functions height helper horizontal horizontally input left location method modifies modify object overlay position positioned positioning positions preferred rectangle representing service side size sizes smartedit targetheight targetwidth top vertical viewport width ypopupoverlay ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEdit",
      "id": "ySelectModule",
      "shortName": "ySelectModule",
      "type": "overview",
      "moduleName": "ySelectModule",
      "shortDescription": "The ySelectModule",
      "keywords": "overview smartedit yselectmodule"
    },
    {
      "section": "smartEdit",
      "id": "ySelectModule.directive:ySelect",
      "shortName": "ySelect",
      "type": "directive",
      "moduleName": "ySelectModule",
      "shortDescription": "This component is a wrapper around ui-select directive and provides filtering capabilities for the dropdown menu that is customizable with an item template.",
      "keywords": "access adds arguments boolean button call called capabilities component contract controller controls customizable data-ng-bind-html default defined determine directive disable disablechoicefn disabled display displayed drop-down dropdown elements entities false fetch fetchall fetchentity fetchpage fetchstrategy field filtering flavour force fulfill function functions i18nkey identification identifier identify initialization initialized input internal invoked isreadonly item items itemtemplate keepmodelonreset keepmodelonresetfor label list longer magnifier mask match menu mode model multi-selectable multiselect non-paged object opposed option optional paged parameter passed path placeholder populate print printed promise property provided providing purposes receives remove renders required reset resolving result resultsheaderlabel resultsheadertemplate resultsheadertemplateurl resultsheadertemplateutl return search selected selection server set single smartedit specifies standard strategy styling template testing time top translate true ui-select values widget work wrapper y-select yinfinitescrolling yinfinitescrollingmodule yselect yselectmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "administration",
      "shortName": "administration",
      "type": "overview",
      "moduleName": "administration",
      "shortDescription": "The administration module",
      "keywords": "administration configurations data display manage module overview point property service services smarteditcontainer uri web"
    },
    {
      "section": "smartEditContainer",
      "id": "administration.ConfigurationEditor",
      "shortName": "administration.ConfigurationEditor",
      "type": "service",
      "moduleName": "administration",
      "shortDescription": "The Configuration Editor Service is a convenience service that provides the methods to manage configurations within the Configuration Editor UI, such as filtering configurations, adding entries and removing entries.",
      "keywords": "actual add addentry adding adds administration angularjs array button call callback called clicks configuration configurationeditor configurationform configurations control convenience delete deleted deletes edited editor entries entry executed false filterconfiguration filtered filtering form formcontroller https init initializes instance list loadcallback loading loads making manage method methods monitor object org parameter remove removeentry removes removing rest returns saves service set smarteditcontainer submit todelete ui user web"
    },
    {
      "section": "smartEditContainer",
      "id": "administration.directive:generalConfiguration",
      "shortName": "generalConfiguration",
      "type": "directive",
      "moduleName": "administration",
      "shortDescription": "The Generation Configuration directive is an HTML marker. It attaches functions of the Configuration Editor to the",
      "keywords": "administration attaches configuration directive display dom editor elements functions general generation html marker order smarteditcontainer template"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule",
      "shortName": "alertFactoryModule",
      "type": "overview",
      "moduleName": "alertFactoryModule",
      "shortDescription": "The Alert service module",
      "keywords": "alert alertfactorymodule alerts application centralized component container display displayed iframed module overview service smarteditcontainer systemalerts systemalertsmodule users"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule.Alert",
      "shortName": "alertFactoryModule.Alert",
      "type": "service",
      "moduleName": "alertFactoryModule",
      "shortDescription": "Alert",
      "keywords": "$alertinjectedcontroller alert alertconf alertconfig alertfactorymodule alias allowed application component consumed controller currently custom display displayed displays dom embedded false hide hides html instance isdisplayed issue message method note object packaged parameter passed represents root se_alert_defaults service shouldn smartedit smarteditcontainer successful systemalerts systemalertsmodule template templateurl true type user"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule.alertFactory",
      "shortName": "alertFactoryModule.alertFactory",
      "type": "service",
      "moduleName": "alertFactoryModule",
      "shortDescription": "alertFactory",
      "keywords": "alert alertconfig alertfactory alertfactorymodule alerts alertservice alertservicemodule allows better create createalert createdanger createinfo createsuccess createwarning danger factory hide info instance instances isdisplayed message method methods needed object se_alert_defaults service set single smarteditcontainer string success type warning"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule.object:AlertConfig",
      "shortName": "AlertConfig",
      "type": "object",
      "moduleName": "alertFactoryModule",
      "shortDescription": "Configuration object for creating/displaying alerts.",
      "keywords": "alert alertfactorymodule alerts allows auto-dismissed automatically closeable closeabled configuration creating default dismiss dismissed displayed dynamized false file greater i18n info inline manually message messageplaceholders object placeholders prevent property se_alert_service_types setting smarteditcontainer template templateurl timeout true type user values"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule.object:SE_ALERT_DEFAULTS",
      "shortName": "SE_ALERT_DEFAULTS",
      "type": "object",
      "moduleName": "alertFactoryModule",
      "shortDescription": "The default values used for alerts.",
      "keywords": "alertconfig alertfactorymodule alerts default details object smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "alertFactoryModule.object:SE_ALERT_SERVICE_TYPES",
      "shortName": "SE_ALERT_SERVICE_TYPES",
      "type": "object",
      "moduleName": "alertFactoryModule",
      "shortDescription": "The available Alert types that can be provided in the type property of an AlertConfig",
      "keywords": "action actions alert alertconfig alertfactorymodule consequences danger encountered errors general harmful info object presents problems property provided result risky smarteditcontainer success successful type types user warning"
    },
    {
      "section": "smartEditContainer",
      "id": "alertServiceModule",
      "shortName": "alertServiceModule",
      "type": "overview",
      "moduleName": "alertServiceModule",
      "shortDescription": "The Alert service module",
      "keywords": "alert alerts alertservicemodule application centralize component container displayed displaying iframed module overview service smarteditcontainer systemalerts systemalertsmodule user"
    },
    {
      "section": "smartEditContainer",
      "id": "alertServiceModule.alertService",
      "shortName": "alertServiceModule.alertService",
      "type": "service",
      "moduleName": "alertServiceModule",
      "shortDescription": "The alert service provides a simple interface for presenting alerts to the user.",
      "keywords": "acts alert alertconfig alertfactory alertfactorymodule alerts alertservice alertservicemodule backwards call compatibility convenience create displays explicitly facade hide interface legacy level lower message method methods needed object presenting pushalerts removealertbyid se_alert_defaults service showalert showdanger showinfo showsuccess showwarning showxy simple smarteditcontainer string user working"
    },
    {
      "section": "smartEditContainer",
      "id": "alertServiceModule.SE_ALERT_SERVICE_GATEWAY_ID",
      "shortName": "alertServiceModule.SE_ALERT_SERVICE_GATEWAY_ID",
      "type": "object",
      "moduleName": "alertServiceModule",
      "shortDescription": "The gateway UID used to proxy the alertService",
      "keywords": "alertservice alertservicemodule gateway match object proxy se_alert_service_gateway_id smartedit smarteditcontainer uid version warning"
    },
    {
      "section": "smartEditContainer",
      "id": "authenticationModule",
      "shortName": "authenticationModule",
      "type": "overview",
      "moduleName": "authenticationModule",
      "shortDescription": "The authenticationModule",
      "keywords": "allows application authenticate authentication authenticationmodule entry logout management module overview points resources service smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "authenticationModule.object:DEFAULT_AUTH_MAP",
      "shortName": "DEFAULT_AUTH_MAP",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The default authentication map contains the entry points to use before an authentication map",
      "keywords": "authentication authenticationmodule configuration default entry loaded map object points service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "authenticationModule.object:DEFAULT_CREDENTIALS_MAP",
      "shortName": "DEFAULT_CREDENTIALS_MAP",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The default credentials map contains the credentials to use before an authentication map",
      "keywords": "authentication authenticationmodule configuration credentials default loaded map object service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "authenticationModule.service:authenticationService",
      "shortName": "authenticationService",
      "type": "service",
      "moduleName": "authenticationModule",
      "shortDescription": "The authenticationService is used to authenticate and logout from SmartEdit.",
      "keywords": "access allows application assigned associated authenticate authenticated authenticates authentication authenticationmap authenticationmodule authenticationservice compare current currently default default_authentication_entry_point entered entry entrypoint expiry expression filterentrypoints flag flow identifying indicate indicates isauthenticated isauthentrypoint key landing logout management map maps matching method object point points promise provided re-authenticated re-authentication redirects registered regular relevant removes requested resolves resource resourcelocationsmodule resources retrieve returns service setreauthinprogress shareddataservice shareddataservicemodule smartedit smarteditcontainer stored successful suitable token tokens true uri url user"
    },
    {
      "section": "smartEditContainer",
      "id": "authorizationModule.AuthorizationService",
      "shortName": "authorizationModule.AuthorizationService",
      "type": "service",
      "moduleName": "authorizationModule",
      "shortDescription": "This service makes calls to the Global Permissions REST API to check if the current user was",
      "keywords": "api array authorizationmodule authorizationservice calls canperformoperation check checks comma current empty error false global granted hasglobalpermissions list method operation parameter perform permissionnames permissions required rest separated service smarteditcontainer string throw true user values version"
    },
    {
      "section": "smartEditContainer",
      "id": "authorizationModule.authorizationService",
      "shortName": "authorizationModule.authorizationService",
      "type": "overview",
      "moduleName": "authorizationModule.authorizationService",
      "shortDescription": "The authorization module provides a service that checks if the current user was granted certain",
      "keywords": "api authorization authorizationmodule authorizationservice backend checks contacting current global granted module overview permissions poll rest restservicefactorymodule service smarteditcontainer user"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogDetailsModule.component:catalogDetails",
      "shortName": "catalogDetails",
      "type": "directive",
      "moduleName": "catalogDetailsModule",
      "shortDescription": "Component responsible for displaying a catalog details. It contains a thumbnail representing the whole ",
      "keywords": "associated boolean catalog catalog-details catalogdetailsmodule component current currently details directive displayed displaying flag iscatalogforcurrentsite landing list provided representing responsible selected site siteidforcatalog smarteditcontainer specifies string thumbnail user versions"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogServiceModule",
      "shortName": "catalogServiceModule",
      "type": "overview",
      "moduleName": "catalogServiceModule",
      "shortDescription": "The catalogServiceModule",
      "keywords": "catalog catalogs catalogservicemodule fetches hybris module overview platform registered service site sites smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogServiceModule.service:catalogService",
      "shortName": "catalogService",
      "type": "service",
      "moduleName": "catalogServiceModule",
      "shortDescription": "The Catalog Service fetches catalogs for a specified site or for all sites registered on the hybris platform using",
      "keywords": "active api array attempt built caches calls catalog catalogid catalogs catalogservicemodule catalogversion catalogversionuuid clearcache cmswebservices configured content contentcatalogid convenience corresponds default descriptor descriptors details determines empties exception experience fail fetched fetches find finds flagged full getactivecontentcatalogversionbycatalogid getactiveproductcatalogversionbycatalogid getallcatalogsgroupedbyid getallcontentcatalogsgroupedbyid getcatalogbyversion getcatalogsforsite getcatalogversionbyuuid getcontentcatalogactiveversion getcontentcatalogsforsite getdefaultsiteforcontentcatalog getproductcatalogsforsite groupings hybris identified invoker iscontentcatalogversionnonactive list match method object objects optional perform performed permitted platform product productcatalogid promise properties provided registered resolves resourcelocationsmodule rest retrieve retrieveuricontext return returnactivecatalogversionuids returned search service shareddataservice shareddataservicemodule simply site siteid sites siteuid smarteditcontainer sorted storing thrown true uid uricontext uuid version versions wrapped"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionDetailsModule",
      "shortName": "catalogVersionDetailsModule",
      "type": "overview",
      "moduleName": "catalogVersionDetailsModule",
      "shortDescription": "This module contains the catalogVersionDetailsModule.component:catalogVersionDetails component.",
      "keywords": "catalogversiondetailsmodule component module overview smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionDetailsModule.component:catalogVersionDetails",
      "shortName": "catalogVersionDetails",
      "type": "directive",
      "moduleName": "catalogVersionDetailsModule",
      "shortDescription": "Component responsible for displaying a catalog version details. Contains a link, called homepage, that ",
      "keywords": "active activecatalogversion associated called catalog catalog-version-details catalogversion catalogversiondetailsmodule component custom default details directive display displaying experience extended functionality homepage items link links object parent provide provided redirects representing responsible site siteid smarteditcontainer version"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionDetailsModule.object:CATALOG_DETAILS_COLUMNS",
      "shortName": "CATALOG_DETAILS_COLUMNS",
      "type": "object",
      "moduleName": "catalogVersionDetailsModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "add angular catalog_details_columns catalogversiondetailsmodule component constant currently enumeration extend injectable items left object options places smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionDetailsModule.service:catalogDetailsService",
      "shortName": "catalogDetailsService",
      "type": "service",
      "moduleName": "catalogVersionDetailsModule",
      "shortDescription": "The catalog details Service makes it possible to add items in form of directive",
      "keywords": "add additems allows array catalog catalogversiondetailsmodule column components constant currently default defined details directive empty extending form getitems hold item items left list method object place places retrieves service side smarteditcontainer template version"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionPermissionModule.service:catalogVersionPermissionService",
      "shortName": "catalogVersionPermissionService",
      "type": "service",
      "moduleName": "catalogVersionPermissionModule",
      "shortDescription": "The catalogVersionPermissionService",
      "keywords": "allows boolean catalog catalogid catalogversion catalogversionpermissionmodule catalogversionpermissionservice current false hasreadpermission hasreadpermissiononcurrent haswritepermission haswritepermissiononcurrent logic method permission permissions promise provided read resolving service smarteditcontainer true user verifies verify version write"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionPermissionRestServiceModule.object:CATALOG_VERSION_PERMISSIONS_RESOURCE_URI",
      "shortName": "CATALOG_VERSION_PERMISSIONS_RESOURCE_URI",
      "type": "object",
      "moduleName": "catalogVersionPermissionRestServiceModule",
      "shortDescription": "Path to fetch permissions of a given catalog version.",
      "keywords": "catalog catalogversionpermissionrestservicemodule fetch object path permissions smarteditcontainer version"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionPermissionRestServiceModule.service:catalogVersionPermissionRestService",
      "shortName": "catalogVersionPermissionRestService",
      "type": "service",
      "moduleName": "catalogVersionPermissionRestServiceModule",
      "shortDescription": "The catalog version permission service is used to check if the current user has been granted certain permissions",
      "keywords": "api apparel-decontentcatalog array catalog catalogid catalogversion catalogversionpermissionrestservicemodule check current exposing false getcatalogversionpermissions granted key method object online permission permissions permissionslist promise read request response returned returns sample service smarteditcontainer true user version write"
    },
    {
      "section": "smartEditContainer",
      "id": "catalogVersionsThumbnailCarouselModule.component:catalogVersionsThumbnailCarousel",
      "shortName": "catalogVersionsThumbnailCarousel",
      "type": "directive",
      "moduleName": "catalogVersionsThumbnailCarouselModule",
      "shortDescription": "Component responsible for displaying a thumbnail of the provided catalog. When clicked,",
      "keywords": "active associated catalog catalog-versions-thumbnail-carousel catalogversionsthumbnailcarouselmodule clicked component current directive displaying object provided redirects representing responsible site siteid smarteditcontainer storefront string thumbnail version"
    },
    {
      "section": "smartEditContainer",
      "id": "clientPagedListModule",
      "shortName": "clientPagedListModule",
      "type": "overview",
      "moduleName": "clientPagedListModule",
      "shortDescription": "The clientPagedListModule",
      "keywords": "allows clientpagedlistmodule custom directive display items list overview paginated renderers search smarteditcontainer sort user"
    },
    {
      "section": "smartEditContainer",
      "id": "clientPagedListModule.directive:clientPagedList",
      "shortName": "clientPagedList",
      "type": "directive",
      "moduleName": "clientPagedListModule",
      "shortDescription": "Directive responsible for displaying a client-side paginated list of items with custom renderers. It allows the user to search and sort the list.",
      "keywords": "$location _buildexperiencepath access allows array bind click client-paged-list client-side clientpagedlistmodule collection column columns current custom data-ng-click descending descriptors determine directive display display-count displaycount displayed displaying event example experiencepath exposes fields filter filterbyfieldfilter filterbyfieldfiltermodule filtered function functions headerpageid headerpagetemplate headerpagetitle headerpagetype headers hitch html i18n iframemanager injectedcontext item itemfilterkeys items items-per-page itemsperpage key list match ngmodel number object onlink pagelist pagelistctl paginated path properties property query renderer renderers responsible return returns reversed search set setcurrentlocation size smarteditcontainer sort sort-by sorted specific string table template title true typecode uid user values var"
    },
    {
      "section": "smartEditContainer",
      "id": "compileHtmlModule",
      "shortName": "compileHtmlModule",
      "type": "overview",
      "moduleName": "compileHtmlModule",
      "shortDescription": "The compileHtmlModule",
      "keywords": "compile compilehtmlmodule directive evaluate html markup overview smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "compileHtmlModule.directive:compileHtml",
      "shortName": "compileHtml",
      "type": "directive",
      "moduleName": "compileHtmlModule",
      "shortDescription": "Directive responsible for evaluating and compiling HTML markup.",
      "keywords": "compile-html compiled compilehtmlmodule compiling data-ng-click directive evaluated evaluating html injectedcontext item markup onlink path property responsible smarteditcontainer string"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule",
      "shortName": "componentHandlerServiceModule",
      "type": "overview",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "this module aims at handling smartEdit components both on the original storefront and the smartEdit overlay",
      "keywords": "aims componenthandlerservicemodule components handling module original overlay overview smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.COMPONENT_CLASS",
      "shortName": "componentHandlerServiceModule.COMPONENT_CLASS",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the css class of the smartEdit components as per contract with the storefront",
      "keywords": "class component_class componenthandlerservicemodule components contract css object smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.componentHandlerService",
      "shortName": "componentHandlerServiceModule.componentHandlerService",
      "type": "service",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "The service provides convenient methods to get DOM references of smartEdit components both in the original laye rof the storefornt and in the smartEdit overlay",
      "keywords": "api applicable application argument body call catalog catalogversionuuid child children class closest combined component componenthandlerservice componenthandlerservicemodule components contained container container_id_attribute container_type_attribute contentslot contract convenient corresponding css cssclass current deep defaults depths determines direct div dom element encoding evaluates example extracts false fetched flag getoverlaycomponent getoverlaycomponentwithinslot handler id_attribute identified ids iframe invoked laye layer level list loaded matching method methods methodsof_getallcomponentsselector methodsof_getallslotsselector methodsof_getcatalogversionuuid methodsof_getcatalogversionuuidfrompage methodsof_getclosestsmarteditcomponent methodsof_getcomponent methodsof_getcomponentinoverlay methodsof_getcomponentunderslot methodsof_getfirstsmarteditcomponentchildren methodsof_getfromselector methodsof_getid methodsof_getoriginalcomponent methodsof_getoriginalcomponentswithinslot methodsof_getoriginalcomponentwithinslot methodsof_getoverlay methodsof_getoverlaycomponent methodsof_getoverlaycomponentwithinslot methodsof_getpageuid methodsof_getpageuuid methodsof_getparent methodsof_getparentslotforcomponent methodsof_getparentslotuuidforcomponent methodsof_getslotoperationrelatedid methodsof_getslotoperationrelatedtype methodsof_getslotoperationrelateduuid methodsof_gettype methodsof_getuuid methodsof_isexternalcomponent methodsof_isoverlayon methodsof_issmarteditcomponent methodsof_setid methodsof_settype node object operations optional original originalcomponent overlay pageuid pageuuid parameter parent passed perform provided references relevant represents resides restrict retrieves return returned returns rof search searched selector service set sets slot slotid smae-layer smartedit smartedit-component-id smarteditcomponentid smarteditcomponenttype smarteditcontainer smarteditslotid storefornt storefront string true type type_attribute typically uid uuid version visible wrapper wrapping yjquery"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id attribute of the smartEdit container, when applicable, as per contract with the storefront",
      "keywords": "applicable attribute componenthandlerservicemodule container container_id_attribute contract object smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type attribute of the smartEdit container, when applicable, as per contract with the storefront",
      "keywords": "applicable attribute componenthandlerservicemodule container container_type_attribute contract object smartedit smarteditcontainer storefront type"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.CONTENT_SLOT_TYPE",
      "shortName": "componentHandlerServiceModule.CONTENT_SLOT_TYPE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type value of the smartEdit slots as per contract with the storefront",
      "keywords": "componenthandlerservicemodule content_slot_type contract object slots smartedit smarteditcontainer storefront type"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.ID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.ID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract id_attribute object smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.OVERLAY_COMPONENT_CLASS",
      "shortName": "componentHandlerServiceModule.OVERLAY_COMPONENT_CLASS",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the css class of the smartEdit component clones copied to the storefront overlay",
      "keywords": "class clones component componenthandlerservicemodule copied css object overlay overlay_component_class smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.OVERLAY_ID",
      "shortName": "componentHandlerServiceModule.OVERLAY_ID",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the identifier of the overlay placed in front of the storefront to where all smartEdit component decorated clones are copied.",
      "keywords": "clones component componenthandlerservicemodule copied decorated front identifier object overlay overlay_id smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.SMARTEDIT_ATTRIBUTE_PREFIX",
      "shortName": "componentHandlerServiceModule.SMARTEDIT_ATTRIBUTE_PREFIX",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "If the storefront needs to expose more attributes than the minimal contract, these attributes must be prefixed with this constant value",
      "keywords": "attributes componenthandlerservicemodule constant contract expose minimal object prefixed smartedit_attribute_prefix smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.SMARTEDIT_IFRAME_ID",
      "shortName": "componentHandlerServiceModule.SMARTEDIT_IFRAME_ID",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the id of the iframe which contains storefront",
      "keywords": "componenthandlerservicemodule iframe object smartedit_iframe_id smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.TYPE_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.TYPE_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the type attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit smarteditcontainer storefront type type_attribute"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the uuid attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit smarteditcontainer storefront uuid uuid_attribute"
    },
    {
      "section": "smartEditContainer",
      "id": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "shortName": "componentHandlerServiceModule.UUID_ATTRIBUTE",
      "type": "object",
      "moduleName": "componentHandlerServiceModule",
      "shortDescription": "the uuid attribute of the smartEdit components as per contract with the storefront",
      "keywords": "attribute componenthandlerservicemodule components contract object smartedit smarteditcontainer storefront uuid uuid_attribute"
    },
    {
      "section": "smartEditContainer",
      "id": "confirmationModalServiceModule",
      "shortName": "confirmationModalServiceModule",
      "type": "overview",
      "moduleName": "confirmationModalServiceModule",
      "shortDescription": "The confirmationModalServiceModule",
      "keywords": "allows confirmation confirmationmodalservicemodule content dependent modal modalservicemodule module opening overview prompt service smarteditcontainer title"
    },
    {
      "section": "smartEditContainer",
      "id": "confirmationModalServiceModule.service:confirmationModalService",
      "shortName": "confirmationModalService",
      "type": "service",
      "moduleName": "confirmationModalServiceModule",
      "shortDescription": "Service used to open a confirmation modal in which an end-user can confirm or cancel an action. A confirmation modal",
      "keywords": "action actioned button cancel configuration confirm confirmation confirmationmodalservicemodule consists content context default description displayed end-user i18n initialized input key method modal modalservice modalservicemodule object open optional override overrides passed promise property provided rejected required resolved service smarteditcontainer text title user"
    },
    {
      "section": "smartEditContainer",
      "id": "containerComponentHandlerServiceModule",
      "shortName": "containerComponentHandlerServiceModule",
      "type": "overview",
      "moduleName": "containerComponentHandlerServiceModule",
      "shortDescription": "this module aims at handling smartEdit container components both on the original storefront and the smartEdit overlay",
      "keywords": "aims components container containercomponenthandlerservicemodule handling module original overlay overview smartedit smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "crossFrameEventServiceModule.service:CrossFrameEventService",
      "shortName": "CrossFrameEventService",
      "type": "service",
      "moduleName": "crossFrameEventServiceModule",
      "shortDescription": "The Cross Frame Event Service is responsible for publishing and subscribing events within and between frames.",
      "keywords": "call callback cross crossframeeventservicegatway crossframeeventservicemodule data event eventid events eventservice eventservicemodule frame frames function gateway gatewayfactory gatewayfactorymodule handler identifier invoked listening message messagegateway method order payload promise publish publishes publishing register registereventhandler registration resolve responsible send sendevent service smarteditcontainer subscribe subscribing systemeventservice transmit unsubscribe"
    },
    {
      "section": "smartEditContainer",
      "id": "dateFormatterModule",
      "shortName": "dateFormatterModule",
      "type": "overview",
      "moduleName": "dateFormatterModule",
      "shortDescription": "The dateFormatterModule",
      "keywords": "dateformattermodule desired displaying format formatter module overview smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule",
      "shortName": "dateTimePickerModule",
      "type": "overview",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePickerModule",
      "keywords": "datetimepicker datetimepickerlocalizationservice datetimepickermodule directive displaying localize module open opened overview picker service smarteditcontainer time tooling"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule.directive:dateTimePicker",
      "shortName": "dateTimePicker",
      "type": "directive",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePicker",
      "keywords": "datetimepicker datetimepickermodule directive smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule.directive:dateTimePicker",
      "shortName": "dateTimePicker",
      "type": "directive",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePicker",
      "keywords": "attributes data-date-formatter datetimepicker datetimepickermodule default desired directive eg- filter format format-type medium pass short smarteditcontainer tag template text type"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule.object: tooltipsMap",
      "shortName": "tooltipsMap",
      "type": "object",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "Contains a map of all tooltips to be localized in the date time picker",
      "keywords": "datetimepickermodule localized map object picker smarteditcontainer time tooltips tooltipsmap"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule.object:resolvedLocaleToMomentLocaleMap",
      "shortName": "resolvedLocaleToMomentLocaleMap",
      "type": "object",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "Contains a map of all inconsistent locales ISOs between SmartEdit and MomentJS",
      "keywords": "datetimepickermodule inconsistent isos locales map momentjs object smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "dateTimePickerModule.service:dateTimePickerLocalizationService",
      "shortName": "dateTimePickerLocalizationService",
      "type": "service",
      "moduleName": "dateTimePickerModule",
      "shortDescription": "The dateTimePickerLocalizationService is responsible for both localizing the date time picker as well as the tooltips",
      "keywords": "datetimepickerlocalizationservice datetimepickermodule localizing picker responsible service smarteditcontainer time tooltips"
    },
    {
      "section": "smartEditContainer",
      "id": "dragAndDropServiceModule",
      "shortName": "dragAndDropServiceModule",
      "type": "overview",
      "moduleName": "dragAndDropServiceModule",
      "shortDescription": "The dragAndDropServiceModule",
      "keywords": "drag draganddropservicemodule drop functionality html overview service smarteditcontainer wraps"
    },
    {
      "section": "smartEditContainer",
      "id": "dragAndDropServiceModule.service:dragAndDropService",
      "shortName": "dragAndDropService",
      "type": "service",
      "moduleName": "dragAndDropServiceModule",
      "shortDescription": "The dragAndDropService wraps over the HTML 5 drag and drop functionality to provide a browser independent interface and very",
      "keywords": "applied applies apply applyall area array basic browser callback called clean configuration configurationid configurations configurationsidlist current custom doesn dom drag draganddropservice draganddropservicemodule dragentercallback draggable dragging dragovercallback drop dropcallback droppable dropped element elements enable enablescrolling enters execute executed experience flag forces function functionality helper hovers html identifier ids iframe ignored image images independent instance instances intended interface items leaves locate managed markdragstarted method mouse note operation outcallback parameter prepare provide provided register registered registering registers remove removed removes richer scrolling selector service smarteditcontainer sourceselector specifies start startcallback started stopcallback stopped support targetselector time track unique unregister update updates user wraps yjquery"
    },
    {
      "section": "smartEditContainer",
      "id": "DropdownPopulatorInterfaceModule.DropdownPopulatorInterface",
      "shortName": "DropdownPopulatorInterfaceModule.DropdownPopulatorInterface",
      "type": "service",
      "moduleName": "DropdownPopulatorInterfaceModule",
      "shortDescription": "Interface describing the contract of a DropdownPopulator fetched through dependency injection by the",
      "keywords": "additional append array attribute attributes building call comma contract current currentpage data default depend dependency dependson deprecated describing descriptor determining directive dropdown dropdownpopulator dropdownpopulatorinterface dropdownpopulatorinterfacemodule dropdowns edited false fetch fetchall fetched fetchpage field filter filtered full genericeditor genericeditormodule idattribute include injection interface ispaged item items key label labelattributes list lists making matches meant method mode model modified number object objects opposed option optional options optionsdropdownpopulator optionsdropdownpopulatormodule ordered orderedlabelattributes original paged pagesize params payload populate populateattributes populates populator promise properties property query request resolving rest retrieve returns search searches searchterm sedropdown sedropdownmodule selected selection separated service set setting smarteditcontainer specifies string term types uri uridropdownpopulator uridropdownpopulatormodule user work"
    },
    {
      "section": "smartEditContainer",
      "id": "editorFieldMappingServiceModule.service:editorFieldMappingService",
      "shortName": "editorFieldMappingService",
      "type": "service",
      "moduleName": "editorFieldMappingServiceModule",
      "shortDescription": "The editorFieldMappingServices contains the strategy that the genericEditor directive",
      "keywords": "add addfieldmapping adhere api area based box call check choose class cmsstructureenumtype cmsstructuretype cmsstructuretypes component componenttypename conditions configurable configuration contract created currently custom customsanitize date-time default defined directive discriminator displays dropdown editor editorfieldmappingservicemodule editorfieldmappingservices editors enabled ensure ensures enum field fields filterable form function functionsmodule generic genericeditor genericeditormodule getfieldmapping holder html identified input instructions invoked list mandatory mapping media method multiple note null optional override overrides path payload picker preselected program property propertyeditortemplate provided rendered required rest retrieve retrieved retrieves returning sanitize sedropdown sedropdownmodule select selects service set short shortstringtemplate smartedit smarteditcomponenttype smarteditcontainer specific specifies strategy structure structuretypename system template templates text transformed type values ways web"
    },
    {
      "section": "smartEditContainer",
      "id": "editorFieldMappingServiceModule.service:PropertyEditorTemplate",
      "shortName": "PropertyEditorTemplate",
      "type": "object",
      "moduleName": "editorFieldMappingServiceModule",
      "shortDescription": "The purpose of the property editor template is to assign a value to model[qualifier].",
      "keywords": "achieve actual api assign de defined described description edited editor editorfieldmappingservicemodule en entities field full genericeditor genericeditormodule goal identifier language localized map model object parent property purpose qualifier receive scope service smarteditcontainer structure template templates"
    },
    {
      "section": "smartEditContainer",
      "id": "eventServiceModule",
      "shortName": "eventServiceModule",
      "type": "overview",
      "moduleName": "eventServiceModule",
      "shortDescription": "eventServiceModule contains an event service which is supported by the SmartEdit gatewayFactory to propagate events between SmartEditContainer and SmartEdit.",
      "keywords": "event events eventservicemodule gatewayfactory gatewayfactorymodule overview propagate service smartedit smarteditcontainer supported"
    },
    {
      "section": "smartEditContainer",
      "id": "eventServiceModule.EVENT_SERVICE_MODE_ASYNCH",
      "shortName": "eventServiceModule.EVENT_SERVICE_MODE_ASYNCH",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "A constant used in the constructor of the Event Service to specify asynchronous event transmission.",
      "keywords": "asynchronous constant event event_service_mode_asynch eventservicemodule object service smarteditcontainer transmission"
    },
    {
      "section": "smartEditContainer",
      "id": "eventServiceModule.EVENT_SERVICE_MODE_SYNCH",
      "shortName": "eventServiceModule.EVENT_SERVICE_MODE_SYNCH",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "A constant that is used in the constructor of the Event Service to specify synchronous event transmission.",
      "keywords": "constant event event_service_mode_synch eventservicemodule object service smarteditcontainer synchronous transmission"
    },
    {
      "section": "smartEditContainer",
      "id": "eventServiceModule.EVENTS",
      "shortName": "eventServiceModule.EVENTS",
      "type": "object",
      "moduleName": "eventServiceModule",
      "shortDescription": "Events that are fired/handled in the SmartEdit application",
      "keywords": "application events eventservicemodule fired object smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "eventServiceModule.EventService",
      "shortName": "eventServiceModule.EventService",
      "type": "service",
      "moduleName": "eventServiceModule",
      "shortDescription": "The event service is used to transmit events synchronously or asynchronously. It also contains options to send",
      "keywords": "asynchronous asynchronously constant constants data defaultmode depending event event_service_mode_asynch event_service_mode_synch eventid events eventservice eventservicemodule handlers identifier method mode options payload register send sendevent service set sets smarteditcontainer synchronous synchronously transmission transmit unregister"
    },
    {
      "section": "smartEditContainer",
      "id": "ExperienceInterceptorModule.experienceInterceptor",
      "shortName": "ExperienceInterceptorModule.experienceInterceptor",
      "type": "service",
      "moduleName": "ExperienceInterceptorModule",
      "shortDescription": "A HTTP request interceptor which intercepts all &#39;cmswebservices/catalogs&#39; requests and adds the current catalog and version",
      "keywords": "$httpprovider adding adds angularjs array called catalog cmswebservices config configuration context current current_context_catalog current_context_catalog_version data define dependencies details experience experienceinterceptor experienceinterceptormodule factories factory headers holds http https initialization injected interceptor interceptors intercepts loaded method methods note object org page_context_catalog page_context_catalog_version page_context_site_id passed preview promise registered replaced request requests retrieve returns service set shared siteid smarteditcontainer stored uri url variables version versions"
    },
    {
      "section": "smartEditContainer",
      "id": "experienceServiceModule.service:experienceService",
      "shortName": "experienceService",
      "type": "service",
      "moduleName": "experienceServiceModule",
      "shortDescription": "The experience Service deals with building experience objects given a context.",
      "keywords": "active builddefaultexperience building catalog catalogid catalogversion catalogversions changes context current deals experience experienceservicemodule getcurrentexperience loaded loadexperience method newpageid object objects pageuid parameters params paratements product provided reconstructed reloads retrieves return service site siteid smarteditcontainer stored update updateexperiencepageid uuid version visible"
    },
    {
      "section": "smartEditContainer",
      "id": "exponentialRetrylModule",
      "shortName": "exponentialRetrylModule",
      "type": "overview",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "This module provides the exponentialRetry service.",
      "keywords": "exponentialretry exponentialretrylmodule module overview service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "exponentialRetrylModule.object:EXPONENTIAL_RETRY_DEFAULT_SETTING",
      "shortName": "EXPONENTIAL_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default exponentialretrylmodule object retry setting smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "exponentialRetrylModule.service:exponentialRetry",
      "shortName": "exponentialRetry",
      "type": "service",
      "moduleName": "exponentialRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide an exponential delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts calculate calculatenextdelay canretry check current delay exponential exponentialretrylmodule false functionality maxattempt maxbackoff maximum method minbackoff minimum number perform provide request retries retry returns service smarteditcontainer strategy time true valid"
    },
    {
      "section": "smartEditContainer",
      "id": "featureInterfaceModule",
      "shortName": "featureInterfaceModule",
      "type": "overview",
      "moduleName": "featureInterfaceModule",
      "keywords": "featureinterfacemodule overview smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "featureInterfaceModule.service:FeatureServiceInterface",
      "shortName": "FeatureServiceInterface",
      "type": "service",
      "moduleName": "featureInterfaceModule",
      "shortDescription": "The interface stipulates how to register features in the SmartEdit application and the SmartEdit container.",
      "keywords": "_registeraliases action activate addcontextualmenubutton adddecorator additems addmappings addtoolbaritem ant-like api applicable application applied bound button buttons callback callbacks classes clicked clicking component componentid components componenttype concrete condition configuration container containerid containertype content contextual contextualmenuservice contextualmenuservicemodule cross css deal decorator decorators decoratorservice decoratorservicemodule default defined delegates description descriptioni18nkey disable disabled disablingcallback displayclass displayed dom element eligible enable enabled enablingcallback entry event exist expression feature featureinterfacemodule featurekey features frame full function functions gateway gatewayfactory gatewayfactorymodule getfeatureproperty handler hold holding html hybrid_action i18n icon iconidle iconnonidle icons identified identifies idle image images implementation include included instance instances interface invocation invoke invoked item items key keythe l18n list location mappings match meant menu method methods methods_additems methods_disable methods_enable methods_register namei18nkey needed non-idle null object optional options order parameter performed permissions perspective perspectiveinterfacemodule perspectiveserviceinterface point properties property propertyname reference regexpkey register registered registers registry regular removed representing represents required respective returns selected selects separate service simplified slot slotid smaller smallicon smartedit smarteditcontainer stand stipulates stores strict style template templates toolbar toolbarid toolbarinterfacemodule toolbarserviceinterface tooltip translated triggered type types unique uniquely url urls user version web wildcard wrapper wrapping"
    },
    {
      "section": "smartEditContainer",
      "id": "FetchDataHandlerInterfaceModule.FetchDataHandlerInterface",
      "shortName": "FetchDataHandlerInterfaceModule.FetchDataHandlerInterface",
      "type": "service",
      "moduleName": "FetchDataHandlerInterfaceModule",
      "shortDescription": "Interface describing the contract of a fetchDataHandler fetched through dependency injection by the",
      "keywords": "contract defined dependency describing descriptor dropdowns eligible entities entity fetch fetchdatahandler fetchdatahandlerinterface fetchdatahandlerinterfacemodule fetched field findbymask genericeditor genericeditormodule getbyid identifier identifying injection interface list mask matching method populate promise resolving returns search service smarteditcontainer type witch"
    },
    {
      "section": "smartEditContainer",
      "id": "filterByFieldFilterModule.filter:filterByField",
      "shortName": "filterByField",
      "type": "filter",
      "moduleName": "filterByFieldFilterModule",
      "shortDescription": "A filter for an array of objects, that will search all the first level fields of an object,",
      "keywords": "add address allows array callbackfcn check considered correspond determines executed field fields filter filterbyfieldfiltermodule filtered filtering function implements include items iteration key level mobile object objects optionally order original parse query return returned search smarteditcontainer strategy string strings values"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule",
      "shortName": "functionsModule",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "provides a list of useful functions that can be used as part of the SmartEdit framework.",
      "keywords": "framework functions functionsmodule list service smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.closeOpenModalsOnBrowserBack",
      "shortName": "functionsModule.closeOpenModalsOnBrowserBack",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "close any open modal window when a user clicks browser back button",
      "keywords": "$modalstack angular-ui browser button clicks close closeopenmodalsonbrowserback functionsmodule modal modalstack open service smarteditcontainer user window"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.convertToArray",
      "shortName": "functionsModule.convertToArray",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "convertToArray will convert the given object to array.",
      "keywords": "array convert converttoarray created elements functionsmodule input inputobject key object original output service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.copy",
      "shortName": "functionsModule.copy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "copy will do a deep copy of the given input object.",
      "keywords": "candidate copied copy deep functionsmodule input javascript object service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.customTimeout",
      "shortName": "functionsModule.customTimeout",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "customTimeout will call the javascrit&#39;s native setTimeout method to execute a given function after a specified period of time.",
      "keywords": "$timeout assert better call customtimeout difficult duration end-to-end execute executed func function functionsmodule javascrit method milliseconds native period service settimeout smarteditcontainer testing time"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.escapeHtml",
      "shortName": "functionsModule.escapeHtml",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "escapeHtml will escape &amp;, &lt;, &gt;, &quot; and &#39; characters .",
      "keywords": "characters escape escaped escapehtml functionsmodule service smarteditcontainer string"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.extend",
      "shortName": "functionsModule.extend",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "extend provides a convenience to either default a new child or &quot;extend&quot; an existing child with the prototype of the parent",
      "keywords": "child childclass convenience default existing extend extended functionsmodule parent parentclass prototype service set smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.extractFromElement",
      "shortName": "functionsModule.extractFromElement",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object",
      "keywords": "dom element elements extract extracted extractfromelement extractionselector functionsmodule html identifying matching object parent parses queriable selector selectors service smarteditcontainer string yjquery"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.generateIdentifier",
      "shortName": "functionsModule.generateIdentifier",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "generateIdentifier will generate a unique string based on system time and a random generator.",
      "keywords": "based functionsmodule generate generateidentifier generator identifier random service smarteditcontainer string system time unique"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.getOrigin",
      "shortName": "functionsModule.getOrigin",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "returns document location origin",
      "keywords": "browsers caters document function functionsmodule gap getorigin location origin returns service smarteditcontainer support w3c"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.getQueryString",
      "shortName": "functionsModule.getQueryString",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "getQueryString will convert a given object into a query string.",
      "keywords": "code convert functionsmodule getquerystring input key1 key2 key3 list object output params query sample service smarteditcontainer snippet string value1 value2 value3 var"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.getURI",
      "shortName": "functionsModule.getURI",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will return the URI part of a URL",
      "keywords": "functionsmodule geturi return returned service smarteditcontainer uri url"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.hitch",
      "shortName": "functionsModule.hitch",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "hitch will create a new function that will pass our desired context (scope) to the given function.",
      "keywords": "assigned binded binding context create desired function functionsmodule hitch method parameters pass pre-bind scope service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.injectJS",
      "shortName": "functionsModule.injectJS",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "injectJS will inject script tags into html for a given set of sources.",
      "keywords": "array callback callbacks configuration configurations execute extract functionsmodule html inject injectjs javascript method object potential provided script service set smarteditcontainer source sources tag tags triggered wired"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.isBlank",
      "shortName": "functionsModule.isBlank",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "isBlank will check if a given string is undefined or null or empty.",
      "keywords": "check empty false functionsmodule input inputstring isblank null returns service smarteditcontainer string true undefined"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.merge",
      "shortName": "functionsModule.merge",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "merge will merge the contents of two objects together into the first object.",
      "keywords": "contents functionsmodule javascript merge object objects result service smarteditcontainer source target"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.parseHTML",
      "shortName": "functionsModule.parseHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object, stripping any JavaScript from the HTML.",
      "keywords": "dom functionsmodule html javascript object parse parsehtml parses queriable representation service smarteditcontainer string stringhtml stripping"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.parseQuery",
      "shortName": "functionsModule.parseQuery",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parseQuery will convert a given query string to an object.",
      "keywords": "code convert functionsmodule input key1 key2 key3 object output params parsed parsequery query sample service smarteditcontainer snippet string value1 value2 value3 var"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.regExpFactory",
      "shortName": "functionsModule.regExpFactory",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "regExpFactory will convert a given pattern into a regular expression.",
      "keywords": "append convert converted expression functionsmodule generated method pattern prepend proper regex regexpfactory regular replaces service smarteditcontainer string wildcards"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.sanitize",
      "shortName": "functionsModule.sanitize",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "escapes any harmful scripting from a string, leaves innocuous HTML untouched/b&gt;",
      "keywords": "functionsmodule harmful html innocuous leaves sanitize sanitized scripting service smarteditcontainer string untouched"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.sanitizeHTML",
      "shortName": "functionsModule.sanitizeHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "sanitizeHTML will remove breaks and space .",
      "keywords": "breaks escaped functionsmodule html remove sanitized sanitizehtml service smarteditcontainer space string"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:formatDateAsUtc",
      "shortName": "formatDateAsUtc",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Formats provided dateTime as utc.",
      "keywords": "datetime format formats formatted functionsmodule provided service smarteditcontainer string utc"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:getDataFromResponse",
      "shortName": "getDataFromResponse",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "when provided with a response returned from a backend call, will filter the response",
      "keywords": "array backend call data filter functionsmodule interest provided response retrieve returned returns service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:getKeyHoldingDataFromResponse",
      "shortName": "getKeyHoldingDataFromResponse",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "when provided with a response returned from a backend call, will filter the response",
      "keywords": "array backend call data filter functionsmodule holding interest key provided response retrieve returned returns service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:isAllTruthy",
      "shortName": "isAllTruthy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Iterate on the given array of Functions, return true if each function returns true",
      "keywords": "arguments array function functions functionsmodule iterate return returns service smarteditcontainer true"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:isAnyTruthy",
      "shortName": "isAnyTruthy",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Iterate on the given array of Functions, return true if at least one function returns true",
      "keywords": "arguments array function functions functionsmodule iterate return returns service smarteditcontainer true"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:isFunctionEmpty",
      "shortName": "isFunctionEmpty",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will determine whether a function body is empty",
      "keywords": "body boolean determine empty evaluate fn function functionsmodule service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:isObjectEmptyDeep",
      "shortName": "isObjectEmptyDeep",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Will check if the object is empty and will return true if each and every property of the object is empty",
      "keywords": "boolean check empty evaluate functionsmodule object property return service smarteditcontainer true"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:resetObject",
      "shortName": "resetObject",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "Resets a given object&#39;s properties&#39; values",
      "keywords": "functionsmodule modelobject object properties reset resets returns service smarteditcontainer structure targetobject values"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.service:URIBuilder",
      "shortName": "URIBuilder",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "builder or URIs, build() method must be invoked to actually retrieve a URI",
      "keywords": "$modalstack angular-ui build builder functionsmodule invoked map matching method modalstack names params placeholder placeholders replaceparams retrieve service smarteditcontainer substitute uri uris values"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.toPromise",
      "shortName": "functionsModule.toPromise",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "toPromise&lt;/&gt; transforms a function into a function that is guaranteed to return a Promise that resolves to the",
      "keywords": "exceptino fails function functionsmodule guaranteed invocation object original promise rejected rejects resolves return service smarteditcontainer topromise transforms"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.trim",
      "shortName": "functionsModule.trim",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "trim will remove spaces at the beginning and end of a given string.",
      "keywords": "functionsmodule input inputstring modified newly remove service smarteditcontainer spaces string trim"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.uniqueArray",
      "shortName": "functionsModule.uniqueArray",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "uniqueArray will return the first Array argument supplemented with new entries from the second Array argument.",
      "keywords": "argument array array1 array2 entries functionsmodule javascript return second service smarteditcontainer supplemented uniquearray"
    },
    {
      "section": "smartEditContainer",
      "id": "functionsModule.unsafeParseHTML",
      "shortName": "functionsModule.unsafeParseHTML",
      "type": "service",
      "moduleName": "functionsModule",
      "shortDescription": "parses a string HTML into a queriable DOM object, preserving any JavaScript present in the HTML.",
      "keywords": "dom failure functionsmodule html javascript location note object originating parse parses preserves preserving queriable representation result safe service smarteditcontainer string stringhtml strings unsafeparsehtml vulnerability xss"
    },
    {
      "section": "smartEditContainer",
      "id": "gatewayFactoryModule.gatewayFactory",
      "shortName": "gatewayFactoryModule.gatewayFactory",
      "type": "service",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "The Gateway Factory controls the creation of and access to MessageGateway",
      "keywords": "access application argument caches call calls channel clients construct controls corresponding create created creategateway creates creation dispatches error event events exist factory fail gateway gatewayfactory gatewayfactorymodule gatewayid handle handler handling identifier initializes initlistener instances lifecycle logged message messagegateway method newly null order postmessage prevent provide return returns second service smarteditcontainer subsequent"
    },
    {
      "section": "smartEditContainer",
      "id": "gatewayFactoryModule.MessageGateway",
      "shortName": "gatewayFactoryModule.MessageGateway",
      "type": "service",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "The Message Gateway is a private channel that is used to publish and subscribe to events across iFrame",
      "keywords": "angularjs attempt attempts based benefits boundaries call callback chain channel controlled creation cross-origin current data event eventid events exist failure function gateway gatewayfactory gatewayfactorymodule gatewayid generated identifier iframe implementation implements instance instances interrupted invoked key listener listening message messagegateway messages method number occurs optional order origins parameter payload pk postmessage primary private promise promises publish publishes receiving registers reject rejected resolve retries scenarios send service side smarteditcontainer subscribe technology underlying unsubscribe w3c-compliant works"
    },
    {
      "section": "smartEditContainer",
      "id": "gatewayFactoryModule.object:TIMEOUT_TO_RETRY_PUBLISHING",
      "shortName": "TIMEOUT_TO_RETRY_PUBLISHING",
      "type": "object",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "Period between two retries of a gatewayFactoryModule.MessageGateway to publish an event",
      "keywords": "browser event explorer frames gatewayfactorymodule greater internet messagegateway needed object period postmessage process publish retries smarteditcontainer time"
    },
    {
      "section": "smartEditContainer",
      "id": "gatewayFactoryModule.object:WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY",
      "shortName": "WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY",
      "type": "object",
      "moduleName": "gatewayFactoryModule",
      "shortDescription": "the name of the configuration key containing the list of white listed storefront domain names",
      "keywords": "configuration domain gatewayfactorymodule key list listed names object smarteditcontainer storefront white"
    },
    {
      "section": "smartEditContainer",
      "id": "gatewayProxyModule.gatewayProxy",
      "shortName": "gatewayProxyModule.gatewayProxy",
      "type": "service",
      "moduleName": "gatewayProxyModule",
      "shortDescription": "To seamlessly integrate the gateway factory between two services on different frames, you can use a gateway",
      "keywords": "allowing api attaches automatically avoid body call calls communication declared default delegates empty explicit factory forward frames function functions gateway gatewayfactory gatewayfactorymodule gatewayid gatewayproxy gatewayproxymodule initforservice inner instance instances integrate internaly listeners method methods methodssubset module mutate mutates param process promise promises property provide provided providing proxied proxy publish registers registration requires result seamlessly service services set simplifies smarteditcontainer string stub trigger turned unique unnecessarily ways wraps"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule",
      "shortName": "genericEditorModule",
      "type": "overview",
      "moduleName": "genericEditorModule",
      "keywords": "genericeditormodule overview smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.directive:genericEditor",
      "shortName": "genericEditor",
      "type": "directive",
      "moduleName": "genericEditorModule",
      "shortDescription": "Component responsible for generating custom HTML CRUD form for any smarteditComponent type.",
      "keywords": "api arguments binding button callback called calls cancel component content contentapi contract controller controls create created creates crud current custom customonsubmit data delete deleted described directive display dom edited editor element example expected experience exposes extracted find form format forms fulfills function generating generic generic-editor genericeditor genericeditormodule getapi getcomponent html identifier indicates initial inner instance invoked invoker isdirty isvalid json local making method model modified object onsubmit optional original overridden parameter params pass pristine promise read reset response responsible rest return returns scope server service set sets shareddataservice smartedit smarteditcomponent smarteditcomponentid smarteditcomponenttype smarteditcontainer storefront string structure structureapi submit successful type update updatecallback updated uri uricontext valid widgets"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.object:GENERIC_EDITOR_LOADED_EVENT",
      "shortName": "GENERIC_EDITOR_LOADED_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify subscribers that GenericEditor is loaded.",
      "keywords": "event genericeditor genericeditormodule loaded notify object smarteditcontainer subscribers"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT",
      "shortName": "GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify GenericEditor about errors that can be relevant to it.",
      "keywords": "errors event generic_editor_unrelated_validation_messages_event genericeditor genericeditormodule notification notify object receive received relevant situation smarteditcontainer validation"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT",
      "shortName": "GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "Event to notify GenericEditor about messages (errors, warnings) that can be relevant to it.",
      "keywords": "allows delivered editor editors event generic genericeditor genericeditormodule list map messages notification notify object optional optional-id payload properties receive received relevant send sendasynchevent situation smarteditcontainer specific systemeventservice target targetgenericeditorid unrelatedvalidationmessages validation warnings"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.object:genericEditorApi",
      "shortName": "genericEditorApi",
      "type": "object",
      "moduleName": "genericEditorModule",
      "shortDescription": "The generic editor&#39;s api object exposing public functionality",
      "keywords": "api changes clearmessages clears component content copy current editor everytime exposing function functionality generic genericeditormodule getcomponent messages method model object oncontentchange public reinitialize replace returns smarteditcontainer triggered updatecomponent updates validation"
    },
    {
      "section": "smartEditContainer",
      "id": "genericEditorModule.service:GenericEditor",
      "shortName": "GenericEditor",
      "type": "service",
      "moduleName": "genericEditorModule",
      "shortDescription": "The Generic Editor is a class that makes it possible for SmartEdit users (CMS managers, editors, etc.) to edit components in the SmartEdit interface.",
      "keywords": "actual advised api array associated attempted attributes based boolean build built call case class cmsstructureenumtype cmsstructuretype code comparing component componentform components componenttypes content convention crud current data datahandler datetime de default defining details determine determined directive dirty display displayed documentation dropdown dropdowns edit editable edited editor editorfieldmappingservice editorfieldmappingservicemodule editors element en english entries entry enum error errors example exception expect expected expects expression extensible fail fallback false fetch fetchdatahandlerinterface fetchdatahandlerinterfacemodule fetches field fields filter filtered form format fr french fulfill function functionality generic genericeditor genericeditorapi genericeditormodule getapi hindi holds html i18nkey i18nkeyforsomequalifier1 i18nkeyforsomequalifier2 i18nkeyforsomequalifier3 i18nkeyforsomequalifier4 i18nkeyforsomequalifier5 i18nkeyforsomequalifier6 i18nkeyforsomequalifier7 identifier implementation implies indicates indicator initialization interface invoked isdirty iso json key label language learn list local localized long longstring manage managers mandatory map mask match mechanism media message messages method methods_refreshoptions modified mypackage naming non-localized object occurs option options orientation original owned pattern payload payloads performing populates post predicate preparepayload pristine property provided public qualified qualifier qualifier1 qualifier2 qualifier3 qualifier4 reads received refreshoptions regular request requested requests required requirements requires reset resource response rest return returned returns richtext saved saves saving search second select selection serialized server service sets shortstring smartedit smarteditcontainer somequalifier1 somequalifier2 somequalifier3 somequalifier4 somequalifier5 somequalifier6 somequalifier7 sort specific strategy string strings structure structures subject submit support templates throw timestamps transformed transforms translated true type types update updated updates users validation validationerror values warning widget widgets"
    },
    {
      "section": "smartEditContainer",
      "id": "hasOperationPermissionModule",
      "shortName": "hasOperationPermissionModule",
      "type": "overview",
      "moduleName": "hasOperationPermissionModule",
      "shortDescription": "This module provides a directive used to determine if the current user has permission to perform the action defined",
      "keywords": "action current defined determine directive dom elements hasoperationpermissionmodule key module overview perform permission removes smarteditcontainer user"
    },
    {
      "section": "smartEditContainer",
      "id": "hasOperationPermissionModule.directive:hasOperationPermission",
      "shortName": "hasOperationPermission",
      "type": "directive",
      "moduleName": "hasOperationPermissionModule",
      "shortDescription": "Authorization HTML mark-up that will remove elements from the DOM if the user does not have authorization defined",
      "keywords": "access array authorization called check comma-separated commas context current data defined directive dom elements extra function has-operation-permission hasoperationpermissionmodule html included input list mark-up names object objects parameter permission permissionservice permissionserviceinterface permissionserviceinterfacemodule property remove rule separated service set smarteditcontainer string structured takes user validate verify"
    },
    {
      "section": "smartEditContainer",
      "id": "httpAuthInterceptorModule.httpAuthInterceptor",
      "shortName": "httpAuthInterceptorModule.httpAuthInterceptor",
      "type": "service",
      "moduleName": "httpAuthInterceptorModule",
      "shortDescription": "Makes it possible to perform global authentication by intercepting requests before they are forwarded to the server",
      "keywords": "$http $httpprovider adding adds application array authentication call called code config configuration dependencies factories factory forwarded forwards global holds http httpauthinterceptor httpauthinterceptormodule injected intercepted intercepting interceptor interceptors intercepts method methods object perform registered request requests resource responses rest returns server service smarteditcontainer token"
    },
    {
      "section": "smartEditContainer",
      "id": "httpErrorInterceptorServiceModule",
      "shortName": "httpErrorInterceptorServiceModule",
      "type": "overview",
      "moduleName": "httpErrorInterceptorServiceModule",
      "shortDescription": "This module provides the functionality to add custom HTTP error interceptors.",
      "keywords": "add code custom error execute fails functionality http httperrorinterceptorservicemodule interceptors module overview request smarteditcontainer time"
    },
    {
      "section": "smartEditContainer",
      "id": "httpErrorInterceptorServiceModule.service:httpErrorInterceptorService",
      "shortName": "httpErrorInterceptorService",
      "type": "service",
      "moduleName": "httpErrorInterceptorServiceModule",
      "shortDescription": "The httpErrorInterceptorService provides the functionality to add custom HTTP error interceptors.",
      "keywords": "$q add addinterceptor alertservice alertservicemodule angular associated call called calls controller current custom customerrorinterceptor custominterceptorexample default defined designed error examplecontroller factory fails fulfill function functionality functions generic http httperrorinterceptorservice httperrorinterceptorservicemodule interceptor interceptors iterates iteration js matches message method modified modifies module object override pair predicate registered reject rejected represented request resolved resolves response responseerror return returning sequentially service showdanger smarteditcontainer specific status stops time true unregister unregistercustominterceptor var"
    },
    {
      "section": "smartEditContainer",
      "id": "i18nInterceptorModule.object:I18NAPIROOT",
      "shortName": "I18NAPIROOT",
      "type": "object",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "The I18NAPIroot is a hard-coded URI that is used to initialize the translationServiceModule.",
      "keywords": "hard-coded i18n_resource_uri i18napiroot i18ninterceptor i18ninterceptormodule initialize intercepts methods_request object replaces request resourcelocationsmodule service smarteditcontainer translationservicemodule uri"
    },
    {
      "section": "smartEditContainer",
      "id": "i18nInterceptorModule.object:UNDEFINED_LOCALE",
      "shortName": "UNDEFINED_LOCALE",
      "type": "object",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "The undefined locale set as the preferred language of the translationServiceModule so that",
      "keywords": "browser i18ninterceptor i18ninterceptormodule intercept language locale methods_request object preferred replace request service set smarteditcontainer translationservicemodule undefined"
    },
    {
      "section": "smartEditContainer",
      "id": "i18nInterceptorModule.service:i18nInterceptor",
      "shortName": "i18nInterceptor",
      "type": "service",
      "moduleName": "i18nInterceptorModule",
      "shortDescription": "A HTTP request interceptor that intercepts all i18n calls and handles them as required in the i18nInterceptor.request method.",
      "keywords": "$httpprovider adding angularjs appends array called calls config configuration defined dependencies factories factory getresolvelocale handles http https i18n i18n_resource_uri i18napiroot i18ninterceptor i18ninterceptormodule injected interceptor interceptors intercepts invoked languageservice languageservicemodule locale method methods methods_getresolvelocale methods_request object org passed promise provided registered replaces request requests required resourcelocationsmodule retrieved returns service smarteditcontainer url"
    },
    {
      "section": "smartEditContainer",
      "id": "iframeClickDetectionServiceModule",
      "shortName": "iframeClickDetectionServiceModule",
      "type": "overview",
      "moduleName": "iframeClickDetectionServiceModule",
      "shortDescription": "The iframeClickDetectionServiceModule",
      "keywords": "application click container detection events functionality gateway iframe iframeclickdetectionservicemodule listen module mousedown overview proxy requires smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "iframeClickDetectionServiceModule.service:iframeClickDetectionService",
      "shortName": "iframeClickDetectionService",
      "type": "service",
      "moduleName": "iframeClickDetectionServiceModule",
      "shortDescription": "The iframe Click Detection service uses the  gatewayProxy service to listen",
      "keywords": "application callback callbacks click currently detection events function functionality gatewayproxy gatewayproxymodule iframe iframeclickdetectionservicemodule listen listener method mousedown occurs oniframeclick register registercallback registered registers remove removecallback removes service smartedit smarteditcontainer triggered triggers"
    },
    {
      "section": "smartEditContainer",
      "id": "iFrameManagerModule",
      "shortName": "iFrameManagerModule",
      "type": "service",
      "moduleName": "iFrameManagerModule",
      "shortDescription": "Module that provides a service called iFrameManager which has a set of methods",
      "keywords": "called iframe iframemanager iframemanagermodule load methods module service set smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "iFrameManagerModule.iFrameManager",
      "shortName": "iFrameManagerModule.iFrameManager",
      "type": "service",
      "moduleName": "iFrameManagerModule",
      "shortDescription": "The iFrame Manager service provides methods to load the storefront into an iframe. The preview of the storefront can be loaded for a specified input homepage and a specified preview ticket. The iframe src attribute is updated with that information in order to display the storefront in SmartEdit.",
      "keywords": "add ajax api append attribute boolean call called catalog check constraint content-type context current data display exist exists experience homepage homepageorpagefrompagelist iframe iframemanager iframemanagermodule indicating initial initialize initializecatalogpreview input landing language list load loaded loading loadpreview loads location manager method methods mode modified order preview previewticket prior query redirected requested returns select server service set setcurrentlocation sets setting shared smartedit smarteditcontainer src stored storefront stores string subsequent ticket time updated uri url user"
    },
    {
      "section": "smartEditContainer",
      "id": "interceptorHelperModule.service:interceptorHelper",
      "shortName": "interceptorHelper",
      "type": "service",
      "moduleName": "interceptorHelperModule",
      "shortDescription": "Helper service used to handle request and response in interceptors",
      "keywords": "body callback config error function handle handled handles helper initial interceptor interceptorhelpermodule interceptors method methodsof_handlerequest methodsof_handleresponse methodsof_handleresponseerror object promise rejecting request resolving response return service smarteditcontainer success"
    },
    {
      "section": "smartEditContainer",
      "id": "l10nModule.filter:l10n",
      "shortName": "l10n",
      "type": "filter",
      "moduleName": "l10nModule",
      "shortDescription": "Filter that accepts a localized map as input and returns the value corresponding to the resolvedLocale of languageServiceModule and defaults to the first entry.",
      "keywords": "accepts class corresponding defaults entry extended filter input instantiated interface isocodes l10nmodule language languageservicemodule localized localizedmap map resolvedlocale returns serves smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "landingPageModule",
      "shortName": "landingPageModule",
      "type": "overview",
      "moduleName": "landingPageModule",
      "shortDescription": "This module contains the component responsible of displaying the SmartEdit landing page.",
      "keywords": "component displaying landing landingpagemodule module overview responsible smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "landingPageModule.component:landingPage",
      "shortName": "landingPage",
      "type": "directive",
      "moduleName": "landingPageModule",
      "shortDescription": "Component responsible of displaying the SmartEdit landing page. It retrieves a list of sites, which can be filtered.",
      "keywords": "application areas associated catalog component directive display displaying filtered landing landing-page landingpagemodule list navigate responsible retrieves selected site sites smartedit smarteditcontainer storefront user"
    },
    {
      "section": "smartEditContainer",
      "id": "languageSelectorModule",
      "shortName": "languageSelectorModule",
      "type": "overview",
      "moduleName": "languageSelectorModule",
      "shortDescription": "The language selector module contains a directive which allow the user to select a language.",
      "keywords": "allow api backend call directive language languages languageselectormodule languageservice languageservicemodule list module order overview select selector service smarteditcontainer supported user"
    },
    {
      "section": "smartEditContainer",
      "id": "languageSelectorModule.directive:languageSelector",
      "shortName": "languageSelector",
      "type": "directive",
      "moduleName": "languageSelectorModule",
      "shortDescription": "Language selector provides a drop-down list which contains a list of supported languages.",
      "keywords": "directive drop-down language languages languageselectormodule list select selector smarteditcontainer supported system translate"
    },
    {
      "section": "smartEditContainer",
      "id": "languageServiceModule",
      "shortName": "languageServiceModule",
      "type": "overview",
      "moduleName": "languageServiceModule",
      "shortDescription": "The languageServiceModule",
      "keywords": "fetches language languages languageservicemodule module overview service site smarteditcontainer supported"
    },
    {
      "section": "smartEditContainer",
      "id": "languageServiceModule.SELECTED_LANGUAGE",
      "shortName": "languageServiceModule.SELECTED_LANGUAGE",
      "type": "object",
      "moduleName": "languageServiceModule",
      "shortDescription": "A constant that is used as key to store the selected language in the storageService",
      "keywords": "constant key language languageservicemodule object selected selected_language smarteditcontainer storageservice store"
    },
    {
      "section": "smartEditContainer",
      "id": "languageServiceModule.service:languageService",
      "shortName": "languageService",
      "type": "service",
      "moduleName": "languageServiceModule",
      "shortDescription": "The Language Service fetches all languages for a specified site using REST service calls to the cmswebservices languages API.",
      "keywords": "active api array browser callback calls check cmswebservices code current currently descriptor descriptors determine determines en en_us english fetched fetches format fr french function gateway getbrowserlanguageisocode getbrowserlocale getlanguagesforsite getresolvelocale getresolvelocaleisocode gettoolinglanguages i18n identifier iso isocode language languages languageservicemodule list locale method nativename object order preference promise properties register registerswitchlanguage required resolve resolved resolves rest retrieves saved selected service set setselectedlanguage site sites siteuid smarteditcontainer smarteditwebservices storage storefront supported switch system tooling true uid unique user"
    },
    {
      "section": "smartEditContainer",
      "id": "languageServiceModule.SWITCH_LANGUAGE_EVENT",
      "shortName": "languageServiceModule.SWITCH_LANGUAGE_EVENT",
      "type": "object",
      "moduleName": "languageServiceModule",
      "shortDescription": "A constant that is used as key to publish and receive events when a language is changed.",
      "keywords": "changed constant events key language languageservicemodule object publish receive smarteditcontainer switch_language_event"
    },
    {
      "section": "smartEditContainer",
      "id": "linearRetrylModule",
      "shortName": "linearRetrylModule",
      "type": "overview",
      "moduleName": "linearRetrylModule",
      "shortDescription": "This module provides the linearRetry service.",
      "keywords": "linearretry linearretrylmodule module overview service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "linearRetrylModule.object:LINEAR_RETRY_DEFAULT_SETTING",
      "shortName": "LINEAR_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "linearRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default linearretrylmodule object retry setting smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "linearRetrylModule.service:linearRetry",
      "shortName": "linearRetry",
      "type": "service",
      "moduleName": "linearRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide a linear delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts base calculate calculatenextdelay canretry check current delay false functionality interval linear linearretrylmodule maxbackoff maximum maxretry method minbackoff minimum number perform provide request retries retry retryinterval returns service smarteditcontainer strategy time true valid"
    },
    {
      "section": "smartEditContainer",
      "id": "loadConfigModule",
      "shortName": "loadConfigModule",
      "type": "overview",
      "moduleName": "loadConfigModule",
      "shortDescription": "The loadConfigModule supplies configuration information to SmartEdit. Configuration is stored in key/value pairs.",
      "keywords": "array configuration exposes functionsmodule key load loadconfigmodule module ngresource object overview pairs resourcelocationsmodule service shareddataservicemodule smartedit smarteditcontainer stored supplies"
    },
    {
      "section": "smartEditContainer",
      "id": "loadConfigModule.service:LoadConfigManager",
      "shortName": "LoadConfigManager",
      "type": "service",
      "moduleName": "loadConfigModule",
      "shortDescription": "The LoadConfigManager is used to retrieve configurations stored in configuration API.",
      "keywords": "$log $resource _prettify api array conf configuration configurations converts converttoarray copy create defaulttoolinglanguage example function hitch key loadasarray loadasobject loadconfigmanager loadconfigmanagerservice loadconfigmodule mapped method object pairs promise resourcelocationsmodule retrieve retrieves returns service set shareddataservice smarteditcontainer stored values"
    },
    {
      "section": "smartEditContainer",
      "id": "loadConfigModule.service:loadConfigManagerService",
      "shortName": "loadConfigManagerService",
      "type": "service",
      "moduleName": "loadConfigModule",
      "shortDescription": "A service that is a singleton of loadConfigModule.service:LoadConfigManager  which is used to ",
      "keywords": "configuration entry loadconfigmanager loadconfigmodule module point retrieve service services singleton smartedit smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "modalServiceModule",
      "shortName": "modalServiceModule",
      "type": "overview",
      "moduleName": "modalServiceModule",
      "shortDescription": "The modalServiceModule",
      "keywords": "achieving actions adding additionally affect behave button buttons chaning closing components create devoted easy goal manage modal modalmanager modalservice modalservicemodule module object open opened overview providing service smarteditcontainer style styles title window windows"
    },
    {
      "section": "smartEditContainer",
      "id": "modalServiceModule.modalService",
      "shortName": "modalServiceModule.modalService",
      "type": "service",
      "moduleName": "modalServiceModule",
      "shortDescription": "Convenience service to open and style a promise-based templated modal window.",
      "keywords": "$log $q $scope action actions acts addbutton additional angular angularjs animation animations app array button buttonhandlerfn buttons callbacks called caller calling cancel choose classed close closed common complex conf config configuration configurations content controller convenience css cssclasses custom data debug declared defer deferred dependency depending details determines dismiss display errorcallback example explicit explicitly factory feel fragment function functions html https injection inline key label list log logic method methods_addbutton methods_close modal modal_button_actions modal_button_styles modalcontroller modalmanager modalservice modalservicemodule module multiple object open org path piece promise promise-based reject rejected resolve resolved result return separated service setbuttonhandler share simple smarteditcontainer someresult someservice space style submit successcallback template templated templateinline templateurl title translated true validatesomething var ways window windows"
    },
    {
      "section": "smartEditContainer",
      "id": "modalServiceModule.object:MODAL_BUTTON_ACTIONS",
      "shortName": "MODAL_BUTTON_ACTIONS",
      "type": "object",
      "moduleName": "modalServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "action addbutton adding angular angularjs button close close_modal constant defines dismiss example executing existing getbuttons https indicates injectable label methods_getbuttons methods_open modal modal_button_actions modalmanager modalservice modalservicemodule mymodalmanager object open opening org performed promise property rejected resolved returned service smarteditcontainer window"
    },
    {
      "section": "smartEditContainer",
      "id": "modalServiceModule.object:MODAL_BUTTON_STYLES",
      "shortName": "MODAL_BUTTON_STYLES",
      "type": "object",
      "moduleName": "modalServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "addbutton adding angular button cancel cancel_button constant default defines equivalent example existing feel getbuttons indicates injectable label methods_getbuttons methods_open modal modal_button_styles modalmanager modalservice modalservicemodule mymodalmanager object open opening primary property save secondary service smarteditcontainer style styled submit window"
    },
    {
      "section": "smartEditContainer",
      "id": "modalServiceModule.service:ModalManager",
      "shortName": "ModalManager",
      "type": "service",
      "moduleName": "modalServiceModule",
      "shortDescription": "The ModalManager is a service designed to provide easy runtime modification to various aspects of a modal window,",
      "keywords": "$log $q $scope access action addbutton adding additionally allowing allows angularjs applied array aspects avoid button buttonhandlerfn buttonhandlerfunction buttonid buttonpressedcallback buttons callback callbacks called caller cancel cancelled case caution chain clone close closed closing code complexity conf configuration content continue controller corner create data debug default defer deferred designed details disablebutton disabled dismiss dismisscallback displayed don easy empty enable enablebutton enabled enables errorcallback example execute executed exposed fetched fired flag function getbutton getbuttons handler handlers happen header hello https i18n ignore implicitly instance isolated key kind label long manager matching method methods_addbutton methods_setbuttonhandler modal modal_button_actions modal_button_styles modalmanager modalservice modalservicemodule modaltestcontroller modification modifying multiple newly note null object open org parameter parameters pass passed passing press pressed prevent preventing process promise properties provide provided publicly read receives reference registered reject rejected rejecting remove removeallbuttons removebutton removed representing resolve resolved resolving return returned returns runtime sample scenarios scope service setbuttonhandler setdismisscallback setshowheaderdismiss setting showx single smarteditcontainer someresult string style submit successcallback suggested title top translated trigger true undefined unique unnecessary update validatesomething validating validation var window"
    },
    {
      "section": "smartEditContainer",
      "id": "nonvalidationErrorInterceptorModule.service:nonValidationErrorInterceptor",
      "shortName": "nonValidationErrorInterceptor",
      "type": "service",
      "moduleName": "nonvalidationErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 400. It removes all errors of type &#39;ValidationError&#39; and displays alert messages for non-validation errors.",
      "keywords": "alert code displays error errors http messages non-validation nonvalidationerrorinterceptormodule removes service smarteditcontainer type validationerror"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceInterfaceModule",
      "shortName": "notificationServiceInterfaceModule",
      "type": "overview",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The notification module provides a service to display visual cues to inform",
      "keywords": "application container cues display iframed inform module notification notificationserviceinterfacemodule overview service smarteditcontainer user visual"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceInterfaceModule.object:SE_NOTIFICATION_SERVICE_GATEWAY_ID",
      "shortName": "SE_NOTIFICATION_SERVICE_GATEWAY_ID",
      "type": "object",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The gateway UID used to proxy the NotificationService.",
      "keywords": "gateway notificationservice notificationserviceinterfacemodule object proxy smarteditcontainer uid"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceInterfaceModule.service:NotificationServiceInterface",
      "shortName": "NotificationServiceInterface",
      "type": "service",
      "moduleName": "notificationServiceInterfaceModule",
      "shortDescription": "The interface defines the methods required to manage notifications that are to be displayed to the user.",
      "keywords": "adds based configuration creates defines displayed empty error html identifier interface list manage method methods notification notificationid notifications notificationserviceinterfacemodule pushnotification removeallnotifications removenotification removes required service smarteditcontainer string template templateurl top unique url user"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceModule",
      "shortName": "notificationServiceModule",
      "type": "overview",
      "moduleName": "notificationServiceModule",
      "shortDescription": "The notification module provides a service to display visual cues to inform",
      "keywords": "application container cues display iframed inform module notification notificationservicemodule overview service smarteditcontainer user visual"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceModule.object:EVENT_NOTIFICATION_CHANGED",
      "shortName": "EVENT_NOTIFICATION_CHANGED",
      "type": "object",
      "moduleName": "notificationServiceModule",
      "shortDescription": "The ID of the event that is triggered when a notification is pushed or removed.",
      "keywords": "event notification notificationservicemodule object pushed removed smarteditcontainer triggered"
    },
    {
      "section": "smartEditContainer",
      "id": "notificationServiceModule.service:notificationService",
      "shortName": "notificationService",
      "type": "service",
      "moduleName": "notificationServiceModule",
      "shortDescription": "The notification service is used to display visual cues to inform the user of the state of the application.",
      "keywords": "application cues display inform notification notificationservicemodule service smarteditcontainer user visual"
    },
    {
      "section": "smartEditContainer",
      "id": "operationContextServiceModule",
      "shortName": "operationContextServiceModule",
      "type": "overview",
      "moduleName": "operationContextServiceModule",
      "shortDescription": "This module provides the functionality to register a set of url with their associated operation contexts.",
      "keywords": "associated contexts functionality module operation operationcontextservicemodule overview register set smarteditcontainer url"
    },
    {
      "section": "smartEditContainer",
      "id": "operationContextServiceModule.object:OPERATION_CONTEXT",
      "shortName": "OPERATION_CONTEXT",
      "type": "object",
      "moduleName": "operationContextServiceModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "angular application constant context enumeration injectable object operation operationcontextservicemodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "operationContextServiceModule.service:operationContextService",
      "shortName": "operationContextService",
      "type": "service",
      "moduleName": "operationContextServiceModule",
      "keywords": "associated context find findoperationcontext matching method operation operationcontext operationcontextservice operationcontextservicemodule register request service smarteditcontainer url"
    },
    {
      "section": "smartEditContainer",
      "id": "optionsDropdownPopulatorModule.service:optionsDropdownPopulator",
      "shortName": "optionsDropdownPopulator",
      "type": "service",
      "moduleName": "optionsDropdownPopulatorModule",
      "shortDescription": "implementation of DropdownPopulatorInterface for &quot;EditableDropdown&quot; cmsStructureType",
      "keywords": "attribute cmsstructuretype dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown implementation method options optionsdropdownpopulatormodule populate service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "Page.object:Page",
      "shortName": "Page",
      "type": "object",
      "moduleName": "Page",
      "shortDescription": "An object representing the backend response to a paged query",
      "keywords": "array backend elements exceed object paged pagination pertaining property query representing requested response returned size smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "Page.object:Pagination",
      "shortName": "Pagination",
      "type": "object",
      "moduleName": "Page",
      "shortDescription": "An object representing the returned pagination information from backend",
      "keywords": "backend elements mask matching object pagination property representing returned smarteditcontainer total totalcount"
    },
    {
      "section": "smartEditContainer",
      "id": "pageListLinkModule.directive:homePageLink",
      "shortName": "homePageLink",
      "type": "directive",
      "moduleName": "pageListLinkModule",
      "shortDescription": "Directive that displays a link to the main storefront page.",
      "keywords": "associated boolean catalog catalogversion directive displays link main object pagelistlinkmodule provided representing site siteid smarteditcontainer storefront string version"
    },
    {
      "section": "smartEditContainer",
      "id": "pageSensitiveDirectiveModule",
      "shortName": "pageSensitiveDirectiveModule",
      "type": "overview",
      "moduleName": "pageSensitiveDirectiveModule",
      "shortDescription": "This module defines the pageSensitive attribute directive.",
      "keywords": "attribute defines directive module overview pagesensitive pagesensitivedirectivemodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "pageSensitiveDirectiveModule.directive:pageSensitive",
      "shortName": "pageSensitive",
      "type": "directive",
      "moduleName": "pageSensitiveDirectiveModule",
      "shortDescription": "Will cause an Angular re-compilation of the node declaring this directive whenever the page identifier in smartEdit layer changes",
      "keywords": "angular changes declaring directive identifier layer node pagesensitivedirectivemodule re-compilation smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "pageToolMenuModule",
      "shortName": "pageToolMenuModule",
      "type": "overview",
      "moduleName": "pageToolMenuModule",
      "shortDescription": "The sharedDataServiceModule",
      "keywords": "application container data key module overview pagetoolmenumodule retrieve service shared shareddataservicemodule smartedit smarteditcontainer store"
    },
    {
      "section": "smartEditContainer",
      "id": "permissionServiceInterfaceModule",
      "shortName": "permissionServiceInterfaceModule",
      "type": "overview",
      "moduleName": "permissionServiceInterfaceModule",
      "shortDescription": "The permissions service resolves user permissions returning true or false.",
      "keywords": "false overview permissions permissionserviceinterfacemodule resolves returning service smarteditcontainer true user"
    },
    {
      "section": "smartEditContainer",
      "id": "permissionServiceInterfaceModule.service:PermissionServiceInterface",
      "shortName": "PermissionServiceInterface",
      "type": "service",
      "moduleName": "permissionServiceInterfaceModule",
      "shortDescription": "The permission service is used to check if a user has been granted certain permissions.",
      "keywords": "accidentally alias aliases appropriate array associated attempting avoid cached caches called calling character check checks clearance clearcache clears combination common configuration configured context convenience data default defined determine empty entry error execute executed fake false function getpermission granted isn ispermitted list logic lookup method names namespace number object objects order overriding parameter permission permissionname permissions permissionserviceinterfacemodule permissionservicemodule prefixed promise property reduces references register registerdefaultrule registered registering registerpermission registerrule registers rejects required reserved resolve resolves responds return returns rule ruleconfiguration rules se service set smarteditcontainer spaced structured takes throw thrown true un-registers undefined unregisterdefaultrule user valid verification verified verify"
    },
    {
      "section": "smartEditContainer",
      "id": "permissionServiceModule.object:DEFAULT_RULE_NAME",
      "shortName": "DEFAULT_RULE_NAME",
      "type": "object",
      "moduleName": "permissionServiceModule",
      "shortDescription": "The name used to register the default rule.",
      "keywords": "default object permissionservicemodule register rule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "perspectiveInterfaceModule",
      "shortName": "perspectiveInterfaceModule",
      "type": "overview",
      "moduleName": "perspectiveInterfaceModule",
      "keywords": "overview perspectiveinterfacemodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "perspectiveInterfaceModule.service:PerspectiveServiceInterface",
      "shortName": "PerspectiveServiceInterface",
      "type": "service",
      "moduleName": "perspectiveInterfaceModule",
      "keywords": "activated activating active actives allowed application associated bound changed configuration consists cookie crossframeeventservice crossframeeventservicemodule current currently currently-selected data deactivates deactivating default description descriptioni18nkey determine disable disabled disablingcallback enabled enabling enablingcallback event event_perspective_changed exising feature features flag functions hasactiveperspective i18n identified identifies indicates invoked isemptyperspectiveactive key list method methods_register mode namei18nkey names object optional overlay parameter permission permissions permitted perspective perspectiveinterfacemodule perspectives prespective preview published referenced refresh refreshed refreshperspective register registered registers registry replaying represents respective returns se seconstantsmodule selectdefault selected selects service set smartedit smartedit-perspectives smarteditcontainer stored stores switches switchto system tooltip translated true uniquely user web"
    },
    {
      "section": "smartEditContainer",
      "id": "previewDataDropdownPopulatorModule.service:PreviewDatapreviewCatalogDropdownPopulator",
      "shortName": "PreviewDatapreviewCatalogDropdownPopulator",
      "type": "service",
      "moduleName": "previewDataDropdownPopulatorModule",
      "shortDescription": "implementation of DropdownPopulatorInterface for catalog dropdown in",
      "keywords": "additional attributes based call catalog catalogs displayed dropdown dropdownpopulatorinterface dropdownpopulatorinterfacemodule experience field implementation list making method model payload populate previewdatadropdownpopulatormodule promise resolving rest retrieve returns selector service site sites smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "PreviewDatalanguageDropdownPopulator.service:PreviewDatalanguageDropdownPopulator",
      "shortName": "PreviewDatalanguageDropdownPopulator",
      "type": "service",
      "moduleName": "PreviewDatalanguageDropdownPopulator",
      "shortDescription": "implementation of DropdownPopulatorInterface for language dropdown in",
      "keywords": "additional assigned attributes call catalog descriptor dropdown dropdownpopulatorinterface dropdownpopulatorinterfacemodule experience field generated genericeditor genericeditormodule implementation langauges language languages list making method model payload populate previewdatalanguagedropdownpopulator promise resolving rest retrieve returns selected selector service site smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "previewErrorInterceptorModule.service:previewErrorInterceptor",
      "shortName": "previewErrorInterceptor",
      "type": "service",
      "moduleName": "previewErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 400 from the Preview API when the pageId is not found in the context. The request will be replayed without the pageId.",
      "keywords": "api code context error http pageid preview previewerrorinterceptormodule replayed request service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "previewTicketInterceptorModule.previewTicketInterceptor",
      "shortName": "previewTicketInterceptorModule.previewTicketInterceptor",
      "type": "service",
      "moduleName": "previewTicketInterceptorModule",
      "shortDescription": "A HTTP request interceptor that adds the preview ticket to the HTTP header object before a request is made.",
      "keywords": "$httpprovider adding adds array called config configuration current dependencies extracts factories factory header holds http injected interceptor interceptors method methods modified object preview previewticketinterceptor previewticketinterceptormodule property registered request resource returns service smarteditcontainer ticket url x-preview-ticket"
    },
    {
      "section": "smartEditContainer",
      "id": "recompileDomModule",
      "shortName": "recompileDomModule",
      "type": "overview",
      "moduleName": "recompileDomModule",
      "shortDescription": "This module defines the recompileDom component.",
      "keywords": "component defines directive module overview recompiledom recompiledommodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "recompileDomModule.directive:recompileDom",
      "shortName": "recompileDom",
      "type": "directive",
      "moduleName": "recompileDomModule",
      "shortDescription": "The recompile dom directive accepts a function param, and can be applied to any part of the dom.",
      "keywords": "$timeout accepts angular applied content contents directive dom execution function inner invoked outer param recompile recompiled recompiledom recompiledommodule recompiling scope smarteditcontainer transcluded trigger"
    },
    {
      "section": "smartEditContainer",
      "id": "renderServiceInterfaceModule",
      "shortName": "renderServiceInterfaceModule",
      "type": "overview",
      "moduleName": "renderServiceInterfaceModule",
      "shortDescription": "The renderServiceInterfaceModule",
      "keywords": "abstract accelerator component components data designed displays extensible interface module operation overview performed re-render render renderservice renderserviceinterfacemodule service smarteditcontainer update"
    },
    {
      "section": "smartEditContainer",
      "id": "renderServiceInterfaceModule.service:RenderServiceInterface",
      "shortName": "RenderServiceInterface",
      "type": "service",
      "moduleName": "renderServiceInterfaceModule",
      "shortDescription": "Designed to re-render components after an update component data operation has been performed, according to",
      "keywords": "$compile accelerator backend blocked blockrendering boolean class compilation component componentid components componenttype content correctly current custom customcontent data decorators decoratorservicemodule designed determines displayed displays error executed extended flag frontend html indicates indicator instantiated interface isblocked isrenderingblocked method methods_storeprecompiledcomponent note object operation optional original overlay performed position positioned promise propagate re-render re-rendered re-renders refreshoverlaydimensions reject rejected remove removed removes render rendercomponent rendered rendering renderpage renderremoval renderserviceinterfacemodule renderslots replace requires resets resolve returns saved serves service showoverlay slot slotids slotsids smarteditcontainer stack storefront success time toggleoverlay toggles type update updated updates values visibility wrapping"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CATALOGS_PATH",
      "shortName": "CATALOGS_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the catalogs",
      "keywords": "catalogs object path resourcelocationsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CMSWEBSERVICES_PATH",
      "shortName": "CMSWEBSERVICES_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Regular expression identifying CMS related URIs",
      "keywords": "cms expression identifying object regular resourcelocationsmodule smarteditcontainer uris"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CMSWEBSERVICES_RESOURCE_URI",
      "shortName": "CMSWEBSERVICES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant for the cmswebservices API root",
      "keywords": "api cmswebservices constant object resourcelocationsmodule root smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CONFIGURATION_COLLECTION_URI",
      "shortName": "CONFIGURATION_COLLECTION_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "The SmartEdit configuration collection API root",
      "keywords": "api collection configuration object resourcelocationsmodule root smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CONFIGURATION_URI",
      "shortName": "CONFIGURATION_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "the name of the SmartEdit configuration API root",
      "keywords": "api configuration object resourcelocationsmodule root smartedit smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG",
      "shortName": "CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog uid placeholder in URLs",
      "keywords": "catalog constant object placeholder resourcelocationsmodule smarteditcontainer uid urls"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_CATALOG_VERSION",
      "shortName": "CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the catalog version placeholder in URLs",
      "keywords": "catalog constant object placeholder resourcelocationsmodule smarteditcontainer urls version"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:CONTEXT_SITE_ID",
      "shortName": "CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the site uid placeholder in URLs",
      "keywords": "constant object placeholder resourcelocationsmodule site smarteditcontainer uid urls"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:DEFAULT_AUTHENTICATION_CLIENT_ID",
      "shortName": "DEFAULT_AUTHENTICATION_CLIENT_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "The default OAuth 2 client id to use during authentication.",
      "keywords": "authentication client default oauth object resourcelocationsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:DEFAULT_AUTHENTICATION_ENTRY_POINT",
      "shortName": "DEFAULT_AUTHENTICATION_ENTRY_POINT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "When configuration is not available yet to provide authenticationMap, one needs a default authentication entry point to access configuration API itself",
      "keywords": "access api authentication authenticationmap configuration default entry object point provide resourcelocationsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:ENUM_RESOURCE_URI",
      "shortName": "ENUM_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to fetch list of values of a given enum type",
      "keywords": "enum fetch list object path resourcelocationsmodule smarteditcontainer type values"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:I18N_LANGUAGE_RESOURCE_URI",
      "shortName": "I18N_LANGUAGE_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to fetch the supported i18n languages.",
      "keywords": "fetch i18n languages object resource resourcelocationsmodule smarteditcontainer supported uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:I18N_RESOURCE_URI",
      "shortName": "I18N_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI to fetch the i18n initialization map for a given locale.",
      "keywords": "fetch i18n initialization locale map object resource resourcelocationsmodule smarteditcontainer uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:LANDING_PAGE_PATH",
      "shortName": "LANDING_PAGE_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the landing page",
      "keywords": "landing object path resourcelocationsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:LANGUAGE_RESOURCE_URI",
      "shortName": "LANGUAGE_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the languages REST service.",
      "keywords": "languages object resource resourcelocationsmodule rest service smarteditcontainer uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:MEDIA_PATH",
      "shortName": "MEDIA_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the media",
      "keywords": "media object path resourcelocationsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:MEDIA_RESOURCE_URI",
      "shortName": "MEDIA_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the media REST service.",
      "keywords": "media object resource resourcelocationsmodule rest service smarteditcontainer uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG",
      "shortName": "PAGE_CONTEXT_CATALOG",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog uid placeholder in URLs",
      "keywords": "catalog constant current object placeholder resourcelocationsmodule smarteditcontainer uid urls"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION",
      "shortName": "PAGE_CONTEXT_CATALOG_VERSION",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page catalog version placeholder in URLs",
      "keywords": "catalog constant current object placeholder resourcelocationsmodule smarteditcontainer urls version"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID",
      "shortName": "PAGE_CONTEXT_SITE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Constant containing the name of the current page site uid placeholder in URLs",
      "keywords": "constant current object placeholder resourcelocationsmodule site smarteditcontainer uid urls"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PageUriContext",
      "shortName": "PageUriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives for a given page.",
      "keywords": "catalog cms directives map object page_context_catalog page_context_catalog_version page_context_site_id resourcelocationsmodule services site smarteditcontainer uid version"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PERMISSIONSWEBSERVICES_RESOURCE_URI",
      "shortName": "PERMISSIONSWEBSERVICES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path to fetch permissions of a given type",
      "keywords": "fetch object path permissions resourcelocationsmodule smarteditcontainer type"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:PREVIEW_RESOURCE_URI",
      "shortName": "PREVIEW_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the preview ticket API",
      "keywords": "api object path preview resourcelocationsmodule smarteditcontainer ticket"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:SITES_RESOURCE_URI",
      "shortName": "SITES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the sites REST service.",
      "keywords": "object resource resourcelocationsmodule rest service sites smarteditcontainer uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:SMARTEDIT_RESOURCE_URI_REGEXP",
      "shortName": "SMARTEDIT_RESOURCE_URI_REGEXP",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "to calculate platform domain URI, this regular expression will be used",
      "keywords": "calculate domain expression object platform regular resourcelocationsmodule smarteditcontainer uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:SMARTEDIT_ROOT",
      "shortName": "SMARTEDIT_ROOT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "the name of the webapp root context",
      "keywords": "context object resourcelocationsmodule root smarteditcontainer webapp"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:STORE_FRONT_CONTEXT",
      "shortName": "STORE_FRONT_CONTEXT",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "to fetch the store front context for inflection points.",
      "keywords": "context fetch front inflection object points resourcelocationsmodule smarteditcontainer store"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:STOREFRONT_PATH",
      "shortName": "STOREFRONT_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the storefront",
      "keywords": "object path resourcelocationsmodule smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:STOREFRONT_PATH_WITH_PAGE_ID",
      "shortName": "STOREFRONT_PATH_WITH_PAGE_ID",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the storefront with a page ID",
      "keywords": "object path resourcelocationsmodule smarteditcontainer storefront"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:STRUCTURES_RESOURCE_URI",
      "shortName": "STRUCTURES_RESOURCE_URI",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Resource URI of the component structures REST service.",
      "keywords": "component object resource resourcelocationsmodule rest service smarteditcontainer structures uri"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:SYNC_PATH",
      "shortName": "SYNC_PATH",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Path of the synchronization service",
      "keywords": "object path resourcelocationsmodule service smarteditcontainer synchronization"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.object:UriContext",
      "shortName": "UriContext",
      "type": "object",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "A map that contains the necessary site and catalog information for CMS services and directives.",
      "keywords": "catalog cms context_catalog context_catalog_version context_site_id directives map object resourcelocationsmodule services site smarteditcontainer uid version"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceLocationsModule.resourceLocationToRegex",
      "shortName": "resourceLocationsModule.resourceLocationToRegex",
      "type": "service",
      "moduleName": "resourceLocationsModule",
      "shortDescription": "Generates a regular expresssion matcher from a given resource location URL, replacing predefined keys by wildcard",
      "keywords": "$httpbackend backend endpoint endpointregex example expresssion generates hits http location match matcher matchers mocked passed predefined regex regular replacing resource resourcelocationsmodule resourcelocationtoregex respond service smarteditcontainer someresource somevalue url var whenget wildcard"
    },
    {
      "section": "smartEditContainer",
      "id": "resourceNotFoundErrorInterceptorModule.service:resourceNotFoundErrorInterceptor",
      "shortName": "resourceNotFoundErrorInterceptor",
      "type": "service",
      "moduleName": "resourceNotFoundErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 404 (Not Found) except for an HTML or a language resource. It will display the response.message in an alert message.",
      "keywords": "alert code display error html http language message resource resourcenotfounderrorinterceptormodule response service smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "restServiceFactoryModule",
      "shortName": "restServiceFactoryModule",
      "type": "overview",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "The restServiceFactoryModule",
      "keywords": "factory generate module overview providing resource rest restservicefactorymodule service smarteditcontainer url wrapper"
    },
    {
      "section": "smartEditContainer",
      "id": "restServiceFactoryModule.service:ResourceWrapper",
      "shortName": "ResourceWrapper",
      "type": "service",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "The ResourceWrapper is service used to make REST calls to the wrapped target URI. It is created",
      "keywords": "angularjs appends based call called calls component components created deletes evaluate evaluates factory getbyid https identifier list loads mapped match matching method methods_get object objects org parameters params payload placeholder placeholders promise provided query rejected rejecting rejects remove resolves resolving resource resourcewrapper rest restservicefactory restservicefactorymodule returned returns save saved saves search searchparams server service single smarteditcontainer string target unique update updated updates uri verify warpper wrapped wrapper"
    },
    {
      "section": "smartEditContainer",
      "id": "restServiceFactoryModule.service:restServiceFactory",
      "shortName": "restServiceFactory",
      "type": "service",
      "moduleName": "restServiceFactoryModule",
      "shortDescription": "A factory used to generate a REST service wrapper for a given resource URL, providing a means to perform HTTP",
      "keywords": "$resource angularjs appended argument automatically best context-wide create created default delegated domain factory forward generate http https identifier location method multiple object operations opposed optional org parameter paths payload perform placeholder points post prefix prefixed prepended provided providing reference relative remain resource resourceid rest restservicefactory restservicefactorymodule retrieved returned separate service services set setdomain slash smarteditcontainer split target uri uris url working wrapped wrapper wraps"
    },
    {
      "section": "smartEditContainer",
      "id": "retryInterceptorModule",
      "shortName": "retryInterceptorModule",
      "type": "overview",
      "moduleName": "retryInterceptorModule",
      "shortDescription": "This module provides the functionality to retry failing xhr requests through the registration of retry strategies.",
      "keywords": "failing functionality module overview registration requests retry retryinterceptormodule smarteditcontainer strategies xhr"
    },
    {
      "section": "smartEditContainer",
      "id": "retryInterceptorModule.service:retryInterceptor",
      "shortName": "retryInterceptor",
      "type": "service",
      "moduleName": "retryInterceptorModule",
      "shortDescription": "The retryInterceptor provides the functionality to register a set of predicates with their associated retry strategies.",
      "keywords": "angular argument associated attemptcount boolean calculatenextdelay canretry controller custominterceptorexample custompredicate delay determine examplecontroller fails false find firstfastretry function functionality http httpobj instanciated js match matching method module operation_context operationcontext operationcontextservicemodule predicate predicates prototype register remain request response retried retries retry retryinterceptor retryinterceptormodule retrystrategyinterface retrystrategyinterfacemodule return run-time service set smarteditcontainer status strategies strategy strategyholder subject takes time tooling true var"
    },
    {
      "section": "smartEditContainer",
      "id": "RetryStrategyInterface",
      "shortName": "RetryStrategyInterface",
      "type": "overview",
      "moduleName": "RetryStrategyInterface",
      "keywords": "overview retrystrategyinterface smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "retryStrategyInterfaceModule.service:RetryStrategyInterface",
      "shortName": "RetryStrategyInterface",
      "type": "service",
      "moduleName": "retryStrategyInterfaceModule",
      "keywords": "canretry current delay function method request retried retry retrystrategyinterfacemodule return returns service smarteditcontainer time true"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:ALL_PERSPECTIVE",
      "shortName": "ALL_PERSPECTIVE",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The key of the default All Perspective.",
      "keywords": "default key object perspective seconstantsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_ADDED",
      "shortName": "EVENT_PERSPECTIVE_ADDED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when a new perspective (known as mode for users) is registered.",
      "keywords": "event mode object perspective registered seconstantsmodule smarteditcontainer triggered users"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_CHANGED",
      "shortName": "EVENT_PERSPECTIVE_CHANGED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when the perspective (known as mode for users) is changed.",
      "keywords": "changed event mode object perspective seconstantsmodule smarteditcontainer triggered users"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_REFRESHED",
      "shortName": "EVENT_PERSPECTIVE_REFRESHED",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when the perspective (known as mode for users) is refreshed.",
      "keywords": "event mode object perspective refreshed seconstantsmodule smarteditcontainer triggered users"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:EVENT_PERSPECTIVE_UNLOADING",
      "shortName": "EVENT_PERSPECTIVE_UNLOADING",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The ID of the event that is triggered when a perspective is about to be unloaded.",
      "keywords": "disabled event features object perspective seconstantsmodule smarteditcontainer triggered unloaded"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:NONE_PERSPECTIVE",
      "shortName": "NONE_PERSPECTIVE",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "The key of the default None Perspective.",
      "keywords": "default key object perspective seconstantsmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "seConstantsModule.object:VALIDATION_MESSAGE_TYPES",
      "shortName": "VALIDATION_MESSAGE_TYPES",
      "type": "object",
      "moduleName": "seConstantsModule",
      "shortDescription": "Validation message types",
      "keywords": "message object seconstantsmodule smarteditcontainer types validation"
    },
    {
      "section": "smartEditContainer",
      "id": "seDropdownModule.directive:seDropdown",
      "shortName": "seDropdown",
      "type": "directive",
      "moduleName": "seDropdownModule",
      "shortDescription": "This directive generates a custom dropdown (standalone or dependent on another one) for the genericEditor.",
      "keywords": "actual angularjs api array associated attribute attributes based boolean call calling changed cmsstructuretype configured contract custom de default defined depend dependent depends dependson described description determine determining directive display dom dropdown dropdownpopulator dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown edited editorfieldmappingservice editorfieldmappingservicemodule element en example exception false fetch field full generated generates genericeditor genericeditormodule i18nkey i18nkeyforsomequalifier1 i18nkeyforsomequalifier2 i18nkeyforsomequalifier3 idattribute identifier implementation item items itemtemplateurl label labelattributes language list localized map maps menu method mode model object opposed option1 option2 option3 options paged parent path populate populated populating populator propertyeditortemplate propertytype qualifier raised recipe rest retrieved retrieving returned se-dropdown sedropdown sedropdownmodule selection service set smarteditcontainer somepropertytype somequalifier1 somequalifier2 somequalifier3 string structure structures template uri uridropdownpopulator uridropdownpopulatormodule work"
    },
    {
      "section": "smartEditContainer",
      "id": "seDropdownModule.service:SEDropdownService",
      "shortName": "SEDropdownService",
      "type": "service",
      "moduleName": "seDropdownModule",
      "shortDescription": "The SEDropdownService handles the initialization and the rendering of the seDropdown Angular component.",
      "keywords": "angular array asynchronous attributes based component configured currently currentpage directive dropdown dropdownpopulatorinterface dropdownpopulatorinterfacemodule event fetch fetchall fetched fetchentity fetchpage field filter getitem handles implementation init initialization initializes instantiating item items list method number object option options pagesize paging populate populated populator promise publishes rendering resolves returned search sedropdown sedropdownmodule sedropdownservice selected service single smarteditcontainer triggeraction"
    },
    {
      "section": "smartEditContainer",
      "id": "seGenericEditorFieldErrorsModule",
      "shortName": "seGenericEditorFieldErrorsModule",
      "type": "overview",
      "moduleName": "seGenericEditorFieldErrorsModule",
      "shortDescription": "This module provides the seGenericEditorFieldErrors component, which is used to show validation errors.",
      "keywords": "component errors module overview segenericeditorfielderrors segenericeditorfielderrorsmodule smarteditcontainer validation"
    },
    {
      "section": "smartEditContainer",
      "id": "seGenericEditorFieldMessagesModule",
      "shortName": "seGenericEditorFieldMessagesModule",
      "type": "overview",
      "moduleName": "seGenericEditorFieldMessagesModule",
      "shortDescription": "This module provides the seGenericEditorFieldMessages component, which is used to show validation messages like errors or warnings.",
      "keywords": "component errors messages module overview segenericeditorfieldmessages segenericeditorfieldmessagesmodule smarteditcontainer validation warnings"
    },
    {
      "section": "smartEditContainer",
      "id": "seGenericEditorFieldMessagesModule.component:seGenericEditorFieldMessages",
      "shortName": "seGenericEditorFieldMessages",
      "type": "directive",
      "moduleName": "seGenericEditorFieldMessagesModule",
      "shortDescription": "Component responsible for displaying validation messages like errors or warnings",
      "keywords": "actual array code component directive displaying errors field iso language localized messages non-localized object qualifier responsible se-generic-editor-field-messages segenericeditorfieldmessagesmodule smarteditcontainer string validation warnings"
    },
    {
      "section": "smartEditContainer",
      "id": "seValidationErrorParserModule",
      "shortName": "seValidationErrorParserModule",
      "type": "overview",
      "moduleName": "seValidationErrorParserModule",
      "shortDescription": "This module provides the validationErrorsParser service, which is used to parse validation errors for parameters",
      "keywords": "errors format language message module overview parameters parse service sevalidationerrorparsermodule smarteditcontainer validation validationerrorsparser"
    },
    {
      "section": "smartEditContainer",
      "id": "seValidationErrorParserModule.seValidationErrorParser",
      "shortName": "seValidationErrorParserModule.seValidationErrorParser",
      "type": "service",
      "moduleName": "seValidationErrorParserModule",
      "shortDescription": "This service provides the functionality to parse validation errors received from the backend.",
      "keywords": "backend details en error errors expects extra final format function functionality language message method object occurred parse parses received service sevalidationerrorparser sevalidationerrorparsermodule smarteditcontainer somekey someval stripped validation var widescreen"
    },
    {
      "section": "smartEditContainer",
      "id": "seValidationMessageParserModule",
      "shortName": "seValidationMessageParserModule",
      "type": "overview",
      "moduleName": "seValidationMessageParserModule",
      "shortDescription": "This module provides the seValidationMessageParser service, which is used to parse validation messages (errors, warnings)",
      "keywords": "format language message messages module overview parameters parse service sevalidationmessageparser sevalidationmessageparsermodule smarteditcontainer validation warnings"
    },
    {
      "section": "smartEditContainer",
      "id": "seValidationMessageParserModule.seValidationMessageParser",
      "shortName": "seValidationMessageParserModule.seValidationMessageParser",
      "type": "service",
      "moduleName": "seValidationMessageParserModule",
      "shortDescription": "This service provides the functionality to parse validation messages (errors, warnings) received from the backend.",
      "keywords": "backend details en expects extra final format function functionality language message messages method object occurred parse parses received service sevalidationmessageparser sevalidationmessageparsermodule smarteditcontainer somekey someval stripped validation var warning warnings widescreen"
    },
    {
      "section": "smartEditContainer",
      "id": "sharedDataServiceInterfaceModule.SharedDataServiceInterface",
      "shortName": "sharedDataServiceInterfaceModule.SharedDataServiceInterface",
      "type": "service",
      "moduleName": "sharedDataServiceInterfaceModule",
      "shortDescription": "Provides an abstract extensible shared data service. Used to store any data to be used either the SmartEdit",
      "keywords": "abstract application callback class container content convenience data extended extensible fed fetch fly instantiated interface key method modify modifyingcallback object retrieve return serves service set shared shareddataserviceinterface shareddataserviceinterfacemodule smartedit smarteditcontainer store stored update"
    },
    {
      "section": "smartEditContainer",
      "id": "sharedDataServiceModule.sharedDataService",
      "shortName": "sharedDataServiceModule.sharedDataService",
      "type": "service",
      "moduleName": "sharedDataServiceModule",
      "shortDescription": "The Shared Data Service is used to store data that is to be shared between the SmartEdit application and the",
      "keywords": "application container data extends gateway gatewayproxy gatewayproxymodule service share shared shareddata shareddataservice shareddataserviceinterface shareddataserviceinterfacemodule shareddataservicemodule smartedit smarteditcontainer store"
    },
    {
      "section": "smartEditContainer",
      "id": "simpleRetrylModule",
      "shortName": "simpleRetrylModule",
      "type": "overview",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "This module provides the simpleRetry service.",
      "keywords": "module overview service simpleretry simpleretrylmodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "simpleRetrylModule.object:SIMPLE_RETRY_DEFAULT_SETTING",
      "shortName": "SIMPLE_RETRY_DEFAULT_SETTING",
      "type": "object",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "The setting object to be used as default values for retry.",
      "keywords": "default object retry setting simpleretrylmodule smarteditcontainer values"
    },
    {
      "section": "smartEditContainer",
      "id": "simpleRetrylModule.service:simpleRetry",
      "shortName": "simpleRetry",
      "type": "service",
      "moduleName": "simpleRetrylModule",
      "shortDescription": "When used by a retry strategy, this service could provide a simple fixed delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.",
      "keywords": "attemptcount attempts base calculate calculatenextdelay canretry check current delay false fixed functionality interval maxattempt maximum method minbackoff minimum number perform provide request retries retry retryinterval returns service simple simpleretrylmodule smarteditcontainer strategy time true valid"
    },
    {
      "section": "smartEditContainer",
      "id": "siteServiceModule",
      "shortName": "siteServiceModule",
      "type": "overview",
      "moduleName": "siteServiceModule",
      "shortDescription": "The siteServiceModule",
      "keywords": "configured fetches hybris module overview platform service site sites siteservicemodule smarteditcontainer"
    },
    {
      "section": "smartEditContainer",
      "id": "siteServiceModule.service:siteService",
      "shortName": "siteService",
      "type": "service",
      "moduleName": "siteServiceModule",
      "shortDescription": "The Site Service fetches all sites configured on the hybris platform using REST calls to the cmswebservices sites API.",
      "keywords": "api array calls cmswebservices configured descriptor descriptors descriptos fetched fetches getsitebyid getsites hybris list method platform previewurl properties redirecturl rest service site sites siteservicemodule smarteditcontainer uid"
    },
    {
      "section": "smartEditContainer",
      "id": "sliderPanelModule",
      "shortName": "sliderPanelModule",
      "type": "overview",
      "moduleName": "sliderPanelModule",
      "shortDescription": "This module defines the slider panel Angular component and its associated constants and controller.",
      "keywords": "$ctrl add advanced angular applied associated automatically basic btn btn-default calling class component configuration configurations constants content controller data-slider-panel-configuration data-slider-panel-show default define defined defines definition dependency directive display embed example expected function getnewserviceinstance html implementation include instantiate instantiated json merge method methods_getnewserviceinstance modifications module ng-click nganimate object overview overwrite panel panels provide service set settings showsliderpanel slider sliderpanelconfiguration sliderpanelmodule sliderpanelservicemodule sliderpanelshow smartedit smarteditcontainer specific tag template trigger type update visible yourapp yourcontroller ysliderpanel"
    },
    {
      "section": "smartEditContainer",
      "id": "sliderPanelModule.directive:ySliderPanel",
      "shortName": "ySliderPanel",
      "type": "directive",
      "moduleName": "sliderPanelModule",
      "shortDescription": "The ySliderPanel Angular component allows for the dynamic display of any HTML content on a sliding panel.",
      "keywords": "allows angular applied binding component configuration content controller datasliderpanelconfiguration datasliderpanelhide datasliderpanelshow directive display dynamic function hiding html json main object panel shared slider sliderpanelmodule sliding smarteditcontainer trigger ways ysliderpanel"
    },
    {
      "section": "smartEditContainer",
      "id": "sliderPanelModule.object:CSS_CLASSNAMES",
      "shortName": "CSS_CLASSNAMES",
      "type": "object",
      "moduleName": "sliderPanelModule",
      "shortDescription": "This object defines injectable Angular constants that store the CSS class names used in the controller to define the",
      "keywords": "action angular animation applied class common constants container content controller css define defines injectable names object panel prefix property rendered rendering slide slider sliderpanel_animated sliderpanel_slideprefix sliderpanelmodule sliding smarteditcontainer store trigger"
    },
    {
      "section": "smartEditContainer",
      "id": "sliderPanelServiceModule",
      "shortName": "sliderPanelServiceModule",
      "type": "overview",
      "moduleName": "sliderPanelServiceModule",
      "shortDescription": "This module provides a service to initialize and control the rendering of the ySliderPanel Angular component.",
      "keywords": "angular component control directive initialize module overview rendering service sliderpanelmodule sliderpanelservicemodule smarteditcontainer ysliderpanel"
    },
    {
      "section": "smartEditContainer",
      "id": "sliderPanelServiceModule.service:sliderPanelService",
      "shortName": "sliderPanelService",
      "type": "service",
      "moduleName": "sliderPanelServiceModule",
      "shortDescription": "The sliderPanelService handles the initialization and the rendering of the ySliderPanel Angular component.",
      "keywords": "angular applied cancel component configuration container covered css cssselector default dimension directive disabled dismiss displayed displayedbydefault element framework getnewserviceinstance greyed-out handles html indicate indicates initialization inline instance javascript json label method modal nogreyedoutoverlay object onclick overlay overlaydimension panel parent pattern pointer position provided rendering renders resize returns save screen screenresized select service set sets showdismissbutton side slidefrom slider sliderpanelmodule sliderpanelservice sliderpanelservicemodule slides smartedit smarteditcontainer specific specifies styling tag title triggered update updatecontainerinlinestyling values window ysliderpanel z-index zindex"
    },
    {
      "section": "smartEditContainer",
      "id": "storageServiceModule",
      "shortName": "storageServiceModule",
      "type": "overview",
      "moduleName": "storageServiceModule",
      "shortDescription": "The storageServiceModule",
      "keywords": "allows browser module overview service smarteditcontainer storage storageservicemodule storing temporary"
    },
    {
      "section": "smartEditContainer",
      "id": "storageServiceModule.service:storageService",
      "shortName": "storageService",
      "type": "service",
      "moduleName": "storageServiceModule",
      "shortDescription": "The Storage service is used to store temporary information in the browser. The service keeps track of key/value pairs",
      "keywords": "access associated associates authenticate authenticated authentication authtoken authtokens authuri browser content cookie cookiename creates current desired determine doesn encode encoded entry exist flag getauthtoken getauthtokens getprincipalidentifier getvaluefromcookie identified identifies indicates initialized isencoded isinitialized key login map method null obfuscate pairs point principal principalnamekey principalnamevalue principaluid properly provided putvalueincookie remove removeallauthtokens removeauthtoken removed removeprincipalidentifier removes resource retrieve retrieved retrieves service smartedit-sessions smarteditcontainer specifies storage storageservicemodule store storeauthtoken stored storeprincipalidentifier stores temporary token track uid uri uris user"
    },
    {
      "section": "smartEditContainer",
      "id": "systemAlertsModule",
      "shortName": "systemAlertsModule",
      "type": "overview",
      "moduleName": "systemAlertsModule",
      "shortDescription": "systemAlertsModule",
      "keywords": "component overview smarteditcontainer systemalerts systemalertsmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "systemAlertsModule.systemAlerts",
      "shortName": "systemAlertsModule.systemAlerts",
      "type": "directive",
      "moduleName": "systemAlertsModule",
      "shortDescription": "The systemAlerts component provides the view layer for system alerts. It renders alerts triggered both from",
      "keywords": "add alertfactory alertfactorymodule alerts alertservice alertservicemodule application component directive instability instance instances layer lower multiple remove rendered renders root smartedit smarteditcontainer suite system systemalerts systemalertsmodule test triggered unique view web"
    },
    {
      "section": "smartEditContainer",
      "id": "tabsetModule",
      "shortName": "tabsetModule",
      "type": "overview",
      "moduleName": "tabsetModule",
      "shortDescription": "The Tabset module provides the directives required to display a group of tabsets within a tabset. The",
      "keywords": "developers directive directives display displaying group interest module organizing overview required responsible smartedit smarteditcontainer tabs tabset tabsetmodule tabsets ytabset"
    },
    {
      "section": "smartEditContainer",
      "id": "tabsetModule.directive:yTab",
      "shortName": "yTab",
      "type": "directive",
      "moduleName": "tabsetModule",
      "shortDescription": "The directive  responsible for wrapping the content of a tab within a",
      "keywords": "allows called caller content contents custom data determine directive displayed executed extra fragment function functionality html match model modify object optional parameter parse path register responsible scope smartedit-tab smartedit-tabset smarteditcontainer tab tabcontrol tabid tabs tabset tabsetmodule track wrapping ytabset"
    },
    {
      "section": "smartEditContainer",
      "id": "tabsetModule.directive:yTabset",
      "shortName": "yTabset",
      "type": "directive",
      "moduleName": "tabsetModule",
      "shortDescription": "The directive responsible for displaying and organizing tabs within a tabset. A specified number of tabs will",
      "keywords": "allows body called caller changes child clicks configuration content contents custom data defined determine directive display displayed displaying drop-down error executed extra flag fragment function functionality grouped haserrors header headers html indicates item list maximum menu model modify note number numtabsdisplayed object optional organizing parameter parse passed path register remaining responsible scope selected smartedit-tab smartedit-tabset smarteditcontainer tab tabcontrol tabs tabset tabsetmodule tabslist templateurl title track user visual wrapped ytab"
    },
    {
      "section": "smartEditContainer",
      "id": "timerModule",
      "shortName": "timerModule",
      "type": "overview",
      "moduleName": "timerModule",
      "shortDescription": "A module that provides a Timer object that can invoke a callback after a certain period of time.",
      "keywords": "callback invoke module object overview period service smarteditcontainer time timer timermodule"
    },
    {
      "section": "smartEditContainer",
      "id": "timerModule.service:Timer",
      "shortName": "Timer",
      "type": "service",
      "moduleName": "timerModule",
      "shortDescription": "A Timer must be instanciated calling timerService.createTimer().",
      "keywords": "$timeout active additional adds angular angularjs callback calling checking clean createtimer current currently dirty duration false function functions https instanciated instruct internal invoke invoked isactive method milliseconds model number object order org parameter prevent references resetduration restart returns running service set sets skip smarteditcontainer start starts stops teardown thise timeout timer timermodule timerservice true wait wraps"
    },
    {
      "section": "smartEditContainer",
      "id": "toolbarInterfaceModule.ToolbarServiceInterface",
      "shortName": "toolbarInterfaceModule.ToolbarServiceInterface",
      "type": "service",
      "moduleName": "toolbarInterfaceModule",
      "shortDescription": "Provides an abstract extensible toolbar service. Used to manage and perform actions to either the SmartEdit",
      "keywords": "abstract action actions additems aliases application array callback class classes clicked close container content default description descriptioni18nkey determines display dom dropdown extended extensible fonts higher html hybrid_action icon iconclassname icons identified identifier image images include included instantiated interface internal internally item itemkey keepaliveonclose key list lower manage maps method middle named namei18nkey number perform position priority properties provided ranging remove removeitembykey removes sections serves service smartedit smarteditcontainer takes template toolbar toolbarinterfacemodule toolbarserviceinterface translation trigger triggered type unique url urls variable"
    },
    {
      "section": "smartEditContainer",
      "id": "toolbarModule.directive:toolbar",
      "shortName": "toolbar",
      "type": "directive",
      "moduleName": "toolbarModule",
      "shortDescription": "Toolbar HTML mark-up that compiles into a configurable toolbar with an assigned ToolbarService for functionality.",
      "keywords": "assigned classes compiles configurable css cssclass directive folder functionality gateway html imageroot images item mark-up path proxy root service smarteditcontainer space-separated string styling toolbar toolbarmodule toolbarname toolbarservice"
    },
    {
      "section": "smartEditContainer",
      "id": "toolbarModule.string:CLOSE_ALL_ACTION_ITEMS",
      "shortName": "CLOSE_ALL_ACTION_ITEMS",
      "type": "object",
      "moduleName": "toolbarModule",
      "shortDescription": "Injectable angular constant",
      "keywords": "action angular closure constant identifying injectable items object smarteditcontainer string toolbarmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "toolbarModule.ToolbarService",
      "shortName": "toolbarModule.ToolbarService",
      "type": "service",
      "moduleName": "toolbarModule",
      "shortDescription": "The SmartEdit container toolbar service is used to add toolbar items that can perform actions to either",
      "keywords": "action actions add additems additemsstyling adds alias aliases application callback classes clients communication container cross css directive displayed forwarded function gateway gatewayid gatewayproxy gatewayproxymodule iframe items key-name list mapping maps meant method methods methods_additems models pass perform proxy responsible service smartedit smarteditcontainer space-separated specific toolbar toolbarinterfacemodule toolbarmodule toolbarservice toolbarserviceinterface user view"
    },
    {
      "section": "smartEditContainer",
      "id": "toolbarModule.toolbarServiceFactory",
      "shortName": "toolbarModule.toolbarServiceFactory",
      "type": "service",
      "moduleName": "toolbarModule",
      "shortDescription": "The toolbar service factory generates instances of the ToolbarService based on",
      "keywords": "based cached communication corresponding created cross exist exists factory gateway gatewayid gatewayproxy gatewayproxymodule generates gettoolbarservice identifier iframe instance instances method provided respect returns service single singleton smarteditcontainer toolbar toolbarmodule toolbarservice toolbarservicefactory"
    },
    {
      "section": "smartEditContainer",
      "id": "translationServiceModule",
      "shortName": "translationServiceModule",
      "type": "service",
      "moduleName": "translationServiceModule",
      "shortDescription": "This module is used to configure the translate service, the filter, and the directives from the &#39;pascalprecht.translate&#39; package. The configuration consists of:",
      "keywords": "appropriate browser combined configuration configure consists constant directives filter getbrowserlocale i18napiroot i18ninterceptor i18ninterceptormodule initializing languageservice languageservicemodule locale map methods_getbrowserlocale methods_request module object package pascalprecht preferredlanguage replace request retrieved runtime service setting smarteditcontainer time translate translation translationservicemodule unaccessible undefined_locale uri"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule",
      "shortName": "treeModule",
      "type": "overview",
      "moduleName": "treeModule",
      "shortDescription": "This module deals with rendering and management of node trees",
      "keywords": "deals management module node overview rendering smarteditcontainer treemodule trees"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.controller:YTreeController",
      "shortName": "YTreeController",
      "type": "controller",
      "moduleName": "treeModule",
      "shortDescription": "Extensible controller of the ytree directive",
      "keywords": "add affect angular-ui-tree boolean causing checking child children collapse collapseall controller determine directive existing expand expandable expanded expanding extensible fetch fetched getnodebyid handle haschildren https identifier initiated method methods_removenode methods_savenode native newchild newsibling node nodes nodeuid object parent referenced refresh refreshparent remove removed removenode return savenode server service sibling smarteditcontainer status successful time toggle toggleandfetch tree treemodule treeservice ytree"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.directive:ytree",
      "shortName": "ytree",
      "type": "directive",
      "moduleName": "treeModule",
      "shortDescription": "This directive renders a tree of nodes and manages CRUD operations around the nodes.",
      "keywords": "action actions angular-ui-tree arg1 arg2 behaviour callback closure closure-bound controller crud ctrl customize data-ng-click data-node-actions data-node-template-url data-node-uri defined delete directive drag dragoptions drop enhance entry example expecting exposing function functions hereunder html https included instance invoke library manage manages map methods mymethod newchild node nodeactions nodes nodetemplateurl nodeuri object operations optional order parameter parent party passed point post prebound relies rendering renders rest root scope service smarteditcontainer template third tree treemodule treeservice ytree ytreecontroller ytreedndevent"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.object:dragOptions",
      "shortName": "dragOptions",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A JSON object holding callbacks related to nodes drag and drop functionality in the ytree directive.",
      "keywords": "acceptdropcallback allow allows beforedropcallback block callback callbacks confirmation directive drag drop droppable dropped executed exposes false function functionality holding hovering json key modal node nodes object ondropcallback opens property rejects return slots smarteditcontainer treemodule true ytree ytreedndevent"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.object:Node",
      "shortName": "Node",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A plain JSON object, representing the node of a tree managed by the ytree directive.",
      "keywords": "boolean catalog children directive haschildren identifier ignored json localized managed node object optional parent parentuid plain posting property read representing required retrieved saving smarteditcontainer tree treemodule uid unique ytree"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.object:yTreeDndEvent",
      "shortName": "yTreeDndEvent",
      "type": "object",
      "moduleName": "treeModule",
      "shortDescription": "A plain JSON object, representing the event triggered when dragging and dropping nodes in the ytree directive.",
      "keywords": "array children destination destinationnodes directive dragged dragging drop dropped dropping element event handle json location node nodes object parent plain position property representing set siblings smarteditcontainer source sourcenode sourceparenthandle targeted targetparenthandle treemodule triggered ui ytree"
    },
    {
      "section": "smartEditContainer",
      "id": "treeModule.service:TreeService",
      "shortName": "TreeService",
      "type": "service",
      "moduleName": "treeModule",
      "shortDescription": "A class to manage tree nodes through a REST API.",
      "keywords": "api assigned calls child children class create delete entry false fetch fetchchildren final front generated handle haschildren hit initiated list manage manytoone marked method model navigationnodes node node2 node4 nodes nodeuri object parent parentuid payload point post posting property querying re-evaluated reference removenode rest retrieved return returns root save savenode sending server service side smarteditcontainer subsequent support takes tree treemodule treeservice true uid verbs wrapped"
    },
    {
      "section": "smartEditContainer",
      "id": "unauthorizedErrorInterceptorModule.service:unauthorizedErrorInterceptor",
      "shortName": "unauthorizedErrorInterceptor",
      "type": "service",
      "moduleName": "unauthorizedErrorInterceptorModule",
      "shortDescription": "Used for HTTP error code 401 (Forbidden). It will display the login modal.",
      "keywords": "code display error http login modal service smarteditcontainer unauthorizederrorinterceptormodule"
    },
    {
      "section": "smartEditContainer",
      "id": "uriDropdownPopulatorModule.service:uriDropdownPopulator",
      "shortName": "uriDropdownPopulator",
      "type": "service",
      "moduleName": "uriDropdownPopulatorModule",
      "shortDescription": "implementation of DropdownPopulatorInterface for &quot;EditableDropdown&quot; cmsStructureType",
      "keywords": "attribute attributes building call cmsstructuretype comma data defined dependson dropdownpopulatorinterface dropdownpopulatorinterfacemodule editabledropdown fetch fetchall fetched fetchpage field getitem idattribute implementation include item label labelattributes list method model object option options params payload promise query request resolves rest separated service setting smarteditcontainer uri uridropdownpopulatormodule"
    },
    {
      "section": "smartEditContainer",
      "id": "urlServiceInterfaceModule.UrlServiceInterface",
      "shortName": "urlServiceInterfaceModule.UrlServiceInterface",
      "type": "service",
      "moduleName": "urlServiceInterfaceModule",
      "shortDescription": "Provides an abstract extensible url service, Used to open a given URL",
      "keywords": "abstract authentication browser class extended extensible instantiated interface invocation method navigate navigates open opens openurlinpopup path pop serves service smarteditcontainer tab url urlserviceinterface urlserviceinterfacemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "waitDialogServiceModule",
      "shortName": "waitDialogServiceModule",
      "type": "overview",
      "moduleName": "waitDialogServiceModule",
      "shortDescription": "A module that provides a service that can be used to display a loading overlay.",
      "keywords": "display loading module overlay overview service smarteditcontainer waitdialogservicemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "waitDialogServiceModule.service:waitDialogService",
      "shortName": "waitDialogService",
      "type": "service",
      "moduleName": "waitDialogServiceModule",
      "shortDescription": "This service be used in order to display (or hide) a &#39;loading&#39; overlay. The overlay should display on top of everything, preventing",
      "keywords": "action called corresponds dialog display displayed hidden hide i18n key loading message method order overlay preventing removes se service showwaitmodal smarteditcontainer top user wait waitdialogservicemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "wizardServiceModule",
      "shortName": "wizardServiceModule",
      "type": "overview",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The wizardServiceModule",
      "keywords": "add angular called controller create creating dependencies dosomething function getwizardconfig html i18n implement inject methods_open modal modalwizard modalwizardconfig module mymodule myservice mywizardcontroller object open overview passing return returns service services simple smarteditcontainer step1 step2 steps templateurl title wizard wizardmanager wizardservicemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "wizardServiceModule.modalWizard",
      "shortName": "wizardServiceModule.modalWizard",
      "type": "service",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The modalWizard service is used to create wizards that are embedded into the modalService",
      "keywords": "accessible alternate angular angularjs boilerplate cancelled care conf configuration controller controlleras create default embedded feel finished function https implement initialize injected manipulation manual map method modal modalservice modalservicemodule modalwizard modalwizardconfig navigation object open org promise properties rejected resolved returns service simple smarteditcontainer step templates underlying wizard wizardcontroller wizardmanager wizards wizardservicemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "wizardServiceModule.object:ModalWizardConfig",
      "shortName": "ModalWizardConfig",
      "type": "object",
      "moduleName": "wizardServiceModule",
      "shortDescription": "A plain JSON object, representing the configuration options for a modal wizard",
      "keywords": "action array backlabel boolean button callback caller cancel cancellabel close closed configuration continue current default defaults defined donelabel easy enable enabled fired function halt i18n isformvalid json key label load methods_open modal modalwizard nextlabel null object oncancel ondone onnext open opportunity optional options ordered override parameter parameters pass plain promise property receives rejecting representing resolved result resultfn return returned returning returns single smarteditcontainer step steps stopping triggered true wizard wizardservicemodule wizardstepconfig"
    },
    {
      "section": "smartEditContainer",
      "id": "wizardServiceModule.object:WizardStepConfig",
      "shortName": "WizardStepConfig",
      "type": "object",
      "moduleName": "wizardServiceModule",
      "shortDescription": "A plain JSON object, representing the configuration options for a single step in a wizard",
      "keywords": "automatically callback choose configuration displayed easier explicitly generated html i18n identify json key making meaning menu navigation object optional options plain property provide provided reference representing service single smarteditcontainer step template templateurl title top triggered unique url wizard wizardservicemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "wizardServiceModule.WizardManager",
      "shortName": "wizardServiceModule.WizardManager",
      "type": "service",
      "moduleName": "wizardServiceModule",
      "shortDescription": "The Wizard Manager is a wizard management service that can be injected into your wizard controller.",
      "keywords": "additional adds addstep array automatically cancel close containsstep controller controllers creator currently default displayed equal error exists form function generated getcurrentstep getcurrentstepid getcurrentstepindex getstepindexfromid getsteps getstepscount getstepwithid getstepwithindex getwizardconfig gotostepwithid gotostepwithindex injected management manager method methods_getsteps modalwizardconfig navigates newstep note number object passed position promise provided rejected remove removestepbyid removestepbyindex removing resolved result resultfn return returned runtime service size smarteditcontainer step steps true wizard wizardmanager wizardservicemodule wizardstepconfig"
    },
    {
      "section": "smartEditContainer",
      "id": "yActionableSearchItemModule",
      "shortName": "yActionableSearchItemModule",
      "type": "overview",
      "moduleName": "yActionableSearchItemModule",
      "shortDescription": "This module defines the yActionableSearchItem component",
      "keywords": "component defines directive eventservicemodule module overview smarteditcontainer yactionablesearchitem yactionablesearchitemmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yActionableSearchItemModule.directive:yActionableSearchItem",
      "shortName": "yActionableSearchItem",
      "type": "directive",
      "moduleName": "yActionableSearchItemModule",
      "shortDescription": "The yActionableSearchItem Angular component is designed to work with the ySelect drop down. It allows you to add",
      "keywords": "action add allows angular aread button component create designed directive drop drop-down event i18n key label pressed resultsheader se smarteditcontainer systemeventservice trigger triggered user-defined work yactionablesearchitem yactionablesearchitem_action_create yactionablesearchitemmodule yationablesearchitem yselect"
    },
    {
      "section": "smartEditContainer",
      "id": "yCollapsibleContainerModule",
      "shortName": "yCollapsibleContainerModule",
      "type": "overview",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "This module defines the collapsible container Angular component and its associated constants and controller.",
      "keywords": "$yourctrl add angular applied associated basic binded bootstrap collapsible component configuration configurations constants container content controller customize data-configuration define defines dependency directive embed function html implementation include modifications module overview panel passed rendering settings sliderpanelmodule smarteditcontainer tag template title ui variable ycollapsiblecontainer ycollapsiblecontainermodule ylodashmodule yourapp yourcontroller"
    },
    {
      "section": "smartEditContainer",
      "id": "yCollapsibleContainerModule.directive:yCollapsibleContainer",
      "shortName": "yCollapsibleContainer",
      "type": "directive",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "The yCollapsibleContainer Angular component allows for the dynamic display of any HTML content on a collapsible container.",
      "keywords": "_right_ allows angular applied collapsible component configuration container content default directive display displayed dynamic expand-collapse expanded expandedbydefault header html icon iconalignment iconvisible json object rendered smarteditcontainer specifies ycollapsiblecontainer ycollapsiblecontainermodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS",
      "shortName": "COLLAPSIBLE_CONTAINER_CONSTANTS",
      "type": "object",
      "moduleName": "yCollapsibleContainerModule",
      "shortDescription": "This object defines injectable Angular constants that store the default configuration and CSS class names used in the controller to define the rendering and animation of the collapsible container.",
      "keywords": "allowing angular animation applied class classname collapsible configuration constants container controller css css-based default default_configuration define defines defining display displayed expand-collapse expanded expandedbydefault header icon icon_left icon_right iconalignment iconvisible injectable json left names object positioned property rendered rendering smarteditcontainer specifies store ycollapsiblecontainermodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yDropDownMenuModule.directive:yDropDownMenu",
      "shortName": "yDropDownMenu",
      "type": "directive",
      "moduleName": "yDropDownMenuModule",
      "shortDescription": "yDropDownMenu builds a drop-down menu. It has two parameters dropdownItems and selectedItem. The dropdownItems is an array of object which contains an key, condition and callback function. ",
      "keywords": "alert argument array associated bind builds callback called click condition defined directive drop drop-down dropdown dropdownitems edit example false function hide implemented item key menu object open pageeditormodalservice pagelist parameters passed reloadupdatedpage response return returns selecteditem smarteditcontainer sync true uid user ydropdownmenu ydropdownmenumodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yEditableListModule",
      "shortName": "yEditableListModule",
      "type": "overview",
      "moduleName": "yEditableListModule",
      "shortDescription": "The yEditableList module contains a component which allows displaying a list of elements. The items in ",
      "keywords": "allows component displaying elements items list module overview re-ordered removed smarteditcontainer yeditablelist yeditablelistmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yEditableListModule.directive:yEditableList",
      "shortName": "yEditableList",
      "type": "directive",
      "moduleName": "yEditableListModule",
      "shortDescription": "The yEditableList component allows displaying a list of items. The list can be managed dynamically, by ",
      "keywords": "adding allows called changes collection component content directive display displaying dynamically editable function identify items itemtemplateurl list managed modified onchange path property re-ordering refresh removing smarteditcontainer specifies string template track update y-editable-list yeditablelist yeditablelistmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yHelpModule.component:yHelp",
      "shortName": "yHelp",
      "type": "directive",
      "moduleName": "yHelpModule",
      "shortDescription": "This component will generate a help button that will show a customizable popover on top of it when hovering.",
      "keywords": "automatically body button component customizable defined directive exactly generate help hovering html location optional popover relies smarteditcontainer template templateurl title top trusted y-help yhelpmodule ypopover ypopovermodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yInfiniteScrollingModule",
      "shortName": "yInfiniteScrollingModule",
      "type": "overview",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "This module holds the base web component to perform infinite scrolling from paged backend",
      "keywords": "backend base component holds infinite module overview paged perform scrolling smarteditcontainer web yinfinitescrollingmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yInfiniteScrollingModule.directive:yInfiniteScrolling",
      "shortName": "yInfiniteScrolling",
      "type": "directive",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "A component that you can use to implement infinite scrolling for an expanding content (typically with a ng-repeat) nested in it.",
      "keywords": "approaches arguments attached backend bottom call change class close component container content context crosses css currentpage data decide default defaults determined directive distance dropdown dropdownclass dropdowncontainerclass element evaluated example expanding expected expression failure fetch fetched fetching fetchpage fill filters free function handle height implement implementers infinite invoked items large left listens mask max-height maximum meant measured multiples mycontext nested nextpage ng-repeat number object optional overflow overflow-y override pagesize paginated pagination pixels push query re-fetch reach representing requested requests reset resolved restrict return scroll scrolling search server set size smarteditcontainer space starts string tall times triggered type typically yinfinitescrollingmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yInfiniteScrollingModule.object:THROTTLE_MILLISECONDS",
      "shortName": "THROTTLE_MILLISECONDS",
      "type": "object",
      "moduleName": "yInfiniteScrollingModule",
      "shortDescription": "Configures the yInfiniteScrolling directive to throttle the page fetching with the value provided in milliseconds.",
      "keywords": "configures directive fetching milliseconds object provided smarteditcontainer throttle yinfinitescrolling yinfinitescrollingmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yjqueryModule",
      "shortName": "yjqueryModule",
      "type": "overview",
      "moduleName": "yjqueryModule",
      "shortDescription": "This module manages the use of the jQuery library in SmartEdit.",
      "keywords": "enables jquery library manages module noconflict overview smartedit smarteditcontainer storefront version work yjquerymodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yjqueryModule.yjQuery",
      "shortName": "yjqueryModule.yjQuery",
      "type": "object",
      "moduleName": "yjqueryModule",
      "shortDescription": "Expose a jQuery wrapping factory all the while preserving potentially pre-existing jQuery in storefront and smartEditContainer",
      "keywords": "expose factory jquery object pre-existing preserving smarteditcontainer storefront wrapping yjquery yjquerymodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yLoDashModule",
      "shortName": "yLoDashModule",
      "type": "overview",
      "moduleName": "yLoDashModule",
      "shortDescription": "This module manages the use of the lodash library in SmartEdit. It makes sure the library is introduced",
      "keywords": "angular easy introduced library lifecycle lodash manages mock module overview smartedit smarteditcontainer tests unit ylodashmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yLoDashModule.lodash",
      "shortName": "yLoDashModule.lodash",
      "type": "object",
      "moduleName": "yLoDashModule",
      "shortDescription": "Makes the underscore library available to SmartEdit.",
      "keywords": "clash dependency enforce injection libraries library lodash namespace note object order original proper removed smartedit smarteditcontainer storefront underscore window ylodashmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yMessageModule",
      "shortName": "yMessageModule",
      "type": "overview",
      "moduleName": "yMessageModule",
      "shortDescription": "This module provides the yMessage component, which is responsible for rendering contextual",
      "keywords": "actions component contextual feedback messages module overview rendering responsible smarteditcontainer user ymessage ymessagemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yMessageModule.component:yMessage",
      "shortName": "yMessage",
      "type": "directive",
      "moduleName": "yMessageModule",
      "shortDescription": "This component provides contextual feedback messages for the user actions. To provide title and description for the yMessage",
      "keywords": "actions component contextual default description directive elements feedback info message-description message-title messageid messages provide smarteditcontainer success title transcluded type user warning ymessage ymessagemodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopoverModule.directive:yPopover",
      "shortName": "yPopover",
      "type": "directive",
      "moduleName": "yPopoverModule",
      "shortDescription": "This directive attaches a customizable popover on a DOM element.",
      "keywords": "attaches automatically body bottom bottom-right click concatenation customizable default defined directive dom element event exactly format html left location optional outsideclick placement placement1-placement2 popover possibles smarteditcontainer target template templateurl title top trigger trusted type values ypopovermodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopupOverlayModule",
      "shortName": "yPopupOverlayModule",
      "type": "overview",
      "moduleName": "yPopupOverlayModule",
      "shortDescription": "This module provides the yPopupOverlay directive, and it&#39;s helper services",
      "keywords": "directive helper module overview services smarteditcontainer ypopupoverlay ypopupoverlaymodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopupOverlayModule.directive:yPopupOverlay",
      "shortName": "yPopupOverlay",
      "type": "directive",
      "moduleName": "yPopupOverlayModule",
      "shortDescription": "The yPopupOverlay is meant to be a directive that allows popups/overlays to be displayed attached to any element.",
      "keywords": "accepts aligns allows anchor angular appends applied attached body bottom called click clicked configuration contained controls correctly default depending directive displayed dom element executed expression false file halign handle hidden horizontally html implementation initial inner left limitation meant object overlay popup popups positioned positions provided relative scrollable scrolling smarteditcontainer string template templateurl top true url valign values vertically window work ypopupoverlay ypopupoverlaymodule ypopupoverlayonhide ypopupoverlayonshow ypopupoverlaytrigger"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopupOverlayUtilsModule",
      "shortName": "yPopupOverlayUtilsModule",
      "type": "overview",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "This module provides utility services for the yPopupOverlayModule",
      "keywords": "module overview services smarteditcontainer utility ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopupOverlayUtilsModule.service:yPopupOverlayUtilsClickOrderService",
      "shortName": "yPopupOverlayUtilsClickOrderService",
      "type": "service",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "A service that manages the click handlers for all yPopupOverlay overlay DOM elements.",
      "keywords": "case click clicks close delegates directive displayed dom elements execute handlers manages multiple order overlay overlays popup registered reverse service smarteditcontainer stack top user ypopupoverlay ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations",
      "shortName": "yPopupOverlayUtilsDOMCalculations",
      "type": "service",
      "moduleName": "yPopupOverlayUtilsModule",
      "shortDescription": "Contains some yPopupOverlay helper functions for",
      "keywords": "absolutely absposition adjusthorizontaltobeinviewport alignment anchor anchorboundingclientrect based bottom bounding calculatepreferredposition calculates calculating directive dom element functions height helper horizontal horizontally input left location method modifies modify object overlay position positioned positioning positions preferred rectangle representing service side size sizes smarteditcontainer targetheight targetwidth top vertical viewport width ypopupoverlay ypopupoverlaymodule ypopupoverlayutilsmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "ySelectModule",
      "shortName": "ySelectModule",
      "type": "overview",
      "moduleName": "ySelectModule",
      "shortDescription": "The ySelectModule",
      "keywords": "overview smarteditcontainer yselectmodule"
    },
    {
      "section": "smartEditContainer",
      "id": "ySelectModule.directive:ySelect",
      "shortName": "ySelect",
      "type": "directive",
      "moduleName": "ySelectModule",
      "shortDescription": "This component is a wrapper around ui-select directive and provides filtering capabilities for the dropdown menu that is customizable with an item template.",
      "keywords": "access adds arguments boolean button call called capabilities component contract controller controls customizable data-ng-bind-html default defined determine directive disable disablechoicefn disabled display displayed drop-down dropdown elements entities false fetch fetchall fetchentity fetchpage fetchstrategy field filtering flavour force fulfill function functions i18nkey identification identifier identify initialization initialized input internal invoked isreadonly item items itemtemplate keepmodelonreset keepmodelonresetfor label list longer magnifier mask match menu mode model multi-selectable multiselect non-paged object opposed option optional paged parameter passed path placeholder populate print printed promise property provided providing purposes receives remove renders required reset resolving result resultsheaderlabel resultsheadertemplate resultsheadertemplateurl resultsheadertemplateutl return search selected selection server set single smarteditcontainer specifies standard strategy styling template testing time top translate true ui-select values widget work wrapper y-select yinfinitescrolling yinfinitescrollingmodule yselect yselectmodule"
    }
  ],
  "apis": {
    "smartEdit": true,
    "smartEditContainer": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/#/smartEdit",
  "scripts": [
    "angular.min.js"
  ]
};