angular.module('coretemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/common/components/yCollapsibleContainer/accordion-group.html',
    "<div role=\"tab\"\n" +
    "    id=\"{{::headingId}}\"\n" +
    "    aria-selected=\"{{isOpen}}\"\n" +
    "    class=\"yCollapsibleContainer__header\"\n" +
    "    ng-keypress=\"toggleOpen($event)\">\n" +
    "\n" +
    "    <a role=\"button\"\n" +
    "        data-toggle=\"collapse\"\n" +
    "        href\n" +
    "        aria-expanded=\"{{isOpen}}\"\n" +
    "        aria-controls=\"{{::panelId}}\"\n" +
    "        tabindex=\"0\"\n" +
    "        class=\"accordion-toggle yCollapsibleContainer__title\"\n" +
    "        ng-click=\"toggleOpen()\"\n" +
    "        uib-accordion-transclude=\"heading\"\n" +
    "        ng-disabled=\"isDisabled\"\n" +
    "        uib-tabindex-toggle>\n" +
    "        <span uib-accordion-header\n" +
    "            ng-class=\"{'text-muted': isDisabled}\">{{heading}}</span>\n" +
    "    </a>\n" +
    "\n" +
    "    <a class=\"yCollapsibleContainer__icon btn btn-link\"\n" +
    "        data-toggle-id=\"toggle-1\"\n" +
    "        title=\"{{isOpen ? 'se.ycollapsible.action.collapse' : 'se.ycollapsible.action.expand' | translate}}\"\n" +
    "        aria-expanded=\"{{isOpen}}\"\n" +
    "        ng-click=\"toggleOpen()\">\n" +
    "        <span class=\"hyicon hyicon-chevron y-collapsible-icon\"\n" +
    "            data-ng-class=\"{\n" +
    "            'y-collapsible-icon--collapse':isOpen, 'y-collapsible-icon--expand':!isOpen}\"></span>\n" +
    "    </a>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div id=\"{{::panelId}}\"\n" +
    "    aria-labelledby=\"{{::headingId}}\"\n" +
    "    aria-hidden=\"{{!isOpen}}\"\n" +
    "    role=\"tabpanel\"\n" +
    "    class=\"panel-collapse collapse\"\n" +
    "    uib-collapse=\"!isOpen\">\n" +
    "    <div ng-transclude></div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/components/yCollapsibleContainer/yCollapsibleContainer.html',
    "<uib-accordion class=\"y-collapsible\">\n" +
    "    <div uib-accordion-group\n" +
    "        class=\"yCollapsibleContainer__group y-collapsible__group\"\n" +
    "        data-template-url=\"web/common/components/yCollapsibleContainer/accordion-group.html\"\n" +
    "        data-is-open=\"$yCollapsibleContainerCtrl.configuration.expandedByDefault\"\n" +
    "        data-ng-class=\"$yCollapsibleContainerCtrl.getIconRelatedClassname()\">\n" +
    "\n" +
    "        <uib-accordion-heading class=\"y-collapsible__heading\">\n" +
    "            <div class=\"yCollapsibleContainer__title y-collapsible__title\"\n" +
    "                data-ng-transclude=\"collapsible-container-title\"></div>\n" +
    "        </uib-accordion-heading>\n" +
    "\n" +
    "        <div class=\"y-collapsible__content\"\n" +
    "            data-ng-transclude=\"collapsible-container-content\"></div>\n" +
    "\n" +
    "    </div>\n" +
    "</uib-accordion>"
  );


  $templateCache.put('web/common/components/yMessage/yMessage.html',
    "<div message-id=\"{{$ctrl.messageId}}\"\n" +
    "    class=\"{{$ctrl.classes}}\">\n" +
    "    <div class=\"y-message-icon-outer\">\n" +
    "        <span class=\"{{$ctrl.icon}} y-message-icon\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"y-message-text\">\n" +
    "        <div class=\"y-message-info-title\"\n" +
    "            data-ng-transclude=\"messageTitle\">\n" +
    "        </div>\n" +
    "        <div class=\"y-message-info-description\"\n" +
    "            data-ng-transclude=\"messageDescription\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/core/services/loginDialog.html',
    "<div data-ng-cloak\n" +
    "    class=\"se-login\"\n" +
    "    data-ng-class=\"{'se-login--full': !modalController.initialized, 'se-login--modal': modalController.initialized }\">\n" +
    "    <div class=\"se-login--wrapper\">\n" +
    "        <div class=\"se-login--logo-wrapper\"\n" +
    "            data-ng-class=\"{'se-login--logo-wrapper_full': !modalController.initialized}\">\n" +
    "            <img src=\"static-resources/images/SmartEditLogo.svg\"\n" +
    "                class=\"se-login--logo\">\n" +
    "        </div>\n" +
    "        <form autocomplete=\"off\"\n" +
    "            data-ng-submit=\"modalController.submit(loginDialogForm)\"\n" +
    "            class=\"se-login--form\"\n" +
    "            name='loginDialogForm'\n" +
    "            novalidate>\n" +
    "            <div class=\"se-login--auth-message\"\n" +
    "                data-ng-if=\"modalController.initialized\">\n" +
    "                <div data-translate=\"se.logindialogform.reauth.message1\"></div>\n" +
    "                <div data-translate=\"se.logindialogform.reauth.message2\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\"\n" +
    "                data-ng-if=\"loginDialogForm.errorMessage && loginDialogForm.posted && (loginDialogForm.$invalid || loginDialogForm.failed)\">\n" +
    "                <div class=\"alert se-login--alert-error\"\n" +
    "                    id=\"invalidError\"\n" +
    "                    data-translate=\"{{ loginDialogForm.errorMessage }}\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <input class=\"se-login--form-control\"\n" +
    "                    type=\"text\"\n" +
    "                    id=\"username_{{modalController.authURIKey}}\"\n" +
    "                    name=\"username\"\n" +
    "                    placeholder=\"{{ 'se.authentication.form.input.username' | translate}}\"\n" +
    "                    autofocus\n" +
    "                    data-ng-model=\"modalController.auth.username\"\n" +
    "                    required/>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <input class=\"se-login--form-control\"\n" +
    "                    type=\"password\"\n" +
    "                    id='password_{{modalController.authURIKey}}'\n" +
    "                    name='password'\n" +
    "                    placeholder=\"{{ 'se.authentication.form.input.password' | translate}}\"\n" +
    "                    data-ng-model=\"modalController.auth.password\"\n" +
    "                    required/>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <language-selector class=\"se-login-language\"></language-selector>\n" +
    "            </div>\n" +
    "            <button class=\"btn btn-lg btn-primary btn-block se-login--btn\"\n" +
    "                id=\"submit_{{modalController.authURIKey}}\"\n" +
    "                name=\"submit\"\n" +
    "                type=\"submit\"\n" +
    "                data-translate=\"se.authentication.form.button.submit\"></button>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/core/services/waitDialog.html',
    "<div class=\"panel panel-default ySEPanelSpinner\">\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"spinner ySESpinner\">\n" +
    "            <div class=\"spinner-container spinner-container1\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"spinner-container spinner-container2\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"spinner-container spinner-container3\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"ySESpinnerText\"\n" +
    "            data-translate=\"{{ modalController._modalManager.loadingMessage }}\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/alerts/alertsTemplate.html',
    "<div class=\"row-fluid\"\n" +
    "    data-ng-if=\"alerts!=null &amp;&amp; alerts.length>0\">\n" +
    "    <div data-ng-repeat=\"alert in alerts\"\n" +
    "        class=\"ng-class: {'col-xs-12':true, 'alert':true, 'alert-danger':(alert.successful==false),'alert-success':(alert.successful==true)};\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"close\"\n" +
    "            data-ng-hide=\"alert.closeable==false\"\n" +
    "            data-ng-click=\"dismissAlert($index);\">&times;</button>\n" +
    "        <span id=\"alertMsg\">{{alert.message | translate}}</span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/authorization/hasOperationPermissionTemplate.html',
    "<div>\n" +
    "    <div ng-if=\"ctrl.isPermissionGranted\">\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/dropdownMenu/yDropdownMenuTemplate.html',
    "<div class=\"y-dropdown-more-menu\"\n" +
    "    data-uib-dropdown>\n" +
    "    <button class=\"btn btn-link dropdown-toggle\"\n" +
    "        type=\"button\"\n" +
    "        data-uib-dropdown-toggle=\"dropdown\"\n" +
    "        aria-haspopup=\"true\"\n" +
    "        aria-expanded=\"true\">\n" +
    "        <span class=\"hyicon hyicon-more y-dropdown-more-menu__ddlb__icon\"></span>\n" +
    "    </button>\n" +
    "    <ul class=\"dropdown-menu dropdown-menu-right\"\n" +
    "        data-uib-dropdown-menu>\n" +
    "        <li data-ng-repeat='dropdownItem in dropdown.dropdownItems'\n" +
    "            data-ng-if=\"dropdownItem.condition(dropdown.selectedItem)\"><a data-ng-click=\"dropdownItem.callback(dropdown.selectedItem) \">{{dropdownItem.key | translate}}</a></li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/boolean/booleanTemplate.html',
    "<div class=\"ySEBooleanField\">\n" +
    "    <span class=\"y-toggle y-toggle-lg\">\n" +
    "        <input type=\"checkbox\"\n" +
    "            id=\"{{$ctrl.field.qualifier}}-checkbox\"\n" +
    "            class=\"ySEBooleanField__input\"\n" +
    "            placeholder=\"{{$ctrl.field.tooltip| translate}}\"\n" +
    "            name=\"{{$ctrl.field.qualifier}}\"\n" +
    "            data-ng-disabled=\"!$ctrl.field.editable\"\n" +
    "            data-ng-model=\"$ctrl.model[$ctrl.qualifier]\" />\n" +
    "        <label class=\"ySEBooleanField__label\"\n" +
    "            for=\"{{$ctrl.field.qualifier}}-checkbox\"></label>\n" +
    "        <p data-ng-if=\"$ctrl.field.labelText && !$ctrl.model[$ctrl.qualifier]\"\n" +
    "            class=\"ySEBooleanField__text\">{{$ctrl.field.labelText| translate}}</p>\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/dateTimePicker/dateTimePickerTemplate.html',
    "<div class=\"input-group se-date-field\"\n" +
    "    id=\"date-time-picker-{{::name}}\"\n" +
    "    data-ng-show=\"isEditable\">\n" +
    "    <input type=\"text\"\n" +
    "        class=\"form-control se-date-field--input\"\n" +
    "        data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings, 'se-input--is-disabled': !isEditable}\"\n" +
    "        placeholder=\"{{::placeholderText | translate}}\"\n" +
    "        name=\"{{::name}}\" />\n" +
    "    <span class=\"input-group-addon se-date-field--button\"\n" +
    "        data-ng-class=\"{ 'se-date-field--button-has-error': field.hasErrors, 'se-date-field--button-has-warning': field.hasWarnings }\">\n" +
    "        <span class=\"glyphicon glyphicon-calendar se-date-field--button-icon\"></span>\n" +
    "    </span>\n" +
    "</div>\n" +
    "<div class=\"input-group date se-date-field\"\n" +
    "    data-ng-if=\"!isEditable\">\n" +
    "    <input type='text'\n" +
    "        class=\"form-control se-date-field--input\"\n" +
    "        data-ng-class=\"{'se-input--is-disabled': !isEditable}\"\n" +
    "        data-ng-model=\"model\"\n" +
    "        data-date-formatter\n" +
    "        data-ng-disabled=\"true\"\n" +
    "        data-format-type=\"short\">\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/dropdown/seDropdownTemplate.html',
    "<y-select data-ng-if=\"dropdown.initialized\"\n" +
    "    data-id=\"{{dropdown.qualifier}}\"\n" +
    "    data-ng-click=\"dropdown.onClick()\"\n" +
    "    data-placeholder=\"field.placeholder\"\n" +
    "    data-ng-model=\"model[qualifier]\"\n" +
    "    data-on-change=\"dropdown.triggerAction\"\n" +
    "    data-fetch-strategy=\"dropdown.fetchStrategy\"\n" +
    "    data-reset=\"dropdown.reset\"\n" +
    "    data-multi-select=\"dropdown.isMultiDropdown\"\n" +
    "    data-controls=\"dropdown.isMultiDropdown\"\n" +
    "    data-is-read-only=\"!field.editable\"\n" +
    "    data-item-template=\"itemTemplateUrl\" />"
  );


  $templateCache.put('web/common/services/genericEditor/errors/seGenericEditorFieldErrorsTemplate.html',
    "<div data-ng-if=\"ctrl.getFilteredErrors().length > 0\">\n" +
    "    <span data-ng-repeat=\"error in ctrl.getFilteredErrors()\"\n" +
    "        class=\"se-help-block--has-error help-block\">\n" +
    "        {{error | translate}}\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorFieldTemplate.html',
    "<div validation-id=\"{{field.qualifier}}\"\n" +
    "    data-ng-cloak\n" +
    "    class=\"ySEField\">\n" +
    "\n" +
    "    <div data-ng-if=\"field.prefixText\"\n" +
    "        class=\"ySEText ySEFieldPrefix\">{{field.prefixText | translate}}</div>\n" +
    "    <div data-ng-if=\"field.template\"\n" +
    "        class=\"ySEGenericEditorField\"\n" +
    "        data-ng-include=\"field.template\"></div>\n" +
    "\n" +
    "    <se-generic-editor-field-messages data-field=\"field\"\n" +
    "        data-qualifier=\"qualifier\"></se-generic-editor-field-messages>\n" +
    "    <div data-ng-if=\"field.postfixText\"\n" +
    "        class=\"ySEText ySEFieldPostfix\">{{field.postfixText | translate}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorFieldWrapperTemplate.html',
    "<generic-editor-field data-editor=\"model.editor\"\n" +
    "    data-field=\"model.field\"\n" +
    "    data-model=\"model.editor.component[model.field.qualifier]\"\n" +
    "    data-qualifier=\"tabId\"\n" +
    "    data-id=\"id\" />"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorTemplate.html',
    "<div data-ng-cloak\n" +
    "    class=\"ySEGenericEditor\">\n" +
    "    <form name=\"componentForm\"\n" +
    "        novalidate\n" +
    "        data-ng-submit=\"ge.editor.submit(componentForm)\"\n" +
    "        class=\"no-enter-submit\">\n" +
    "        <div class=\"modal-header\"\n" +
    "            data-ng-show=\"modalHeaderTitle\">\n" +
    "            <h4 class=\"modal-title\">{{modalHeaderTitle| translate}}</h4>\n" +
    "        </div>\n" +
    "        <div class=\"ySErow\"\n" +
    "            data-ng-repeat=\"holder in ge.editor.holders\">\n" +
    "            <div class=\"\">\n" +
    "                <label class=\"control-label\"\n" +
    "                    data-ng-class=\"{'required': holder.field.required}\"\n" +
    "                    id=\"{{holder.field.qualifier}}-label\">{{holder.field.i18nKey | lowercase | translate}}\n" +
    "                </label>\n" +
    "\n" +
    "            </div>\n" +
    "            <div class=\"ySEGenericEditorFieldStructure\"\n" +
    "                id=\"{{holder.field.qualifier}}\"\n" +
    "                data-cms-field-qualifier=\"{{holder.field.qualifier}}\"\n" +
    "                data-cms-structure-type=\"{{holder.field.cmsStructureType}}\">\n" +
    "                <localized-element data-ng-if=\"holder.field.localized\"\n" +
    "                    data-model=\"holder\"\n" +
    "                    data-languages=\"ge.editor.languages\"\n" +
    "                    class=\"form-group multi-tabs-editor\"\n" +
    "                    data-input-template=\"genericEditorFieldWrapperTemplate.html\"></localized-element>\n" +
    "                <generic-editor-field data-ng-if=\"!holder.field.localized\"\n" +
    "                    data-editor=\"holder.editor\"\n" +
    "                    data-field=\"holder.field\"\n" +
    "                    data-model=\"ge.editor.component\"\n" +
    "                    data-qualifier=\"holder.field.qualifier\"\n" +
    "                    data-id=\"ge.editor.id\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"ySErow\"\n" +
    "            data-ng-if=\"ge.showNoEditSupportDisclaimer()\">\n" +
    "            <y-message data-type=\"info\"\n" +
    "                id=\"GenericEditor.NoEditingSupportDisclaimer\">\n" +
    "                <message-description>\n" +
    "                    <translate>se.editor.notification.editing.not.supported</translate>\n" +
    "                </message-description>\n" +
    "            </y-message>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"ySEBtnRow modal-footer\"\n" +
    "            data-ng-show=\"(ge.editor.alwaysShowReset || (ge.editor.showReset===true && ge.editor.isDirty()  && ge.editor.isValid())) || (ge.editor.alwaysShowSubmit || (ge.editor.showSubmit===true && ge.editor.isDirty()  && ge.editor.isValid() && componentForm.$valid))\">\n" +
    "            <button type=\"button\"\n" +
    "                id=\"cancel\"\n" +
    "                class=\"btn btn-subordinate\"\n" +
    "                data-ng-if=\"ge.editor.alwaysShowReset || (ge.editor.showReset===true && ge.editor.isDirty() && ge.editor.isValid())\"\n" +
    "                data-ng-click=\"ge.reset()\">{{ge.cancelButtonText| translate}}</button>\n" +
    "            <button type=\"submit\"\n" +
    "                id=\"submit\"\n" +
    "                class=\"btn btn-primary\"\n" +
    "                data-ng-if=\"ge.editor.alwaysShowSubmit || (ge.editor.showSubmit===true && ge.editor.isDirty()  && ge.editor.isValid() && componentForm.$valid)\"\n" +
    "                data-ng-disabled=\"ge.isSubmitDisabled(componentForm)\">{{ge.submitButtonText| translate}}</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/richText/seRichTextFieldTemplate.html',
    "<textarea class=\"form-control\"\n" +
    "    data-ng-class=\"{'has-error': ctrl.field.hasErrors}\"\n" +
    "    name=\"{{ctrl.field.qualifier}}-{{ctrl.qualifier}}\"\n" +
    "    data-ng-disabled=\"!ctrl.field.editable\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-ng-change=\"ctrl.reassignUserCheck()\"></textarea>\n" +
    "<div data-ng-if=\"ctrl.requiresUserCheck()\">\n" +
    "    <input type=\"checkbox\"\n" +
    "        data-ng-model=\"ctrl.field.isUserChecked\" />\n" +
    "    <span class=\"ng-class:{'warning-check-msg':true, 'not-checked':ctrl.editor.hasFrontEndValidationErrors && !ctrl.field.isCheckedByUser}\"\n" +
    "        data-translate=\"se.editor.richtext.check\">{{'se.editor.richtext.check' | translate}}</span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/booleanWrapperTemplate.html',
    "<se-boolean data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-field=\"field\"></se-boolean>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dateTimePickerWrapperTemplate.html',
    "<date-time-picker data-name=\"field.qualifier\"\n" +
    "    data-model=\"model[qualifier]\"\n" +
    "    data-is-editable=\"field.editable\"\n" +
    "    data-field=\"field\"></date-time-picker>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dropdownTemplate.html',
    "<ui-select id=\"{{field.qualifier}}-selector\"\n" +
    "    ng-model=\"model[qualifier]\"\n" +
    "    theme=\"select2\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\"\n" +
    "    search-enabled=\"false\"\n" +
    "    data-dropdown-auto-width=\"false\"\n" +
    "    class=\"se-generic-editor-dropdown\">\n" +
    "    <ui-select-match placeholder=\"Select an option\"\n" +
    "        class=\"se-generic-editor-dropdown__match\">\n" +
    "        <span ng-bind=\"$select.selected.label\"></span>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{field.qualifier}}-list\"\n" +
    "        repeat=\"option in field.options\"\n" +
    "        position=\"down\"\n" +
    "        value=\"{{$selected.selected}}\">\n" +
    "        <span ng-bind=\"option.label\"></span>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dropdownWrapperTemplate.html',
    "<se-dropdown data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></se-dropdown>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/enumTemplate.html',
    "<ui-select id=\"{{field.qualifier}}-selector\"\n" +
    "    ng-model=\"model[qualifier]\"\n" +
    "    theme=\"select2\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    reset-search-input=\"false\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\">\n" +
    "    <ui-select-match placeholder=\"{{'se.genericeditor.dropdown.placeholder' | translate}}\"\n" +
    "        allow-clear=\"true\">\n" +
    "        <span id=\"enum-{{field.qualifier}}\">{{$select.selected.label}}</span>\n" +
    "        <br/>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{field.qualifier}}-list\"\n" +
    "        repeat=\"option.code as option in field.options[qualifier]\"\n" +
    "        refresh=\"editor.refreshOptions(field, qualifier, $select.search)\"\n" +
    "        value=\"{{$select.selected.code}}\"\n" +
    "        refresh-delay=\"0\"\n" +
    "        position=\"down\">\n" +
    "        <small>\n" +
    "            <span>{{option.label}}</span>\n" +
    "        </small>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/errorMessageTemplate.html',
    "<span data-ng-if=\"field.errors\"\n" +
    "    data-ng-repeat=\"error in field.errors | filter:{language:qualifier}\"\n" +
    "    id='validation-error-{{field.qualifier}}-{{qualifier}}-{{$index}}'\n" +
    "    class=\"se-help-block--has-error help-block\">\n" +
    "    {{ error.message | translate }}\n" +
    "</span>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/floatTemplate.html',
    "<input type=\"number\"\n" +
    "    id=\"{{field.qualifier}}-float\"\n" +
    "    class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\"\n" +
    "    data-ng-pattern=\"/^([0-9]+)\\.{1}([0-9]{1,5})?$/\"\n" +
    "    step=\"{{ field.precision }}\" />"
  );


  $templateCache.put('web/common/services/genericEditor/templates/longStringTemplate.html',
    "<textarea class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\"></textarea>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/numberTemplate.html',
    "<input type=\"number\"\n" +
    "    min=\"1\"\n" +
    "    id=\"{{field.qualifier}}-number\"\n" +
    "    class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\" />"
  );


  $templateCache.put('web/common/services/genericEditor/templates/richTextTemplate.html',
    "<se-rich-text-field data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"></se-rich-text-field>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/shortStringTemplate.html',
    "<input type=\"text\"\n" +
    "    id=\"{{field.qualifier}}-shortstring\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    class=\"form-control\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\" />"
  );


  $templateCache.put('web/common/services/genericEditor/validationMessages/seGenericEditorFieldMessagesTemplate.html',
    "<div>\n" +
    "    <div data-ng-if=\"ctrl.errors.length > 0\">\n" +
    "        <span data-ng-repeat=\"error in ctrl.errors\"\n" +
    "            class=\"se-help-block--has-error help-block\">\n" +
    "            {{ error | translate}}\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.warnings.length > 0\">\n" +
    "        <span data-ng-repeat=\"warning in ctrl.warnings\"\n" +
    "            class=\"se-help-block--has-warning help-block\">\n" +
    "            {{ warning | translate }}\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/help/yHelpTemplate.html',
    "<span data-y-popover\n" +
    "    data-title=\"yHelp.title\"\n" +
    "    data-template=\"yHelp.template\"\n" +
    "    data-template-url=\"yHelp.templateUrl\"\n" +
    "    data-placement=\"yHelp.placement\"\n" +
    "    data-trigger=\"yHelp.trigger\">\n" +
    "    <span class=\"hyicon hyicon-help-icon\"></span>\n" +
    "</span>"
  );


  $templateCache.put('web/common/services/infiniteScrolling/yInfiniteScrollingTemplate.html',
    "<div data-ng-attr-id=\"{{scroll.containerId}}\"\n" +
    "    class=\"ySEInfiniteScrolling-container {{scroll.dropDownContainerClass}}\">\n" +
    "    <div class=\"ySEInfiniteScrolling {{scroll.dropDownClass}}\"\n" +
    "        data-infinite-scroll=\"scroll.nextPage()\"\n" +
    "        data-infinite-scroll-disabled=\"scroll.pagingDisabled\"\n" +
    "        data-infinite-scroll-distance=\"scroll.distance\"\n" +
    "        data-infinite-scroll-immediate-check=\"true\"\n" +
    "        data-infinite-scroll-container='scroll.container'>\n" +
    "        <div data-ng-transclude></div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!scroll.pagingDisabled\"\n" +
    "        class=\"panel panel-default panel__ySEInfiniteScrolling\">\n" +
    "        <div class=\"panel-body panel-body__ySEInfiniteScrolling\">\n" +
    "            <div class=\"spinner\">\n" +
    "                <div class=\"spinner-container spinner-container1\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"spinner-container spinner-container2\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"spinner-container spinner-container3\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/languageSelector/languageSelectorTemplate.html',
    "<ui-select id=\"uiSelectToolingLanguage\"\n" +
    "    ng-model=\"ctrl.selectedLanguage\"\n" +
    "    on-select=\"ctrl.setSelectedLanguage($item)\"\n" +
    "    theme=\"select2\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\"\n" +
    "    search-enabled=\"false\"\n" +
    "    data-dropdown-auto-width=\"false\">\n" +
    "    <ui-select-match placeholder=\"{{'se.languageselector.dropdown.placeholder' | translate}}\">\n" +
    "        <span ng-bind=\"ctrl.selectedLanguage.name \"></span>\n" +
    "    </ui-select-match>\n" +
    "\n" +
    "    <ui-select-choices repeat=\"language in ctrl.languages track by language.isoCode\"\n" +
    "        position=\"down \">\n" +
    "        <span ng-bind=\"language.name\"></span>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/list/yEditableListDefaultItemTemplate.html',
    "<div>\n" +
    "    <span>{{node.uid}}</span>\n" +
    "    <y-drop-down-menu data-ng-if=\"ctrl.editable\"\n" +
    "        dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "        selected-item=\"this\"\n" +
    "        class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/list/yEditableListTemplate.html',
    "<div data-ng-class=\"{ 'y-editable-list-disabled': !ctrl.editable }\">\n" +
    "    <ytree remove-default-template=true\n" +
    "        data-root-node-uid=\"ctrl.rootId\"\n" +
    "        data-node-template-url=\"ctrl.itemTemplateUrl\"\n" +
    "        data-node-actions=\"ctrl.actions\"\n" +
    "        data-drag-options=\"ctrl.dragOptions\"\n" +
    "        data-show-as-list=\"true\" />\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/localizedElement/localizedElementTemplate.html',
    "<y-tabset data-model=\"model\"\n" +
    "    tabs-list=\"tabs\"\n" +
    "    data-num-tabs-displayed=\"6\">\n" +
    "</y-tabset>"
  );


  $templateCache.put('web/common/services/modalTemplate.html',
    "<div id=\"y-modal-dialog\">\n" +
    "    <div class=\"modal-header se-modal__header\"\n" +
    "        data-ng-if=\"modalController._modalManager.title\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"close\"\n" +
    "            data-ng-if=\"modalController._modalManager._showDismissButton()\"\n" +
    "            data-ng-click=\"modalController._modalManager._handleDismissButton()\">\n" +
    "            <span class=\"hyicon hyicon-close\"></span>\n" +
    "        </button>\n" +
    "        <h4 class=\"se-nowrap-ellipsis se-modal__header-title\"\n" +
    "            id=\"smartedit-modal-title-{{ modalController._modalManager.title }}\">{{ modalController._modalManager.title | translate }}&nbsp;{{ modalController._modalManager.titleSuffix | translate }}</h4>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\"\n" +
    "        id=\"modalBody\">\n" +
    "        <div data-ng-include=\"modalController.templateUrl\" />\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\"\n" +
    "        data-ng-if=\"modalController._modalManager._hasButtons()\">\n" +
    "        <span data-ng-repeat=\"button in modalController._modalManager.getButtons()\">\n" +
    "            <button id='{{ button.id }}'\n" +
    "                type=\"button\"\n" +
    "                data-ng-disabled=\"button.disabled\"\n" +
    "                data-ng-class=\"{ 'btn':true, 'btn-subordinate':button.style=='default', 'btn-primary':button.style=='primary' }\"\n" +
    "                data-ng-click=\"modalController._modalManager._buttonPressed(button)\">{{ button.label | translate }}</button>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/popover/yPopoverTemplate.html',
    "<div class=\"help-control\"\n" +
    "    data-uib-popover-html=\"ypop.template\"\n" +
    "    data-uib-popover-template=\"ypop.templateUrl\"\n" +
    "    data-popover-title=\"{{ypop.title | translate}}\"\n" +
    "    data-popover-append-to-body=\"true\"\n" +
    "    data-popover-placement=\"{{ypop.placement}}\"\n" +
    "    data-popover-trigger=\"'{{ypop.trigger}}'\"\n" +
    "    data-popover-is-open=\"ypop.isOpen\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\">\n" +
    "    <div data-ng-transclude\n" +
    "        class=\"popoverAnchor\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/select/defaultItemTemplate.html',
    "<span class=\"y-select-default-item\"\n" +
    "    data-ng-bind-html=\"item.label | l10n | translate\"></span>"
  );


  $templateCache.put('web/common/services/select/uiSelectChoicesTemplate.html',
    "<ul tabindex=\"-1\"\n" +
    "    class=\"ui-select-choices ui-select-choices-content select2-results\">\n" +
    "    <li class=\"ui-select-choices-group\"\n" +
    "        ng-class=\"{'select2-result-with-children': $select.choiceGrouped($group) }\">\n" +
    "        <div ng-show=\"$select.choiceGrouped($group)\"\n" +
    "            class=\"ui-select-choices-group-label select2-result-label\"\n" +
    "            ng-bind=\"$group.name\"></div>\n" +
    "        <ul role=\"listbox\"\n" +
    "            id=\"ui-select-choices-{{ $select.generatedId }}\"\n" +
    "            ng-class=\"{'select2-result-sub': $select.choiceGrouped($group), 'select2-result-single': !$select.choiceGrouped($group) }\">\n" +
    "            <div data-ng-if=\"ySelect.bindingCompleted\">\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplate\"\n" +
    "                    data-compile-html=\"ySelect.resultsHeaderTemplate\"></div>\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplateUrl\"\n" +
    "                    data-ng-include=\"ySelect.resultsHeaderTemplateUrl\"></div>\n" +
    "            </div>\n" +
    "            <li class=\"y-infinite-scrolling__listbox-header\"\n" +
    "                data-ng-show=\"ySelect.showResultHeader()\">{{ ySelect.resultsHeaderLabel | translate }}</li>\n" +
    "            <li role=\"option\"\n" +
    "                ng-attr-id=\"ui-select-choices-row-{{ $select.generatedId }}-{{$index}}\"\n" +
    "                class=\"ui-select-choices-row\"\n" +
    "                ng-class=\"{'select2-highlighted': $select.isActive(this), 'select2-disabled': $select.isDisabled(this)}\">\n" +
    "                <div class=\"select2-result-label ui-select-choices-row-inner\"></div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/common/services/select/uiSelectPagedChoicesTemplate.html',
    "<ul tabindex=\"-1\"\n" +
    "    class=\"ui-select-choices ui-select-choices-content select2-results ui-select-choices__infinite-scrolling\">\n" +
    "    <li class=\"ui-select-choices-group\"\n" +
    "        data-ng-class=\"{'select2-result-with-children': $select.choiceGrouped($group) }\">\n" +
    "        <div data-ng-show=\"$select.choiceGrouped($group)\"\n" +
    "            class=\"ui-select-choices-group-label select2-result-label\"\n" +
    "            data-ng-bind=\"$group.name\">\n" +
    "        </div>\n" +
    "        <y-infinite-scrolling data-ng-if=\"$select.open\"\n" +
    "            data-drop-down-class=\"\"\n" +
    "            data-page-size=\"10\"\n" +
    "            data-mask=\"$select.search\"\n" +
    "            data-fetch-page=\"ySelect.fetchStrategy.fetchPage\"\n" +
    "            data-context=\"ySelect\">\n" +
    "            <ul role=\"listbox\"\n" +
    "                id=\"ui-select-choices-{{ $select.generatedId }}\"\n" +
    "                data-ng-class=\"{'select2-result-sub': $select.choiceGrouped($group), 'select2-result-single': !$select.choiceGrouped($group) }\">\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplate\"\n" +
    "                    data-compile-html=\"ySelect.resultsHeaderTemplate\"></div>\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplateUrl\"\n" +
    "                    data-ng-include=\"ySelect.resultsHeaderTemplateUrl\"></div>\n" +
    "                <li class=\"y-infinite-scrolling__listbox-header\"\n" +
    "                    data-ng-show=\"ySelect.showResultHeader()\">{{ ySelect.resultsHeaderLabel | translate }}</li>\n" +
    "                <li role=\"option\"\n" +
    "                    data-ng-attr-id=\"ui-select-choices-row-{{ $select.generatedId }}-{{$index}}\"\n" +
    "                    class=\"ui-select-choices-row\"\n" +
    "                    data-ng-class=\"{'select2-highlighted': $select.isActive(this), 'select2-disabled': $select.isDisabled(this)}\">\n" +
    "                    <div class=\"select2-result-label ui-select-choices-row-inner\">\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </y-infinite-scrolling>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/common/services/select/yMultiSelectTemplate.html',
    "<ui-select id=\"{{ySelect.id}}-selector\"\n" +
    "    multiple\n" +
    "    sortable=\"true\"\n" +
    "    theme=\"<%= theme %>\"\n" +
    "    data-ng-model=\"ySelect.model\"\n" +
    "    style=\"width:100%\"\n" +
    "    data-ng-change=\"ySelect.internalOnChange()\"\n" +
    "    ng-disabled=\"ySelect.isReadOnly\"\n" +
    "    class=\"se-generic-editor-dropdown\"\n" +
    "    data-ng-class=\"{'se-generic-editor-dropdown__paged':ySelect.requiresPaginatedStyling()}\">\n" +
    "    <ui-select-match class=\"se-generic-editor-dropdown__match se-generic-editor-multiple-dropdown__match\"\n" +
    "        placeholder=\"{{ySelect.placeholder || 'se.genericeditor.sedropdown.placeholder' | translate}}\">\n" +
    "        <item-printer id=\"{{ySelect.id}}-selected\"\n" +
    "            data-model=\"$item\"\n" +
    "            data-template-url=\"ySelect.itemTemplate\"\n" +
    "            class=\"\"\n" +
    "            data-ng-class=\"{'se-generic-editor-dropdown__item-printer':ySelect.hasControls()}\"> </item-printer>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{ySelect.id}}-list\"\n" +
    "        \"<%= filtering %>\"\n" +
    "        position=\"down\">\n" +
    "        <div data-ng-include=\"ySelect.itemTemplate\"></div>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/select/ySelectExtensions/yActionableSearchItemTemplate.html',
    "<div class=\"se-actionable-search-item\"\n" +
    "    data-ng-if=\"$ctrl.getInputText()\">\n" +
    "\n" +
    "    <div class=\"se-actionable-search-item__name\">{{ $ctrl.getInputText() }}</div>\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn se-actionable-search-item__create-btn pull-right\"\n" +
    "        data-ng-show=\"$ctrl.showForm()\"\n" +
    "        data-ng-click=\"$ctrl.buttonPressed()\">\n" +
    "        {{ $ctrl.getActionText() | translate }}\n" +
    "    </button>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/select/ySelectTemplate.html',
    "<ui-select id=\"{{ySelect.id}}-selector\"\n" +
    "    data-ng-model=\"ySelect.model\"\n" +
    "    style=\"width:100%\"\n" +
    "    theme=\"<%= theme %>\"\n" +
    "    search-enabled=\"ySelect.searchEnabled\"\n" +
    "    data-ng-change=\"ySelect.internalOnChange()\"\n" +
    "    data-ng-class=\"{'se-generic-editor-dropdown__paged':ySelect.requiresPaginatedStyling()}\"\n" +
    "    class=\"se-generic-editor-dropdown\"\n" +
    "    ng-disabled=\"ySelect.isReadOnly\">\n" +
    "    <ui-select-match placeholder=\"{{ySelect.placeholder || 'se.genericeditor.sedropdown.placeholder' | translate}}\"\n" +
    "        class=\"se-generic-editor-dropdown__match2\">\n" +
    "        <span data-ng-if=\"ySelect.hasControls()\"\n" +
    "            class=\"input-group-addon glyphicon glyphicon-search ySESearchIcon__navigation\"></span>\n" +
    "        <item-printer id=\"{{ySelect.id}}-selected\"\n" +
    "            data-model=\"$select.selected\"\n" +
    "            data-template-url=\"ySelect.itemTemplate\"\n" +
    "            class=\"y-select-item-printer\"\n" +
    "            data-ng-class=\"{'se-generic-editor-dropdown__item-printer':ySelect.hasControls()}\"> </item-printer>\n" +
    "        <span data-ng-if=\"ySelect.hasControls()\"\n" +
    "            class=\"input-group-addon glyphicon glyphicon-remove-sign ySESearchIcon__navigation\"\n" +
    "            data-ng-click=\"ySelect.clear($select, $event)\"></span>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{ySelect.id}}-list\"\n" +
    "        \"<%= filtering %>\"\n" +
    "        position=\"down\"\n" +
    "        ui-disable-choice=\"ySelect.disableChoice(item)\">\n" +
    "        <div data-ng-include=\"ySelect.itemTemplate\"></div>\n" +
    "    </ui-select-choices>\n" +
    "    <ui-select-no-choice data-ng-if=\"ySelect.noResultLabel\"\n" +
    "        class=\"y-select-no-result-label\">\n" +
    "        {{ySelect.noResultLabel | translate}}\n" +
    "    </ui-select-no-choice>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/sliderPanel/sliderPanelTemplate.html',
    "<div class=\"sliderpanel-container\"\n" +
    "    ng-class=\"::{'sliderpanel--noGreyedOutOverlay': $sliderPanelCtrl.sliderPanelConfiguration.noGreyedOutOverlay }\"\n" +
    "    ng-style=\"{'height': $sliderPanelCtrl.inlineStyling.container.height, 'width': $sliderPanelCtrl.inlineStyling.container.width,  'left': $sliderPanelCtrl.inlineStyling.container.left, 'top': $sliderPanelCtrl.inlineStyling.container.top, 'z-index': $sliderPanelCtrl.inlineStyling.container.zIndex }\"\n" +
    "    ng-if=\"$sliderPanelCtrl.isShown\">\n" +
    "\n" +
    "    <div class=\"sliderpanel-content {{ ::$sliderPanelCtrl.slideClassName }}\"\n" +
    "        ng-style=\"{{ ::$sliderPanelCtrl.inlineStyling.content }}\">\n" +
    "\n" +
    "        <div class=\"sliderpanel-header\"\n" +
    "            ng-show=\"{{ ::$sliderPanelCtrl.sliderPanelConfiguration.modal }}\">\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn-sliderpanel__close\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.showDismissButton\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelDismissAction()\">\n" +
    "                <span class=\"hyicon hyicon-close\"></span>\n" +
    "            </button>\n" +
    "\n" +
    "            <h4 class=\"h4__sliderpanel\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.title\">{{ $sliderPanelCtrl.sliderPanelConfiguration.modal.title | translate }}</h4>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"sliderpanel-body\"\n" +
    "            ng-transclude></div>\n" +
    "\n" +
    "        <div class=\"sliderpanel-footer\"\n" +
    "            data-ng-if=\"($sliderPanelCtrl.sliderPanelConfiguration.modal.cancel || $sliderPanelCtrl.sliderPanelConfiguration.modal.save)\">\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn btn-lg btn-subordinate sliderpanel-footer__button\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.cancel\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.cancel.onClick()\">\n" +
    "                {{ $sliderPanelCtrl.sliderPanelConfiguration.modal.cancel.label | translate }}\n" +
    "            </button>\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn btn-lg btn-default sliderpanel-footer__button\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.save\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.save.onClick()\"\n" +
    "                data-ng-disabled=\"$sliderPanelCtrl.isSaveDisabled()\">\n" +
    "                {{ $sliderPanelCtrl.sliderPanelConfiguration.modal.save.label | translate }}\n" +
    "            </button>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/tabset/yTabsetTemplate.html',
    "<div>\n" +
    "    <ul class=\"nav nav-tabs  multi-tabs\">\n" +
    "        <li ng-if=\"tabsList.length!=numTabsDisplayed\"\n" +
    "            data-ng-repeat=\"tab in tabsList.slice( 0, numTabsDisplayed-1 ) track by $index\"\n" +
    "            data-ng-class=\"{'active': tab.active }\"\n" +
    "            data-tab-id=\"{{tab.id}}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"!tab.message\">{{tab.title | translate}}</a>\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"tab.message\"\n" +
    "                data-popover=\"{{tab.message}}\"\n" +
    "                data-popover-trigger=\"mouseenter\">{{tab.title | translate}}</a>\n" +
    "        </li>\n" +
    "        <li ng-if=\"tabsList.length==numTabsDisplayed\"\n" +
    "            data-ng-repeat=\"tab in tabsList.slice( 0, numTabsDisplayed ) track by $index\"\n" +
    "            data-ng-class=\"{'active': tab.active }\"\n" +
    "            data-tab-id=\"{{tab.id}}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"!tab.message\">{{tab.title | translate}}</a>\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"tab.message\"\n" +
    "                data-popover=\"{{tab.message}}\"\n" +
    "                data-popover-trigger=\"mouseenter\">{{tab.title | translate}}</a>\n" +
    "        </li>\n" +
    "        <li data-ng-if=\"tabsList.length > numTabsDisplayed\"\n" +
    "            class=\"more-tab\"\n" +
    "            data-ng-class=\"{'active': isActiveInMoreTab()}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': dropDownHasErrors()}\"\n" +
    "                class=\"dropdown-toggle\"\n" +
    "                data-toggle=\"dropdown\">\n" +
    "                <span data-ng-if=\"!isActiveInMoreTab()\"\n" +
    "                    class=\"multi-tabs__more-span\"\n" +
    "                    data-translate=\"se.ytabset.tabs.more\"></span>\n" +
    "                <span data-ng-if=\"isActiveInMoreTab()\"\n" +
    "                    class=\"multi-tabs__more-span\">{{selectedTab.title | translate}}</span>\n" +
    "                <span class=\"caret\"></span>\n" +
    "            </a>\n" +
    "            <ul class=\"dropdown-menu\">\n" +
    "                <li ng-if=\"tabsList.length!=numTabsDisplayed\"\n" +
    "                    data-ng-repeat=\"tab in tabsList.slice( numTabsDisplayed-1)\"\n" +
    "                    data-tab-id=\"{{tab.id}}\">\n" +
    "                    <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                        data-ng-click=\"selectTab(tab)\">{{tab.title | translate}}</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "\n" +
    "    <div>\n" +
    "        <y-tab data-ng-repeat=\"tab in tabsList\"\n" +
    "            ng-show=\"tab.active\"\n" +
    "            data-tab-id=\"{{tab.id}}\"\n" +
    "            content=\"tab.templateUrl\"\n" +
    "            data-model=\"model\"\n" +
    "            tab-control=\"tabControl\">\n" +
    "        </y-tab>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/tree/treeNodeRendererTemplate.html',
    "<div ui-tree-handle\n" +
    "    class=\"tree-node tree-node-content ySETreeNodeDesktop clearfix\"\n" +
    "    data-ng-class=\"{ hovered: node.mouseHovered }\"\n" +
    "    data-ng-mouseenter=\"ctrl.onNodeMouseEnter(node)\"\n" +
    "    data-ng-mouseleave=\"ctrl.onNodeMouseLeave(node)\">\n" +
    "\n" +
    "    <div data-ng-show=\"!ctrl.showAsList\"\n" +
    "        class=\"col-xs-1 text-center\">\n" +
    "        <a class=\"ySETreeNodeDesktop__expander btn btn-link btn-sm\"\n" +
    "            data-ng-if=\"ctrl.hasChildren(this)\"\n" +
    "            data-ng-disabled=\"ctrl.isDisabled\"\n" +
    "            data-ng-click=\"ctrl.toggleAndfetch(this)\">\n" +
    "            <span class=\"glyphicon\"\n" +
    "                data-ng-class=\"{'glyphicon-chevron-down': collapsed,'glyphicon-chevron-up': !collapsed}\"></span>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-show=\"ctrl.displayDefaultTemplate()\"\n" +
    "        class=\"ySETreeNodeDesktop__node-name col-xs-5\">\n" +
    "        <span>{{node.name | l10n}}</span>\n" +
    "        <h6 data-ng-show=\"node.title\">{{node.title | l10n}}\n" +
    "        </h6>\n" +
    "    </div>\n" +
    "    <div data-ng-include=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-ng-if=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-include-replace></div>\n" +
    "\n" +
    "</div>\n" +
    "<ol data-ui-tree-nodes=\"\"\n" +
    "    data-ng-model=\"node.nodes\"\n" +
    "    data-ng-class=\"{hidden: collapsed}\"\n" +
    "    class=\"ySETreeNodeDesktop__ol\">\n" +
    "    <li data-ng-repeat=\"node in node.nodes\"\n" +
    "        data-ui-tree-node\n" +
    "        data-ng-include=\"'treeNodeRendererTemplate.html'\">\n" +
    "    </li>\n" +
    "</ol>"
  );


  $templateCache.put('web/common/services/tree/treeTemplate.html',
    "<div data-ui-tree=\"ctrl.treeOptions.callbacks\"\n" +
    "    data-drag-enabled=\"ctrl.treeOptions.dragEnabled\"\n" +
    "    data-drag-delay=\"ctrl.treeOptions.dragDelay\"\n" +
    "    class=\"categoryTable ySENavigationTree\">\n" +
    "\n" +
    "    <div class=\"ySENavigationTree-body\">\n" +
    "        <div class=\"mobileLayout clearfix visible-xs \">\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"desktopLayout clearfix visible-sm visible-md visible-lg \">\n" +
    "\n" +
    "            <ol data-ui-tree-nodes=\"\"\n" +
    "                data-ng-model=\"ctrl.root.nodes\"\n" +
    "                class=\"ySENavigationTree-body__nodes\">\n" +
    "\n" +
    "                <li data-ng-if=\"ctrl.root.nodes.length === 0\"\n" +
    "                    class=\"ySENavigationTree__nodata\"\n" +
    "                    data-translate=\"se.ytree.no.nodes.to.display\"></li>\n" +
    "\n" +
    "                <li class=\"ySENavigationTree-body__nodes__item\"\n" +
    "                    data-ng-repeat=\"node in ctrl.root.nodes\"\n" +
    "                    data-ui-tree-node\n" +
    "                    data-ng-include=\"'treeNodeRendererTemplate.html'\"></li>\n" +
    "            </ol>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/wizard/modalWizardNavBarTemplate.html',
    "<div class=\"modal-wizard-template\">\n" +
    "    <div class=\"modal-wizard-template__steps\">\n" +
    "        <div data-ng-repeat=\"action in wizardController._wizardContext.navActions track by action.id\"\n" +
    "            class=\"modal-wizard-template-step\">\n" +
    "            <button id=\"{{ action.id }}\"\n" +
    "                data-ng-class=\"{ 'btn modal-wizard-template-step__action': true, 'modal-wizard-template-step__action__enabled': action.enableIfCondition(), 'modal-wizard-template-step__action__disabled': !action.enableIfCondition(), 'modal-wizard-template-step__action__current': action.isCurrentStep() }\"\n" +
    "                data-ng-click=\"wizardController.executeAction(action)\"\n" +
    "                data-ng-disabled=\"!action.enableIfCondition()\">{{ action.i18n | translate }}\n" +
    "            </button>\n" +
    "            <span data-ng-if=\"!$last\"\n" +
    "                class=\"\"\n" +
    "                data-ng-class=\"{ 'modal-wizard-template-step__glyph-enabled':  action.enableIfCondition(), 'modal-wizard-template-step__glyph-disabled': !action.enableIfCondition()}\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-repeat=\"step in wizardController._wizardContext._steps track by step.id\">\n" +
    "        <div data-ng-show=\"step.templateUrl === wizardController._wizardContext.templateUrl\"\n" +
    "            class=\"modal-wizard-template-content\"\n" +
    "            data-ng-include=\"step.templateUrl\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/wizard/modalWizardTemplate.html',
    "<div>\n" +
    "    <div id=\"yModalWizard\"\n" +
    "        data-ng-include=\"wizardController._wizardContext.templateOverride || wizardController._wizardContext.templateUrl\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/landingPage/landingPage.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"se-landing-page\">\n" +
    "    <span class=\"se-landing-page-title\"\n" +
    "        data-translate='se.landingpage.title' />\n" +
    "\n" +
    "    <div class=\"se-landing-page-site-selection\"\n" +
    "        data-ng-if=\"landingCtl.model != undefined\">\n" +
    "        <se-dropdown data-field=\"landingCtl.field\"\n" +
    "            data-qualifier=\"landingCtl.qualifier\"\n" +
    "            data-model=\"landingCtl.model\"\n" +
    "            data-id=\"landingCtl.sites_id\"></se-dropdown>\n" +
    "    </div>\n" +
    "\n" +
    "    <span class=\"se-landing-page-label\"\n" +
    "        data-translate='se.landingpage.label' />\n" +
    "\n" +
    "    <div class=\"se-landing-page-catalogs\">\n" +
    "        <div class=\"se-landing-page-catalog\"\n" +
    "            data-ng-repeat=\"catalog in landingCtl.catalogs\">\n" +
    "            <catalog-details data-catalog=\"catalog\"\n" +
    "                data-is-catalog-for-current-site=\"$last\"></catalog-details>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/hotkeys/perspectiveSelectorHotkey/perspectiveSelectorHotkeyNotificationTemplate.html',
    "<y-hotkey-notification data-hotkey-names=\"['esc']\"\n" +
    "    data-title=\"'se.cms.application.status.hotkey.title' | translate\"\n" +
    "    data-message=\"'se.cms.application.status.hotkey.message' | translate\">\n" +
    "</y-hotkey-notification>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/hotkeys/yHotkeyNotificationTemplate.html',
    "<div class=\"y-notification__hotkey\">\n" +
    "    <div data-ng-repeat=\"key in $ctrl.hotkeyNames\"\n" +
    "        data-ng-if=\"$ctrl.hotkeyNames\">\n" +
    "        <div class=\"y-notification__hotkey--key\">\n" +
    "            <span>{{key}}</span>\n" +
    "        </div>\n" +
    "        <span class=\"y-notification__hotkey__icon--add\"\n" +
    "            data-ng-if=\"!$last\">+</span>\n" +
    "    </div>\n" +
    "    <div class=\"y-notification__hotkey--text\">\n" +
    "        <div class=\"y-notification__hotkey--title\">{{$ctrl.title}}</div>\n" +
    "        <div class=\"y-notification__hotkey--message\">{{$ctrl.message}}</div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/yNotificationPanelTemplate.html',
    "<div class=\"y-notification-panel\"\n" +
    "    data-ng-if=\"$ctrl.getNotifications().length > 0\"\n" +
    "    data-ng-hide=\"$ctrl.isMouseOver\"\n" +
    "    data-ng-mouseenter=\"$ctrl.onMouseEnter()\">\n" +
    "    <y-notification data-ng-repeat=\"notification in $ctrl.getNotifications()\"\n" +
    "        data-notification=\"notification\">\n" +
    "    </y-notification>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/yNotificationTemplate.html',
    "<div class=\"y-notification\"\n" +
    "    id=\"{{ $ctrl.id }}\"\n" +
    "    data-ng-if=\"$ctrl.notification\">\n" +
    "    <div data-ng-if=\"$ctrl.hasTemplate()\"\n" +
    "        data-ng-bind-html=\"$ctrl.notification.template\"></div>\n" +
    "    <div data-ng-if=\"$ctrl.hasTemplateUrl()\"\n" +
    "        data-ng-include=\"$ctrl.notification.templateUrl\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/systemAlerts/systemAlertsTemplate.html',
    "<!--<div class=\"row-fluid alert-overlay\">-->\n" +
    "<div class=\"alert-overlay\"\n" +
    "    data-ng-if=\"$ctrl.getAlerts().length > 0\">\n" +
    "    <div uib-alert\n" +
    "        id=\"{{ 'system-alert-' + $index }}\"\n" +
    "        data-ng-repeat=\"alert in $ctrl.getAlerts()\"\n" +
    "        data-ng-class=\"['alert-' + (alert.type || 'info'), {'alert-item--closeable': alert.closeable}]\"\n" +
    "        class=\"alert-item\"\n" +
    "        data-close=\"alert.hide()\">\n" +
    "        <div class=\"alert-item--icon\">\n" +
    "            <span class=\"alert-item--icon-span hyicon\"\n" +
    "                data-ng-class=\"$ctrl.getIconType(alert.type)\"></span>\n" +
    "        </div>\n" +
    "        <div class=\"alert-item--content\">\n" +
    "            <span id=\"{{ 'close-alert-' + $index }}\"\n" +
    "                data-ng-if=\"alert.message\">\n" +
    "                {{ alert.message | translate:alert.messagePlaceholders }}\n" +
    "            </span>\n" +
    "            <div data-ng-if=\"alert.controller\"\n" +
    "                data-ng-controller=\"alert.controller as $alertInjectedCtrl\">\n" +
    "                <div data-ng-if=\"alert.template\"\n" +
    "                    data-compile-html=\"alert.template\"></div>\n" +
    "                <div data-ng-if=\"alert.templateUrl\"\n" +
    "                    data-ng-include=\"alert.templateUrl\"></div>\n" +
    "            </div>\n" +
    "            <div data-ng-if=\"!alert.controller\">\n" +
    "                <div data-ng-if=\"alert.template\"\n" +
    "                    data-compile-html=\"alert.template\"></div>\n" +
    "                <div data-ng-if=\"alert.templateUrl\"\n" +
    "                    data-ng-include=\"alert.templateUrl\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/experienceSelectorButton/experienceSelectorButtonTemplate.html',
    "<div class=\"ySEpreview\"\n" +
    "    data-uib-dropdown\n" +
    "    data-is-open=\"status.isopen\"\n" +
    "    data-auto-close=\"disabled\">\n" +
    "    <div class=\"btn yWebsiteSelectBtn\"\n" +
    "        type=\"button\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        id=\"experience-selector-btn\"\n" +
    "        data-ng-click=\"resetExperienceSelector()\">\n" +
    "        <span class=\"yWebsiteSelectBtn--info\">\n" +
    "            <span class=\"yWebsiteSelectBtn--label\"\n" +
    "                title=\"{{$ctrl.buildExperienceText()}}\"\n" +
    "                data-translate=\"se.experience.selector.previewing\"></span>\n" +
    "            <span class=\"yWebsiteSelectBtn--text-wrapper\">\n" +
    "                <span class=\"yWebsiteSelectBtn--globe\"\n" +
    "                    data-ng-if=\"$ctrl.iscurrentPageFromParent\"\n" +
    "                    data-y-popover\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-placement=\"'bottom'\"\n" +
    "                    data-template=\"$ctrl.parentCatalogVersion\">\n" +
    "                    <span class=\"hyicon hyicon-globe yWebsiteSelectBtn--globe--icon\"></span>\n" +
    "                </span>\n" +
    "                <span title=\"{{$ctrl.buildExperienceText()}}\"\n" +
    "                    class=\"yWebsiteSelectBtn--text se-nowrap-ellipsis\">{{$ctrl.buildExperienceText()}}</span>\n" +
    "                <span class=\"yWebsiteSelectBtn--arrow hyicon hyicon-arrow \"></span>\n" +
    "            </span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"dropdown-menu bottom btn-block yDdResolution \"\n" +
    "        role=\"menu\">\n" +
    "        <experience-selector data-experience=\"$ctrl.experience\"\n" +
    "            data-dropdown-status=\"status\"\n" +
    "            data-reset-experience-selector=\"resetExperienceSelector\"></experience-selector>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/inflectionPointSelectorWidget/inflectionPointSelectorWidgetTemplate.html',
    "<div data-uib-dropdown\n" +
    "    is-open=\"status.isopen\"\n" +
    "    auto-close=\"disabled\"\n" +
    "    id=\"inflectionPtDropdown\">\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn btn-default yDddlbResolution\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        ng-disabled=\"disabled \"\n" +
    "        aria-pressed=\"false\">\n" +
    "        <img data-ng-src=\"{{imageRoot}}{{currentPointSelected.icon}}\"\n" +
    "            data-ng-typeSelected=\"{{currentPointSelected.type}}\" />\n" +
    "    </button>\n" +
    "    <ul class=\"dropdown-menu bottom btn-block yDdResolution \"\n" +
    "        role=\"menu\">\n" +
    "        <li ng-repeat=\"choice in points\"\n" +
    "            class=\"item text-center\">\n" +
    "            <a href\n" +
    "                data-ng-click=\"selectPoint(choice);\"\n" +
    "                data-ng-class=\"{'inflection-point-dropdown--option': currentPointSelected.type !== choice.type, 'inflection-point-dropdown--selected':currentPointSelected.type === choice.type}\">\n" +
    "                <img data-ng-show=\"currentPointSelected.type !== choice.type\"\n" +
    "                    data-ng-src=\"{{imageRoot}}{{choice.icon}}\"\n" +
    "                    class=\"file\" />\n" +
    "                <img data-ng-show=\"currentPointSelected.type === choice.type\"\n" +
    "                    data-ng-src=\"{{imageRoot}}{{choice.selectedIcon}}\"\n" +
    "                    class=\"file\" />\n" +
    "            </a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/perspectiveSelectorWidget/perspectiveSelectorHotkeyTooltipTemplate.html',
    "<div class=\"ySEPerspectiveSelector__hotkey-tooltip\">\n" +
    "    <span data-translate=\"se.hotkey.tooltip\"\n" +
    "        class=\"ySEPerspectiveSelector__hotkey-tooltip--info\"></span>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/perspectiveSelectorWidget/perspectiveSelectorWidgetTemplate.html',
    "<div data-uib-dropdown\n" +
    "    is-open=\"$ctrl.isOpen\"\n" +
    "    auto-close=\"disabled\"\n" +
    "    class=\"ySEPerspectiveSelector\">\n" +
    "\n" +
    "    <a type=\"button\"\n" +
    "        class=\"btn btn-default yHybridAction\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        ng-disabled=\"disabled \"\n" +
    "        aria-pressed=\"false\">\n" +
    "\n" +
    "        <span id=\"hotkeyTooltip\"\n" +
    "            data-ng-if=\"$ctrl.isHotkeyTooltipVisible() && !$ctrl.isOpen\"\n" +
    "            data-y-popover\n" +
    "            data-placement=\"'bottom'\"\n" +
    "            data-template-url=\"'perspectiveSelectorHotkeyTooltipTemplate.html'\"\n" +
    "            data-trigger=\"'hover'\">\n" +
    "            <span class=\"hyicon hyicon-info-icon ySEPerspectiveSelector__hotkey-tooltip--icon\"></span>\n" +
    "        </span>\n" +
    "        <span class=\"ySEPerspectiveSelector__btn-text\">{{$ctrl.getActivePerspectiveName() | translate}}</span>\n" +
    "        <span class=\"hyicon hyicon-arrow\"></span>\n" +
    "    </a>\n" +
    "    <ul class=\"dropdown-menu bottom btn-block ySEPerspectiveList\"\n" +
    "        role=\"menu\">\n" +
    "        <li ng-repeat=\"choice in $ctrl.getDisplayedPerspectives()\"\n" +
    "            class=\"item ySEPerspectiveList--item\"\n" +
    "            data-ng-click=\"$ctrl.selectPerspective(choice.key)\">\n" +
    "            <a href\n" +
    "                data-ng-click=\"$ctrl.selectPerspective(choice.key);\">{{choice.name | translate}}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/deviceSupportTemplate.html',
    "<inflection-point-selector data-ng-if=\"isOnStorefront()\"></inflection-point-selector>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/experienceSelectorWrapperTemplate.html',
    "<experience-selector-button data-ng-if=\"isOnStorefront()\"></experience-selector-button>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/leftToolbarTemplate.html',
    "<div class=\"se-left-menu\">\n" +
    "    <nav class=\"se-left-nav\"\n" +
    "        ng-show=\"showLeftMenu\">\n" +
    "\n" +
    "        <ul class=\"se-left-nav__section se-left-nav__section--top\">\n" +
    "            <li class=\"se-left-nav__section__close\">\n" +
    "                <a href=\"#\"\n" +
    "                    class=\"se-left-nav__section__close--link\"\n" +
    "                    data-ng-click=\"closeLeftToolbar($event)\">\n" +
    "                    <span class=\"hyicon hyicon-close > se-left-nav__section__close--icon\"></span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <p class=\"se-user-name\">{{username | uppercase}}</p>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <a class=\"se-sign-out__link\"\n" +
    "                    data-ng-click=\"signOut($event)\">{{'se.left.toolbar.sign.out' | translate}}</a>\n" +
    "            </li>\n" +
    "\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <language-selector class=\"se-left-nav__section--top__language\"></language-selector>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "\n" +
    "        <ul data-ng-show=\"!showLevel2\"\n" +
    "            class=\"se-left-nav__section se-left-nav__section--level1\"\n" +
    "            id=\"hamburger-menu-level1\">\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <a class=\"se-left-nav__link\"\n" +
    "                    data-ng-click=\"showSites()\">{{'se.left.toolbar.sites' | translate}}</a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section__list-item\"\n" +
    "                has-operation-permission=\"configurationCenterReadPermissionKey\">\n" +
    "                <a id=\"configurationCenter\"\n" +
    "                    class=\"se-left-nav__link\"\n" +
    "                    data-ng-click=\"showCfgCenter($event)\">{{'se.left.toolbar.configuration.center' | translate}}\n" +
    "                    <span class=\"hyicon hyicon-chevron\"></span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "        <ul data-ng-show=\"showLevel2\"\n" +
    "            class=\"se-left-nav__section se-left-nav__section--level2\"\n" +
    "            id=\"hamburger-menu-level2\">\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <a data-ng-click=\"goBack()\"\n" +
    "                    class=\"se-left-nav__link\">\n" +
    "                    <span class=\"hyicon hyicon-back\"></span>\n" +
    "                    <span class=\"\">{{'se.left.toolbar.back' | translate}}</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <general-configuration class=\"se-left-nav__link\" />\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </nav>\n" +
    "\n" +
    "    <div class=\"navbar-header se-menu-button-wrapper\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"navbar-toggle se-menu-button__toggle\"\n" +
    "            id=\"nav-expander\"\n" +
    "            data-ng-click=\"showToolbar($event) \">\n" +
    "            <span class=\"sr-only \">{{'se.left.toolbar.toggle.navigation' | translate}}</span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/leftToolbarWrapperTemplate.html',
    "<left-toolbar data-image-root=\"imageRoot\"></left-toolbar>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/logoTemplate.html',
    "<div class=\"ySmartEditAppLogo \"\n" +
    "    alt=\"Smart Edit \" />\n" +
    "\n" +
    "<svg xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "    width=\"142\"\n" +
    "    height=\"25\"\n" +
    "    viewBox=\"0 0 142 25\">\n" +
    "    <defs>\n" +
    "        <style>\n" +
    "        .cls-1 {\n" +
    "            fill: #fff;\n" +
    "            fill-rule: evenodd;\n" +
    "        }\n" +
    "        </style>\n" +
    "    </defs>\n" +
    "    <path id=\"LOGO\"\n" +
    "        class=\"cls-1\"\n" +
    "        d=\"M115.056,33.546a30.4,30.4,0,0,1,1.458-9.4l3.809-.185V21l-5.972.278-0.376.88A31.674,31.674,0,0,0,112,33.546a31.674,31.674,0,0,0,1.975,11.389l0.376,0.926L120.275,46V43.037l-3.808-.093A30.782,30.782,0,0,1,115.056,33.546Zm9.968,2.268c0.189-.37.377-0.787,0.565-1.2a58.774,58.774,0,0,0,2.351-6.065l-3.2-.093c-0.329.88-.846,2.315-1.457,3.75-0.612-1.481-1.082-2.87-1.364-3.75l-3.15.139a52.838,52.838,0,0,0,2.257,6.065c0.8,1.713,1.787,3.565,2.445,4.768l3.432-.046C126.952,39.333,126.012,37.713,125.024,35.815Zm7.994-13.657-0.329-.88L126.717,21v2.963l3.809,0.185a30.964,30.964,0,0,1,1.457,9.4,32.976,32.976,0,0,1-1.41,9.4l-3.809.093V46l5.925-.139,0.376-.926a31.674,31.674,0,0,0,1.975-11.389A32.286,32.286,0,0,0,133.018,22.157Zm20.406,3.518a9.73,9.73,0,0,1,5.831,1.713l-1.646,2.454a7.951,7.951,0,0,0-4.09-1.3,2.607,2.607,0,0,0-1.74.556,1.657,1.657,0,0,0-.659,1.389,1.222,1.222,0,0,0,.424,1.019,5.041,5.041,0,0,0,1.5.694l2.116,0.556c2.962,0.787,4.42,2.454,4.42,5.046a5.1,5.1,0,0,1-1.975,4.167,8.087,8.087,0,0,1-5.266,1.574,12.179,12.179,0,0,1-5.971-1.528l1.316-2.685a10.646,10.646,0,0,0,4.8,1.389c2.069,0,3.056-.787,3.056-2.315,0-1.065-.752-1.806-2.3-2.222l-1.927-.509a5.385,5.385,0,0,1-3.433-2.176,5.048,5.048,0,0,1-.752-2.593,4.714,4.714,0,0,1,1.74-3.8A7.327,7.327,0,0,1,153.424,25.676Zm9.31,8.8a7.068,7.068,0,0,0-.47-3.194l3.056-.787a4.667,4.667,0,0,1,.471,1.25,4.522,4.522,0,0,1,3.15-1.25,3.609,3.609,0,0,1,2.633,1.019c0.141,0.139.282,0.324,0.517,0.6a5.023,5.023,0,0,1,3.668-1.62,3.831,3.831,0,0,1,2.68.926,3.878,3.878,0,0,1,.846,2.917v8.889h-3.291V34.935a5.086,5.086,0,0,0-.094-1.2,0.906,0.906,0,0,0-.94-0.556,3.6,3.6,0,0,0-2.257,1.065v8.982h-3.2V35.074a4.238,4.238,0,0,0-.141-1.343,0.968,0.968,0,0,0-1.034-.6,3.365,3.365,0,0,0-2.21.972v9.12h-3.292v-8.75h-0.094Zm21.018,0.046L182.3,32.157a17,17,0,0,1,2.069-1.019,9.107,9.107,0,0,1,3.526-.741c2.257,0,3.621.741,4.138,2.176a6.76,6.76,0,0,1,.235,2.268l-0.094,4.213v0.231a4.054,4.054,0,0,0,.188,1.482,3.5,3.5,0,0,0,.987,1.111l-1.786,1.991a3.572,3.572,0,0,1-1.834-1.528,3.737,3.737,0,0,1-.8.648,4.367,4.367,0,0,1-2.586.695,4.736,4.736,0,0,1-3.291-1.019,3.706,3.706,0,0,1-1.129-2.917q0-4.306,6.207-4.306a3.855,3.855,0,0,1,.752.046V34.982A2.728,2.728,0,0,0,188.6,33.5a1.426,1.426,0,0,0-1.222-.37A7.49,7.49,0,0,0,183.752,34.519Zm5.172,5.926,0.047-2.685H188.83a4.983,4.983,0,0,0-2.445.417,1.6,1.6,0,0,0-.658,1.528,1.715,1.715,0,0,0,.423,1.2,1.31,1.31,0,0,0,1.082.463A3.108,3.108,0,0,0,188.924,40.444Zm7.147-5.6a10.3,10.3,0,0,0-.47-3.657l3.01-.787a4.29,4.29,0,0,1,.517,1.806,4.816,4.816,0,0,1,1.928-1.574,2.6,2.6,0,0,1,1.128-.185,3.325,3.325,0,0,1,1.27.278l-0.941,2.87a1.932,1.932,0,0,0-.987-0.231,2.764,2.764,0,0,0-2.069,1.065v8.8h-3.386v-8.38Zm12.32-4.167H211.4l-0.846,2.176h-2.163v6.389a3.213,3.213,0,0,0,.282,1.62,1.322,1.322,0,0,0,1.175.417,6.572,6.572,0,0,0,1.223-.231l0.423,1.944a7.361,7.361,0,0,1-2.727.556,4.706,4.706,0,0,1-2.21-.509,2.711,2.711,0,0,1-1.27-1.343,6.39,6.39,0,0,1-.235-1.944v-6.9h-1.269v-2.13h1.269a21.49,21.49,0,0,1,.141-2.963l3.433-.833A32.137,32.137,0,0,0,208.391,30.676Zm4.8,12.546V26.232h9.968l-0.47,2.778h-5.972v3.935h5.031v2.778H216.76v4.537h6.63v2.963h-10.2Zm20.171-18.148,3.339,0.509V38.639a13.341,13.341,0,0,0,.47,4.583h-3.009a2.447,2.447,0,0,1-.189-0.741,4.458,4.458,0,0,1-2.962,1.065,4.817,4.817,0,0,1-3.9-1.713,6.852,6.852,0,0,1-1.458-4.63,7.114,7.114,0,0,1,1.552-4.768,5.046,5.046,0,0,1,4-1.852,3.674,3.674,0,0,1,2.3.694,15.107,15.107,0,0,1-.094-1.852V25.074h-0.047Zm0.094,14.907V34.009a2.678,2.678,0,0,0-1.833-.787c-1.458,0-2.163,1.343-2.163,4.028a5.461,5.461,0,0,0,.517,2.778,2.028,2.028,0,0,0,1.834.787A2.465,2.465,0,0,0,233.452,39.982Zm6.677-12.731a1.911,1.911,0,0,1,.611-1.435,2,2,0,0,1,1.5-.6,1.973,1.973,0,0,1,1.458.6,2.016,2.016,0,0,1,.611,1.435,1.911,1.911,0,0,1-.611,1.435,2,2,0,0,1-1.5.6,1.973,1.973,0,0,1-1.458-.6A1.911,1.911,0,0,1,240.129,27.25Zm0.376,15.972V30.907l3.386-.509V43.222h-3.386ZM250.9,30.676h3.009l-0.846,2.176H250.9v6.389a3.213,3.213,0,0,0,.282,1.62,1.322,1.322,0,0,0,1.175.417,6.572,6.572,0,0,0,1.223-.231L254,42.991a7.361,7.361,0,0,1-2.727.556,4.706,4.706,0,0,1-2.21-.509,2.711,2.711,0,0,1-1.27-1.343,6.39,6.39,0,0,1-.235-1.944v-6.9h-1.269v-2.13h1.269a21.49,21.49,0,0,1,.141-2.963l3.433-.833A32.137,32.137,0,0,0,250.9,30.676Z\"\n" +
    "        transform=\"translate(-112 -21)\" />\n" +
    "</svg>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/perspectiveSelectorWrapperTemplate.html',
    "<perspective-selector></perspective-selector>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/toolbarActionTemplate.html',
    "<div>\n" +
    "    <div data-ng-if=\"item.type == 'ACTION'\"\n" +
    "        class=\"toolbar-action\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"btn btn-default toolbar-action--button\"\n" +
    "            data-ng-click=\"triggerAction(item, $event)\"\n" +
    "            aria-pressed=\"false\"\n" +
    "            aria-haspopup=\"true\"\n" +
    "            id=\"{{toolbarName}}_option_{{item.key}}_btn\"\n" +
    "            aria-expanded=\"false\">\n" +
    "            <span data-ng-if='item.iconClassName'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_iconclass\"\n" +
    "                data-ng-class=\"item.iconClassName\"></span>\n" +
    "            <img data-ng-if='!item.iconClassName && item.icons'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}\"\n" +
    "                data-ng-src=\"{{::imageRoot}}{{item.icons[0]}}\"\n" +
    "                class=\"file\"\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_icon\"\n" +
    "                title=\"{{item.name | translate}}\"\n" +
    "                alt=\"{{item.name | translate}}\" />\n" +
    "            <span class=\"toolbar-action--button--txt\"\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_lbl\">{{item.name | translate}}</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"item.type == 'HYBRID_ACTION'\"\n" +
    "        class=\"toolbar-action toolbar-action--hybrid\"\n" +
    "        data-uib-dropdown\n" +
    "        data-auto-close=\"outsideClick\"\n" +
    "        data-is-open=\"item.isOpen\"\n" +
    "        data-item-key=\"{{ item.key }}\">\n" +
    "        <button data-ng-show=\"item.iconClassName || item.icons\"\n" +
    "            type=\"button\"\n" +
    "            class=\"btn btn-default toolbar-action--button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            ng-disabled=\"disabled \"\n" +
    "            aria-pressed=\"false \"\n" +
    "            data-ng-click=\"triggerAction(item, $event)\">\n" +
    "            <span data-ng-if='item.iconClassName'\n" +
    "                data-ng-class=\"item.iconClassName\"></span>\n" +
    "            <img data-ng-if='!item.iconClassName && item.icons'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}\"\n" +
    "                data-ng-src=\"{{::imageRoot}}{{item.icons[0]}}\"\n" +
    "                class=\"file\"\n" +
    "                title=\"{{item.name | translate}}\"\n" +
    "                alt=\"{{item.name | translate}}\" />\n" +
    "            <span class=\"toolbar-action--button--txt\">{{item.name | translate}}</span>\n" +
    "        </button>\n" +
    "\n" +
    "        <div data-uib-dropdown-menu\n" +
    "            data-ng-class=\"{ 'dropdown-menu-left': item.section == 'left' || item.section == 'middle', 'dropdown-menu-right': item.section == 'right' }\"\n" +
    "            class=\"btn-block toolbar-action--include\">\n" +
    "            <div data-ng-if=\"getItemVisibility(item)\"\n" +
    "                data-ng-include=\"item.include\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/toolbar/toolbarTemplate.html',
    "<div class='{{cssClass}} yToolbar'>\n" +
    "\n" +
    "    <div class=\"yToolbar__Left\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'left'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__left {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "    <div class=\"yToolbar__Middle\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'middle'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__middle {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "    <div class=\"yToolbar__Right\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'right'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__right {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/fragments/mainview.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditExperienceSelectorToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"experienceSelectorToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"js_iFrameWrapper\"\n" +
    "    class=\"iframeWrapper\">\n" +
    "    <iframe id=\"ySmartEditFrame\"\n" +
    "        src=\"\"\n" +
    "        hy-dropabpe-iframe></iframe>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/editConfigurationsTemplate.html',
    "<div id=\"editConfigurationsBody\"\n" +
    "    class=\"ySEConfigBody\">\n" +
    "    <form name=\"form.configurationForm\"\n" +
    "        novalidate\n" +
    "        data-ng-submit=\"editor.submit(form.configurationForm)\">\n" +
    "        <div class=\"row ySECfgTableHeader\">\n" +
    "            <div class=\"col-xs-6\">\n" +
    "                <label data-translate=\"se.configurationform.header.key.name\"></label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-6\">\n" +
    "                <label data-translate=\"se.configurationform.header.value.name\"></label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"row ySECfgAddEntity\">\n" +
    "            <button class=\"y-add-btn\"\n" +
    "                type=\"button\"\n" +
    "                data-ng-click=\"editor.addEntry(); \">\n" +
    "                <span class=\"hyicon hyicon-add \"></span>\n" +
    "                {{'se.general.configuration.add.button' | translate}}\n" +
    "            </button>\n" +
    "        </div>\n" +
    "        <div class=\"row ySECfgEntity \"\n" +
    "            data-ng-repeat=\"entry in editor.filterConfiguration() \"\n" +
    "            data-ng-mouseenter=\"mouseenter() \"\n" +
    "            data-ng-mouseout=\"mouseout() \">\n" +
    "            <div class=\"col-xs-6 \">\n" +
    "                <input type=\"text \"\n" +
    "                    class=\"ng-class:{ 'col-xs-12':true, 'has-error':entry.errors.keys.length>0}\"\n" +
    "                    name=\"{{entry.key}}_key\"\n" +
    "                    data-ng-model=\"entry.key\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-disabled=\"!entry.isNew\"\n" +
    "                    title=\"{{entry.key}}\" />\n" +
    "                <span id=\"{{entry.key}}_error_{{$index}}\"\n" +
    "                    data-ng-if=\"entry.errors.keys\"\n" +
    "                    data-ng-repeat=\"error in entry.errors.keys\"\n" +
    "                    class=\"error-input help-block\">\n" +
    "                    {{error.message|translate}}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-5\">\n" +
    "                <textarea class=\"ng-class:{'col-xs-12':true, 'has-error':entry.errors.values.length>0}\"\n" +
    "                    name=\"{{entry.key}}_value\"\n" +
    "                    data-ng-model=\"entry.value\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-change=\"editor._validateUserInput(entry)\"></textarea>\n" +
    "                <div data-ng-if=\"entry.requiresUserCheck\">\n" +
    "                    <input id=\"{{entry.key}}_absoluteUrl_check_{{$index}}\"\n" +
    "                        type=\"checkbox\"\n" +
    "                        data-ng-model=\"entry.isCheckedByUser\" />\n" +
    "                    <span id=\"{{entry.key}}_absoluteUrl_msg_{{$index}}\"\n" +
    "                        class=\"ng-class:{'warning-check-msg':true, 'not-checked':entry.hasErrors && !entry.isCheckedByUser}\">{{'se.configurationform.absoluteurl.check' | translate}}</span>\n" +
    "                </div>\n" +
    "\n" +
    "                <span id=\"{{entry.key}}_error_{{$index}}\"\n" +
    "                    data-ng-if=\"entry.errors.values\"\n" +
    "                    data-ng-repeat=\"error in entry.errors.values\"\n" +
    "                    class=\"error-input help-block\">\n" +
    "                    {{error.message|translate}}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-1\">\n" +
    "                <button type=\"button\"\n" +
    "                    id=\"{{entry.key}}_removeButton_{{$index}}\"\n" +
    "                    class=\"btn btn-subordinate\"\n" +
    "                    data-ng-click=\"editor.removeEntry(entry, form.configurationForm);\">\n" +
    "                    <span class=\"hyicon hyicon-remove\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/generalConfigurationTemplate.html',
    "<a id=\"generalConfiguration\"\n" +
    "    data-ng-click=\"editConfiguration()\">{{'se.general.configuration.title' | translate}}</a>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/multiProductCatalogVersionSelector/multiProductCatalogVersionSelectorTemplate.html',
    "<div class=\"se-multi-product-catalog-version-selector\">\n" +
    "\n" +
    "    <div class=\"form-group se-multi-product-catalog-version-selector__label\"> {{'se.product.catalogs.multiple.list.header' | translate}}</div>\n" +
    "\n" +
    "    <div class=\"se-multi-product-catalog-version-selector__catalog form-group\"\n" +
    "        data-ng-repeat=\"productCatalog in $ctrl.productCatalogs\">\n" +
    "\n" +
    "        <label class=\"control-label se-multi-product-catalog-version-selector__catalog-name\"\n" +
    "            id=\"{{productCatalog.catalogId}}-label\">\n" +
    "            {{productCatalog.name | l10n}}\n" +
    "        </label>\n" +
    "\n" +
    "        <div class=\"se-multi-product-catalog-version-selector__catalog-version\">\n" +
    "            <y-select data-id=\"{{productCatalog.catalogId}}\"\n" +
    "                data-ng-model=\"productCatalog.selectedItem\"\n" +
    "                data-on-change=\"$ctrl.updateModel\"\n" +
    "                data-fetch-strategy=\"productCatalog.fetchStrategy\" />\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/multiProductCatalogVersionsConfigurationsTemplate.html',
    "<multi-product-catalog-version-selector data-product-catalogs=\"modalController.productCatalogs\"\n" +
    "    data-selected-versions=\"modalController.selectedCatalogVersions\"\n" +
    "    data-on-selection-change=\"modalController.updateSelection($selectedVersions)\"></multi-product-catalog-version-selector>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/productCatalogVersionsSelectorTemplate.html',
    "<div data-ng-if=\"$ctrl.isReady\">\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.isSingleVersionSelector\">\n" +
    "        <y-select data-id=\"{{$ctrl.qualifier}}\"\n" +
    "            data-ng-model=\"$ctrl.model.productCatalogVersions[0]\"\n" +
    "            data-reset=\"$ctrl.reset\"\n" +
    "            data-fetch-strategy=\"$ctrl.fetchStrategy\" />\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.isMultiVersionSelector\"\n" +
    "        data-y-popover\n" +
    "        data-trigger=\"'hover'\"\n" +
    "        class=\"se-products-catalog-select-multiple__popover\"\n" +
    "        data-placement=\"'bottom'\"\n" +
    "        data-template=\"$ctrl.buildMultiProductCatalogVersionsTemplate()\"\n" +
    "        data-is-open=\"$ctrl.isTooltipOpen\">\n" +
    "        <div id=\"multi-product-catalog-versions-selector\"\n" +
    "            data-ng-click=\"$ctrl.onClick($ctrl.productCatalogs, $ctrl.model[$ctrl.qualifier])\"\n" +
    "            class=\"se-products-catalog-select-multiple\">\n" +
    "            <input type=\"text\"\n" +
    "                data-ng-model=\"$ctrl.model[$ctrl.qualifier]\"\n" +
    "                data-ng-value=\"$ctrl.getMultiProductCatalogVersionsSelectedOptions()\"\n" +
    "                class=\"form-control se-products-catalog-select-multiple__catalogs se-nowrap-ellipsis\"\n" +
    "                name=\"{{$ctrl.qualifier}}\"\n" +
    "                readonly/>\n" +
    "            <span class=\"hyicon hyicon-optionssm se-products-catalog-select-multiple__icon\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/productCatalogVersionsSelectorWrapperTemplate.html',
    "<se-product-catalog-versions-selector data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\" />"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogDetailsTemplate.html',
    "<div class=\"se-catalog-details\"\n" +
    "    data-ng-class=\"{'se-catalog-for-current-site': $ctrl.isCatalogForCurrentSite}\">\n" +
    "    <y-collapsible-container data-configuration=\"$ctrl.collapsibleConfiguration\"\n" +
    "        class=\"se-catalog-details__collapse\">\n" +
    "        <header class=\"se-catalog-details__header\">\n" +
    "            {{$ctrl.catalog.name | l10n}}\n" +
    "        </header>\n" +
    "        <content class=\"se-catalog-details__content\">\n" +
    "            <!-- Left Side -->\n" +
    "            <div class=\"se-catalog-details__panel se-catalog-details__panel--left\">\n" +
    "                <catalog-versions-thumbnail-carousel data-catalog=\"$ctrl.catalog\"\n" +
    "                    data-site-id=\"$ctrl.siteIdForCatalog\" />\n" +
    "            </div>\n" +
    "            <!-- Right Side -->\n" +
    "            <div class=\"se-catalog-details__panel se-catalog-details__panel--right\">\n" +
    "                <div data-ng-repeat=\"catalogVersion in $ctrl.sortedCatalogVersions\">\n" +
    "                    <catalog-version-details data-catalog=\"$ctrl.catalog\"\n" +
    "                        data-catalog-version=\"catalogVersion\"\n" +
    "                        data-active-catalog-version=\"$ctrl.activeCatalogVersion\"\n" +
    "                        data-site-id=\"$ctrl.siteIdForCatalog\" />\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </content>\n" +
    "    </y-collapsible-container>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"se-catalog-divider\"\n" +
    "    data-ng-if=\"!$ctrl.isCatalogForCurrentSite\">\n" +
    "    <img data-ng-src=\"{{::$ctrl.cataloDeviderImage}}\"\n" +
    "        alt=\"\" />\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogVersionDetailsTemplate.html',
    "<div class=\"se-catalog-version-container\">\n" +
    "    <div class=\"se-catalog-version-container__left\">\n" +
    "        <div class=\"se-catalog-version-container__name\">{{$ctrl.catalogVersion.version}}</div>\n" +
    "        <div class=\"se-catalog-version-container__left__templates\">\n" +
    "            <div class=\"se-catalog-version-container__left__template\"\n" +
    "                data-ng-repeat=\"item in $ctrl.leftItems\">\n" +
    "                <div data-ng-include=\"item.include\"></div>\n" +
    "                <div class=\"se-catalog-version-container__divider\"\n" +
    "                    data-ng-if=\"!$last\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"se-catalog-version-container__right\">\n" +
    "        <div class=\"se-catalog-version-container__right__template\"\n" +
    "            data-ng-repeat=\"item in $ctrl.rightItems\"\n" +
    "            ng-include=\"item.include\" />\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogVersionsThumbnailCarouselTemplate.html',
    "<div class=\"se-active-catalog-thumbnail\">\n" +
    "    <div class=\"se-active-catalog-version-container__thumbnail\"\n" +
    "        data-ng-click=\"$ctrl.onClick()\">\n" +
    "        <div class=\"se-active-catalog-version-container__thumbnail__default-img\">\n" +
    "            <div class=\"se-active-catalog-version-container__thumbnail__img\"\n" +
    "                style=\"background-image: url('{{$ctrl.selectedVersion.thumbnailUrl}}');\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"se-active-catalog-version-container__name\">{{$ctrl.selectedVersion.version}}</div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/links/homePageLinkTemplate.html',
    "<div class=\"home-link-container\">\n" +
    "    <a class=\"home-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-click=\"ctrl.onClick()\"\n" +
    "        data-translate=\"se.landingpage.homepage\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/links/homePageLinkWrapperTemplate.html',
    "<home-page-link data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-site-id=\"$ctrl.siteId\"></home-page-link>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/clientPagedList/clientPagedList.html',
    "<div class=\"fluid-container ySEPageListResult\">\n" +
    "\n" +
    "    <p class=\"paged-list-count\"\n" +
    "        ng-if=\"displayCount\">\n" +
    "        <span>({{ totalItems }} {{'se.cms.pagelist.countsearchresult' | translate}})</span>\n" +
    "    </p>\n" +
    "\n" +
    "    <table class=\"paged-list-table table table-striped table-hover techne-table\">\n" +
    "        <thead>\n" +
    "            <tr>\n" +
    "                <th data-ng-repeat=\"key in keys\"\n" +
    "                    data-ng-click=\"orderByColumn(key.property)\"\n" +
    "                    data-ng-style=\"{ 'width': columnWidth + '%' }\"\n" +
    "                    class=\"paged-list-header\"\n" +
    "                    data-ng-class=\"'paged-list-header-'+key.property\"\n" +
    "                    ng-if=\"key.i18n\">\n" +
    "                    {{ key.i18n | translate }}\n" +
    "                    <span class=\"header-icon\"\n" +
    "                        ng-show=\"visibleSortingHeader === key.property\"\n" +
    "                        ng-class=\"{ 'down': headersSortingState[key.property] === true, 'up': headersSortingState[key.property] === false }\"></span>\n" +
    "                </th>\n" +
    "\n" +
    "                <th class=\"paged-list-header\"></th>\n" +
    "                <th class=\"paged-list-header\"\n" +
    "                    data-ng-if=\"dropdownItems!==undefined\"></th>\n" +
    "            </tr>\n" +
    "        </thead>\n" +
    "        <tbody class=\"paged-list-table__body\">\n" +
    "            <tr data-ng-repeat=\" item in items | filterByField: query : getFilterKeys() : filterCallback | startFrom:(currentPage-1)*itemsPerPage | limitTo:itemsPerPage \"\n" +
    "                class=\"techne-table-xs-right techne-table-xs-left paged-list-item \">\n" +
    "                <td ng-repeat=\"key in keys\"\n" +
    "                    ng-class=\"'paged-list-item-'+key.property\">\n" +
    "                    <div data-ng-if=\"renderers[key.property]\"\n" +
    "                        compile-html=\"renderers[key.property](item, key)\"></div>\n" +
    "                    <span data-ng-if=\"!renderers[key.property]\">{{ item[key.property] }}</span>\n" +
    "                </td>\n" +
    "                <td>\n" +
    "                    <img data-ng-src=\"{{ item.visibilityIconSrc }}\"\n" +
    "                        tooltip-placement=\"bottom\"\n" +
    "                        tooltip=\"{{ 'se.cms.icon.tooltip.visibility' | translate: item.translationData }}\" />\n" +
    "                </td>\n" +
    "                <td data-ng-if=\"dropdownItems!==undefined\"\n" +
    "                    has-operation-permission=\"'se.edit.page'\"\n" +
    "                    class=\"paged-list-table__body__td paged-list-table__body__td-menu\">\n" +
    "                    <y-drop-down-menu dropdown-items=\"dropdownItems\"\n" +
    "                        selected-item=\"item\"\n" +
    "                        class=\"y-dropdown pull-right\" />\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "        </tbody>\n" +
    "    </table>\n" +
    "\n" +
    "    <div class=\"pagination-container \">\n" +
    "        <ul data-uib-pagination\n" +
    "            boundary-links=\"true \"\n" +
    "            total-items=\"totalItems \"\n" +
    "            items-per-page=\"itemsPerPage \"\n" +
    "            ng-model=\"currentPage \"\n" +
    "            class=\"pagination-lg \"\n" +
    "            previous-text=\"&lsaquo; \"\n" +
    "            next-text=\"&rsaquo; \"\n" +
    "            first-text=\"&laquo; \"\n" +
    "            last-text=\"&raquo; \"></ul>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/core/decoratorFilter/managePerspectivesTemplate.html',
    "<div class=\"modal-header\">\n" +
    "    <h3 class=\"modal-title\">{{ 'se.modal.perpectives.title' | translate }}</h3>\n" +
    "</div>\n" +
    "<div class=\"modal-body\">\n" +
    "    <table>\n" +
    "        <tr>\n" +
    "            <td rowspan=\"2\">\n" +
    "                {{'se.modal.perpectives.header.perspectives.name' | translate}}\n" +
    "                <div>\n" +
    "                    <select size=\"12\"\n" +
    "                        data-ng-change=\"perspectiveChanged()\"\n" +
    "                        class=\"form-control\"\n" +
    "                        data-ng-options=\"p.name | translate for p in perspectives\"\n" +
    "                        data-ng-model=\"perspective\"></select>\n" +
    "                </div>\n" +
    "                <button type=\"button\"\n" +
    "                    class=\"btn btn-default btn-xs\"\n" +
    "                    data-ng-click=\"createPerspective()\">\n" +
    "                    <span class=\"glyphicon glyphicon-plus\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "                <button type=\"button\"\n" +
    "                    class=\"btn btn-danger btn-xs\"\n" +
    "                    data-ng-if=\"perspective.system !== true\"\n" +
    "                    data-ng-click=\"deletePerspective()\">\n" +
    "                    <span class=\"glyphicon glyphicon-remove\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </td>\n" +
    "            <td width=\"15px\">\n" +
    "            </td>\n" +
    "            <td valign=\"top\">\n" +
    "                {{'se.modal.perpectives.header.name.name' | translate}}:\n" +
    "                <span data-ng-if=\"perspective.system===true\">\n" +
    "                    <span style=\"padding-left: 5px\">{{ perspective.name | translate }}</span>\n" +
    "                    <span class='pull-right'\n" +
    "                        style=\"color: #feffc1; padding-left: 10px\">{{ warningForSystem | translate }}</span>\n" +
    "                </span>\n" +
    "                <input style='padding-left: 5px; color: black'\n" +
    "                    data-ng-if=\"perspective.system!==true\"\n" +
    "                    type=\"text\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-trim=\"true\"\n" +
    "                    data-ng-change=\"nameChanged()\"\n" +
    "                    data-ng-disabled=\"perspective.system === true\"\n" +
    "                    data-ng-model=\"perspective.name\">\n" +
    "                <span style=\"color: #ff0e18\">{{ renameError | translate }}</span>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td width=\"25px\">\n" +
    "            </td>\n" +
    "            <td>\n" +
    "                {{'se.modal.perpectives.header.decorators.name' | translate}}\n" +
    "                <div data-ng-repeat=\"ps in decoratorSet\">\n" +
    "                    <input type=\"checkbox\"\n" +
    "                        data-ng-model=\"ps.checked\"\n" +
    "                        data-ng-change=\"save()\"\n" +
    "                        data-ng-disabled=\"perspective.system === true\"> {{ ps.name }}\n" +
    "                </div>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>\n" +
    "<div class=\"modal-footer\">\n" +
    "    <button class=\"btn btn-default\"\n" +
    "        data-ng-click=\"close()\"\n" +
    "        data-translate=\"se.perspectives.actions.close\"></button>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/core/decoratorFilter/perspectiveTemplate.html',
    "<div>\n" +
    "    <span class=\"floating-perspective\"\n" +
    "        data-ng-mouseleave=\"mouseOff()\"\n" +
    "        data-ng-mouseenter=\"mouseOn()\">\n" +
    "        <div>\n" +
    "            <span class=\"xposed-perspectives\">\n" +
    "                <span class=\"glyphicon glyphicon-th\"\n" +
    "                    aria-hidden=\"true\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"floating-bg\"\n" +
    "            data-ng-show=\"!showButtonOnly()\">\n" +
    "            <div>\n" +
    "                <span data-translate=\"se.modal.perpectives.header.perspectives.name\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"form-group form-inline\">\n" +
    "                <select data-ng-change=\"perspectiveSelected(perspective)\"\n" +
    "                    class=\"form-control\"\n" +
    "                    data-ng-options=\"p.name | translate for p in perspectives\"\n" +
    "                    ng-model=\"perspective\" />\n" +
    "                <button type=\"button\"\n" +
    "                    data-ng-click=\"manage()\"\n" +
    "                    class=\"btn btn-default\"\n" +
    "                    aria-hidden=\"true\">\n" +
    "                    <span class=\"glyphicon glyphicon-edit\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </span>\n" +
    "</div>\n" +
    "<div data-ng-transclude></div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/contextualMenuDecoratorTemplate.html',
    "<div class=\"cmsx-ctx-wrapper1\">\n" +
    "    <div class=\"cmsx-ctx-wrapper2\">\n" +
    "        <div class=\"decorative-top-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-right-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-bottom-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-left-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"contextualMenuOverlay\"\n" +
    "            data-ng-show=\"ctrl.showOverlay() || ctrl.status.isopen\">\n" +
    "            <div data-ng-repeat=\"item in ctrl.getItems().leftMenuItems\"\n" +
    "                id=\"{{ item.key }}\"\n" +
    "                class=\"btn btn-primary cmsx-ctx-btns\">\n" +
    "                <contextual-menu-item data-mode=\"small\"\n" +
    "                    class=\"se-contextual--menu-item\"\n" +
    "                    data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                    y-popup-overlay=\"ctrl.itemTemplateOverlayWrapper\"\n" +
    "                    y-popup-overlay-trigger=\"{{ctrl.shouldShowTemplate(item)}}\"\n" +
    "                    y-popup-overlay-on-show=\"ctrl.onShowItemPopup()\"\n" +
    "                    y-popup-overlay-on-hide=\"ctrl.onHideItemPopup()\"\n" +
    "                    data-index=\"$index\"\n" +
    "                    data-component-attributes=\"ctrl.componentAttributes\"\n" +
    "                    data-slot-attributes=\"ctrl.slotAttributes\"\n" +
    "                    data-item-config=\"item\"\n" +
    "                    data-component-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                    data-component-uuid=\"{{ctrl.componentAttributes.smarteditComponentUuid}}\"\n" +
    "                    data-component-type=\"{{ctrl.smarteditComponentType}}\"\n" +
    "                    data-slot-id=\"{{ctrl.smarteditSlotId}}\"\n" +
    "                    data-slot-uuid=\"{{ctrl.smarteditSlotUuid}}\"\n" +
    "                    data-container-id=\"{{ctrl.smarteditContainerId}}\"\n" +
    "                    data-container-type=\"{{ctrl.smarteditContainerType}}\">\n" +
    "                </contextual-menu-item>\n" +
    "            </div>\n" +
    "            <div data-ng-if=\"ctrl.getItems().moreMenuItems.length > 0\"\n" +
    "                class=\"cmsx-ctx-more\">\n" +
    "                <div class=\"btn-group yCmsCtxMenu\">\n" +
    "                    <a type=\"button\"\n" +
    "                        class=\"cmsx-ctx-more-btn pull-right\"\n" +
    "                        data-ng-click=\"ctrl.moreMenuIsOpen = !ctrl.moreMenuIsOpen\"\n" +
    "                        y-popup-overlay=\"ctrl.moreMenuPopupConfig\"\n" +
    "                        y-popup-overlay-trigger=\"{{ctrl.moreMenuIsOpen}}\"\n" +
    "                        y-popup-overlay-on-show=\"ctrl.onShowMoreMenuPopup()\"\n" +
    "                        y-popup-overlay-on-hide=\"ctrl.onHideMoreMenuPopup()\">\n" +
    "                        <span title=\"{{ctrl.moreButton.i18nKey | translate}}\"\n" +
    "                            class=\"{{ctrl.moreButton.displayClass}}\"></span>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"contextualmenuBackground\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"yWrapperData\">\n" +
    "            <div data-ng-transclude></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/contextualMenuItemOverlayWrapper.html',
    "<div data-ng-if=\"item.action.template || item.action.templateUrl\"\n" +
    "    class=\"se-contextual-extra-menu\">\n" +
    "    <div data-ng-if=\"item.action.template\">\n" +
    "        <div data-compile-html=\"item.action.template\"></div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"item.action.templateUrl\">\n" +
    "        <div data-ng-include=\"item.action.templateUrl\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/moreItemsTemplate.html',
    "<div class=\"se-contextual-more-menu\">\n" +
    "    <div id=\"{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-more-menu\">\n" +
    "        <div data-ng-repeat=\"item in ctrl.getItems().moreMenuItems\">\n" +
    "            <a data-smartedit-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                data-smartedit-type=\"{{ctrl.smarteditComponentType}}\">\n" +
    "                <contextual-menu-item data-mode=\"compact\"\n" +
    "                    class=\"se-contextual-more-menu--item\"\n" +
    "                    data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                    y-popup-overlay=\"ctrl.itemTemplateOverlayWrapper\"\n" +
    "                    y-popup-overlay-trigger=\"{{ctrl.shouldShowTemplate(item)}}\"\n" +
    "                    y-popup-overlay-on-show=\"ctrl.onShowItemPopup()\"\n" +
    "                    y-popup-overlay-on-hide=\"ctrl.onHideItemPopup(true)\"\n" +
    "                    data-index=\"$index\"\n" +
    "                    data-component-attributes=\"ctrl.componentAttributes\"\n" +
    "                    data-slot-attributes=\"ctrl.slotAttributes\"\n" +
    "                    data-item-config=\"item\"\n" +
    "                    data-component-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                    data-component-uuid=\"{{ctrl.componentAttributes.smarteditComponentUuid}}\"\n" +
    "                    data-component-type=\"{{ctrl.smarteditComponentType}}\"\n" +
    "                    data-slot-id=\"{{ctrl.smarteditSlotId}}\"\n" +
    "                    data-slot-uuid=\"{{ctrl.smarteditSlotUuid}}\"\n" +
    "                    data-container-id=\"{{ctrl.smarteditContainerId}}\"\n" +
    "                    data-container-type=\"{{ctrl.smarteditContainerType}}\">\n" +
    "                </contextual-menu-item>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenuItem/contextualMenuItemComponentTemplate.html',
    "<span>\n" +
    "    <span data-ng-if=\"$ctrl.mode === 'small'\">\n" +
    "        <span id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-if=\"$ctrl.isHybrisIcon($ctrl.itemConfig.displayIconClass)\"\n" +
    "            data-ng-class=\"[$ctrl.itemConfig.displayIconClass, $ctrl.itemConfig.displayClass]\"\n" +
    "            class=\"se-contextual--icon clickable\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\">\n" +
    "        </span>\n" +
    "        <img data-ng-init=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseout=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseover=\"$ctrl.itemsrc = $ctrl.itemConfig.iconNonIdle\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\"\n" +
    "            data-ng-if=\"!$ctrl.isHybrisIcon($ctrl.itemConfig.displayIconClass)\"\n" +
    "            class=\"{{$ctrl.itemConfig.displayClass}}\"\n" +
    "            data-ng-class=\"{clickable:true}\"\n" +
    "            data-ng-src=\"{{$ctrl.itemsrc}}\" />\n" +
    "    </span>\n" +
    "    <span data-ng-if=\"$ctrl.mode === 'compact'\">\n" +
    "        <span id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-if=\"$ctrl.isHybrisIcon($ctrl.itemConfig.displaySmallIconClass)\"\n" +
    "            data-ng-class=\"[$ctrl.itemConfig.displaySmallIconClass, $ctrl.itemConfig.displayClass]\"\n" +
    "            class=\"se-contextual-more-menu--icon clickable\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\">\n" +
    "        </span>\n" +
    "        <img data-ng-init=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseout=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseover=\"$ctrl.itemsrc = $ctrl.itemConfig.iconNonIdle\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\"\n" +
    "            data-ng-if=\"$ctrl.itemConfig.smallIconIdle && !$ctrl.isHybrisIcon($ctrl.itemConfig.displaySmallIconClass)\"\n" +
    "            class=\"{{ $ctrl.classes }}\"\n" +
    "            data-ng-src=\"{{$ctrl.itemsrc}}\" />\n" +
    "        <span class=\"se-contextual--label\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-class=\"$ctrl.itemConfig.displayClass\">\n" +
    "            {{$ctrl.itemConfig.i18nKey | translate}}\n" +
    "        </span>\n" +
    "    </span>\n" +
    "</span>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/slotContextualMenu/slotContextualMenuDecoratorTemplate.html',
    "<div>\n" +
    "    <div class=\"cmsx-ctx-wrapper1 se-slot-contextual-menu-level1\">\n" +
    "        <div class=\"cmsx-ctx-wrapper2 se-slot-contextual-menu-level2\">\n" +
    "            <div class=\"decorative-panel-area\"\n" +
    "                data-ng-if=\"ctrl.showOverlay()\">\n" +
    "                <p class=\"decorative-panel-text\">{{::ctrl.smarteditComponentId}}</p>\n" +
    "                <div class=\"decorator-panel-padding-center\"></div>\n" +
    "                <div class=\"decorative-panel-slot-contextual-menu\"\n" +
    "                    data-ng-if=\"ctrl.showOverlay()\">\n" +
    "                    <div data-ng-repeat=\"item in ctrl.getItems().leftMenuItems\"\n" +
    "                        class=\"btn btn-primary cmsx-ctx-btns pull-right\"\n" +
    "                        data-ng-init=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseout=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseover=\"itemsrc = item.iconNonIdle\">\n" +
    "                        <div data-ng-if=\"!item.templateUrl\">\n" +
    "                            <span id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon\"\n" +
    "                                data-ng-if=\"item.iconIdle && ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                class=\"ng-class:{clickable:true}\">\n" +
    "                                <img data-ng-src=\"{{itemsrc}}\"\n" +
    "                                    id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon-img\"\n" +
    "                                    title=\"{{::item.i18nKey | translate}}\" />\n" +
    "                            </span>\n" +
    "                            <img id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}\"\n" +
    "                                title=\"{{::item.i18nKey | translate}}\"\n" +
    "                                data-ng-if=\"item.iconIdle && !ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                class=\"{{item.displayClass}}\"\n" +
    "                                data-ng-class=\"{clickable:true}\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                data-ng-src=\"{{itemsrc}}\"\n" +
    "                                alt=\"{{item.i18nKey}}\" />\n" +
    "                        </div>\n" +
    "                        <div data-ng-if=\"item.templateUrl\">\n" +
    "                            <div data-ng-include=\"item.templateUrl\"></div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slot-context-menu-divider\"></div>\n" +
    "                    </div>\n" +
    "                    <div data-ng-repeat=\"item in ctrl.getItems().moreMenuItems\"\n" +
    "                        class=\"btn btn-primary cmsx-ctx-btns pull-right\"\n" +
    "                        data-ng-init=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseout=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseover=\"itemsrc = item.iconNonIdle\">\n" +
    "                        <div data-ng-if=\"!item.templateUrl\">\n" +
    "                            <span id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon\"\n" +
    "                                data-ng-if=\"item.iconIdle && ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                class=\"ng-class:{clickable:true}\">\n" +
    "                                <img data-ng-src=\"{{itemsrc}}\"\n" +
    "                                    id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon-img\"\n" +
    "                                    title=\"{{::item.i18nKey | translate}}\" />\n" +
    "                            </span>\n" +
    "                            <img id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}\"\n" +
    "                                title=\"{{::item.i18nKey | translate}}\"\n" +
    "                                data-ng-if=\"item.iconIdle && !ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                class=\"{{item.displayClass}}\"\n" +
    "                                data-ng-class=\"{clickable:true}\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                data-ng-src=\"{{itemsrc}}\"\n" +
    "                                alt=\"{{item.i18nKey}}\" />\n" +
    "                        </div>\n" +
    "                        <div data-ng-if=\"item.templateUrl\">\n" +
    "                            <div data-ng-include=\"item.templateUrl\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"decorator-padding-container\">\n" +
    "                <div class=\"decorator-padding-left\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"decorator-slot-border\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"yWrapperData\"\n" +
    "                    data-ng-transclude\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"decorator-padding-right\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name yCollapsibleContainerModule
 * @requires ui.bootstrap
 * @requires yLoDashModule
 * @description
 * This module defines the collapsible container Angular component and its associated constants and controller.
 *
 * ## Basic Implementation
 *
 * To define a new collapsible container, you must make some basic modifications to your Angular module and controller, as well
 * as to your HTML template. You can also customize the rendering of your collapsible container in your controller.
 *
 * - ### Angular Module
 *
 * You must add the yCollapsibleContainerModule as a dependency to your Angular module.
 *
 * <pre>
 angular.module('yourApp', ['yCollapsibleContainerModule']) { ... }
 * </pre>
 *
 * - ### HTML template
 *
 * To include HTML content in the collapsible panel, you must embed it within a `<y-collapsible-container> </y-collapsible-container>` tag.<br />
 *
 * <pre>
 <y-collapsible-container>
    <header>
        Your title here
    </header>
    <content>
        Your content here
    </content>
 </y-collapsible-container>
 * </pre>
 *
 * - ### Angular Controller
 *
 * Within your Angular controller, you can define configurations which will get applied on the collapsible container.
 * <pre>
 angular.module('yourApp', ['sliderPanelModule'])
 .controller('yourController', function() {
     ...
     this.configuration = { ... };
     ...
 });
 * </pre>
 *
 * The configurations are passed and applied to the collapsible container through the binded variable 'configuration'
 *
 * <pre>
 <y-collapsible-container data-configuration="$yourCtrl.configuration">
    ...
 </y-collapsible-container>

 * </pre>
 *
 * For information about the available settings, see the {@link yCollapsibleContainerModule.directive:yCollapsibleContainer yCollapsibleContainer} Angular component.
 */
angular.module('yCollapsibleContainerModule', [
    'yLoDashModule',
    'ui.bootstrap'
])

/**
 * @ngdoc object
 * @name yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS
 * @description
 * This object defines injectable Angular constants that store the default configuration and CSS class names used in the controller to define the rendering and animation of the collapsible container.
 */
.constant("COLLAPSIBLE_CONTAINER_CONSTANTS", {

    /**
     * @ngdoc property
     * @name DEFAULT_CONFIGURATION {Object}
     * @propertyOf yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS
     * @param {Boolean} expandedByDefault Specifies if the collapsible container is expanded by default.
     * @param {String} iconAlignment Specifies if the expand-collapse icon is to be displayed to the left or to the right of the container header.
     * @param {Boolean} iconVisible Specifies if the expand-collapse icon is to be rendered.
     * @description
     * A JSON object defining the configuration applied by default to each collapsible container.
     **/
    DEFAULT_CONFIGURATION: {
        expandedByDefault: false,
        iconAlignment: "right",
        iconVisible: true
    },

    /**
     * @ngdoc property
     * @name ICON_LEFT {String}
     * @propertyOf yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS
     *
     * @description
     * A classname allowing for the display of a CSS-based icon positioned to the left of the collapsible container's header
     **/
    ICON_LEFT: "icon-left",

    /**
     * @ngdoc property
     * @name ICON_RIGHT {String}
     * @propertyOf yCollapsibleContainerModule.object:COLLAPSIBLE_CONTAINER_CONSTANTS
     *
     * @description
     * A classname allowing for the display of a CSS-based icon positioned to the right of the collapsible container's header
     **/
    ICON_RIGHT: "icon-right"

})

.controller('yCollapsibleContainerController', ['lodash', 'COLLAPSIBLE_CONTAINER_CONSTANTS', function(
    lodash,
    COLLAPSIBLE_CONTAINER_CONSTANTS
) {

    this.getIconRelatedClassname = function() {
        if (this.configuration.iconVisible) {
            return COLLAPSIBLE_CONTAINER_CONSTANTS["ICON_" + this.configuration.iconAlignment.toUpperCase()];
        }
    }.bind(this);

    this.$onInit = function() {

        this.configuration = lodash.defaultsDeep(
            this.configuration,
            COLLAPSIBLE_CONTAINER_CONSTANTS.DEFAULT_CONFIGURATION
        );

    };

}])

/**
 * @ngdoc directive
 * @name yCollapsibleContainerModule.directive:yCollapsibleContainer
 * @restrict E
 * @param {<Object=} configuration JSON object containing the configuration to be applied on a collapsible container.
 * @param {Boolean} configuration.expandedByDefault Specifies if the collapsible container is expanded by default.
 * @param {String} configuration.iconAlignment Specifies if the expand-collapse icon is to be displayed to the *left* or to the _right_ of the container header.
 * @param {Boolean} configuration.iconVisible Specifies if the expand-collapse icon is to be rendered.
 * @description
 * The yCollapsibleContainer Angular component allows for the dynamic display of any HTML content on a collapsible container.
 */
.component('yCollapsibleContainer', {
    templateUrl: 'yCollapsibleContainer.html',
    controller: 'yCollapsibleContainerController',
    controllerAs: '$yCollapsibleContainerCtrl',
    transclude: {
        "collapsible-container-content": "content",
        "collapsible-container-title": "?header"
    },
    bindings: {
        configuration: '<?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name yMessageModule
 * @description
 * This module provides the yMessage component, which is responsible for rendering contextual
 * feedback messages for the user actions.
 */
angular.module('yMessageModule', [])
    .controller('YMessageController', function() {
        this.$onInit = function() {
            this.messageId = this.messageId || 'y-message-default-id';
            switch (this.type) {
                case 'danger':
                    this.classes = 'y-message-danger';
                    this.icon = 'hyicon-msgdanger';
                    break;
                case 'info':
                    this.classes = 'y-message-info';
                    this.icon = 'hyicon-msginfo';
                    break;
                case 'success':
                    this.classes = 'y-message-success';
                    this.icon = 'hyicon-msgsuccess';
                    break;
                case 'warning':
                    this.classes = 'y-message-warning';
                    this.icon = 'hyicon-msgwarning';
                    break;
                default:
                    this.classes = 'y-message-info';
                    this.icon = 'hyicon-msginfo';
            }

            this.icon = 'hyicon ' + this.icon;
        };
    })
    /**
     *  @ngdoc directive
     *  @name yMessageModule.component:yMessage
     *  @scope
     *  @restrict E
     *  @element yMessage
     *
     *  @description
     *  This component provides contextual feedback messages for the user actions. To provide title and description for the yMessage
     *  use transcluded elements: message-title and message-description.
     *  @param {@String=} messageId Id for the component.
     *  @param {@String} type The type of the component (danger, info, success, warning). Default: info
     */
    .component('yMessage', {
        templateUrl: 'yMessage.html',
        controller: 'YMessageController',
        transclude: {
            messageTitle: '?messageTitle',
            messageDescription: '?messageDescription'
        },
        bindings: {
            messageId: '@?',
            type: '@'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name authenticationModule
 *
 * @description
 * # The authenticationModule
 *
 * The authentication module provides a service to authenticate and logout from SmartEdit.
 * It also allows the management of entry points used to authenticate the different resources in the application.
 *
 */
angular.module('authenticationInterfaceModule', [])
    /**
     * @ngdoc service
     * @name authenticationModule.object:DEFAULT_AUTH_MAP
     *
     * @description
     * The default authentication map contains the entry points to use before an authentication map
     * can be loaded from the configuration.
     */
    .factory('DEFAULT_AUTH_MAP', ['I18N_ROOT_RESOURCE_URI', 'DEFAULT_AUTHENTICATION_ENTRY_POINT', function(I18N_ROOT_RESOURCE_URI, DEFAULT_AUTHENTICATION_ENTRY_POINT) {
        var DEFAULT_ENTRY_POINT_MATCHER = "^(?!" + I18N_ROOT_RESOURCE_URI + '\/.*$).*$';
        var DEFAULT_AUTH_MAP = {};
        DEFAULT_AUTH_MAP[DEFAULT_ENTRY_POINT_MATCHER] = DEFAULT_AUTHENTICATION_ENTRY_POINT;

        return DEFAULT_AUTH_MAP;
    }])
    /**
     * @ngdoc service
     * @name authenticationModule.object:DEFAULT_CREDENTIALS_MAP
     *
     * @description
     * The default credentials map contains the credentials to use before an authentication map
     * can be loaded from the configuration.
     */
    .factory('DEFAULT_CREDENTIALS_MAP', ['DEFAULT_AUTHENTICATION_ENTRY_POINT', 'DEFAULT_AUTHENTICATION_CLIENT_ID', function(DEFAULT_AUTHENTICATION_ENTRY_POINT, DEFAULT_AUTHENTICATION_CLIENT_ID) {
        var DEFAULT_CREDENTIALS_MAP = {};
        DEFAULT_CREDENTIALS_MAP[DEFAULT_AUTHENTICATION_ENTRY_POINT] = {
            client_id: DEFAULT_AUTHENTICATION_CLIENT_ID
        };
        return DEFAULT_CREDENTIALS_MAP;
    }])
    /**
     * @ngdoc service
     * @name authenticationModule.service:authenticationService
     *
     * @description
     * The authenticationService is used to authenticate and logout from SmartEdit.
     * It also allows the management of entry points used to authenticate the different resources in the application.
     *
     */
    .factory('AuthenticationServiceInterface', function() {


        var AuthenticationServiceInterface = function() {

        };


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService#authenticate
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * Authenticates the current SmartEdit user against the entry point assigned to the requested resource. If no
         * suitable entry point is found, the resource will be authenticated against the
         * {@link resourceLocationsModule.object:DEFAULT_AUTHENTICATION_ENTRY_POINT DEFAULT_AUTHENTICATION_ENTRY_POINT}
         *
         * @param {String} resource The URI identifying the resource to access.
         * @returns {Promise} A promise that resolves if the authentication is successful.
         */
        AuthenticationServiceInterface.prototype.authenticate = function() {};


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService#logout
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * The logout method removes all stored authentication tokens and redirects to the
         * landing page.
         *
         */
        AuthenticationServiceInterface.prototype.logout = function() {};


        AuthenticationServiceInterface.prototype.isReAuthInProgress = function() {};


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService#setReAuthInProgress
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * Used to indicate that the user is currently within a re-authentication flow for the given entry point.
         * This flow is entered by default through authentication token expiry.
         *
         * @param {String} entryPoint The entry point which the user must be re-authenticated against.
         *
         */
        AuthenticationServiceInterface.prototype.setReAuthInProgress = function() {};


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService#filterEntryPoints
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * Will retrieve all relevant authentication entry points for a given resource.
         * A relevant entry point is an entry value of the authenticationMap found in {@link sharedDataServiceModule.service:sharedDataService sharedDataService}.The key used in that map is a regular expression matching the resource.
         * When no entry point is found, the method returns the {@link resourceLocationsModule.object:DEFAULT_AUTHENTICATION_ENTRY_POINT DEFAULT_AUTHENTICATION_ENTRY_POINT}
         * @param {string} resource The URL for which a relevant authentication entry point must be found.
         */
        AuthenticationServiceInterface.prototype.filterEntryPoints = function() {};


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService##isAuthEntryPoint
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * Indicates if the resource URI provided is one of the registered authentication entry points.
         *
         * @param {String} resource The URI to compare
         * @returns {Boolean} Flag that will be true if the resource URI provided is an authentication entry point.
         */
        AuthenticationServiceInterface.prototype.isAuthEntryPoint = function() {};


        /**
         * @ngdoc method
         * @name authenticationModule.service:authenticationService##isAuthenticated
         * @methodOf authenticationModule.service:authenticationService
         *
         * @description
         * Indicates if the resource URI provided maps to a registered authentication entry point and the associated entry point has an authentication token.
         *
         * @param {String} resource The URI to compare
         * @returns {Boolean} Flag that will be true if the resource URI provided maps to an authentication entry point which has an authentication token.
         */
        AuthenticationServiceInterface.prototype.isAuthenticated = function() {};

        return AuthenticationServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name catalogServiceModule
 * @description
 * # The catalogServiceModule
 *
 * The Catalog Service module provides a service that fetches catalogs for a specified site or for all sites registered
 * on the hybris platform.
 */
angular.module('catalogServiceInterfaceModule', [])

/**
 * @ngdoc service
 * @name catalogServiceModule.service:catalogService
 *
 * @description
 * The Catalog Service fetches catalogs for a specified site or for all sites registered on the hybris platform using
 * REST calls to the cmswebservices Catalog Version Details API.
 */
.factory('CatalogServiceInterface', function() {

    var CatalogServiceInterface = function() {};

    // ------------------------------------------------------------------------------------------------------------------------
    //  Deprecated
    // ------------------------------------------------------------------------------------------------------------------------

    /**
     * @deprecated since 6.4
     *
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getCatalogsForSite
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of catalogs for the site that corresponds to the specified site UID.
     *
     * @param {String} siteUID The UID of the site that the catalog versions are to be fetched.
     *
     * @returns {Array} An array of catalog descriptors. Each descriptor provides the following catalog properties:
     * catalog (name), catalogId, and catalogVersion.
     */
    CatalogServiceInterface.prototype.getCatalogsForSite = function() {};

    /**
     * @deprecated since 6.4
     *
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getAllCatalogsGroupedById
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of content catalog groupings for all sites.
     *
     * @returns {Array} An array of catalog groupings sorted by catalog ID, each of which has a name, a catalog ID, and a list of
     * catalog version descriptors.
     */
    CatalogServiceInterface.prototype.getAllCatalogsGroupedById = function() {};

    // ------------------------------------------------------------------------------------------------------------------------
    //  Active
    // ------------------------------------------------------------------------------------------------------------------------
    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#retrieveUriContext
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Convenience method to return a full {@link resourceLocationsModule.object:UriContext uriContext} to the invoker through a promise.
     * <br/>if uriContext is provided, it will be returned as such.
     * <br/>if uriContext is not provided, A uriContext will be built from the experience present in {@link  sharedDataServiceModule.sharedDataService sharedDataService}.
     * if we fail to find a uriContext in sharedDataService, an exception will be thrown.
     * @param {=Object=} uriContext An optional uriContext that, if provided, is simply returned wrapped in a promise
     *
     * @returns {Object} a {@link resourceLocationsModule.object:UriContext uriContext}
     */
    CatalogServiceInterface.prototype.retrieveUriContext = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getContentCatalogsForSite
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of content catalogs for the site that corresponds to the specified site UID.
     *
     * @param {String} siteUID The UID of the site that the catalog versions are to be fetched.
     *
     * @returns {Array} An array of catalog descriptors. Each descriptor provides the following catalog properties:
     * catalog (name), catalogId, and catalog version descriptors.
     */
    CatalogServiceInterface.prototype.getContentCatalogsForSite = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getAllContentCatalogsGroupedById
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of content catalog groupings for all sites.
     *
     * @returns {Array} An array of catalog groupings sorted by catalog ID, each of which has a name, a catalog ID, and a list of
     * catalog version descriptors.
     */
    CatalogServiceInterface.prototype.getAllContentCatalogsGroupedById = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getCatalogByVersion
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of catalogs for the given site UID and a given catalog version.
     *
     * @param {String} siteUID The UID of the site that the catalog versions are to be fetched.
     * @param {String} catalogVersion The version of the catalog that is to be fetched.
     *
     * @returns {Array} An array containing the catalog descriptor (if any). Each descriptor provides the following catalog properties:
     * catalog (name), catalogId, and catalogVersion.
     */
    //FIXME : this method does not seem to be safe for same catalogversion version name across multiple catalogs
    CatalogServiceInterface.prototype.getCatalogByVersion = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#isContentCatalogVersionNonActive
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Determines whether the catalog version identified by the given uriContext is a non active one
     * if no uriContext is provided, an attempt will be made to retrieve an experience from {@link sharedDataServiceModule.sharedDataService sharedDataService} 
     *
     * @param {Object} uriContext the {@link resourceLocationsModule.object:UriContext UriContext}. Optional
     * @returns {Boolean} true if the given catalog version is non active
     */
    CatalogServiceInterface.prototype.isContentCatalogVersionNonActive = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getContentCatalogActiveVersion
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * find the version that is flagged as active for the given uriContext
     * if no uriContext is provided, an attempt will be made to retrieve an experience from {@link sharedDataServiceModule.sharedDataService sharedDataService} 
     *
     * @param {Object} uriContext the {@link resourceLocationsModule.object:UriContext UriContext}. Optional
     * @returns {String} the version name
     */
    CatalogServiceInterface.prototype.getContentCatalogActiveVersion = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getActiveContentCatalogVersionByCatalogId
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Finds the version name that is flagged as active for the given content catalog.
     *
     * @param {String} contentCatalogId The UID of content catalog for which to retrieve its active catalog version name.
     * @returns {String} the version name
     */
    CatalogServiceInterface.prototype.getActiveContentCatalogVersionByCatalogId = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getDefaultSiteForContentCatalog
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Finds the ID of the default site configured for the provided content catalog. 
     *
     * @param {String} contentCatalogId The UID of content catalog for which to retrieve its default site ID.
     * @returns {String} the ID of the default site found. 
     */
    CatalogServiceInterface.prototype.getDefaultSiteForContentCatalog = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getCatalogVersionByUuid
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Finds the catalog version descriptor identified by the provided UUID. An exception is thrown if no
     * match is found. 
     *
     * @param {String} catalogVersionUuid The UID of the catalog version descriptor to find. 
     * @param {String=} siteId the ID of the site where to perform the search. If no ID is provided, the search will 
     * be performed on all permitted sites.
     * @returns {Promise} A promise that resolves to the catalog version descriptor found. 
     * 
     */
    CatalogServiceInterface.prototype.getCatalogVersionByUuid = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getProductCatalogsForSite
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches a list of product catalogs for the site that corresponds to the specified site UID.
     *
     * @param {String} siteUID The UID of the site that the catalog versions are to be fetched.
     *
     * @returns {Array} An array of catalog descriptors. Each descriptor provides the following catalog properties:
     * catalog (name), catalogId, and catalog version descriptors.
     */
    CatalogServiceInterface.prototype.getProductCatalogsForSite = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#getActiveProductCatalogVersionByCatalogId
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Finds the version name that is flagged as active for the given product catalog.
     *
     * @param {String} productCatalogId The UID of product catalog for which to retrieve its active catalog version name.
     * @returns {String} the version name
     */
    CatalogServiceInterface.prototype.getActiveProductCatalogVersionByCatalogId = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#clearCache
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Empties the caches storing catalog service information.
     *
     */
    CatalogServiceInterface.prototype.clearCache = function() {};

    /**
     * @ngdoc method
     * @name catalogServiceModule.service:catalogService#returnActiveCatalogVersionUIDs
     * @methodOf catalogServiceModule.service:catalogService
     *
     * @description
     * Fetches all the active catalog version uuid's for a provided array of catalogs.
     *
     * @param {Array} An array of catalogs objects. Each catalog object must have a versions array.
     * @returns {Array} An array of catalog version uuid's
     */
    CatalogServiceInterface.prototype.returnActiveCatalogVersionUIDs = function() {};

    return CatalogServiceInterface;

});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("configModule", [])
    .config(['$locationProvider', '$qProvider', function($locationProvider, $qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name featureInterfaceModule
 */
angular.module('featureInterfaceModule', ['functionsModule'])

/**
 * @ngdoc service
 * @name featureInterfaceModule.service:FeatureServiceInterface
 *
 * @description
 * The interface stipulates how to register features in the SmartEdit application and the SmartEdit container.
 * The SmartEdit implementation stores two instances of the interface across the {@link gatewayFactoryModule.gatewayFactory gateway}: one for the SmartEdit application and one for the SmartEdit container.
 */
.factory('FeatureServiceInterface', ['$q', '$log', 'hitch', 'isBlank', function($q, $log, hitch, isBlank) {

    function FeatureServiceInterface() {

    }

    FeatureServiceInterface.prototype._validate = function(configuration) {
        if (isBlank(configuration.key)) {
            throw new Error("featureService.configuration.key.error.required");
        }
        if (isBlank(configuration.nameI18nKey)) {
            throw new Error("featureService.configuration.nameI18nKey.error.required");
        }
        if (isBlank(configuration.enablingCallback) || typeof configuration.enablingCallback !== 'function') {
            throw new Error("featureService.configuration.enablingCallback.error.not.function");
        }
        if (isBlank(configuration.disablingCallback) || typeof configuration.disablingCallback !== 'function') {
            throw new Error("featureService.configuration.disablingCallback.error.not.function");
        }
    };
    /**
     * @ngdoc method
     * @name featureInterfaceModule.service:FeatureServiceInterface#register
     * @methodOf featureInterfaceModule.service:FeatureServiceInterface
     * @description
     * This method registers a feature.
     * When an end user selects a perspective, all the features that are bound to the perspective
     * will be enabled when their respective enablingCallback functions are invoked
     * and all the features that are not bound to the perspective will be disabled when their respective disablingCallback functions are invoked.
     * The SmartEdit application and the SmartEdit container hold/store an instance of the implementation because callbacks cannot cross the gateway as they are functions.
     *
     * this method is meant to register a feature (identified by a key).
     * When a perspective (registered through {@link perspectiveInterfaceModule.service:PerspectiveServiceInterface#methods_register PerspectiveServiceInterface.register}) is selected, all its bound features will be enabled by invocation of their respective enablingCallback functions
     * and any feature not bound to it will be disabled by invocation of its disablingCallback function.
     * Both SmartEdit and SmartEditContainer will hold a concrete implementation since Callbacks, being functions, cannot cross the gateway.
     * The function will keep a frame bound reference on a full feature in order to be able to invoke its callbacks when needed.
     *
     * @param {Object} configuration The configuration that represents the feature to be registered.
     * @param {String} configuration.key The key that uniquely identifies the feature in the registry.
     * @param {String} configuration.nameI18nKey The i18n key that stores the feature name to be translated.
     * @param {String} configuration.descriptionI18nKey The i18n key that stores the feature description to be translated. The description is used as a tooltip in the web application. This is an optional parameter.
     * @param {Function} configuration.enablingCallback The callback function invoked to enable the feature when it is required by a perspective.
     * @param {Function} configuration.disablingCallback The callback function invoked to disable the feature when it is not required by a perspective.
     * @param {String[]} configuration.permissions The list of permissions required to enable the feature.
     */
    FeatureServiceInterface.prototype.register = function(configuration) {

        this._validate(configuration);

        this.featuresToAlias = this.featuresToAlias || {};
        this.featuresToAlias[configuration.key] = {
            enablingCallback: configuration.enablingCallback,
            disablingCallback: configuration.disablingCallback
        };

        delete configuration.enablingCallback;
        delete configuration.disablingCallback;

        this._registerAliases(configuration);
    };

    FeatureServiceInterface.prototype.enable = function(key) {
        if (this.featuresToAlias && this.featuresToAlias[key]) {
            this.featuresToAlias[key].enablingCallback();
            return;
        } else {
            this._remoteEnablingFromInner(key);
        }
    };

    FeatureServiceInterface.prototype.disable = function(key) {
        if (this.featuresToAlias && this.featuresToAlias[key]) {
            this.featuresToAlias[key].disablingCallback();
            return;
        } else {
            this._remoteDisablingFromInner(key);
        }
    };


    /**
     * @ngdoc method
     * @name featureInterfaceModule.service:FeatureServiceInterface#getFeatureProperty
     * @methodOf featureInterfaceModule.service:FeatureServiceInterface
     * @description
     * Returns a feature property
     *
     * @param {Object} featureKey the key property value of the feature
     * @param {String} propertyName name of the property
     *
     * @return {Object} returns property value or null if property does not exist
     */
    FeatureServiceInterface.prototype.getFeatureProperty = function() {};

    FeatureServiceInterface.prototype._remoteEnablingFromInner = function() {};
    FeatureServiceInterface.prototype._remoteDisablingFromInner = function() {};


    /**
     * @ngdoc method
     * @name featureInterfaceModule.service:FeatureServiceInterface#_registerAliases
     * @methodOf featureInterfaceModule.service:FeatureServiceInterface
     * @description
     * This method registers a feature, identified by a unique key, across the {@link gatewayFactoryModule.gatewayFactory gateway}.
     * It is a simplified version of the register method, from which callbacks have been removed.
     *
     * @param {Object} configuration the configuration representing the feature to register
     * @param {String} configuration.key The key that uniquely identifies the feature in the registry.
     * @param {String} configuration.nameI18nKey The i18n key that uniquely identifies the feature name to be translated.
     * @param {String} configuration.descriptionI18nKey The description of the l18n key to be translated. An optional parameter.
     */
    FeatureServiceInterface.prototype._registerAliases = function() {};

    /**
     * @ngdoc method
     * @name featureInterfaceModule.service:FeatureServiceInterface#addToolbarItem
     * @methodOf featureInterfaceModule.service:FeatureServiceInterface
     *
     * @description
     * This method registers toolbar items as features. It is a wrapper around {@link featureInterfaceModule.service:FeatureServiceInterface#methods_register register}.
     *
     * @param {Object} configuration The configuration that represents the toolbar action item to be registered.
     * @param {String} configuration.toolbarId The key that uniquely identifies the toolbar that the feature is added to.
     * @param {String} configuration.keyThe key that uniquely identifies the toolbar item in the registry as as defined in the {@link toolbarInterfaceModule.ToolbarServiceInterface#addItems ToolbarServiceInterface.addItems} API. See for more information on the available properties of item for the included template.
     * @param {String} configuration.nameI18nKey The i18n key that stores the toolbar item name to be translated.
     * @param {String} configuration.descriptionI18nKey The i18n key that stores the toolbar item description to be translated. This is an optional parameter.
     * @param {Function} configuration.callback The callback that is triggered when the toolbar action item is clicked.
     * @param {String[]} configuration.icons A list of image URLs for the icon images to be displayed in the toolbar for the items. The images are only available for ACTION and HYBRID_ACTION toolbar items.
     * @param {String} configuration.type The type of toolbar item. The possible value are: TEMPLATE, ACTION, and HYBRID_ACTION.
     * @param {String} configuration.include The URL to the HTML template. By default, templates are available for TEMPLATE and HYBRID_ACTION toolbar items.
     * @param {String[]} configuration.permissions The list of permissions required to enable the feature.
     */
    FeatureServiceInterface.prototype.addToolbarItem = function() {};

    /**
     * @ngdoc method
     * @name featureInterfaceModule.service:FeatureServiceInterface#addDecorator
     * @methodOf featureInterfaceModule.service:FeatureServiceInterface
     *
     * @description
     * this method registers decorator and delegates to the
     *  {@link decoratorServiceModule.service:decoratorService#methods_enable enable}
     *  {@link decoratorServiceModule.service:decoratorService#methods_disable disable} methods of
     *  {@link decoratorServiceModule.service:decoratorService decoratorService}.
     * This method is not a wrapper around {@link decoratorServiceModule.service:decoratorService#addMappings decoratorService.addMappings}:
     * From a feature stand point, we deal with decorators, not their mappings to SmartEdit components.
     * We still need to have a separate invocation of {@link decoratorServiceModule.service:decoratorService#addMappings decoratorService.addMappings}
     * @param {Object} configuration The configuration that represents the decorator to be registered.
     * @param {Arrays} configuration.key The decorator key defined in the {@link decoratorServiceModule.service:decoratorService#addMappings decoratorService.addMappings} API
     * @param {String} configuration.nameI18nKey the i18n key that stores the decorator name to be translated.
     * @param {String} configuration.descriptionI18nKey The i18n key that stores the decorator description to be translated. The description is used as a tooltip in the web application. This is an optional parameter.
     * @param {String[]} configuration.permissions The list of permissions required to enable the feature.
     */
    FeatureServiceInterface.prototype.addDecorator = function() {};


    /**
         * @ngdoc method
         * @name featureInterfaceModule.service:FeatureServiceInterface#addContextualMenuButton
         * @methodOf featureInterfaceModule.service:FeatureServiceInterface
         *
         * @description
         * This method registers contextual menu buttons. It is a wrapper around {@link contextualMenuServiceModule.ContextualMenuService#methods_addItems contextualMenuService.addItems}.
         *
         * @param {Object} configuration The configuration representing the decorator to be registered.
         * @param {Arrays} configuration.key The key that uniquely identifies the feature in the registry.
         * @param {String} configuration.regexpKey A regular expression, ant-like wildcard or strict match that identifies the component types eligible for the specified contextual menu button.
         * @param {String} configuration.nameI18nKey They key that stores the name of the button to be translated.
         * @param {String} configuration.descriptionI18nKey The key that stores the description of the button to be translated. An optional parameter.
         * @param {Object} configuration.condition An optional entry that stores the condition required to activate the menu item. it is invoked with:
         * <pre>
         * {
                    	componentType: the smartedit component type
                    	componentId: the smartedit component id
                    	containerType: the type of the container wrapping the component, if applicable
                    	containerId: the id of the container wrapping the component, if applicable
                    	element: the dom element of the component onto which the contextual menu is applied
		}
         * </pre>
         * @param {Object} configuration.callback The action to be performed by clicking on the menu item. It is invoked with
         * <pre>
         * {
                    	componentType: the smartedit component type
                    	componentId: the smartedit component id
                    	containerType: the type of the container wrapping the component, if applicable
                    	containerId: the id of the container wrapping the component, if applicable
                    	slotId: the id of the content slot containing the component
		}
         * </pre>
         * @param {Object} configuration.callbacks A object holding a list of functions where the key is the name of the event to be performed
         * on the element and the value is the event handler function to be invoked when that particular event is triggered.
         * @param {String} configuration.displayClass The CSS classes used to style the contextual menu item.
         * @param {String} configuration.iconIdle The location of the idle icon of the contextual menu item to be displayed.
         * @param {String} configuration.iconNonIdle The location of the non-idle icon of the contextual menu item to be displayed.
         * @param {String} configuration.smallIcon The location of the smaller version of the icon to be displayed when the menu item is part of the More... menu options.
         * @param {String[]} configuration.permissions The list of permissions required to enable the feature.
         */
    FeatureServiceInterface.prototype.addContextualMenuButton = function() {};

    return FeatureServiceInterface;

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('httpAuthInterceptorModule', ['interceptorHelperModule', 'authenticationModule', 'storageServiceModule'])


/**
 * @ngdoc service
 * @name httpAuthInterceptorModule.httpAuthInterceptor
 * 
 * @description
 * Makes it possible to perform global authentication by intercepting requests before they are forwarded to the server
 * and responses before they are forwarded to the application code.
 * 
 * The interceptors are service factories that are registered with the $httpProvider by adding them to the $httpProvider.interceptors array. 
 * The factory is called and injected with dependencies and returns the interceptor object, which contains the interceptor methods.
 */
.factory('httpAuthInterceptor', ['$log', 'storageService', 'authenticationService', 'interceptorHelper', function($log, storageService, authenticationService, interceptorHelper) {
        return {
            /** 
             * @ngdoc method
             * @name httpAuthInterceptorModule.httpAuthInterceptor#request
             * @methodOf httpAuthInterceptorModule.httpAuthInterceptor
             * 
             * @description
             * Interceptor method which gets called with a http config object, intercepts any request made using $http service.
             * A call to any REST resource will be intercepted by this method, which then adds an authentication token to the request
             * and then forwards it to the REST resource.
             * 
             * @param {Object} config - the http config object that holds the configuration information.
             */
            request: function(config) {
                return interceptorHelper.handleRequest(config, function() {
                    return authenticationService.filterEntryPoints(config.url).then(function(entryPoints) {
                        if (entryPoints && entryPoints.length) {
                            return storageService.getAuthToken(entryPoints[0]).then(function(authToken) {
                                $log.debug(['Intercepting request ' + (authToken ? 'adding access token' : 'no access token found'), config.url].join(' '));
                                if (authToken) {
                                    config.headers.Authorization = authToken.token_type + " " + authToken.access_token;
                                }
                                return config;
                            });
                        } else {
                            return config;
                        }
                    }, function() {
                        return config;
                    });
                });
            }
        };
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('httpAuthInterceptor');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('nonvalidationErrorInterceptorModule', ['alertServiceModule'])
    /**
     * @ngdoc service
     * @name nonvalidationErrorInterceptorModule.service:nonValidationErrorInterceptor
     * @description
     * Used for HTTP error code 400. It removes all errors of type 'ValidationError' and displays alert messages for non-validation errors.
     */
    .factory('nonValidationErrorInterceptor', ['$q', 'alertService', function($q, alertService) {
        return {
            predicate: function(response) {
                return response.status === 400;
            },
            responseError: function(response) {
                response.data.errors.filter(function(error) {
                    return error.type !== 'ValidationError';
                }).forEach(function(error) {
                    alertService.showDanger({
                        message: error.message || 'se.unknown.request.error',
                        timeout: 10000
                    });
                });
                return $q.reject(response);
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('previewErrorInterceptorModule', ['resourceLocationsModule', 'sharedDataServiceModule', 'functionsModule'])
    /**
     * @ngdoc service
     * @name previewErrorInterceptorModule.service:previewErrorInterceptor
     * @description
     * Used for HTTP error code 400 from the Preview API when the pageId is not found in the context. The request will be replayed without the pageId.
     */
    .factory('previewErrorInterceptor', ['$injector', '$q', 'PREVIEW_RESOURCE_URI', 'sharedDataService', 'isBlank', function($injector, $q, PREVIEW_RESOURCE_URI, sharedDataService, isBlank) {
        return {
            predicate: function(response) {
                return response.status === 400 && response.config.url.indexOf(PREVIEW_RESOURCE_URI) > -1 && !isBlank(response.config.data.pageId) && _hasUnknownIdentifierError(response.data.errors);
            },
            responseError: function(response) {
                delete response.config.data.pageId;
                sharedDataService.update("experience", function(experience) {
                    delete experience.pageId;
                    return experience;
                });
                $injector.get('iFrameManager').setCurrentLocation(null);
                return $q.when($injector.get('$http')(response.config));
            }
        };

        function _hasUnknownIdentifierError(errors) {
            var unknownIdentifierErrors = errors.filter(function(error) {
                return error.type === 'UnknownIdentifierError';
            });
            return unknownIdentifierErrors.length ? true : false;
        }
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('resourceNotFoundErrorInterceptorModule', ['alertServiceModule', 'resourceLocationsModule', 'httpRequestUtilModule'])
    /**
     * @ngdoc service
     * @name resourceNotFoundErrorInterceptorModule.service:resourceNotFoundErrorInterceptor
     * @description
     * Used for HTTP error code 404 (Not Found) except for an HTML or a language resource. It will display the response.message in an alert message.
     */
    .factory('resourceNotFoundErrorInterceptor', ['$q', 'alertService', 'httpRequestUtil', 'LANGUAGE_RESOURCE_URI', function($q, alertService, httpRequestUtil, LANGUAGE_RESOURCE_URI) {
        return {
            predicate: function(response) {
                return response.status === 404 && !httpRequestUtil.isHTMLRequest(response) && !_isLanguageResourceRequest(response.config.url);
            },
            responseError: function(response) {
                alertService.showDanger({
                    message: response.message || 'se.unknown.request.error',
                    timeout: 10000
                });
                return $q.reject(response);
            }
        };

        function _isLanguageResourceRequest(url) {
            var languageResourceRegex = new RegExp(LANGUAGE_RESOURCE_URI.replace(/\:.*\//g, '.*/'));
            return languageResourceRegex.test(url);
        }
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name retryInterceptorModule
 *
 * @description
 * This module provides the functionality to retry failing xhr requests through the registration of retry strategies.
 */
angular.module('retryInterceptorModule', ['functionsModule', 'operationContextServiceModule', 'httpMethodPredicatesModule', 'httpErrorPredicatesModule', 'operationContextPredicatesModule', 'defaultRetryStrategyModule', 'exponentialRetryStrategyModule', 'linearRetryStrategyModule', 'alertServiceModule'])
    /**
     * @ngdoc service
     * @name retryInterceptorModule.service:retryInterceptor
     *
     * @description
     * The retryInterceptor provides the functionality to register a set of predicates with their associated retry strategies.
     * Each time an HTTP request fails, the service try to find a matching retry strategy for the given response.
     */
    .factory('retryInterceptor', ['$http', '$q', '$timeout', '$translate', 'operationContextService', 'alertService', 'OPERATION_CONTEXT', 'isAllTruthy', 'isAnyTruthy', 'noInternetConnectionErrorPredicate', 'readPredicate', 'updatePredicate', 'clientErrorPredicate', 'timeoutErrorPredicate', 'serverErrorPredicate', 'retriableErrorPredicate', 'operationContextInteractivePredicate', 'operationContextNonInteractivePredicate', 'operationContextCMSPredicate', 'operationContextToolingPredicate', 'defaultRetryStrategy', 'exponentialRetryStrategy', 'linearRetryStrategy', function($http, $q, $timeout, $translate, operationContextService, alertService, OPERATION_CONTEXT, isAllTruthy, isAnyTruthy, noInternetConnectionErrorPredicate, readPredicate, updatePredicate, clientErrorPredicate, timeoutErrorPredicate, serverErrorPredicate, retriableErrorPredicate, operationContextInteractivePredicate, operationContextNonInteractivePredicate, operationContextCMSPredicate, operationContextToolingPredicate, defaultRetryStrategy, exponentialRetryStrategy, linearRetryStrategy) {
        var TRANSLATE_NAMESPACE = 'se.gracefuldegradation.';

        // Array<obj>
        // obj.predicate {Function}
        // obj.retryStrategy {Function}
        var predicatesRegistry = [];

        /**
         * Find a matching strategy for the given response and (optional) operationContext
         * If not provided, the default operationContext is OPERATION_CONTEXT.INTERACTIVE
         * 
         * @param {Object} response The http response object
         * 
         * @return {Function} The matching retryStrategy
         */
        var findMatchingStrategy = function(response) {
            var operationContext = operationContextService.findOperationContext(response.config.url) || OPERATION_CONTEXT.INTERACTIVE;
            var matchStrategy = predicatesRegistry.find(function(predicateObj) {
                return predicateObj.predicate(response, operationContext);
            });
            return matchStrategy ? matchStrategy.retryStrategy : null;
        };

        var handleRetry = function(response) {
            var retryStrategy = response.config.retryStrategy;
            retryStrategy.attemptCount++;
            if (retryStrategy.canRetry()) {
                var defer = $q.defer();
                var delay = retryStrategy.firstFastRetry ? 0 : retryStrategy.calculateNextDelay();
                retryStrategy.firstFastRetry = false;
                $timeout(function() {
                    defer.resolve($http(response.config));
                }, delay);
                return defer.promise;
            } else {
                alertService.showDanger({
                    message: $translate.instant(TRANSLATE_NAMESPACE + 'somethingwrong')
                });
                return $q.reject(response);
            }
        };

        var retryInterceptor = {
            predicate: function(response) {
                return findMatchingStrategy(response) !== null;
            },

            responseError: function(response) {
                if (response.config.retryStrategy) {
                    return $q.when(handleRetry(response));
                } else {
                    var StrategyHolder = findMatchingStrategy(response);
                    if (StrategyHolder) {
                        alertService.showWarning({
                            message: $translate.instant(TRANSLATE_NAMESPACE + 'stillworking')
                        });
                        var strategyInstance = new StrategyHolder();
                        strategyInstance.attemptCount = 0;
                        response.config.retryStrategy = strategyInstance;
                        return $q.when(handleRetry(response));
                    } else {
                        return $q.reject(response);
                    }
                }
            },

            /**
             * @ngdoc method
             * @name retryInterceptorModule.service:retryInterceptor#register
             * @methodOf retryInterceptorModule.service:retryInterceptor
             *
             * @description
             * Register a new predicate with it's associated strategyHolder.
             *
             * @param {Function} predicate This function takes the 'response' {Object} argument and an (optional) operationContext {String}. This function must return a Boolean that is true if the given response match the predicate.
             * @param {Function} strategyHolder This function will be instanciated at run-time. See {@link retryStrategyInterfaceModule.RetryStrategyInterface RetryStrategyInterface}.
             * 
             * @return {Object} retryInterceptor The retryInterceptor service.
             *
             * @example
             * ```js
             * angular.module('customInterceptorExample', ['retryInterceptorModule', 'operationContextServiceModule'])
             *  .controller('ExampleController', function($q, retryInterceptor, OPERATION_CONTEXT) {
             *      var customPredicate = function(httpObj, operationContext) {
             *          return httpObj.status === 500 && operationContext === OPERATION_CONTEXT.TOOLING;
             *      };
             *      var StrategyHolder = function() {
             *          // set the firstFastRetry value to true for the retry made immediately only for the very first retry (subsequent retries will remain subject to the calculateNextDelay response)
             *          this.firstFastRetry = true;
             *      };
             *      StrategyHolder.prototype.canRetry = function() {
             *          // this function must return a {Boolean} if the given request must be retried.
             *          // use this.attemptCount value to determine if the function should return true or false
             *      };
             *      StrategyHolder.prototype.calculateNextDelay = function() {
             *          // this function must return the next delay time {Number}
             *          // use this.attemptCount value to determine the next delay value
             *      };
             *      retryInterceptor.register(customPredicate, StrategyHolder);
             *  });
             * ```
             */
            register: function(predicate, strategyHolder) {
                if (typeof predicate !== 'function') {
                    throw new Error('retryInterceptor.register error: predicate must be a function');
                }
                if (typeof strategyHolder !== 'function') {
                    throw new Error('retryInterceptor.register error: strategyHolder must be a function');
                }
                predicatesRegistry.unshift({
                    predicate: predicate,
                    retryStrategy: strategyHolder
                });
                return this;
            }
        };

        retryInterceptor
            .register(noInternetConnectionErrorPredicate, exponentialRetryStrategy)
            .register(isAnyTruthy(clientErrorPredicate, timeoutErrorPredicate), defaultRetryStrategy)
            .register(isAllTruthy(operationContextInteractivePredicate, retriableErrorPredicate), defaultRetryStrategy)
            .register(isAllTruthy(readPredicate, retriableErrorPredicate), defaultRetryStrategy)
            .register(serverErrorPredicate, exponentialRetryStrategy)
            .register(isAllTruthy(operationContextNonInteractivePredicate, retriableErrorPredicate), exponentialRetryStrategy)
            .register(isAllTruthy(operationContextCMSPredicate, timeoutErrorPredicate, updatePredicate), exponentialRetryStrategy)
            .register(isAllTruthy(operationContextToolingPredicate, timeoutErrorPredicate, updatePredicate), linearRetryStrategy);

        return retryInterceptor;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name operationContextServiceModule
 * 
 * @description
 * This module provides the functionality to register a set of url with their associated operation contexts.
 */
angular.module('operationContextServiceModule', ['functionsModule'])
    /**
     * @ngdoc object
     * @name operationContextServiceModule.object:OPERATION_CONTEXT
     *
     * @description
     * Injectable angular constant<br/>
     * This object provides an enumeration of operation context for the application.
     */
    .constant('OPERATION_CONTEXT', {
        BACKGROUND_TASKS: 'Background Tasks',
        INTERACTIVE: 'Interactive',
        NON_INTERACTIVE: 'Non-Interactive',
        BATCH_OPERATIONS: 'Batch Operations',
        TOOLING: 'Tooling',
        CMS: 'CMS'
    })
    /**
     * @ngdoc service
     * @name operationContextServiceModule.service:operationContextService
     */
    .service('operationContextService', ['isBlank', function(isBlank) {
        this._store = [];

        /**
         * @ngdoc method
         * @name operationContextServiceModule.service:operationContextService#register
         * @methodOf operationContextServiceModule.service:operationContextService
         * 
         * @description
         * Register a new url with it's associated operationContext.
         * 
         * @param {String} url The url that is associated to the operation context.
         * @param {String} operationContext The operation context name that is associated to the given url.
         * 
         * @return {Object} operationContextService The operationContextService service
         */
        this.register = function(url, operationContext) {
            if (typeof url !== 'string' || isBlank(url)) {
                throw new Error('operationContextService.register error: url is invalid');
            }
            if (typeof operationContext !== 'string' || isBlank(operationContext)) {
                throw new Error('operationContextService.register error: operationContext is invalid');
            }
            var regexIndex = this._store.findIndex(function(store) {
                return store.urlRegex.test(url) === true && store.operationContext === operationContext;
            });
            if (regexIndex !== -1) {
                return;
            }
            var urlRegex = new RegExp(url.replace(/\/:[^\/]*/g, '/.*'));
            this._store.push({
                urlRegex: urlRegex,
                operationContext: operationContext
            });
            return this;
        };

        /**
         * @ngdoc method
         * @name operationContextServiceModule.service:operationContextService#findOperationContext
         * @methodOf operationContextServiceModule.service:operationContextService
         * 
         * @description
         * Find the first matching operation context for the given url.
         * 
         * @param {String} url The request url.
         * 
         * @return {String} operationContext
         */
        this.findOperationContext = function(url) {
            var regexIndex = this._store.findIndex(function(store) {
                return store.urlRegex.test(url) === true;
            });
            return ~regexIndex ? this._store[regexIndex].operationContext : null;
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('httpErrorPredicatesModule', ['yLoDashModule', 'functionsModule'])
    .constant('SERVER_ERROR_PREDICATE_HTTP_STATUSES', [500, 502, 503, 504])
    .constant('CLIENT_ERROR_PREDICATE_HTTP_STATUSES', [429])
    .constant('TIMEOUT_ERROR_PREDICATE_HTTP_STATUSES', [408])
    .service('serverErrorPredicate', ['lodash', 'SERVER_ERROR_PREDICATE_HTTP_STATUSES', function(lodash, SERVER_ERROR_PREDICATE_HTTP_STATUSES) {
        return function(response) {
            return lodash.includes(SERVER_ERROR_PREDICATE_HTTP_STATUSES, response.status);
        };
    }])
    .service('clientErrorPredicate', ['lodash', 'CLIENT_ERROR_PREDICATE_HTTP_STATUSES', function(lodash, CLIENT_ERROR_PREDICATE_HTTP_STATUSES) {
        return function(response) {
            return lodash.includes(CLIENT_ERROR_PREDICATE_HTTP_STATUSES, response.status);
        };
    }])
    .service('timeoutErrorPredicate', ['lodash', 'TIMEOUT_ERROR_PREDICATE_HTTP_STATUSES', function(lodash, TIMEOUT_ERROR_PREDICATE_HTTP_STATUSES) {
        return function(response) {
            return lodash.includes(TIMEOUT_ERROR_PREDICATE_HTTP_STATUSES, response.status);
        };
    }])
    .service('retriableErrorPredicate', ['isAnyTruthy', 'serverErrorPredicate', 'clientErrorPredicate', 'timeoutErrorPredicate', function(isAnyTruthy, serverErrorPredicate, clientErrorPredicate, timeoutErrorPredicate) {
        return function(response) {
            return isAnyTruthy(serverErrorPredicate, clientErrorPredicate, timeoutErrorPredicate)(response);
        };
    }])
    .service('noInternetConnectionErrorPredicate', function() {
        return function(response) {
            return response.status === 0;
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('httpMethodPredicatesModule', ['yLoDashModule'])
    .constant('HTTP_METHODS_UPDATE', ['PUT', 'POST', 'DELETE', 'PATCH'])
    .constant('HTTP_METHODS_READ', ['GET', 'OPTIONS', 'HEAD'])
    .service('updatePredicate', ['lodash', 'HTTP_METHODS_UPDATE', function(lodash, HTTP_METHODS_UPDATE) {
        return function(response) {
            return lodash.includes(HTTP_METHODS_UPDATE, response.config.method);
        };
    }])
    .service('readPredicate', ['lodash', 'HTTP_METHODS_READ', function(lodash, HTTP_METHODS_READ) {
        return function(response) {
            return lodash.includes(HTTP_METHODS_READ, response.config.method);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('operationContextPredicatesModule', ['operationContextServiceModule', 'yLoDashModule'])
    .service('operationContextInteractivePredicate', ['OPERATION_CONTEXT', function(OPERATION_CONTEXT) {
        return function(response, operationContext) {
            return operationContext === OPERATION_CONTEXT.INTERACTIVE;
        };
    }])
    .service('operationContextNonInteractivePredicate', ['OPERATION_CONTEXT', 'lodash', function(OPERATION_CONTEXT, lodash) {
        return function(response, operationContext) {
            return lodash.includes([OPERATION_CONTEXT.BACKGROUND_TASKS, OPERATION_CONTEXT.NON_INTERACTIVE, OPERATION_CONTEXT.BATCH_OPERATIONS], operationContext);
        };
    }])
    .service('operationContextCMSPredicate', ['OPERATION_CONTEXT', function(OPERATION_CONTEXT) {
        return function(response, operationContext) {
            return operationContext === OPERATION_CONTEXT.CMS;
        };
    }])
    .service('operationContextToolingPredicate', ['OPERATION_CONTEXT', function(OPERATION_CONTEXT) {
        return function(response, operationContext) {
            return operationContext === OPERATION_CONTEXT.TOOLING;
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name exponentialRetrylModule
 *
 * @description
 * This module provides the exponentialRetry service.
 */
angular.module('exponentialRetrylModule', [])
    /**
     * @ngdoc object
     * @name exponentialRetrylModule.object:EXPONENTIAL_RETRY_DEFAULT_SETTING
     * 
     * @description
     * The setting object to be used as default values for retry.
     */
    .constant("EXPONENTIAL_RETRY_DEFAULT_SETTING", {
        MAX_BACKOFF: 64000,
        MAX_ATTEMPT: 5,
        MIN_BACKOFF: 0
    })
    /**
     * @ngdoc service
     * @name exponentialRetrylModule.service:exponentialRetry
     * @description
     * When used by a retry strategy, this service could provide an exponential delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.
     */
    .service('exponentialRetry', ['EXPONENTIAL_RETRY_DEFAULT_SETTING', function(EXPONENTIAL_RETRY_DEFAULT_SETTING) {
        /**
         * @ngdoc method
         * @name exponentialRetrylModule.service:exponentialRetry#calculateNextDelay
         * @methodOf exponentialRetrylModule.service:exponentialRetry
         * 
         * @description
         * This method will calculate the next delay time.
         * 
         * @param {Number} attemptCount The current number of retry attempts
         * @param {Number =} maxBackoff The maximum delay between two retries
         * @param {Number =} minBackoff The minimum delay between two retries
         * 
         * @return {Number} The next delay value
         */
        this.calculateNextDelay = function(attemptCount, maxBackoff, minBackoff) {
            maxBackoff = maxBackoff || EXPONENTIAL_RETRY_DEFAULT_SETTING.MAX_BACKOFF;
            minBackoff = minBackoff || EXPONENTIAL_RETRY_DEFAULT_SETTING.MIN_BACKOFF;

            var waveShield = minBackoff + Math.random();

            var delay = Math.min(((Math.pow(2, attemptCount) * 1000) + waveShield), maxBackoff);
            return delay;
        };

        /**
         * @ngdoc method
         * @name exponentialRetrylModule.service:exponentialRetry#canRetry
         * @methodOf exponentialRetrylModule.service:exponentialRetry
         * 
         * @description
         * This method returns true if it is valid to perform another retry, otherwise, it returns false.
         * 
         * @param {Number} attemptCount The current number of retry attempts
         * @param {Number =} maxAttempt The maximum number of retry attempts
         * 
         * @return {Boolean} is valid to perform another retry?
         */
        this.canRetry = function(attemptCount, maxAttempt) {
            maxAttempt = maxAttempt || EXPONENTIAL_RETRY_DEFAULT_SETTING.MAX_ATTEMPT;
            return (attemptCount <= maxAttempt);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name linearRetrylModule
 *
 * @description
 * This module provides the linearRetry service.
 */
angular.module('linearRetrylModule', [])
    /**
     * @ngdoc object
     * @name linearRetrylModule.object:LINEAR_RETRY_DEFAULT_SETTING
     * 
     * @description
     * The setting object to be used as default values for retry.
     */
    .constant("LINEAR_RETRY_DEFAULT_SETTING", {
        MAX_ATTEMPT: 5,
        MAX_BACKOFF: 32000,
        MIN_BACKOFF: 0,
        RETRY_INTERVAL: 500
    })
    /**
     * @ngdoc service
     * @name linearRetrylModule.service:linearRetry
     * @description
     * When used by a retry strategy, this service could provide a linear delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.
     */
    .service('linearRetry', ['LINEAR_RETRY_DEFAULT_SETTING', function(LINEAR_RETRY_DEFAULT_SETTING) {
        /**
         * @ngdoc method
         * @name linearRetrylModule.service:linearRetry#calculateNextDelay
         * @methodOf linearRetrylModule.service:linearRetry
         * 
         * @description
         * This method will calculate the next delay time.
         * 
         * @param {Number} attemptCount The current number of retry attempts
         * @param {Number =} retryInterval The base interval between two retries
         * @param {Number =} maxBackoff The maximum delay between two retries
         * @param {Number =} minBackoff The minimum delay between two retries
         * 
         * @return {Number} The next delay value
         */
        this.calculateNextDelay = function(attemptCount, retryInterval, maxBackoff, minBackoff) {
            maxBackoff = maxBackoff || LINEAR_RETRY_DEFAULT_SETTING.MAX_BACKOFF;
            minBackoff = minBackoff || LINEAR_RETRY_DEFAULT_SETTING.MIN_BACKOFF;
            retryInterval = retryInterval || LINEAR_RETRY_DEFAULT_SETTING.RETRY_INTERVAL;

            var waveShield = minBackoff + Math.random();
            var delay = Math.min(attemptCount * retryInterval + waveShield, maxBackoff);
            return delay;
        };

        /**
         * @ngdoc method
         * @name linearRetrylModule.service:linearRetry#canRetry
         * @methodOf linearRetrylModule.service:linearRetry
         * 
         * @description
         * This method returns true if it is valid to perform another retry, otherwise, it returns false.
         * 
         * @param {Number} attemptCount The current number of retry attempts
         * @param {Number =} maxRetry The maximum number of retry attempts
         * 
         * @return {Boolean} is valid to perform another retry?
         */
        this.canRetry = function(attemptCount, maxAttempt) {
            maxAttempt = maxAttempt || LINEAR_RETRY_DEFAULT_SETTING.MAX_ATTEMPT;
            return (attemptCount <= maxAttempt);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name simpleRetrylModule
 *
 * @description
 * This module provides the simpleRetry service.
 */
angular.module('simpleRetrylModule', [])
    /**
     * @ngdoc object
     * @name simpleRetrylModule.object:SIMPLE_RETRY_DEFAULT_SETTING
     * 
     * @description
     * The setting object to be used as default values for retry.
     */
    .constant("SIMPLE_RETRY_DEFAULT_SETTING", {
        MAX_ATTEMPT: 5,
        MIN_BACKOFF: 0,
        RETRY_INTERVAL: 500
    })
    /**
     * @ngdoc service
     * @name simpleRetrylModule.service:simpleRetry
     * @description
     * When used by a retry strategy, this service could provide a simple fixed delay time to be used by the strategy before the next request is sent. The service also provides functionality to check if it is possible to perform a next retry.
     */
    .service('simpleRetry', ['SIMPLE_RETRY_DEFAULT_SETTING', function(SIMPLE_RETRY_DEFAULT_SETTING) {
        /**
         * @ngdoc method
         * @name simpleRetrylModule.service:simpleRetry#calculateNextDelay
         * @methodOf simpleRetrylModule.service:simpleRetry
         * 
         * @description
         * This method will calculate the next delay time.
         * 
         * @param {Number =} retryInterval The base interval between two retries
         * @param {Number =} minBackoff The minimum delay between two retries
         * 
         * @return {Number} The next delay value
         */
        this.calculateNextDelay = function(retryInterval, minBackoff) {
            minBackoff = minBackoff || SIMPLE_RETRY_DEFAULT_SETTING.MIN_BACKOFF;
            retryInterval = retryInterval || SIMPLE_RETRY_DEFAULT_SETTING.RETRY_INTERVAL;

            var waveShield = minBackoff + Math.random();
            var delay = retryInterval + waveShield;
            return delay;
        };

        /**
         * @ngdoc method
         * @name simpleRetrylModule.service:simpleRetry#canRetry
         * @methodOf simpleRetrylModule.service:simpleRetry
         * 
         * @description
         * This method returns true if it is valid to perform another retry, otherwise, it returns false.
         * 
         * @param {Number} attemptCount The current number of retry attempts
         * @param {Number =} maxAttempt The maximum number of retry attempts
         * 
         * @return {Boolean} is valid to perform another retry?
         */
        this.canRetry = function(attemptCount, maxAttempt) {
            maxAttempt = maxAttempt || SIMPLE_RETRY_DEFAULT_SETTING.MAX_ATTEMPT;
            return (attemptCount <= maxAttempt);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('defaultRetryStrategyModule', ['simpleRetrylModule'])
    .factory('defaultRetryStrategy', ['simpleRetry', function(simpleRetry) {
        var Strategy = function() {
            this.firstFastRetry = true;
        };
        Strategy.prototype.canRetry = function() {
            return simpleRetry.canRetry(this.attemptCount);
        };
        Strategy.prototype.calculateNextDelay = function() {
            return simpleRetry.calculateNextDelay();
        };
        return Strategy;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('exponentialRetryStrategyModule', ['exponentialRetrylModule'])
    .factory('exponentialRetryStrategy', ['exponentialRetry', function(exponentialRetry) {
        var Strategy = function() {
            this.firstFastRetry = true;
        };
        Strategy.prototype.canRetry = function() {
            return exponentialRetry.canRetry(this.attemptCount);
        };
        Strategy.prototype.calculateNextDelay = function() {
            return exponentialRetry.calculateNextDelay(this.attemptCount);
        };
        return Strategy;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('linearRetryStrategyModule', ['linearRetrylModule'])
    .factory('linearRetryStrategy', ['linearRetry', function(linearRetry) {
        var Strategy = function() {
            this.firstFastRetry = true;
        };
        Strategy.prototype.canRetry = function() {
            return linearRetry.canRetry(this.attemptCount);
        };
        Strategy.prototype.calculateNextDelay = function() {
            return linearRetry.calculateNextDelay(this.attemptCount);
        };
        return Strategy;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name RetryStrategyInterface
 */
angular.module('retryStrategyInterfaceModule', [])
    /**
     * @ngdoc service
     * @name retryStrategyInterfaceModule.service:RetryStrategyInterface
     */
    .factory('RetryStrategyInterface', function() {
        var Interface = function() {
            /**
             * @ngdoc method
             * @name retryStrategyInterfaceModule.service:RetryStrategyInterface#canRetry
             * @methodOf retryStrategyInterfaceModule.service:RetryStrategyInterface
             * 
             * @description
             * Function that must return a {Boolean} if the current request must be retried
             * 
             * @return {Boolean} true if the current request must be retried
             */
            this.canRetry = function() {};

            /**
             * @ngdoc method
             * @name retryStrategyInterfaceModule.service:RetryStrategyInterface#canRetry
             * @methodOf retryStrategyInterfaceModule.service:RetryStrategyInterface
             * 
             * @description
             * Function that returns the next delay time {Number}
             * 
             * @return {Number} delay the delay until the next retry
             */
            this.calculateNextDelay = function() {};
        };
        return Interface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('unauthorizedErrorInterceptorModule', ['authenticationModule'])
    /**
     * @ngdoc service
     * @name unauthorizedErrorInterceptorModule.service:unauthorizedErrorInterceptor
     * @description
     * Used for HTTP error code 401 (Forbidden). It will display the login modal.
     */
    .factory('unauthorizedErrorInterceptor', ['$injector', '$q', 'authenticationService', function($injector, $q, authenticationService) {
        // key: auth entry point, value: array of deferred
        var promisesToResolve = {};

        return {
            predicate: function(response) {
                return response.status === 401;
            },
            responseError: function(response) {
                var deferred = $q.defer();
                authenticationService.isAuthEntryPoint(response.config.url).then(function(isAuthEntryPoint) {
                    if (!isAuthEntryPoint) {
                        authenticationService.filterEntryPoints(response.config.url).then(function(entryPoints) {
                            var entryPoint = entryPoints[0];
                            promisesToResolve[entryPoint] = promisesToResolve[entryPoint] || [];
                            promisesToResolve[entryPoint].push(deferred);
                            authenticationService.isReAuthInProgress(entryPoint).then(function(isReAuthInProgress) {
                                if (!isReAuthInProgress) {
                                    authenticationService.setReAuthInProgress(entryPoint).then(function() {
                                        authenticationService.authenticate(response.config.url).then(function() {
                                            angular.forEach(promisesToResolve[this], function(def) {
                                                def.resolve();
                                            });
                                            promisesToResolve[this] = [];
                                        }.bind(entryPoint), function() {
                                            angular.forEach(promisesToResolve[this], function(def) {
                                                def.reject();
                                            });
                                            promisesToResolve[this] = [];
                                        }.bind(entryPoint));
                                    });
                                }
                            }.bind(this));
                        });
                    } else {
                        deferred.reject(response);
                    }
                });

                return deferred.promise.then(function() {
                    return $injector.get('$http')(response.config);
                });
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name httpErrorInterceptorServiceModule
 * 
 * @description
 * This module provides the functionality to add custom HTTP error interceptors.
 * Interceptors are used to execute code each time an HTTP request fails.
 */
angular.module('httpErrorInterceptorServiceModule', ['interceptorHelperModule'])
    .factory('httpErrorInterceptor', ['httpErrorInterceptorService', 'interceptorHelper', function(httpErrorInterceptorService, interceptorHelper) {
        return {
            responseError: function(response) {
                return interceptorHelper.handleResponseError(response, function() {
                    return httpErrorInterceptorService.responseError(response);
                });
            }
        };
    }])
    /**
     * @ngdoc service
     * @name httpErrorInterceptorServiceModule.service:httpErrorInterceptorService
     * 
     * @description
     * The httpErrorInterceptorService provides the functionality to add custom HTTP error interceptors.
     * An interceptor can be an {Object} or an Angular Factory and must be represented by a pair of functions:
     * - predicate(response) {Function} that must return true if the response is associated to the interceptor. Important: The predicate must be designed to fulfill a specific function. It must not be defined for generic use.
     * - responseError(response) {Function} function called if the current response error matches the predicate. It must return a {Promise} with the resolved or rejected response.
     * 
     * Each time an HTTP request fails, the service iterates through all registered interceptors. It sequentially calls the responseError function for all interceptors that have a predicate returning true for the current response error. If an interceptor modifies the response, the next interceptor that is called will have the modified response.
     * The last interceptor added to the service will be the first interceptor called. This makes it possible to override default interceptors.
     * If an interceptor resolves the response, the service service stops the iteration.
     */
    .service('httpErrorInterceptorService', ['$injector', '$log', '$q', function($injector, $log, $q) {
        this._errorInterceptors = [];
        this._interceptorsDeferred = $q.defer();

        /**
         * @ngdoc method
         * @name httpErrorInterceptorServiceModule.service:httpErrorInterceptorService#addInterceptor
         * @methodOf httpErrorInterceptorServiceModule.service:httpErrorInterceptorService
         * 
         * @description
         * Add a new error interceptor
         * 
         * @param {Object|String} interceptor The interceptor {Object} or angular Factory
         * 
         * @returns {Function} Function to call to unregister the interceptor from the service
         * 
         * @example
         * ```js
         * angular.module('customInterceptorExample', ['httpErrorInterceptorServiceModule', 'alertServiceModule'])
         *  .controller('ExampleController', function($q, httpErrorInterceptorService, alertService) {
         *      // Add a new interceptor with an object:
         *      var unregisterCustomInterceptor = httpErrorInterceptorService.addInterceptor({
         *          predicate: function(response) {
         *              return response.status === 400;
         *          },
         *          responseError: function(response) {
         *              alertService.showDanger({
         *                  message: response.message
         *              });
         *              return $q.reject(response);
         *          }
         *      });
         *
         *      // Add an interceptor with an angular Factory:
         *      var unregisterCustomInterceptor = httpErrorInterceptorService.addInterceptor('customErrorInterceptor'); 
         * 
         *      // Unregister the interceptor:
         *      unregisterCustomInterceptor();
         *  });
         * ```
         */
        this.addInterceptor = function(interceptor) {
            if (typeof interceptor === 'string') {
                if ($injector.has(interceptor)) {
                    interceptor = $injector.get(interceptor);
                } else {
                    throw "httpErrorInterceptorService.interceptor.undefined";
                }
            }

            this._validateInterceptor(interceptor);
            this._errorInterceptors.unshift(interceptor);

            return function() {
                this._errorInterceptors.splice(this._errorInterceptors.indexOf(interceptor), 1);
            }.bind(this);
        };

        this.responseError = function(response) {
            var matchingErrorInterceptors = this._errorInterceptors.filter(function(errorInterceptor) {
                return errorInterceptor.predicate(response) === true;
            });
            this._interceptorsDeferred = $q.defer();
            if (matchingErrorInterceptors.length) {
                this._iterateErrorInterceptors(angular.copy(response), matchingErrorInterceptors);
            } else {
                this._interceptorsDeferred.reject(response);
            }
            return this._interceptorsDeferred.promise;
        };

        this._iterateErrorInterceptors = function(response, interceptors, idx) {
            if (idx === interceptors.length) {
                this._interceptorsDeferred.reject(response);
            } else {
                var def = this._interceptorsDeferred;
                var iterateFn = this._iterateErrorInterceptors.bind(this);
                idx = idx || 0;
                $q.when(interceptors[idx].responseError(response)).then(function(interceptedResponse) {
                    def.resolve(interceptedResponse);
                }, function(interceptedResponse) {
                    iterateFn(interceptedResponse, interceptors, ++idx);
                });
            }
        };

        /**
         * @ignore
         * Validate if the provided interceptor respects the Interface (predicate and responseError functions are mandatory).
         * @param {Object|String} interceptor The interceptor {Object} or angular Factory
         */
        this._validateInterceptor = function(interceptor) {
            if (typeof interceptor.predicate !== 'function') {
                throw new Error('httpErrorInterceptorService.addInterceptor.error.interceptor.has.no.predicate');
            }
            if (typeof interceptor.responseError !== 'function') {
                throw new Error('httpErrorInterceptorService.addInterceptor.error.interceptor.has.no.responseError');
            }
        };
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('httpErrorInterceptor');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('httpRequestUtilModule', [])
    .service('httpRequestUtil', function() {
        this.isHTMLRequest = function(response) {
            return response.config.method === 'GET' && response.headers('Content-type').indexOf('text/html') >= 0;
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name notificationServiceInterfaceModule
 * 
 * @description
 * The notification module provides a service to display visual cues to inform
 * the user of the state of the application in the container or the iFramed application.
 */
angular.module('notificationServiceInterfaceModule', [])

/**
 * @ngdoc object
 * @name notificationServiceInterfaceModule.object:SE_NOTIFICATION_SERVICE_GATEWAY_ID
 * 
 * @description
 * The gateway UID used to proxy the NotificationService.
 */
.constant('SE_NOTIFICATION_SERVICE_GATEWAY_ID', 'SE_NOTIFICATION_SERVICE_GATEWAY_ID')

/**
 * @ngdoc service
 * @name notificationServiceInterfaceModule.service:NotificationServiceInterface
 * 
 * @description
 * The interface defines the methods required to manage notifications that are to be displayed to the user.
 */
.factory('NotificationServiceInterface', function() {
    function NotificationServiceInterface() {}

    /**
     * @ngdoc method
     * @name notificationServiceInterfaceModule.service:NotificationServiceInterface#pushNotification
     * @methodOf notificationServiceInterfaceModule.service:NotificationServiceInterface
     * 
     * @description
     * This method creates a new notification based on the given configuration and
     * adds it to the top of the list.
     * 
     * The configuration must contain either a template or template URL, but not both.
     * 
     * @param {Object} configuration The notification's configuration.
     * @param {String} configuration.id The notification's unique identifier.
     * @param {String=} configuration.template The notification's HTML template.
     * @param {String=} configuration.templateUrl The notification's template URL.
     * 
     * @throws An error if no configuration is given.
     * @throws An error if the configuration does not contain a unique identifier.
     * @throws An error if the configuration's unique identifier is an empty string.
     * @throws An error if the configuration does not contain a template or templateUrl.
     * @throws An error if the configuration contains both a template and template Url.
     */
    NotificationServiceInterface.prototype.pushNotification = function() {};

    /**
     * @ngdoc method
     * @name notificationServiceInterfaceModule.service:NotificationServiceInterface#removeNotification
     * @methodOf notificationServiceInterfaceModule.service:NotificationServiceInterface
     * 
     * @description
     * This method removes the notification with the given ID from the list.
     * 
     * @param {String} notificationId The notification's unique identifier.
     */
    NotificationServiceInterface.prototype.removeNotification = function() {};

    /**
     * @ngdoc method
     * @name notificationServiceInterfaceModule.service:NotificationServiceInterface#removeAllNotifications
     * @methodOf notificationServiceInterfaceModule.service:NotificationServiceInterface
     * 
     * @description
     * This method removes all notifications.
     */
    NotificationServiceInterface.prototype.removeAllNotifications = function() {};

    return NotificationServiceInterface;
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name permissionServiceInterfaceModule
 *
 * @description
 * The permissions service resolves user permissions returning true or false.
 *
 */
angular.module('permissionServiceInterfaceModule', ['functionsModule'])
    .constant('SE_PERMISSION_SERVICE_GATEWAY_ID', 'SE_PERMISSION_SERVICE_GATEWAY_ID')
    /**
     * @ngdoc service
     * @name permissionServiceInterfaceModule.service:PermissionServiceInterface
     *
     * @description
     * The permission service is used to check if a user has been granted certain permissions.
     *
     * It is configured with rules and permissions. A rule is used to execute some logic to determine whether or not
     * the permission should be granted. A permission references a list of rules. In order for a permission to be
     * granted, each rule must be executed successfully and return true.
     */
    .factory('PermissionServiceInterface', function() {

        var PermissionServiceInterface = function() {};

        var prepareRuleConfiguration = function(ruleConfiguration) {
            this.ruleVerifyFunctions = this.ruleVerifyFunctions || {};
            this.ruleVerifyFunctions[ruleConfiguration.names.join("-")] = {
                verify: ruleConfiguration.verify
            };
            delete ruleConfiguration.verify;

            return ruleConfiguration;
        };

        var validateRule = function(rule) {
            if (!(rule.names instanceof Array)) {
                throw "Rule names must be array";
            }

            if (rule.names.length < 1) {
                throw "Rule requires at least one name";
            }

            if (!rule.verify) {
                throw "Rule requires a verify function";
            }

            if (typeof rule.verify !== 'function') {
                throw "Rule verify must be a function";
            }
        };

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#isPermitted
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method checks if a user has been granted certain permissions.
         *
         * It takes an array of permission objects structured as follows:
         *
         * {
         *     names: ["permission.aliases"],
         *     context: {
         *         data: "required to check a permission"
         *     }
         * }
         *
         * @param {Object[]} permissions A list of permission objects.
         *
         * @returns {IPromise} A promise that resolves to true if permission is granted, rejects to false if it isn't and rejects on error.
         */
        PermissionServiceInterface.prototype.isPermitted = function() {};

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#clearCache
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method clears all cached results in the rules' caches.
         */
        PermissionServiceInterface.prototype.clearCache = function() {};

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#getPermission
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method returns the registered permission that contains the given name in its
         * array of names.
         *
         * @param {String} permissionName The name of the permission to lookup.
         *
         * @returns {Object} rule The permission with the given name, undefined otherwise.
         */
        PermissionServiceInterface.prototype.getPermission = function() {};

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#registerPermission
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method registers a permission.
         *
         * A permission is defined by a set of aliases and rules. It is verified by its set of rules.
         * The set of aliases is there for convenience, as there may be different permissions
         * that use the same set of rules to be verified. The permission aliases property
         * will resolve if any one alias is in the aliases' array. Calling {@link permissionServiceModule.service:PermissionService#isPermitted}
         * with any of these aliases will use the same permission object, therefore the same
         * combination of rules to check if the user has the appropriate clearance. This reduces the
         * number of permissions you need to register.
         *
         * @param {Object} configuration The configuration of the permission to register.
         * @param {String[]} configuration.aliases The list of aliases associated to the permission. A permission alias must be prefixed by at least one
         * namespace followed by a "." character to be valid. i.e. "se.fake.permission"
         * @param {String[]} configuration.rules The list of the names of the rules used to verify.
         *
         * @throws Will throw an error if the permission has no aliases array
         * @throws Will throw an error if the permission's aliases array is empty
         * @throws Will throw an error if the permission has no rules array
         * @throws Will throw an error if the permission's rule aliases array is empty
         * @throws Will throw an error if a permission is already registered with a common entry in its array of aliases
         * @throws Will throw an error if one of the permission's aliases is not name spaced
         * @throws Will throw an error if no rule is registered with on of the permission's rule names
         */
        PermissionServiceInterface.prototype.registerPermission = function() {};

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#registerRule
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method registers a rule. These rules can be used by registering permissions that
         * use them to verify if a user has the appropriate clearance.
         *
         * To avoid accidentally overriding the default rule, an error is thrown when attempting
         * to register a rule with the {@link permissionServiceModule.object:DEFAULT_RULE_NAME
         * default rule name}.
         *
         * To register the default rule, see {@link permissionServiceModule.service:PermissionService#registerDefaultRule}.
         *
         * @param {Object} ruleConfiguration The configuration of the rule to register.
         * @param {String[]} ruleConfiguration.names The list of names associated to the rule.
         * @param {Function} ruleConfiguration.verify The verification function of the rule. It must return a promise that responds with true, false, or an error.
         *
         * @throws Will throw an error if the list of rule names contains the reserved {@link permissionServiceModule.object:DEFAULT_RULE_NAME default rule name}.
         * @throws Will throw an error if the rule has no names array.
         * @throws Will throw an error if the rule's names array is empty.
         * @throws Will throw an error if the rule has no verify function.
         * @throws Will throw an error if the rule's verify parameter is not a function.
         * @throws Will throw an error if a rule is already registered with a common entry in its names array
         */
        PermissionServiceInterface.prototype.registerRule = function(ruleConfiguration) {
            validateRule(ruleConfiguration);
            ruleConfiguration = prepareRuleConfiguration.bind(this)(ruleConfiguration);
            this._registerRule(ruleConfiguration);
        };

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#registerDefaultRule
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method registers the default rule.
         *
         * The default rule is used when no permission is found for a given permission name when
         * {@link permissionServiceModule.service:PermissionService#isPermitted} is called.
         *
         * @param {Object} ruleConfiguration The configuration of the default rule.
         * @param {String[]} ruleConfiguration.names The list of names associated to the default rule (must contain {@link permissionServiceModule.object:DEFAULT_RULE_NAME}).
         * @param {Function} ruleConfiguration.verify The verification function of the default rule.
         *
         * @throws Will throw an error if the default rule's names does not contain {@link permissionServiceModule.object:DEFAULT_RULE_NAME}
         * @throws Will throw an error if the default rule has no names array.
         * @throws Will throw an error if the default rule's names array is empty.
         * @throws Will throw an error if the default rule has no verify function.
         * @throws Will throw an error if the default rule's verify parameter is not a function.
         * @throws Will throw an error if a rule is already registered with a common entry in its names array
         */
        PermissionServiceInterface.prototype.registerDefaultRule = function(ruleConfiguration) {
            ruleConfiguration = prepareRuleConfiguration.bind(this)(ruleConfiguration);
            this._registerDefaultRule(ruleConfiguration);
        };

        /**
         * @ngdoc method
         * @name permissionServiceInterfaceModule.service:PermissionServiceInterface#unregisterDefaultRule
         * @methodOf permissionServiceInterfaceModule.service:PermissionServiceInterface
         *
         * @description
         * This method un-registers the default rule.
         */
        PermissionServiceInterface.prototype.unregisterDefaultRule = function() {};

        PermissionServiceInterface.prototype._registerRule = function() {};

        PermissionServiceInterface.prototype._registerDefaultRule = function() {};

        return PermissionServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name perspectiveInterfaceModule
 */
angular.module('perspectiveServiceInterfaceModule', ['functionsModule'])
    //'decorator' wording to be replaced, constructor to be modified and ngdoc to be added once dependent code is refactored
    .factory('Perspective', ['copy', function(copy) {
        var Perspective = function(name, features, system) {
            this.name = name;
            this.system = system === undefined ? false : system;
            this.features = features;
            this.setFeatures = function(features) {
                this.features = copy(features);
            };
            this.getFeatures = function() {
                return this.features;
            };
        };

        Perspective.prototype.clone = function() {
            var featureClone = [];
            var thisFeatures = this.getFeatures();
            for (var pk in thisFeatures) {
                featureClone.push(thisFeatures[pk]);
            }
            return new Perspective(this.name, featureClone, this.system);
        };

        return Perspective;
    }])
    /**
     * @ngdoc service
     * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface
     *
     */
    .factory('PerspectiveServiceInterface', function() {


        function PerspectiveServiceInterface() {}

        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#register
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * This method registers a perspective.
         * When an end user selects a perspective in the SmartEdit web application,
         * all features bound to the perspective will be enabled when their respective enablingCallback functions are invoked
         * and all features not bound to the perspective will be disabled when their respective disablingCallback functions are invoked.
         * 
         * @param {Object} configuration The configuration that represents the feature to be registered.
         * @param {String} configuration.key The key that uniquely identifies the perspective in the registry.
         * @param {String} configuration.nameI18nKey The i18n key that stores the perspective name to be translated.
         * @param {String} configuration.descriptionI18nKey The i18n key that stores the perspective description to be translated. The description is used as a tooltip in the web application. This is an optional parameter.
         * @param {Array}  configuration.features A list of features to be bound to the perspective.
         * @param {Array}  configuration.perspectives A list of referenced perspectives to be bound to this system perspective. This list is optional.
         * @param {Array}  configuration.permissions A list of permission names to be bound to the perspective to determine if the user is allowed to see it and use it. This list is optional.
         */
        PerspectiveServiceInterface.prototype.register = function() {};

        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#switchTo
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * This method actives a perspective identified by its key and deactivates the currently active perspective.
         * Activating a perspective consists in activating any feature that is bound to the perspective
         * or any feature that is bound to the perspective's referenced perspectives and deactivating any features
         * that are not bound to the perspective or to its referenced perspectives.
         * After the perspective is changed, the {@link seConstantsModule.object:EVENT_PERSPECTIVE_CHANGED
         * EVENT_PERSPECTIVE_CHANGED} event is published on the {@link crossFrameEventServiceModule.service:CrossFrameEventService
         * crossFrameEventService}, with no data.
         *
         * @param {String} key The key that uniquely identifies the perspective to be activated. This is the same key as the key used in the {@link perspectiveInterfaceModule.service:PerspectiveServiceInterface#methods_register register} method.
         */
        PerspectiveServiceInterface.prototype.switchTo = function() {};

        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#hasActivePerspective
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * 	This method returns true if a perspective is selected.
         *
         * @returns {Boolean} The key of the active perspective.
         */
        PerspectiveServiceInterface.prototype.hasActivePerspective = function() {};


        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#selectDefault
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * This method switches the currently-selected perspective to the default perspective.
         * It will also disable all features for the default perspective before enabling them all back.
         * If no value has been stored in the smartedit-perspectives cookie, the value of the default perspective is se.none.
         * If a value is stored in the cookie, that value is used as the default perspective.
         *
         */
        PerspectiveServiceInterface.prototype.selectDefault = function() {};

        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#isEmptyPerspectiveActive
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * This method returns true if the current active perspective is the Preview mode (No active overlay).
         *
         * @returns {Boolean} Flag that indicates if the current perspective is the Preview mode.
         */
        PerspectiveServiceInterface.prototype.isEmptyPerspectiveActive = function() {};

        /**
         * @ngdoc method
         * @name perspectiveInterfaceModule.service:PerspectiveServiceInterface#refreshPerspective
         * @methodOf perspectiveInterfaceModule.service:PerspectiveServiceInterface
         *
         * @description
         * This method is used to refresh the prespective.
         * If there is an exising perspective set then it is refreshed by replaying all the features associated to the current perspective.
         * If there is no perspective set or if the perspective is not permitted then we set the default perspective.
         * 
         */
        PerspectiveServiceInterface.prototype.refreshPerspective = function() {};

        return PerspectiveServiceInterface;

    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name renderServiceInterfaceModule
     * @description
     * # The renderServiceInterfaceModule
     *
     * The render service interface module provides an abstract extensible
     * {@link renderServiceInterfaceModule.service:RenderServiceInterface renderService} . It is designed to
     * re-render components after an update component data operation has been performed, according to how
     * the Accelerator displays the component.
     *
     */
    angular.module('renderServiceInterfaceModule', ['eventServiceModule', 'componentHandlerServiceModule', 'notificationServiceModule', 'seConstantsModule', 'componentHandlerServiceModule', 'perspectiveServiceModule'])
        .constant("KEY_CODES", {
            ESC: 27
        })
        .constant("MOUSE_AND_KEYBOARD_EVENTS", {
            KEY_DOWN: 'keydown',
            KEY_UP: 'keyup',
            CLICK: 'click'
        })
        .constant('OVERLAY_DISABLED', 'OVERLAY_DISABLED')
        .constant('MODAL_DIALOG_ID', 'y-modal-dialog')
        .constant('OVERLAY_RERENDERED_EVENT', 'overlayRerendered')
        /**
         * @ngdoc service
         * @name renderServiceInterfaceModule.service:RenderServiceInterface
         * @description
         * Designed to re-render components after an update component data operation has been performed, according to
         * how the Accelerator displays the component.
         *
         * This class serves as an interface and should be extended, not instantiated.
         *
         */
        .factory('RenderServiceInterface', createRenderServiceInterface);

    function createRenderServiceInterface($document, $window, $q, KEY_CODES, MOUSE_AND_KEYBOARD_EVENTS, OVERLAY_DISABLED, MODAL_DIALOG_ID, NONE_PERSPECTIVE, systemEventService, notificationService, componentHandlerService, perspectiveService) {

        var HOTKEY_NOTIFICATION_ID = 'HOTKEY_NOTIFICATION_ID';
        var HOTKEY_NOTIFICATION_TEMPLATE_URL = 'perspectiveSelectorHotkeyNotificationTemplate.html';

        var HOTKEY_NOTIFICATION_CONFIGURATION = {
            id: HOTKEY_NOTIFICATION_ID,
            templateUrl: HOTKEY_NOTIFICATION_TEMPLATE_URL
        };

        function RenderServiceInterface() {
            // Bind to document events
            this._bindEvents();
        }

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#renderSlots
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * Re-renders a slot in the page.
         *
         * @param {String} slotIds The slotsIds that need to be re-rendered.
         */
        RenderServiceInterface.prototype.renderSlots = function() {};

        // Proxied Functions
        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#renderComponent
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * Re-renders a component in the page.
         *
         * @param {String} componentId The ID of the component.
         * @param {String} componentType The type of the component.
         * @param {String} customContent The custom content to replace the component content with. If specified, the
         * component content will be rendered with it, instead of the accelerator's. Optional.
         *
         * @returns {Promise} Promise that will resolve on render success or reject if there's an error. When rejected,
         * the promise returns an Object{message, stack}.
         */
        RenderServiceInterface.prototype.renderComponent = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#renderRemoval
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * This method removes a component from a slot in the current page. Note that the component is only removed
         * on the frontend; the operation does not propagate to the backend.
         *
         * @param {String} componentId The ID of the component to remove.
         * @param {String} componentType The type of the component.
         *
         * @returns {Object} Object wrapping the removed component.
         */
        RenderServiceInterface.prototype.renderRemoval = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#renderPage
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * Re-renders all components in the page.
         * this method first resets the HTML content all of components to the values saved by {@link decoratorServiceModule.service:decoratorService#methods_storePrecompiledComponent} at the last $compile time
         * then requires a new compilation.
         */
        RenderServiceInterface.prototype.renderPage = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#toggleOverlay
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * Toggles on/off the visibility of the page overlay (containing the decorators).
         *
         * @param {Boolean} showOverlay Flag that indicates if the overlay must be displayed.
         */
        RenderServiceInterface.prototype.toggleOverlay = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#refreshOverlayDimensions
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * This method updates the position of the decorators in the overlay. Normally, this method must be executed every
         * time the original storefront content is updated to keep the decorators correctly positioned.
         *
         */
        RenderServiceInterface.prototype.refreshOverlayDimensions = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#blockRendering
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * Toggles the rendering to be blocked or not which determines whether the overlay should be rendered or not.
         *
         *@param {Boolean} isBlocked Flag that indicates if the rendering should be blocked or not.
         */
        RenderServiceInterface.prototype.blockRendering = function() {};

        /**
         * @ngdoc method
         * @name renderServiceInterfaceModule.service:RenderServiceInterface#isRenderingBlocked
         * @methodOf renderServiceInterfaceModule.service:RenderServiceInterface
         *
         * @description
         * This method returns a boolean that determines whether the rendering is blocked or not.
         *
         *@param {Boolean} An indicator if the rendering is blocked or not.
         */
        RenderServiceInterface.prototype.isRenderingBlocked = function() {};

        RenderServiceInterface.prototype._bindEvents = function() {

            $document.on(MOUSE_AND_KEYBOARD_EVENTS.KEY_UP, function(event) {
                this._shouldEnableKeyPressEvent(event).then(function(shouldEnableKeyPressEvent) {
                    if (shouldEnableKeyPressEvent) {
                        this._keyPressEvent();
                    }
                }.bind(this));
            }.bind(this));

            $document.on(MOUSE_AND_KEYBOARD_EVENTS.CLICK, function() {
                this._clickEvent();
            }.bind(this));

        };

        RenderServiceInterface.prototype._keyPressEvent = function() {
            this.isRenderingBlocked().then(function(isBlocked) {
                if (this._areAllModalWindowsClosed()) {
                    if (!isBlocked) {
                        this.blockRendering(true);
                        this.renderPage(false);
                        notificationService.pushNotification(HOTKEY_NOTIFICATION_CONFIGURATION);
                        systemEventService.sendAsynchEvent(OVERLAY_DISABLED);
                    } else {
                        this.blockRendering(false);
                        this.renderPage(true);
                        notificationService.removeNotification(HOTKEY_NOTIFICATION_ID);
                    }
                }
            }.bind(this));
        };

        RenderServiceInterface.prototype._clickEvent = function() {
            this.isRenderingBlocked().then(function(isBlocked) {
                if (isBlocked && !$window.frameElement) {
                    this.blockRendering(false);
                    this.renderPage(true);
                    notificationService.removeNotification(HOTKEY_NOTIFICATION_ID);
                }
            }.bind(this));
        };

        RenderServiceInterface.prototype._areAllModalWindowsClosed = function() {
            return componentHandlerService.getFromSelector('[id="' + MODAL_DIALOG_ID + '"]').length === 0;
        };

        RenderServiceInterface.prototype._shouldEnableKeyPressEvent = function(event) {
            try {
                if (componentHandlerService.getPageUUID()) {
                    return perspectiveService.isEmptyPerspectiveActive().then(function(isEmptyPerspectiveActive) {
                        return event.which === KEY_CODES.ESC && !isEmptyPerspectiveActive;
                    });
                }
            } catch (e) {
                return $q.when(false);
            }
            return $q.when(false);
        };

        return RenderServiceInterface;
    }
    createRenderServiceInterface.$inject = ['$document', '$window', '$q', 'KEY_CODES', 'MOUSE_AND_KEYBOARD_EVENTS', 'OVERLAY_DISABLED', 'MODAL_DIALOG_ID', 'NONE_PERSPECTIVE', 'systemEventService', 'notificationService', 'componentHandlerService', 'perspectiveService'];
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc service
 * @name sharedDataServiceInterfaceModule.SharedDataServiceInterface
 *
 * @description
 * Provides an abstract extensible shared data service. Used to store any data to be used either the SmartEdit
 * application or the SmartEdit container.
 *
 * This class serves as an interface and should be extended, not instantiated.
 */
angular.module('sharedDataServiceInterfaceModule', [])
    .factory('SharedDataServiceInterface', function() {

        function SharedDataServiceInterface() {}


        /** 
         * @ngdoc method
         * @name sharedDataServiceInterfaceModule.SharedDataServiceInterface#get
         * @methodOf sharedDataServiceInterfaceModule.SharedDataServiceInterface
         *
         * @description
         * Get the data for the given key.
         *
         * @param {String} key The key of the data to fetch
         */
        SharedDataServiceInterface.prototype.get = function() {};


        /** 
         * @ngdoc method
         * @name sharedDataServiceInterfaceModule.SharedDataServiceInterface#set
         * @methodOf sharedDataServiceInterfaceModule.SharedDataServiceInterface
         *
         * @description
         * Set data for the given key.
         *
         * @param {String} key The key of the data to set
         * @param {String} value The value of the data to set
         */
        SharedDataServiceInterface.prototype.set = function() {};

        /**
         * @ngdoc method
         * @name sharedDataServiceInterfaceModule.SharedDataServiceInterface#update
         * @methodOf sharedDataServiceInterfaceModule.SharedDataServiceInterface
         *
         * @description
         * Convenience method to retrieve and modify on the fly the content stored under a given key
         *
         * @param {String} key The key of the data to store
         * @param {Function} modifyingCallback callback fed with the value stored under the given key. The callback must return the new value of the object to update.
         */
        SharedDataServiceInterface.prototype.update = function() {};

        return SharedDataServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('toolbarInterfaceModule', ['functionsModule'])

/**
 * @ngdoc service
 * @name toolbarInterfaceModule.ToolbarServiceInterface
 *
 * @description
 * Provides an abstract extensible toolbar service. Used to manage and perform actions to either the SmartEdit
 * application or the SmartEdit container.
 *
 * This class serves as an interface and should be extended, not instantiated.
 */
.factory('ToolbarServiceInterface', ['$q', '$log', 'hitch', function($q, $log, hitch) {

    /////////////////////////////////////
    // ToolbarServiceInterface Prototype
    /////////////////////////////////////

    function ToolbarServiceInterface() {}

    ToolbarServiceInterface.prototype.getItems = function() {
        return this.actions;
    };

    ToolbarServiceInterface.prototype.getAliases = function() {
        return this.aliases;
    };

    /**
     * @ngdoc method
     * @name toolbarInterfaceModule.ToolbarServiceInterface#addItems
     * @methodOf toolbarInterfaceModule.ToolbarServiceInterface
     *
     * @description
     * Takes an array of actions and maps them internally for display and trigger through an internal callback key.
     * The action's properties are made available through the included template by a variable named 'item'.
     *
     * @param {Object[]} actions - List of actions
     * @param {String} actions.key - Unique identifier of the toolbar action item.
     * @param {Function} actions.callback - Callback triggered when this toolbar action item is clicked.
     * @param {String} actions.nameI18nkey - Name translation key
     * @param {String} actions.descriptionI18nkey - Description translation key
     * @param {String[]=} actions.icons - List of image URLs for the icon images (only for ACTION and HYBRID_ACTION)
     * @param {String} actions.type - TEMPLATE, ACTION, or HYBRID_ACTION
     * @param {String} actions.include - HTML template URL (only for TEMPLATE and HYBRID_ACTION)
     * @param {Integer} actions.priority - Determines the position of the item in the toolbar, ranging from 0-1000 with the default priority being 500.
     * An item with a higher priority number will be to the right of an item with a lower priority number in the toolbar.
     * @param {String} actions.section - Determines the sections(left, middle or right) of the item in the toolbar.
     * @param {String=} actions.iconClassName - List of classes used to display icons from fonts
     * @param {Boolean=} [actions.keepAliveOnClose=false] - Keeps the dropdown content in the DOM on close.
     */
    ToolbarServiceInterface.prototype.addItems = function(actions) {
        var aliases = actions.filter(hitch(this, function(action) {
            // Validate provided actions -> The filter will return only valid items.
            var includeAction = true;

            if (!action.key) {
                $log.error("addItems() - Cannot add action without key.");
                includeAction = false;
            } else if (action.key in this.actions) {
                $log.debug("addItems() - Action already exists in toolbar with key " + action.key);
                includeAction = false;
            }

            return includeAction;
        })).map(function(action) {
            var key = action.key;
            this.actions[key] = action.callback;
            return {
                key: key,
                name: action.nameI18nKey,
                iconClassName: action.iconClassName,
                description: action.descriptionI18nKey,
                icons: action.icons,
                type: action.type,
                include: action.include,
                className: action.className,
                priority: action.priority || 500,
                section: action.section || 'left',
                isOpen: false,
                keepAliveOnClose: action.keepAliveOnClose || false
            };
        }, this);

        if (aliases.length > 0) {
            this.addAliases(aliases);
        }
    };

    /////////////////////////////////////
    // Proxied Functions : these functions will be proxied if left unimplemented
    /////////////////////////////////////

    ToolbarServiceInterface.prototype.addAliases = function() {};

    ToolbarServiceInterface.prototype.removeItemByKey = function() {};

    ToolbarServiceInterface.prototype._removeItemOnInner = function() {};

    ToolbarServiceInterface.prototype.removeAliasByKey = function() {};

    ToolbarServiceInterface.prototype.addItemsStyling = function() {};

    ToolbarServiceInterface.prototype.triggerActionOnInner = function() {};

    return ToolbarServiceInterface;
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc service
 * @name urlServiceInterfaceModule.UrlServiceInterface
 *
 * @description
 * Provides an abstract extensible url service, Used to open a given URL
 * in a new browser url upon invocation. 
 * 
 *
 * This class serves as an interface and should be extended, not instantiated.
 */
angular.module('urlServiceInterfaceModule', ['resourceLocationsModule'])
    .factory('UrlServiceInterface', ['CONTEXT_SITE_ID', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION', function(CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, PAGE_CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION) {

        function UrlServiceInterface() {}


        /** 
         * @ngdoc method
         * @name urlServiceInterfaceModule.UrlServiceInterface#openUrlInPopup
         * @methodOf urlServiceInterfaceModule.UrlServiceInterface
         *
         * @description
         * Opens a given URL in a new browser pop up without authentication.
         *
         * @param {String} url - the URL we wish to open.
         */
        UrlServiceInterface.prototype.openUrlInPopup = function() {};

        /**
         * @ngdoc method
         * @name urlServiceInterfaceModule.UrlServiceInterface#path
         * @methodOf urlServiceInterfaceModule.UrlServiceInterface
         *
         * @description
         * Navigates to the given path in the same browser tab.
         *
         * @param {String} path - the path we wish to navigate to.
         */
        UrlServiceInterface.prototype.path = function() {};


        UrlServiceInterface.prototype.buildUriContext = function(siteId, catalogId, catalogVersion) {
            var uriContext = {};
            uriContext[CONTEXT_SITE_ID] = siteId;
            uriContext[CONTEXT_CATALOG] = catalogId;
            uriContext[CONTEXT_CATALOG_VERSION] = catalogVersion;
            return uriContext;
        };

        UrlServiceInterface.prototype.buildPageUriContext = function(siteId, catalogId, catalogVersion) {
            var uriContext = {};
            uriContext[PAGE_CONTEXT_SITE_ID] = siteId;
            uriContext[PAGE_CONTEXT_CATALOG] = catalogId;
            uriContext[PAGE_CONTEXT_CATALOG_VERSION] = catalogVersion;
            return uriContext;
        };

        return UrlServiceInterface;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yPopupOverlayModule
 * @description
 * This module provides the yPopupOverlay directive, and it's helper services
 */
angular.module('yPopupOverlayModule', [
    'yPopupOverlayUtilsModule',
    'yjqueryModule',
    'yLoDashModule',
    'compileHtmlModule',
    'componentHandlerServiceModule'
])

/**
 * A string, representing the prefix of the generated UUID for each yPopupOverlay.
 * This uuid is added as an attribute to the overlay DOM element.
 */
.constant('yPopupOverlayUuidPrefix', 'ypo-uuid-_')


/**
 *  @ngdoc directive
 *  @name yPopupOverlayModule.directive:yPopupOverlay
 *  @restrict A
 *
 *  @description
 *  The yPopupOverlay is meant to be a directive that allows popups/overlays to be displayed attached to any element.
 *  The element that the directive is applied to is called the anchor element. Once the popup is displayed, it is
 *  positioned relative to the anchor, depending on the configuration provided.<br />
 *  <br />
 *  <h3>Scrolling Limitation</h3>
 *  In this initial implementation, it appends the popup element to the body, and positions itself relative to body.
 *  This means that it can handle default window/body scrolling, but if the anchor is contained within an inner
 *  scrollable DOM element then the positions will not work correctly.
 *
 *  @param {< Object} yPopupOverlay A popup overlay configuration object that must contain either a template or a templateUrl
 *  @param {string} yPopupOverlay.template|templateUrl An html string template or a url to an html file
 *  @param {string =} [yPopupOverlay.halign='right'] Aligns the popup horizontally
 *      relative to the anchor (element). Accepts values: 'left' or 'right'.
 *  @param {string =} [yPopupOverlay.valign='bottom'] Aligns the popup vertically
 *      relative to the anchor (element). Accepts values: 'top' or 'bottom'.
 *  @param {@ string =} yPopupOverlayTrigger 'true'|'false'|'click' Controls when the overlay is displayed.
 *      If yPopupOverlayTrigger is true, the overlay is displayed, if false (or something other then true or click)
 *      then the overlay is hidden.
 *      If yPopupOverlayTrigger is 'click' then the overlay is displayed when the anchor (element) is clicked on
 *  @param {& expression =} yPopupOverlayOnShow An angular expression executed whenever this overlay is displayed
 *  @param {& expression =} yPopupOverlayOnHide An angular expression executed whenever this overlay is hidden
 */
.directive('yPopupOverlay', function() {
    return {
        restrict: 'A',
        scope: false,
        controllerAs: '$yPopupCtrl',
        controller: 'yPopupOverlayController'
    };
})

.controller('yPopupOverlayController', ['$scope', '$element', '$document', '$compile', '$attrs', '$timeout', '$interpolate', 'yjQuery', 'lodash', 'yPopupOverlayUuidPrefix', 'yPopupOverlayUtilsDOMCalculations', 'yPopupOverlayUtilsClickOrderService', 'componentHandlerService', function($scope,
    $element,
    $document,
    $compile,
    $attrs,
    $timeout,
    $interpolate,
    yjQuery,
    lodash,
    yPopupOverlayUuidPrefix,
    yPopupOverlayUtilsDOMCalculations,
    yPopupOverlayUtilsClickOrderService,
    componentHandlerService) {

    /**
     * Check if a yjQuery element contains a child element.
     * @param parentElement
     * @param childElement Click event target
     * @returns {boolean|*} True if parent contains child
     */
    function isChildOfElement(parentElement, childElement) {
        return parentElement[0] === childElement || yjQuery.contains(parentElement[0], childElement);
    }

    /**
     * Namespace to protect the non-isolated scope for this directive
     */
    function SafeController() {

        /**
         * Calculates the size of the popup content and stores it.
         * Returns true if the size has changed since the previous call.
         */
        this.checkPopupSizeChanged = function() {
            if (this.popupElement) {
                var firstChildOfRootPopupElement = this.popupElement.children().first();
                var popupBounds = firstChildOfRootPopupElement[0].getBoundingClientRect();
                var changed = popupBounds.width !== this.popupSize.width || popupBounds.height !== this.popupSize.height;
                this.popupSize = {
                    width: popupBounds.width,
                    height: popupBounds.height
                };
                if (changed) {
                    this.updatePopupElementPositionAndSize();
                }
            }
            return false;
        }.bind(this);

        /**
         *
         */
        this.updatePopupElementPositionAndSize = function() {
            if (this.popupElement) {
                try {
                    // Always calculate based on first child of popup, but apply css to root of popup
                    // otherwise any applied css may harm the content by enforcing size
                    var anchorBounds = $element[0].getBoundingClientRect();
                    var position = yPopupOverlayUtilsDOMCalculations.calculatePreferredPosition(anchorBounds,
                        this.popupSize.width, this.popupSize.height, this.config.valign, this.config.halign);
                    yPopupOverlayUtilsDOMCalculations.adjustHorizontalToBeInViewport(position);
                    this.popupElement.css(position);
                } catch (e) {
                    // There are racing conditions where some of the elements are not ready yet...
                    // Since we're constantly recalculating, this is just an easy way to avoid all these conditions
                }
            }
        }.bind(this);

        this.togglePoppup = function($event) {
            $event.stopPropagation();
            $event.preventDefault();
            if (this.popupDisplayed) {
                this.hide();
            } else {
                this.show();
            }
        }.bind(this);

        this.getTemplateString = function() {
            var outerElement = componentHandlerService.getFromSelector('<div>');
            outerElement.attr('data-uuid', this.uuid);
            outerElement.addClass('y-popover-outer');

            var innerElement;
            if (this.config.template) {
                innerElement = componentHandlerService.getFromSelector('<div>');
                innerElement.html(this.config.template);
            } else if (this.config.templateUrl) {
                innerElement = yjQuery('<data-ng-include>');
                innerElement.attr('src', "'" + this.config.templateUrl + "'");
            } else {
                throw "yPositiongetTemplateString() - Missing template";
            }

            innerElement.addClass('y-popover-inner');
            outerElement.append(innerElement);

            return outerElement[0].outerHTML;
        }.bind(this);


        this.hide = function() {
            if (this.popupDisplayed) {
                yPopupOverlayUtilsClickOrderService.unregister(this);
                if (this.popupElementScope) {
                    this.popupElementScope.$destroy();
                    this.popupElementScope = null;
                }
                if (this.popupElement) {
                    this.popupElement.remove();
                    this.popupElement = null;
                }
                if ($attrs.yPopupOverlayOnHide) {
                    // We want to evaluate this angular expression inside of a digest cycle
                    $timeout(function() {
                        $scope.$eval($attrs.yPopupOverlayOnHide);
                    });
                }
                this.resetPopupSize();
            }
            this.popupDisplayed = false;
        }.bind(this);

        this.show = function() {
            if (!this.popupDisplayed) {
                this.popupElement = this.getTemplateString();
                this.popupElementScope = $scope.$new(false);
                this.popupElement = $compile(this.popupElement)(this.popupElementScope);
                var containerElement = angular.element($document[0].body);
                this.updatePopupElementPositionAndSize();
                this.popupElement.appendTo(containerElement);
                angular.element(function() {
                    this.updatePopupElementPositionAndSize();
                    yPopupOverlayUtilsClickOrderService.register(this);
                    if ($attrs.yPopupOverlayOnShow) {
                        // We want to evaluate this angular expression inside of a digest cycle
                        $timeout(function() {
                            $scope.$eval($attrs.yPopupOverlayOnShow);
                        });
                    }

                }.bind(this));
            }
            this.popupDisplayed = true;
        }.bind(this);

        this.updateTriggers = function(newValue) {
            if (typeof newValue === 'undefined') {
                newValue = 'click';
            }
            if (this.oldTrigger === newValue) {
                return;
            }
            this.oldTrigger = newValue;
            if (this.untrigger) {
                this.untrigger();
            }
            if (newValue === 'click') {
                angular.element($element).on('click', this.togglePoppup);
                this.untrigger = function() {
                    angular.element($element).off('click', this.togglePoppup);
                };
                return;
            }
            if (newValue === "true" || newValue === true) {
                this.show();
            } else {
                this.hide();
            }
        }.bind(this);

        /**
         * Handles click event, triggered by the
         */
        this.onBodyElementClicked = function($event) {
            if (this.popupElement) {
                var isPopupClicked = isChildOfElement(this.popupElement, $event.target);
                var isAnchorClicked = isChildOfElement($element, $event.target);
                if (!isPopupClicked && !isAnchorClicked) {
                    this.hide();
                    $event.stopPropagation();
                    $event.preventDefault();
                    return true;
                }
            }
            return false;
        }.bind(this);

        this.resetPopupSize = function() {
            this.popupSize = {
                width: 0,
                height: 0
            };
        }.bind(this);

        this.$doCheck = function() {
            if (this.active) {
                this.checkPopupSizeChanged();
                var trigger = $interpolate($attrs.yPopupOverlayTrigger)($scope);
                if (trigger !== this.doCheckTrigger) {
                    this.doCheckTrigger = trigger;
                    this.updateTriggers(trigger);
                }
            }
        }.bind(this);

        this.$onInit = function() {
            this.uuid = lodash.uniqueId(yPopupOverlayUuidPrefix);
            this.popupDisplayed = false;
            this.config = $scope.$eval($attrs.yPopupOverlay);
            this.resetPopupSize();
            this.updateTriggers();

            // only activate
            this.active = this.config !== 'undefined';
        };

        this.$onDestroy = function() {
            if (this.untrigger) {
                this.untrigger();
            }
            this.hide();
        }.bind(this);
    }


    // --------------------------------------------
    // --------------- PUBLIC API -----------------
    // --------------------------------------------

    var safeController = new SafeController();

    this.$doCheck = function() {
        safeController.$doCheck();
    };

    this.$onInit = function() {
        safeController.$onInit();
    };

    this.$onDestroy = function() {
        safeController.$onDestroy();
    };

    // EXPOSED Close API for users to programmatically close
    // the popup via their template or templateUrl
    $scope.closePopupOverlay = function() {
        safeController.hide();
    }.bind(this);

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yPopupOverlayUtilsModule
 * @description
 * This module provides utility services for the {@link yPopupOverlayModule}
 */
angular.module('yPopupOverlayUtilsModule', [])

/**
 * @ngdoc service
 * @name yPopupOverlayUtilsModule.service:yPopupOverlayUtilsClickOrderService
 *
 * @description
 * A service that manages the click handlers for all {@link yPopupOverlayModule.directive:yPopupOverlay yPopupOverlay} overlay DOM elements.
 * When the user clicks outside of an overlay, the overlay should close, but in the case of multiple overlays
 * the click handlers are registered in the reverse order that want to execute them.<br />
 * <br />
 * So this service keeps a stack of all displayed popup overlays, and delegates clicks only to the top of the stack.
 */
.service('yPopupOverlayUtilsClickOrderService', ['$document', '$log', function($document, $log) {

    var controllerRegistry = [];

    function clickHandler($event) {
        if (controllerRegistry.length > 0) {
            controllerRegistry[0].onBodyElementClicked($event);
        }
    }

    this.register = function(instance) {
        var index = controllerRegistry.indexOf(instance);
        if (index === -1) {
            if (controllerRegistry.length === 0) {
                angular.element($document[0].body).on('click', clickHandler);
            }
            controllerRegistry.unshift(instance);
        } else {
            $log.warn('yPopupOverlayUtilsClickOrderService.onHide() - instance already registered');
        }
    };

    this.unregister = function(instance) {
        var index = controllerRegistry.indexOf(instance);
        if (index !== -1) {
            controllerRegistry.splice(index, 1);
        }
        if (controllerRegistry.length === 0) {
            angular.element($document[0].body).off('click', clickHandler);
        }
    };


}])

/**
 * @ngdoc service
 * @name yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations
 *
 * @description
 * Contains some {@link yPopupOverlayModule.directive:yPopupOverlay yPopupOverlay} helper functions for
 * calculating positions and sizes on the DOM
 */
.service('yPopupOverlayUtilsDOMCalculations', ['$window', '$document', function($window, $document) {

    function getScrollBarWidth() {
        if ($document[0].body.scrollHeight > $document[0].body.clientHeight) {
            var inner = $document[0].createElement('p');
            inner.style.width = "100%";
            inner.style.height = "200px";

            var outer = $document[0].createElement('div');
            outer.style.position = "absolute";
            outer.style.top = "0px";
            outer.style.left = "0px";
            outer.style.visibility = "hidden";
            outer.style.width = "200px";
            outer.style.height = "150px";
            outer.style.overflow = "hidden";
            outer.appendChild(inner);

            $document[0].body.appendChild(outer);
            var w1 = inner.offsetWidth;
            outer.style.overflow = 'scroll';
            var w2 = inner.offsetWidth;
            if (w1 === w2) {
                w2 = outer.clientWidth;
            }
            $document[0].body.removeChild(outer);
            return (w1 - w2);
        } else {
            return 0;
        }
    }

    /**
     * @ngdoc method
     * @name yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations#calculatePreferredPosition
     * @methodOf yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations
     *
     * @description
     * Calculates the preferred position of the overlay, based on the size and position of the anchor
     * and the size of the overlay element
     *
     * @param {Object} anchorBoundingClientRect A bounding rectangle representing the overlay's anchor
     * @param {number} anchorBoundingClientRect.top The top of the anchor, absolutely positioned
     * @param {number} anchorBoundingClientRect.right The right of the anchor, absolutely positioned
     * @param {number} anchorBoundingClientRect.bottom The bottom of the anchor, absolutely positioned
     * @param {number} anchorBoundingClientRect.left The left of the anchor, absolutely positioned
     * @param {number} targetWidth The width of the overlay element
     * @param {number} targetHeight The height of the overlay element
     * @param {string =} [targetValign='bottom'] The preferred vertical alignment, either 'top' or 'bottom'
     * @param {string =} [targetHalign='right'] The preferred horizontal alignment, either 'left' or 'right'
     *
     * @returns {Object} A new size and position for the overlay
     */
    this.calculatePreferredPosition = function(anchorBoundingClientRect, targetWidth,
        targetHeight, targetValign, targetHalign) {

        // fix for IE11, $window.scrollX/Y is undefined in IE
        var scrollX = $window.scrollX || $window.pageXOffset;
        var scrollY = $window.scrollY || $window.pageYOffset;

        var position = {
            width: targetWidth,
            height: targetHeight
        };

        switch (targetValign) {
            case 'top':
                position.top = anchorBoundingClientRect.top + scrollY - targetHeight;
                break;

            case 'bottom':
                /* falls through */
            default:
                position.top = anchorBoundingClientRect.bottom + scrollY;
        }

        switch (targetHalign) {
            case 'left':
                position.left = anchorBoundingClientRect.right + scrollX - targetWidth;
                break;

            case 'right':
                /* falls through */
            default:
                position.left = anchorBoundingClientRect.left + scrollX;
        }
        return position;
    };

    /**
     * @ngdoc method
     * @name yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations#adjustHorizontalToBeInViewport
     * @methodOf yPopupOverlayUtilsModule.service:yPopupOverlayUtilsDOMCalculations
     *
     * @description
     * Modifies the input rectangle to be absolutely positioned horizontally in the viewport.<br />
     * Does not modify vertical positioning.
     *
     * @param {Object} absPosition A rectangle object representing the size and absolutely positioned location of the overlay
     * @param {number} absPosition.left The left side of the overlay element
     * @param {number} absPosition.width The width of the overlay element
     */
    this.adjustHorizontalToBeInViewport = function(absPosition) {

        // HORIZONTAL POSITION / SIZE
        // if width of popup is wider then viewport, set it full width
        if (absPosition.width >= $window.innerWidth) {
            absPosition.left = 0;
            absPosition.width = $window.innerWidth;
        } else {
            var scrollWidth = getScrollBarWidth(); // maybe replace this with proper calculated value but im not sure if its worth the cpu cost
            // var scrollWidth = getScrollBarWidth();
            // if right edge of popup would be off the viewport on the right, then
            // move it left until right edge of popup is on right side of viewport
            if (absPosition.left - $window.scrollX + absPosition.width >= $window.innerWidth - scrollWidth) {
                absPosition.left = $window.innerWidth - absPosition.width - scrollWidth;
            }
            // if left edge is off the viewport to left, move to left edge
            if (absPosition.left - $window.scrollX <= 0) {
                absPosition.left = $window.scrollX;
            }
        }

    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name componentHandlerServiceModule
 * @description
 * 
 * this module aims at handling smartEdit components both on the original storefront and the smartEdit overlay
 * 
 */
angular.module('componentHandlerServiceModule', ['functionsModule', 'yjqueryModule'])

/**
 * @ngdoc object
 * @name componentHandlerServiceModule.OVERLAY_ID
 * @description
 * the identifier of the overlay placed in front of the storefront to where all smartEdit component decorated clones are copied.
 */
.constant('OVERLAY_ID', 'smarteditoverlay')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.COMPONENT_CLASS
     * @description
     * the css class of the smartEdit components as per contract with the storefront
     */
    .constant('COMPONENT_CLASS', 'smartEditComponent')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.OVERLAY_COMPONENT_CLASS
     * @description
     * the css class of the smartEdit component clones copied to the storefront overlay
     */
    .constant('OVERLAY_COMPONENT_CLASS', 'smartEditComponentX')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.SMARTEDIT_ATTRIBUTE_PREFIX
     * @description
     * If the storefront needs to expose more attributes than the minimal contract, these attributes must be prefixed with this constant value
     */
    .constant('SMARTEDIT_ATTRIBUTE_PREFIX', 'data-smartedit-')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.ID_ATTRIBUTE
     * @description
     * the id attribute of the smartEdit components as per contract with the storefront
     */
    .constant('ID_ATTRIBUTE', 'data-smartedit-component-id')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.UUID_ATTRIBUTE
     * @description
     * the uuid attribute of the smartEdit components as per contract with the storefront
     */
    .constant('UUID_ATTRIBUTE', 'data-smartedit-component-uuid')
    /**
     * @description
     * the front-end randomly generated uuid of the smartEdit components and their equivalent in the overlay
     */
    .constant('ELEMENT_UUID_ATTRIBUTE', 'data-smartedit-element-uuid')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.UUID_ATTRIBUTE
     * @description
     * the uuid attribute of the smartEdit components as per contract with the storefront
     */
    .constant('CATALOG_VERSION_UUID_ATTRIBUTE', 'data-smartedit-catalog-version-uuid')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.TYPE_ATTRIBUTE
     * @description
     * the type attribute of the smartEdit components as per contract with the storefront
     */
    .constant('TYPE_ATTRIBUTE', 'data-smartedit-component-type')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE
     * @description
     * the id attribute of the smartEdit container, when applicable, as per contract with the storefront
     */
    .constant('CONTAINER_ID_ATTRIBUTE', 'data-smartedit-container-id')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE
     * @description
     * the type attribute of the smartEdit container, when applicable, as per contract with the storefront
     */
    .constant('CONTAINER_TYPE_ATTRIBUTE', 'data-smartedit-container-type')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.CONTENT_SLOT_TYPE
     * @description
     * the type value of the smartEdit slots as per contract with the storefront
     */
    .constant('CONTENT_SLOT_TYPE', 'ContentSlot')
    /**
     * @ngdoc object
     * @name componentHandlerServiceModule.SMARTEDIT_IFRAME_ID
     * @description
     * the id of the iframe which contains storefront
     */
    .constant('SMARTEDIT_IFRAME_ID', 'ySmartEditFrame')

.constant('SMARTEDIT_IFRAME_WRAPPER_ID', '#js_iFrameWrapper')

/**
 * @ngdoc service
 * @name componentHandlerServiceModule.componentHandlerService
 * @description
 *
 * The service provides convenient methods to get DOM references of smartEdit components both in the original laye rof the storefornt and in the smartEdit overlay
 */
.factory(
    'componentHandlerService',
    ['yjQuery', '$window', 'isBlank', 'OVERLAY_ID', 'COMPONENT_CLASS', 'OVERLAY_COMPONENT_CLASS', 'ID_ATTRIBUTE', 'UUID_ATTRIBUTE', 'CATALOG_VERSION_UUID_ATTRIBUTE', 'TYPE_ATTRIBUTE', 'CONTAINER_ID_ATTRIBUTE', 'CONTAINER_TYPE_ATTRIBUTE', 'CONTENT_SLOT_TYPE', 'SMARTEDIT_IFRAME_ID', 'SMARTEDIT_IFRAME_WRAPPER_ID', function(yjQuery, $window, isBlank, OVERLAY_ID, COMPONENT_CLASS, OVERLAY_COMPONENT_CLASS, ID_ATTRIBUTE, UUID_ATTRIBUTE, CATALOG_VERSION_UUID_ATTRIBUTE, TYPE_ATTRIBUTE, CONTAINER_ID_ATTRIBUTE, CONTAINER_TYPE_ATTRIBUTE, CONTENT_SLOT_TYPE, SMARTEDIT_IFRAME_ID, SMARTEDIT_IFRAME_WRAPPER_ID) {

        var buildComponentQuery = function(smarteditComponentId, smarteditComponentType, cssClass) {
            var query = '';
            query += (cssClass ? '.' + cssClass : '');
            query += '[' + ID_ATTRIBUTE + '=\'' + smarteditComponentId + '\']';
            query += '[' + TYPE_ATTRIBUTE + '=\'' + smarteditComponentType + '\']';
            return query;
        };

        var buildComponentsInSlotQuery = function(slotId) {
            var query = '';
            query += ('.' + COMPONENT_CLASS);
            query += '[' + ID_ATTRIBUTE + '=\'' + slotId + '\']';
            query += '[' + TYPE_ATTRIBUTE + '=\'' + CONTENT_SLOT_TYPE + '\']';
            query += ' > ';
            query += '[' + ID_ATTRIBUTE + '\]';
            return query;
        };

        var buildComponentInSlotQuery = function(smarteditComponentId, smarteditComponentType, smarteditSlotId, cssClass) {
            var slotQuery = buildComponentQuery(smarteditSlotId, CONTENT_SLOT_TYPE);
            var componentQuery = buildComponentQuery(smarteditComponentId, smarteditComponentType, cssClass);
            return slotQuery + ' ' + componentQuery;
        };

        return {

            _isIframe: function() {
                return $window.frameElement;
            },

            _getTargetBody: function() {
                return this._isIframe() ? this.getFromSelector('body') : this._getTargetIframe().contents().find('body');
            },
            _getTargetIframe: function() {
                return this.getFromSelector('iframe#' + SMARTEDIT_IFRAME_ID);
            },
            _getTargetIframeWrapper: function() {
                return this.getFromSelector(SMARTEDIT_IFRAME_WRAPPER_ID);
            },


            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getPageUID
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * This extracts the pageUID of the storefront page loaded in the smartedit iframe.
             *
             * @return {String} a string matching the page's ID
             */
            getPageUID: function() {
                try {
                    return /smartedit-page-uid\-(\S+)/.exec(this._getTargetBody().attr('class'))[1];
                } catch (e) {
                    throw {
                        name: "InvalidStorefrontPageError",
                        message: "Error detected. The page is not a valid storefront page."
                    };
                }
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getPageUUID
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * This extracts the pageUUID of the storefront page loaded in the smartedit iframe.
             * The UUID is different from the UID in that it is an encoding of uid and catalog version combined
             *
             * @return {String} The page's UUID
             */
            getPageUUID: function() {
                try {
                    return /smartedit-page-uuid\-(\S+)/.exec(this._getTargetBody().attr('class'))[1];
                } catch (e) {
                    throw {
                        name: "InvalidStorefrontPageError",
                        message: "Error detected. The page is not a valid storefront page."
                    };
                }
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getCatalogVersionUUIDFromPage
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * This extracts the catalogVersionUUID of the storefront page loaded in the smartedit iframe.
             * The UUID is different from the UID in that it is an encoding of uid and catalog version combined
             *
             * @return {String} The page's UUID
             */
            getCatalogVersionUUIDFromPage: function() {
                try {
                    return /smartedit-catalog-version-uuid\-(\S+)/.exec(this._getTargetBody().attr('class'))[1];
                } catch (e) {
                    throw {
                        name: "InvalidStorefrontPageError",
                        message: "Error detected. The page is not a valid storefront page."
                    };
                }
            },

            buildOverlayQuery: function() {
                return '#' + OVERLAY_ID;
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getFromSelector
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * This is a wrapper around yjQuery selector
             *
             * @param {String} selector String selector as per yjQuery API
             * 
             * @return {Object} a yjQuery object for the given selector
             */
            getFromSelector: function(selector) {
                return yjQuery(selector);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOverlay
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves a handler on the smartEdit overlay div
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             */
            getOverlay: function() {
                return this.getFromSelector(this.buildOverlayQuery());
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_isOverlayOn
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * determines whether the overlay is visible
             * This method can only be invoked from the smartEdit application and not the smartEdit iframe.
             */
            isOverlayOn: function() {
                return this.getOverlay().is(":visible");
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getComponentUnderSlot
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component identified by its smartEdit id, smartEdit type and an optional class
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             *
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * @param {String} smarteditSlotId the slot id of the slot containing the component as per the smartEdit contract with the storefront
             * @param {String} cssClass the css Class to further restrict the search on. This parameter is optional.
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getComponentUnderSlot: function(smarteditComponentId, smarteditComponentType, smarteditSlotId, cssClass) {
                return this.getFromSelector(buildComponentInSlotQuery(smarteditComponentId, smarteditComponentType, smarteditSlotId, cssClass));
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component identified by its smartEdit id, smartEdit type and an optional class
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             *
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * @param {String} cssClass the css Class to further restrict the search on. This parameter is optional.
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getComponent: function(smarteditComponentId, smarteditComponentType, cssClass) {
                return this.getFromSelector(buildComponentQuery(smarteditComponentId, smarteditComponentType, cssClass));
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOriginalComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component of the original storefront layer identified by its smartEdit id, smartEdit type
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getOriginalComponent: function(smarteditComponentId, smarteditComponentType) {
                return this.getComponent(smarteditComponentId, smarteditComponentType, COMPONENT_CLASS);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOriginalComponentWithinSlot
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component of the original storefront layer identified by its smartEdit id, smartEdit type and slot ID
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * @param {String} slotId the ID of the slot within which the component resides
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getOriginalComponentWithinSlot: function(smarteditComponentId, smarteditComponentType, slotId) {
                return this.getComponentUnderSlot(smarteditComponentId, smarteditComponentType, slotId, COMPONENT_CLASS);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOverlayComponentWithinSlot
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component of the overlay layer identified by its smartEdit id, smartEdit type and slot ID
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * @param {String} slotId the ID of the slot within which the component resides
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getOverlayComponentWithinSlot: function(smarteditComponentId, smarteditComponentType, slotId) {
                return this.getComponentUnderSlot(smarteditComponentId, smarteditComponentType, slotId, OVERLAY_COMPONENT_CLASS);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOverlayComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around the smartEdit component of the overlay layer corresponding to the storefront layer component passed as argument
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {Object} originalComponent the DOM element in the storefront layer
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             */
            getOverlayComponent: function(originalComponent) {
                var slotId = this.getParentSlotForComponent(originalComponent.parent());
                if (slotId) {
                    //it is not a slot
                    return this.getComponentUnderSlot(originalComponent.attr(ID_ATTRIBUTE), originalComponent.attr(TYPE_ATTRIBUTE), slotId, OVERLAY_COMPONENT_CLASS);
                } else {
                    //it is a slot
                    return this.getComponent(originalComponent.attr(ID_ATTRIBUTE), originalComponent.attr(TYPE_ATTRIBUTE), OVERLAY_COMPONENT_CLASS);
                }
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getComponentInOverlay
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a smartEdit component of the overlay div identified by its smartEdit id, smartEdit type
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * 
             * @return {Object} a yjQuery object wrapping the searched component
             * @deprecated since 6.5, use {@link componentHandlerServiceModule.componentHandlerService#methodsOf_getOverlayComponentWithinSlot getOverlayComponentWithinSlot} or {@link componentHandlerServiceModule.componentHandlerService#methodsOf_getOverlayComponent getOverlayComponent} 
             */
            getComponentInOverlay: function(smarteditComponentId, smarteditComponentType) {
                return this.getComponent(smarteditComponentId, smarteditComponentType, OVERLAY_COMPONENT_CLASS);
            },

            getComponentUnderParentOverlay: function(smarteditComponentId, smarteditComponentType, parentOverlay) {
                return this.getFromSelector(parentOverlay)
                    .find(buildComponentQuery(smarteditComponentId, smarteditComponentType, OVERLAY_COMPONENT_CLASS));
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getParent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the direct smartEdit component parent of a given component.
             * The parent is fetched in the same layer (original storefront or smartEdit overlay) as the child 
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             *
             * @param {Object} component the yjQuery component for which to search a parent
             * 
             * @return {Object} a yjQuery object wrapping the smae-layer parent component
             */
            getParent: function(component) {
                var parentClassToLookFor = component.hasClass(COMPONENT_CLASS) ? COMPONENT_CLASS : (component.hasClass(OVERLAY_COMPONENT_CLASS) ? OVERLAY_COMPONENT_CLASS : null);
                if (isBlank(parentClassToLookFor)) {
                    throw "componentHandlerService.getparent.error.component.from.unknown.layer";
                }
                return component.closest("." + parentClassToLookFor + "[" + ID_ATTRIBUTE + "]" + "[" + ID_ATTRIBUTE + "!='" + component.attr(ID_ATTRIBUTE) + "']");
            },



            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getClosestSmartEditComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Returns the closest parent (or self) being a smartEdit component
             *
             * @param {Object} component the DOM/yjQuery element for which to search a parent
             */
            getClosestSmartEditComponent: function(component) {
                var wrappedComponent = this.getFromSelector(component);
                return this.getFromSelector(wrappedComponent.closest("." + COMPONENT_CLASS));
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_isSmartEditComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Determines whether a DOM/yjQuery element is a smartEdit component
             *
             * @param {Object} component the DOM/yjQuery element
             */
            isSmartEditComponent: function(component) {
                return this.getFromSelector(component).hasClass(COMPONENT_CLASS);
            },


            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_setId
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Sets the smartEdit component id of a given component
             *
             * @param {Object} component the yjQuery component for which to set the id
             * @param {String} id the id to be set
             */
            setId: function(component, id) {
                return this.getFromSelector(component).attr(ID_ATTRIBUTE, id);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getId
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the smartEdit component id of a given component
             *
             * @param {Object} component the yjQuery component for which to get the id
             * 
             * @return {String} the component id
             */
            getId: function(component) {
                return this.getFromSelector(component).attr(ID_ATTRIBUTE);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getUuid
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the smartEdit component id of a given component
             *
             * @param {Object} component the yjQuery component for which to get the id
             * 
             * @return {String} the component id
             */
            getUuid: function(component) {
                return this.getFromSelector(component).attr(UUID_ATTRIBUTE);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getCatalogVersionUuid
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the smartEdit component id of a given component
             *
             * @param {Object} component the yjQuery component for which to get the id
             * 
             * @return {String} the component id
             */
            getCatalogVersionUuid: function(component) {
                return this.getFromSelector(component).attr(CATALOG_VERSION_UUID_ATTRIBUTE);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getSlotOperationRelatedId
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the id that is relevant to be able to perform slot related operations for this components
             * It typically is {@link componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE} when applicable and defaults to {@link componentHandlerServiceModule.ID_ATTRIBUTE}
             *
             * @param {Object} component the yjQuery component for which to get the id
             * 
             * @return {String} the slot operations related id
             */
            getSlotOperationRelatedId: function(component) {
                var containerId = this.getFromSelector(component).attr(CONTAINER_ID_ATTRIBUTE);
                return containerId && this.getFromSelector(component).attr(CONTAINER_TYPE_ATTRIBUTE) ? containerId : this.getFromSelector(component).attr(ID_ATTRIBUTE);
            },

            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getSlotOperationRelatedUuid
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the id that is relevant to be able to perform slot related operations for this components
             * It typically is {@link componentHandlerServiceModule.CONTAINER_ID_ATTRIBUTE} when applicable and defaults to {@link componentHandlerServiceModule.ID_ATTRIBUTE}
             *
             * @param {Object} component the yjQuery component for which to get the Uuid
             *
             * @return {String} the slot operations related Uuid
             */
            getSlotOperationRelatedUuid: function(component) {
                var containerId = this.getFromSelector(component).attr(CONTAINER_ID_ATTRIBUTE);
                return containerId && this.getFromSelector(component).attr(CONTAINER_TYPE_ATTRIBUTE) ? containerId : this.getFromSelector(component).attr(UUID_ATTRIBUTE);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_setType
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Sets the smartEdit component type of a given component
             *
             * @param {Object} component the yjQuery component for which to set the type
             * @param {String} type the type to be set
             */
            setType: function(component, type) {
                return this.getFromSelector(component).attr(TYPE_ATTRIBUTE, type);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getType
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the smartEdit component type of a given component
             *
             * @param {Object} component the yjQuery component for which to get the type
             * 
             * @return {String} the component type
             */
            getType: function(component) {
                return this.getFromSelector(component).attr(TYPE_ATTRIBUTE);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getSlotOperationRelatedType
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Gets the type that is relevant to be able to perform slot related operations for this components
             * It typically is {@link componentHandlerServiceModule.CONTAINER_TYPE_ATTRIBUTE} when applicable and defaults to {@link componentHandlerServiceModule.TYPE_ATTRIBUTE}
             *
             * @param {Object} component the yjQuery component for which to get the type
             * 
             * @return {String} the slot operations related type
             */
            getSlotOperationRelatedType: function(component) {
                var containerType = this.getFromSelector(component).attr(CONTAINER_TYPE_ATTRIBUTE);
                return containerType && this.getFromSelector(component).attr(CONTAINER_ID_ATTRIBUTE) ? containerType : this.getFromSelector(component).attr(TYPE_ATTRIBUTE);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getAllComponentsSelector
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery selector matching all smartEdit components that are not of type ContentSlot
             */
            getAllComponentsSelector: function() {
                return "." + COMPONENT_CLASS + "[" + TYPE_ATTRIBUTE + "!='ContentSlot']";
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getAllSlotsSelector
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery selector matching all smartEdit components that are of type ContentSlot
             */
            getAllSlotsSelector: function() {
                return "." + COMPONENT_CLASS + "[" + TYPE_ATTRIBUTE + "='ContentSlot']";
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getParentSlotForComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the the slot ID for a given element
             * 
             * @param {Object} the DOM element which represents the component
             * 
             * @return {String} the slot ID for that particular component
             */
            getParentSlotForComponent: function(component) {
                var parent = component.closest('[' + TYPE_ATTRIBUTE + '=' + CONTENT_SLOT_TYPE + ']');
                return parent.attr(ID_ATTRIBUTE);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getParentSlotUuidForComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the the slot Uuid for a given element
             * 
             * @param {Object} the DOM element which represents the component
             * 
             * @return {String} the slot Uuid for that particular component
             */
            getParentSlotUuidForComponent: function(component) {
                var parent = component.closest('[' + TYPE_ATTRIBUTE + '=' + CONTENT_SLOT_TYPE + ']');
                return parent.attr(UUID_ATTRIBUTE);
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getOriginalComponentsWithinSlot
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Retrieves the yjQuery wrapper around a list of smartEdit components contained in the slot identified by the given slotId.
             * This method can only be invoked from the smartEdit application and not the smartEdit container.
             * 
             * @param {String} slotId the ID of the slot within which the component resides
             * 
             * @return {Object} a yjQuery object wrapping the searched components
             */
            getOriginalComponentsWithinSlot: function(slotId) {
                return this.getFromSelector(buildComponentsInSlotQuery(slotId));
            },
            /**
             * @ngdoc method
             * @name componentHandlerServiceModule.componentHandlerService#methodsOf_isExternalComponent
             * @methodOf componentHandlerServiceModule.componentHandlerService
             *
             * @description
             * Determines whether the component identified by the provided smarteditComponentId and smarteditComponentType
             * resides in a different catalog version to the one of the current page.  
             * 
             * @param {String} smarteditComponentId the component id as per the smartEdit contract with the storefront
             * @param {String} smarteditComponentType the component type as per the smartEdit contract with the storefront
             * 
             * @return {Boolean} flag that evaluates to true if the component resides in a catalog version different to 
             * the one of the current page.  False otherwise. 
             */
            isExternalComponent: function(smarteditComponentId, smarteditComponentType) {
                var component = this.getOriginalComponent(smarteditComponentId, smarteditComponentType);
                var componentCatalogVersionUuid = this.getCatalogVersionUuid(component);

                return (componentCatalogVersionUuid !== this.getCatalogVersionUUIDFromPage());
            },


            /**
                 * @ngdoc method
                 * @name componentHandlerServiceModule.componentHandlerService#methodsOf_getFirstSmartEditComponentChildren
                 * @methodOf componentHandlerServiceModule.componentHandlerService
                 *
                 * @description
                 * This method can only be invoked from the smartEdit application and not the smartEdit container.
                 * Get first level smartEdit component children for a given node, regardless how deep they are found.
                 * The returned children may have different depths relatively to the parent:
                 * Example: a call on the body would return 4 components with ids: 1,2,3,4
                 * <pre>
                 * <body>
        		 	    <div>
        			        <component smartedit-component-id="1">
        			            <component smartedit-component-id="1_1"></component>
        			        </component>
        			        <component smartedit-component-id="2">
        			            <component smartedit-component-id="2_1"></component>
        			        </component>
        			    </div>

        			    <component smartedit-component-id="3">
        			        <component smartedit-component-id="3_1"></component>
        			    </component>

        			    <div>
        			        <div>
        			            <component smartedit-component-id="4">
        			                <component smartedit-component-id="4_1"></component>
        			            </component>
        			        </div>
        			    </div>
        			</body>
                 * </pre>
                 */
            getFirstSmartEditComponentChildren: function(seComponent) {
                var stem = this.getFromSelector(seComponent);
                var parentCssPath = stem.getCssPath();
                var firstChildrenRegex = new RegExp(COMPONENT_CLASS, 'g');
                var self = this;
                return stem.find("." + COMPONENT_CLASS).filter(function() {
                    var match = self.getFromSelector(this).getCssPath().replace(parentCssPath, "").match(firstChildrenRegex);
                    return match && match.length === 1;
                });
            }
        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('alertsBoxModule', []).directive('alertsBox', function() {
    return {
        templateUrl: 'alertsTemplate.html',
        restrict: 'E', // it kicks in on <alerts-box> elements
        transclude: true,
        replace: false,
        scope: {
            alerts: '=',
        },
        link: function(scope) {
            scope.dismissAlert = function(index) {
                scope.alerts.splice(index, 1);
            };
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name hasOperationPermissionModule
 * @description
 * This module provides a directive used to determine if the current user has permission to perform the action defined
 * by a given permission key(s), and removes/adds elements from the DOM accordingly.
 */
angular.module('hasOperationPermissionModule', ['eventServiceModule', 'permissionServiceModule', 'yLoDashModule'])

.controller('hasOperationPermissionController', ['$log', 'EVENTS', 'systemEventService', 'permissionService', 'lodash', function($log, EVENTS, systemEventService, permissionService, lodash) {

    this.refreshIsPermissionGranted = function() {
        permissionService.isPermitted(this._validateAndPreparePermissions(this.hasOperationPermission)).then(function(isPermissionGranted) {
            this.isPermissionGranted = isPermissionGranted;
        }.bind(this), function(error) {
            $log.error('Failed to retrieve authorization', error);
            this.isPermissionGranted = false;
        }.bind(this));
    };

    this._validateAndPreparePermissions = function(permissions) {
        if (typeof permissions !== 'string' && !Array.isArray(permissions)) {
            throw new Error("Permission should be string or an array of objects");
        }

        var preparedPermissions = lodash.cloneDeep(permissions);
        if (typeof permissions === 'string') {
            preparedPermissions = [{
                names: permissions.split(",")
            }];
        }
        return preparedPermissions;
    };

    this.$onInit = function() {
        this._unregisterHandler = systemEventService.registerEventHandler(EVENTS.AUTHORIZATION_SUCCESS, this.refreshIsPermissionGranted.bind(this));
    };

    this.$onChanges = function(changesObject) {
        if (changesObject.hasOperationPermission) {
            this.isPermissionGranted = false;
            this.refreshIsPermissionGranted();
        }
    };

    this.$onDestroy = function() {
        this._unregisterHandler();
    };

}])

/**
 * @ngdoc directive
 * @name hasOperationPermissionModule.directive:hasOperationPermission
 * @scope
 * @restrict A
 * @element ANY
 *
 * @description
 * Authorization HTML mark-up that will remove elements from the DOM if the user does not have authorization defined
 * by the input parameter permission keys. This directive makes use of the {@link permissionServiceInterfaceModule.service:PermissionServiceInterface PermissionServiceInterface}
 * permissionService} service to validate if the current user has access to the given permission set or not.
 *
 * It takes a comma-separated list of permission names or an array of permission name objects structured as follows:
 *
 * {
 *     names: "permission.names.separated.by.commas",
 *     context: {
 *         data: "with the context property, extra data can be included to check a permission when the Rule.verify function is called"
 *     }
 * }
 *
 *
 * @param {< String || Object[]} has-operation-permission A comma-separated list of permission names or an array of permission name objects.
 */
.directive('hasOperationPermission', function() {
    return {
        transclude: true,
        restrict: 'A',
        templateUrl: 'hasOperationPermissionTemplate.html',
        controller: 'hasOperationPermissionController',
        controllerAs: 'ctrl',
        scope: {},
        bindToController: {
            hasOperationPermission: '<'
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name authorizationModule.authorizationService
 * @description
 * The authorization module provides a service that checks if the current user was granted certain
 * permissions by contacting the Global Permissions REST API.
 * 
 * This module makes use of the {@link restServiceFactoryModule restServiceFactoryModule} to poll the
 * Global Permissions REST API in the backend.
 */
angular.module('authorizationModule', ['restServiceFactoryModule', 'loadConfigModule', 'storageServiceModule', 'resourceLocationsModule'])
    /**
     * @ngdoc service
     * @name authorizationModule.AuthorizationService
     *
     * @description
     * This service makes calls to the Global Permissions REST API to check if the current user was
     * granted certain permissions.
     */
    .service('authorizationService', ['$log', 'restServiceFactory', 'storageService', 'USER_GLOBAL_PERMISSIONS_RESOURCE_URI', function($log, restServiceFactory, storageService, USER_GLOBAL_PERMISSIONS_RESOURCE_URI) {
        var permissionsResource = restServiceFactory.get(USER_GLOBAL_PERMISSIONS_RESOURCE_URI);

        /*
         * This method will look for the result for the given permission name. If found, it is
         * verified that it has been granted. Otherwise, the method will return false.
         */
        var getPermissionResult = function(permissionResults, permissionName) {
            var permission = permissionResults.find(function(permission) {
                return permission.key.toLowerCase() === permissionName.toLowerCase();
            });

            return !!permission && permission.value === 'true';
        };

        /*
         * This method merges permission results. It iterates through the list of permission names that
         * were checked and evaluates if the permission is granted. It immediately returns false when
         * it encounters a permission that is denied.
         */
        var mergePermissionResults = function(permissionResults, permissionNames) {
            var hasPermission = !!permissionNames && permissionNames.length > 0;
            var index = 0;

            while (hasPermission && index < permissionNames.length) {
                hasPermission = hasPermission && getPermissionResult(permissionResults, permissionNames[index++]);
            }

            return hasPermission;
        };

        /*
         * This method makes a call to the Global Permissions API with the given permission names
         * and returns the list of results.
         */
        var getPermissions = function(permissionNames) {
            return storageService.getPrincipalIdentifier().then(function(user) {
                if (!user) {
                    return [];
                }

                return permissionsResource.get({
                    user: user,
                    permissionNames: permissionNames.join(',')
                }).then(function(response) {
                    return response.permissions;
                });
            });
        };

        /**
         * @ngdoc method
         * @name authorizationModule.AuthorizationService#canPerformOperation
         * @methodOf authorizationModule.AuthorizationService
         *
         * @description
         * This method checks if the current user has been granted the permissions required to
         * perform a certain operation.
         *
         * @param {String} permissionNames A string of comma separated values that contains the global permissions to check.
         * 
         * @return {Boolean} true if the user is granted all of the given permissions, false otherwise
         * 
         * @throws Will throw an error if the permissionNames string is empty
         * 
         * @deprecated since version 6.4. Use {@link authorizationModule.AuthorizationService#hasGlobalPermissions hasGlobalPermissions()} instead.
         */
        this.canPerformOperation = function(permissionNames) {
            var permissionNamesArray = !!permissionNames ? permissionNames.split(',') : [];

            return this.hasGlobalPermissions(permissionNamesArray);
        };

        /**
         * @ngdoc method
         * @name authorizationModule.AuthorizationService#hasGlobalPermissions
         * @methodOf authorizationModule.AuthorizationService
         *
         * @description
         * This method checks if the current user is granted the given global permissions.
         *
         * @param {String[]} permissionNames The list of global permissions to check.
         * 
         * @return {Boolean} true if the user is granted all of the given permissions, false otherwise
         * 
         * @throws Will throw an error if the permissionNames parameter is not an array
         * @throws Will throw an error if the permissionNames array is empty
         */
        this.hasGlobalPermissions = function(permissionNames) {
            if (!(permissionNames instanceof Array)) {
                throw 'permissionNames must be an array';
            }

            if (permissionNames.length < 1) {
                throw 'permissionNames cannot be empty';
            }

            var onSuccess = function(permissions) {
                return mergePermissionResults(permissions, permissionNames);
            };

            var onError = function() {
                $log.error('AuthorizationService - Failed to determine authorization for the following permissions: ' + permissionNames.toString());
                return false;
            };

            return getPermissions(permissionNames).then(onSuccess, onError);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    angular.module('browserServiceModule', [])
        .constant('SUPPORTED_BROWSERS', {
            IE: 'IE',
            CHROME: 'Chrome',
            FIREFOX: 'Firefox',
            EDGE: 'Edge',
            SAFARI: 'Safari',
            UNKNOWN: 'Uknown'
        })
        .service('browserService', ['$window', 'SUPPORTED_BROWSERS', function($window, SUPPORTED_BROWSERS) {

            this.getCurrentBrowser = function() {
                var browser = SUPPORTED_BROWSERS.UNKNOWN;
                if (typeof InstallTrigger !== 'undefined') {
                    browser = SUPPORTED_BROWSERS.FIREFOX;
                } else if ( /*@cc_on!@*/ false || !!document.documentMode) {
                    browser = SUPPORTED_BROWSERS.IE;
                } else if (!!window.StyleMedia) {
                    browser = SUPPORTED_BROWSERS.EDGE;
                } else if (!!window.chrome && !!window.chrome.webstore) {
                    browser = SUPPORTED_BROWSERS.CHROME;
                } else if (this.isSafari()) {
                    browser = SUPPORTED_BROWSERS.SAFARI;
                }

                return browser;
            }.bind(this);

            /*
                It is always better to detect a browser via features. Unfortunately, it's becoming really hard to identify 
                Safari, since newer versions do not match the previous ones. Thus, we have to rely on User Agent as the last
                option. 
            */
            this.isSafari = function() {
                var userAgent = $window.navigator.userAgent;
                var vendor = $window.navigator.vendor;

                var testFeature = /constructor/i.test(function HTMLElementConstructor() {});
                var testUserAgent = vendor && vendor.indexOf('Apple') > -1 && userAgent && !userAgent.match('CriOS');

                return testFeature || testUserAgent;
            };

        }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('catalogAwareRouteResolverModule', [])
    .constant('catalogAwareRouteResolverFunctions', {
        /**
         * This function initializes new experience based on route params. It will redirect current user to the landing page
         * if the user doesn't have a read permission to the current catalog version. If the user has read permission for the
         * catalog version then EVENTS.EXPERIENCE_UPDATE is sent, but only when the experience has been changed.
         *
         * This function can be assigned to the resolve property of any route.
         */
        setExperience: ['$log', '$q', '$route', '$location', 'experienceService', 'sharedDataService', 'systemEventService', 'EVENTS', 'LANDING_PAGE_PATH', 'catalogVersionPermissionService',
            function($log, $q, $route, $location, experienceService, sharedDataService, systemEventService, EVENTS, LANDING_PAGE_PATH, catalogVersionPermissionService) {
                var experienceUpdated = function(prev, next) {
                    return (prev === undefined ||
                        (next.catalogDescriptor.catalogId !== prev.catalogDescriptor.catalogId) ||
                        (next.catalogDescriptor.catalogVersion !== prev.catalogDescriptor.catalogVersion));
                };

                var prepareExperiences = function() {
                    return experienceService.buildDefaultExperience($route.current.params).then(function(nextExperience) {
                        return sharedDataService.get('experience').then(function(previousExperience) {
                            return sharedDataService.set('experience', nextExperience).then(function() {
                                return $q.when({
                                    previousExperience: previousExperience,
                                    nextExperience: nextExperience
                                });
                            });
                        });
                    }, function(buildError) {
                        $log.error("the provided path could not be parsed: " + $location.url());
                        $log.error(buildError);
                        $location.url(LANDING_PAGE_PATH);
                    });
                };

                var verifyCatalogReadPermission = function() {
                    return catalogVersionPermissionService.hasReadPermissionOnCurrent().then(function(hasReadPermission) {
                        if (!hasReadPermission) {
                            $location.url(LANDING_PAGE_PATH);
                        }
                    }, function() {
                        $location.url(LANDING_PAGE_PATH);
                    });
                };

                return prepareExperiences().then(function(experiences) {
                    return verifyCatalogReadPermission().then(function() {
                        if (experienceUpdated(experiences.previousExperience, experiences.nextExperience)) {
                            return systemEventService.sendAsynchEvent(EVENTS.EXPERIENCE_UPDATE);
                        }
                    });
                });
            }
        ]
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {
    /**
     * @ngdoc overview
     * @name compileHtmlModule
     * @description
     * # The compileHtmlModule
     *
     * The compileHtmlModule provides a directive to evaluate and compile HTML markup.
     *
     */
    angular.module('compileHtmlModule', [])

    /**
     * @ngdoc directive
     * @name compileHtmlModule.directive:compileHtml
     * @scope
     * @restrict A
     * @attribute compile-html
     *
     * @description
     * Directive responsible for evaluating and compiling HTML markup.
     *
     * @param {String} String HTML string to be evaluated and compiled.
     * @example
     * <pre>
     *      <div compile-html="<a data-ng-click=\"injectedContext.onLink( item.path )\">{{ item[key.property] }}</a>"></div>
     * </pre>
     **/
    .directive('compileHtml', ['$compile', function($compile) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.$watch(
                    function(scope) {
                        return scope.$eval(attrs.compileHtml);
                    },
                    function(value) {
                        element.html(value);
                        $compile(element.contents())(scope);
                    }
                );
            }
        };
    }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seConstantsModule', [])
    .constant('DATE_CONSTANTS', {
        ANGULAR_FORMAT: 'short',
        MOMENT_FORMAT: 'M/D/YY h:mm A',
        MOMENT_ISO: 'YYYY-MM-DDTHH:mm:00ZZ',
        ISO: 'yyyy-MM-ddTHH:mm:00Z'
    })
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_CHANGED
     * @description
     * The ID of the event that is triggered when the perspective (known as mode for users) is changed.
     */
    .constant('EVENT_PERSPECTIVE_CHANGED', 'EVENT_PERSPECTIVE_CHANGED')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_ADDED
     * @description
     * The ID of the event that is triggered when a new perspective (known as mode for users) is registered.
     */
    .constant('EVENT_PERSPECTIVE_ADDED', 'EVENT_PERSPECTIVE_ADDED')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_UNLOADING
     * @description
     * The ID of the event that is triggered when a perspective is about to be unloaded.
     * This event is triggered immediately before the features are disabled.
     */
    .constant('EVENT_PERSPECTIVE_UNLOADING', 'EVENT_PERSPECTIVE_UNLOADING')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:EVENT_PERSPECTIVE_REFRESHED
     * @description
     * The ID of the event that is triggered when the perspective (known as mode for users) is refreshed.
     */
    .constant('EVENT_PERSPECTIVE_REFRESHED', 'EVENT_PERSPECTIVE_REFRESHED')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:ALL_PERSPECTIVE
     * @description
     * The key of the default All Perspective.
     */
    .constant('ALL_PERSPECTIVE', 'se.all')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:NONE_PERSPECTIVE
     * @description
     * The key of the default None Perspective.
     */
    .constant('NONE_PERSPECTIVE', 'se.none')
    /**
     * @ngdoc object
     * @name seConstantsModule.object:VALIDATION_MESSAGE_TYPES
     * @description
     * Validation message types
     */
    .constant('VALIDATION_MESSAGE_TYPES', {
        VALIDATION_ERROR: 'ValidationError',
        WARNING: 'Warning'
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('crossFrameEventServiceModule', ['gatewayFactoryModule', 'eventServiceModule'])
    .constant('CROSS_FRAME_EVENT', 'CROSS_FRAME_EVENT')
    .factory('crossFrameEventServiceGateway', ['CROSS_FRAME_EVENT', 'gatewayFactory', function(CROSS_FRAME_EVENT, gatewayFactory) {
        return gatewayFactory.createGateway(CROSS_FRAME_EVENT);
    }])
    /**
     * @ngdoc service
     * @name crossFrameEventServiceModule.service:CrossFrameEventService
     *
     * @description
     * The Cross Frame Event Service is responsible for publishing and subscribing events within and between frames.
     * It uses {@link gatewayFactoryModule.gatewayFactory gatewayFactory} and {@link eventServiceModule.EventService EventService} to transmit events.
     * 
     */
    .factory('crossFrameEventService', ['$q', 'systemEventService', 'crossFrameEventServiceGateway', function($q, systemEventService, crossFrameEventServiceGateway) {

        var CrossFrameEventService = function() {

            /**
             * @ngdoc method
             * @name crossFrameEventServiceModule.service:CrossFrameEventService#publish
             * @methodOf crossFrameEventServiceModule.service:CrossFrameEventService
             *
             * @description
             * Publishes an event within and across the gateway.
             *
             * The publish method is used to send events using {@link eventServiceModule.EventService#sendEvent sendEvent} of
             * {@link eventServiceModule.EventService EventService} and as well send the message across the gateway by using 
             * {@link gatewayFactoryModule.MessageGateway#publish publish} of the {@link gatewayFactoryModule.gatewayFactory gatewayFactory}.
             *
             * @param {String} eventId Event identifier
             * @param {String} data The event payload
             * @returns {Promise} Promise to resolve
             */
            this.publish = function(eventId, data) {
                return $q.all(systemEventService.sendAsynchEvent(eventId, data), crossFrameEventServiceGateway.publish(eventId, data));
            };

            /**
             * @ngdoc method
             * @name crossFrameEventServiceModule.service:CrossFrameEventService#subscribe
             * @methodOf crossFrameEventServiceModule.service:CrossFrameEventService
             *
             * @description
             * Subscribe to an event across both frames.
             *
             * The subscribe method is used to register for listening to events using registerEventHandler method of
             * {@link eventServiceModule.EventService EventService} and as well send the registration message across the gateway by using 
             * {@link gatewayFactoryModule.MessageGateway#subscribe subscribe} of the {@link gatewayFactoryModule.gatewayFactory gatewayFactory}.
             *
             * @param {String} eventId Event identifier
             * @param {Function} handler Callback function to be invoked
             * @returns {Function} The function to call in order to unsubscribe the event listening; this will unsubscribe both from the systemEventService and the crossFrameEventServiceGatway
             */
            this.subscribe = function(eventId, handler) {
                var systemEventServiceUnsubscribeFn = systemEventService.registerEventHandler(eventId, handler);
                var crossFrameEventServiceGatewayUnsubscribeFn = crossFrameEventServiceGateway.subscribe(eventId, handler);

                var unSubscribeFn = function() {
                    systemEventServiceUnsubscribeFn();
                    crossFrameEventServiceGatewayUnsubscribeFn();
                };

                return unSubscribeFn;
            };
        };
        return new CrossFrameEventService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    angular.module('_dragAndDropScrollingModule', ['yjqueryModule', 'browserServiceModule', 'yLoDashModule'])
        .service('_dragAndDropScrollingService', ['$window', '$translate', 'yjQuery', 'lodash', 'browserService', 'SUPPORTED_BROWSERS', function($window, $translate, yjQuery, lodash, browserService, SUPPORTED_BROWSERS) {

            // Constants
            this._SCROLLING_AREA_HEIGHT = 50;
            this._FAST_SCROLLING_AREA_HEIGHT = 25;

            this._SCROLLING_STEP = 5;
            this._FAST_SCROLLING_STEP = 15;

            var TOP_SCROLL_AREA_ID = 'top_scroll_page';

            // Variables
            this._topScrollArea = null;
            this._bottomScrollArea = null;

            this._initialize = function() {
                var currentBrowser = browserService.getCurrentBrowser();
                this._scrollable = (currentBrowser === SUPPORTED_BROWSERS.FIREFOX || currentBrowser === SUPPORTED_BROWSERS.IE) ?
                    this._getSelector('html') : this._getSelector('body');

                this._addScrollAreas();
                this._addEventListeners();

                this._scrollDelta = 0;
                this._initialized = true;
            };

            this._deactivate = function() {
                this._removeEventListeners();

                this._scrollDelta = 0;
                this._initialized = false;
            };

            this._enable = function() {
                if (this._initialized) {
                    // Calculate limits based on current state.
                    this._scrollLimitY = this._scrollable.outerHeight(true) - $window.innerHeight;
                    this._showScrollAreas();
                }
            };

            this._disable = function() {
                if (this._initialized) {
                    var scrollAreas = this._getScrollAreas();
                    scrollAreas.hide();
                }
            };

            this._addScrollAreas = function() {
                this._topScrollArea = this._getSelector('<div id="top_scroll_page" class="ySECmsScrollArea"></div>').appendTo('body');
                this._bottomScrollArea = this._getSelector('<div id="bottom_scroll_page" class="ySECmsScrollArea"></div>').appendTo('body');

                var scrollAreas = this._getScrollAreas();
                scrollAreas.height(this._SCROLLING_AREA_HEIGHT);

                this._topScrollArea.css({
                    top: 0
                });
                this._bottomScrollArea.css({
                    bottom: 0
                });

                scrollAreas.hide();

                var topMessage, bottomMessage;
                $translate('se.draganddrop.uihint.top').then(function(localizedTopMessage) {
                    topMessage = localizedTopMessage;
                    return $translate('se.draganddrop.uihint.bottom');
                }).then(function(localizedBottomMsg) {
                    bottomMessage = localizedBottomMsg;

                    this._topScrollArea.text(topMessage);
                    this._bottomScrollArea.text(bottomMessage);
                }.bind(this));
            };

            this._addEventListeners = function() {
                var scrollAreas = this._getScrollAreas();

                scrollAreas.on('dragenter', this._onDragEnter.bind(this));
                scrollAreas.on('dragover', this._onDragOver.bind(this));
                scrollAreas.on('dragleave', this._onDragLeave.bind(this));
            };

            this._removeEventListeners = function() {
                var scrollAreas = this._getScrollAreas();

                scrollAreas.off('dragenter');
                scrollAreas.off('dragover');
                scrollAreas.off('dragleave');

                scrollAreas.remove();
            };

            // Event Listeners
            this._onDragEnter = function(event) {
                var scrollDelta = this._SCROLLING_STEP;
                var scrollArea = this._getSelector(event.target);
                var scrollAreaId = scrollArea.attr('id');
                if (scrollAreaId === TOP_SCROLL_AREA_ID) {
                    scrollDelta *= -1;
                }

                this._scrollDelta = scrollDelta;

                this._animationFrameId = $window.requestAnimationFrame(this._scrollPage.bind(this));
            };

            this._onDragOver = function(evt) {
                var event = evt.originalEvent;
                var scrollArea = this._getSelector(event.target);
                var scrollAreaId = scrollArea.attr('id');

                if (scrollAreaId === TOP_SCROLL_AREA_ID) {
                    if (event.clientY <= this._FAST_SCROLLING_AREA_HEIGHT) {
                        this._scrollDelta = -this._FAST_SCROLLING_STEP;
                    } else {
                        this._scrollDelta = -this._SCROLLING_STEP;
                    }
                } else {
                    var windowHeight = this._getSelector($window).height();

                    if (event.clientY >= windowHeight - this._FAST_SCROLLING_AREA_HEIGHT) {
                        this._scrollDelta = this._FAST_SCROLLING_STEP;
                    } else {
                        this._scrollDelta = this._SCROLLING_STEP;
                    }
                }

            };

            this._onDragLeave = function() {
                this._scrollDelta = 0;
                $window.cancelAnimationFrame(this._animationFrameId);
            };

            this._scrollPage = function() {
                if (this._scrollDelta) {
                    var scrollTop = this._scrollable.scrollTop();
                    var continueScrolling = false;

                    if (this._scrollDelta > 0 && scrollTop < this._scrollLimitY) {
                        continueScrolling = true;
                    } else if (this._scrollDelta < 0 && scrollTop > 0) {
                        continueScrolling = true;
                    }

                    if (continueScrolling) {
                        this._scrollable.scrollTop(function(i, currentValue) {
                            return currentValue + this._scrollDelta;
                        }.bind(this));

                        this._animationFrameId = $window.requestAnimationFrame(this._scrollPage.bind(this));
                    }

                    this._showScrollAreas();
                }
            };

            this._getSelector = function(selector) {
                return yjQuery(selector);
            };

            this._getScrollAreas = function() {
                return yjQuery('#top_scroll_page, #bottom_scroll_page');
            };

            this._showScrollAreas = function() {
                var scrollTop = this._scrollable.scrollTop();

                if (scrollTop === 0) {
                    this._topScrollArea.hide();
                } else {
                    this._topScrollArea.show();
                }

                if (scrollTop >= this._scrollLimitY) {
                    this._bottomScrollArea.hide();
                } else {
                    this._bottomScrollArea.show();
                }
            };
        }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    /**
     * @ngdoc overview
     * @name dragAndDropServiceModule
     * @description
     * # The dragAndDropServiceModule
     *
     * The dragAndDropServiceModule provides a service that wraps over the HTML 5 drag and drop functionality.
     *
     */
    angular.module('dragAndDropServiceModule', ['yjqueryModule', 'yLoDashModule', '_dragAndDropScrollingModule'])

    /**
     * @ngdoc service
     * @name dragAndDropServiceModule.service:dragAndDropService
     *
     * @description
     * The dragAndDropService wraps over the HTML 5 drag and drop functionality to provide a browser independent interface and very
     * basic functionality, like scrolling. The service is intended to be used by another service to provide a richer drag and drop
     * experience.
     */
    .service('dragAndDropService', ['$timeout', 'yjQuery', 'lodash', '_dragAndDropScrollingService', function($timeout, yjQuery, lodash, _dragAndDropScrollingService) {
        // Constants
        var DRAGGABLE_ATTR = 'draggable';
        var DROPPABLE_ATTR = 'data-droppable';

        // Variables
        this.configurations = {};
        this._isDragAndDropExecuting = false;

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#register
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method registers a new instance of the drag and drop service.
         * Note: Registering doesn't start the service. It just provides the configuration, which later must be applied with the apply method.
         *
         * @param {Object} configuration The drag and drop configuration
         * @param {String} configuration.id The identifier of the new drag and drop instance. This parameter must be unique, as it is used to track the instance in the service.
         * @param {String} configuration.sourceSelector This parameter specifies the yjQuery selector used to locate the draggable elements managed by the current instance of the drag and drop service.
         * @param {String} configuration.targetSelector This parameter specifies the yjQuery selector used to locate the droppable elements where draggable items of this instance can be dropped.
         * @param {Function} configuration.startCallback This callback is executed when draggable elements start dragging.
         * @param {Function} configuration.dragEnterCallback This callback is executed when the mouse enters a droppable area during a drag and drop operation.
         * @param {Function} configuration.dragOverCallback This callback is executed when the mouse hovers a droppable area during a drag and drop operation.
         * @param {Function} configuration.dropCallback This callback is executed when a draggable element is dropped over a droppable area during a drag and drop operation.
         * @param {Function} configuration.outCallback This callback is executed when the mouse leaves a droppable area during a drag and drop operation.
         * @param {Function} configuration.stopCallback This callback is executed when draggable elements stop dragging.
         * @param {Function} configuration.helper This function is called to specify a custom drag image. Note: IE does not support this functionality, so custom drag images are ignored for that browser.
         * @param {Boolean} configuration.enableScrolling This flag specifies whether to enable scrolling while dragging or not.
         *
         */
        this.register = function(configuration) {
            // Validate
            if (!configuration || !configuration.id) {
                throw new Error('dragAndDropService - register(): Configuration needs an ID.');
            }

            this.configurations[configuration.id] = configuration;
        };

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#unregister
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method removes the drag and drop instances specified by the provided IDs.
         *
         * @param {Array} configurationsIDList The array of drag and drop configuration IDs to remove.
         *
         */
        this.unregister = function(configurationsIDList) {
            configurationsIDList.forEach(function(configurationID) {
                var configuration = this.configurations[configurationID];
                if (configuration) {
                    this._deactivateConfiguration(configuration);
                    this._deactivateScrolling(configuration);
                    delete this.configurations[configurationID];
                }
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#applyAll
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method applies all drag and drop configurations registered.
         *
         */
        this.applyAll = function() {
            lodash.forEach(this.configurations, function(currentConfig) {
                this.apply(currentConfig.id);
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#apply
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method apply the configuration specified by the provided ID in the current page. After this method is executed drag and drop can be started by the user.
         *
         * @param {String} configurationID The identifier of the drag and drop configuration to apply in the current page.
         *
         */
        this.apply = function(configurationID) {
            var configuration = this.configurations[configurationID];
            if (configuration) {
                this.update(configuration);
                this._cacheDragImages(configuration);
                this._initializeScrolling(configuration);
            }
        };

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#update
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method updates the drag and drop instance specified by the provided ID in the current page. It is important to execute this method every time a draggable or droppable element
         * is added or removed from the page DOM.
         *
         * @param {String} configurationID The identifier of the drag and drop instance to update.
         *
         */
        this.update = function(configurationID) {
            var configuration = this.configurations[configurationID];
            if (configuration) {
                this._deactivateConfiguration(configuration);
                this._update(configuration);
            }
        };

        this._update = function(configuration) {
            var currentService = this;

            var sourceSelectors = lodash.isArray(configuration.sourceSelector) ? configuration.sourceSelector : [configuration.sourceSelector];

            sourceSelectors.forEach(function(sourceSelector) {
                var draggableElements = this._getSelector(sourceSelector).filter(function() {
                    return !currentService._getSelector(this).attr(DRAGGABLE_ATTR);
                });

                draggableElements.attr(DRAGGABLE_ATTR, true);

                draggableElements.on('dragstart', this._onDragStart.bind(this, configuration));
                draggableElements.on('dragend', this._onDragEnd.bind(this, configuration));

            }.bind(this));

            var droppableElements = this._getSelector(configuration.targetSelector).filter(function() {
                return !currentService._getSelector(this).attr(DROPPABLE_ATTR);
            });

            droppableElements.attr(DROPPABLE_ATTR, true); // Not needed by HTML5. It's to mark element as processed.

            droppableElements.on('dragenter', this._onDragEnter.bind(this, configuration));
            droppableElements.on('dragover', this._onDragOver.bind(this, configuration));
            droppableElements.on('drop', this._onDrop.bind(this, configuration));
            droppableElements.on('dragleave', this._onDragLeave.bind(this, configuration));
        };

        this._deactivateConfiguration = function(configuration) {
            var draggableElements = this._getSelector(configuration.sourceSelector);
            var droppableElements = this._getSelector(configuration.targetSelector);

            draggableElements.removeAttr(DRAGGABLE_ATTR);
            droppableElements.removeAttr(DROPPABLE_ATTR);

            draggableElements.off('dragstart');
            draggableElements.off('dragend');

            droppableElements.off('dragenter');
            droppableElements.off('dragover');
            droppableElements.off('dragleave');
            droppableElements.off('drop');
        };

        // Draggable Listeners
        this._onDragStart = function(configuration, yjQueryEvent) {

            // The native transferData object is modified outside the $timeout since it can only be modified 
            // inside the dragStart event handler (otherwise an exception is thrown by the browser).
            var evt = yjQueryEvent.originalEvent;
            this._setDragTransferData(configuration, evt);

            // Necessary because there's a bug in Chrome (and probably Safari) where dragEnd is triggered right after
            // dragStart whenever DOM is modified in the event handler. The problem can be circumvented by using $timeout.
            $timeout(function() {

                this._setDragAndDropExecutionStatus(true);

                _dragAndDropScrollingService._enable();

                if (configuration.startCallback) {
                    configuration.startCallback(evt);
                }
            }.bind(this), 0);
        };

        this._onDragEnd = function(configuration, yjQueryEvent) {
            var evt = yjQueryEvent.originalEvent;
            _dragAndDropScrollingService._disable();

            if (this._isDragAndDropExecuting && configuration.stopCallback) {
                configuration.stopCallback(evt);
            }

            this._setDragAndDropExecutionStatus(false);
        };

        // Droppable Listeners
        this._onDragEnter = function(configuration, yjQueryEvent) {
            var evt = yjQueryEvent.originalEvent;
            evt.preventDefault();

            if (this._isDragAndDropExecuting && configuration.dragEnterCallback) {
                configuration.dragEnterCallback(evt);
            }
        };

        this._onDragOver = function(configuration, yjQueryEvent) {
            var evt = yjQueryEvent.originalEvent;
            evt.preventDefault();

            if (this._isDragAndDropExecuting && configuration.dragOverCallback) {
                configuration.dragOverCallback(evt);
            }
        };

        this._onDrop = function(configuration, yjQueryEvent) {
            var evt = yjQueryEvent.originalEvent;
            evt.preventDefault(); // Necessary to receive the on drop event. Otherwise, other handlers are executed.
            evt.stopPropagation();

            if (evt.relatedTarget && evt.relatedTarget.nodeType === 3) {
                return;
            }
            if (evt.target === evt.relatedTarget) {
                return;
            }

            if (this._isDragAndDropExecuting && configuration.dropCallback) {
                configuration.dropCallback(evt);
            }

            return false;
        };

        this._onDragLeave = function(configuration, yjQueryEvent) {
            var evt = yjQueryEvent.originalEvent;
            evt.preventDefault();

            if (this._isDragAndDropExecuting && configuration.outCallback) {
                configuration.outCallback(evt);
            }
        };

        // Helper Functions
        this._cacheDragImages = function(configuration) {
            var helperImg = null;
            if (configuration.helper) {
                helperImg = configuration.helper();
            }

            if (helperImg) {
                if (typeof helperImg === 'string') {
                    configuration._cachedDragImage = new Image();
                    configuration._cachedDragImage.src = helperImg;
                } else {
                    configuration._cachedDragImage = helperImg;
                }
            }
        };

        this._setDragTransferData = function(configuration, evt) {
            /*
                Note: Firefox recently added some restrictions to their drag and drop functionality; it only
                allows starting drag and drop operations if there's data present in the dataTransfer object. 
                Otherwise, the whole operation fails silently. Thus, some data needs to be added. 
            */
            evt.dataTransfer.setData('Text', configuration.id);

            if (configuration._cachedDragImage && evt.dataTransfer.setDragImage) {
                evt.dataTransfer.setDragImage(configuration._cachedDragImage, 0, 0);
            }
        };

        this._getSelector = function(selector) {
            return yjQuery(selector);
        };

        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#markDragStarted
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method forces the page to prepare for a drag and drop operation. This method is necessary when the drag and drop operation is started somewhere else,
         * like on a different iFrame.
         *
         */
        this.markDragStarted = function() {
            this._setDragAndDropExecutionStatus(true);
            _dragAndDropScrollingService._enable();
        };

        // Method used to stop drag and drop from another frame.
        /**
         * @ngdoc method
         * @name dragAndDropServiceModule.service:dragAndDropService#markDragStarted
         * @methodOf dragAndDropServiceModule.service:dragAndDropService
         *
         * @description
         * This method forces the page to clean after a drag and drop operation. This method is necessary when the drag and drop operation is stopped somewhere else,
         * like on a different iFrame.
         *
         */
        this.markDragStopped = function() {
            this._setDragAndDropExecutionStatus(false);
            _dragAndDropScrollingService._disable();
        };

        this._setDragAndDropExecutionStatus = function(isExecuting) {
            this._isDragAndDropExecuting = isExecuting;
        };

        this._initializeScrolling = function(configuration) {
            if (configuration.enableScrolling && this._browserRequiresCustomScrolling()) {
                _dragAndDropScrollingService._initialize();
            }
        };

        this._deactivateScrolling = function(configuration) {
            if (configuration.enableScrolling && this._browserRequiresCustomScrolling()) {
                _dragAndDropScrollingService._deactivate();
            }
        };

        this._browserRequiresCustomScrolling = function() {
            // NOTE: It'd be better to identify if native scrolling while dragging is enabled in the browser, but
            // currently there's no way to know. Thus, browser fixing is necessary.

            return true;
        };
    }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('yDropDownMenuModule', [])
    .controller('yDropDownController', function() {

        this.$onInit = function() {
            this.dropdownItems.forEach(function(item) {
                item.condition = item.condition || function() {
                    return true;
                };
            });
        };
    })
    /**
     * @ngdoc directive
     * @name yDropDownMenuModule.directive:yDropDownMenu
     * @scope
     * @restrict E
     * @description
     * yDropDownMenu builds a drop-down menu. It has two parameters dropdownItems and selectedItem. The dropdownItems is an array of object which contains an key, condition and callback function. 
     * The callback function will be called when the user click on the drop down item.
     * The selectedItem is the object associated to the drop-down. It is passed as argument for the callback of dropdownItems.
     * For a given item, if a condition callback is defined, the item will show only if this callback returns true
     * Example:
     * <pre>
     *	this.dropdownItems = [{
     *       key: 'pagelist.dropdown.edit',
     *       condition:function(item){
     *       	return true;
     *       },
     *       callback: function(item) {
     *           pageEditorModalService.open(item).then(function(response) {
     *               this.reloadUpdatedPage(item.uid, response.uid);
     *           }.bind(this));
     *       }.bind(this)
     *   }, {
     *       key: 'pagelist.dropdown.sync',
     *       condition:function(item){
     *       	return false;
     *       },
     *       callback: function(item) {
     *           alert('not yet implemented');
     *       }
     *   }, {
     *       key: 'pagelist.dropdown.hide',
     *       callback: function(item) {
     *           alert('not yet implemented');
     *       }
     *   }];
     * </pre>
     */
    .component(
        'yDropDownMenu', {
            templateUrl: 'yDropdownMenuTemplate.html',
            transclude: true,
            controller: 'yDropDownController',
            controllerAs: 'dropdown',
            bindings: {
                dropdownItems: '<',
                selectedItem: '<'
            }
        }
    );

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name eventServiceModule
 * @description
 *
 * eventServiceModule contains an event service which is supported by the SmartEdit {@link gatewayFactoryModule.gatewayFactory gatewayFactory} to propagate events between SmartEditContainer and SmartEdit.
 *
 */
angular.module('eventServiceModule', ['functionsModule'])

/**
 * @ngdoc object
 * @name eventServiceModule.EVENT_SERVICE_MODE_ASYNCH
 *
 * @description
 * A constant used in the constructor of the Event Service to specify asynchronous event transmission.
 */
.constant('EVENT_SERVICE_MODE_ASYNCH', 'EVENT_SERVICE_MODE_ASYNCH')

/**
 * @ngdoc object
 * @name eventServiceModule.EVENT_SERVICE_MODE_SYNCH
 *
 * @description
 * A constant that is used in the constructor of the Event Service to specify synchronous event transmission.
 */
.constant('EVENT_SERVICE_MODE_SYNCH', 'EVENT_SERVICE_MODE_SYNCH')

/**
 * @ngdoc object
 * @name eventServiceModule.EVENTS
 *
 * @description
 * Events that are fired/handled in the SmartEdit application
 */
.constant('EVENTS', {
    AUTHORIZATION_SUCCESS: 'AUTHORIZATION_SUCCESS',
    LOGOUT: 'SE_LOGOUT_EVENT',
    CLEAR_PERSPECTIVE_FEATURES: 'CLEAR_PERSPECTIVE_FEATURES',
    EXPERIENCE_UPDATE: 'experienceUpdate',
    PAGE_CHANGE: 'PAGE_CHANGE'
})

/**
 * @ngdoc service
 * @name eventServiceModule.EventService
 * @description
 *
 * The event service is used to transmit events synchronously or asynchronously. It also contains options to send
 * events, as well as register and unregister event handlers.
 *
 * @param {String} defaultMode Uses constants to set event transmission. The EVENT_SERVICE_MODE_ASYNCH constant sets
 * event transmission to asynchronous mode and the EVENT_SERVICE_MODE_SYNCH constant sets the event transmission to
 * synchronous mode.
 *
 */
.factory('EventService', ['customTimeout', '$q', '$log', 'hitch', 'toPromise', 'EVENT_SERVICE_MODE_ASYNCH', 'EVENT_SERVICE_MODE_SYNCH', function(customTimeout, $q, $log, hitch, toPromise, EVENT_SERVICE_MODE_ASYNCH, EVENT_SERVICE_MODE_SYNCH) {
    var EventService = function(defaultMode) {


        this.eventHandlers = {};

        this.mode = EVENT_SERVICE_MODE_ASYNCH;
        if (defaultMode === EVENT_SERVICE_MODE_ASYNCH || defaultMode === EVENT_SERVICE_MODE_SYNCH) {
            this.mode = defaultMode;
        }

        this._recursiveCallToEventHandlers = function(eventId, data) {
            return $q.all(this.eventHandlers[eventId].map(function(event) {
                var promiseClosure = toPromise(event);
                return promiseClosure(eventId, data);
            })).then(function(results) {
                return results.pop();
            });
        };

        /**
         * @ngdoc method
         * @name eventServiceModule.EventService#sendEvent
         * @methodOf eventServiceModule.EventService
         *
         * @description
         * Send the event with data. The event is sent either synchronously or asynchronously depending on the event
         * mode.
         *
         * @param {String} eventId The identifier of the event.
         * @param {String} data The event payload.
         */
        this.sendEvent =
            function(eventId, data) {
                if (this.mode === EVENT_SERVICE_MODE_ASYNCH) {
                    this.sendAsynchEvent(eventId, data);
                } else if (this.mode === EVENT_SERVICE_MODE_SYNCH) {
                    this.sendSynchEvent(eventId, data);
                } else {
                    throw ('Unknown event service mode: ' + this.mode);
                }
            };

        /**
         * @ngdoc method
         * @name eventServiceModule.EventService#sendEvent
         * @methodOf eventServiceModule.EventService
         *
         * @description
         * send the event with data synchronously.
         *
         * @param {String} eventId The identifier of the event.
         * @param {String} data The event payload.
         */
        this.sendSynchEvent = function(eventId, data) {
            var deferred = $q.defer();
            if (!eventId) {
                $log.error('Failed to send event. No event ID provided for data: ' + data);
                deferred.reject();
                return;
            }
            if (this.eventHandlers[eventId] && this.eventHandlers[eventId].length > 0) {
                this._recursiveCallToEventHandlers(eventId, data).then(
                    function(resolvedDataOfLastSubscriber) {
                        deferred.resolve(resolvedDataOfLastSubscriber);
                    },
                    function(error) {
                        deferred.reject(error);
                    }
                );
            } else {
                deferred.resolve();
            }
            return deferred.promise;
        };

        this.sendAsynchEvent = function(eventId, data) {
            var deferred = $q.defer();
            customTimeout(hitch(this, function() {
                this.sendSynchEvent(eventId, data).then(
                    function(resolvedData) {
                        deferred.resolve(resolvedData);
                    },
                    function(error) {
                        deferred.reject(error);
                    }
                );
            }), 0);
            return deferred.promise;
        };

        this.registerEventHandler = function(eventId, handler) {
            if (!eventId || !handler) {
                $log.error('Failed to register event handler for event: ' + eventId);
                return;
            }
            // create handlers array for this event if not already created
            if (this.eventHandlers[eventId] === undefined) {
                this.eventHandlers[eventId] = [];
            }
            this.eventHandlers[eventId].push(handler);

            var unregisterFn = function() {
                this.unRegisterEventHandler(eventId, handler);
            }.bind(this);

            return unregisterFn;
        };

        this.unRegisterEventHandler = function(eventId, handler) {
            var handlersArray = this.eventHandlers[eventId];
            var index = handlersArray ? this.eventHandlers[eventId].indexOf(handler) : -1;
            if (index >= 0) {
                this.eventHandlers[eventId].splice(index, 1);
            } else {
                $log.warn('Attempting to remove event handler for ' + eventId + ' but handler not found.');
            }
        };
    };



    return EventService;
}])

.factory('systemEventService', ['EventService', function(EventService) {
    var es = new EventService();
    return es;
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('experienceInterceptorModule', ['interceptorHelperModule', 'functionsModule', 'sharedDataServiceModule', 'restServiceFactoryModule', 'resourceLocationsModule', 'yLoDashModule'])
    /**
     * @ngdoc service
     * @name ExperienceInterceptorModule.experienceInterceptor
     *
     * @description
     * A HTTP request interceptor which intercepts all 'cmswebservices/catalogs' requests and adds the current catalog and version
     * from any URI which define the variables 'CURRENT_CONTEXT_CATALOG' and 'CURRENT_CONTEXT_CATALOG_VERSION' in the URL.
     *
     *
     * Note: The interceptors are service factories that are registered with the $httpProvider by adding them to the $httpProvider.interceptors array.
     * The factory is called and injected with dependencies and returns the interceptor object with contains the interceptor methods.
     */
    .factory('experienceInterceptor', ['hitch', 'lodash', 'sharedDataService', 'interceptorHelper', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', 'CMSWEBSERVICES_PATH', 'CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG', 'PAGE_CONTEXT_CATALOG_VERSION', 'PAGE_CONTEXT_SITE_ID', function(hitch, lodash, sharedDataService, interceptorHelper, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, CMSWEBSERVICES_PATH, CONTEXT_SITE_ID, PAGE_CONTEXT_CATALOG, PAGE_CONTEXT_CATALOG_VERSION, PAGE_CONTEXT_SITE_ID) {

        /**
         * @ngdoc method
         * @name ExperienceInterceptorModule.experienceInterceptor#request
         * @methodOf ExperienceInterceptorModule.experienceInterceptor
         *
         * @description
         * Interceptor method which gets called with a http config object, intercepts any 'cmswebservices/catalogs' requests and adds
         * the current catalog and version
         * from any URI which define the variables 'CURRENT_CONTEXT_CATALOG' and 'CURRENT_CONTEXT_CATALOG_VERSION' in the URL.
         * If the request URI contains any of 'PAGE_CONTEXT_SITE_ID', 'PAGE_CONTEXT_CATALOG' or 'PAGE_CONTEXT_CATALOG_VERSION', 
         * then it is replaced by the siteId/catalogId/catalogVersion of the current page in context.
         *
         * The catalog name and catalog versions of the current experience and the page loaded are stored in the shared data service object called 'experience' during preview initialization
         * and here we retrieve those details and set it to headers.
         *
         * @param {Object} config the http config object that holds the configuration information.
         *
         * @returns {Promise} Returns a {@link https://docs.angularjs.org/api/ng/service/$q promise} of the passed config object.
         */
        var request = function request(config) {
            return interceptorHelper.handleRequest(config, function() {
                if (CMSWEBSERVICES_PATH.test(config.url)) {
                    return sharedDataService.get('experience').then(function(data) {
                        if (data) {

                            var keys = {};
                            keys.CONTEXT_SITE_ID_WITH_COLON = data.siteDescriptor.uid;
                            keys.CONTEXT_CATALOG_VERSION_WITH_COLON = data.catalogDescriptor.catalogVersion;
                            keys.CONTEXT_CATALOG_WITH_COLON = data.catalogDescriptor.catalogId;
                            keys[CONTEXT_SITE_ID] = data.siteDescriptor.uid;
                            keys[CONTEXT_CATALOG_VERSION] = data.catalogDescriptor.catalogVersion;
                            keys[CONTEXT_CATALOG] = data.catalogDescriptor.catalogId;

                            keys[PAGE_CONTEXT_SITE_ID] = data.pageContext ? data.pageContext.siteId : data.siteDescriptor.uid;
                            keys[PAGE_CONTEXT_CATALOG_VERSION] = data.pageContext ? data.pageContext.catalogVersion : data.catalogDescriptor.catalogVersion;
                            keys[PAGE_CONTEXT_CATALOG] = data.pageContext ? data.pageContext.catalogId : data.catalogDescriptor.catalogId;

                            config.url = replaceAll(config.url, keys);

                            if (config.params && typeof config.params === 'object') {
                                config.params = JSON.parse(replaceAll(JSON.stringify(config.params), keys));
                            }
                        }
                        return config;
                    });
                } else {
                    return config;
                }
            });
        };

        var replaceAll = function(str, mapObj) {
            var regex = new RegExp(Object.keys(mapObj).join("|"), "g");
            return str.replace(regex, function(matched) {
                return mapObj[matched];
            });
        };

        var interceptor = {};
        interceptor.request = hitch(interceptor, request);
        return interceptor;
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('experienceInterceptor');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('l10nModule', ['languageServiceModule', 'eventServiceModule'])
    /**
     * @ngdoc filter
     * @name l10nModule.filter:l10n
     * @description
     * Filter that accepts a localized map as input and returns the value corresponding to the resolvedLocale of {@link languageServiceModule} and defaults to the first entry.
     *
     * @param {Object} localizedMap the map of language isocodes / values
     * This class serves as an interface and should be extended, not instantiated.
     *
     */
    .filter('l10n', ['languageService', 'systemEventService', 'SWITCH_LANGUAGE_EVENT', function(languageService, systemEventService, SWITCH_LANGUAGE_EVENT) {

        var l10n;

        function prepareFilter() {
            l10n = function initialFilter(str) {
                return str + ' filtered initially';
            };
            l10n.$stateful = false;

            languageService.getResolveLocaleIsoCode().then(function(resolvedLanguage) {
                l10n = function localizedFilter(localizedMap) {
                    if ('string' === typeof localizedMap) {
                        return localizedMap;
                    } else if (localizedMap) {
                        return localizedMap[resolvedLanguage] ? localizedMap[resolvedLanguage] : localizedMap[Object.keys(localizedMap)[0]];
                    }
                };
                l10n.$stateful = false;
            });
        }
        prepareFilter();

        systemEventService.registerEventHandler(SWITCH_LANGUAGE_EVENT, prepareFilter);




        return function(localizedMap) {
            return l10n(localizedMap);
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc service
 * @name functionsModule
 *
 * @description
 * provides a list of useful functions that can be used as part of the SmartEdit framework.
 */
angular.module('functionsModule', ['yjqueryModule', 'yLoDashModule', 'ngSanitize'])

.factory('ParseError', function() {
        var ParseError = function(value) {
            this.value = value;
        };
        return ParseError;
    })
    /**
     * @ngdoc service
     * @name functionsModule.getOrigin
     *
     * @description
     * returns document location origin
     * Some browsers still do not support W3C document.location.origin, this function caters for gap.
     */
    .factory('getOrigin', function() {
        return function() {
            return window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
        };
    })
    /**
     * @ngdoc service
     * @name functionsModule.isBlank
     *
     * @description
     * <b>isBlank</b> will check if a given string is undefined or null or empty.
     * - returns TRUE for undefined / null/ empty string
     * - returns FALSE otherwise
     *
     * @param {String} inputString any input string.
     * 
     * @returns {boolean} true if the string is null else false
     */
    .factory('isBlank', function() {
        return function(value) {
            return (typeof value === 'undefined' || value === null || value === "null" || value.toString().trim().length === 0);
        };
    })

/**
 * @ngdoc service
 * @name functionsModule.extend
 *
 * @description
 * <b>extend</b> provides a convenience to either default a new child or "extend" an existing child with the prototype of the parent
 *
 * @param {Class} ParentClass which has a prototype you wish to extend.
 * @param {Class} ChildClass will have its prototype set.
 * 
 * @returns {Class} ChildClass which has been extended
 */
.factory('extend', function() {
    return function(ParentClass, ChildClass) {
        if (!ChildClass) {
            ChildClass = function() {};
        }
        ChildClass.prototype = Object.create(ParentClass.prototype);
        return ChildClass;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.hitch
 *
 * @description
 * <b>hitch</b> will create a new function that will pass our desired context (scope) to the given function.
 * This method will also pre-bind the given parameters.
 *
 * @param {Object} scope scope that is to be assigned.
 * @param {Function} method the method that needs binding. 
 * 
 * @returns {Function} a new function thats binded to the given scope
 */
.factory('hitch', function() {
    return function(scope, method) {

        var argumentArray = Array.prototype.slice.call(arguments); // arguments is not an array
        // (from  http://www.sitepoint.com/arguments-a-javascript-oddity/)

        var preboundArguments = argumentArray.slice(2);

        return function lockedMethod() {

            // from here, "arguments" are the arguments passed to lockedMethod

            var postBoundArguments = Array.prototype.slice.call(arguments);

            return method.apply(scope, preboundArguments.concat(postBoundArguments));

        };

    };
})

/**
 * @ngdoc service
 * @name functionsModule.customTimeout
 *
 * @description
 * <b>customTimeout</b> will call the javascrit's native setTimeout method to execute a given function after a specified period of time.
 * This method is better than using $timeout since it is difficult to assert on $timeout during end-to-end testing.
 *
 * @param {Function} func function that needs to be executed after the specified duration.
 * @param {Number} duration time in milliseconds. 
 */
.factory('customTimeout', ['$rootScope', function($rootScope) {
    return function(func, duration) {
        setTimeout(function() {
            func();
            $rootScope.$digest();
        }, duration);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.copy
 *
 * @description
 * <b>copy</b> will do a deep copy of the given input object.
 *
 * @param {Object} candidate the javaScript value that needs to be deep copied.
 * 
 * @returns {Object} A deep copy of the input
 */
.factory('copy', function() {
    return function(candidate) {
        return JSON.parse(JSON.stringify(candidate));
    };
})

/**
 * @ngdoc service
 * @name functionsModule.merge
 *
 * @description
 * <b>merge</b> will merge the contents of two objects together into the first object.
 *
 * @param {Object} target any JavaScript object.
 * @param {Object} source any JavaScript object.
 * 
 * @returns {Object} a new object as a result of merge
 */
.factory('merge', ['yjQuery', function(yjQuery) {
    return function(source, target) {

        yjQuery.extend(source, target);

        return source;
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.getQueryString
 *
 * @description
 * <b>getQueryString</b> will convert a given object into a query string.
 * 
 * Below is the code snippet for sample input and sample output:
 * 
 * <pre>
 * var params = {
 *  key1 : 'value1',
 *  key2 : 'value2',
 *  key3 : 'value3'
 *  }
 *  
 *  var output = getQueryString(params);
 *  
 *  // The output is '?&key1=value1&key2=value2&key3=value3' 
 *
 *</pre>
 *
 * @param {Object} params Object containing a list of params.
 * 
 * @returns {String} a query string
 */
.factory('getQueryString', function() {
    return function(params) {

        var queryString = "";
        if (params) {
            for (var param in params) {
                queryString += '&' + param + "=" + params[param];
            }
        }
        return "?" + queryString;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.getURI
 *
 * @description
 * Will return the URI part of a URL
 * @param {String} url the URL the URI of which is to be returned
 */
.factory('getURI', function() {
    return function(url) {
        return url && url.indexOf("?") > -1 ? url.split("?")[0] : url;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.parseQuery
 *
 * @description
 * <b>parseQuery</b> will convert a given query string to an object.
 *
 * Below is the code snippet for sample input and sample output:
 *
 * <pre>
 * var query = '?key1=value1&key2=value2&key3=value3';
 *  
 * var output = parseQuery(query);
 * 
 * // The output is { key1 : 'value1', key2 : 'value2', key3 : 'value3' }
 *
 *</pre>
 *
 * @param {String} query String that needs to be parsed.
 * 
 * @returns {Object} an object containing all params of the given query
 */
.factory('parseQuery', function() {
    return function(str) {

        var objURL = {};

        str.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function($0, $1, $2, $3) {
            objURL[$1] = $3;
        });
        return objURL;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.trim
 *
 * @description
 * <b>trim</b> will remove spaces at the beginning and end of a given string.
 *
 * @param {String} inputString any input string.
 * 
 * @returns {String} the newly modified string without spaces at the beginning and the end
 */
.factory('trim', function() {

    return function(aString) {
        var regExpBeginning = /^\s+/;
        var regExpEnd = /\s+$/;
        return aString.replace(regExpBeginning, "").replace(regExpEnd, "");
    };
})

/**
 * @ngdoc service
 * @name functionsModule.convertToArray
 *
 * @description
 * <b>convertToArray</b> will convert the given object to array.
 * The output array elements are an object that has a key and value,
 * where key is the original key and value is the original object.
 * 
 * @param {Object} inputObject any input object.
 * 
 * @returns {Array} the array created from the input object
 */
.factory('convertToArray', function() {

    return function(object) {
        var configuration = [];
        for (var key in object) {
            if (key.indexOf('$') !== 0 && key.indexOf('toJSON') !== 0) {
                configuration.push({
                    key: key,
                    value: object[key]
                });
            }
        }
        return configuration;
    };

})

/**
 * @ngdoc service
 * @name functionsModule.injectJS
 *
 * @description
 * <b>injectJS</b> will inject script tags into html for a given set of sources.
 *
 */
.factory('injectJS', ['customTimeout', '$log', 'hitch', function(customTimeout, $log, hitch) {

    function getInjector() {
        return $script;
    }

    return {
        getInjector: getInjector,

        /** 
         * @ngdoc method
         * @name functionsModule.injectJS#execute
         * @methodOf functionsModule.injectJS
         * 
         * @description
         * <b>execute</b> will extract a given set of sources from the provided configuration object
         * and then inject each source as a JavaScript source tag and potential callbacks once all the
         * sources are wired.
         * 
         * @param {Object} configuration - a given set of configurations.
         * @param {Array} configuration.sources - an array of sources that needs to be added.
         * @param {Function} configuration.callback - Callback to be triggered once all the sources are wired.
         */
        execute: function(conf) {
            var srcs = conf.srcs;
            var index = conf.index;
            var callback = conf.callback;
            if (index === undefined) {
                index = 0;
            }
            if (srcs[index] !== undefined) {
                this.getInjector()(srcs[index], hitch(this, function() {
                    if (index + 1 < srcs.length) {
                        this.execute({
                            srcs: srcs,
                            index: index + 1,
                            callback: callback
                        });
                    } else if (typeof callback === 'function') {
                        callback();
                    }
                }));

            }
        }
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.uniqueArray
 *
 * @description
 * <b>uniqueArray</b> will return the first Array argument supplemented with new entries from the second Array argument.
 * 
 * @param {Array} array1 any JavaScript array.
 * @param {Array} array2 any JavaScript array.
 */
.factory('uniqueArray', function() {

    return function(array1, array2) {

        array2.forEach(function(instance) {
            if (array1.indexOf(instance) === -1) {
                array1.push(instance);
            }
        });

        return array1;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.regExpFactory
 *
 * @description
 * <b>regExpFactory</b> will convert a given pattern into a regular expression.
 * This method will prepend and append a string with ^ and $ respectively replaces
 * and wildcards (*) by proper regex wildcards.
 * 
 * @param {String} pattern any string that needs to be converted to a regular expression.
 * 
 * @returns {RegExp} a regular expression generated from the given string.
 *
 */
.factory('regExpFactory', function() {

    return function(pattern) {

        var onlyAlphanumericsRegex = new RegExp(/^[a-zA-Z\d]+$/i);
        var antRegex = new RegExp(/^[a-zA-Z\d\*]+$/i);

        var regexpKey;
        if (onlyAlphanumericsRegex.test(pattern)) {
            regexpKey = ['^', '$'].join(pattern);
        } else if (antRegex.test(pattern)) {
            regexpKey = ['^', '$'].join(pattern.replace(/\*/, '.*'));
        } else {
            regexpKey = pattern;
        }

        return new RegExp(regexpKey, 'g');
    };
})

/**
 * @ngdoc service
 * @name functionsModule.generateIdentifier
 *
 * @description
 * <b>generateIdentifier</b> will generate a unique string based on system time and a random generator.
 * 
 * @returns {String} a unique identifier.
 *
 */
.factory('generateIdentifier', function() {
    return function() {
        var d = new Date().getTime();
        if (window.performance && typeof window.performance.now === "function") {
            d += window.performance.now(); //use high-precision timer if available
        }
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    };
})




/**
 * @ngdoc service
 * @name functionsModule.escapeHtml
 *
 * @description
 * <b>escapeHtml</b> will escape &, <, >, " and ' characters .
 *
 * @param {String} a string that needs to be escaped.
 *
 * @returns {String} the escaped string.
 *
 */
.factory('escapeHtml', function() {
    return function(string) {
        if (typeof string === 'string') {
            return string.replace(/&/g, '&amp;')
                .replace(/>/g, '&gt;')
                .replace(/</g, '&lt;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&apos;');
        } else {
            return string;
        }
    };
})

/**
 * @ngdoc service
 * @name functionsModule.sanitize
 *
 * @description
 * <b>escapes any harmful scripting from a string, leaves innocuous HTML untouched/b>
 *
 * @param {String} a string that needs to be sanitized.
 *
 * @returns {String} the sanitized string.
 *
 */
.factory('sanitize', ['isBlank', function(isBlank) {
    return function(string) {
        return !isBlank(string) ? string.replace(/(?=[()])/g, '\\') : string;
        //return $sanitize(string);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.sanitizeHTML
 *
 * @description
 * <b>sanitizeHTML</b> will remove breaks and space .
 *
 * @param {String} a string that needs to be escaped.
 *
 * @returns {String} the sanitized HTML.
 *
 */
.factory('sanitizeHTML', ['isBlank', function(isBlank) {
    return function(obj) {
        var result = angular.copy(obj);
        if (!isBlank(result)) {
            result = result.replace(/(\r\n|\n|\r)/gm, '').replace(/>\s+</g, '><').replace(/<\/br\>/g, '');
        }
        return result;
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.toPromise
 *
 * @description
 * <b>toPromise</> transforms a function into a function that is guaranteed to return a Promise that resolves to the
 * original return value of the function, rejects with the rejected return value and rejects with an exceptino object when the invocation fails
 */
.factory('toPromise', ['$q', '$log', function($q, $log) {
    return function(method, context) {
        return function() {
            try {
                return $q.when(method.apply(context, arguments));
            } catch (e) {
                $log.error('execution of a method that was turned into a promise failed');
                $log.error(e);
                return $q.reject(e);
            }
        };
    };
}])


/**
 * Checks if `value` is a function.
 *
 * @static
 * @category Objects
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if the `value` is a function, else `false`.
 */
.factory('isFunction', function() {
    return function(value) {
        return typeof value === 'function';
    };
})

// check if the value is the ECMAScript language type of Object
.factory('isObject', function() {
    /** Used to determine if values are of the language type Object */
    var objectTypes = {
        'boolean': false,
        'function': true,
        'object': true,
        'number': false,
        'string': false,
        'undefined': false
    };
    return function(value) {
        return !!(value && objectTypes[typeof value]);
    };
})

/**
 * Creates a function that will delay the execution of `func` until after
 * `wait` milliseconds have elapsed since the last time it was invoked.
 * Provide an options object to indicate that `func` should be invoked on
 * the leading and/or trailing edge of the `wait` timeout. Subsequent calls
 * to the debounced function will return the result of the last `func` call.
 *
 * Note: If `leading` and `trailing` options are `true` `func` will be called
 * on the trailing edge of the timeout only if the the debounced function is
 * invoked more than once during the `wait` timeout.
 *
 * @static
 * @category Functions
 * @param {Function} func The function to debounce.
 * @param {number} wait The number of milliseconds to delay.
 * @param {Object} [options] The options object.
 * @param {boolean} [options.leading=false] Specify execution on the leading edge of the timeout.
 * @param {number} [options.maxWait] The maximum time `func` is allowed to be delayed before it's called.
 * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
 * @returns {Function} Returns the new debounced function.
 * @example
 *
 * // avoid costly calculations while the window size is in flux
 * var lazyLayout = lodash.debounce(calculateLayout, 150);
 * yjQuery(window).on('resize', lazyLayout);
 *
 * // execute `sendMail` when the click event is fired, debouncing subsequent calls
 * yjQuery('#postbox').on('click', lodash.debounce(sendMail, 300, {
 *   'leading': true,
 *   'trailing': false
 * });
 *
 * // ensure `batchLog` is executed once after 1 second of debounced calls
 * var source = new EventSource('/stream');
 * source.addEventListener('message', lodash.debounce(batchLog, 250, {
 *   'maxWait': 1000
 * }, false);
 */
.factory('debounce', ['isFunction', 'isObject', function(isFunction, isObject) {
    function TypeError() {

    }

    return function(func, wait, options) {
        var args,
            maxTimeoutId,
            result,
            stamp,
            thisArg,
            timeoutId,
            trailingCall,
            leading,
            lastCalled = 0,
            maxWait = false,
            trailing = true,
            isCalled;

        if (!isFunction(func)) {
            throw new TypeError();
        }
        wait = Math.max(0, wait) || 0;
        if (options === true) {
            leading = true;
            trailing = false;
        } else if (isObject(options)) {
            leading = options.leading;
            maxWait = 'maxWait' in options && (Math.max(wait, options.maxWait) || 0);
            trailing = 'trailing' in options ? options.trailing : trailing;
        }
        var delayed = function() {
            var remaining = wait - (Date.now() - stamp);
            if (remaining <= 0) {
                if (maxTimeoutId) {
                    clearTimeout(maxTimeoutId);
                }
                isCalled = trailingCall;
                maxTimeoutId = timeoutId = trailingCall = undefined;
                if (isCalled) {
                    lastCalled = Date.now();
                    result = func.apply(thisArg, args);
                    if (!timeoutId && !maxTimeoutId) {
                        args = thisArg = null;
                    }
                }
            } else {
                timeoutId = setTimeout(delayed, remaining);
            }
        };

        var maxDelayed = function() {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
            maxTimeoutId = timeoutId = trailingCall = undefined;
            if (trailing || (maxWait !== wait)) {
                lastCalled = Date.now();
                result = func.apply(thisArg, args);
                if (!timeoutId && !maxTimeoutId) {
                    args = thisArg = null;
                }
            }
        };

        return function() {
            args = arguments;
            stamp = Date.now();
            thisArg = this;
            trailingCall = trailing && (timeoutId || !leading);
            var leadingCall, isCalled;

            if (maxWait === false) {
                leadingCall = leading && !timeoutId;
            } else {
                if (!maxTimeoutId && !leading) {
                    lastCalled = stamp;
                }
                var remaining = maxWait - (stamp - lastCalled);
                isCalled = remaining <= 0;

                if (isCalled) {
                    if (maxTimeoutId) {
                        maxTimeoutId = clearTimeout(maxTimeoutId);
                    }
                    lastCalled = stamp;
                    result = func.apply(thisArg, args);
                } else if (!maxTimeoutId) {
                    maxTimeoutId = setTimeout(maxDelayed, remaining);
                }
            }
            if (isCalled && timeoutId) {
                timeoutId = clearTimeout(timeoutId);
            } else if (!timeoutId && wait !== maxWait) {
                timeoutId = setTimeout(delayed, wait);
            }
            if (leadingCall) {
                isCalled = true;
                result = func.apply(thisArg, args);
            }
            if (isCalled && !timeoutId && !maxTimeoutId) {
                args = thisArg = null;
            }
            return result;
        };
    };
}])

/**
 * Creates a function that, when executed, will only call the `func` function
 * at most once per every `wait` milliseconds. Provide an options object to
 * indicate that `func` should be invoked on the leading and/or trailing edge
 * of the `wait` timeout. Subsequent calls to the throttled function will
 * return the result of the last `func` call.
 *
 * Note: If `leading` and `trailing` options are `true` `func` will be called
 * on the trailing edge of the timeout only if the the throttled function is
 * invoked more than once during the `wait` timeout.
 *
 * @static
 * @category Functions
 * @param {Function} func The function to throttle.
 * @param {number} wait The number of milliseconds to throttle executions to.
 * @param {Object} [options] The options object.
 * @param {boolean} [options.leading=true] Specify execution on the leading edge of the timeout.
 * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
 * @returns {Function} Returns the new throttled function.
 * @example
 *
 * // avoid excessively updating the position while scrolling
 * var throttled = lodash.throttle(updatePosition, 100);
 * yjQuery(window).on('scroll', throttled);
 *
 * // execute `renewToken` when the click event is fired, but not more than once every 5 minutes
 * yjQuery('.interactive').on('click', lodash.throttle(renewToken, 300000, {
 *   'trailing': false
 * }));
 */
.factory('throttle', ['debounce', 'isFunction', 'isObject', function(debounce, isFunction, isObject) {
    return function(func, wait, options) {
        var leading = true,
            trailing = true;

        if (!isFunction(func)) {
            throw new TypeError();
        }
        if (options === false) {
            leading = false;
        } else if (isObject(options)) {
            leading = 'leading' in options ? options.leading : leading;
            trailing = 'trailing' in options ? options.trailing : trailing;
        }
        options = {};
        options.leading = leading;
        options.maxWait = wait;
        options.trailing = trailing;

        return debounce(func, wait, options);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.parseHTML
 *
 * @description
 * parses a string HTML into a queriable DOM object, stripping any JavaScript from the HTML.
 *
 * @param {String} stringHTML, the string representation of the HTML to parse
 */
.factory('parseHTML', ['yjQuery', function(yjQuery) {
    return function(stringHTML) {
        return yjQuery.parseHTML(stringHTML);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.unsafeParseHTML
 *
 * @description
 * parses a string HTML into a queriable DOM object, preserving any JavaScript present in the HTML.
 * Note - as this preserves the JavaScript present it must only be used on HTML strings originating
 * from a known safe location. Failure to do so may result in an XSS vulnerability.
 *
 * @param {String} stringHTML, the string representation of the HTML to parse
 */
.factory('unsafeParseHTML', ['yjQuery', function(yjQuery) {
    return function(stringHTML) {
        return yjQuery.parseHTML(stringHTML, null, true);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.extractFromElement
 *
 * @description
 * parses a string HTML into a queriable DOM object
 *
 * @param {Object} parent, the DOM element from which we want to extract matching selectors
 * @param {String} extractionSelector, the yjQuery selector identifying the elements to be extracted
 */
.factory('extractFromElement', ['yjQuery', function(yjQuery) {
    return function(parent, extractionSelector) {
        parent = yjQuery(parent);
        return parent.filter(extractionSelector).add(parent.find(extractionSelector));
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.closeOpenModalsOnBrowserBack
 *
 * @description
 * close any open modal window when a user clicks browser back button
 *
 * @param {Object} modalStack, the $modalStack service of angular-ui.
 */
.factory('closeOpenModalsOnBrowserBack', ['$uibModalStack', function($uibModalStack) {
        return function() {
            if ($uibModalStack.getTop()) {
                $uibModalStack.dismissAll();
            }
        };
    }])
    /**
     * @ngdoc service
     * @name functionsModule.service:URIBuilder
     *
     * @description
     * builder or URIs, build() method must be invoked to actually retrieve a URI
     *
     * @param {Object} modalStack, the $modalStack service of angular-ui.
     */
    .factory('URIBuilder', ['lodash', function(lodash) {

        function URIBuilder(uri) {

            this.uri = uri;

            this.build = function() {
                return this.uri;
            };

            /**
             * @ngdoc method
             * @name functionsModule.service:URIBuilder#replaceParams
             * @methodOf functionsModule.service:URIBuilder
             *
             * @description
             * Substitute all placeholders in the URI with the matching values in the given params
             *
             * @param {Object} params a map of placeholder names / values
             */
            this.replaceParams = function(params) {
                var clone = lodash.cloneDeep(this);
                if (params) {
                    //order the keys by descending length
                    var keys = Object.keys(params).sort(function(a, b) {
                        return b.length - a.length;
                    });
                    keys.forEach(function(key) {
                        var re = new RegExp('\\b' + key + '\\b');
                        clone.uri = clone.uri.replace(':' + key, params[key]).replace(re, params[key]);
                    });
                }
                return clone;
            };
        }

        return URIBuilder;
    }])

/**
 * @ngdoc service
 * @name functionsModule.service:getDataFromResponse
 *
 * @description
 * when provided with a response returned from a backend call, will filter the response
 * to retrieve the data of interest.
 *
 * @param {Object} response, response returned from a backend call.
 * @returns {Array} Returns the array from the response.
 */
.factory('getDataFromResponse', function() {
    return function(response) {
        var dataKey = Object.keys(response).filter(function(key) {
            return response[key] instanceof Array;
        })[0];

        return response[dataKey];
    };
})

/**
 * @ngdoc service
 * @name functionsModule.service:getKeyHoldingDataFromResponse
 *
 * @description
 * when provided with a response returned from a backend call, will filter the response
 * to retrieve the key holding the data of interest. 
 *
 * @param {Object} response, response returned from a backend call.
 * @returns {String} Returns the name of the key holding the array from the response. 
 */
.factory('getKeyHoldingDataFromResponse', function() {
    return function(response) {
        var dataKey = Object.keys(response).filter(function(key) {
            return response[key] instanceof Array;
        })[0];

        return dataKey;
    };
})

/**
 * @ngdoc service
 * @name functionsModule.service:resetObject
 *
 * @description
 * Resets a given object's properties' values
 *
 * @param {Object} targetObject, the object to reset
 * @param {Object} modelObject, an object that contains the structure that targetObject should have after a reset

 * @returns {Object} Returns the object that has been reset
 */
.factory('resetObject', ['copy', function(copy) {
        return function(targetObject, modelObject) {
            if (!targetObject) {
                targetObject = copy(modelObject);
            } else {
                for (var i in targetObject) {
                    delete targetObject[i];
                }
                angular.extend(targetObject, copy(modelObject));
            }

            return targetObject;
        };
    }])
    /**
     * @ngdoc service
     * @name functionsModule.service:isFunctionEmpty
     *
     * @description
     * Will determine whether a function body is empty
     *
     * @param {Function} fn, the function to evaluate

     * @returns {Boolean} a boolean.
     */
    .factory('isFunctionEmpty', function() {
        return function(fn) {
            return fn.toString().match(/\{([\s\S]*)\}/m)[1].trim() === '';
        };
    })

/**
 * @ngdoc service
 * @name functionsModule.service:isObjectEmptyDeep
 *
 * @description
 * Will check if the object is empty and will return true if each and every property of the object is empty
 *
 * @param {Object} value, the value to evaluate

 * @returns {Boolean} a boolean.
 */
.factory('isObjectEmptyDeep', ['lodash', function(lodash) {
    return function(value) {
        if (lodash.isObject(value)) {
            for (var key in value) {
                if (!lodash.isEmpty(value[key])) {
                    return false;
                }
            }
            return true;
        }
        return lodash.isEmpty(value);
    };
}])

/**
 * @ngdoc service
 * @name functionsModule.service:isAllTruthy
 * 
 * @description
 * Iterate on the given array of Functions, return true if each function returns true
 * 
 * @param {Array} arguments the functions
 * 
 * @return {Boolean} true if every function returns true
 */
.factory('isAllTruthy', function() {
    return function() {
        var fns = Array.prototype.slice.call(arguments);
        return function() {
            var args = arguments;
            return fns.every(function(f) {
                return f.apply(f, args);
            });
        };
    };
})

/**
 * @ngdoc service
 * @name functionsModule.service:isAnyTruthy
 * 
 * @description
 * Iterate on the given array of Functions, return true if at least one function returns true
 * 
 * @param {Array} arguments the functions
 * 
 * @return {Boolean} true if at least one function returns true
 */
.factory('isAnyTruthy', function() {
    return function() {
        var fns = Array.prototype.slice.call(arguments);
        return function() {
            var args = arguments;
            return fns.some(function(f) {
                return f.apply(f, args);
            });
        };
    };
})

/**
 * @ngdoc service
 * @name functionsModule.service:formatDateAsUtc
 *
 * @description
 * Formats provided dateTime as utc.
 *
 * @param {Object|String} dateTime DateTime to format in utc.
 *
 * @return {String} formatted string.
 */
.factory('formatDateAsUtc', ['DATE_CONSTANTS', function(DATE_CONSTANTS) {
    return function(dateTime) {
        return moment(dateTime).utc().format(DATE_CONSTANTS.MOMENT_ISO);
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('gatewayFactoryModule', ['eventServiceModule', 'componentHandlerServiceModule'])
    /**
     * @ngdoc object
     * @name gatewayFactoryModule.object:WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY
     *
     * @description
     * the name of the configuration key containing the list of white listed storefront domain names
     */
    .constant('WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY', 'whiteListedStorefronts')
    /**
     * @ngdoc object
     * @name gatewayFactoryModule.object:TIMEOUT_TO_RETRY_PUBLISHING
     *
     * @description
     * Period between two retries of a {@link gatewayFactoryModule.MessageGateway} to publish an event
     * this value must be greater than the time needed by the browser to process a postMessage back and forth across two frames.
     * Internet Explorer is now known to need more than 100ms.
     */
    .constant('TIMEOUT_TO_RETRY_PUBLISHING', 500)
    /**
     * @ngdoc service
     * @name gatewayFactoryModule.gatewayFactory
     *
     * @description
     * The Gateway Factory controls the creation of and access to {@link gatewayFactoryModule.MessageGateway MessageGateway}
     * instances.
     *
     * To construct and access a gateway, you must use the GatewayFactory's createGateway method and provide the channel
     * ID as an argument. If you try to create the same gateway twice, the second call will return a null.
     */
    .service('gatewayFactory', ['$rootScope', '$q', 'hitch', 'getOrigin', 'systemEventService', 'customTimeout', '$log', '$window', '$injector', 'WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY', 'TIMEOUT_TO_RETRY_PUBLISHING', 'SMARTEDIT_IFRAME_ID', function($rootScope, $q, hitch, getOrigin, systemEventService, customTimeout, $log, $window, $injector, WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY, TIMEOUT_TO_RETRY_PUBLISHING, SMARTEDIT_IFRAME_ID) {

        var PROMISE_ACKNOWLEDGEMENT_EVENT_ID = 'promiseAcknowledgement';
        var PROMISE_RETURN_EVENT_ID = 'promiseReturn';
        var SUCCESS = 'success';
        var FAILURE = 'failure';

        //TODO: a resolved or rejected promise is never cleaned up, are there memory leak issues?
        var promisesToResolve = [];

        var messageGatewayMap = {};

        /**
         * @ngdoc method
         * @name gatewayFactoryModule.gatewayFactory#initListener
         * @methodOf gatewayFactoryModule.gatewayFactory
         *
         * @description
         * Initializes a postMessage event handler that dispatches the handling of an event to the specified gateway.
         * If the corresponding gateway does not exist, an error is logged.
         */
        function initListener() {

            var processedPrimaryKeys = [];

            // Listen to message from child window
            $window.addEventListener("message", function(e) {

                if (this._isAllowed(e.origin)) {
                    //add control on e.origin
                    var event = e.data;

                    if (processedPrimaryKeys.indexOf(event.pk) > -1) {
                        return;
                    }
                    processedPrimaryKeys.push(event.pk);
                    $log.debug('message event handler called', event.eventId);

                    var gatewayId = event.gatewayId;
                    var gateway = messageGatewayMap[gatewayId];

                    if (!gateway) {
                        $log.debug('Incoming message on gateway ' + gatewayId + ', but no destination exists.');
                        return;
                    }

                    gateway._processEvent(event);
                } else {
                    $log.error("disallowed storefront is trying to communicate with smarteditcontainer");
                }

            }.bind(this), false);
        }

        /**
         * @ngdoc method
         * @name gatewayFactoryModule.gatewayFactory#createGateway
         * @methodOf gatewayFactoryModule.gatewayFactory
         *
         * @description
         * Creates a gateway for the specified gateway identifier and caches it in order to handle postMessage events
         * later in the application lifecycle. This method will fail on subsequent calls in order to prevent two
         * clients from using the same gateway.
         *
         * @param {String} gatewayId The identifier of the gateway.
         * @returns {MessageGateway} Returns the newly created Message Gateway or null.
         */
        function createGateway(gatewayId) {
            if (messageGatewayMap[gatewayId]) {
                $log.error('Message Gateway for ' + gatewayId + ' already reserved');
                return null;
            }

            messageGatewayMap[gatewayId] = new MessageGateway(gatewayId);
            return messageGatewayMap[gatewayId];
        }


        /**
         * @ngdoc service
         * @name gatewayFactoryModule.MessageGateway
         *
         * @description
         * The Message Gateway is a private channel that is used to publish and subscribe to events across iFrame
         * boundaries. The gateway uses the W3C-compliant postMessage as its underlying technology. The benefits of
         * the postMessage are that:
         * <ul>
         *     <li>It works in cross-origin scenarios.</li>
         *     <li>The receiving end can reject messages based on their origins.</li>
         * </ul>
         *
         * The creation of instances is controlled by the {@link gatewayFactoryModule.gatewayFactory gatewayFactory}. Only one
         * instance can exist for each gateway ID.
         *
         * @param {String} gatewayId The channel identifier
         * @constructor
         */
        var MessageGateway = function MessageGateway(gatewayId) {
            this.gatewayId = gatewayId;

            this._getTargetFrame = function() {
                if ($window.frameElement) {
                    return $window.parent;
                } else if ($window.document.getElementById(SMARTEDIT_IFRAME_ID)) {
                    return $window.document.getElementById(SMARTEDIT_IFRAME_ID).contentWindow;
                } else {
                    throw new Error('It is standalone. There is no iframe');
                }
            };

            this._generateIdentifier = function() {
                return new Date().getTime() + Math.random().toString();
            };

            this._getSystemEventId = function(eventId) {
                return this.gatewayId + ':' + eventId;
            };

            /**
             * @ngdoc method
             * @name gatewayFactoryModule.MessageGateway#publish
             * @methodOf gatewayFactoryModule.MessageGateway
             *
             * @description
             * Publishes a message across the gateway using the postMessage.
             *
             * The gateway's publish method implements promises, which are an AngularJS implementation. To resolve a
             * publish promise, all listener promises on the side of the channel must resolve. If a failure occurs in the
             * chain, the chain is interrupted and the publish promise is rejected.
             *
             * @param {String} eventId Event identifier
             * @param {Object} data Message payload
             * @param {Number} retries The current number of attempts to publish a message.
             * @param {Number} pk An optional parameter. It is a primary key for the event, which is generated after
             * the first attempt to send a message.
             * @returns {Promise} Promise to resolve
             */
            this.publish = function(eventId, data, retries, pk) {
                if (!retries) {
                    retries = 0;
                }

                var deferred = $q.defer();
                var target;
                try {
                    target = this._getTargetFrame();
                    pk = pk || this._generateIdentifier();
                    target.postMessage({
                        pk: pk, //necessary to identify a incoming postMessage that would carry the response to resolve the promise
                        gatewayId: this.gatewayId,
                        eventId: eventId,
                        data: data
                    }, '*');

                    promisesToResolve[pk] = deferred;

                    //in case promise does not return because, say, a non ready frame
                    customTimeout(hitch(this, function(deferred, retries) {
                        if (!deferred.acknowledged && eventId !== PROMISE_RETURN_EVENT_ID && eventId !== PROMISE_ACKNOWLEDGEMENT_EVENT_ID) { //still pending
                            if (retries < 5) {
                                retries++;
                                $log.debug(document.location.href, "is retrying to publish event", eventId);
                                target.postMessage({
                                    pk: pk, //necessary to identify a incoming postMessage that would carry the response to resolve the promise
                                    gatewayId: this.gatewayId,
                                    eventId: eventId,
                                    data: data
                                }, '*');
                            } else {
                                $log.error(document.location.href, "failed to publish event", eventId);
                                deferred.reject();
                            }
                        }

                    }, deferred, retries), TIMEOUT_TO_RETRY_PUBLISHING);
                } catch (e) {
                    deferred.reject();
                    return deferred.promise;
                }

                return deferred.promise;
            };

            /**
             * @ngdoc method
             * @name gatewayFactoryModule.MessageGateway#subscribe
             * @methodOf gatewayFactoryModule.MessageGateway
             *
             * @description
             * Registers the given callback function to the given event ID.
             *
             * @param {String} eventId Event identifier
             * @param {Function} callback Callback function to be invoked
             * @returns {Function} The function to call in order to unsubscribe the event listening.
             */
            this.subscribe = function(eventId, callback) {
                var systemEventId = this._getSystemEventId(eventId);
                return systemEventService.registerEventHandler(systemEventId, callback);
            };

            this._processEvent = function(event) {
                if (event.eventId !== PROMISE_RETURN_EVENT_ID && event.eventId !== PROMISE_ACKNOWLEDGEMENT_EVENT_ID) {
                    $log.debug(document.location.href, "sending acknowledgement for", event);

                    this.publish(PROMISE_ACKNOWLEDGEMENT_EVENT_ID, {
                        pk: event.pk,
                    });

                    var systemEventId = this._getSystemEventId(event.eventId);
                    systemEventService.sendAsynchEvent(systemEventId, event.data)
                        .then(
                            hitch(this, function(resolvedDataOfLastSubscriber) {
                                $log.debug(document.location.href, "sending promise resolve", event);
                                this.publish(PROMISE_RETURN_EVENT_ID, {
                                    pk: event.pk,
                                    type: SUCCESS,
                                    resolvedDataOfLastSubscriber: resolvedDataOfLastSubscriber
                                });
                            }),
                            hitch(this, function() {
                                $log.debug(document.location.href, "sending promise reject", event);
                                this.publish(PROMISE_RETURN_EVENT_ID, {
                                    pk: event.pk,
                                    type: FAILURE
                                });
                            })
                        );
                } else if (event.eventId === PROMISE_RETURN_EVENT_ID) {

                    if (promisesToResolve[event.data.pk]) {
                        if (event.data.type === SUCCESS) {
                            $log.debug(document.location.href, "received promise resolve", event);
                            promisesToResolve[event.data.pk].resolve(event.data.resolvedDataOfLastSubscriber);
                        } else if (event.data.type === FAILURE) {
                            $log.debug(document.location.href, "received promise reject", event);
                            promisesToResolve[event.data.pk].reject();
                        }
                    }

                } else if (event.eventId === PROMISE_ACKNOWLEDGEMENT_EVENT_ID) {
                    if (promisesToResolve[event.data.pk]) {
                        $log.debug(document.location.href, "received acknowledgement", event);
                        promisesToResolve[event.data.pk].acknowledged = true;
                    }

                }
            };
        };

        return {
            initListener: initListener,
            createGateway: createGateway,
            _isIframe: function() {
                return $window.frameElement;
            },
            /**
             * allowed if receiving end is frame or [container + (white listed storefront or same origin)]
             */
            _isAllowed: function(origin) {
                var whiteListedStorefronts = $injector.has(WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY) ? $injector.get(WHITE_LISTED_STOREFRONTS_CONFIGURATION_KEY) : [];
                return this._isIframe() || getOrigin() === origin || (whiteListedStorefronts.some(function(allowedURI) {
                    return origin.indexOf(allowedURI) > -1;
                }));
            }

        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('gatewayProxyModule', ['gatewayFactoryModule'])
    /**
     * @ngdoc service
     * @name gatewayProxyModule.gatewayProxy
     *
     * @description
     * To seamlessly integrate the gateway factory between two services on different frames, you can use a gateway
     * proxy. The gateway proxy service simplifies using the gateway module by providing an API that registers an
     * instance of a service that requires a gateway for communication.
     *
     * This registration process automatically attaches listeners to each of the service's functions (turned into promises), allowing stub
     * instances to forward calls to these functions using an instance of a gateway from {@link
     * gatewayFactoryModule.gatewayFactory gatewayFactory}. Any function that has an empty body declared on the service is used
     * as a proxy function. It delegates a publish call to the gateway under the same function name, and wraps the result
     * of the call in a Promise.
     */
    .factory('gatewayProxy', ['$log', '$q', 'hitch', 'toPromise', 'isFunctionEmpty', 'gatewayFactory', function($log, $q, hitch, toPromise, isFunctionEmpty, gatewayFactory) {

        var turnToProxy = function(fnName, service, gateway) {
            delete service[fnName];
            service[fnName] = function() {
                return gateway.publish(fnName, {
                    arguments: Array.prototype.slice.call(arguments)
                }).then(function(resolvedData) {
                    return resolvedData;
                });
            };
        };

        var onGatewayEvent = function(fnName, _service, eventId, data) {
            return _service[fnName].apply(_service, data.arguments);
        };

        /**
         * @ngdoc method
         * @name gatewayProxyModule.gatewayProxy#initForService
         * @methodOf gatewayProxyModule.gatewayProxy
         *
         * @description Mutates the given service into a proxied service.
         * You must provide a unique string gatewayId, in one of 2 ways.<br />
         * 1) Having a gatewayId property on the service provided<br />
         * OR<br />
         * 2) providing a gatewayId as 3rd param of this function<br />
         *
         * @param {Service} service Service to mutate into a proxied service.
         * @param {Array=} methodsSubset An explicit set of methods on which the gatewayProxy will trigger. Otherwise, by default all functions will be proxied. This is particularly useful to avoid inner methods being unnecessarily turned into promises.
         * @param {String=} gatewayId The gateway ID to use internaly for the proxy. If not provided, the service <strong>must<strong> have a gatewayId property.
         */
        var initForService = function(service, methodsSubset, gatewayId) {

            var gwId = gatewayId || service.gatewayId;

            if (!gwId) {
                $log.error('initForService() - service expected to have an associated gatewayId');
                return null;
            }

            var gateway = gatewayFactory.createGateway(gwId);

            var loopedOver = methodsSubset;
            if (!loopedOver) {
                loopedOver = [];
                for (var key in service) {
                    if (typeof service[key] === "function") {
                        loopedOver.push(key);
                    }
                }
            }

            loopedOver.forEach(function(fnName) {
                if (typeof service[fnName] === 'function') {
                    if (isFunctionEmpty(service[fnName])) {
                        turnToProxy(fnName, service, gateway);
                    } else {
                        service[fnName] = toPromise(service[fnName], service);
                        gateway.subscribe(fnName, hitch(null, onGatewayEvent, fnName, service));
                    }
                }
            });
        };

        return {
            initForService: initForService
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('FetchDataHandlerInterfaceModule', [])
    /**
     * @ngdoc service
     * @name FetchDataHandlerInterfaceModule.FetchDataHandlerInterface
     *
     * @description
     * Interface describing the contract of a fetchDataHandler fetched through dependency injection by the
     * {@link genericEditorModule.service:GenericEditor GenericEditor} to populate dropdowns
     */
    .factory('FetchDataHandlerInterface', function() {

        var FetchDataHandlerInterface = function() {};

        /**
         * @ngdoc method
         * @name FetchDataHandlerInterfaceModule.FetchDataHandlerInterface#getById
         * @methodOf FetchDataHandlerInterfaceModule.FetchDataHandlerInterface
         *
         * @description
         * will returns a promise resolving to an entity, of type defined by field, matching the given identifier
         *
         * @param {object} field the field descriptor in {@link genericEditorModule.service:GenericEditor GenericEditor}
         * @param {string} identifier the value identifying the entity to fetch
         * @returns {object} an entity
         */
        FetchDataHandlerInterface.prototype.getById = function() {
            throw "getById is not implemented";
        };

        /**
         * @ngdoc method
         * @name FetchDataHandlerInterfaceModule.FetchDataHandlerInterface#findBymask
         * @methodOf FetchDataHandlerInterfaceModule.FetchDataHandlerInterface
         *
         * @description
         * will returns a promise resolving to list of entities, of type defined by field, eligible for a given search mask
         *
         * @param {object} field the field descriptor in {@link genericEditorModule.service:GenericEditor GenericEditor}
         * @param {string} mask the value against witch to fetch entities.
         * @returns {Array} a list of eligible entities
         */
        FetchDataHandlerInterface.prototype.findBymask = function() {
            throw "findBymask is not implemented";
        };

        return FetchDataHandlerInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('fetchEnumDataHandlerModule', ['FetchDataHandlerInterfaceModule', 'restServiceFactoryModule', 'resourceLocationsModule'])

.factory('fetchEnumDataHandler', ['$q', 'FetchDataHandlerInterface', 'restServiceFactory', 'isBlank', 'extend', 'ENUM_RESOURCE_URI', function($q, FetchDataHandlerInterface, restServiceFactory, isBlank, extend, ENUM_RESOURCE_URI) {

    var cache = {};

    var FetchEnumDataHandler = function() {
        this.restServiceForEnum = restServiceFactory.get(ENUM_RESOURCE_URI);
    };

    FetchEnumDataHandler = extend(FetchDataHandlerInterface, FetchEnumDataHandler);

    FetchEnumDataHandler.prototype.findByMask = function(field, search) {
        return (cache[field.cmsStructureEnumType] ? $q.when(cache[field.cmsStructureEnumType]) : this.restServiceForEnum.get({
            enumClass: field.cmsStructureEnumType
        })).then(function(response) {
            cache[field.cmsStructureEnumType] = response;
            return cache[field.cmsStructureEnumType].enums.filter(function(element) {
                return isBlank(search) || element.label.toUpperCase().indexOf(search.toUpperCase()) > -1;
            });
        });
    };

    return new FetchEnumDataHandler();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('seBooleanModule', [])
    .component('seBoolean', {
        templateUrl: 'booleanTemplate.html',
        restrict: 'E',
        controller: function() {
            this.$onInit = function() {
                if (this.model[this.qualifier] === undefined) {
                    this.model[this.qualifier] = false;
                }
            };
        },
        bindings: {
            field: '<',
            qualifier: '<',
            model: '<',
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name dateFormatterModule
 * @description
 * # The dateFormatterModule
 *
 * The date formatter module is a module for displaying the date in the desired format.
 *
 */
angular.module('dateFormatterModule', ['seConstantsModule'])
    /**
     * @ngdoc directive
     * @name dateTimePickerModule.directive:dateTimePicker
     * @description
     * # The dateTimePicker
     * You can pass the desired format in the attributes of this directive and it will be shown. 
     * It is  used with the <input> tag as we cant use date filter with it.
     * for eg- <input type='text'  data-date-formatter  format-type="short">
     * format-type can be short, medium etc.
     * If the format-type is not given in the directive template, by default it uses the short type
     */
    .directive('dateFormatter', ['dateFilter', 'DATE_CONSTANTS', function(dateFilter, DATE_CONSTANTS) {
        var defaultFormatType = DATE_CONSTANTS.ANGULAR_FORMAT;
        return {
            require: 'ngModel',
            restrict: 'A',
            scope: {
                formatType: '@'
            },
            link: function(scope, element, attrs, ctrl) {
                ctrl.$parsers.push(function(data) {
                    return dateFilter(data, (scope.formatType || defaultFormatType));
                });

                ctrl.$formatters.push(function(data) {
                    return dateFilter(data, (scope.formatType || defaultFormatType));
                });
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name dateTimePickerModule
 * @description
 * # The dateTimePickerModule
 *
 * The date time picker service module is a module used for displaying a date time picker
 *
 * Use the {@link dateTimePickerModule.directive:dateTimePicker dateTimePicker} to open the date time picker.
 *
 * Once the datetimepicker is opened, its {@link dateTimePickerModule.service:dateTimePickerLocalizationService dateTimePickerLocalizationService} is used to localize the tooling.
 *
 *
 */
angular.module('dateTimePickerModule', ['yjqueryModule', 'seConstantsModule', 'languageServiceModule', 'translationServiceModule', 'dateFormatterModule', 'functionsModule'])
    /**
     * @ngdoc directive
     * @name dateTimePickerModule.directive:dateTimePicker
     * @description
     * # The dateTimePicker
     *
     */
    .directive('dateTimePicker', ['$timeout', 'yjQuery', 'dateTimePickerLocalizationService', 'DATE_CONSTANTS', 'formatDateAsUtc', function($timeout, yjQuery, dateTimePickerLocalizationService, DATE_CONSTANTS, formatDateAsUtc) {
        return {
            templateUrl: 'dateTimePickerTemplate.html',
            restrict: 'E',
            transclude: true,
            replace: false,
            scope: {
                name: '=',
                model: '=',
                isEditable: '=',
                field: '='
            },
            link: function($scope, elem) {
                $scope.placeholderText = 'se.componentform.select.date';

                if ($scope.isEditable) {
                    getPickerNode()
                        .datetimepicker({
                            format: DATE_CONSTANTS.MOMENT_FORMAT,
                            keepOpen: true,
                            minDate: 0,
                            showClear: true,
                            showClose: true,
                            useCurrent: false,
                            widgetPositioning: {
                                horizontal: 'right',
                                vertical: 'bottom'
                            }
                        })
                        .on('dp.change', function() {
                            $timeout(function() {
                                var momentDate = getDatetimepicker().date();
                                if (momentDate) {
                                    this.model = formatDateAsUtc(momentDate);
                                } else {
                                    this.model = void 0;
                                }
                            }.bind($scope));
                        })
                        .on('dp.show', function() {
                            dateTimePickerLocalizationService.localizeDateTimePicker(getDatetimepicker());
                        });

                    if ($scope.model) {
                        getPickerNode().datetimepicker().data("DateTimePicker").date(moment($scope.model));
                    }
                }

                function getPickerNode() {
                    return yjQuery(elem.children()[0]);
                }

                function getDatetimepicker() {
                    return getPickerNode().datetimepicker().data("DateTimePicker");
                }
            }
        };
    }])
    /**
     * @ngdoc object
     * @name dateTimePickerModule.object:resolvedLocaleToMomentLocaleMap
     *
     * @description
     * Contains a map of all inconsistent locales ISOs between SmartEdit and MomentJS
     *
     */
    .constant('resolvedLocaleToMomentLocaleMap', {
        'in': 'id',
        'zh': 'zh-cn'
    })
    /**
     * @ngdoc object
     * @name dateTimePickerModule.object: tooltipsMap
     *
     * @description
     * Contains a map of all tooltips to be localized in the date time picker
     *
     */
    .constant('tooltipsMap', {
        today: 'se.datetimepicker.today',
        clear: 'se.datetimepicker.clear',
        close: 'se.datetimepicker.close',
        selectMonth: 'se.datetimepicker.selectmonth',
        prevMonth: 'se.datetimepicker.previousmonth',
        nextMonth: 'se.datetimepicker.nextmonth',
        selectYear: 'se.datetimepicker.selectyear',
        prevYear: 'se.datetimepicker.prevyear',
        nextYear: 'se.datetimepicker.nextyear',
        selectDecade: 'se.datetimepicker.selectdecade',
        prevDecade: 'se.datetimepicker.prevdecade',
        nextDecade: 'se.datetimepicker.nextdecade',
        prevCentury: 'se.datetimepicker.prevcentury',
        nextCentury: 'se.datetimepicker.nextcentury',
        pickHour: 'se.datetimepicker.pickhour',
        incrementHour: 'se.datetimepicker.incrementhour',
        decrementHour: 'se.datetimepicker.decrementhour',
        pickMinute: 'se.datetimepicker.pickminute',
        incrementMinute: 'se.datetimepicker.incrementminute',
        decrementMinute: 'se.datetimepicker.decrementminute',
        pickSecond: 'se.datetimepicker.picksecond',
        incrementSecond: 'se.datetimepicker.incrementsecond',
        decrementSecond: 'se.datetimepicker.decrementsecond',
        togglePeriod: 'se.datetimepicker.toggleperiod',
        selectTime: 'se.datetimepicker.selecttime'
    })
    /**
     * @ngdoc service
     * @name dateTimePickerModule.service:dateTimePickerLocalizationService
     *
     * @description
     * The dateTimePickerLocalizationService is responsible for both localizing the date time picker as well as the tooltips
     */
    .service('dateTimePickerLocalizationService', ['$translate', 'resolvedLocaleToMomentLocaleMap', 'tooltipsMap', 'languageService', function($translate, resolvedLocaleToMomentLocaleMap, tooltipsMap, languageService) {

        var convertResolvedToMomentLocale = function(resolvedLocale) {
            var conversion = resolvedLocaleToMomentLocaleMap[resolvedLocale];
            if (conversion) {
                return conversion;
            } else {
                return resolvedLocale;
            }
        };

        var getLocalizedTooltips = function() {

            var localizedTooltips = {};


            for (var index in tooltipsMap) {
                localizedTooltips[index] = $translate.instant(tooltipsMap[index]);
            }

            return localizedTooltips;

        };

        var compareTooltips = function(tooltips1, tooltips2) {
            for (var index in tooltipsMap) {
                if (tooltips1[index] !== tooltips2[index]) {
                    return false;
                }
            }
            return true;
        };

        var localizeDateTimePickerUI = function(datetimepicker) {
            languageService.getResolveLocale().then(function(language) {

                var momentLocale = convertResolvedToMomentLocale(language);

                //This if statement was added to prevent infinite recursion, at the moment it triggers twice
                //due to what seems like datetimepicker.locale(<string>) broadcasting dp.show
                if (datetimepicker.locale() !== momentLocale) {
                    datetimepicker.locale(momentLocale);
                }

            });

        };

        var localizeDateTimePickerTooltips = function(datetimepicker) {
            var currentTooltips = datetimepicker.tooltips();
            var translatedTooltips = getLocalizedTooltips();

            //This if statement was added to prevent infinite recursion, at the moment it triggers twice
            //due to what seems like datetimepicker.tooltips(<tooltips obj>) broadcasting dp.show
            if (!compareTooltips(currentTooltips, translatedTooltips)) {
                datetimepicker.tooltips(translatedTooltips);
            }

        };

        this.localizeDateTimePicker = function(datetimepicker) {
            localizeDateTimePickerUI(datetimepicker);
            localizeDateTimePickerTooltips(datetimepicker);
        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

angular.module('seDropdownModule', ['restServiceFactoryModule', 'eventServiceModule', 'optionsDropdownPopulatorModule', 'uriDropdownPopulatorModule', 'functionsModule'])
    .constant('LINKED_DROPDOWN', 'LinkedDropdown')
    .constant('CLICK_DROPDOWN', 'ClickDropdown')
    .constant('DROPDOWN_IMPLEMENTATION_SUFFIX', 'DropdownPopulator')

/**
 * @ngdoc service
 * @name seDropdownModule.service:SEDropdownService
 *
 * @description
 * The SEDropdownService handles the initialization and the rendering of the {@link seDropdownModule.directive:seDropdown seDropdown} Angular component.
 */
.factory('SEDropdownService', ['$q', '$injector', 'isBlank', 'isFunctionEmpty', 'LINKED_DROPDOWN', 'CLICK_DROPDOWN', 'DROPDOWN_IMPLEMENTATION_SUFFIX', 'systemEventService', 'getKeyHoldingDataFromResponse', function(
    $q,
    $injector,
    isBlank,
    isFunctionEmpty,
    LINKED_DROPDOWN,
    CLICK_DROPDOWN,
    DROPDOWN_IMPLEMENTATION_SUFFIX,
    systemEventService,
    getKeyHoldingDataFromResponse) {

    /**
     * @constructor
     */
    var SEDropdownService = function(conf) {
        this.field = conf.field;
        this.qualifier = conf.qualifier;
        this.model = conf.model;
        this.id = conf.id;
        this.onClickOtherDropdown = conf.onClickOtherDropdown;
        this.items = [];
    };

    SEDropdownService.prototype._respondToChange = function(key,
        handle) {
        if (this.field.dependsOn && this.field.dependsOn.split(",").indexOf(handle.qualifier) > -1) {
            this.selection = handle.optionObject;
            if (this.reset) {
                this.reset();
            }
        }
    };


    SEDropdownService.prototype._respondToOtherClicks = function(key, qualifier) {
        if (this.field.qualifier !== qualifier && typeof this.onClickOtherDropdown === "function") {
            this.onClickOtherDropdown(key, qualifier);
        }
    };

    /**
     * @ngdoc method
     * @name seDropdownModule.service:SEDropdownService#triggerAction
     * @methodOf seDropdownModule.service:SEDropdownService
     *
     * @description
     * Publishes an asynchronous event for the currently selected option
     */
    SEDropdownService.prototype.triggerAction = function() {
        var selectedObj = this.items.filter(function(option) {
            return option.id === this.model[this.qualifier];
        }.bind(this))[0];
        var handle = {
            qualifier: this.qualifier,
            optionObject: selectedObj
        };
        systemEventService.sendAsynchEvent(this.eventId, handle);
    };

    SEDropdownService.prototype.onClick = function() {
        systemEventService.sendAsynchEvent(this.clickEventKey, this.field.qualifier);
    };

    /**
     * @ngdoc method
     * @name seDropdownModule.service:SEDropdownService#fetchAll
     * @methodOf seDropdownModule.service:SEDropdownService
     *
     * @description
     * Uses the configured implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface}
     * to populate the seDropdown items using {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface:populate populate}
     * 
     * @returns {Promise} A promise that resolves to a list of options to be populated
     */
    SEDropdownService.prototype.fetchAll = function(search) {
        return this.populator.populate({
            field: this.field,
            model: this.model,
            selection: this.selection,
            search: search,
        }).then(function(options) {
            this.items = options;
            return this.items;
        }.bind(this));

    };

    /**
     * @ngdoc method
     * @name seDropdownModule.service:SEDropdownService#fetchEntity
     * @methodOf seDropdownModule.service:SEDropdownService
     *
     * @description
     * Uses the configured implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface}
     * to populate a single item {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface:getItem getItem}
     * 
     * @param {String} id The id of the option to fetch
     * 
     * @returns {Promise} A promise that resolves to the option that was fetched
     */
    SEDropdownService.prototype.fetchEntity = function(id) {
        return this.populator.getItem({
            field: this.field,
            id: id,
            model: this.model
        });
    };

    /**
     * @ngdoc method
     * @name seDropdownModule.service:SEDropdownService#fetchPage
     * @methodOf seDropdownModule.service:SEDropdownService
     * 
     * @param {String} search The search to filter options by
     * @param {Number} pageSize The number of items to be returned
     * @param {Number} currentPage The page to be returned
     *
     * @description
     * Uses the configured implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface}
     * to populate the seDropdown items using {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface:fetchPage fetchPage}
     * 
     * @returns {Promise} A promise that resolves to an object containing the array of items and paging information
     */
    SEDropdownService.prototype.fetchPage = function(search, pageSize, currentPage) {
        return this.populator.fetchPage({
            field: this.field,
            model: this.model,
            selection: this.selection,
            search: search,
            pageSize: pageSize,
            currentPage: currentPage
        }).then(function(page) {
            var holderProperty = getKeyHoldingDataFromResponse(page);
            page.results = page[holderProperty];

            delete page[holderProperty];
            this.items = page.results;
            return page;
        }.bind(this));
    };

    /**
     * @ngdoc method
     * @name seDropdownModule.service:SEDropdownService#init
     * @methodOf seDropdownModule.service:SEDropdownService
     *
     * @description
     * Initializes the seDropdown with a configured dropdown populator based on field attributes used when instantiating
     * the {@link  seDropdownModule.service:SEDropdownService}.
     */
    SEDropdownService.prototype.init = function() {

        this.isMultiDropdown = this.field.collection ? this.field.collection : false;

        this.triggerAction = this.triggerAction.bind(this);

        var populatorName;

        this.eventId = (this.id || '') + LINKED_DROPDOWN;
        this.clickEventKey = (this.id || '') + CLICK_DROPDOWN;

        if (this.field.dependsOn) {
            systemEventService.registerEventHandler(this.eventId, this._respondToChange.bind(this));
        }

        systemEventService.registerEventHandler(this.clickEventKey, this._respondToOtherClicks.bind(this));

        if (this.field.options && this.field.uri) {
            throw "se.dropdown.contains.both.uri.and.options";
        } else if (this.field.options) {
            populatorName = "options" + DROPDOWN_IMPLEMENTATION_SUFFIX;
            this.isPaged = false;
        } else if (this.field.uri) {
            populatorName = "uri" + DROPDOWN_IMPLEMENTATION_SUFFIX;
            this.isPaged = this.field.paged ? this.field.paged : false;
        } else if (this.field.propertyType) {
            if ($injector.has(this.field.propertyType + DROPDOWN_IMPLEMENTATION_SUFFIX)) {
                populatorName = this.field.propertyType + DROPDOWN_IMPLEMENTATION_SUFFIX;
                this.isPaged = isPopulatorPaged(populatorName);
            } else {
                throw "sedropdown.no.populator.found";
            }
        } else {
            if ($injector.has(this.field.smarteditComponentType + this.field.qualifier + DROPDOWN_IMPLEMENTATION_SUFFIX)) {
                populatorName = this.field.smarteditComponentType + this.field.qualifier + DROPDOWN_IMPLEMENTATION_SUFFIX;
            } else if ($injector.has(this.field.smarteditComponentType + DROPDOWN_IMPLEMENTATION_SUFFIX)) {
                populatorName = this.field.smarteditComponentType + DROPDOWN_IMPLEMENTATION_SUFFIX;
            } else {
                throw "se.dropdown.no.populator.found";
            }
            this.isPaged = isPopulatorPaged(populatorName);
        }

        this.populator = $injector.get(populatorName);

        this.fetchStrategy = {
            fetchEntity: this.fetchEntity.bind(this)
        };

        if (this.isPaged) {
            this.fetchStrategy.fetchPage = this.fetchPage.bind(this);
        } else {
            this.fetchStrategy.fetchAll = this.fetchAll.bind(this);
        }

        this.initialized = true;

    };

    var isPopulatorPaged = function(populatorName) {
        var populator = $injector.get(populatorName);
        return populator.isPaged && populator.isPaged();
    };

    return SEDropdownService;

}])

/**
 * @ngdoc directive
 * @name seDropdownModule.directive:seDropdown
 * @scope
 * @restrict E
 * @element se-dropdown
 *
 * @description
 * This directive generates a custom dropdown (standalone or dependent on another one) for the {@link genericEditorModule.service:GenericEditor genericEditor}.
 * It is an implementation of the PropertyEditorTemplate {@link editorFieldMappingServiceModule.service:PropertyEditorTemplate contract}.
 * <br/>{@link editorFieldMappingServiceModule.service:editorFieldMappingService editorFieldMappingService} maps seDropdown by default to the "EditableDropdown" cmsStructureType.
 * <br/>The dropdown will be configured and populated based on the field structure retrieved from the Structure API.
 * The following is an example of the 4 possible field structures that can be returned by the Structure API for seDropdown to work:
 * <pre>
 * [
 * ...
 * {
 *		cmsStructureType: "EditableDropdown",
 *		qualifier: "someQualifier1",
 *		i18nKey: 'i18nkeyForsomeQualifier1',
 *		idAttribute: "id",
 *		labelAttributes: ["label"],
 *		paged: false,
 *		options: [{
 *      	id: '1',
 *      	label: 'option1'
 *      	}, {
 *      	id: '2',
 *      	label: 'option2'
 *      	}, {
 *      	id: '3',
 *      	label: 'option3'
 *      }],
 * }, {
 *		cmsStructureType: "EditableDropdown",
 *		qualifier: "someQualifier2",
 *		i18nKey: 'i18nkeyForsomeQualifier2',
 *		idAttribute: "id",
 *		labelAttributes: ["label"],
 *		paged: false,
 *		uri: '/someuri',
 *		dependsOn: 'someQualifier1'
 * }, {
 *		cmsStructureType: "EditableDropdown",
 *		qualifier: "someQualifier2",
 *		i18nKey: 'i18nkeyForsomeQualifier2',
 *		idAttribute: "id",
 *		labelAttributes: ["label"],
 *		paged: false,
 * }, {
 *		cmsStructureType: "EditableDropdown",
 *		qualifier: "someQualifier3",
 *		i18nKey: 'i18nkeyForsomeQualifier3',
 *		idAttribute: "id",
 *		labelAttributes: ["label"],
 *		paged: false,
 *		propertyType: 'somePropertyType',
 * }
 * ...
 * ]
 * </pre>
 * 
 * <br/>If uri, options and propertyType are not set, then seDropdown will look for an implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface} with the following AngularJS recipe name:
 * <pre>smarteditComponentType + qualifier + "DropdownPopulator"</pre>
 * and default to:
 * <pre>smarteditComponentType + "DropdownPopulator"</pre>
 * If no custom populator can be found, an exception will be raised.
 * <br/><br/>For the above example, since someQualifier2 will depend on someQualifier1, then if someQualifier1 is changed, then the list of options
 * for someQualifier2 is populated by calling the populate method of {@link uriDropdownPopulatorModule.service:uriDropdownPopulator uriDropdownPopulator}.
 * 
 * @param {= Object} field The field description of the field being edited as defined by the structure API described in {@link genericEditorModule.service:GenericEditor genericEditor}.
 * @param {Array =} field.options An array of options to be populated.
 * @param {String =} field.uri The uri to fetch the list of options from a REST call, especially if the dropdown is dependent on another one.
 * @param {String =} field.propertyType If a propertyType is defined, the seDropdown will use the populator associated to it with the following AngularJS recipe name : <pre>propertyType + "DropdownPopulator"</pre>.
 * @param {String =} field.dependsOn The qualifier of the parent dropdown that this dropdown depends on.
 * @param {String =} field.idAttribute The name of the id attribute to use when populating dropdown items.
 * @param {Array =} field.labelAttributes An array of attributes to use when determining the label for each item in the dropdown
 * @param {Boolean =} field.paged A boolean to determine if we are in paged mode as opposed to retrieving all items at once.
 * @param {= String} qualifier If the field is not localized, this is the actual field.qualifier, if it is localized, it is the language identifier such as en, de...
 * @param {= Object} model If the field is not localized, this is the actual full parent model object, if it is localized, it is the language map: model[field.qualifier].
 * @param {= String} id An identifier of the generated DOM element.
 * @param {<String=} itemTemplateUrl the path to the template that will be used to display items in both the dropdown menu and the selection.
 */
.directive('seDropdown', ['$rootScope', 'SEDropdownService', function($rootScope, SEDropdownService) {
    return {
        templateUrl: 'seDropdownTemplate.html',
        restrict: 'E',
        transclude: true,
        replace: false,
        scope: {
            field: '=',
            qualifier: '=',
            model: '=',
            id: '=',
            itemTemplateUrl: '<?'
        },
        link: function($scope) {

            $scope.onClickOtherDropdown = function() {
                $scope.closeSelect();
            };

            $scope.closeSelect = function() {
                var uiSelectCtrl = $scope.getUiSelectCtrl();
                uiSelectCtrl.open = false;
            };

            $scope.getUiSelectCtrl = function() {
                var uiSelectId = "#" + $scope.field.qualifier + "-selector";
                return angular.element(uiSelectId).controller("uiSelect");
            };

            $scope.dropdown = new SEDropdownService({
                field: $scope.field,
                qualifier: $scope.qualifier,
                model: $scope.model,
                id: $scope.id,
                onClickOtherDropdown: $scope.onClickOtherDropdown
            });

            $scope.dropdown.init();

        }
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('dropdownPopulatorInterfaceModule', ['yLoDashModule'])
    /**
     * @ngdoc service
     * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
     *
     * @description
     * Interface describing the contract of a DropdownPopulator fetched through dependency injection by the
     * {@link genericEditorModule.service:GenericEditor GenericEditor} to populate the dropdowns of {@link seDropdownModule.directive:seDropdown seDropdown}.
     */
    .factory('DropdownPopulatorInterface', ['lodash', function(lodash) {

        var DropdownPopulatorInterface = function() {};

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#populate
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         * @description
         * Will returns a promise resolving to a list of items.
         * this method is deprecated, use {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#fetchAll, fetchAll}.
         * @param {object} payload contains the field, model and additional attributes.
         * @param {object} payload.field The field descriptor from {@link genericEditorModule.service:GenericEditor GenericEditor} containing information about the dropdown.
         * @param {object} payload.model The full model being edited in {@link genericEditorModule.service:GenericEditor GenericEditor}.
         * @param {object} payload.selection The object containing the full option object that is now selected in a dropdown that we depend on (Optional, see dependsOn property in {@link seDropdownModule.directive:seDropdown seDropdown}).
         * @param {String} payload.search The search key when the user types in the dropdown (optional).
         * @returns {object} a list of objects.
         */
        DropdownPopulatorInterface.prototype.populate = function(payload) {
            return this.fetchAll(payload);
        };

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#fetchAll
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         * @deprecated
         * @description
         * Will returns a promise resolving to a list of items.
         * The items must all contain a property <b>id</b>.
         * @param {object} payload contains the field, model and additional attributes.
         * @param {String} payload.field.options The original array of options (used by {@link optionsDropdownPopulatorModule.service:optionsDropdownPopulator optionsDropdownPopulator})
         * @param {String} payload.field.uri The uri used to make a rest call to fetch data (used by {@link uriDropdownPopulatorModule.service:uriDropdownPopulator uriDropdownPopulator})
         * @param {object} payload.field The field descriptor from {@link genericEditorModule.service:GenericEditor GenericEditor} containing information about the dropdown.
         * @param {String} payload.field.dependsOn A comma separated list of attributes to include from the model when building the request params
         * @param {String} payload.field.idAttribute The name of the attribute to use when setting the id attribute
         * @param {String} payload.field.labelAttributes A list of attributes to use when setting the label attribute
         * @param {object} payload.model The full model being edited in {@link genericEditorModule.service:GenericEditor GenericEditor}.
         * @param {object} payload.selection The object containing the full option object that is now selected in a dropdown that we depend on (Optional, see dependsOn property in {@link seDropdownModule.directive:seDropdown seDropdown}).
         * @param {String} payload.search The search key when the user types in the dropdown (optional).
         * @returns {object} a list of objects.
         */
        DropdownPopulatorInterface.prototype.fetchAll = function() {};

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#fetchPage
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         *
         * @description
         * Will returns a promise resolving to a {@link Page.object:Page page} of items.
         * The items must all contain a property <b>id</b>.
         * @param {object} payload contains the field, model and additional attributes.
         * @param {object} payload.field The field descriptor from {@link genericEditorModule.service:GenericEditor GenericEditor} containing information about the dropdown.
         * @param {String} payload.field.options The original array of options (used by {@link optionsDropdownPopulatorModule.service:optionsDropdownPopulator optionsDropdownPopulator})
         * @param {String} payload.field.uri The uri used to make a rest call to fetch data (used by {@link uriDropdownPopulatorModule.service:uriDropdownPopulator uriDropdownPopulator})
         * @param {String} payload.field.dependsOn A comma separated list of attributes to include from the model when building the request params
         * @param {String} payload.field.idAttribute The name of the attribute to use when setting the id attribute
         * @param {String} payload.field.labelAttributes A list of attributes to use when setting the label attribute
         * @param {object} payload.field.params An object containing properties to append as query string while making a call.
         * @param {object} payload.model The full model being edited in {@link genericEditorModule.service:GenericEditor GenericEditor}.
         * @param {object} payload.selection The object containing the full option object that is now selected in a dropdown that we depend on (Optional, see dependsOn property in {@link seDropdownModule.directive:seDropdown seDropdown}).
         * @param {String} payload.search The search key when the user types in the dropdown (optional).
         * @param {String} payload.pageSize number of items in the page.
         * @param {String} payload.currentPage current page number.
         * @returns {object} a {@link Page.object:Page page}
         */
        DropdownPopulatorInterface.prototype.fetchPage = function() {};

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#isPaged
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         *
         * @description
         * Specifies whether this populator is meant to work in paged mode as opposed to retrieve lists. Optional, default is false
         */
        DropdownPopulatorInterface.prototype.isPaged = function() {
            return false;
        };

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#populateAttributes
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         *
         * @description
         * Populates the id and label property for each item in the list. If the label property is not already set,
         * then we use an ordered list of attributes to use when determining the label for each item.
         * @param {Array} items The array of items to set the id and label attributes on
         * @param {String} idAttribute The name of the id attribute
         * @param {Array} orderedLabelAttributes The ordered list of label attributes
         * @returns {Array} the modified list of items
         */
        DropdownPopulatorInterface.prototype.populateAttributes = function(items, idAttribute, orderedLabelAttributes) {
            return lodash.map(items, function(item) {
                if (idAttribute && lodash.isEmpty(item.id)) {
                    item.id = item[idAttribute];
                }

                if (lodash.isEmpty(item.label)) {
                    // Find the first attribute that the item object contains
                    var labelAttribute = lodash.find(orderedLabelAttributes, function(attr) {
                        return !lodash.isEmpty(item[attr]);
                    });

                    // If we found an attribute, set the label
                    if (labelAttribute) {
                        item.label = item[labelAttribute];
                    }
                }

                return item;
            });
        };

        /**
         * @ngdoc method
         * @name DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#search
         * @methodOf DropdownPopulatorInterfaceModule.DropdownPopulatorInterface
         *
         * @description
         * Searches a list and returns only items with a label attribute that matches the search term
         * @param {Array} items The list of items to search
         * @param {Array} searchTerm The search term to filter items by
         * @returns {Array} the filtered list of items
         */
        DropdownPopulatorInterface.prototype.search = function(items, searchTerm) {
            return lodash.filter(items, function(item) {
                return item.label && item.label.toUpperCase().indexOf(searchTerm.toUpperCase()) > -1;
            });
        };

        return DropdownPopulatorInterface;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('optionsDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule'])
    /**
     * @ngdoc service
     * @name optionsDropdownPopulatorModule.service:optionsDropdownPopulator
     * @description
     * implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface} for "EditableDropdown" cmsStructureType
     * containing options attribute.
     */
    .factory('optionsDropdownPopulator', ['DropdownPopulatorInterface', '$q', 'extend', function(DropdownPopulatorInterface, $q, extend) {

        var optionsDropdownPopulator = function() {};

        optionsDropdownPopulator = extend(DropdownPopulatorInterface, optionsDropdownPopulator);

        /**
         * @ngdoc method
         * @name optionsDropdownPopulatorModule.service:optionsDropdownPopulator#populate
         * @methodOf optionsDropdownPopulatorModule.service:optionsDropdownPopulator
         *
         * @description
         * Implementation of the {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#populate DropdownPopulatorInterface.populate} method
         */
        optionsDropdownPopulator.prototype.populate = function(payload) {
            var options = this.populateAttributes(payload.field.options, payload.field.idAttribute, payload.field.labelAttributes);

            if (payload.search) {
                options = this.search(options, payload.search);
            }

            return $q.when(options);
        };

        return new optionsDropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('uriDropdownPopulatorModule', ['dropdownPopulatorInterfaceModule', 'restServiceFactoryModule', 'yLoDashModule', 'functionsModule'])
    /**
     * @ngdoc service
     * @name uriDropdownPopulatorModule.service:uriDropdownPopulator
     * @description
     * implementation of {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface DropdownPopulatorInterface} for "EditableDropdown" cmsStructureType
     * containing uri attribute.
     */
    .factory('uriDropdownPopulator', ['$q', 'lodash', 'DropdownPopulatorInterface', 'extend', 'restServiceFactory', 'getDataFromResponse', 'getKeyHoldingDataFromResponse', function($q, lodash, DropdownPopulatorInterface, extend, restServiceFactory, getDataFromResponse, getKeyHoldingDataFromResponse) {

        var uriDropdownPopulator = function() {};

        uriDropdownPopulator = extend(DropdownPopulatorInterface, uriDropdownPopulator);

        uriDropdownPopulator.prototype._buildQueryParams = function(dependsOn, model) {
            var queryParams = dependsOn.split(",").reduce(function(obj, current) {
                obj[current] = model[current];
                return obj;
            }, {});

            return queryParams;
        };

        /**
         * @ngdoc method
         * @name uriDropdownPopulatorModule.service:uriDropdownPopulator#fetchAll
         * @methodOf uriDropdownPopulatorModule.service:uriDropdownPopulator
         *
         * @description
         * Implementation of the {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#fetchAll DropdownPopulatorInterface.fetchAll} method
         */
        uriDropdownPopulator.prototype.fetchAll = function(payload) {

            var params;

            if (payload.field.dependsOn) {
                params = this._buildQueryParams(payload.field.dependsOn, payload.model);
            }

            return restServiceFactory.get(payload.field.uri).get(params).then(function(response) {
                var dataFromResponse = getDataFromResponse(response);
                var options = this.populateAttributes(dataFromResponse, payload.field.idAttribute, payload.field.labelAttributes);

                if (payload.search) {
                    options = this.search(options, payload.search);
                }

                return $q.when(options);
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name uriDropdownPopulatorModule.service:uriDropdownPopulator#fetchPage
         * @methodOf uriDropdownPopulatorModule.service:uriDropdownPopulator
         *
         * @description
         * Implementation of the {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#fetchPage DropdownPopulatorInterface.fetchPage} method
         */
        uriDropdownPopulator.prototype.fetchPage = function(payload) {

            var params = {};

            if (payload.field.dependsOn) {
                params = this._buildQueryParams(payload.field.dependsOn, payload.model);
            }

            params.pageSize = payload.pageSize;
            params.currentPage = payload.currentPage;
            params.mask = payload.search;

            if (payload.field.params) {
                lodash.extend(params, payload.field.params);
            }

            return restServiceFactory.get(payload.field.uri).get(params).then(function(response) {
                var key = getKeyHoldingDataFromResponse(response);
                response[key] = this.populateAttributes(response[key], payload.field.idAttribute, payload.field.labelAttributes);

                return $q.when(response);
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name uriDropdownPopulatorModule.service:uriDropdownPopulator#getItem
         * @methodOf uriDropdownPopulatorModule.service:uriDropdownPopulator
         *
         * @description
         * Implementation of the {@link DropdownPopulatorInterfaceModule.DropdownPopulatorInterface#getItem DropdownPopulatorInterface.getItem} method
         *
         * @param {Object} payload The payload object containing the uri and other options
         * @param {String} payload.id The id of the item to fetch
         * @param {String} payload.field.uri The uri used to make a rest call to fetch data
         * @param {String} [payload.field.dependsOn=null] A comma separated list of attributes to include from the model when building the request params
         * @param {String} [payload.field.idAttribute=id] The name of the attribute to use when setting the id attribute
         * @param {String} [payload.field.labelAttributes=label] A list of attributes to use when setting the label attribute
         * @param {String} [payload.model=null] The model used when building query params on attributes defined in payload.field.dependsOn
         * 
         * @returns {Promise} A promise that resolves to the option that was fetched
         */
        uriDropdownPopulator.prototype.getItem = function(payload) {
            return restServiceFactory.get(payload.field.uri).getById(payload.id).then(function(item) {
                if (payload.field.labelAttributes) {
                    item = this.populateAttributes([item], payload.field.idAttribute, payload.field.labelAttributes)[0];
                }

                return $q.when(item);
            }.bind(this));
        };

        return new uriDropdownPopulator();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    angular.module('editorFieldMappingServiceModule', ['yLoDashModule'])

    /**
     * FLOAT PRECISION 
     */
    .constant('GENERIC_EDITOR_FLOAT_PRECISION', '0.01')
        /**
         * @ngdoc service
         * @name editorFieldMappingServiceModule.service:editorFieldMappingService
         * @description
         * The editorFieldMappingServices contains the strategy that the {@link genericEditorModule.directive:genericEditor genericEditor} directive
         * uses to select a property editor for a specific cmsStructureType.
         * The cmsStructureType for a specific field name is retrieved from a call to the REST Structure API. 
         * 
         * The genericEditor uses preselected property editors.
         * The genericEditor selects the property editor based on the cmsStructureType.
         * Property editors are defined for the following default cmsStructureTypes:
         * 	<ul>
         * 		<li><b>ShortString</b>:			Displays a text input field.</li>
         * 		<li><b>LongString</b>:  		Displays a text area.</li>
         * 		<li><b>RichText</b>:    		Displays an HTML/rich text editor.</li>
         * 		<li><b>Boolean</b>:     		Displays a check box.</li>
         * 		<li><b>DateTime</b>:        	Displays an input field with a date-time picker.</li>
         * 		<li><b>Media</b>:       		Displays a filterable dropdown list of media</li>
         * 		<li><b>Enum</b>:		 		Displays a filterable dropdown list of the enum class values identified by cmsStructureEnumType property.
         * 		<li><b>EditableDropdown</b>: 	Displays a configurable dropdown list that is enabled by {@link seDropdownModule.directive:seDropdown seDropdown} directive.
         * </ul>
         * 
         * You can program the {@link genericEditorModule.directive:genericEditor genericEditor} to use other property editors for these cmsStructureTypes. You can also add custom cmsStructureTypes.
         * All default and custom property editors are HTML templates. These templates must adhere to the PropertyEditorTemplate {@link editorFieldMappingServiceModule.service:PropertyEditorTemplate contract}.
         */

    /**
     * @ngdoc object
     * @name editorFieldMappingServiceModule.service:PropertyEditorTemplate
     * @description
     * The purpose of the property editor template is to assign a value to model[qualifier].
     * To achieve this goal, templates will receive the following entities in their scope:
     */
    /**
     * @ngdoc property
     * @name field
     * @propertyOf editorFieldMappingServiceModule.service:PropertyEditorTemplate
     * @description
     * The field description of the field being edited as defined by the structure API described in {@link genericEditorModule.service:GenericEditor genericEditor}
     **/
    /**
     * @ngdoc property
     * @name qualifier
     * @propertyOf editorFieldMappingServiceModule.service:PropertyEditorTemplate
     * @description
     * If the field is not localized, this is the actual field.qualifier, if it is localized, it is the language identifier such as en, de...
     **/
    /**
     * @ngdoc property
     * @name model
     * @propertyOf editorFieldMappingServiceModule.service:PropertyEditorTemplate
     * @description
     * If the field is not localized, this is the actual full parent model object, if it is localized, it is the language map: model[field.qualifier].
     *
     */
    .service('editorFieldMappingService', ['GENERIC_EDITOR_FLOAT_PRECISION', '$log', 'lodash', function(GENERIC_EDITOR_FLOAT_PRECISION, $log, lodash) {

        // Constants
        var KEY_SEPARATOR = '_';

        // Variables
        this._editorsFieldMapping = {};

        this._getMappingKey = function(structureTypeName, componentTypeName, discriminator) {
            var mappingKey = structureTypeName;
            if (componentTypeName) {
                mappingKey += KEY_SEPARATOR + componentTypeName;
                if (discriminator) {
                    mappingKey += (KEY_SEPARATOR + discriminator);
                }
            }

            return mappingKey;
        };
        /**
         * @ngdoc method
         * @name editorFieldMappingServiceModule.service:editorFieldMappingService#addFieldMapping
         * @methodOf editorFieldMappingServiceModule.service:editorFieldMappingService
         * @description
         * This method overrides the default strategy of the {@link genericEditorModule.directive:genericEditor genericEditor} directive
         * used to choose the property editor for a given cmsStructureType.
         * This method can be invoked 3 different ways (cmsStructureType and configuration are always mandatory):
         * - If you only specify cmsStructureType, your custom editor will be used for all fields of the specified cmsStructureType in any smarteditComponentType.
         * - If you specify cmsStructureType and smarteditComponentType, your custom editor will be used for all fields of the specified cmsStructureType only for the specified smarteditComponentType.
         * - If you specify  cmsStructureType, smarteditComponentType and discriminator, your custom editor will only be used for the field of the specified discriminator and cmsStructureType only for the specified smarteditComponentType.
         * 
         * If multiple overrides are set, the one which specifies the most conditions is used.
         * 
         * Note: 
         * Currently, all templates in SmartEdit use the short form. Thus, when the field mapping is retrieved by the generic editor, the system will ensure that 
         * the template is in short form. E.g, 
         * - A template 'web/common/services/genericEditor/templates/shortStringTemplate.html' will be transformed to 'shortStringTemplate.html'
         * 
         * @param {String} cmsStructureType The cmsStructureType for which a custom property editor is required. Cannot be null.
         * @param {String=} smarteditComponentType The SmartEdit component type that the custom property editor is created for. Can be null.
         * @param {String} discriminator The field name of the smarteditComponentType that the custom property editor is created for. Can be null.
         * @param {Object} configuration The holder that contains the override instructions. Cannot be null.
         * @param {String} configuration.template The path to the HTML template used in the override. Cannot be null.
         * @param {Function=} configuration.customSanitize Custom sanitize function for a custom property editor. It's provided with a payload and an optional {@link functionsModule.sanitize sanitize} function.
         */
        this.addFieldMapping = function(structureTypeName, componentTypeName, discriminator, configuration) {
            componentTypeName = componentTypeName || "";

            var mappingKey = this._getMappingKey(structureTypeName, componentTypeName, discriminator);

            this._editorsFieldMapping[mappingKey] = lodash.cloneDeep(configuration);
        };

        /**
         * @ngdoc method
         * @name editorFieldMappingServiceModule.service:editorFieldMappingService#getFieldMapping
         * @methodOf editorFieldMappingServiceModule.service:editorFieldMappingService
         * @description
         * 
         * This method retrieves the property editor rendered for a given cmsStructureType in a generic editor. 
         * 
         * Note: 
         * Currently, all templates in SmartEdit use the short form. Thus, before returning, this method ensures that 
         * the template provided to the generic editor is in short form. E.g, 
         * - A template 'web/common/services/genericEditor/templates/shortStringTemplate.html' will be transformed to 'shortStringTemplate.html'
         * 
         * @param {String} structureTypeName The cmsStructureType for which to retrieve its property editor.
         * @param {String=} componentTypeName The SmartEdit component type for which to retrieve its property editor.
         * @param {String=} discriminator The field name of the smarteditComponentType for which to retrieve its discriminator. 
         */
        this.getFieldMapping = function(structureTypeName, componentTypeName, discriminator) {
            componentTypeName = componentTypeName || "";
            // Go from specific to generic.
            var params = [structureTypeName, componentTypeName, discriminator];

            var fieldMapping = null;
            while (params.length > 0) {
                var mappingKey = this._getMappingKey.apply(this, params);
                fieldMapping = this._editorsFieldMapping[mappingKey];
                if (fieldMapping) {
                    break;
                }

                params.pop();
            }

            if (!fieldMapping) {
                $log.warn('editorFieldMappingService - Cannot find suitable field mapping for type ', structureTypeName);
                fieldMapping = null;
            } else {
                fieldMapping.template = this._cleanTemplate(fieldMapping.template);
            }

            return fieldMapping;
        };

        this._cleanTemplate = function(template) {
            var index = (template) ? template.lastIndexOf('/') : -1;
            if (index !== -1) {
                template = template.substring(index + 1);
            }

            return template;
        };

        this._registerDefaultFieldMappings = function() {
            if (lodash.isEmpty(this._editorsFieldMapping)) {
                this.addFieldMapping('Boolean', null, null, {
                    template: 'booleanWrapperTemplate.html'
                });

                this.addFieldMapping('ShortString', null, null, {
                    template: 'shortStringTemplate.html'
                });

                this.addFieldMapping('LongString', null, null, {
                    template: 'longStringTemplate.html'
                });

                this.addFieldMapping('RichText', null, null, {
                    template: 'richTextTemplate.html'
                });

                this.addFieldMapping('Number', null, null, {
                    template: 'numberTemplate.html'
                });

                this.addFieldMapping('Float', null, null, {
                    template: 'floatTemplate.html',
                    precision: GENERIC_EDITOR_FLOAT_PRECISION
                });

                this.addFieldMapping('Dropdown', null, null, {
                    template: 'dropdownTemplate.html'
                });

                this.addFieldMapping('EditableDropdown', null, null, {
                    template: 'dropdownWrapperTemplate.html'
                });

                this.addFieldMapping('DateTime', null, null, {
                    template: 'dateTimePickerWrapperTemplate.html'
                });

                this.addFieldMapping('Enum', null, null, {
                    template: 'enumTemplate.html'
                });
            }

        };
    }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @deprecated since 6.5
 * @ngdoc overview
 * @name seGenericEditorFieldErrorsModule
 *
 * @description
 * This module provides the seGenericEditorFieldErrors component, which is used to show validation errors.
 */
angular.module('seGenericEditorFieldErrorsModule', [])
    .controller('seGenericEditorFieldErrorsController', function() {
        this.getFilteredErrors = function() {
            return (this.field.errors || []).filter(function(error) {
                return error.language === this.qualifier && !error.format;
            }.bind(this)).map(function(error) {
                return error.message;
            });
        };
    })
    .directive('seGenericEditorFieldErrors', function() {
        return {
            templateUrl: 'seGenericEditorFieldErrorsTemplate.html',
            restrict: 'E',
            scope: {},
            controller: 'seGenericEditorFieldErrorsController',
            controllerAs: 'ctrl',
            bindToController: {
                field: '=',
                qualifier: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name genericEditorModule
 */
angular.module('genericEditorModule', ['yjqueryModule', 'genericEditorFieldModule', 'restServiceFactoryModule', 'functionsModule', 'eventServiceModule', 'coretemplates', 'translationServiceModule', 'localizedElementModule', 'languageServiceModule', 'experienceInterceptorModule', 'dateTimePickerModule', 'seBooleanModule', 'fetchEnumDataHandlerModule', 'seRichTextFieldModule', 'seValidationMessageParserModule', 'seDropdownModule', 'editorFieldMappingServiceModule', 'yLoDashModule', 'seConstantsModule'])
    /**
     * @deprecated since 6.5 use GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT.
     * @ngdoc object
     * @name genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT
     * @description
     * Event to notify GenericEditor about errors that can be relevant to it.
     * Any GenericEditor may receive notification from another GenericEditor that the latter received validation errors not relevant for themselves
     * In such a situation every GenericEditor will try and see if errors are relevant to them.
     */
    .constant('GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT', 'UnrelatedValidationErrors')
    /**
     * @ngdoc object
     * @name genericEditorModule.object:GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT
     * @description
     * Event to notify GenericEditor about messages (errors, warnings) that can be relevant to it.
     * Any GenericEditor may receive notification from another GenericEditor that the latter received validation messages (errors, warnings) not relevant for themselves.
     * In such a situation every GenericEditor will try and see if messages are relevant to them.
     *
     * @param {Object} payload A map that contains the 'messages' and optional 'targetGenericEditorId' properties.
     * @param {Array} payload.messages List of validation messages (errors, warnings)
     * @param {String=} payload.targetGenericEditorID The id of target generic editor. Allows to send a list of messages to a specific generic editor. Otherwise the list of messages will be delivered to all generic editors.
     *
     * @example systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {messages: unrelatedValidationMessages, targetGenericEditorId: 'optional-id'});
     */
    .constant('GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'UnrelatedValidationMessagesEvent')
    /**
     * @ngdoc object
     * @name genericEditorModule.object:GENERIC_EDITOR_LOADED_EVENT
     * @description
     * Event to notify subscribers that GenericEditor is loaded.
     */
    .constant('GENERIC_EDITOR_LOADED_EVENT', 'genericEditorLoadedEvent')
    .run(['editorFieldMappingService', function(editorFieldMappingService) {
        editorFieldMappingService._registerDefaultFieldMappings();
    }])
    /**
     * @ngdoc service
     * @name genericEditorModule.service:GenericEditor
     * @description
     * The Generic Editor is a class that makes it possible for SmartEdit users (CMS managers, editors, etc.) to edit components in the SmartEdit interface.
     * The Generic Editor class is used by the {@link genericEditorModule.directive:genericEditor genericEditor} directive.
     * The genericEditor directive makes a call either to a Structure API or, if the Structure API is not available, it reads the data from a local structure to request the information that it needs to build an HTML form.
     * It then requests the component by its type and ID from the Content API. The genericEditor directive populates the form with the data that is has received.
     * The form can now be used to edit the component. The modified data is saved using the Content API if it is provided else it would return the form data itself.
     * <br/><br/>
     * <strong>The structure and the REST structure API</strong>.
     * <br/>
     * The constructor of the {@link genericEditorModule.service:GenericEditor GenericEditor} must be provided with the pattern of a REST Structure API, which must contain the string  ":smarteditComponentType", or with a local data structure.
     * If the pattern, Structure API, or the local structure is not provided, the Generic Editor will fail. If the Structure API is used, it must return a JSON payload that holds an array within the attributes property.
     * If the actual structure is used, it must return an array. Each entry in the array provides details about a component property to be displayed and edited. The following details are provided for each property:
     *
     *<ul>
     * <li><strong>qualifier:</strong> Name of the property.
     * <li><strong>i18nKey:</strong> Key of the property label to be translated into the requested language.
     * <li><strong>editable:</strong> Boolean that indicates if a property is editable or not. The default value is true.
     * <li><strong>localized:</strong> Boolean that indicates if a property is localized or not. The default value is false.
     * <li><strong>required:</strong> Boolean that indicates if a property is mandatory or not. The default value is false.
     * <li><strong>cmsStructureType:</strong> Value that is used to determine which form widget (property editor) to display for a specified property.
     * The selection is based on an extensible strategy mechanism owned by {@link editorFieldMappingServiceModule.service:editorFieldMappingService editorFieldMappingService}.
     * <li><strong>cmsStructureEnumType:</strong> The qualified name of the Enum class when cmsStructureType is "Enum"
     * </li>
     * <ul><br/>
     * There are two options when you use the Structure API. The first option is to use an API resource that returns the structure object. 
     * The following is an example of the JSON payload that is returned by the Structure API in this case:
     * <pre>
     * {
     *     attributes: [{
     *         cmsStructureType: "ShortString",
     *         qualifier: "someQualifier1",
     *         i18nKey: 'i18nkeyForsomeQualifier1',
     *         localized: false
     *     }, {
     *         cmsStructureType: "LongString",
     *         qualifier: "someQualifier2",
     *         i18nKey: 'i18nkeyForsomeQualifier2',
     *         localized: false
     *    }, {
     *         cmsStructureType: "RichText",
     *         qualifier: "someQualifier3",
     *         i18nKey: 'i18nkeyForsomeQualifier3',
     *         localized: true,
     *         required: true
     *     }, {
     *         cmsStructureType: "Boolean",
     *         qualifier: "someQualifier4",
     *         i18nKey: 'i18nkeyForsomeQualifier4',
     *         localized: false
     *     }, {
     *         cmsStructureType: "DateTime",
     *         qualifier: "someQualifier5",
     *         i18nKey: 'i18nkeyForsomeQualifier5',
     *         localized: false
     *     }, {
     *         cmsStructureType: "Media",
     *         qualifier: "someQualifier6",
     *         i18nKey: 'i18nkeyForsomeQualifier6',
     *         localized: true,
     *         required: true
     *     }, {
     *         cmsStructureType: "Enum",
     *         cmsStructureEnumType:'de.mypackage.Orientation'
     *         qualifier: "someQualifier7",
     *         i18nKey: 'i18nkeyForsomeQualifier7',
     *         localized: true,
     *         required: true
     *     }]
     * }
     * </pre><br/>
     * The second option is to use an API resource that returns a list of structures. In this case, the generic editor will select the first element from the list and use it to display its attributes.
     * The generic editor expects the structures to be in one of the two fields below.  
     * <pre>
     * {
     *     structures: [{}, {}]
     * }
     *
     * or
     *
     * {
     *     componentTypes: [{}, {}]
     * }
     * </pre>
     * If the list has more than one element, the Generic Editor will throw an exception, otherwise it will get the first element on the list. 
     * The following is an example of the JSON payload that is returned by the Structure API in this case:
     * <pre>
     * {
     *     structures: [
     *         {
     *             attributes: [{
     *                 		cmsStructureType: "ShortString",
     *                 		qualifier: "someQualifier1",
     *                 		i18nKey: 'i18nkeyForsomeQualifier1',
     *                 		localized: false
     *             		}, {
     *                 		cmsStructureType: "LongString",
     *                 		qualifier: "someQualifier2",
     *                 		i18nKey: 'i18nkeyForsomeQualifier2',
     *                 		localized: false
     *         	   		}]
     *         }
     *     ]
     * }
     * </pre>
     * <pre>
     * {
     *     componentTypes: [
     *         {
     *             attributes: [{
     *                 		cmsStructureType: "ShortString",
     *                 		qualifier: "someQualifier1",
     *                 		i18nKey: 'i18nkeyForsomeQualifier1',
     *                 		localized: false
     *             		}, {
     *                 		cmsStructureType: "LongString",
     *                 		qualifier: "someQualifier2",
     *                 		i18nKey: 'i18nkeyForsomeQualifier2',
     *                 		localized: false
     *         	   		}]
     *         }
     *     ]
     * }
     * </pre>
     * The following is an example of the expected format of a structure:
     * <pre>
     *    [{
     *         cmsStructureType: "ShortString",
     *         qualifier: "someQualifier1",
     *         i18nKey: 'i18nkeyForsomeQualifier1',
     *         localized: false
     *     }, {
     *         cmsStructureType: "LongString",
     *         qualifier: "someQualifier2",
     *         i18nKey: 'i18nkeyForsomeQualifier2',
     *         editable: false,
     *         localized: false
     *    }, {
     *         cmsStructureType: "RichText",
     *         qualifier: "someQualifier3",
     *         i18nKey: 'i18nkeyForsomeQualifier3',
     *         localized: true,
     *         required: true
     *     }, {
     *         cmsStructureType: "Boolean",
     *         qualifier: "someQualifier4",
     *         i18nKey: 'i18nkeyForsomeQualifier4',
     *         localized: false
     *     }, {
     *         cmsStructureType: "DateTime",
     *         qualifier: "someQualifier5",
     *         i18nKey: 'i18nkeyForsomeQualifier5',
     *         editable: false,
     *         localized: false
     *     }, {
     *         cmsStructureType: "Media",
     *         qualifier: "someQualifier6",
     *         i18nKey: 'i18nkeyForsomeQualifier6',
     *         localized: true,
     *         required: true
     *     }, {
     *         cmsStructureType: "Enum",
     *         cmsStructureEnumType:'de.mypackage.Orientation'
     *         qualifier: "someQualifier7",
     *         i18nKey: 'i18nkeyForsomeQualifier7',
     *         localized: true,
     *         required: true
     *     }]
     * </pre>
     * 
     * <strong>The REST CRUD API</strong>, is given to the constructor of {@link genericEditorModule.service:GenericEditor GenericEditor}.
     * The CRUD API must support GET and PUT of JSON payloads.
     * The PUT method must return the updated payload in its response. Specific to the GET and PUT, the payload must fulfill the following requirements:
     * <ul>
     * 	<li>DateTime types: Must be serialized as long timestamps.</li>
     * 	<li>Media types: Must be serialized as identifier strings.</li>
     * 	<li>If a cmsStructureType is localized, then we expect that the CRUD API returns a map containing the type (string or map) and the map of values, where the key is the language and the value is the content that the type returns.</li>
     * </ul>
     *
     * The following is an example of a localized payload:
     * <pre>
     * {
     *    content: {
     * 		'en': 'content in english',
     * 		'fr': 'content in french',
     * 		'hi': 'content in hindi'
     * 	  }
     * }
     * </pre>
     *
     * <br/><br/>
     *
     * If a validation warning or error occurs, the PUT method of the REST CRUD API will return a validation warning/error object that contains an array of validation messages. The information returned for each validation message is as follows:
     * <ul>
     * 	<li><strong>subject:</strong> The qualifier that has the error</li>
     * 	<li><strong>message:</strong> The error message to be displayed</li>
     * 	<li><strong>type:</strong> The type of message returned. This is of the type ValidationError or Warning.</li>
     * 	<li><strong>language:</strong> The language the error needs to be associated with. If no language property is provided, a match with regular expression /(Language: \[)[a-z]{2}\]/g is attempted from the message property. As a fallback, it implies that the field is not localized.</li>
     * </ul>
     *
     * The following code is an example of an error response object:
     * <pre>
     * {
     *    errors: [{
     *		  subject: 'qualifier1',
     *		  message: 'error message for qualifier',
     *		  type: 'ValidationError'
     *	  }, {
     *		  subject: 'qualifier2',
     *		  message: 'error message for qualifier2 language: [fr]',
     *		  type: 'ValidationError'
     *    }, {
     *		  subject: 'qualifier3',
     *		  message: 'error message for qualifier2',
     *		  type: 'ValidationError'
     *    }, {
     *        subject: 'qualifier4',
     *        message: 'warning message for qualifier4',
     *        type: 'Warning'
     *    }]
     * }
     *
     * </pre>
     *
     * Whenever any sort of dropdown is used in one of the cmsStructureType widgets, it is advised using {@link genericEditorModule.service:GenericEditor#methods_refreshOptions refreshOptions method}. See this method documentation to learn more.
     *
     */
    .factory('GenericEditor', ['yjQuery', 'GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT', 'GENERIC_EDITOR_LOADED_EVENT', 'GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT', 'VALIDATION_MESSAGE_TYPES', 'lodash', 'restServiceFactory', 'languageService', 'sharedDataService', 'systemEventService', 'sanitize', 'sanitizeHTML', 'copy', 'isBlank', 'isObjectEmptyDeep', '$q', '$log', '$translate', '$injector', 'seValidationMessageParser', 'editorFieldMappingService', 'resetObject', 'CONTEXT_SITE_ID', 'CONTEXT_CATALOG', 'CONTEXT_CATALOG_VERSION', function(yjQuery, GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT, GENERIC_EDITOR_LOADED_EVENT, GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, VALIDATION_MESSAGE_TYPES, lodash, restServiceFactory, languageService, sharedDataService, systemEventService, sanitize, sanitizeHTML, copy, isBlank, isObjectEmptyDeep, $q, $log, $translate, $injector, seValidationMessageParser, editorFieldMappingService, resetObject, CONTEXT_SITE_ID, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION) {

        var primitiveTypes = ["Boolean", "ShortString", "LongString", "RichText", "Date", "Dropdown"];

        editorFieldMappingService._registerDefaultFieldMappings();

        var validate = function(conf) {
            if (isBlank(conf.structureApi) && !conf.structure) {
                throw "genericEditor.configuration.error.no.structure";
            } else if (!isBlank(conf.structureApi) && conf.structure) {
                throw "genericEditor.configuration.error.2.structures";
            }
        };

        /**
         * @constructor
         */
        var GenericEditor = function(conf) {
            validate(conf);
            this.id = conf.id;
            this.smarteditComponentType = conf.smarteditComponentType;
            this.smarteditComponentId = conf.smarteditComponentId;
            this.updateCallback = conf.updateCallback;
            this.structure = conf.structure;
            if (conf.structureApi) {
                this.editorStructureService = restServiceFactory.get(conf.structureApi);
            }
            this.uriContext = conf.uriContext;
            if (conf.contentApi) {
                this.editorCRUDService = restServiceFactory.get(conf.contentApi);
            }
            this.initialContent = conf.content;
            this.component = null;
            this.fields = [];
            this.languages = [];

            if (conf.customOnSubmit) {
                this.onSubmit = conf.customOnSubmit;
            }

            this._unregisterUnrelatedErrorsEvent = systemEventService.registerEventHandler(GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT, this._handleLegacyUnrelatedValidationErrors.bind(this));
            this._unregisterUnrelatedMessagesEvent = systemEventService.registerEventHandler(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, this._handleUnrelatedValidationMessages.bind(this));
        };

        GenericEditor.prototype._finalize = function() {
            this._unregisterUnrelatedErrorsEvent();
            this._unregisterUnrelatedMessagesEvent();
        };

        GenericEditor.prototype._setApi = function(api) {
            this.api = api;
        };

        GenericEditor.prototype._handleLegacyUnrelatedValidationErrors = function(key, validationMessages, forceEvenIfNotDirty) {
            this._handleUnrelatedValidationMessages(key, {
                messages: validationMessages
            }, forceEvenIfNotDirty);
        };

        GenericEditor.prototype._handleUnrelatedValidationMessages = function(key, validationData, forceEvenIfNotDirty) {
            if (validationData.targetGenericEditorId && validationData.targetGenericEditorId !== this.id) {
                return;
            }

            // A tab only cares about unrelated validation errors in other tabs if it isn't dirty.
            // Otherwise its default handling mechanism will find the errors.
            if (!this.isDirty() || forceEvenIfNotDirty) {
                this.removeValidationMessages();
                this._displayValidationMessages(validationData.messages);
                var hasErrors = this._containsValidationMessageType(validationData.messages, VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR);
                if (hasErrors) {
                    systemEventService.sendAsynchEvent("EDITOR_IN_ERROR_EVENT", this.id);
                }
            }
            return $q.when();
        };

        GenericEditor.prototype._isPrimitive = function(type) {
            return primitiveTypes.indexOf(type) > -1;
        };

        GenericEditor.prototype._getSelector = function(selector) {
            return yjQuery(selector);
        };

        /**
         * @ngdoc method
         * @name genericEditorModule.service:GenericEditor#reset
         * @methodOf genericEditorModule.service:GenericEditor
         *
         * @description
         * Sets the content within the editor to its original state.
         *
         * @param {Object} componentForm The component form to be reset.
         */
        GenericEditor.prototype.reset = function(componentForm) {
            //need to empty the searches for refreshOptions to enable resetting to pristine state
            this._getSelector('.ui-select-search').val('');
            this._getSelector('.ui-select-search').trigger('input');
            this.removeValidationMessages();
            this.component = resetObject(this.component, this.pristine);

            this.fields.forEach(function(field) {
                delete field.initiated;
            });
            if (!this.holders) {
                this.holders = this.fields.map(function(field) {
                    return {
                        editor: this,
                        field: field
                    };
                }.bind(this));
            }
            if (componentForm) {
                componentForm.$setPristine();
            }
            return $q.when();
        };

        GenericEditor.prototype.removeValidationMessages = function() {
            for (var f = 0; f < this.fields.length; f++) {
                var field = this.fields[f];
                field.messages = undefined;
                field.hasErrors = false;
                field.hasWarnings = false;
            }
        };

        GenericEditor.prototype.fetch = function() {
            return this.initialContent ? $q.when(this.initialContent) : (this.smarteditComponentId ? this.editorCRUDService.get({
                identifier: this.smarteditComponentId
            }) : $q.when({}));
        };

        GenericEditor.prototype.sanitizeLoad = function(response) {
            this.fields.forEach(function(field) {
                if (field.localized === true && isBlank(response[field.qualifier])) {
                    response[field.qualifier] = {};
                }
            });
            return response;
        };

        GenericEditor.prototype.load = function() {
            var deferred = $q.defer();
            this.fetch().then(
                function(response) {
                    this.pristine = this.sanitizeLoad(response);
                    this.reset();

                    deferred.resolve();
                }.bind(this),
                function(failure) {
                    $log.error("GenericEditor.load failed");
                    $log.error(failure);
                    deferred.reject();
                }
            );
            return deferred.promise;
        };

        /**
         * upon submitting, server side may have been updated,
         * since we PUT and not PATCH, we need to take latest of the fields not presented and push them back with the editable ones
         */
        GenericEditor.prototype._merge = function(refreshedComponent, modifiedComponent) {

            var merger = refreshedComponent;
            modifiedComponent = copy(modifiedComponent);

            this.fields.forEach(function(field) {
                if (field.editable === true) {
                    merger[field.qualifier] = modifiedComponent[field.qualifier];
                }
            }.bind(this));

            return merger;
        };

        GenericEditor.prototype.getComponent = function() {
            return this.component;
        };

        GenericEditor.prototype.sanitizePayload = function(payload, fields) {

            var CMS_STRUCTURE_TYPE = {
                SHORT_STRING: "ShortString",
                LONG_STRING: "LongString"
            };

            fields.filter(function(field) {
                return (field.cmsStructureType === CMS_STRUCTURE_TYPE.LONG_STRING || field.cmsStructureType === CMS_STRUCTURE_TYPE.SHORT_STRING || typeof field.customSanitize === 'function');
            }).map(function(field) {
                return {
                    name: field.qualifier,
                    localized: !!field.localized,
                    customSanitize: field.customSanitize
                };
            }).forEach(function(fieldInfo) {

                if (typeof payload[fieldInfo.name] !== 'undefined' && fieldInfo.name in payload) {
                    if (fieldInfo.customSanitize) {
                        fieldInfo.customSanitize(payload[fieldInfo.name], sanitize);
                    } else {
                        if (fieldInfo.localized) {
                            var qualifierValueObject = payload[fieldInfo.name];
                            Object.keys(qualifierValueObject).forEach(function(locale) {
                                qualifierValueObject[locale] = sanitize(qualifierValueObject[locale]);
                            });
                        } else {
                            payload[fieldInfo.name] = sanitize(payload[fieldInfo.name]);
                        }
                    }
                }

            });

            return payload;
        };

        GenericEditor.prototype._fieldsAreUserChecked = function() {
            return this.fields.every(function(field) {
                var requiresUserCheck = false;
                for (var qualifier in field.requiresUserCheck) {
                    requiresUserCheck = requiresUserCheck || field.requiresUserCheck[qualifier];
                }
                return !requiresUserCheck || field.isUserChecked;
            });
        };


        /**
         * Check for html validation errors on all the form fields
         * If so, assign an error to the field.
         * The seGenericEditorFieldError will render these errors, just like
         * errors we receive form the backend
         *
         * @param componentForm The generic editor html form from the genericEditorTemplate.html
         * @returns {boolean} true if the form passes html validation
         */
        GenericEditor.prototype._doHtmlFieldValidation = function(componentForm) {
            if (!componentForm.$valid) {
                var validationErrors = this.fields.filter(function(field) {
                    var formField = componentForm[field.qualifier];
                    return formField && formField.$invalid === true;
                }).map(function(field) {
                    return {
                        type: VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR,
                        subject: field.qualifier,
                        message: 'se.editor.html.validation.error'
                    };
                });
                if (validationErrors.length > 0) {
                    this._handleUnrelatedValidationMessages(null, {
                        messages: validationErrors
                    }, true);
                }
                return validationErrors.length === 0;
            } else {
                return true;
            }

        };


        /**
         * @ngdoc method
         * @name genericEditorModule.service:GenericEditor#preparePayload
         * @methodOf genericEditorModule.service:GenericEditor
         *
         * @description
         * Transforms the payload before POST/PUT to server
         *
         * @param {Object} the transformed payload
         */
        GenericEditor.prototype.preparePayload = function(originalPayload) {
            return $q.when(originalPayload);
        };

        GenericEditor.prototype.onSubmit = function() {

            return this.fetch().then(function(refreshedComponent) {
                var payload = this._merge(refreshedComponent, this.component);
                payload = copy(payload);

                payload = this.sanitizePayload(payload, this.fields);

                if (this.smarteditComponentId) {
                    payload.identifier = this.smarteditComponentId;
                }

                // if POST mode
                if (this.editorCRUDService && !this.smarteditComponentId) {
                    // if we have a type field in the structure, use it for the type in the POST payload
                    if (this.structure && this.structure.type) {
                        // if the user already provided a type field, lets be nice
                        if (!payload.type) {
                            payload.type = this.structure.type;
                        }
                    }
                }

                return this.preparePayload(payload).then(function(preparedPayload) {
                    var promise = this.editorCRUDService ? (this.smarteditComponentId ? this.editorCRUDService.update(preparedPayload) : this.editorCRUDService.save(preparedPayload)) : $q.when(preparedPayload);
                    return promise.then(function(response) {
                        return {
                            payload: payload,
                            response: response
                        };
                    });
                }.bind(this));

            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name genericEditorModule.service:GenericEditor#submit
         * @methodOf genericEditorModule.service:GenericEditor
         *
         * @description
         * Saves the content within the form for a specified component. If there are any validation errors returned by the CRUD API after saving the content, it will display the errors.
         *
         * @param {Object} componentForm The component form to be saved.
         */
        GenericEditor.prototype.submit = function(componentForm) {
            var deferred = $q.defer();

            var cleanComponent = this.getComponent(componentForm);

            // It's necessary to remove validation errors even if the form is not dirty. This might be because of unrelated validation errors
            // triggered in other tab.
            this.removeValidationMessages();
            this.hasFrontEndValidationErrors = false;

            if (!this._fieldsAreUserChecked()) {
                deferred.reject(true); // Mark this tab as "in error" due to front-end validation. 
                this.hasFrontEndValidationErrors = true;
            } else if (!this._doHtmlFieldValidation(componentForm)) {
                deferred.reject(true);
            } else if (componentForm.$valid) {
                /*
                 * upon submitting, server side may have been updated,
                 * since we PUT and not PATCH, we need to take latest of the fields not presented and send them back with the editable ones
                 */
                this.onSubmit().then(function(submitResult) {
                    // If we're doing a POST or PUT and the request returns non empty response, then this response is returned.
                    // Otherwise the payload for the request is returned.
                    if (submitResult.response) {
                        this.pristine = copy(submitResult.response);
                    } else {
                        this.pristine = copy(submitResult.payload);
                    }

                    delete this.pristine.identifier;

                    if (!this.smarteditComponentId && submitResult.response) {
                        this.smarteditComponentId = submitResult.response.uuid;
                    }
                    this.removeValidationMessages();

                    this.reset(componentForm);
                    deferred.resolve(lodash.cloneDeep(this.pristine));
                    if (this.updateCallback) {
                        this.updateCallback(this.pristine, submitResult.response);
                    }
                }.bind(this), function(failure) {
                    this.removeValidationMessages();
                    this._displayValidationMessages(failure.data.errors);
                    var hasErrors = this._containsValidationMessageType(failure.data.errors, VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR);
                    //send unrelated validation messages to any other listening genericEditor when no other errors
                    var unrelatedValidationMessages = this._collectUnrelatedValidationMessages(failure.data.errors);
                    if (unrelatedValidationMessages.length > 0) {
                        systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_ERRORS_EVENT, unrelatedValidationMessages);
                        systemEventService.sendAsynchEvent(GENERIC_EDITOR_UNRELATED_VALIDATION_MESSAGES_EVENT, {
                            messages: unrelatedValidationMessages
                        });
                        deferred.reject(hasErrors); // Marks this tab if it has errors.
                    } else {
                        deferred.reject(true); // Marks this tab as "in error".
                    }
                }.bind(this));
            } else {
                $log.error("GenericEditor.submit() - unable to submit form. Form is unexpectedly invalid.");
                deferred.reject(cleanComponent);
            }
            return deferred.promise;
        };

        GenericEditor.prototype._validationMessageBelongsToCurrentInstance = function(validationMessage) {
            return lodash.some(this.fields, function(field) {
                return field.qualifier === validationMessage.subject;
            });
        };

        GenericEditor.prototype._containsValidationMessageType = function(validationMessages, messageType) {
            return lodash.some(validationMessages, function(message) {
                return message.type === messageType && this._validationMessageBelongsToCurrentInstance(message);
            }.bind(this));
        };

        GenericEditor.prototype._isValidationMessageType = function(messageType) {
            return lodash.includes(lodash.values(VALIDATION_MESSAGE_TYPES), messageType);
        };

        GenericEditor.prototype._displayValidationMessages = function(validationMessages) {
            validationMessages.filter(function(message) {
                return this._isValidationMessageType(message.type);
            }.bind(this)).forEach(function(validationMessage) {

                validationMessage.type = validationMessage.type || VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR;

                var field = this.fields.filter(function(element) {
                    return (element.qualifier === validationMessage.subject);
                })[0];

                if (field) {
                    if (field.messages === undefined) {
                        field.messages = [];
                    }

                    var message = seValidationMessageParser.parse(validationMessage.message);
                    message.marker = field.localized ? message.language : field.qualifier;
                    message.type = validationMessage.type;

                    field.messages.push(message);
                    field.hasErrors = this._containsValidationMessageType(validationMessages, VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR);
                    field.hasWarnings = this._containsValidationMessageType(validationMessages, VALIDATION_MESSAGE_TYPES.WARNING);
                }
            }.bind(this));

            return $q.when();
        };

        GenericEditor.prototype._collectUnrelatedValidationMessages = function(messages) {
            return messages.filter(function(message) {
                return this._isValidationMessageType(message.type) && !this._validationMessageBelongsToCurrentInstance(message);
            }.bind(this));
        };

        GenericEditor.prototype.fieldAdaptor = function(fields) {

            fields.forEach(function(field) {
                var fieldMapping = editorFieldMappingService.getFieldMapping(field.cmsStructureType, this.smarteditComponentType, field.qualifier);
                lodash.assign(field, fieldMapping);

                if (field.editable === undefined) {
                    field.editable = true;
                }

                if (!field.postfixText) {
                    var key = (this.smarteditComponentType ? this.smarteditComponentType.toLowerCase() : '') + '.' + field.qualifier.toLowerCase() + '.postfix.text';
                    var translated = $translate.instant(key);
                    field.postfixText = translated !== key ? translated : "";
                }

                field.smarteditComponentType = this.smarteditComponentType;
            }.bind(this));
            return fields;
        };

        /**
         * @ngdoc method
         * @name genericEditorModule.service:GenericEditor#refreshOptions
         * @methodOf genericEditorModule.service:GenericEditor
         *
         * @description
         * Is invoked by HTML field templates that update and manage dropdowns.
         *  It updates the dropdown list upon initialization (creates a list of one option) and when performing a search (returns a filtered list).
         *  To do this, the GenericEditor fetches an implementation of the  {@link FetchDataHandlerInterfaceModule.FetchDataHandlerInterface FetchDataHandlerInterface} using the following naming convention: 
         * <pre>"fetch" + cmsStructureType + "DataHandler"</pre>
         * @param {Object} field The field in the structure that requires a dropdown to be built.
         * @param {string} qualifier For a non-localized field, it is the actual field.qualifier. For a localized field, it is the ISO code of the language.
         * @param {string} search The value of the mask to filter the dropdown entries on.
         */

        GenericEditor.prototype.refreshOptions = function(field, qualifier, search) {
            var theHandlerObj = "fetch" + field.cmsStructureType + "DataHandler";
            var theIdentifier, optionsIdentifier;

            if (field.localized) {
                theIdentifier = this.component[field.qualifier][qualifier];
                optionsIdentifier = qualifier;
            } else {
                theIdentifier = this.component[field.qualifier];
                optionsIdentifier = field.qualifier;
            }

            var objHandler = $injector.get(theHandlerObj);

            field.initiated = field.initiated || [];
            field.options = field.options || {};

            if (field.cmsStructureType === 'Enum') {
                field.initiated.push(optionsIdentifier);
            }
            if (field.initiated.indexOf(optionsIdentifier) > -1) {
                if (search.length > 2 || field.cmsStructureType === 'Enum') {
                    objHandler.findByMask(field, search).then(function(entities) {
                        field.options[optionsIdentifier] = entities;
                    });
                }
            } else if (theIdentifier) {
                objHandler.getById(field, theIdentifier).then(function(entity) {
                    field.options[optionsIdentifier] = [entity];
                    field.initiated.push(optionsIdentifier);
                }.bind(field));
            } else {
                field.initiated.push(optionsIdentifier);
            }
        };

        GenericEditor.prototype._buildComparable = function(source) {
            if (!source) {
                return source;
            }
            var comparable = {};
            this.fields.forEach(function(field) {
                var fieldValue = source[field.qualifier];
                if (field.localized) {
                    var sub = {};
                    angular.forEach(fieldValue, function(langValue, lang) {
                        if (langValue !== null) {
                            sub[lang] = _buildFieldComparable(langValue, field);
                        }
                    });
                    comparable[field.qualifier] = sub;
                } else {
                    comparable[field.qualifier] = _buildFieldComparable(fieldValue, field);
                }
            });

            //sometimes, such as in navigationNodeEntryEditor, we update properties not part of the fields and still want the editor to turn dirty
            angular.forEach(source, function(value, key) {
                var notDisplayed = !this.fields.some(function(field) {
                    return field.qualifier === key;
                }.bind(this));
                if (notDisplayed) {
                    comparable[key] = value;
                }
            }.bind(this));

            return comparable;
        };

        var _buildFieldComparable = function(fieldValue, field) {
            switch (field.cmsStructureType) {
                case 'RichText':
                    return fieldValue !== undefined ? sanitizeHTML(fieldValue) : null;
                case 'Boolean':
                    return fieldValue !== undefined ? fieldValue : false;
                default:
                    return fieldValue;
            }
        };


        /**
         * @ngdoc method
         * @name genericEditorModule.service:GenericEditor#isDirty
         * @methodOf genericEditorModule.service:GenericEditor
         *
         * @description
         * A predicate function that returns true if the editor is in dirty state or false if it not.
         *  The state of the editor is determined by comparing the current state of the component with the state of the component when it was pristine.
         *
         * @return {Boolean} An indicator if the editor is in dirty state or not.
         */
        GenericEditor.prototype.isDirty = function() {
            //try to get away from angular.equals
            this.bcPristine = this._buildComparable(this.pristine);
            this.bcComp = this._buildComparable(this.component);
            return !angular.equals(this._buildComparable(this.pristine), this._buildComparable(this.component));
        };

        GenericEditor.prototype.isValid = function(componentForm) {
            if (componentForm) {
                this.fields.forEach(function(field) {
                    var componentField = componentForm[field.qualifier];
                    if (!lodash.isNil(componentField) && componentField.$invalid === true) {
                        return false;
                    }
                });
            }
            return !this.fields.filter(function(field) {
                    return field.required;
                })
                .map(function(field) {
                    return field.qualifier;
                })
                .some(function(qualifier) {
                    return this.component && isObjectEmptyDeep(this.component[qualifier]);
                }.bind(this));
        };

        GenericEditor.prototype._getUriContext = function() {

            return this.uriContext ? $q.when(this.uriContext) : sharedDataService.get('experience').then(function(experience) {
                var uriContext = {};
                uriContext[CONTEXT_SITE_ID] = experience.siteDescriptor.uid;
                uriContext[CONTEXT_CATALOG] = experience.catalogDescriptor.catalogId;
                uriContext[CONTEXT_CATALOG_VERSION] = experience.catalogDescriptor.catalogVersion;
                return uriContext;
            });
        };

        /**
         * Conversion function in case the first attribute of the response is an array of type structures.  
         */
        GenericEditor.prototype._convertStructureArray = function(structure) {
            var structureArray = structure.structures || structure.componentTypes;
            if (lodash.isArray(structureArray)) {
                if (structureArray.length > 1) {
                    throw "GenericEditor.prototype.init: Invalid structure, multiple structures returned";
                }
                structure = structureArray[0];
            }
            return structure;
        };

        GenericEditor.prototype.init = function() {
            /**
             * @ngdoc object
             * @name genericEditorModule.object:genericEditorApi
             * @description
             * The generic editor's api object exposing public functionality
             */
            this._setApi({
                /**
                 * @ngdoc method
                 * @name updateComponent
                 * @methodOf genericEditorModule.object:genericEditorApi
                 * @description
                 * Function that updates the content of the generic editor without having to reinitialize
                 *
                 * @param {Object} component The component to replace the current model for the generic editor
                 */
                updateContent: function(component) {
                    this.component = copy(component);
                }.bind(this),

                /**
                 * @ngdoc method
                 * @name getComponent
                 * @methodOf genericEditorModule.object:genericEditorApi
                 * @description
                 * Function returns the copy of the current model
                 */
                getContent: function() {
                    return copy(this.component);
                }.bind(this),

                /**
                 * @ngdoc method
                 * @name onContentChange
                 * @methodOf genericEditorModule.object:genericEditorApi
                 * @description
                 * Function triggered everytime the current model changes
                 */
                onContentChange: function() {},

                /**
                 * @ngdoc method
                 * @name clearMessages
                 * @methodOf genericEditorModule.object:genericEditorApi
                 * @description
                 * Function that clears all validation messages in the editor
                 */
                clearMessages: function() {
                    this.removeValidationMessages();
                }.bind(this)
            });

            var deferred = $q.defer();

            var structurePromise = this.editorStructureService ? this.editorStructureService.get({
                smarteditComponentType: this.smarteditComponentType
            }) : $q.when({
                attributes: this.structure
            });

            structurePromise.then(
                function(structure) {
                    structure = this._convertStructureArray(structure);
                    this.structure = structure;

                    this._getUriContext().then(function(uriContext) {
                        languageService.getLanguagesForSite(uriContext[CONTEXT_SITE_ID]).then(function(languages) {
                            this.languages = languages;
                            this.fields = this.fieldAdaptor(structure ? structure.attributes : []);
                            //for setting uri params into custom widgets
                            this.parameters = {
                                siteId: uriContext[CONTEXT_SITE_ID],
                                catalogId: uriContext[CONTEXT_CATALOG],
                                catalogVersion: uriContext[CONTEXT_CATALOG_VERSION]
                            };
                            this.load().then(function() {
                                systemEventService.sendAsynchEvent(GENERIC_EDITOR_LOADED_EVENT, this.id);
                                deferred.resolve();
                            }.bind(this), function() {
                                deferred.reject();
                            });
                        }.bind(this), function() {
                            $log.error("GenericEditor failed to fetch storefront languages");
                            deferred.reject();
                        });
                    }.bind(this));
                }.bind(this),
                function(e) {
                    $log.error("GenericEditor.init failed");
                    $log.error(e);
                    deferred.reject();
                });

            return deferred.promise;
        };

        return GenericEditor;

    }])

.controller('GenericEditorController', ['GenericEditor', 'isBlank', 'generateIdentifier', 'yjQuery', '$element', '$attrs', '$scope', function(GenericEditor, isBlank, generateIdentifier, yjQuery, $element, $attrs, $scope) {

        this.$onChanges = function() {
            if (this.editor) {
                this.editor._finalize();
            }

            this.editor = new GenericEditor({
                id: this.id || generateIdentifier(),
                smarteditComponentType: this.smarteditComponentType,
                smarteditComponentId: this.smarteditComponentId,
                structureApi: this.structureApi,
                structure: this.structure,
                contentApi: this.contentApi,
                updateCallback: this.updateCallback,
                content: this.content,
                uriContext: this.uriContext,
                customOnSubmit: this.customOnSubmit
            });

            this.editor.init();

            this.editor.showReset = isBlank($attrs.reset);
            this.editor.showSubmit = isBlank($attrs.submit);

            this.submitButtonText = 'se.componentform.actions.submit';
            this.cancelButtonText = 'se.componentform.actions.cancel';

            //#################################################################################################################

            if (typeof this.getApi === 'function') {
                /**
                 * @ngdoc method
                 * @name genericEditorModule.service:GenericEditor#getApi
                 * @methodOf genericEditorModule.service:GenericEditor
                 *
                 * @description
                 * Returns the generic editor's api object defining all public functionality
                 *
                 * @return {Object} api The {@link genericEditorModule.object:genericEditorApi GenericEditorApi} object
                 */
                this.getApi({
                    $api: this.editor.api
                });
            }

            var previousContent = angular.toJson(this.editor.api.getContent());

            this.$doCheck = function() {
                var newContent = angular.toJson(this.editor.api.getContent());
                if (previousContent !== newContent) {
                    previousContent = newContent;
                    this.editor.api.onContentChange();
                }
            }.bind(this);

            this.isDirty = function() {
                return this.editor ? this.editor.isDirty() : false;
            }.bind(this);

            this.reset = function() {
                return this.editor.reset($scope.componentForm);
            }.bind(this);

            this.submit = function() {
                return this.editor.submit($scope.componentForm);
            }.bind(this);

            this.getComponent = function() {
                return this.editor.getComponent();
            }.bind(this);

            this.isValid = function() {
                return this.editor.isValid($scope.componentForm);
            }.bind(this);

            this.isSubmitDisabled = function(componentForm) {
                return !(this.editor.isDirty() && componentForm.$valid);
            }.bind(this);

            /**
             *  The generic editor wraps fields in "holders" that are instantiated after init
             *  So we only want to display the warning if holders exists (init is finished)
             *  but we still have no fields (holder is empty)
             *
             * @returns {boolean} True if we should display the disclaimer message to the user that either
             * the type is blacklisted or has no editable fields (there's no structure fields in technical terms)
             */
            this.showNoEditSupportDisclaimer = function() {
                return this.editor &&
                    this.editor.holders &&
                    this.editor.holders.length === 0;
            };
        };

        this.$onDestroy = function() {
            this.editor._finalize();
        };

        //FIXME : unregister event on destroy
        this.$postLink = function() {
            // Prevent enter key from triggering form submit
            yjQuery($element.find('.no-enter-submit')[0]).bind('keypress', function(key) {
                if (key.keyCode === 13) {
                    return false;
                }
            });
        };

    }])
    /**
     * @ngdoc directive
     * @name genericEditorModule.directive:genericEditor
     * @scope
     * @restrict E
     * @element generic-editor
     *
     * @description
     * Component responsible for generating custom HTML CRUD form for any smarteditComponent type.
     *
     * The controller has a method that creates a new instance for the {@link genericEditorModule.service:GenericEditor GenericEditor}
     * and sets the scope of smarteditComponentId and smarteditComponentType to a value that has been extracted from the original DOM element in the storefront.
     *
     * @param {= String} id Id of the current generic editor.
     * @param {= String} smarteditComponentType The SmartEdit component type that is to be created, read, updated, or deleted.
     * @param {= String} smarteditComponentId The identifier of the SmartEdit component that is to be created, read, updated, or deleted.
     * @param {< String =} structureApi The data binding to a REST Structure API that fulfills the contract described in the  {@link genericEditorModule.service:GenericEditor GenericEditor} service. Only the Structure API or the local structure must be set.
     * @param {< String =} structure The data binding to a REST Structure JSON that fulfills the contract described in the {@link genericEditorModule.service:GenericEditor GenericEditor} service. Only the Structure API or the local structure must be set.
     * @param {= String} contentApi The REST API used to create, read, update, or delete content.
     * @param {= Object} content The model for the generic editor (the initial content when the component is being edited).
     * @param {< Object =} uriContext is an optional parameter and is used to pass the uri Params which can be used in making
     * api calls in custom widgets. It is an optional parameter and if not found, generic editor will find an experience in
     * sharedDataService and set this uriContext.
     * @param {= Function =} submit It exposes the inner submit function to the invoker scope. If this parameter is set, the directive will not display an inner submit button.
     * @param {= Function =} reset It exposes the inner reset function to the invoker scope. If this parameter is set, the directive will not display an inner cancel button.
     * @param {= Function =} isDirty Indicates if the the generic editor is in a pristine state (for example: has been modified).
     * @param {= Function =} isValid Indicates if all of the containing forms and controls in the generic editor are valid.
     * @param {& Function =} getApi Exposes the generic editor's api object
     * @param {= Function =} getComponent @deprecated since 6.5, use getComponent() method of getApi. Returns the current model for the generic editor.
     * @param {< Function =} updateCallback Callback called at the end of a successful submit. It is invoked with two arguments: the pristine object and the response from the server.
     * @param {= Function =} customOnSubmit It exposes the inner onSubmit function to the invoker scope. If the parameter is set, the inner onSubmit function is overridden by the custom function and the custom function must return a promise in the response format expected by the generic editor.
     */
    .component('genericEditor', {
        templateUrl: 'genericEditorTemplate.html',
        controller: 'GenericEditorController',
        controllerAs: 'ge',
        transclude: true,
        bindings: {
            id: '=',
            smarteditComponentId: '=',
            smarteditComponentType: '=?',
            structureApi: '<?',
            structure: '<?',
            contentApi: '=',
            content: '=',
            uriContext: '<?',
            submit: '=?',
            reset: '=?',
            isDirty: '=?',
            isValid: '=?',
            getApi: '&?',
            getComponent: '=?',
            updateCallback: '<?',
            customOnSubmit: '=?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('genericEditorFieldModule', ['translationServiceModule', 'ui.bootstrap', 'ui.select', 'ngSanitize', 'seGenericEditorFieldMessagesModule'])
    .directive('genericEditorField', function() {
        return {
            templateUrl: 'genericEditorFieldTemplate.html',
            restrict: 'E',
            transclude: false,
            replace: false,
            scope: {
                editor: '=',
                field: '=',
                model: '=',
                qualifier: '=',
                id: '='
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seRichTextFieldModule', ['ngSanitize', 'languageServiceModule'])
    .constant('seRichTextConfiguration', {
        toolbar: 'full',
        toolbar_full: [{
                name: 'basicstyles',
                items: ['Bold', 'Italic', 'Strike', 'Underline']
            }, {
                name: 'paragraph',
                items: ['BulletedList', 'NumberedList', 'Blockquote']
            }, {
                name: 'editing',
                items: ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
            }, {
                name: 'links',
                items: ['Link', 'Unlink', 'Anchor']
            }, {
                name: 'tools',
                items: ['SpellChecker', 'Maximize']
            },
            '/', {
                name: 'styles',
                items: ['Format', 'FontSize', 'TextColor', 'PasteText', 'PasteFromWord', 'RemoveFormat']
            }, {
                name: 'insert',
                items: ['Image', 'Table', 'SpecialChar']
            }, {
                name: 'forms',
                items: ['Outdent', 'Indent']
            }, {
                name: 'clipboard',
                items: ['Undo', 'Redo']
            }, {
                name: 'document',
                items: ['PageBreak', 'Source']
            }
        ],
        disableNativeSpellChecker: false,
        height: '100px',
        width: '100%',
        autoParagraph: false,
        enterMode: CKEDITOR.ENTER_BR,
        shiftEnterMode: CKEDITOR.ENTER_BR,
        basicEntities: false,
        allowedContent: true,
        fillEmptyBlocks: false,
        contentsCss: 'static-resources/dist/smartedit/css/outer-styling.css'
    })
    .service('seRichTextLoaderService', ['$q', '$interval', function($q, $interval) {
        var loadDeferred = $q.defer();

        var checkLoadedInterval = $interval(function() {
            if (CKEDITOR.status === 'loaded') {
                loadDeferred.resolve();
                $interval.cancel(checkLoadedInterval);
            }
        }, 100);

        return {
            load: function() {
                var deferred = $q.defer();
                loadDeferred.promise.then(function() {
                    deferred.resolve();
                });
                return deferred.promise;
            }
        };
    }])
    .service('genericEditorSanitizationService', ['$sanitize', function($sanitize) {
        return {
            isSanitized: function(content) {

                var sanitizedContent = $sanitize(content);
                sanitizedContent = sanitizedContent.replace(/&#10;/g, '\n').replace(/&#160;/g, "\u00a0").replace(/<br>/g, '<br />');
                content = content.replace(/&#10;/g, '\n').replace(/&#160;/g, "\u00a0").replace(/<br>/g, '<br />');
                var sanitizedContentMatchesContent = sanitizedContent === content;
                return sanitizedContentMatchesContent;
            }
        };
    }])
    .controller('seRichTextFieldController', function() {})
    .directive('seRichTextField', ['seRichTextLoaderService', 'seRichTextConfiguration', 'genericEditorSanitizationService', 'seRichTextFieldLocalizationService', function(seRichTextLoaderService, seRichTextConfiguration, genericEditorSanitizationService, seRichTextFieldLocalizationService) {
        return {
            restrict: 'E',
            templateUrl: 'seRichTextFieldTemplate.html',
            scope: {},
            controller: 'seRichTextFieldController',
            controllerAs: 'ctrl',
            bindToController: {
                field: '=',
                qualifier: '=',
                model: '=',
                editor: '='

            },
            link: function($scope, $element) {
                seRichTextLoaderService.load().then(function() {
                    var textAreaElement = $element.find('textarea')[0];
                    var editorInstance = CKEDITOR.replace(textAreaElement, seRichTextConfiguration);

                    seRichTextFieldLocalizationService.localizeCKEditor();

                    $element.bind('$destroy', function() {
                        if (editorInstance && CKEDITOR.instances[editorInstance.name]) {
                            CKEDITOR.instances[editorInstance.name].destroy();
                        }
                    });

                    $scope.ctrl.onChange = function() {
                        $scope.$apply(function() {
                            $scope.ctrl.model[$scope.ctrl.qualifier] = editorInstance.getData();
                            $scope.ctrl.reassignUserCheck();

                        });
                    };

                    $scope.ctrl.onMode = function() {
                        if (this.mode === 'source') {
                            var editable = editorInstance.editable();
                            editable.attachListener(editable, 'input', function() {
                                editorInstance.fire('change');
                            });
                        }
                    };

                    $scope.ctrl.onInstanceReady = function(ev) {
                        ev.editor.dataProcessor.writer.setRules('br', {
                            indent: false,
                            breakBeforeOpen: false,
                            breakAfterOpen: false,
                            breakBeforeClose: false,
                            breakAfterClose: false
                        });
                    };

                    $scope.ctrl.requiresUserCheck = function() {
                        var requiresUserCheck = false;
                        for (var qualifier in this.field.requiresUserCheck) {
                            requiresUserCheck = requiresUserCheck || this.field.requiresUserCheck[qualifier];
                        }
                        return requiresUserCheck;
                    };


                    $scope.ctrl.reassignUserCheck = function() {
                        if ($scope.ctrl.model && $scope.ctrl.qualifier && $scope.ctrl.model[$scope.ctrl.qualifier]) {
                            var sanitizedContentMatchesContent = genericEditorSanitizationService.isSanitized($scope.ctrl.model[$scope.ctrl.qualifier]);
                            $scope.ctrl.field.requiresUserCheck = $scope.ctrl.field.requiresUserCheck || {};
                            $scope.ctrl.field.requiresUserCheck[$scope.ctrl.qualifier] = !sanitizedContentMatchesContent;
                        } else {
                            $scope.ctrl.field.requiresUserCheck = $scope.ctrl.field.requiresUserCheck || {};
                            $scope.ctrl.field.requiresUserCheck[$scope.ctrl.qualifier] = false;
                        }
                    };

                    editorInstance.on('change', $scope.ctrl.onChange);
                    editorInstance.on('mode', $scope.ctrl.onMode);
                    CKEDITOR.on('instanceReady', $scope.ctrl.onInstanceReady);

                });
            }
        };
    }])
    .constant('resolvedLocaleToCKEDITORLocaleMap', {
        'in': 'id',
        'es_CO': 'es'
    })
    .service('seRichTextFieldLocalizationService', ['languageService', 'resolvedLocaleToCKEDITORLocaleMap', function(languageService, resolvedLocaleToCKEDITORLocaleMap) {

        var convertResolvedToCKEditorLocale = function(resolvedLocale) {
            var conversion = resolvedLocaleToCKEDITORLocaleMap[resolvedLocale];
            if (conversion) {
                return conversion;
            } else {
                return resolvedLocale;
            }
        };

        this.localizeCKEditor = function() {
            languageService.getResolveLocale().then(function(locale) {
                CKEDITOR.config.language = convertResolvedToCKEditorLocale(locale);
            });
        };

    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seValidationErrorParserModule
 * @description
 * This module provides the validationErrorsParser service, which is used to parse validation errors for parameters
 * such as language and format, which are sent as part of the message itself.
 */
angular.module('seValidationErrorParserModule', ['seValidationMessageParserModule'])

/**
 * @deprecated since 6.5
 * @ngdoc service
 * @name seValidationErrorParserModule.seValidationErrorParser
 * @description
 * This service provides the functionality to parse validation errors received from the backend.
 */
.service('seValidationErrorParser', ['seValidationMessageParser', function(seValidationMessageParser) {

    /**
     * @ngdoc method
     * @name seValidationErrorParserModule.seValidationErrorParser.parse
     * @methodOf seValidationErrorParserModule.seValidationErrorParser
     * @description
     * Parses extra details, such as language and format, from a validation error message. These details are also
     * stripped out of the final message. This function expects the message to be in the following format:
     *
     * <pre>
     * var message = "Some validation error occurred. Language: [en]. Format: [widescreen]. SomeKey: [SomeVal]."
     * </pre>
     *
     * The resulting error object is as follows:
     * <pre>
     * {
     *     message: "Some validation error occurred."
     *     language: "en",
     *     format: "widescreen",
     *     somekey: "someval"
     * }
     * </pre>
     */
    this.parse = function(message) {
        return seValidationMessageParser.parse(message);
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seValidationMessageParserModule
 * @description
 * This module provides the seValidationMessageParser service, which is used to parse validation messages (errors, warnings)
 * for parameters such as language and format, which are sent as part of the message itself.
 */
angular.module('seValidationMessageParserModule', [])
    /**
     * @ngdoc service
     * @name seValidationMessageParserModule.seValidationMessageParser
     * @description
     * This service provides the functionality to parse validation messages (errors, warnings) received from the backend.
     */
    .service('seValidationMessageParser', function() {

        /**
         * @ngdoc method
         * @name seValidationMessageParserModule.seValidationMessageParser.parse
         * @methodOf seValidationMessageParserModule.seValidationMessageParser
         * @description
         * Parses extra details, such as language and format, from a validation message (error, warning). These details are also
         * stripped out of the final message. This function expects the message to be in the following format:
         *
         * <pre>
         * var message = "Some validation message occurred. Language: [en]. Format: [widescreen]. SomeKey: [SomeVal]."
         * </pre>
         *
         * The resulting message object is as follows:
         * <pre>
         * {
         *     message: "Some validation message occurred."
         *     language: "en",
         *     format: "widescreen",
         *     somekey: "someval"
         * }
         * </pre>
         */
        this.parse = function(message) {
            var expression = new RegExp('[a-zA-Z]+: (\[|\{)([a-zA-Z0-9]+)(\]|\})\.?', 'g');
            var matches = message.match(expression) || [];
            return matches.reduce(function(messages, match) {
                messages.message = messages.message.replace(match, '').trim();
                var key = match.split(':')[0].trim().toLowerCase();
                var value = match.split(':')[1].match(/[a-zA-Z0-9]+/g)[0];

                messages[key] = value;
                return messages;
            }, {
                message: message
            });
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name seGenericEditorFieldMessagesModule
 *
 * @description
 * This module provides the seGenericEditorFieldMessages component, which is used to show validation messages like errors or warnings.
 */
angular.module('seGenericEditorFieldMessagesModule', ['seConstantsModule'])
    .controller('seGenericEditorFieldMessagesController', ['VALIDATION_MESSAGE_TYPES', function(VALIDATION_MESSAGE_TYPES) {

        var previousMessages = null;

        this.getFilteredMessagesByType = function(field, messageType) {
            return (field.messages || []).filter(function(validationMessage) {
                return validationMessage.marker === this.qualifier &&
                    !validationMessage.format && validationMessage.type === messageType;
            }.bind(this)).map(function(validationMessage) {
                return validationMessage.message;
            });
        };

        this.$doCheck = function() {
            if (this.field) {
                var currentMessages = angular.toJson(this.field.messages);
                if (previousMessages !== currentMessages) {
                    previousMessages = currentMessages;
                    var field = this.field || {};
                    this.errors = this.getFilteredMessagesByType(field, VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR);
                    this.warnings = this.getFilteredMessagesByType(field, VALIDATION_MESSAGE_TYPES.WARNING);
                }
            }
        };
    }])
    /**
     * @ngdoc directive
     * @name seGenericEditorFieldMessagesModule.component:seGenericEditorFieldMessages
     * @element se-generic-editor-field-messages
     *
     * @description
     * Component responsible for displaying validation messages like errors or warnings
     * @param {< Object} field The field object that contains array of messages.
     * @param {< String} qualifier For a non-localized field, it is the actual field.qualifier. For a localized field, it is the ISO code of the language.
     */
    .component('seGenericEditorFieldMessages', {
        templateUrl: 'seGenericEditorFieldMessagesTemplate.html',
        controller: 'seGenericEditorFieldMessagesController',
        controllerAs: 'ctrl',
        bindings: {
            field: '<',
            qualifier: '<'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('yHelpModule', ['yPopoverModule'])
    /**
     * @ngdoc directive
     * @name yHelpModule.component:yHelp
     * @scope
     * @restrict E
     * @element y-help
     * 
     * @description
     * This component will generate a help button that will show a customizable popover on top of it when hovering.
     * It relies on the {@link yPopoverModule.directive:yPopover yPopover} directive.
     * @param {String} template the HTML body to be used in the popover body, it will automatically be trusted by the directive. Optional but exactly one of either template or templateUrl must be defined.
     * @param {String} templateUrl the location of the HTML template to be used in the popover body. Optional but exactly one of either template or templateUrl must be defined.
     * @param {String} title the title to be used in the popover title section. Optional.
     */
    .component('yHelp', {
        templateUrl: 'yHelpTemplate.html',
        controller: function() {
            this.$onInit = function() {
                this.placement = 'top';
                this.trigger = 'hover';
            };

            this.$onChanges = function(changesObj) {
                if (this.template && changesObj.template) {
                    this.template = "<div class='y-help-template-text'>" + this.template + "</div>";
                }
            };
        },
        controllerAs: 'yHelp',
        bindings: {
            title: '<?',
            template: '<?',
            templateUrl: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('i18nInterceptorModule', ['interceptorHelperModule', 'resourceLocationsModule', 'languageServiceModule']) //loose dependency on loadConfigModule since it exists in smartEditContainer but not smartEdit
    .constant('temporaryKeys', {})
    /**
     * @ngdoc object
     * @name i18nInterceptorModule.object:I18NAPIROOT
     *
     * @description
     * The I18NAPIroot is a hard-coded URI that is used to initialize the {@link translationServiceModule}.
     * The {@link i18nInterceptorModule.service:i18nInterceptor#methods_request i18nInterceptor.request} intercepts the URI and replaces it with the {@link resourceLocationsModule.object:I18N_RESOURCE_URI I18N_RESOURCE_URI}.
     */
    .constant('I18NAPIROOT', 'i18nAPIRoot')
    /**
     * @ngdoc object
     * @name i18nInterceptorModule.object:UNDEFINED_LOCALE
     *
     * @description
     * The undefined locale set as the preferred language of the {@link translationServiceModule} so that
     * an {@link i18nInterceptorModule.service:i18nInterceptor#methods_request i18nInterceptor.request} can intercept it and replace it with the browser locale.
     */
    .constant('UNDEFINED_LOCALE', 'UNDEFINED')
    /**
     * @ngdoc service
     * @name i18nInterceptorModule.service:i18nInterceptor
     *
     * @description
     * A HTTP request interceptor that intercepts all i18n calls and handles them as required in the {@link i18nInterceptorModule.service:i18nInterceptor#methods_request i18nInterceptor.request} method.
     *
     * The interceptors are service factories that are registered with the $httpProvider by adding them to the $httpProvider.interceptors array.
     * The factory is called and injected with dependencies and returns the interceptor object, which contains the interceptor methods.
     */
    .factory('i18nInterceptor', ['$injector', 'interceptorHelper', 'I18N_RESOURCE_URI', 'I18NAPIROOT', 'UNDEFINED_LOCALE', 'temporaryKeys', function($injector, interceptorHelper, I18N_RESOURCE_URI, I18NAPIROOT, UNDEFINED_LOCALE, temporaryKeys) {

        return {

            /**
             * @ngdoc method
             * @name i18nInterceptorModule.service:i18nInterceptor#request
             * @methodOf i18nInterceptorModule.service:i18nInterceptor
             *
             * @description
             * Interceptor method that is invoked with a HTTP configuration object.
             *  It intercepts all requests that are i18n calls, that is, it intercepts all requests that have an {@link i18nInterceptorModule.object:I18NAPIROOT I18NAPIROOT} in their calls.
             *  It replaces the URL provided in a request with the URL provided by the {@link resourceLocationsModule.object:I18N_RESOURCE_URI I18N_RESOURCE_URI}.
             *  If a locale has not already been defined, the interceptor method appends the locale retrieved using the {@link languageServiceModule.service:languageService#methods_getResolveLocale languageService.getResolveLocale}.


             * @param {Object} config The HTTP configuration information that contains the configuration information.
             *
             * @returns {Promise} Returns a {@link https://docs.angularjs.org/api/ng/service/$q promise} of the passed configuration object.
             */
            request: function(config) {
                return interceptorHelper.handleRequest(config, function() {
                    /*
                     * always intercept i18n calls so as to replace URI by one from configuration (cannot be done at config time of $translateProvider)
                     * regex matching /i18nAPIRoot/<my_locale>
                     */
                    var regex = new RegExp(I18NAPIROOT + "\/([a-zA-Z_]+)$");
                    if (regex.test(config.url)) {
                        return $injector.get('languageService').getResolveLocale().then(function(isoCode) {
                            config.url = [I18N_RESOURCE_URI, isoCode].join('/');
                            return config;
                        });
                    } else {
                        return config;
                    }
                });
            },
            response: function(response) {
                return interceptorHelper.handleResponse(response, function() {
                    /*
                     * if it intercepts a call to I18N_RESOURCE_URI the response body will be adapted to
                     * read the value from response.data.value instead.
                     */
                    var regex = new RegExp(I18N_RESOURCE_URI + "/([a-zA-Z_]+)$");
                    if (response.config.url) {
                        var url = response.config.url;
                        if (regex.test(url) && response.data.value) {
                            response.data = response.data.value;
                        }
                        if (regex.test(url)) {
                            /*
                             * MISF-445 : temporary adding of i18n keys that did nto make it to 6.0 feature freeze
                             */
                            for (var key in temporaryKeys) {
                                if (!response.data[key]) {
                                    response.data[key] = temporaryKeys[key];
                                }
                            }
                        }

                    }
                    $injector.get('languageService').setInitialized(true);
                    return response;
                });
            }
        };
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('i18nInterceptor');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('includeReplaceModule', [])
    .directive('includeReplace', function() {
        return {
            require: 'ngInclude',
            restrict: 'A',
            /* optional */
            link: function(scope, el) {
                el.replaceWith(el.children());
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yInfiniteScrollingModule
 * @description
 * <h1>This module holds the base web component to perform infinite scrolling from paged backend</h1>
 */
angular.module('yInfiniteScrollingModule', ['infinite-scroll', 'functionsModule'])
    //value used by third party infinite-scroll and used here when typing new value for mask
    /**
     * @ngdoc object
     * @name yInfiniteScrollingModule.object:THROTTLE_MILLISECONDS
     *
     * @description
     * Configures the {@link yInfiniteScrollingModule.directive:yInfiniteScrolling yInfiniteScrolling} directive to throttle the page fetching with the value provided in milliseconds.
     */
    .value('THROTTLE_MILLISECONDS', 250)
    .controller('yInfiniteScrollingController', ['yjQuery', 'throttle', 'THROTTLE_MILLISECONDS', 'generateIdentifier', '$timeout', function(yjQuery, throttle, THROTTLE_MILLISECONDS, generateIdentifier, $timeout) {

        this.initiated = false;
        this.containerId = generateIdentifier();
        this.containerIdSelector = "#" + this.containerId;

        this.context = this.context || this;

        this.reset = function() {
            this.distance = this.distance || 0;
            this.context.items = [];
            this.currentPage = -1;
            this.pagingDisabled = false;
            //necessary so that infinite-scroll directive can rely on resolved container id
            if (!this.container) {
                $timeout(function() {
                    this.container = yjQuery(this.containerIdSelector).get(0);
                    this.initiated = true;
                }.bind(this), 0);
            } else {
                this.container.scrollTop = 0;
            }
        };

        this.nextPage = function() {
            if (this.pagingDisabled) {
                return;
            }
            this.pagingDisabled = true;
            this.currentPage++;
            this.mask = this.mask || "";
            this.fetchPage(this.mask, this.pageSize, this.currentPage).then(function(page) {
                Array.prototype.push.apply(this.context.items, page.results);
                this.pagingDisabled = page.results.length === 0 || (page.pagination && this.context.items.length === page.pagination.totalCount);
            }.bind(this));
        }.bind(this);

        this.$onChanges = throttle(function() {
            var wasInitiated = this.initiated;
            this.reset();
            if (wasInitiated) {
                this.nextPage();
            }
        }.bind(this), THROTTLE_MILLISECONDS);

    }])
    /**
     * @ngdoc object
     * @name Page.object:Page
     * @description
     * An object representing the backend response to a paged query
     */
    /**
     * @ngdoc property
     * @name results
     * @propertyOf Page.object:Page
     * @description
     * The array containing the elements pertaining to the requested page, its size will not exceed the requested page size.
     **/
    /**
     * @ngdoc property
     * @name pagination
     * @propertyOf Page.object:Page
     * @description
     * The returned {@link Page.object:Pagination Pagination} object
     */

/**
 * @ngdoc object
 * @name Page.object:Pagination
 * @description
 * An object representing the returned pagination information from backend
 */

/**
 * @ngdoc property
 * @name totalCount
 * @propertyOf Page.object:Pagination
 * @description
 * the total of elements matching the given mask once all pages are returned
 **/

/**
 * @ngdoc directive
 * @name yInfiniteScrollingModule.directive:yInfiniteScrolling
 * @scope
 * @restrict E
 *
 * @description
 * A component that you can use to implement infinite scrolling for an expanding content (typically with a ng-repeat) nested in it.
 * It is meant to handle paginated requests from a backend when data is expected to be large.
 * Since the expanding content is a <b>transcluded</b> element, we must specify the context to which the items will be attached:
 * If context is myContext, each pagination will push its new items to myContext.items.
 * @param {String} pageSize The maximum size of each page requested from the backend.
 * @param {String} mask A string value sent to the server upon fetching a page to further restrict the search, it is sent as query string "mask".
 * <br>The directive listens for change to mask and will reset the scroll and re-fetch data.
 * <br/>It it left to the implementers to decide what it filters on
 * @param {String} distance A number representing how close the bottom of the element must be to the bottom of the container before the expression specified by fetchPage function is triggered. Measured in multiples of the container height; for example, if the container is 1000 pixels tall and distance is set to 2, the infinite scroll expression will be evaluated when the bottom of the element is within 2000 pixels of the bottom of the container. Defaults to 0 (e.g. the expression will be evaluated when the bottom of the element crosses the bottom of the container).
 * @param {Object} context The container object to which the items of the fetched {@link Page.object:Page Page} will be added
 * @param {Function} fetchPage function to fetch the next page when the bottom of the element approaches the bottom of the container.
 *        fetchPage will be invoked with 3 arguments : <b>mask, pageSize, currentPage</b>. The currentPage is determined by the scrolling and starts with 0. The function must return a page of type {@link Page.object:Page Page}.
 * @param {String} dropDownContainerClass An optional CSS class to be added to the container of the dropdown. It would typically be used to override the default height. <b>The resolved CSS must set a height (or max-height) and overflow-y:scroll.</b>
 * @param {String} dropDownClass An optional CSS class to be added to the dropdown. <b>Neither height nor overflow should be set on the dropdown, it must be free to fill up the space and reach the container size. Failure to do so will cause the directive to call nextPage as many times as the number of available pages on the server.</b>
 */
.component(
    'yInfiniteScrolling', {
        templateUrl: 'yInfiniteScrollingTemplate.html',
        transclude: true,
        replace: true,
        controller: 'yInfiniteScrollingController',
        controllerAs: 'scroll',
        bindings: {
            pageSize: '<',
            mask: '<?',
            fetchPage: '<',
            distance: '<?',
            context: '<?',
            dropDownContainerClass: '@?',
            dropDownClass: '@?'
        }
    }
);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('interceptorHelperModule', [])
    /**
     * @ngdoc service
     * @name interceptorHelperModule.service:interceptorHelper
     *
     * @description
     * Helper service used to handle request and response in interceptors
     */
    .service('interceptorHelper', ['$q', '$log', function($q, $log) {

        return {

            _isEligibleForInterceptors: function(config) {
                return config && config.url && !/.+\.html$/.test(config.url);
            },

            _handle: function(chain, config, callback, isError) {
                try {
                    if (this._isEligibleForInterceptors(config)) {
                        return $q.when(callback());
                    } else {
                        if (isError) {
                            return $q.reject(chain);
                        } else {
                            return chain;
                        }
                    }
                } catch (e) {
                    $log.error("caught error in one of the interceptors", e);
                    if (isError) {
                        return $q.reject(chain);
                    } else {
                        return chain;
                    }
                }
            },

            /** 
             * @ngdoc method
             * @name interceptorHelperModule.service:interceptorHelper#methodsOf_handleRequest
             * @methodOf interceptorHelperModule.service:interceptorHelper
             *
             * @description
             * Handles body of an interceptor request
             * @param {object} config the request's config to be handled by an interceptor method
             * @param {callback} callback the callback function to handle the object. callback will either return a promise or the initial object.
             * @return {object} the config or a promise resolving or rejecting with the config
             */
            handleRequest: function(config, callback) {
                return this._handle(config, config, callback, false);
            },

            /** 
             * @ngdoc method
             * @name interceptorHelperModule.service:interceptorHelper#methodsOf_handleResponse
             * @methodOf interceptorHelperModule.service:interceptorHelper
             *
             * @description
             * Handles body of an interceptor response success
             * @param {object} response the response to be handled by an interceptor method
             * @param {callback} callback the callback function to handle the response. callback will either return a promise or the initial object.
             * @return {object} the response or a promise resolving or rejecting with the response
             */
            handleResponse: function(response, callback) {
                return this._handle(response, response.config, callback, false);
            },
            /** 
             * @ngdoc method
             * @name interceptorHelperModule.service:interceptorHelper#methodsOf_handleResponseError
             * @methodOf interceptorHelperModule.service:interceptorHelper
             *
             * @description
             * Handles body of an interceptor response error
             * @param {object} response the response to be handled by an interceptor method
             * @param {callback} callback the callback function to handle the response in error. callback will either return a promise or the initial object.
             * @return {object} the response or a promise resolving or rejecting with the response
             */
            handleResponseError: function(response, callback) {
                return this._handle(response, response.config, callback, true);
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yjqueryModule
 * @description
 * This module manages the use of the jQuery library in SmartEdit.
 * It enables smartEdit to work with a "noConflict" version of jQuery in a storefront that may contain another version
 */
angular.module('yjqueryModule', [])
    /**
     * As a configuration step for this module, add the getCssPath method to jquery selectors. This method will return
     * the CSS path of the wrapped JQuery element.
     */
    .run(['yjQuery', function(yjQuery) {
        yjQuery.fn.extend({        
            getCssPath: function() {            
                var path, node = this;            
                while (node.length) {                
                    var realNode = node[0],
                                            name = realNode.className;                
                    if (realNode.tagName === 'BODY') {                    
                        break;                
                    }                
                    node = node.parent();                
                    path = name + (path ? '>' + path : '');            
                }            
                return path;        
            }    
        });
    }])
    /**
     * @ngdoc object
     * @name yjqueryModule.yjQuery
     * @description
     * 
     * Expose a jQuery wrapping factory all the while preserving potentially pre-existing jQuery in storefront and smartEditContainer
     */
    /* forbiddenNameSpaces:false */
    .factory('yjQuery', function() {

        var namespace = "smarteditJQuery";

        if (!window[namespace]) {
            if (window.$ && window.$.noConflict) {
                window[namespace] = window.$.noConflict();
            } else {
                window[namespace] = window.$;
            }
        }
        return window[namespace];
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name languageSelectorModule
 * @description
 *
 * The language selector module contains a directive which allow the user to select a language.
 *
 * Use the {@link languageServiceModule.service:languageService languageService}
 * to call backend API in order to get the list of supported languages
 */
angular
    .module('languageSelectorModule', ['languageServiceModule', 'ui.select', 'ngSanitize', 'eventServiceModule'])
    .controller('languageSelectorController', ['systemEventService', '$scope', 'languageService', 'SWITCH_LANGUAGE_EVENT', function(systemEventService, $scope, languageService, SWITCH_LANGUAGE_EVENT) {


        this.setSelectedLanguage = function(item) {
            languageService.setSelectedToolingLanguage(item);
        };

        languageService.getToolingLanguages().then(function(data) {
            this.languages = data;
            languageService.getResolveLocale().then(function(isoCode) {
                this.selectedLanguage = this.languages.find(function(obj) {
                    return obj.isoCode === isoCode;
                });
            }.bind(this));
        }.bind(this));

        this.eventHandler = function() {
            return languageService.getResolveLocale().then(function(isoCode) {
                this.selectedLanguage = this.languages.find(function(obj) {
                    return obj.isoCode === isoCode;
                });
            }.bind(this));
        };

        var boundEventHandler = this.eventHandler.bind(this);
        systemEventService.registerEventHandler(SWITCH_LANGUAGE_EVENT, boundEventHandler);

        $scope.$on('$destroy', function() {
            systemEventService.unRegisterEventHandler(SWITCH_LANGUAGE_EVENT, boundEventHandler);
        });
    }])
    /**
     * @ngdoc directive
     * @name languageSelectorModule.directive:languageSelector
     * @scope
     * @restrict E
     * @element ANY
     * @description
     * Language selector provides a drop-down list which contains a list of supported languages.
     * It is used to select a language and translate the system accordingly.
     */
    .directive(
        'languageSelector',
        function() {
            return {
                templateUrl: 'languageSelectorTemplate.html',
                restrict: 'E',
                transclude: true,
                scope: {},
                bindToController: {},
                controller: 'languageSelectorController',
                controllerAs: 'ctrl'
            };
        });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name languageServiceModule
 * @description
 * # The languageServiceModule
 *
 * The Language Service module provides a service that fetches all languages that are supported for specified site.
 */
angular.module('languageServiceModule', ['restServiceFactoryModule', 'resourceLocationsModule', 'storageServiceModule', 'translationServiceModule', 'gatewayFactoryModule', 'eventServiceModule', 'operationContextServiceModule'])
    /**
     * @ngdoc object
     * @name languageServiceModule.SELECTED_LANGUAGE
     *
     * @description
     * A constant that is used as key to store the selected language in the storageService
     */
    .constant('SELECTED_LANGUAGE', 'SELECTED_LANGUAGE')
    /**
     * @ngdoc object
     * @name languageServiceModule.SWITCH_LANGUAGE_EVENT
     *
     * @description
     * A constant that is used as key to publish and receive events when a language is changed.
     */
    .constant('SWITCH_LANGUAGE_EVENT', 'SWITCH_LANGUAGE_EVENT')
    /**
     * @ngdoc service
     * @name languageServiceModule.service:languageService
     *
     * @description
     * The Language Service fetches all languages for a specified site using REST service calls to the cmswebservices languages API.
     */
    .factory('languageServiceGateway', ['gatewayFactory', function(gatewayFactory) {
        return gatewayFactory.createGateway("languageSwitch");
    }])
    .factory('languageService', ['restServiceFactory', 'LANGUAGE_RESOURCE_URI', '$q', 'I18N_LANGUAGES_RESOURCE_URI', 'SELECTED_LANGUAGE', 'storageService', 'languageServiceGateway', '$translate', 'SWITCH_LANGUAGE_EVENT', 'systemEventService', 'operationContextService', 'OPERATION_CONTEXT', function(restServiceFactory, LANGUAGE_RESOURCE_URI, $q, I18N_LANGUAGES_RESOURCE_URI, SELECTED_LANGUAGE, storageService, languageServiceGateway, $translate, SWITCH_LANGUAGE_EVENT, systemEventService, operationContextService, OPERATION_CONTEXT) {

        var cache = {};
        var languageRestService = restServiceFactory.get(LANGUAGE_RESOURCE_URI);
        operationContextService.register(LANGUAGE_RESOURCE_URI, OPERATION_CONTEXT.TOOLING);

        var i18nLanguageRestService = restServiceFactory.get(I18N_LANGUAGES_RESOURCE_URI);

        var _getBrowserLocale = function() {
            var locale = (navigator.language || navigator.userLanguage).split("-");

            if (locale.length === 1) {
                locale = locale[0];
            } else {
                locale = locale[0] + "_" + locale[1].toUpperCase();
            }
            return locale;
        };

        var _getDefaultLanguage = function() {
            var browserLocale = _getBrowserLocale();
            return storageService.getValueFromCookie(SELECTED_LANGUAGE, false).then(
                function(selectedLanguage) {
                    if (selectedLanguage) {
                        return selectedLanguage.isoCode;
                    } else {
                        return browserLocale;
                    }
                },
                function() {
                    return browserLocale;
                }
            );
        };

        var initDeferred = $q.defer();

        return {
            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getBrowserLanguageIsoCode
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Uses the browser's current locale to determine the selected language ISO code.
             *
             * @returns {String} The language ISO code of the browser's currently selected locale.
             */
            getBrowserLanguageIsoCode: function() {
                return (navigator.language || navigator.userLanguage).split("-")[0];
            },

            setInitialized: function(_initialized) {
                if (_initialized === true) {
                    initDeferred.resolve();
                } else {
                    initDeferred.reject();
                }
            },
            isInitialized: function() {
                return initDeferred.promise;
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getBrowserLocale
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * determines the browser locale in the format en_US
             *
             * @returns {string} the browser locale
             */
            getBrowserLocale: function() {
                return _getBrowserLocale();
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getResolveLocale
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Resolve the user preference tooling locale. It determines in the
             * following order:
             *
             * 1. Check if the user has previously selected the language
             * 2. Check if the user browser locale is supported in the system
             * @returns {string} the locale
             */
            getResolveLocale: function() {
                return $q.when(_getDefaultLanguage());
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getResolveLocaleIsoCode
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Resolve the user preference tooling locale ISO code. i.e.: If the selected tooling language is 'en_US',
             * the resolved value will be 'en'.
             *
             * @returns {Promise} A promise that resolves to the isocode of the tooling language.
             */
            getResolveLocaleIsoCode: function() {
                return this.getResolveLocale().then(function(resolveLocale) {
                    return resolveLocale.split('_')[0];
                });
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getLanguagesForSite
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Fetches a list of language descriptors for the specified storefront site UID. The object containing the list of sites is fetched
             * using REST calls to the cmswebservices languages API.
             * @param {string} siteUID the site unique identifier.
             * @returns {Array} An array of language descriptors. Each descriptor provides the following language
             * properties: isocode, nativeName, name, active, and required.
             * format:
             * <pre>
             * [{
             *   language: 'en',
             *   required: true
             *  }, {
             *   language: 'fr',
             *  }]
             * </pre>
             */
            getLanguagesForSite: function(siteUID) {
                return cache[siteUID] ? $q.when(cache[siteUID]) : languageRestService.get({
                    siteUID: siteUID
                }).then(function(languagesListDTO) {
                    cache[siteUID] = languagesListDTO.languages;
                    return cache[siteUID];
                });
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#getToolingLanguages
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Retrieves a list of language descriptors using REST calls to the smarteditwebservices i18n API.
             *
             * @returns {Array} An array of language descriptors. Each descriptor provides the following language
             * properties: isocode, name
             * format:
             * <pre>
             * [{
             *   isoCode: 'en',
             *   name: 'English'
             *  }, {
             *   isoCode: 'fr',
             *   name: 'French'
             *  }]
             * </pre>
             */

            getToolingLanguages: function() {
                var deferred = $q.defer();

                i18nLanguageRestService.get().then(
                    function(response) {
                        deferred.resolve(response.languages);
                    },
                    function() {
                        deferred.reject();
                    }
                );

                return deferred.promise;
            },

            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#setSelectedLanguage
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Set the user preference language in the storage service
             *
             * @param {object} the language object to be saved. the object contains the following properties:isoCode and name.
             * <pre>
             * {
             * isoCode:'fr',
             * name: 'French'
             * }
             * </pre>
             */
            setSelectedToolingLanguage: function(language) {
                storageService.putValueInCookie(SELECTED_LANGUAGE, language, false);
                $translate.use(language.isoCode);
                languageServiceGateway.publish(SWITCH_LANGUAGE_EVENT, {
                    isoCode: language.isoCode
                });
                systemEventService.sendEvent(SWITCH_LANGUAGE_EVENT);
            },
            /**
             * @ngdoc method
             * @name languageServiceModule.service:languageService#registerSwitchLanguage
             * @methodOf languageServiceModule.service:languageService
             *
             * @description
             * Register a callback function to the gateway in order to switch the tooling language
             */
            registerSwitchLanguage: function() {
                languageServiceGateway.subscribe(SWITCH_LANGUAGE_EVENT, function(eventId, data) {
                    return $translate.use(data.isoCode);
                });
            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yEditableListModule
 * @description
 *
 * The yEditableList module contains a component which allows displaying a list of elements. The items in 
 * that list can be added, removed, and re-ordered.
 * 
 */
angular.module("yEditableListModule", ['treeModule', 'yLoDashModule'])
    .controller('YEditableListController', ['$q', function($q) {

        this.$onInit = function() {
            this._enableDragAndDrop = function() {
                this.dragOptions.allowDropCallback = function(event) {
                    // Just allow dropping elements of the same list.
                    return (event.sourceNode.parentUid === this.rootId);
                }.bind(this);

                this.dragOptions.onDropCallback = function() {
                    this.actions.performUpdate();
                }.bind(this);

            };

            var dropdownItems = [{
                key: 'se.ydropdownmenu.remove',
                callback: function(handle) {
                    this.actions.removeItem(handle);
                }.bind(this)
            }, {
                key: 'se.ydropdownmenu.move.up',
                condition: function(handle) {
                    return this.actions.isMoveUpAllowed(handle);
                }.bind(this),
                callback: function(handle) {
                    this.actions.moveUp(handle);
                }.bind(this)
            }, {
                key: 'se.ydropdownmenu.move.down',
                condition: function(handle) {
                    return this.actions.isMoveDownAllowed(handle);
                }.bind(this),
                callback: function(handle) {
                    this.actions.moveDown(handle);
                }.bind(this)
            }];

            this.actions = {

                fetchData: function(treeService, nodeData) {
                    this.items.map(function(item) {
                        if (item.id && !item.uid) {
                            item.uid = item.id;
                        }

                        item.parentUid = this.rootId;
                    }.bind(this));
                    nodeData.nodes = this.items;

                    return $q.when(nodeData);
                }.bind(this),
                getDropdownItems: function() {
                    return dropdownItems;
                },
                removeItem: function(treeService, handle) {
                    var nodeData = handle.$modelValue;
                    var pos = this.root.nodes.indexOf(nodeData);
                    this.root.nodes.splice(pos, 1);

                    this.performUpdate(parent, handle);
                },
                moveUp: function(treeService, handle) {
                    var nodeData = handle.$modelValue;
                    var pos = this.root.nodes.indexOf(nodeData);
                    var upperEntry = this.root.nodes[pos - 1];
                    this.root.nodes.splice(pos - 1, 2, nodeData, upperEntry);

                    this.performUpdate(parent, handle);
                },
                moveDown: function(treeService, handle) {
                    var nodeData = handle.$modelValue;
                    var pos = this.root.nodes.indexOf(nodeData);
                    var lowerEntry = this.root.nodes[pos + 1];
                    this.root.nodes.splice(pos, 2, lowerEntry, nodeData);

                    this.performUpdate(parent, handle);
                },

                isMoveUpAllowed: function(treeService, handle) {
                    var nodeData = handle.$modelValue;
                    return this.root.nodes.indexOf(nodeData) > 0;
                },

                isMoveDownAllowed: function(treeService, handle) {
                    var nodeData = handle.$modelValue;
                    var entriesArrayLength = this.root.nodes.length;

                    return this.root.nodes.indexOf(nodeData) !== (entriesArrayLength - 1);
                },

                performUpdate: function() {
                    if (this.onChange) {
                        this.onChange();
                    }
                }.bind(this),
                refreshList: function() {
                    this.fetchData(this.root);
                }
            };

            this.dragOptions = {}; // set in $onInit based on editable input binding

            this.refresh = function() {
                this.actions.refreshList();
            }.bind(this);

            if (!this.itemTemplateUrl) {
                this.itemTemplateUrl = 'yEditableListDefaultItemTemplate.html';
            }

            this.rootId = 'root' + this.id;
            if (this.editable === undefined) {
                this.editable = true;
            }

            if (this.editable === true) {
                this._enableDragAndDrop();
            }
        };
    }])
    /**
     * @ngdoc directive
     * @name yEditableListModule.directive:yEditableList
     * @scope
     * @restrict E
     * @element y-editable-list
     * @description
     * The yEditableList component allows displaying a list of items. The list can be managed dynamically, by 
     * adding, removing, and re-ordering it. 
     * @param {@String} id A string used to track and identify the component. 
     * @param {<Array} items The collection of items to display in the component.
     * @param {=Function=} refresh A function that can be called to update the content of the list. 
     * @param {<Function} onChange A function that will be called whenever the content of the list changes. 
     * @param {<String=} itemTemplateUrl The path to the template to display each of the items in the list. 
     * @param {<boolean=} editable The property specifies whether the content of the list can be modified. 
     */
    .component('yEditableList', {
        templateUrl: 'yEditableListTemplate.html',
        controller: 'YEditableListController',
        controllerAs: 'ctrl',
        bindings: {
            id: '@',
            items: '<',
            refresh: '=?',
            onChange: '<',
            itemTemplateUrl: '<?',
            editable: '<?'
        }
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name loadConfigModule
 * @description
 * The loadConfigModule supplies configuration information to SmartEdit. Configuration is stored in key/value pairs.
 * The module exposes a service which is used to load configuration as an array or object.
 * @requires functionsModule
 * @requires ngResource
 * @requires sharedDataServiceModule
 * @requires resourceLocationsModule
 */
angular.module('loadConfigModule', ['functionsModule', 'ngResource', 'sharedDataServiceModule', 'resourceLocationsModule', 'operationContextServiceModule'])
    /**
     * @ngdoc service
     * @name loadConfigModule.service:LoadConfigManager
     * @description
     * The LoadConfigManager is used to retrieve configurations stored in configuration API.
     * @requires $resource
     * @requires hitch
     * @requires copy
     * @requires convertToArray
     * @requires sharedDataService
     * @requires $log
     * @requires resourceLocationsModule.object:SMARTEDIT_ROOT
     * @requires resourceLocationsModule.object:SMARTEDIT_RESOURCE_URI_REGEXP
     * @requires resourceLocationsModule.object:CONFIGURATION_URI
     */
    .factory('LoadConfigManager', ['$resource', 'hitch', 'copy', '$q', 'convertToArray', 'sharedDataService', 'operationContextService', '$log', 'SMARTEDIT_ROOT', 'SMARTEDIT_RESOURCE_URI_REGEXP', 'CONFIGURATION_URI', 'OPERATION_CONTEXT', function($resource, hitch, copy, $q, convertToArray, sharedDataService, operationContextService, $log, SMARTEDIT_ROOT, SMARTEDIT_RESOURCE_URI_REGEXP, CONFIGURATION_URI, OPERATION_CONTEXT) {

        /**
         * @ngdoc method
         * @name loadConfigModule.service:LoadConfigManager#LoadConfigManager
         * @methodOf loadConfigModule.service:LoadConfigManager
         * @description
         * This function is used to create a new object of the LoadConfigManager. 
         */
        var LoadConfigManager = function() {
            operationContextService.register(CONFIGURATION_URI, OPERATION_CONTEXT.TOOLING);

            this.editorViewService = $resource(CONFIGURATION_URI);
            this.configuration = [];

            this._convertToObject = function(configuration) {

                var configurations = configuration.reduce(function(previous, current) {
                    try {
                        if (typeof previous[current.key] !== 'undefined') {
                            $log.error('LoadConfigManager._convertToObject() - duplicate configuration keys found: ' + current.key);
                        }
                        previous[current.key] = JSON.parse(current.value);
                    } catch (parseError) {
                        $log.error("item _key_ from configuration contains unparsable JSON data _value_ and was ignored".replace("_key_", current.key).replace("_value_", current.value));
                    }
                    return previous;
                }, {});

                configurations.domain = SMARTEDIT_RESOURCE_URI_REGEXP.exec(this._getLocation())[1];
                configurations.smarteditroot = configurations.domain + '/' + SMARTEDIT_ROOT;
                return configurations;
            };

            this._getLocation = function() {
                return document.location.href;
            };
        };

        LoadConfigManager.prototype._parse = function(configuration) {
            var conf = copy(configuration);
            Object.keys(conf).forEach(function(key) {
                try {
                    conf[key] = JSON.parse(conf[key]);
                } catch (e) {
                    //expected for properties coming form $resource framework such as $promise.... and one wants the configuration editor itself to deal with parsable issues
                }
            });
            return conf;
        };

        /**
         * @ngdoc method
         * @name loadConfigModule.service.LoadConfigManager#loadAsArray
         * @methodOf loadConfigModule.service:LoadConfigManager
         * @description
         * Retrieves configuration from an API and returns as an array of mapped key/value pairs.
         *
         * Example:
         * <pre>
         * loadConfigManagerService.loadAsArray().then(
         *   hitch(this, function(response) {
         *     this._prettify(response);
         * }));
         * </pre>
         * 
         * @returns {Array} a promise of configuration values as an array of mapped configuration key/value pairs
         */
        LoadConfigManager.prototype.loadAsArray = function() {
            var deferred = $q.defer();
            this.editorViewService.query().$promise.then(
                hitch(this, function(response) {
                    deferred.resolve(this._parse(response));
                }),
                function() {
                    $log.log("Fail to load the configurations.");
                    deferred.reject();
                }
            );
            return deferred.promise;
        };

        /**
         * @ngdoc method
         * @name loadConfigModule.service.LoadConfigManager#loadAsObject
         * @methodOf loadConfigModule.service:LoadConfigManager
         *
         * @description
         * Retrieves a configuration from the API and converts it to an object.  
         * 
         * Example
         * <pre>
         * loadConfigManagerService.loadAsObject().then(function(conf) {
         *   sharedDataService.set('defaultToolingLanguage', conf.defaultToolingLanguage);
         *  });
         * </pre>
         * @returns {Object} a promise of configuration values as an object of mapped configuration key/value pairs
         */
        LoadConfigManager.prototype.loadAsObject = function() {
            var deferred = $q.defer();
            this.loadAsArray().then(
                hitch(this, function(response) {
                    var conf = this._convertToObject(response);
                    sharedDataService.set('configuration', conf);
                    deferred.resolve(conf);
                }),
                function() {
                    deferred.reject();
                }
            );
            return deferred.promise;
        };

        return LoadConfigManager;

    }])

/**
 * @ngdoc service
 * @name loadConfigModule.service:loadConfigManagerService
 *
 * @description
 * A service that is a singleton of {@link loadConfigModule.service:LoadConfigManager}  which is used to 
 * retrieve smartedit configuration values.
 * This services is the entry point of the smartedit configuration module. 
 * @requires LoadConfigManager
 */
.factory('loadConfigManagerService', ['LoadConfigManager', function(LoadConfigManager) {
    return new LoadConfigManager();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('localizedElementModule', ['tabsetModule', 'seConstantsModule'])
    .directive('localizedElement', ['VALIDATION_MESSAGE_TYPES', function(VALIDATION_MESSAGE_TYPES) {

        return {
            templateUrl: 'localizedElementTemplate.html',
            restrict: 'E',
            transclude: false,
            replace: false,
            scope: {
                model: '=',
                languages: '=',
                inputTemplate: '='
            },

            link: function($scope, element, attrs) {

                $scope.tabs = [];

                $scope.$watch("model", function(model) {
                    if (model) {
                        var inputTemplate = $scope.inputTemplate ? $scope.inputTemplate : attrs.inputTemplate;

                        $scope.tabs.length = 0;
                        Array.prototype.push.apply($scope.tabs, $scope.languages.map(function(language) {
                            return {
                                id: language.isocode,
                                title: language.isocode.toUpperCase() + (language.required ? "*" : ""),
                                templateUrl: inputTemplate
                            };
                        }));
                    }
                });

                $scope.$watch("model.field.messages", function(messages) {
                    if ($scope.model) {
                        var messageMap = messages ? messages.filter(function(messsage) {
                            return messsage.type === VALIDATION_MESSAGE_TYPES.VALIDATION_ERROR;
                        }).reduce(function(holder, next) {
                            holder[next.language] = true;
                            return holder;
                        }, {}) : {};

                        $scope.tabs.forEach(function(tab) {
                            var message = messageMap[tab.id];
                            tab.hasErrors = message !== undefined ? message : false;
                        });
                    }
                }, true);

            }
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    /**
     * @ngdoc overview
     * @name yLoDashModule
     * @description
     * This module manages the use of the lodash library in SmartEdit. It makes sure the library is introduced
     * in the Angular lifecycle and makes it easy to mock for unit tests.
     */
    angular.module('yLoDashModule', [])
        /**
         * @ngdoc object
         * @name yLoDashModule.lodash
         * @description
         * 
         * Makes the underscore library available to SmartEdit.
         *
         * Note: original _ namespace is removed from window in order not to clash with other libraries especially in the storefront AND to enforce proper dependency injection.
         */
        /* forbiddenNameSpaces:false */
        .factory('lodash', function() {
            if (!window.smarteditLodash) {
                if (window._ && window._.noConflict) {
                    window.smarteditLodash = window._.noConflict();
                } else {
                    throw "could not find lodash library under window._ namespace";
                }
            }
            return window.smarteditLodash;
        });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name modalServiceModule
 * @description
 * # The modalServiceModule
 *
 * The modal service module is a module devoted to providing an easy way to create, use, manage, and style modal windows,
 * and it contains all the components related to achieving this goal.
 *
 * Use the {@link modalServiceModule.modalService modalService} to open modal windows.
 *
 * Once a modal window is opened, you can use it's {@link modalServiceModule.service:ModalManager ModalManager} to manage
 * the modal, such as closing the modal, adding buttons, or chaning the title.
 *
 * Additionally you may use {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button actions} or
 * {@link modalServiceModule.object:MODAL_BUTTON_STYLES button styles} to affect how buttons look and behave on the modal
 * window.
 *
 */
angular.module('modalServiceModule', ['ui.bootstrap', 'translationServiceModule', 'functionsModule', 'coretemplates'])

/**
 * @ngdoc object
 * @name modalServiceModule.object:MODAL_BUTTON_ACTIONS
 *
 * @description
 * Injectable angular constant<br/>
 * Defines the action to be taken after executing a button on a modal window. To be used when adding a button to the modal,
 * either when opening a modal (see {@link modalServiceModule.service:ModalManager#methods_getButtons ModalManager.getButtons()}) or
 * when adding a button to an existing modal (see {@link modalServiceModule.modalService#methods_open modalService.open()})
 *
 * Example:
 * <pre>
 *      myModalManager.addButton({
 *          id: 'button id',
 *          label: 'close_modal',
 *          action: MODAL_BUTTON_ACTIONS.CLOSE
 *      });
 * </pre>
 */
.constant('MODAL_BUTTON_ACTIONS', {
    /**
     * @ngdoc property
     * @name NONE
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_ACTIONS
     *
     * @description
     * Indicates to the {@link modalServiceModule.service:ModalManager ModalManager} that after executing the modal button
     * no action should be performed.
     **/
    NONE: "none",

    /**
     * @ngdoc property
     * @name CLOSE
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_ACTIONS
     *
     * @description
     * Indicates to the {@link modalServiceModule.service:ModalManager ModalManager} that after executing the modal button,
     * the modal window should close, and the {@link https://docs.angularjs.org/api/ng/service/$q promise} returned by the modal should be resolved.
     **/
    CLOSE: "close",

    /**
     * @ngdoc property
     * @name DISMISS
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_ACTIONS
     *
     * @description
     * Indicates to the {@link modalServiceModule.service:ModalManager ModalManager} that after executing the modal button,
     * the modal window should close, and the {@link https://docs.angularjs.org/api/ng/service/$q promise} returned by the modal should be rejected.
     **/
    DISMISS: "dismiss"
})

/**
 * @ngdoc object
 * @name modalServiceModule.object:MODAL_BUTTON_STYLES
 *
 * @description
 * Injectable angular constant<br/>
 * Defines the look and feel of a button on a modal window. To be used when adding a button to the modal,
 * either when opening a modal (see {@link modalServiceModule.service:ModalManager#methods_getButtons ModalManager.getButtons()}) or
 * when adding a button to an existing modal (see {@link modalServiceModule.modalService#methods_open modalService.open()})
 *
 * Example:
 * <pre>
 *      myModalManager.addButton({
 *          id: 'button id',
 *          label: 'cancel_button',
 *          style: MODAL_BUTTON_STYLES.SECONDARY
 *      });
 * </pre>
 */
.constant('MODAL_BUTTON_STYLES', {

    /**
     * @ngdoc property
     * @name DEFAULT
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_STYLES
     *
     * @description
     * Equivalent to SECONDARY
     **/
    DEFAULT: "default",

    /**
     * @ngdoc property
     * @name PRIMARY
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_STYLES
     *
     * @description
     * Indicates to the modal window that this button is the primary button of the modal, such as save or submit,
     * and should be styled accordingly.
     **/
    PRIMARY: "primary",

    /**
     * @ngdoc property
     * @name SECONDARY
     * @propertyOf modalServiceModule.object:MODAL_BUTTON_STYLES
     *
     * @description
     * Indicates to the modal window that this button is a secondary button of the modal, such as cancel,
     * and should be styled accordingly.
     **/
    SECONDARY: "default"
})

/**
 * @ngdoc service
 * @name modalServiceModule.modalService
 *
 * @description
 * Convenience service to open and style a promise-based templated modal window.
 *
 * Simple Example:
 * <pre>
    angular.module('app', ['modalServiceModule'])
        .factory('someService', function($log, modalService, MODAL_BUTTON_ACTIONS) {

            modalService.open({
                title: "My Title",
                template: '<div>some content</div>',
                buttons: [{
                    label: "Close",
                    action: MODAL_BUTTON_ACTIONS.CLOSE
                }]
            }).then(function (result) {
                $log.debug("modal closed!");
                }, function (failure) {
            }
        );

    });
 * </pre>
 *
 * More complex example:
 * <pre>
 *
    angular.module('app', ['modalServiceModule'])

     .factory('someService',
        function($q, modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES) {

            modalService.open({
                title: "modal.title",
                template: '<div>some content</div>',
                controller: 'modalController',
                buttons: [
                    {
                        id: 'submit',
                        label: "Submit",
                        action: MODAL_BUTTON_ACTIONS.CLOSE
                    },
                    {
                        label: "Cancel",
                        action: MODAL_BUTTON_ACTIONS.DISMISS
                    },

                ]
            }).then(function (result) {
                $log.log("Modal closed with data:", result);
            }, function (failure) {
            });
        }
     )

     .controller('modalController', function($scope, $q) {

            function validateSomething() {
                return true;
            };

            var buttonHandlerFn = function (buttonId) {
                if (buttonId === 'submit') {
                    var deferred = $q.defer();
                    if (validateSomething()) {
                        deferred.resolve("someResult");
                    } else {
                        deferred.reject();  // cancel the submit button's close action
                    }
                    return deferred.promise;
                }
            };

            $scope.modalManager.setButtonHandler(buttonHandlerFn);

        });
 * </pre>
 */
.factory('modalService', ['$uibModal', '$injector', '$controller', '$rootScope', '$templateCache', '$translate', '$log', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', 'merge', 'copy', 'hitch', 'generateIdentifier', function($uibModal, $injector, $controller, $rootScope, $templateCache, $translate, $log, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, merge, copy, hitch, generateIdentifier) {


    /**
     * @ngdoc service
     * @name modalServiceModule.service:ModalManager
     *
     * @description
     * The ModalManager is a service designed to provide easy runtime modification to various aspects of a modal window,
     * such as the modifying the title, adding a buttons, setting callbacks, etc...
     *
     * The ModalManager constructor is not exposed publicly, but an instance of ModalManager is added to the scope of
     * the modal content implicitly through the scope chain/prototyping. As long as you don't create an
     * {@link https://docs.angularjs.org/guide/scope isolated scope} for the modal, you can access it through $scope.modalManager
     *
     * <pre>
     *  .controller('modalTestController', function($scope, $log) {
     *    var buttonHandlerFn = function (buttonId) {
     *        $log.debug("button with id", buttonId, "was pressed!");
     *    };
     *    $scope.modalManager.setButtonHandler(buttonHandlerFn);
     *    ...
     * </pre>
     *
     */
    function ModalManager(conf) {

        var buttonEventCallback;
        var showDismissX = true;
        var dismissCallback = null;
        var buttons = [];

        if (!conf.modalInstance) {
            throw 'no.modalInstance.injected';
        }
        this.closeFunction = conf.modalInstance.close;
        this.dismissFunction = conf.modalInstance.dismiss;

        this._defaultButtonOptions = {
            id: 'button.id',
            label: 'button.label',
            action: MODAL_BUTTON_ACTIONS.NONE,
            style: MODAL_BUTTON_STYLES.PRIMARY,
            disabled: false,
            callback: null
        };

        this._createButton = function(buttonConfig) {
            var defaultButtonConfig = copy(this._defaultButtonOptions);

            merge(defaultButtonConfig, buttonConfig || {});
            $translate(defaultButtonConfig.label).then(
                function(translatedValue) {
                    defaultButtonConfig.label = translatedValue;
                }
            );

            var styleValidated = false;
            for (var style in MODAL_BUTTON_STYLES) {
                if (MODAL_BUTTON_STYLES[style] === defaultButtonConfig.style) {
                    styleValidated = true;
                    break;
                }
            }
            if (!styleValidated) {
                throw 'modalService.ModalManager._createButton.illegal.button.style';
            }

            var actionValidated = false;
            for (var action in MODAL_BUTTON_ACTIONS) {
                if (MODAL_BUTTON_ACTIONS[action] === defaultButtonConfig.action) {
                    actionValidated = true;
                    break;
                }
            }
            if (!actionValidated) {
                throw 'modalService.ModalManager._createButton.illegal.button.action';
            }

            return defaultButtonConfig;
        };

        this.title = "";

        if (typeof conf.title === 'string') {
            this.title = conf.title;
        }

        if (typeof conf.titleSuffix === 'string') {
            this.titleSuffix = conf.titleSuffix;
        }

        if (conf.buttons) {
            for (var index = 0; index < conf.buttons.length; index++) {
                buttons.push(this._createButton(conf.buttons[index]));
            }
        }

        this._buttonPressed = function(button) {
            var callbackReturnedPromise = null;
            if (button.callback) {
                callbackReturnedPromise = button.callback();
            } else if (buttonEventCallback) {
                callbackReturnedPromise = buttonEventCallback(button.id);
            }
            if (button.action !== MODAL_BUTTON_ACTIONS.NONE) {
                // by contract, callbackReturnedPromise must be a promise if it exists by this point
                var exitFn = button.action === MODAL_BUTTON_ACTIONS.CLOSE ? this.close : this.dismiss;
                if (callbackReturnedPromise) {
                    callbackReturnedPromise.then(hitch(this, function(data) {
                        exitFn.call(this, data);
                    }));
                    // if promise rejected - do nothing
                } else {
                    exitFn.call(this);
                }
            }
        };

        this._handleDismissButton = function() {
            if (dismissCallback) {
                var promise = dismissCallback();
                promise.then(hitch(this, function(result) {
                    this.dismiss(result);
                }));
            } else {
                this.dismiss();
            }
        };


        this._showDismissButton = function() {
            return showDismissX;
        };

        this._hasButtons = function() {
            return buttons.length > 0;
        };

        // -------------------------- Public API -----------------------------

        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#addButton
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {Object} conf (OPTIONAL) Button configuration
         * @param {String} [conf.id='button.id'] An ID for the button. It does not need to be unique, but it is suggested.
         * Can be used with the modal manager to enable/disable buttons, see which button is fired in the button handler, etc...
         * @param {String} [conf.label='button.label'] An i18n key that will be translated, and applied as the label of the button
         * @param {Boolean} [conf.disabled=false] Flag to enable/disable the button on the modal
         * @param {MODAL_BUTTON_STYLES} [conf.style=MODAL_BUTTON_STYLES.DEFAULT] One of {@link modalServiceModule.object:MODAL_BUTTON_STYLES MODAL_BUTTON_STYLES}
         * @param {MODAL_BUTTON_ACTIONS} [conf.action=MODAL_BUTTON_ACTIONS.NONE] One of {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS MODAL_BUTTON_ACTIONS}
         * @param {function} [conf.callback=null] A function that will be called with no parameters when the button is pressed.
         * This (optional) function may return null, undefined, or a {@link https://docs.angularjs.org/api/ng/service/$q promise}.
         * Resolving the {@link https://docs.angularjs.org/api/ng/service/$q promise} will trigger the
         * {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button action} (if any), and rejecting the
         * {@link https://docs.angularjs.org/api/ng/service/$q promise} will prevent the action from being executed.
         *
         * Note: If a button has a callback and the ModalManager has registered a
         * {@link modalServiceModule.service:ModalManager#methods_setButtonHandler button handler}, only the button callback
         * will be executed on button press. This is to avoid the unnecessary complexity of having multiple handlers for a single button.
         *
         * @returns {Object} An object representing the newly added button
         */
        this.addButton = function(newButtonConf) {
            var newButton = this._createButton(newButtonConf);
            buttons.push(newButton);
            return newButton;
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#getButtons
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @description
         * Caution!
         *
         * This is a reference to the buttons being used by the modal manager, not a clone. This should
         * only be used to read or update properties provided in the Button configuration. See
         * {@link modalServiceModule.service:ModalManager#methods_addButton addButton()} for more details.
         *
         * @returns {Array} An array of all the buttons on the modal window, empty array if there are no buttons.
         */
        this.getButtons = function() {
            return buttons;
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#removeAllButtons
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @description
         * Remove all buttons from the modal window
         *
         */
        this.removeAllButtons = function() {
            buttons.splice(0, buttons.length);
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#removeButton
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {String} id The id of the button to be removed.
         *
         * @description
         * Remove a buttons from the modal window
         *
         */
        this.removeButton = function(buttonId) {
            for (var buttonIndex = buttons.length - 1; buttonIndex >= 0; buttonIndex--) {
                if (buttons[buttonIndex].id === buttonId) {
                    buttons.splice(buttonIndex, 1);
                }
            }
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#enableButton
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {String} id The id of the button to be enabled.
         *
         * @description
         * Enables a button on the modal window, allowing it to be pressed.
         *
         */
        this.enableButton = function(buttonId) {
            for (var buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {
                if (buttons[buttonIndex].id === buttonId) {
                    buttons[buttonIndex].disabled = false;
                }
            }
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#disableButton
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {String} id The id of the button to be disabled.
         *
         * @description
         * Disabled a button on the modal window, preventing it from be pressed.
         *
         */
        this.disableButton = function(buttonId) {
            for (var buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {
                if (buttons[buttonIndex].id === buttonId) {
                    buttons[buttonIndex].disabled = true;
                }
            }
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#getButton
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {String} id The id of the button to be fetched
         *
         * @returns {Object} The first button found with a matching id, or null
         */
        this.getButton = function(buttonId) {
            for (var buttonIndex = 0; buttonIndex < buttons.length; buttonIndex++) {
                if (buttons[buttonIndex].id === buttonId) {
                    return buttons[buttonIndex];
                }
            }
            return null;
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#setShowHeaderDismiss
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {boolean} showX Flag to show/hide the X dismiss button at the top right corner of the modal window,
         * when the modal header is displayed
         *
         */
        this.setShowHeaderDismiss = function(showButton) {
            if (typeof showButton === 'boolean') {
                showDismissX = showButton;
            } else {
                throw 'modalService.ModalManager.showDismissX.illegal.param';
            }
        };


        /**
         * @ngdoc method
         * @name modalServiceModule.service:ModalManager#setDismissCallback
         * @methodOf modalServiceModule.service:ModalManager
         *
         * @param {function} dismissCallback A function to be called when the X dismiss button at the top right corner of the modal window
         * is pressed. This function must either return null or a {@link https://docs.angularjs.org/api/ng/service/$q promise}.
         *
         * If the {@link https://docs.angularjs.org/api/ng/service/$q promise} is resolved, or if the function returns null or undefined, then the modal is closed and the returned
         * modal {@link https://docs.angularjs.org/api/ng/service/$q promise} is rejected.
         *
         * If the callback {@link https://docs.angularjs.org/api/ng/service/$q promise} is rejected, the modal is not closed, allowing you to provide some kind of validation
         * before closing.
         *
         */
        this.setDismissCallback = function(dismissCallbackFunction) {
            dismissCallback = dismissCallbackFunction;
        };


        /**
             * @ngdoc method
             * @name modalServiceModule.service:ModalManager#setButtonHandler
             * @methodOf modalServiceModule.service:ModalManager
             *
             * @description
             *
             * @param {Function} buttonPressedCallback The buttonPressedCallback is a function that is called when any button on the
             * modal, that has no {@link modalServiceModule.service:ModalManager#methods_addButton button callback}, is pressed. If a button has a
             * {@link modalServiceModule.service:ModalManager#methods_addButton button callback} function, then that function will be
             * called instead of the buttonPressedCallback.
             *
             * This buttonPressedCallback receives a single parameter, which is the string ID of the button that was pressed.
             * Additionally, this function must either return null, undefined or a {@link https://docs.angularjs.org/api/ng/service/$q promise}.
             *
             * If null/undefined is return, the modal will continue to process the {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button action}
             * In this case, no data will be returned to the modal {@link https://docs.angularjs.org/api/ng/service/$q promise} if the modal is closed.
             *
             * If a promise is returned by this function, then the {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button action}
             * may be cancelled/ignored by rejecting the promise. If the promise is resolved, the {@link modalServiceModule.service:ModalManager ModalManager}
             * will continue to process the {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button action}.
             *
             * If by resolving the promise returned by the buttonHandlerFunction with data passed to the resolve, and the {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button action}
             * is such that it results in the modal closing, then the modal promise is resolved/rejected with that same data. This allows you to pass data from the  buttonHandlerFunction
             * the the modalService.open(...) caller.
             *
             * See {@link modalServiceModule.service:ModalManager#methods_addButton addButton() for more details on the button callback }
             *
             *
             * A few scenarios for example:
             * #1 A button with a button callback is pressed.
             * <br/>Result: buttonPressedCallback is never called.
             *
             * #2 A button is pressed, buttonPressedCallback return null
             * <br/>Result: The modal manager will execute any action on the button
             *
             * #3 A button is pressed, buttonPressedCallback returns a promise, that promise is rejected
             * <br/>Result: Modal Manager will ignore the button action and nothing else will happen
             *
             * #4 A button with a dismiss action is pressed, buttonPressedCallback returns a promise, and that promise is resolved with data "Hello"
             * <br/>Result: ModalManager will execute the dismiss action, closing the modal, and errorCallback of the modal promise, passing "Hello" as data
             *
             *
             * Code sample of validating some data before closing the modal
             * <pre>
             function validateSomething() {
        return true;
    };

             var buttonHandlerFn = function (buttonId) {
        if (buttonId === 'submit') {
            var deferred = $q.defer();
            if (validateSomething()) {
                deferred.resolve("someResult");
            } else {
                deferred.reject();  // cancel the submit button's close action
            }
            return deferred.promise;
        }
    };

             $scope.modalManager.setButtonHandler(buttonHandlerFn);
             * </pre>
             */
        this.setButtonHandler = function(buttonHandlerFunction) {
            buttonEventCallback = buttonHandlerFunction;
        };

    }

    /**
     * @ngdoc method
     * @name modalServiceModule.service:ModalManager#close
     * @methodOf modalServiceModule.service:ModalManager
     *
     * @description
     * The close function will close the modal window, passing the provided data (if any) to the successCallback
     * of the modal {@link https://docs.angularjs.org/api/ng/service/$q promise} by resolving the {@link https://docs.angularjs.org/api/ng/service/$q promise}.
     *
     * @param {Object} data Any data to be returned to the resolved modal {@link https://docs.angularjs.org/api/ng/service/$q promise} when the modal is closed.
     *
     */
    ModalManager.prototype.close = function(dataToReturn) {
        if (this.closeFunction) {
            this.closeFunction(dataToReturn);
        }
    };


    /**
     * @ngdoc method
     * @name modalServiceModule.service:ModalManager#dismiss
     * @methodOf modalServiceModule.service:ModalManager
     *
     * @description
     * The dismiss function will close the modal window, passing the provided data (if any) to the {@link https://docs.angularjs.org/api/ng/service/$q errorCallback}
     * of the modal {@link https://docs.angularjs.org/api/ng/service/$q promise} by rejecting the {@link https://docs.angularjs.org/api/ng/service/$q promise}.
     *
     * @param {Object} data Any data to be returned to the rejected modal {@link https://docs.angularjs.org/api/ng/service/$q promise} when the modal is closed.
     *
     */
    ModalManager.prototype.dismiss = function(dataToReturn) {
        if (this.dismissFunction) {
            this.dismissFunction(dataToReturn);
        }
    };


    function getControllerClass(conf) {
        var that = this;
        return ['$scope', '$uibModalInstance', function ModalController($scope, $uibModalInstance) {
            conf.modalInstance = $uibModalInstance;

            this._modalManager = new ModalManager(conf);
            that._modalManager = this._modalManager;
            $scope.modalController = this;

            if (conf.controller) {
                angular.extend(this, $controller(conf.controller, {
                    $scope: $scope,
                    modalManager: this._modalManager
                }));

                if (this.init) {
                    this.init();
                }
            }
            if (conf.controllerAs) {
                $scope[conf.controllerAs] = this;
            }

            if (conf.templateInline) {
                this.templateUrl = "modalTemplateKey" + btoa(generateIdentifier());
                $templateCache.put(this.templateUrl, conf.templateInline);
            } else {
                this.templateUrl = conf.templateUrl;
            }

            this.close = hitch(this, function(data) {
                this._modalManager.close(data);
                $templateCache.remove(this.templateUrl);
            });

            this.dismiss = hitch(this, function(data) {
                this._modalManager.dismiss(data);
                $templateCache.remove(this.templateUrl);
            });
        }];
    }


    function ModalOpener() {}


    /**
     * @ngdoc method
     * @name modalServiceModule.modalService#open
     * @methodOf modalServiceModule.modalService
     *
     * @description
     * Open provides a simple way to open modal windows with custom content, that share a common look and feel.
     *
     * The modal window can be closed multiple ways, through {@link modalServiceModule.object:MODAL_BUTTON_ACTIONS button actions},
     * by explicitly calling the {@link modalServiceModule.service:ModalManager#methods_close close} or
     * {@link modalServiceModule.service:ModalManager#methods_close dismiss} functions, etc... Depending on how you
     * choose to close a modal, either the modal {@link https://docs.angularjs.org/api/ng/service/$q promise's}
     * {@link https://docs.angularjs.org/api/ng/service/$q successCallback} or {@link https://docs.angularjs.org/api/ng/service/$q errorCallback}
     * will be called. You can use the callbacks to return data from the modal content to the caller of this function.
     *
     * @param {Object} conf configuration
     * @param {String} conf.title (OPTIONAL) key for your modal title to be translated
     * @param {Array} conf.buttons (OPTIONAL) Array of button configurations. See {@link modalServiceModule.service:ModalManager#methods_addButton ModalManager.addButton()} for config details.
     * @param {String} conf.templateUrl path to an HTML fragment you mean to display in the modal window
     * @param {String} conf.templateInline inline HTML fragment you mean to display in the the modal window
     * @param {function} conf.controller the piece of logic that acts as controller of your template. It can be declared through its ID in AngularJS dependency injection or through explicit function
     * @param {String} conf.cssClasses space separated list of additional css classed to be added to the overall modal
     * @param {=boolean=} [conf.animation=true] determines whether CSS animations will be used for the modal
     *
     * @returns {function} {@link https://docs.angularjs.org/api/ng/service/$q promise} that will either be resolved or
     * rejected when the modal window is closed.
     */
    ModalOpener.prototype.open = function(conf) {
        var configuration = conf || {};

        if (!configuration.templateUrl && !configuration.templateInline) {
            throw "modalService.configuration.errors.no.template.provided";
        }
        if (configuration.templateUrl && configuration.templateInline) {
            throw "modalService.configuration.errors.2.templates.provided";
        }

        return $uibModal.open({
            templateUrl: 'modalTemplate.html',
            size: configuration.size || 'lg',
            backdrop: 'static',
            keyboard: false,
            controller: getControllerClass.call(this, configuration),
            controllerAs: 'modalController',
            windowClass: configuration.cssClasses || null,
            animation: (!conf.animation || conf.animation !== false) && !($injector.has('$animate') && !$injector.get('$animate').enabled())
        }).result;
    };

    return new ModalOpener();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('notificationMouseLeaveDetectionServiceInterfaceModule', [])
    .constant('SE_NOTIFICATION_MOUSE_LEAVE_DETECTION_SERVICE_GATEWAY_ID', 'SE_NOTIFICATION_MOUSE_LEAVE_DETECTION_SERVICE_GATEWAY_ID')
    /*
     * The interface defines the methods required to detect when the mouse leaves the notification panel
     * in the SmartEdit application and in the SmartEdit container.
     * 
     * It is solely meant to be used with the notificationService.
     */
    .factory('NotificationMouseLeaveDetectionServiceInterface', function() {
        function NotificationMouseLeaveDetectionServiceInterface() {}

        /*
         * This method starts tracking the movement of the mouse pointer in order to detect when it
         * leaves the notification panel.
         * 
         * The innerBounds parameter is considered optional. If it is not provided, it will not be
         * validated and detection will only be started in the SmartEdit container.
         * 
         * Here is an example of a bounds object:
         * 
         * {
         *     x: 100,
         *     y: 100,
         *     width: 200,
         *     height: 50
         * }
         * 
         * This method will throw an error if:
         *     - the bounds parameter is not provided
         *     - a bounds object does not contain the X coordinate
         *     - a bounds object does not contain the Y coordinate
         *     - a bounds object does not contain the width dimension
         *     - a bounds object does not contain the height dimension
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype.startDetection = function() {};

        /*
         * This method stops tracking the movement of the mouse pointer.
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype.stopDetection = function() {};

        /*
         * These two methods are used to start and stop tracking the movement of the mouse pointer within the iFrame.
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype._remoteStartDetection = function() {};
        NotificationMouseLeaveDetectionServiceInterface.prototype._remoteStopDetection = function() {};

        /*
         * This method is used to call the callback function when it is detected from within the iFrame that the mouse
         * left the notification panel 
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype._callCallback = function() {};

        /*
         * This method is triggered when the service has detected that the mouse left the
         * notification panel. It will execute the callback function and stop detection.
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype._onMouseLeave = function() {
            var callbackFn = this._getCallback() || this._callCallback;

            callbackFn();
            this.stopDetection();
        };

        /*
         * This method is called for each mouse movement. It evaluates whether or not the
         * mouse pointer is in the notification panel. If it isn't, it calls the onMouseLeave.
         */
        NotificationMouseLeaveDetectionServiceInterface.prototype._onMouseMove = function(event) {
            var bounds = this._getBounds();
            var isOutsideX = bounds && event && (event.clientX < bounds.x || event.clientX > bounds.x + bounds.width);
            var isOutsideY = bounds && event && (event.clientY < bounds.y || event.clientY > bounds.y + bounds.height);

            if (isOutsideX || isOutsideY) {
                this._onMouseLeave();
            }
        };

        NotificationMouseLeaveDetectionServiceInterface.prototype._getBounds = function() {};

        NotificationMouseLeaveDetectionServiceInterface.prototype._getCallback = function() {};

        return NotificationMouseLeaveDetectionServiceInterface;
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name pageSensitiveDirectiveModule
 * @description
 * This module defines the {@link pageSensitiveDirectiveModule.directive:pageSensitive pageSensitive} attribute directive.
 **/
angular.module('pageSensitiveDirectiveModule', ['crossFrameEventServiceModule'])

.controller('pageSensitiveController', ['$timeout', 'EVENTS', 'crossFrameEventService', function($timeout, EVENTS, crossFrameEventService) {

        this.$onInit = function() {
            this.hasContent = true;
            this.unRegisterPageChangeListener = crossFrameEventService.subscribe(EVENTS.PAGE_CHANGE, function() {
                this.hasContent = false;
                $timeout(function() {
                    this.hasContent = true;
                }.bind(this), 0);
            }.bind(this));
        };

        this.$onDestroy = function() {
            this.unRegisterPageChangeListener();
        };

    }])
    /**
     * @ngdoc directive
     * @name pageSensitiveDirectiveModule.directive:pageSensitive
     * @restrict A
     * @description
     * Will cause an Angular re-compilation of the node declaring this directive whenever the page identifier in smartEdit layer changes
     */
    .directive("pageSensitive", function() {
        return {
            restrict: "A",
            replace: false,
            transclude: true,
            template: "<div class='se-page-sensitive' data-ng-if='ctrl.hasContent' data-ng-transclude></div>",
            scope: true,
            controller: 'pageSensitiveController',
            controllerAs: 'ctrl',
            bindToController: true
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('yPopoverModule', ['functionsModule', 'translationServiceModule', 'ui.bootstrap', 'coretemplates'])
    .controller('YPopoverController', ['$sce', 'isBlank', function($sce, isBlank) {
        this.$onChanges = function() {

            if (isBlank(this.template) && isBlank(this.templateUrl)) {
                throw "yPopover directive was invoked with neither a template nor a templateUrl";
            }
            if (!isBlank(this.template) && !isBlank(this.templateUrl)) {
                throw "yPopover directive was invoked with both a template and a templateUrl";
            }

            if (!isBlank(this.template)) {
                this.template = $sce.trustAsHtml(this.template);
            }
            this.placement = this.placement || "top";
            if (isBlank(this.trigger)) {
                this.trigger = "click";
            } else if (this.trigger === 'hover') {
                this.trigger = "mouseenter";
            }
        };

        this.$onInit = function() {
            if (this.isOpen === undefined) {
                this.isOpen = false;
            }
        };
    }])

/**
 * @ngdoc directive
 * @name yPopoverModule.directive:yPopover
 * @scope
 * @restrict A
 *
 * @description
 * This directive attaches a customizable popover on a DOM element.
 * @param {<String=} template the HTML body to be used in the popover body, it will automatically be trusted by the directive. Optional but exactly one of either template or templateUrl must be defined.
 * @param {<String=} templateUrl the location of the HTML template to be used in the popover body. Optional but exactly one of either template or templateUrl must be defined.
 * @param {<String=} title the title to be used in the popover title section. Optional.
 * @param {<String=} placement the placement of the popover around the target element. Possible values are <b>top, left, right, bottom</b>, as well as any
 * concatenation of them with the following format: placement1-placement2 such as bottom-right. Optional, default value is top.
 * @param {=String=} trigger the event type that will trigger the popover. Possibles values are <b>hover, click, outsideClick, none</b>. Optional, default value is 'click'.
 */
.directive('yPopover', function() {
    return {
        templateUrl: 'yPopoverTemplate.html',
        restrict: 'A',
        transclude: true,
        replace: false,
        controller: 'YPopoverController',
        controllerAs: 'ypop',
        scope: {},
        bindToController: {
            template: '<?',
            templateUrl: '<?',
            title: '<?',
            placement: "<?",
            trigger: "<?",
            isOpen: "=?"
        }
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('previewTicketInterceptorModule', ['interceptorHelperModule', 'functionsModule', 'componentHandlerServiceModule'])

/**
 * @ngdoc service
 * @name previewTicketInterceptorModule.previewTicketInterceptor
 *
 * @description
 * A HTTP request interceptor that adds the preview ticket to the HTTP header object before a request is made.
 *
 * Interceptors are service factories that are registered with the $httpProvider by adding them to the
 * $httpProvider.interceptors array. The factory is called and injected with dependencies and returns the interceptor
 * object, which contains the interceptor methods.
 */
.factory('previewTicketInterceptor', ['parseQuery', 'interceptorHelper', 'componentHandlerService', function(parseQuery, interceptorHelper, componentHandlerService) {
        var getLocation = function() {
            var location;

            if (window.frameElement) {
                location = document.location.href;
            } else {
                location = componentHandlerService._getTargetIframe().attr("src");
            }
            return location;
        };

        /**
         * @ngdoc method
         * @name previewTicketInterceptorModule.previewTicketInterceptor#request
         * @methodOf previewTicketInterceptorModule.previewTicketInterceptor
         *
         * @description
         * Interceptor method that is called with a HTTP configuration object. It extracts the preview ticket ID (if
         * available) from the URL of the current page and then adds it to the HTTP header object as an
         * "X-Preview-Ticket" property before a request is made to the resource.
         *
         * @param {Object} config The HTTP configuration object that holds the configuration information.
         *
         * @returns {Object} Returns the modified configuration object.
         */
        var request = function request(config) {
            return interceptorHelper.handleRequest(config, function() {
                var location = this._getLocation();
                if (location) {
                    var previewTicket = parseQuery(location).cmsTicketId;
                    if (previewTicket) {
                        config.headers = config.headers || {};
                        config.headers["X-Preview-Ticket"] = previewTicket;
                    }
                }
                return config;
            }.bind(this));
        };

        var interceptor = {};
        interceptor.request = request.bind(interceptor);
        interceptor._getLocation = getLocation.bind(interceptor);
        return interceptor;
    }])
    .config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('previewTicketInterceptor');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name recompileDomModule
 * @description
 * This module defines the {@link recompileDomModule.directive:recompileDom recompileDom} component.
 **/
angular.module('recompileDomModule', [])

/**
 * @ngdoc directive
 * @name recompileDomModule.directive:recompileDom
 * @restrict A
 * @requires $timeout
 * @param {= Function} recompileDom Function invoked from the outer scope to trigger the recompiling of the transcluded content.
 * @description
 * The recompile dom directive accepts a function param, and can be applied to any part of the dom.
 * Upon execution of the function, the inner contents of this dom is recompiled by Angular.
 */
.directive("recompileDom", ['$timeout', function($timeout) {
    return {
        restrict: "A",
        replace: false,
        transclude: true,
        template: "<div data-ng-if='showContent' data-ng-transclude></div>",
        scope: {
            trigger: '=recompileDom'
        },
        link: function(scope) {
            scope.showContent = true;
            scope.trigger = function() {
                scope.showContent = false;
                $timeout(function() {
                    scope.showContent = true;
                }, 0);
            };
        }
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    var STORE_FRONT_CONTEXT_VAR = '/storefront';

    var CONTEXT_CATALOG = 'CURRENT_CONTEXT_CATALOG';
    var CONTEXT_CATALOG_VERSION = 'CURRENT_CONTEXT_CATALOG_VERSION';
    var CONTEXT_SITE_ID = 'CURRENT_CONTEXT_SITE_ID';

    var PAGE_CONTEXT_CATALOG = 'CURRENT_PAGE_CONTEXT_CATALOG',
        PAGE_CONTEXT_CATALOG_VERSION = 'CURRENT_PAGE_CONTEXT_CATALOG_VERSION',
        PAGE_CONTEXT_SITE_ID = 'CURRENT_PAGE_CONTEXT_SITE_ID';

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:UriContext
     *
     * @description
     * A map that contains the necessary site and catalog information for CMS services and directives.
     * It contains the following keys:
     * {@link resourceLocationsModule.object:CONTEXT_SITE_ID CONTEXT_SITE_ID} for the site uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG CONTEXT_CATALOG} for the catalog uid,
     * {@link resourceLocationsModule.object:CONTEXT_CATALOG_VERSION CONTEXT_CATALOG_VERSION} for the catalog version.
     */

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PageUriContext
     *
     * @description
     * A map that contains the necessary site and catalog information for CMS services and directives for a given page.
     * It contains the following keys:
     * {@link resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID PAGE_CONTEXT_SITE_ID} for the site uid,
     * {@link resourceLocationsModule.object:PAGE_CONTEXT_CATALOG PAGE_CONTEXT_CATALOG} for the catalog uid,
     * {@link resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION PAGE_CONTEXT_CATALOG_VERSION} for the catalog version.
     */

    angular.module('resourceLocationsModule', [])

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONTEXT_SITE_ID
     *
     * @description
     * Constant containing the name of the site uid placeholder in URLs
     */
    .constant('CONTEXT_SITE_ID', CONTEXT_SITE_ID)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:CONTEXT_CATALOG
         *
         * @description
         * Constant containing the name of the catalog uid placeholder in URLs
         */
        .constant('CONTEXT_CATALOG', CONTEXT_CATALOG)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:CONTEXT_CATALOG_VERSION
         *
         * @description
         * Constant containing the name of the catalog version placeholder in URLs
         */
        .constant('CONTEXT_CATALOG_VERSION', CONTEXT_CATALOG_VERSION)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:PAGE_CONTEXT_SITE_ID
         *
         * @description
         * Constant containing the name of the current page site uid placeholder in URLs
         */
        .constant('PAGE_CONTEXT_SITE_ID', PAGE_CONTEXT_SITE_ID)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG
     *
     * @description
     * Constant containing the name of the current page catalog uid placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG', PAGE_CONTEXT_CATALOG)

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:PAGE_CONTEXT_CATALOG_VERSION
     *
     * @description
     * Constant containing the name of the current page catalog version placeholder in URLs
     */
    .constant('PAGE_CONTEXT_CATALOG_VERSION', PAGE_CONTEXT_CATALOG_VERSION)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:SMARTEDIT_ROOT
         *
         * @description
         * the name of the webapp root context
         */
        .constant('SMARTEDIT_ROOT', 'smartedit')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:SMARTEDIT_RESOURCE_URI_REGEXP
         *
         * @description
         * to calculate platform domain URI, this regular expression will be used
         */
        .constant('SMARTEDIT_RESOURCE_URI_REGEXP', /^(.*)\/smartedit/)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:CONFIGURATION_URI
         *
         * @description
         * the name of the SmartEdit configuration API root
         */
        .constant('CONFIGURATION_URI', '/smartedit/configuration/:key')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CONFIGURATION_COLLECTION_URI
     *
     * @description
     * The SmartEdit configuration collection API root
     */
    .constant('CONFIGURATION_COLLECTION_URI', '/smartedit/configuration')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:CMSWEBSERVICES_RESOURCE_URI
     *
     * @description
     * Constant for the cmswebservices API root
     */
    .constant('CMSWEBSERVICES_RESOURCE_URI', '/cmswebservices')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:DEFAULT_AUTHENTICATION_ENTRY_POINT
         *
         * @description
         * When configuration is not available yet to provide authenticationMap, one needs a default authentication entry point to access configuration API itself
         */
        .constant('DEFAULT_AUTHENTICATION_ENTRY_POINT', '/authorizationserver/oauth/token')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:DEFAULT_AUTHENTICATION_CLIENT_ID
         *
         * @description
         * The default OAuth 2 client id to use during authentication.
         */
        .constant('DEFAULT_AUTHENTICATION_CLIENT_ID', 'smartedit')
        /**
         * Root resource URI of i18n API 
         */
        .constant('I18N_ROOT_RESOURCE_URI', '/smarteditwebservices/v1/i18n')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:I18N_RESOURCE_URI
         *
         * @description
         * Resource URI to fetch the i18n initialization map for a given locale.
         */
        .constant('I18N_RESOURCE_URI', '/smarteditwebservices/v1/i18n/translations')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:I18N_LANGUAGE_RESOURCE_URI
         *
         * @description
         * Resource URI to fetch the supported i18n languages.
         */
        .constant('I18N_LANGUAGES_RESOURCE_URI', '/smarteditwebservices/v1/i18n/languages')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:LANGUAGE_RESOURCE_URI
         *
         * @description
         * Resource URI of the languages REST service.
         */
        .constant('LANGUAGE_RESOURCE_URI', '/cmswebservices/v1/sites/:siteUID/languages')
        .constant('PRODUCT_RESOURCE_API', '/cmssmarteditwebservices/v1/sites/:siteUID/products/:productUID')
        .constant('PRODUCT_LIST_RESOURCE_API', '/cmssmarteditwebservices/v1/productcatalogs/:catalogId/versions/:catalogVersion/products')
        .constant('PRODUCT_CATEGORY_RESOURCE_URI', '/cmssmarteditwebservices/v1/sites/:siteUID/categories/:categoryUID')
        .constant('PRODUCT_CATEGORY_SEARCH_RESOURCE_URI', '/cmssmarteditwebservices/v1/productcatalogs/:catalogId/versions/:catalogVersion/categories')

    /**
     * @ngdoc object
     * @name resourceLocationsModule.object:SITES_RESOURCE_URI
     *
     * @description
     * Resource URI of the sites REST service.
     */
    .constant('SITES_RESOURCE_URI', '/cmswebservices/v1/sites')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:LANDING_PAGE_PATH
         *
         * @description
         * Path of the landing page
         */
        .constant('LANDING_PAGE_PATH', '/')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:STOREFRONT_PATH
         *
         * @description
         * Path of the storefront
         */
        .constant('STOREFRONT_PATH', STORE_FRONT_CONTEXT_VAR + '/:siteId/:catalogId/:catalogVersion/')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:STOREFRONT_PATH_WITH_PAGE_ID
         *
         * @description
         * Path of the storefront with a page ID
         */
        .constant('STOREFRONT_PATH_WITH_PAGE_ID', STORE_FRONT_CONTEXT_VAR + '/:siteId/:catalogId/:catalogVersion/:pageId')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:STORE_FRONT_CONTEXT
         *
         * @description
         * to fetch the store front context for inflection points.
         */
        .constant('STORE_FRONT_CONTEXT', STORE_FRONT_CONTEXT_VAR)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:CATALOGS_PATH
         *
         * @description
         * Path of the catalogs
         */
        .constant('CATALOGS_PATH', '/cmswebservices/v1/catalogs/')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:MEDIA_PATH
         *
         * @description
         * Path of the media
         */
        .constant('MEDIA_PATH', '/cmswebservices/v1/media')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:CMSWEBSERVICES_PATH
         *
         * @description
         * Regular expression identifying CMS related URIs
         */
        .constant('CMSWEBSERVICES_PATH', /\/cmssmarteditwebservices|\/cmswebservices/)
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:PREVIEW_RESOURCE_URI
         *
         * @description
         * Path of the preview ticket API
         */
        .constant('PREVIEW_RESOURCE_URI', '/previewwebservices/v1/preview')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:ENUM_RESOURCE_URI
         *
         * @description
         * Path to fetch list of values of a given enum type
         */
        .constant('ENUM_RESOURCE_URI', "/cmswebservices/v1/enums")
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:PERMISSIONSWEBSERVICES_RESOURCE_URI
         *
         * @description
         * Path to fetch permissions of a given type
         */
        .constant('USER_GLOBAL_PERMISSIONS_RESOURCE_URI', "/permissionswebservices/v1/permissions/principals/:user/global")
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:SYNC_PATH
         *
         * @description
         * Path of the synchronization service
         */
        .constant('SYNC_PATH', '/cmswebservices/v1/catalogs/:catalog/versions/Staged/synchronizations/versions/Online')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:MEDIA_RESOURCE_URI
         *
         * @description
         * Resource URI of the media REST service.
         */
        .constant('MEDIA_RESOURCE_URI', '/cmswebservices/v1/catalogs/' + CONTEXT_CATALOG + '/versions/' + CONTEXT_CATALOG_VERSION + '/media')
        /**
         * @name resourceLocationsModule.object:TYPES_RESOURCE_URI
         *
         * @description
         * Resource URI of the component types REST service.
         */
        .constant('TYPES_RESOURCE_URI', '/cmswebservices/v1/types')
        /**
         * @ngdoc object
         * @name resourceLocationsModule.object:STRUCTURES_RESOURCE_URI
         *
         * @description
         * Resource URI of the component structures REST service.
         */
        .constant('STRUCTURES_RESOURCE_URI', '/cmssmarteditwebservices/v1/structures')

    /**
     * @ngdoc service
     * @name resourceLocationsModule.resourceLocationToRegex
     *
     * @description
     * Generates a regular expresssion matcher from a given resource location URL, replacing predefined keys by wildcard
     * matchers.
     *
     * Example:
     * <pre>
     *     // Get a regex matcher for the someResource endpoint, ie: /\/smarteditwebservices\/someResource\/.*$/g
     *     var endpointRegex = resourceLocationToRegex('/smarteditwebservices/someResource/:id');
     *
     *     // Use the regex to match hits to the mocked HTTP backend. This regex will match for any ID passed in to the
     *     // someResource endpoint.
     *     $httpBackend.whenGET(endpointRegex).respond({someKey: 'someValue'});
     * </pre>
     */
    .factory('resourceLocationToRegex', function() {
        return function(str) {
            return new RegExp(str.replace(/\/:[^\/]*/g, '/.*'));
        };
    });
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name restServiceFactoryModule
 *
 * @description
 * # The restServiceFactoryModule
 *
 * A module providing a factory used to generate a REST service wrapper for a given resource URL.
 */
angular.module('restServiceFactoryModule', ['ngResource', 'functionsModule'])
    /**
     * @ngdoc service
     * @name restServiceFactoryModule.service:restServiceFactory
     *
     * @description
     * A factory used to generate a REST service wrapper for a given resource URL, providing a means to perform HTTP
     * operations (GET, POST, etc) for the given resource.
     */
    .factory('restServiceFactory', ['$resource', '$q', 'hitch', 'copy', 'isBlank', function($resource, $q, hitch, copy, isBlank) {

        var headers = {
            'x-requested-with': 'Angular'
        };
        var nocacheheader = copy(headers);
        nocacheheader.Pragma = 'no-cache';

        var methodMap = {
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#getById
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Loads a component based on its identifier.
             *
             * @param {String} identifier The value of the object identifier.
             *
             * @returns {Object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} that rejects or resolves
             * to the single object returned by the call.
             */
            getById: {
                method: 'GET',
                params: {},
                isArray: false,
                cache: false,
                headers: nocacheheader
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#get
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Loads a unique component based on its identifier that must match the specified get parameters.
             * <br/>The REST Service Factory evaluates placeholders in the URI, if any are provided, to verify if they
             * match the search parameters.
             *
             * @param {object} searchParams The object that contains the query parameters, which are then mapped to the
             * query string
             *
             * @returns {object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} that rejects or resolves to the single object returned by the call.
             */
            get: {
                method: 'GET',
                params: {},
                isArray: false,
                cache: false,
                headers: nocacheheader
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#query
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Loads a list of components that match the specified search parameters.
             * <br/>The REST service evaluates the placeholders in the URI, if any are provided, to verify if
             * they match the search parameters.
             *
             * @param {object} searchParams The object that contains the query parameters, which are then mapped to the
             * query string
             *
             * @returns {object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} A promise that rejects
             * or resolves to the list of objects returned by the call.
             */
            query: {
                method: 'GET',
                params: {},
                isArray: true,
                cache: false,
                headers: nocacheheader
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#page
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             * @description
             * To be called instead of {@link restServiceFactoryModule.service:ResourceWrapper#query query} when the list is wrapped by server in an object (ex: Page).
             * <br/>The service will evaluate any placeholder in the URI with matching search params.
             * @param {object} searchParams The object that contains the query parameters, which are then mapped to the
             * query string
             * @returns {object} a {@link https://docs.angularjs.org/api/ng/service/$q promise} rejecting or resolving to the page object returned by the call.
             */
            page: {
                method: 'GET',
                params: {},
                isArray: false, // due to spring Page
                cache: false,
                headers: nocacheheader,
                transformResponse: function(data, headersGetter) {
                    var deserialized = !isBlank(data) ? angular.fromJson(data) : {};
                    deserialized.headers = headersGetter();
                    return deserialized;
                }
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#update
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Updates a component.  It appends the value of the identifier to the URI.
             *
             * <br/>The REST service warpper evaluates the placeholders in the URI, if any are provided, to verify if
             * they match the search parameters.
             *
             * @param {object} payload The object to be updated. <br/>The promise will be rejected if the payload does not contain the identifier.
             *
             * @returns {object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} A promise that rejects or resolves to what the server returns.
             */
            update: {
                method: 'PUT',
                cache: false,
                headers: headers
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#save
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Saves a component. It appends the value of the identifier to the URI.
             *
             * <br/>The REST service wrapper evaluates the placeholders in the URI, if any are provided, to verify if
             * they match the search parameters.
             *
             * @param {object} payload The object to be saved.
             * <br/>The promise will be rejected if the payload does not contain the identifier.
             *
             * @returns {object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} that rejects or resolves
             * to what the server returns.
             */
            save: {
                method: 'POST',
                cache: false,
                headers: headers
            },
            /**
             * @ngdoc service
             * @name restServiceFactoryModule.service:ResourceWrapper#remove
             * @methodOf restServiceFactoryModule.service:ResourceWrapper
             *
             * @description
             * Deletes a component. It appends the value of the identifier to the URI.
             *
             * <br/>The REST service wrapper evaluates the placeholders in the URI, if any are provided, to verify if
             * they match the search parameters.
             *
             * @param {object} payload The object to be updated.
             * <br/>The promise will be rejected if the payload does not contain the identifier.
             *
             * @returns {object} A {@link https://docs.angularjs.org/api/ng/service/$q promise} that rejects or resolves
             * to what the server returns.
             */
            remove: {
                method: 'DELETE',
                cache: false,
                headers: headers
            }
        };

        var serviceMap = {};
        var IDENTIFIER = "identifier";
        var DOMAIN = null;

        var wrapMethod = function(method, initialURI, payload) {
            if (payload === undefined) {
                payload = {};
            }

            var deferred = $q.defer();

            //only keep params to be found in the URI or query params
            var params = typeof payload === 'object' ? Object.keys(payload).reduce(hitch(this, function(prev, next) {
                if (new RegExp(":" + next + "\/").test(this.uri) || new RegExp(":" + next + "$").test(this.uri) || new RegExp(":" + next + "&").test(this.uri)) {
                    prev[next] = payload[next];
                }
                return prev;
            }), {}) : {};

            if (method === 'getById') {
                //payload is the actual identifier value
                var identifierValue = payload;
                payload = {};
                payload[this.IDENTIFIER] = identifierValue;
            }
            if (method === 'update' || method === 'remove') {
                if (!payload[this.IDENTIFIER]) {
                    deferred.reject("no data was found under the " + this.IDENTIFIER + " field of object " + JSON.stringify(payload) + ", it is necessary for update and remove operations");
                    return deferred.promise;
                }
                params[this.IDENTIFIER] = payload[this.IDENTIFIER];
                if (method === 'remove') {
                    payload = {};
                }
            }
            if (method === 'get' || method === 'getById' || method === 'query' || method === 'page') {
                //params and payload are inverted
                params = copy(payload);
                payload = {};
            }
            this[method](params, payload).$promise.then(
                function(success) {
                    deferred.resolve(success);
                },
                function(error) {
                    deferred.reject(error);
                }
            );
            return deferred.promise;
        };

        return {

            IDENTIFIER: function() {
                return IDENTIFIER;
            },
            /**
             * @ngdoc method
             * @name restServiceFactoryModule.service:restServiceFactory#setDomain
             * @methodOf restServiceFactoryModule.service:restServiceFactory
             *
             * @description
             * When working with multiple services that reference the same domain, it is best to only specify relative
             * paths for the services and specify the context-wide domain in a separate location. The {@link
             * restServiceFactoryModule.service:restServiceFactory#get get} method of the {@link
             * restServiceFactoryModule.service:restServiceFactory restServiceFactory} will then prefix the specified service
             * URIs with the domain and a forward slash.
             *
             * @param {String} domain The context-wide domain that all URIs will be prefixed with when services are
             * created/when a service is created
             */
            setDomain: function(domain) {
                DOMAIN = domain;
            },
            getDomain: function() {
                return DOMAIN;
            },


            /**
             * @ngdoc method
             * @name restServiceFactoryModule.service:restServiceFactory#get
             * @methodOf restServiceFactoryModule.service:restServiceFactory
             *
             * @description
             * A factory method used to create a REST service that points to the given resource URI. The returned
             * service wraps a $resource object. As opposed to a $resource, the REST services retrieved from the
             * restServiceFactory can only take one object argument. The object argument will automatically be split
             * into a parameter object and a payload object before they are delegated to the wrapped $resource object.
             * If the domain is set, the domain is prepended to the given URI.
             *
             * @param {String} uri The URI of the REST service to be retrieved.
             * @param {String} identifier An optional parameter. The name of the placeholder that is appended to the end
             * of the URI if the name is not already provided as part of the URI. The default value is "identifier".
             * <pre>
             * 	if identifier is "resourceId" and uri is "resource/:resourceId/someValue", the target URI will remain the same.
             * 	if identifier is "resourceId" and uri is "resource", the target URI will be "resource/:resourceId".
             * </pre>
             *
             * @returns {ResourceWrapper} A {@link restServiceFactoryModule.service:ResourceWrapper wrapper} around a {@link https://docs.angularjs.org/api/ngResource/service/$resource $resource}
             */
            get: function(uri, identifier) {

                if (!identifier) {
                    identifier = IDENTIFIER;
                }
                var service = serviceMap[uri + identifier];

                if (typeof service !== 'undefined') {
                    return service;
                } else {
                    var initialURI = '';
                    if (/^https?\:\/\//.test(uri) || /^\//.test(uri)) {
                        initialURI = uri;
                    } else {
                        initialURI = isBlank(DOMAIN) ? uri : DOMAIN + '/' + uri;
                    }

                    var finalURI = initialURI;
                    if (finalURI.indexOf(':' + identifier) === -1) {
                        finalURI += '/:' + identifier;
                    }

                    service = $resource(finalURI, {}, methodMap);

                    var wrapper = {};

                    ['save', 'remove'].concat(Object.keys(methodMap)).forEach(function(methodName) {
                        wrapper[methodName] = hitch(service, wrapMethod, methodName, initialURI);
                    });

                    serviceMap[uri + identifier] = wrapper;
                    service.uri = finalURI;
                    service.IDENTIFIER = identifier;

                    /**
                     * @ngdoc service
                     * @name restServiceFactoryModule.service:ResourceWrapper
                     *
                     * @description
                     * The ResourceWrapper is service used to make REST calls to the wrapped target URI. It is created
                     * by the {@link restServiceFactoryModule.service:restServiceFactory#methods_get get} method of the {@link
                     * restServiceFactoryModule.service:restServiceFactory restServiceFactory}. Each resource wrapper
                     * method returns a {@link https://docs.angularjs.org/api/ng/service/$q promise}.
                     */
                    return wrapper;
                }
            }

        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("itemPrinterModule", [])
    .component('itemPrinter', {
        transclude: false,
        replace: false,
        template: '<div ng-include="printer.templateUrl"></div>',
        require: {
            ySelect: '^ySelect'
        },
        controller: ['$scope', function($scope) {
            $scope.selected = true;
            this.$onChanges = function() {
                /* needs to bind it scope and not controller in order for the templates required by API
                 * to be agnostic of whether they are invoked within ui-select-coices or ui-select-match of ui-select
                 */
                $scope.item = this.model;
                $scope.ySelect = this.ySelect;
            };
        }],
        controllerAs: 'printer',
        bindings: {
            templateUrl: "<",
            model: "<"
        },
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name ySelectModule
 * @description
 * # The ySelectModule
 *
 */
angular.module('ySelectModule', [
    'compileHtmlModule',
    'functionsModule',
    'yInfiniteScrollingModule',
    'itemPrinterModule',
    'ui.select',
    'ngSanitize',
    'coretemplates',
    'yActionableSearchItemModule',
    'l10nModule'
])

.run(['$templateCache', function($templateCache) {

    //use a copy of select2
    $templateCache.put("pagedSelect2/match-multiple.tpl.html", $templateCache.get("select2/match-multiple.tpl.html"));
    $templateCache.put("pagedSelect2/match.tpl.html", $templateCache.get("select2/match.tpl.html"));
    $templateCache.put("pagedSelect2/no-choice.tpl.html", $templateCache.get("select2/no-choice.tpl.html"));
    $templateCache.put("pagedSelect2/select-multiple.tpl.html", $templateCache.get("select2/select-multiple.tpl.html"));
    $templateCache.put("pagedSelect2/select.tpl.html", $templateCache.get("select2/select.tpl.html"));

    //our own flavor of select2 for paging that makes use of yInfiniteScrolling component
    $templateCache.put("pagedSelect2/choices.tpl.html", $templateCache.get("uiSelectPagedChoicesTemplate.html"));
    $templateCache.put("select2/choices.tpl.html", $templateCache.get("uiSelectChoicesTemplate.html"));
}])

.controller('ySelectController', ['$q', '$log', 'lodash', '$attrs', '$templateCache', function($q, $log, lodash, $attrs, $templateCache) {

    // Initialization
    this.$onInit = function() {

        // this.items represent the options available in the control to choose from.
        // this.model represents the item(s) currently selected in the control. If the control is using the multiSelect
        // flag then the model is an array; otherwise it's a single object.
        this.items = [];
        this.searchEnabled = this.searchEnabled === false ? false : true;

        //in order to propagate down changes to ngModel from the parent controller
        this.exposedModel.$viewChangeListeners.push(this.syncModels);
        this.exposedModel.$render = this.syncModels;

        this.reset = function(forceReset) {
            this.items.length = 0;
            if (forceReset) {
                this.resetModel();
            }

            return this.$onChanges();
        }.bind(this);
    };

    // Basic Functions
    this.clear = function($select, $event) {
        $event.preventDefault();
        $event.stopPropagation();
        delete this.model;
        this.internalOnChange();
    }.bind(this);

    this.resultsHeaderLabel = "se.yselect.options.inactiveoption.label";

    this.showResultHeader = function() {
        return this.searchEnabled && this.items && this.items.length > 0;
    };

    this.getActionableTemplateUrl = function() {
        return this.actionableSearchItemTemplateConfig ? this.actionableSearchItemTemplateConfig.templateUrl : "";
    }.bind(this);

    //in case of paged dropdown, the triggering of refresh is handled by yInfiniteScrolling component part of the pagedSelect2/choices.tpl.html template
    this.refreshOptions = function(mask) {
        if (this.fetchStrategy.fetchAll) {
            this.fetchStrategy.fetchAll(mask).then(function(items) {
                this.items = items;
            }.bind(this));
        }
    }.bind(this);

    // Event Listeners

    /*
     * This function is called whenever the value in the ui-select changes from an external source (e.g., like
     * the user making a selection).
     * NOTE: This is not triggered if the model is changed programatically.
     */
    this.syncModels = function() {
        this.model = this.exposedModel.$modelValue;
        this.$onChanges();
    }.bind(this);

    this.$onChanges = function(changes) {
        this.isValidConfiguration();
        this.updateControlTemplate();

        /* we must initialize the list to contain at least the selected item
         * if a fetchEntity has been provided, it will be used
         * if no fetchEntity was provided, we resort to finding a match in the result from fetchAll
         * if we fail to find a match, the directive throws an error to notify that a fetchEntity is required
         */
        var result = $q.when(null);
        if (!this.items || this.items.length === 0) {
            if (!this.isPagedDropdown()) {
                result = this.internalFetchAll();
            } else if (this.fetchStrategy.fetchEntity || this.fetchStrategy.fetchEntities) {
                if (!this.isModelEmpty()) {
                    result = this.internalFetchEntities();
                }
            } else {
                throw "could not initialize dropdown of ySelect, neither fetchEntity, fetchEntities, nor fetchAll were specified";
            }
        }

        if (changes && changes.fetchStrategy) {
            this._updateChild();
        }
        return result;
    };

    /*
     * This method is used to propagate to the parent controller the changes made to the model programatically inside
     * this component.
     */
    this.internalOnChange = function() {
        //in order to propagate up changes to ngModel into parent controller
        this.exposedModel.$setViewValue(this.model);
        if (this.onChange) {
            this.onChange();
        }
    }.bind(this);

    // Items retrieval
    this.internalFetchAll = function() {

        return this.fetchStrategy.fetchAll().then(function(items) {
            this.items = items;

            if (this.model) {
                var result;

                if (this.multiSelect) {
                    result = lodash.every(this.model, function(modelKey) {
                        return lodash.find(this.items, function(item) {
                            return item.id === modelKey;
                        });
                    }.bind(this));
                } else {
                    result = items.find(function(item) {
                        return item.id === this.model;
                    }.bind(this));
                }

                if (!result) {
                    $log.debug("[ySelect - " + this.id + "] fetchAll was used to fetch the option identified by " + this.model + " but failed to find a match");
                }

                this.updateModelIfNecessary();
            }

            this.internalOnChange();
        }.bind(this));
    };

    this.internalFetchEntities = function() {
        var promise;
        if (!this.multiSelect) {
            promise = this.fetchEntity(this.model).then(function(item) {
                return [item];
            });
        } else {
            if (this.fetchStrategy.fetchEntities) {
                promise = this.fetchStrategy.fetchEntities(this.model).then(function(items) {
                    if (items.length !== this.model.length) {
                        $log.debug("!fetchEntities was used to fetch the options identified by " + this.model + " but failed to find all matches");
                    }

                    return items;
                }.bind(this));
            } else {
                var promiseArray = [];
                this.model.forEach(function(entryId) {
                    promiseArray.push(this.fetchEntity(entryId));
                }.bind(this));

                promise = $q.all(promiseArray);
            }
        }

        return promise.then(function(result) {
            this.items = result.filter(function(item) {
                return item !== null;
            });

            this.updateModelIfNecessary();

            this.internalOnChange();
        }.bind(this));
    };

    this.fetchEntity = function(entryId) {
        return this.fetchStrategy.fetchEntity(entryId).then(function(item) {
            if (!item) {
                $log.debug("fetchEntity was used to fetch the option identified by " + item + " but failed to find a match");
            }

            return item;
        });
    };

    this.updateModelIfNecessary = function() {
        if (!this.keepModelOnReset) {
            if (this.multiSelect) {
                this.model = this.model.filter(function(modelKey) {
                    return lodash.find(this.items, function(item) {
                        return item && (item.id === modelKey);
                    });
                }.bind(this));

            } else {
                var result = this.items.filter(function(item) {
                    return item.id === this.model;
                }.bind(this));
                this.model = (result.length > 0) ? this.model : null;
            }
        }
    };

    // Helper functions
    this.isValidConfiguration = function() {
        if (!this.fetchStrategy.fetchAll && !this.fetchStrategy.fetchPage) {
            throw "neither fetchAll nor fetchPage have been specified in fetchStrategy";
        }
        if (this.fetchStrategy.fetchAll && this.fetchStrategy.fetchPage) {
            throw "only one of either fetchAll or fetchPage must be specified in fetchStrategy";
        }
        if (this.fetchStrategy.fetchPage && this.model && !this.fetchStrategy.fetchEntity && !this.fetchStrategy.fetchEntities) {
            throw "fetchPage has been specified in fetchStrategy but neither fetchEntity nor fetchEntities are available to load item identified by " + this.model;
        }

        if (this.isPagedDropdown() && !this.keepModelOnReset) {
            $log.debug('current ySelect is paged, so keepModelOnReset flag is ignored (it will always keep the model on reset).');
        }
    };

    this.updateControlTemplate = function() {
        this.theme = this.isPagedDropdown() ? "pagedSelect2" : "select2";
        this.itemTemplate = this.itemTemplate || 'defaultItemTemplate.html';
    };

    this.requiresPaginatedStyling = function() {
        return (this.isPagedDropdown() || this.hasControls());
    };

    this.isPagedDropdown = function() {
        return !!this.fetchStrategy.fetchPage;
    };

    this.hasControls = function() {
        return (this.controls || false) === true;
    };

    this.isModelEmpty = function() {
        if (this.multiSelect) {
            return !this.model || (this.model && this.model.length === 0);
        } else {
            return !this.model;
        }
    };

    this.resetModel = function() {
        if (this.multiSelect) {
            this.model.length = 0;
        } else {
            delete this.model;
        }
    };

    this.disableChoice = function(item) {
        if (this.disableChoiceFn) {
            return this.disableChoiceFn(item);
        }
        return false;
    }.bind(this);

    this._updateChild = function() {
        var ySelectTemplate = 'ySelectTemplate.html';
        var yMultiSelectTemplate = 'yMultiSelectTemplate.html';

        var theme = ((!this.fetchStrategy.fetchPage) ? "select2" : "pagedSelect2");
        var selectedFilters = ((!this.fetchStrategy.fetchPage) ?
            'repeat="item.id as item in ySelect.items | filter: $select.search" refresh="ySelect.refreshOptions($select.search)"' : 'repeat="item.id as item in ySelect.items"'
        );

        var rawTemplate = $templateCache.get(($attrs.multiSelect && this.multiSelect) ? yMultiSelectTemplate : ySelectTemplate);
        this.result = rawTemplate.replace('<%= theme %>', theme).replace('"<%= filtering %>"', selectedFilters);
    };

}])

/**
 * @ngdoc directive
 * @name ySelectModule.directive:ySelect
 * @scope
 * @restrict E
 * @element y-select
 *
 * @description
 * This component is a wrapper around ui-select directive and provides filtering capabilities for the dropdown menu that is customizable with an item template.
 * <br/>ySelect can work in both paged and non paged mode: providing either fetchAll or fetchPage function in the fetchStrategy will determine the flavour of the dropdown.
 *
 * 
 * @param {@String=} id will be used to identify internal elements of ySelect for styling (and testing) purposes.
 * @param {<boolean=} controls Adds controls such as the magnifier and the remove button. Default is set to false.
 * @param {<Object} fetchStrategy strategy object containing the necessary functions for ySelect to populate the dropdown:
 * <b>Only one of either fetchAll or fetchPage must be defined.</b>
 * @param {<Function} fetchStrategy.fetchAll Function required to fetch all for a given optional mask.
 * fetchAll will be called without arguments upon initialization and with a mask every time the search section receives an input.
 * It must return a promise resolving to a list of items.
 * Every item must have a property "id" used for identification. If no itemTemplate is provided, these items will need to display a "label" property.
 * @param {<Function} fetchStrategy.fetchPage Function required to fetch a page for a given optional mask.
 * fetchPage must fulfill the contract of fetchPage from {@link yInfiniteScrollingModule.directive:yInfiniteScrolling yInfiniteScrolling}
 * It must return a promise resolving to a page of items as per {@link Page.object:Page Page}.
 * Every item must have a property "id" used for identification. If no itemTemplate is provided, these items will need to display a "label" property.
 * @param {<Function} fetchStrategy.fetchEntity Function to fetch an option by its identifier when we are in paged mode (fetchPage is defined) and the dropdown is initialized with a value.
 * @param {<Function=} disableChoiceFn A function to disable results in the drop-down. It is invoked for each item in the drop-down, with a single parameter, the item itself.
 * @param {<String=} placeholder the placeholder label or i18nKey that will be printed in the search section.
 * @param {<String=} itemTemplate the path to the template that will be used to display items in both the dropdown menu and the selection.
 * ItemTemplate has access to item, selected and the ySelect controller.
 * item is the item to print, selected is a boolean that is true when the template is used in the selection as opposed to the dropdown menu.
 * Default template will be:
 * <pre>
 * <span data-ng-bind-html="item.label | translate"></span>
 * </pre>
 * @param {<boolean=} keepModelOnResetFor. a non-paged dropdown: if the value is set to false, the widget will remove the selected entities in the model that no longer match the values available on the server. 
 * For a paged dropdown: After a standard reset, even if keepModelOnReset is set to false,  the widget will not be able to remove the selected entities in the model 
 * that no longer match the values available on the server. to force the widget to remove any selected entities, you must call reset(true).
 * @param {<boolean=} multiSelect The property specifies whether ySelect is multi-selectable.
 * @param {=Function=} reset A function that will be called when ySelect is reset.
 * @param {<boolean=} isReadOnly renders ySelect as disabled field.
 * @param {<String=} resultsHeaderTemplate the template that will be used on top of the result list.
 * @param {<String=} resultsHeaderTemplateUrl the path to the template what will be used on top of the result list.
 * @param {<String=} resultsHeaderLabel the label that will be displayed on top of the result list.
 * Only one of resultsHeaderTemplate, resultsHeaderTemplateUtl, and resultsHeaderLabel shall be passed.
 */
.directive('ySelect', function() {
    return {
        template: "<div data-compile-html='ySelect.result'></div>",
        transclude: true,
        controller: 'ySelectController',
        controllerAs: 'ySelect',
        require: {
            exposedModel: 'ngModel'
        },
        scope: {
            id: '@?',
            fetchStrategy: '<',
            onChange: '<?',
            controls: '<?',
            multiSelect: '<?',
            keepModelOnReset: '<?',
            reset: '=?',
            isReadOnly: '<?',
            resultsHeaderTemplate: '<?',
            resultsHeaderTemplateUrl: '<?',
            resultsHeaderLabel: '<?',
            disableChoiceFn: '<?',
            placeholder: '<?',
            itemTemplate: '<?',
            searchEnabled: '<?'
        },
        bindToController: true
    };
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name yActionableSearchItemModule
 * @requires eventServiceModule
 * @description
 * This module defines the {@link yActionableSearchItemModule.directive:yActionableSearchItem yActionableSearchItem} component
 **/
angular.module("yActionableSearchItemModule", ['eventServiceModule'])


.controller('yActionableSearchItemController', ['$scope', 'systemEventService', function($scope, systemEventService) {

    var defaultEventId = 'yActionableSearchItem_ACTION_CREATE';
    var defaultActionText = 'se.yationablesearchitem.action.create';

    this.getActionText = function() {
        return this.actionText || defaultActionText;
    }.bind(this);

    this.showForm = function() {
        return this.uiSelect && this.uiSelect.search && this.uiSelect.search.length > 0;
    }.bind(this);

    this.getInputText = function() {
        return this.uiSelect.search;
    };

    this.buttonPressed = function() {
        var evtId = this.eventId || defaultEventId;
        systemEventService.sendEvent(evtId, this.uiSelect.search || "");
        this.uiSelect.close();
    }.bind(this);

}])

/**
 * @ngdoc directive
 * @name yActionableSearchItemModule.directive:yActionableSearchItem
 * @restrict E
 * @scope
 * @param {@String=} [eventId='yActionableSearchItem_ACTION_CREATE'] The event ID that is triggered on the
 * systemEventService when the button is pressed
 * @param {@String=} [actionText='se.yationablesearchitem.action.create'] The i18n key label for the button
 * @description
 * The yActionableSearchItem Angular component is designed to work with the ySelect drop down. It allows you to add
 * a button in the resultsHeader aread of the ySelect's drop-down, and trigger a user-defined action when pressed.
 */
.component('yActionableSearchItem', {
    templateUrl: 'yActionableSearchItemTemplate.html',
    require: {
        ySelect: '^ySelect',
        uiSelect: '^uiSelect'
    },
    controller: 'yActionableSearchItemController',
    bindings: {
        eventId: '@?',
        actionText: '@?'
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name sliderPanelModule
 * @requires sliderPanelServiceModule
 * @requires ngAnimate
 * @description
 * This module defines the slider panel Angular component and its associated constants and controller.
 *
 * ## Basic Implementation
 *
 * To define a new slider panel, you must make some basic modifications to your Angular module and controller, as well
 * as to your HTML template.
 *
 * - ### Angular Module
 *
 * You must add the sliderPanelModule as a dependency to your Angular module.
 *
 * <pre>
 angular.module('yourApp', ['sliderPanelModule']) { ... }
 * </pre>
 *
 * - ### Angular Controller
 *
 * Within the Angular controller, you must add a function to be instantiated so that the controller will trigger the
 * display of the slider panel.
 * <pre>
 angular.module('yourApp', ['sliderPanelModule'])
 .controller('yourController', function() {
     ...
     this.showSliderPanel = function() {};
     ...
 });
 * </pre>
 *
 * - ### HTML template
 *
 * To include HTML content in the slider panel, you must embed the HTML content in a `<y-slider-panel> </y-slider-panel>` tag.<br />
 * For more information, see the definition of {@link sliderPanelModule.directive:ySliderPanel ySliderPanel} Angular component.
 *
 * <pre>
 <y-slider-panel data-slider-panel-show="$ctrl.sliderPanelShow">
    <content>
        any HTML content
    </content>
 </y-slider-panel>
 * </pre>
 *
 * You can then make the slider panel visible by calling the "Show Slider Panel" function defined in the associated controller; for example:
 *
 * <pre>
 <button class="btn btn-default" ng-click="$ctrl.sliderPanelShow();">
    Show Slider Panel
 </button>
 * </pre>
 *
 * ## Advanced Configurations
 *
 * A default set of configurations is applied to all slider panels. You can overwrite and update the default configuration.
 *
 * To update the configuration of a specific slider panel, you must instantiate a JSON object that contains the expected
 * configuration in the Angular controller and provide it to the slider panel controller using the HTML template, for example:
 *
 * <pre>
 <y-slider-panel ... data-slider-panel-configuration="$ctrl.sliderPanelConfiguration">
 * </pre>
 *
 * If you define this type of configuration set, SmartEdit will automatically merge it with the slider panel's default configuration.
 * For information about the available settings, see the {@link sliderPanelServiceModule.service:sliderPanelService#methods_getNewServiceInstance getNewServiceInstance} method.
 */
angular.module('sliderPanelModule', [
    'ngAnimate',
    'sliderPanelServiceModule',
    'yjqueryModule',
    'yLoDashModule'
])

/**
 * @ngdoc object
 * @name sliderPanelModule.object:CSS_CLASSNAMES
 * @description
 * This object defines injectable Angular constants that store the CSS class names used in the controller to define the
 * rendering and animation of the slider panel.
 */
.constant("CSS_CLASSNAMES", {

    /**
     * @ngdoc property
     * @name SLIDERPANEL_ANIMATED {String}
     * @propertyOf sliderPanelModule.object:CSS_CLASSNAMES
     *
     * @description
     * The class name applied to the slide panel container to trigger the sliding action in the CSS animation.
     **/
    SLIDERPANEL_ANIMATED: "sliderpanel--animated",

    /**
     * @ngdoc property
     * @name SLIDERPANEL_SLIDEPREFIX {String}
     * @propertyOf sliderPanelModule.object:CSS_CLASSNAMES
     *
     * @description
     * A common prefix for the class names that defines how the content of the slider panel is to be rendered.
     **/
    SLIDERPANEL_SLIDEPREFIX: "sliderpanel--slidefrom"
})

.controller('sliderPanelController', ['$animate', '$element', '$scope', '$window', 'CSS_CLASSNAMES', 'yjQuery', 'lodash', 'sliderPanelServiceFactory', function(
    $animate,
    $element,
    $scope,
    $window,
    CSS_CLASSNAMES,
    yjQuery,
    lodash,
    sliderPanelServiceFactory
) {

    var self = this;
    var sliderPanelService = {};

    var addScreenResizeEventHandler = function() {
        yjQuery(window).on("resize.sliderPanelRedraw_" + self.uniqueId, function() {

            if (self.isShown) {
                $scope.$apply(function() {
                    sliderPanelService.updateContainerInlineStyling(true);
                    self.inlineStyling.container = sliderPanelService.inlineStyling.container;
                });
            }
        });
    };

    this.isSaveDisabled = function() {
        if (this.sliderPanelConfiguration.modal &&
            this.sliderPanelConfiguration.modal.save &&
            this.sliderPanelConfiguration.modal.save.isDisabledFn) {
            return this.sliderPanelConfiguration.modal.save.isDisabledFn();
        }
        return false;
    }.bind(this);

    this.$onInit = function() {

        this.isShown = false;
        this.uniqueId = lodash.uniqueId();

        // setting new instance of slider panel service
        sliderPanelService = sliderPanelServiceFactory.getNewServiceInstance($element, $window, this.sliderPanelConfiguration);

        // variables made available on the html template
        this.sliderPanelConfiguration = sliderPanelService.sliderPanelConfiguration;

        this.slideClassName = CSS_CLASSNAMES.SLIDERPANEL_SLIDEPREFIX + this.sliderPanelConfiguration.slideFrom;
        this.inlineStyling = {
            container: sliderPanelService.inlineStyling.container,
            content: sliderPanelService.inlineStyling.content
        };
        this.sliderPanelShow = this.showSlider;
        this.sliderPanelHide = this.hideSlider;

        this.sliderPanelDismissAction = this.sliderPanelConfiguration.modal &&
            this.sliderPanelConfiguration.modal.dismiss &&
            this.sliderPanelConfiguration.modal.dismiss.onClick ? this.sliderPanelConfiguration.modal.dismiss.onClick : this.hideSlider;

        // applying event handler for screen resize
        addScreenResizeEventHandler();

        if (this.sliderPanelConfiguration.displayedByDefault) {
            this.showSlider();
        }

    };

    this.$onDestroy = function() {
        yjQuery(window).off("resize.doResize");
    };

    this.showSlider = function() {

        // container inline styling
        sliderPanelService.updateContainerInlineStyling(false);
        self.inlineStyling.container = sliderPanelService.inlineStyling.container;

        // container greyed out overlay
        var isSecondarySliderPanel = false;
        angular.forEach(angular.element("y-slider-panel.sliderpanel--animated .sliderpanel-container"), function(sliderPanelContainer) {
            sliderPanelContainer = angular.element(sliderPanelContainer);
            if (!isSecondarySliderPanel) {
                if (sliderPanelContainer.css("height") === self.inlineStyling.container.height &&
                    sliderPanelContainer.css("width") === self.inlineStyling.container.width &&
                    sliderPanelContainer.css("left") === self.inlineStyling.container.left &&
                    sliderPanelContainer.css("top") === self.inlineStyling.container.top) {
                    isSecondarySliderPanel = true;
                }
            }
        });

        // if no related configuration has been set, the no greyed out overlay is set to true for all secondary slider panels.
        self.sliderPanelConfiguration.noGreyedOutOverlay = (typeof self.sliderPanelConfiguration.noGreyedOutOverlay === "boolean") ?
            self.sliderPanelConfiguration.noGreyedOutOverlay :
            isSecondarySliderPanel;

        // triggering slider panel display
        self.isShown = true;
        return $animate.addClass($element, CSS_CLASSNAMES.SLIDERPANEL_ANIMATED);

    };

    this.hideSlider = function() {
        return $animate.removeClass($element, CSS_CLASSNAMES.SLIDERPANEL_ANIMATED).then(function() {
            self.isShown = false;
        });
    };

}])

/**
 * @ngdoc directive
 * @name sliderPanelModule.directive:ySliderPanel
 * @restrict E
 * @param {Object} dataSliderPanelConfiguration (optional) A JSON object containing the configuration to be applied on slider panel.
 * @param {Function} dataSliderPanelHide (optional) A function shared in a two ways binding by the main controller and the slider panel and used to trigger the hiding of the slider panel.
 * @param {Function} dataSliderPanelShow A function shared in a two ways binding by the main controller and the slider panel and used to trigger the display of the slider panel.
 * @description
 * The ySliderPanel Angular component allows for the dynamic display of any HTML content on a sliding panel.
 */
.component('ySliderPanel', {
    templateUrl: 'sliderPanelTemplate.html',
    controller: 'sliderPanelController',
    controllerAs: '$sliderPanelCtrl',
    transclude: true,
    bindings: {
        sliderPanelConfiguration: '<?',
        sliderPanelHide: '=?',
        sliderPanelShow: '='
    }
});

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name sliderPanelServiceModule
 * @description
 * This module provides a service to initialize and control the rendering of the {@link sliderPanelModule.directive:ySliderPanel ySliderPanel} Angular component.
 */
angular.module("sliderPanelServiceModule", ['yLoDashModule'])

.factory("sliderPanelServiceFactory", ['lodash', function(lodash) {

    /**
     * @ngdoc service
     * @name sliderPanelServiceModule.service:sliderPanelService
     * @description
     * The sliderPanelService handles the initialization and the rendering of the {@link sliderPanelModule.directive:ySliderPanel ySliderPanel} Angular component.
     */
    function sliderPanelService(element, window, configuration) {

        /* ----------
         [ variables ]
         ---------- */

        var sliderPanelDefaultConfiguration = {
            slideFrom: "right",
            overlayDimension: "80%"
        };

        var returningHigherZIndex = function() {

            var highestZIndex = 0;
            var zIndex = 0;

            var browseNodeChildren = function(node) {
                angular.forEach(angular.element(node).children(),
                    function(child) {
                        processBranchOrLeaf(child);
                    }
                );
            };

            var checkZIndexValue = function(node) {
                zIndex = angular.element(node).css("z-index");
                if (!isNaN(zIndex) && zIndex > highestZIndex) {
                    highestZIndex = zIndex;
                }
            };

            var processBranchOrLeaf = function(node) {
                // we are only processing the UI tags
                if (["SCRIPT", "lINK", "BASE"].indexOf(angular.element(node)[0].nodeName) === -1) {
                    checkZIndexValue(node);
                    if (angular.element(node).children().length > 0) {
                        browseNodeChildren(node);
                    }
                }

            };

            processBranchOrLeaf(angular.element("body"));

            return parseInt(highestZIndex);

        };

        /**
         * @ngdoc method
         * @name sliderPanelServiceModule.service:sliderPanelService#updateContainerInlineStyling
         * @methodOf sliderPanelServiceModule.service:sliderPanelService
         * @param {Boolean} screenResized Sets whether the update is triggered by a screen resize
         * @description
         * This method sets the inline styling applied to the slider panel container according to the dimension and position values
         * of the parent element.
         */
        this.updateContainerInlineStyling = function(screenResized) {

            var parentClientRect = parent[0].getBoundingClientRect(),
                borders = {
                    left: (parent.css("border-left-width") ? parseInt(parent.css("border-left-width").replace("px", "")) : 0),
                    top: (parent.css("border-top-width") ? parseInt(parent.css("border-top-width").replace("px", "")) : 0)

                };

            this.inlineStyling.container.height = parent[0].clientHeight + "px";
            this.inlineStyling.container.width = parent[0].clientWidth + "px";
            this.inlineStyling.container.left = ((appendChildTarget.nodeName === "BODY") ? Math.round(parentClientRect.left + window.pageXOffset + borders.left) : 0) + "px";
            this.inlineStyling.container.top = ((appendChildTarget.nodeName === "BODY") ? Math.round(parentClientRect.top + window.pageYOffset + borders.top) : 0) + "px";

            // z-index value is not set during screen resize
            if (!screenResized) {
                this.inlineStyling.container.zIndex = (this.sliderPanelConfiguration.zIndex) ? parseInt(this.sliderPanelConfiguration.zIndex) : (returningHigherZIndex() + 1);
            }

        }.bind(this);

        /* ---------------
        [ initialization ]
         -------------- */

        // defining the configuration set on the processed slider panel by merging the JSON object provided as parameter
        // with the default configuration
        this.sliderPanelConfiguration = lodash.defaultsDeep(
            configuration,
            sliderPanelDefaultConfiguration
        );

        // instantiating "parent" local variable
        var parentRawElement = (this.sliderPanelConfiguration.cssSelector) ? document.querySelector(this.sliderPanelConfiguration.cssSelector) : null;
        if (!parentRawElement) {
            parentRawElement = element.parent();
        }

        var parent = angular.element(parentRawElement);

        // instantiating "appendChildTarget" local variable
        for (var testedElement = parent, modalFound = false, i = 0; testedElement[0].nodeName !== "BODY"; testedElement = angular.element(testedElement.parent()), i++) {
            if (testedElement[0].getAttribute("id") === "y-modal-dialog") {
                modalFound = true;
                break;
            }
        }
        var appendChildTarget = (modalFound) ? testedElement[0] : document.body;

        // storing parent dimension and position within session variable
        this.inlineStyling = {
            container: {},
            content: {}
        };

        // setting the inline styling applied on the slider panel content according to its configuration.
        this.inlineStyling.content[(["top", "bottom"].indexOf(this.sliderPanelConfiguration.slideFrom) === -1) ? "width" : "height"] = this.sliderPanelConfiguration.overlayDimension;

        // appending the slider panel HTML tag as last child of the HTML body tag.
        angular.element(appendChildTarget).append(element[0]);

    }

    return {

        /**
         * @ngdoc method
         * @name sliderPanelServiceModule.service:sliderPanelService#getNewServiceInstance
         * @methodOf sliderPanelServiceModule.service:sliderPanelService
         * @description
         * Set and returns a new instance of the slider panel.
         * @param {Object} element A pointer to the slider panel HTML tag provided by the Angular framework.
         * @param {Object} window A pointer to the Javascript window element provided by the Angular framework.
         * @param {Object =} configuration A JSON object containing the specific configuration to be applied on the slider panel.
         * @param {String} configuration.cssSelector CSS pattern used to select the element covered by the slider panel.
         * @param {Object} configuration.modal Renders the slider panel as a SmartEdit modal.
         * ````
         *{
         *   showDismissButton: {Boolean},
         *   title: {String},
         *   cancel: {
         *       onClick: {Function},
         *       label: {String}
         *   }
         *   save: {
         *       disabled: {String} (optional)
         *       onClick: {Function},
         *       label: {String}
         *   },
         *   dismiss: {
         *       onClick: {Function}
         *   }
         *}
         * ````
         * @param {Boolean} configuration.noGreyedOutOverlay Used to indicate if a greyed-out overlay is to be displayed or not.
         * @param {String} configuration.overlayDimension Indicates the dimension of the container slider panel once it is displayed.
         * @param {String} configuration.slideFrom Specifies from which side of its container the slider panel slides out.
         * @param {Boolean} configuration.displayedByDefault Specifies whether the slider panel is to be shown by default.
         * @param {String} configuration.zIndex Indicates the z-index value to be applied on the slider panel.
         */
        getNewServiceInstance: function(element, window, configuration) {
            return new sliderPanelService(element, window, configuration);
        }

    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    /**
     * @ngdoc overview
     * @name tabsetModule
     * @description
     *
     * The Tabset module provides the directives required to display a group of tabsets within a tabset. The
     * {@link tabsetModule.directive:yTabset yTabset} is of particular interest to SmartEdit developers
     * because this directive is responsible for displaying and organizing tabs.
     *
     */
    angular.module('tabsetModule', ['ui.bootstrap', 'coretemplates', 'functionsModule', 'translationServiceModule'])

    /**
     * @ngdoc directive
     * @name tabsetModule.directive:yTabset
     * @scope
     * @restrict E
     * @element smartedit-tabset
     *
     * @description
     * The directive responsible for displaying and organizing tabs within a tabset. A specified number of tabs will
     * display a tab header. If there are more tabs than the maximum number defined, the remaining tabs will be grouped
     * in a drop-down menu with the header "More". When a user clicks on a tab header or an item from the drop-down
     * menu, the content of the tabset changes to the body of the selected tab.
     *
     * Note: The body of each tab is wrapped within a {@link tabsetModule.directive:yTab yTab} directive.
     *
     * @param {Object} model Custom data to be passed to each tab. Neither the smartedit-tabset directive or the
     * smartedit-tab directive can modify this value. The tabs' contents determine how to parse and use this object.
     * @param {Object[]} tabsList A list that contains the configuration for each of the tabs to be displayed in the tabset.
     * @param {string} tabsList.id The ID used to track the tab within the tabset.
     * @param {String} tabsList.title The tab header.
     * @param {String} tabsList.templateUrl Path to the HTML fragment to be displayed as the tab content.
     * @param {boolean} tabsList.hasErrors Flag that indicates whether a visual error is to be displayed in the tab or not.
     * @param {Function} tabControl  An optional parameter. A function that will be called with scope as its parameter.
     * It allows the caller to register extra functionality to be executed in the child tabs.
     * @param {Number} numTabsDisplayed The number of tabs for which tab headers will be displayed. The remaining tab
     * headers will be grouped within the 'MORE' drop-down menu.
     *
     */
    .directive('yTabset', function() {
        return {
            restrict: 'E',
            transclude: false,
            templateUrl: 'yTabsetTemplate.html',
            scope: {
                model: '=',
                tabsList: '=',
                tabControl: '=',
                numTabsDisplayed: '@'
            },
            link: function(scope) {

                var prevValue = 0;

                scope.$watch(
                    function() {
                        return (scope.tabsList && prevValue !== scope.tabsList.length);
                    },
                    function() {
                        scope.initializeTabs();
                        prevValue = scope.tabsList ? scope.tabsList.length : 0;
                    });

                scope.isActiveInMoreTab = function() {
                    return scope.tabsList.indexOf(scope.selectedTab) >= (scope.numTabsDisplayed - 1);
                };

                scope.initializeTabs = function() {

                    if (scope.tabsList && scope.tabsList.length > 0) {

                        var tabAlreadySelected = false;

                        for (var tabIdx in scope.tabsList) {
                            if (scope.tabsList[tabIdx].active) {
                                scope.selectedTab = scope.tabsList[tabIdx];
                                tabAlreadySelected = true;
                            }
                        }

                        if (!tabAlreadySelected) {
                            scope.selectedTab = scope.tabsList[0];
                            for (tabIdx in scope.tabsList) {
                                var tab = scope.tabsList[tabIdx];
                                tab.active = (tabIdx === '0');
                                tab.hasErrors = false;
                            }
                        }

                    }

                };

                scope.selectTab = function(tabToSelect) {
                    if (tabToSelect) {
                        if (!scope.selectedTab.active) {
                            // If a tab is made active 'manually' from outside this controller, there can be a mismatch. 
                            // This method finds the tab that is actually selected. 
                            scope.findSelectedTab();
                        }
                        scope.selectedTab.active = false;
                        scope.selectedTab = tabToSelect;
                        scope.selectedTab.active = true;
                    }
                };

                scope.dropDownHasErrors = function() {
                    var tabsInDropDown = scope.tabsList.slice(scope.numTabsDisplayed - 1);

                    return tabsInDropDown.some(function(tab) {
                        return tab.hasErrors;
                    });
                };

                scope.markTabsInError = function(tabsInErrorList) {
                    scope.resetTabErrors();

                    var tabId;
                    var tabFilter = function(tab) {
                        return (tab.id === tabId);
                    };

                    for (var idx in tabsInErrorList) {
                        tabId = tabsInErrorList[idx];
                        var resultTabs = scope.tabsList.filter(tabFilter);

                        if (resultTabs[0]) {
                            resultTabs[0].hasErrors = true;
                        }
                    }
                };

                scope.resetTabErrors = function() {
                    for (var tabKey in scope.tabsList) {
                        scope.tabsList[tabKey].hasErrors = false;
                    }
                };

                scope.findSelectedTab = function() {
                    var selectedTab = scope.tabsList.filter(function(tab) {
                        return tab.active;
                    })[0];

                    if (selectedTab) {
                        scope.selectedTab = selectedTab;
                    }
                };

                scope.initializeTabs();
            }
        };
    })

    /**
     * @ngdoc directive
     * @name tabsetModule.directive:yTab
     * @scope
     * @restrict E
     * @element smartedit-tab
     *
     * @description
     * The directive  responsible for wrapping the content of a tab within a
     * {@link tabsetModule.directive:yTabset yTabset} directive.
     *
     * @param {Number} tabId The ID used to track the tab within the tabset. It must match the ID used in the tabset.
     * @param {String} content Path to the HTML fragment to be displayed as the tab content.
     * @param {Object} model Custom data. Neither the smartedit-tabset directive or the smartedit-tab directive
     * can modify this value. The tabs' contents determine how to parse and use this object.
     * @param {function} tabControl An optional parameter. A function that will be called with scope as its parameter.
     * It allows the caller to register extra functionality to be executed in the tab.
     *
     */
    .directive('yTab', ['$log', '$templateCache', '$compile', '$q', 'hitch', function($log, $templateCache, $compile, $q, hitch) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                content: '=',
                model: '=',
                tabControl: '=',
                tabId: '@'
            },
            link: function(scope, elem) {

                var template = $templateCache.get(scope.content);
                var compile = $compile(template);
                var compiled = compile(scope);
                elem.append(compiled);

                if (scope.tabControl) {
                    hitch(this, scope.tabControl(scope));
                }
            }
        };
    }]);
})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('templateCacheDecoratorModule', [])
    .config(['$provide', function($provide) {

        var pathRegExp = /web.+\/(\w+)\.html/;
        var namePathMap = {};

        $provide.decorator('$templateCache', ['$delegate', function($delegate) {

            var originalPut = $delegate.put;

            $delegate.put = function() {
                var path = arguments[0];
                var template = arguments[1];
                if (pathRegExp.test(path)) {
                    var fileName = pathRegExp.exec(path)[1] + ".html";
                    if (!namePathMap[fileName]) {

                        originalPut.apply($delegate, [fileName, template]);
                        namePathMap[fileName] = path;
                    } else {
                        throw "[templateCacheDecorator] html templates '" + namePathMap[fileName] + "' and '" + path + "' are conflicting, you must give them different filenames";
                    }
                }
                return originalPut.apply($delegate, arguments);
            };

            // ============== UNCOMMENT THIS TO DEBUG TEMPLATECACHE ==============
            // ========================== DO NOT COMMIT ==========================
            // var originalGet = $delegate.get;
            //
            // $delegate.get = function() {
            //     var path = arguments[0];
            //     var $log = angular.injector(['ng']).get('$log');
            //
            //     $log.debug("$templateCache GET: " + path);
            //     return originalGet.apply($delegate, arguments);
            // };

            return $delegate;
        }]);
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name timerModule
 *
 * @description
 * A module that provides a Timer object that can invoke a callback after a certain period of time.
 * * {@link timerModule.service:Timer Timer}
 */
angular.module('timerModule', [])

/**
 * @ngdoc service
 * @name timerModule.service:Timer
 *
 * @description
 * A `Timer` must be instanciated calling **`timerService.createTimer()`**.
 * This `Timer` service wraps `Angular`'s {@link https://docs.angularjs.org/api/ng/service/$timeout $timeout} service and adds additional functions to it.
 *
 * @param {Function} callback The function that will be invoked upon timeout.
 * @param {Number} [duration=1000] The number of milliseconds to wait before the callback is invoked. 
 * @param {Boolean} [invokeApply=true] Thise parameter can be set to false in order to instruct `Angular`'s {@link https://docs.angularjs.org/api/ng/service/$timeout $timeout} service to skip model dirty checking.
 *
 */
.factory('Timer', ['$timeout', function($timeout) {


    function Timer(callback, duration, invokeApply) {

        if (!callback) {
            throw 'please provide a callback';
        }

        this._callback = callback;
        this._duration = (duration || 1000);
        this._invokeApply = (invokeApply !== false);

        // I hold the $timeout promise. This will only be non-null when the
        // timer is actively counting down to callback invocation.
        this._timer = null;
    }

    // Define the instance methods.
    Timer.prototype = {

        /**
         * @ngdoc method
         * @name timerModule.service:Timer#isActive
         * @methodOf timerModule.service:Timer
         *
         * @description
         * This method can be used to know whether or not the timer is currently active (counting down).
         *
         * @returns {Boolean} Returns true if the timer is active (counting down).
         */
        isActive: function() {
            return (!!this._timer);
        },


        /**
         * @ngdoc method
         * @name timerModule.service:Timer#restart
         * @methodOf timerModule.service:Timer
         *
         * @description
         * Stops the timer, and then starts it again. If a new duration is given, the timer's duration will be set to that new value.
         *
         * @param {Number} [duration=The previously set duration] The new number of milliseconds to wait before the callback is invoked. 
         */
        restart: function(duration) {
            this._duration = duration || this._duration;
            this.stop();
            this.start();
        },


        /**
         * @ngdoc method
         * @name timerModule.service:Timer#start
         * @methodOf timerModule.service:Timer
         *
         * @description
         * Start the timer, which will invoke the callback upon timeout.
         *
         * @param {Number} [duration=The previously set duration] The new number of milliseconds to wait before the callback is invoked. 
         */
        start: function(duration) {

            this._duration = duration || this._duration;

            // NOTE: Instead of passing the callback directly to the timeout,
            // we're going to wrap it in an anonymous function so we can set
            // the enable flag. We need to do this approach, rather than
            // binding to the .then() event since the .then() will initiate a
            // digest, which the user may not want.
            this._timer = $timeout(
                function handleTimeoutResolve() {
                    try {
                        if (this._callback) {
                            this._callback.call(null);
                        } else {
                            this.stop();
                        }

                    } finally {
                        this.start();
                    }
                }.bind(this),
                this._duration,
                this._invokeApply
            );
        },


        /**
         * @ngdoc method
         * @name timerModule.service:Timer#stop
         * @methodOf timerModule.service:Timer
         *
         * @description
         * Stop the current timer, if it is running, which will prevent the callback from being invoked.
         */
        stop: function() {
            $timeout.cancel(this._timer);
            this._timer = false;
        },


        /**
         * @ngdoc method
         * @name timerModule.service:Timer#resetDuration
         * @methodOf timerModule.service:Timer
         *
         * @description
         * Sets the duration to a new value.
         *
         * @param {Number} [duration=The previously set duration] The new number of milliseconds to wait before the callback is invoked. 
         */
        resetDuration: function(duration) {
            this._duration = duration || this._duration;
        },


        /**
         * @ngdoc method
         * @name timerModule.service:Timer#teardown
         * @methodOf timerModule.service:Timer
         *
         * @description
         * Clean up the internal object references 
         */
        teardown: function() {
            this.stop();
            this._callback = null;
            this._duration = null;
            this._invokeApply = null;
            this._timer = null;
        }
    };

    return Timer;
}])


.factory('timerService', ['Timer', function(Timer) {

    function createTimer(callback, duration, invokeApply) {
        return new Timer(callback, duration, invokeApply);
    }

    return {
        createTimer: createTimer
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc service
 * @name translationServiceModule
 *
 * @description
 * 
 * This module is used to configure the translate service, the filter, and the directives from the 'pascalprecht.translate' package. The configuration consists of:
 * 
 * <br/>- Initializing the translation map from the {@link i18nInterceptorModule.object:I18NAPIROOT I18NAPIROOT} constant.
 * <br/>- Setting the preferredLanguage to the {@link i18nInterceptorModule.object:UNDEFINED_LOCALE UNDEFINED_LOCALE} so that the {@link i18nInterceptorModule.service:i18nInterceptor#methods_request i18nInterceptor request} can replace it with the appropriate URI combined with the runtime browser locale retrieved from the {@link languageServiceModule.service:languageService#methods_getBrowserLocale languageService.getBrowserLocale}, which is unaccessible at configuration time.
 */
angular.module('translationServiceModule', ['pascalprecht.translate', 'i18nInterceptorModule', 'operationContextServiceModule'])
    .config(['$translateProvider', 'I18NAPIROOT', 'UNDEFINED_LOCALE', function($translateProvider, I18NAPIROOT, UNDEFINED_LOCALE) {

        /*
         * hard coded url that is always intercepted by i18nInterceptor so as to replace by value from configuration REST call
         */
        $translateProvider.useStaticFilesLoader({
            prefix: '/' + I18NAPIROOT + '/',
            suffix: ''
        });

        // Tell the module what language to use by default
        $translateProvider.preferredLanguage(UNDEFINED_LOCALE);

        // Using 'escapeParameters' strategy. 'sanitize' not supported in current version 2.13.1.
        // Note that this is the only option that should be used for now.
        // The options 'sanitizeParameters' and 'escape' are causing issues (& replaced by &amp; and interpolation parameters values are not displayed correctly).
        $translateProvider.useSanitizeValueStrategy('escapeParameters');
    }])
    .run(['operationContextService', 'I18N_RESOURCE_URI', 'OPERATION_CONTEXT', function(operationContextService, I18N_RESOURCE_URI, OPERATION_CONTEXT) {
        operationContextService.register(I18N_RESOURCE_URI, OPERATION_CONTEXT.TOOLING);
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name treeModule
 * @description
 * <h1>This module deals with rendering and management of node trees</h1>
 */
angular.module("treeModule", ['ui.tree', 'includeReplaceModule', 'functionsModule', 'restServiceFactoryModule', 'translationServiceModule', 'confirmationModalServiceModule', 'yLoDashModule', 'alertServiceModule'])
    .constant('treeConfig', {
        treeClass: 'angular-ui-tree',
        hiddenClass: 'angular-ui-tree-hidden',
        nodesClass: 'angular-ui-tree-nodes',
        nodeClass: 'angular-ui-tree-node',
        handleClass: 'angular-ui-tree-handle',
        placeholderClass: 'angular-ui-tree-placeholder',
        dragClass: 'angular-ui-tree-drag',
        dragThreshold: 3,
        levelThreshold: 30,
        defaultCollapsed: true,
        dragDelay: 200
    })
    /**
     * @ngdoc object
     * @name treeModule.object:Node
     * @description
     * A plain JSON object, representing the node of a tree managed by the {@link treeModule.directive:ytree ytree} directive.
     */
    /**
     * @ngdoc property
     * @name uid
     * @propertyOf treeModule.object:Node
     * @description
     * the unique identifier of a node for the given catalog. Optional upon posting.
     **/
    /**
     * @ngdoc property
     * @name name
     * @propertyOf treeModule.object:Node
     * @description
     * the non localized node name. Required upon posting.
     **/
    /**
     * @ngdoc property
     * @name parentUid
     * @propertyOf treeModule.object:Node
     * @description
     * the unique identifier of the parent node for the given catalog. Required upon posting.
     **/
    /**
     * @ngdoc property
     * @name hasChildren
     * @propertyOf treeModule.object:Node
     * @description
     * boolean specifying whether the retrieved node has children. This is read only and ignored upon saving.
     **/

/**
	 * @ngdoc service
	 * @name treeModule.service:TreeService
	 *
	 * @description
	 * A class to manage tree nodes through a REST API.
	 * @constructs treeModule.TreeService
	 * @param {string} nodeUri the REST entry point to handle tree nodes. it must support GET, POST, PUT and DELETE verbs:
	 * - GET nodeUri?parentUid={parentUid} will return a list of children {@link treeModule.object:Node nodes}  wrapped in an object:
	 * <pre>
	    {
		    navigationNodes:[{
	            uid: "2",
	            name: "node2",
	            parentUid: "root"
	            hasChildren: true
	        }, {
	            uid: "4",
	            name: "node4",
	            parentUid: "1",
	            hasChildren: false
	        }]
		}
	 * </pre>
	 * - POST nodeUri takes a {@link treeModule.object:Node Node} payload and returns the final object.
	 * - PUT nodeUri/{uid} takes a {@link treeModule.object:Node Node} payload and returns the final object.
	 * - DELETE nodeUri/{uid} 
	 */

.factory("TreeService", ['$q', '$log', 'restServiceFactory', 'getDataFromResponse', function($q, $log, restServiceFactory, getDataFromResponse) {

    var TreeService = function(nodeUri) {
        if (nodeUri) {
            this.nodesRestService = restServiceFactory.get(nodeUri);
        }
    };

    /**
     * @ngdoc method
     * @name treeModule.service:TreeService#fetchChildren
     * @methodOf treeModule.service:TreeService
     * @description
     * Will fetch the children of a given node by querying GET nodeUri?parentUid={parentUid}
     * - Once the children retrieved, the node will be marked as "initiated" and subsequent calls will not hit the server.
     * - Each children will be given a ManyToOne reference to their parent.
     * - The parent nodes will be assigned its children through the "nodes" property.
     * @param {Object} parent the parent {@link treeModule.object:Node node} object the nodes of which we want to fetch
     */
    TreeService.prototype.fetchChildren = function(_parent) {
        _parent.nodes = _parent.nodes || [];
        if (_parent.initiated) {
            return $q.when(_parent.nodes);
        } else {
            return this.nodesRestService.get({
                parentUid: _parent.uid
            }).then(function(response) {

                _parent.initiated = true;

                var children = getDataFromResponse(response);

                if (!children) {
                    $log.error('No children found for node: ' + _parent.uid + ' while building yTree.');
                    return [];
                }

                children.forEach(function(child) {
                    child.parent = _parent;
                });

                Array.prototype.push.apply(_parent.nodes, children);
                return children;
            });
        }

    };
    /**
     * @ngdoc method
     * @name treeModule.service:TreeService#saveNode
     * @methodOf treeModule.service:TreeService
     * @description
     * Will save a new node for the given parent by POSTing to nodeUri. The payload will only contain the parentUid and a generated name.
     * On the front end side the parent model will be marked as having children.
     * @param {Object} parent the parent {@link treeModule.object:Node node} object from which to create a child
     */
    TreeService.prototype.saveNode = function(_parent) {
        return this.nodesRestService.save({
            parentUid: _parent.uid,
            name: (_parent.name ? _parent.name : _parent.uid) + _parent.nodes.length
        }).then(function(response) {
            _parent.hasChildren = true;
            response.parent = _parent;
            return response;
        });
    };
    /**
     * @ngdoc method
     * @name treeModule.service:TreeService#removeNode
     * @methodOf treeModule.service:TreeService
     * @description
     * Will delete a node by sending DELETE to nodeUri/{uid}.
     * On the front end side the parent model "hasChildren" will be re-evaluated.
     * @param {Object} node the {@link treeModule.object:Node node} object to delete.
     */
    TreeService.prototype.removeNode = function(node) {
        return this.nodesRestService.remove({
            identifier: node.uid
        }).then(function() {
            var parent = node.parent;
            parent.hasChildren = parent.nodes.length > 1;
            return;
        });
    };

    return TreeService;
}])

/**
 * @ngdoc controller
 * @name treeModule.controller:YTreeController
 * @description
 * Extensible controller of the {@link treeModule.directive:ytree ytree} directive
 */
.controller("YTreeController", ['$scope', '$q', 'TreeService', '_TreeDndOptions', function($scope, $q, TreeService, _TreeDndOptions) {

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#collapseAll
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Causes all the nodes of the tree to collapse.
         * It does not affect their "initiated" status though.
         */
        this.collapseAll = function() {
            $scope.$broadcast('angular-ui-tree:collapse-all');
        };

        this.expandAll = function() {
            $scope.$broadcast('angular-ui-tree:expand-all');
        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#hasChildren
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Return a boolean to determine if the node is expandable or not by checking if a given node has children
         * @param {Object} handle the native {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} handle on a given {@link treeModule.object:Node node}
         */
        this.hasChildren = function(handle) {
            var nodeData = handle.$modelValue;
            return nodeData.hasChildren;
        };

        this.fetchData = function(nodeData) {
            return this.treeService.fetchChildren(nodeData);
        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#toggleAndfetch
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will toggle a {@link treeModule.object:Node node}, causing a fetch from server if expanding for the first time.
         * @param {Object} handle the native {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} handle on a given {@link treeModule.object:Node node}
         */
        this.toggleAndfetch = function(handle) {
            this.isDisabled = true;
            var nodeData = handle.$modelValue;
            if (handle.collapsed) {
                return this.fetchData(nodeData).then(function() {
                    handle.toggle();
                    this.isDisabled = false;
                }.bind(this));
            } else {
                handle.toggle();
                this.isDisabled = false;
                return $q.when();
            }

        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#refresh
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will refresh a node, causing it to expand after fetch if it was expanded before.
         */
        this.refresh = function(handle) {
            var nodeData = handle.$modelValue;
            nodeData.initiated = false;
            var previousCollapsed = handle.collapsed;
            return this.fetchData(nodeData).then(function() {
                if (!previousCollapsed && handle.collapsed) {
                    handle.toggle();
                }
            });
        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#refreshParent
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will refresh the parent of a node, causing it to expand after fetch if it was expanded before.
         */
        this.refreshParent = function(handle) {
            if (handle.$modelValue.parent.uid === this.root.uid) {
                this.fetchData(this.root);
            } else {
                this.refresh(handle.$parentNodeScope);
            }
        };
        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#newChild
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will add a new child to the node referenced by this handle.
         * <br/>The child is added only if {@link treeModule.service:TreeService#methods_saveNode saveNode} from {@link treeModule.service:TreeService TreeService} is successful.
         * @param {Object} handle the native {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} handle on a given {@link treeModule.object:Node node}
         */
        this.newChild = function(handle) {

            var nodeData = handle.$modelValue ? handle.$modelValue : this.root;
            nodeData.nodes = nodeData.nodes || [];

            this.treeService.saveNode(nodeData).then(function(response) {
                (handle.collapsed ? this.toggleAndfetch(handle) : $q.when()).then(function() {
                    var elm = nodeData.nodes.find(function(node) {
                        return node.uid === response.uid;
                    });
                    if (!elm) { //if children list already initiated, one needs to push to the list on the ui side
                        nodeData.nodes.push(response);
                    }
                });
            }.bind(this));
        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#newSibling
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will add a new sibling to the node referenced by this handle.
         * <br/>The sibling is added only if {@link treeModule.service:TreeService#methods_saveNode saveNode} from {@link treeModule.service:TreeService TreeService} is successful.
         * @param {Object} handle the native {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} handle on a given {@link treeModule.object:Node node}
         */
        this.newSibling = function(handle) {

            var nodeData = handle.$modelValue;
            var parent = nodeData.parent;
            this.treeService.saveNode(parent).then(function(response) {
                parent.nodes.push(response);
            }.bind(this));

        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#remove
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will remove the node referenced by this handle.
         * <br/>The node is removed only if {@link treeModule.service:TreeService#methods_removeNode removeNode} from {@link treeModule.service:TreeService TreeService} is successful.
         * @param {Object} handle the native {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} handle on a given {@link treeModule.object:Node node}
         */
        this.remove = function(handle) {

            var nodeData = handle.$modelValue;
            this.treeService.removeNode(nodeData).then(function() {
                var parent = nodeData.parent;
                parent.nodes.splice(parent.nodes.indexOf(nodeData), 1);
                parent.initiated = false;
                delete parent.nodes;
                this.fetchData(parent);
            }.bind(this));

        };


        this.isRoot = function(handle) {
            var nodeData = handle.$modelValue;
            return nodeData.parent.uid === undefined;
        };

        this.displayDefaultTemplate = function() {
            return !this.removeDefaultTemplate;
        };

        this.onNodeMouseEnter = function(node) {
            node.mouseHovered = true;
        };

        this.onNodeMouseLeave = function(node) {
            node.mouseHovered = false;
        };

        /**
         * @ngdoc method
         * @name treeModule.controller:YTreeController#getNodeById
         * @methodOf treeModule.controller:YTreeController
         * @description
         * Will fetch from the existing tree the node whose identifier is the given nodeUid
         * @param {String} nodeUid the identifier of the node to fetched
         */
        this.getNodeById = function(nodeUid, nodeArray) {
            if (!nodeArray) {
                nodeArray = this.root.nodes;
            }
            if (nodeUid === this.rootNodeUid) {
                return this.root;
            } else {

                for (var i in nodeArray) {
                    if (nodeArray[i].uid === nodeUid) {
                        return nodeArray[i];
                    }
                    if (nodeArray[i].hasChildren) {
                        nodeArray[i].nodes = nodeArray[i].nodes || [];
                        var result = this.getNodeById(nodeUid, nodeArray[i].nodes);
                        if (result) {
                            return result;
                        }
                    }
                }

            }
        };

        this.$onInit = function() {
            this.treeOptions = new _TreeDndOptions(this.dragOptions);

            this.treeService = new TreeService(this.nodeUri);

            this.root = {
                uid: this.rootNodeUid
            };

            Object.keys(this.nodeActions).forEach(function(functionName) {
                this[functionName] = this.nodeActions[functionName].bind(this, this.treeService);
                this.nodeActions[functionName] = this.nodeActions[functionName].bind(this, this.treeService);
            }.bind(this));


            this.fetchData(this.root);
        };

    }])
    .factory('_TreeDndOptions', ['$translate', 'treeConfig', 'confirmationModalService', 'lodash', '$q', 'alertService', function($translate, treeConfig, confirmationModalService, lodash, $q, alertService) {
        function TreeDndOptions(options) {
            this.dragEnabled = false;
            this.dragDelay = treeConfig.dragDelay;
            this.callbacks = {};

            if (lodash.isNil(options)) {
                return;
            }

            var optionsType = typeof options;
            if (optionsType !== 'object') {
                throw "Unexpected type for dragOptions, expected object but got " + optionsType;
            }

            if (options.hasOwnProperty('onDropCallback')) {
                var onDropCallbackType = typeof options.onDropCallback;
                if (onDropCallbackType !== 'function') {
                    throw "Unexpected type for onDropCallback, expected function but got " + onDropCallbackType;
                }
                this.dragEnabled = true;
                this.callbacks.dropped = function(event) {
                    if (event.source === null || event.dest === null) {
                        return;
                    }
                    var dndEvent = new yTreeDndEvent(event.source.nodeScope.$modelValue, event.dest.nodesScope.$modelValue, event.dest.index, event.source.nodeScope.$parentNodeScope, event.dest.nodesScope.$nodeScope);
                    options.onDropCallback(dndEvent);
                };
            }

            if (options.hasOwnProperty('beforeDropCallback')) {
                var beforeDropCallbackType = typeof options.beforeDropCallback;
                if (beforeDropCallbackType !== 'function') {
                    throw "Unexpected type for beforeDropCallback, expected function but got " + beforeDropCallbackType;
                }
                this.dragEnabled = true;
                this.callbacks.beforeDrop = function(event) {
                    if (event.source === null || event.dest === null) {
                        return true;
                    }
                    var dndEvent = new yTreeDndEvent(event.source.nodeScope.$modelValue, event.dest.nodesScope.$modelValue, event.dest.index);
                    var condition = options.beforeDropCallback(dndEvent);
                    return $q.when(condition).then(function(result) {
                        if (typeof result === 'object') {
                            if (result.hasOwnProperty('confirmDropI18nKey')) {
                                var message = {
                                    description: result.confirmDropI18nKey
                                };
                                return confirmationModalService.confirm(message);
                            }
                            if (result.hasOwnProperty('rejectDropI18nKey')) {
                                alertService.showDanger({
                                    message: result.rejectDropI18nKey
                                });
                                return false;
                            }
                            throw "Unexpected return value for beforeDropCallback does not contain confirmDropI18nKey nor rejectDropI18nKey: " + result;
                        }
                        return result;
                    });
                };
            }

            if (options.hasOwnProperty('allowDropCallback')) {
                var allowDropCallbackType = typeof options.allowDropCallback;
                if (allowDropCallbackType !== 'function') {
                    throw "Unexpected type for allowDropCallback, expected function but got " + allowDropCallbackType;
                }
                this.dragEnabled = true;
                this.callbacks.accept = function(sourceNodeScope, destNodesScope, destIndex) {
                    var dndEvent = new yTreeDndEvent(sourceNodeScope.$modelValue, destNodesScope.$modelValue, destIndex);
                    return options.allowDropCallback(dndEvent);
                };
            }

            /**
             * @ngdoc object
             * @name treeModule.object:yTreeDndEvent
             * @description
             * A plain JSON object, representing the event triggered when dragging and dropping nodes in the {@link treeModule.directive:ytree ytree} directive.
             *
             * @param {Object} sourceNode is the {@link treeModule.object:Node node} that is being dragged.
             * @param {Object} destinationNodes is the set of the destination's parent's children {@link treeModule.object:Node nodes}.
             * @param {Number} position is the index at which the {@link treeModule.object:Node node} was dropped.
             *
             **/
            /**
             * @ngdoc property
             * @name sourceNode
             * @propertyOf treeModule.object:yTreeDndEvent
             * @description
             * the {@link treeModule.object:Node node} being dragged
             **/
            /**
             * @ngdoc property
             * @name destinationNodes
             * @propertyOf treeModule.object:yTreeDndEvent
             * @description
             * array of siblings {@link treeModule.object:Node nodes} to the location drop location
             **/
            /**
             * @ngdoc property
             * @name position
             * @propertyOf treeModule.object:yTreeDndEvent
             * @description
             * the index at which {@link treeModule.object:Node node} was dropped amongst its siblings
             **/
            /**
             * @ngdoc property
             * @name sourceParentHandle
             * @propertyOf treeModule.object:yTreeDndEvent
             * @description
             * the  UI handle of the parent node of the source element
             **/
            /**
             * @ngdoc property
             * @name targetParentHandle
             * @propertyOf treeModule.object:yTreeDndEvent
             * @description
             * the UI handle of the targeted parent element
             **/
            function yTreeDndEvent(sourceNode, destinationNodes, position, sourceParentHandle, targetParentHandle) {
                this.sourceNode = sourceNode;
                this.destinationNodes = destinationNodes;
                this.position = position;
                this.sourceParentHandle = sourceParentHandle;
                this.targetParentHandle = targetParentHandle;
            }

        }
        return TreeDndOptions;
    }])
    /**
    	 * @ngdoc directive
    	 * @name treeModule.directive:ytree
    	 * @scope
    	 * @restrict E
    	 * @element ytree
    	 *
    	 * @description
    	 * This directive renders a tree of nodes and manages CRUD operations around the nodes.
    	 * <br/>It relies on {@link https://github.com/angular-ui-tree/angular-ui-tree angular-ui-tree} third party library
    	 * <br/>Its behaviour is defined by {@link treeModule.controller:YTreeController YTreeController} controller
    	 * @param {String} nodeTemplateUrl an HTML node template to be included besides each node to enhance rendering and behaviour of the tree. This template may use the nodeActions defined hereunder.
    	 * @param {String} nodeUri the REST entry point to be used to manage the nodes (GET, POST, PUT and DELETE).
    	 * @param {Object} dragOptions a map of callback functions to customize the drag and drop behaviour of the tree by exposing the {@link treeModule.object:yTreeDndEvent yTreeDndEvent}.
    	 * @param {String} nodeActions a map of methods to be closure-bound to the {@link treeModule.controller:YTreeController controller} of the directive in order to manage the tree from the parent scope or from the optional node template.
    	 * <br/> All nodeActions methods must take {@link treeModule.service:TreeService treeService} instance as first parameter.
    	 * <br/> {@link treeModule.service:TreeService treeService} instance will then prebound in the closure made available in the node template or in the parent scope.
    	 * <br/> Example in a parent controller:
    	 * <pre>
    	 	this.actions = {

                myMethod: function(treeService, arg1, arg2) {
            		//some action expecting 'this' 
            		//to be the YTreeController
                    this.newChild(this.root.nodes[0]);
                }
            };
    	 * </pre>
    	 * passed to the directive through:
    	 * <pre>
    	 	<ytree data-node-uri='ctrl.nodeURI' data-node-template-url='ctrl.nodeTemplateUrl' data-node-actions='ctrl.actions'/>
    	 * </pre>
    	 * And in the HTML node template you may invoke it this way:
    	 * <pre>
    	 	<button data-ng-click="ctrl.myMethod('arg1', 'arg2')">my action</button>
    	 * </pre>
    	 * or from the parent controller:
    	 * <pre>
    	 	<button data-ng-click="ctrl.actions.myMethod('arg1', 'arg2')">my action</button>
    	 * </pre>
    	 */
    /**
     * @ngdoc object
     * @name treeModule.object:dragOptions
     * @description
     * A JSON object holding callbacks related to nodes drag and drop functionality in the {@link treeModule.directive:ytree ytree} directive.
     * Each callback exposes the {@link treeModule.object:yTreeDndEvent yTreeDndEvent}
     **/
    /**
     * @ngdoc property
     * @name onDropCallback
     * @propertyOf treeModule.object:dragOptions
     * @description
     * Callback function executed after the node is dropped.
     **/
    /**
     * @ngdoc property
     * @name beforeDropCallback
     * @propertyOf treeModule.object:dragOptions
     * @description
     * Callback function executed before drop. Return true allows drop, false rejects, and an object {confirmDropI18nKey: 'key'} opens a confirmation modal.
     **/
    /**
     * @ngdoc property
     * @name acceptDropCallback
     * @propertyOf treeModule.object:dragOptions
     * @description
     * Callback function executed when hovering over droppable slots, return true to allow, return false to block.
     **/
    .directive('ytree', function() {

        return {
            restrict: 'E',
            transclude: false,
            replace: false,
            templateUrl: 'treeTemplate.html',
            controller: 'YTreeController',
            controllerAs: 'ctrl',
            scope: {},
            bindToController: {
                nodeTemplateUrl: '=',
                nodeUri: '=',
                nodeActions: '=',
                rootNodeUid: '=',
                dragOptions: '=',
                removeDefaultTemplate: '=?',
                showAsList: '=?'
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name wizardServiceModule
 *
 * @description
 * # The wizardServiceModule
 * The wizardServiceModule is a module containing all wizard related services
 * # Creating a modal wizard in a few simple steps
 * 1. Add the wizardServiceModule to your module dependencies
 * 2. Inject {@link wizardServiceModule.modalWizard modalWizard} where you want to use the wizard.
 * 3. Create a new controller for your wizard. This controller will be used for all steps of the wizard.
 * 4. Implement a function in your new controller called <strong>getWizardConfig</strong> that returns a {@link wizardServiceModule.object:ModalWizardConfig ModalWizardConfig}
 * 5. Use {@link wizardServiceModule.modalWizard#methods_open modalWizard.open()} passing in your new controller
 *
 * <pre>
    angular.module('myModule', ['wizardServiceModule'])
        .controller('myWizardController', ['wizardManager',
            function(wizardManager) {
                this.getWizardConfig = function() {
                    return {
                        steps: [{
                            id: 'step1',
                            name: 'i18n.step1.name',
                            title: 'i18n.step1.title',
                            templateUrl: 'some/template1.html'
                        }, {
                            id: 'step2',
                            name: 'i18n.step2.name',
                            title: 'i18n.step2.title',
                            templateUrl: 'some/template2.html'
                        }]
                    };
                }
            }]
        )
        .service('myService', function(modalWizard) {
            this.doSomething = function() {
                    return modalWizard.open({
                        controller: 'myWizardController'
                    });
            };
        });
 * </pre>
 *
 */
angular.module('wizardServiceModule', ['ui.bootstrap', 'translationServiceModule', 'functionsModule', 'coretemplates', 'modalServiceModule'])

.factory('wizardActions', function() {

    var defaultAction = {
        id: "wizard_action_id",
        i18n: 'wizard_action_label',
        isMainAction: true,
        enableIfCondition: function() {
            return true;
        },
        executeIfCondition: function() {
            return true;
        },
        execute: function() {}
    };

    function newAction(conf) {
        var defaultCopy = angular.copy(defaultAction);
        return angular.merge(defaultCopy, conf);
    }

    return {

        customAction: function(conf) {
            return newAction(conf);
        },

        done: function(conf) {
            var nextConf = {
                id: 'ACTION_DONE',
                i18n: 'se.action.done',
                execute: function(wizardService) {
                    wizardService.close();
                }
            };
            angular.merge(nextConf, conf);
            return newAction(nextConf);
        },

        next: function(conf) {
            var nextConf = {
                id: 'ACTION_NEXT',
                i18n: 'se.action.next',
                execute: function(wizardService) {
                    if (this.nextStepId) {
                        wizardService.goToStepWithId(this.nextStepId);
                    } else if (this.nextStepIndex) {
                        wizardService.goToStepWithIndex(this.nextStepIndex);
                    } else {
                        wizardService.goToStepWithIndex(wizardService.getCurrentStepIndex() + 1);
                    }
                }
            };

            angular.merge(nextConf, conf);
            return newAction(nextConf);
        },

        navBarAction: function(conf) {
            if (!conf.wizardService || conf.destinationIndex === null) {
                throw "Error initializating navBarAction, must provide the wizardService and destinationIndex fields";
            }

            var nextConf = {
                id: 'ACTION_GOTO',
                i18n: 'action.goto',
                enableIfCondition: function() {
                    return conf.wizardService.getCurrentStepIndex() >= conf.destinationIndex;
                },
                execute: function(wizardService) {
                    wizardService.goToStepWithIndex(conf.destinationIndex);
                }
            };

            angular.merge(nextConf, conf);
            return newAction(nextConf);
        },

        back: function(conf) {
            var nextConf = {
                id: 'ACTION_BACK',
                i18n: 'se.action.back',
                isMainAction: false,
                execute: function(wizardService) {
                    if (this.backStepId) {
                        wizardService.goToStepWithId(this.backStepId);
                    } else if (this.backStepIndex) {
                        wizardService.goToStepWithIndex(this.backStepIndex);
                    } else {
                        var currentIndex = wizardService.getCurrentStepIndex();
                        if (currentIndex <= 0) {
                            throw "Failure to execute BACK action, no previous index exists!";
                        }
                        wizardService.goToStepWithIndex(currentIndex - 1);
                    }
                }
            };

            angular.merge(nextConf, conf);
            return newAction(nextConf);
        },

        cancel: function() {
            return newAction({
                id: 'ACTION_CANCEL',
                i18n: 'se.action.cancel',
                isMainAction: false,
                execute: function(wizardService) {
                    wizardService.cancel();
                }
            });
        }

    };
})



/**
 * @ngdoc service
 * @name wizardServiceModule.modalWizard
 *
 * @description
 * The modalWizard service is used to create wizards that are embedded into the {@link modalServiceModule modalService}
 */
.service('modalWizard', ['modalService', 'modalWizardControllerFactory', function(modalService, modalWizardControllerFactory) {

    this.validateConfig = function(config) {
        if (!config.controller) {
            throw "WizardService - initialization exception. No controller provided";
        }
    };

    /**
     * @ngdoc object
     * @name wizardServiceModule.object:WizardStepConfig
     * @description
     * A plain JSON object, representing the configuration options for a single step in a wizard
     */
    /**
     * @ngdoc property
     * @name id
     * @propertyOf wizardServiceModule.object:WizardStepConfig
     * @description
     * An optional unique ID for this step in the wizard. If no ID is provided, one is automatically generated.<br />
     * You may choose to provide an ID, making it easier to reference this step explicitly via the wizard service, or
     * be able to identify for which step a callback is being triggered.
     **/
    /**
     * @ngdoc property
     * @name name
     * @propertyOf wizardServiceModule.object:WizardStepConfig
     * @description An i18n key representing a meaning (short) name for this step.
     * This name will be displayed in the wizard navigation menu.
     **/
    /**
     * @ngdoc property
     * @name title
     * @propertyOf wizardServiceModule.object:WizardStepConfig
     * @description An i18n key, representing the title that will be displayed at the top of the wizard for this step.
     **/
    /**
     * @ngdoc property
     * @name templateUrl
     * @propertyOf wizardServiceModule.object:WizardStepConfig
     * @description The url of the html template for this step
     **/

    /**
     * @ngdoc object
     * @name wizardServiceModule.object:ModalWizardConfig
     * @description
     * A plain JSON object, representing the configuration options for a modal wizard
     */
    /**
     * @ngdoc property
     * @name steps (Array)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An ordered array of {@link wizardServiceModule.object:WizardStepConfig WizardStepConfig}
     **/
    /**
     * @ngdoc property
     * @name isFormValid (Function)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional callback function that receives a single parameter, the current step ID. This callback
     * is used to enable/disable the next action and the done action.
     * The callback should return a boolean to enabled the action. Null, or if this callback is not defined defaults to
     * true (enabled)
     **/
    /**
     * @ngdoc property
     * @name onNext (Function)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional callback function that receives a single parameter, the current step ID.
     * This callback is triggered after the next action is fired. You have the opportunity to halt the Next action by
     * returning promise and rejecting it, otherwise the wizard will continue and load the next step.
     **/
    /**
     * @ngdoc property
     * @name onCancel (Function)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional callback function that receives a single parameter, the current step ID.
     * This callback is triggered after the cancel action is fired. You have the opportunity to halt the cancel action
     * (thereby stopping the wizard from being closed), by returning a promise and rejecting it, otherwise the wizard will
     * continue the cancel action.
     **/
    /**
     * @ngdoc property
     * @name onDone (Function)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional callback function that has no parameters. This callback is triggered after the done
     * action is fired. You have the opportunity to halt the done action (thereby stopping the wizard from being closed),
     * by returning a promise and rejecting it, otherwise the wizard will continue and close the wizard.
     **/
    /**
     * @ngdoc property
     * @name resultFn (Function)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional callback function that has no parameters. This callback is triggered after the done
     * action is fired, and the wizard is about to be closed. If this function is defined and returns a value, this
     * value will be returned in the resolved promise returned by the {@link wizardServiceModule.modalWizard#methods_open modalWizard.open()}
     * This is an easy way to pass a result from the wizard to the caller.
     **/
    /**
     * @ngdoc property
     * @name doneLabel (String)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional i18n key to override the default label for the Done button
     **/
    /**
     * @ngdoc property
     * @name nextLabel (String)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional i18n key to override the default label for the Next button
     **/
    /**
     * @ngdoc property
     * @name backLabel (String)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional i18n key to override the default label for the Back button
     **/
    /**
     * @ngdoc property
     * @name cancelLabel (String)
     * @propertyOf wizardServiceModule.object:ModalWizardConfig
     * @description An optional i18n key to override the default label for the Cancel button
     **/

    /**
     * @ngdoc method
     * @name wizardServiceModule.modalWizard#open
     * @methodOf wizardServiceModule.modalWizard
     *
     * @description
     * Open provides a simple way to create modal wizards, with much of the boilerplate taken care of for you, such as look
     * and feel, and wizard navigation.
     *
     * @param {Object} conf configuration
     * @param {String|function|Array} conf.controller An angular controller which will be the underlying controller
     * for all of the wizard. This controller MUST implement the function <strong>getWizardConfig()</strong> which
     * returns a {@link wizardServiceModule.object:ModalWizardConfig ModalWizardConfig}.<br />
     * If you need to do any manual wizard manipulation, 'wizardManager' can be injected into your controller.
     * See {@link wizardServiceModule.WizardManager WizardManager}
     * @param {String} conf.controllerAs (OPTIONAL) An alternate controller name that can be used in your wizard step
     * @param {=String=} conf.properties A map of properties to initialize the wizardManager with. They are accessible under wizardManager.properties.
     * templates. By default the controller name is wizardController.

     * @returns {function} {@link https://docs.angularjs.org/api/ng/service/$q promise} that will either be resolved (wizard finished) or
     * rejected (wizard cancelled).
     */
    this.open = function(config) {
        this.validateConfig(config);
        return modalService.open({
            templateUrl: 'modalWizardTemplate.html',
            controller: modalWizardControllerFactory.fromConfig(config),
            controllerAs: 'wizardController'
        });
    };
}])


.service('modalWizardControllerFactory', ['$controller', 'wizardServiceFactory', 'wizardActions', 'MODAL_BUTTON_STYLES', '$q', function($controller, wizardServiceFactory, wizardActions, MODAL_BUTTON_STYLES, $q) {

    this.fromConfig = function(config) {
        return ['$scope', '$rootScope', 'modalManager',
            function WizardController($scope, $rootScope, modalManager) {

                var wizardServiceImpl = wizardServiceFactory.newWizardService();

                wizardServiceImpl.properties = config.properties;

                angular.extend(this, $controller(config.controller, {
                    $scope: $scope,
                    wizardManager: wizardServiceImpl
                }));
                if (config.controllerAs) {
                    $scope[config.controllerAs] = this;
                }

                if (typeof this.getWizardConfig !== 'function') {
                    throw "The provided controller must provide a getWizardConfig() function.";
                }
                var modalConfig = this.getWizardConfig();
                var controller = this;

                this._wizardContext = {
                    _steps: modalConfig.steps
                };

                this.executeAction = function(action) {
                    wizardServiceImpl.getActionExecutor().executeAction(action);
                };

                function setupNavBar(steps) {
                    controller._wizardContext.navActions = steps.map(function(step, index) {
                        return wizardActions.navBarAction({
                            id: 'NAV-' + step.id,
                            stepIndex: index,
                            wizardService: wizardServiceImpl,
                            destinationIndex: index,
                            i18n: step.name,
                            isCurrentStep: function() {
                                return this.stepIndex === wizardServiceImpl.getCurrentStepIndex();
                            }
                        });
                    });
                }

                function setupModal(modalConfig) {
                    controller._wizardContext.templateOverride = modalConfig.templateOverride;
                    if (modalConfig.cancelAction) {
                        modalManager.setDismissCallback(function() {
                            wizardServiceImpl.getActionExecutor().executeAction(modalConfig.cancelAction);
                            return $q.reject();
                        });
                    }
                    if (modalConfig.cancelAction) {
                        modalManager.setDismissCallback(function() {
                            wizardServiceImpl.getActionExecutor().executeAction(modalConfig.cancelAction);
                            return $q.reject();
                        });
                    }

                    // strategy stuff TODO - move to strategy layer
                    setupNavBar(modalConfig.steps);
                }

                function actionToButtonConf(action) {
                    return {
                        id: action.id,
                        style: action.isMainAction ? MODAL_BUTTON_STYLES.PRIMARY : MODAL_BUTTON_STYLES.SECONDARY,
                        label: action.i18n,
                        callback: function() {
                            wizardServiceImpl.getActionExecutor().executeAction(action);
                        }
                    };
                }

                wizardServiceImpl.onLoadStep = function(stepIndex, step) {
                    modalManager.title = step.title;
                    controller._wizardContext.templateUrl = step.templateUrl;
                    modalManager.removeAllButtons();
                    (step.actions || []).forEach(function(action) {

                        if (typeof action.enableIfCondition === 'function') {
                            $rootScope.$watch(action.enableIfCondition, function(newVal) {
                                if (newVal) {
                                    modalManager.enableButton(action.id);
                                } else {
                                    modalManager.disableButton(action.id);
                                }
                            });
                        }
                        modalManager.addButton(actionToButtonConf(action));
                    }.bind(this));
                };

                wizardServiceImpl.onClose = function(result) {
                    modalManager.close(result);
                };

                wizardServiceImpl.onCancel = function() {
                    modalManager.dismiss();
                };

                wizardServiceImpl.onStepsUpdated = function(steps) {
                    setupNavBar(steps);
                    controller._wizardContext._steps = steps;
                };

                wizardServiceImpl.initialize(modalConfig);
                setupModal(modalConfig);

            }
        ];
    };
}])


.service('defaultWizardActionStrategy', ['wizardActions', function(wizardActions) {

    function applyOverrides(wizardService, action, label, executeCondition, enableCondition) {
        if (label) {
            action.i18n = label;
        }
        if (executeCondition) {
            action.executeIfCondition = function() {
                return executeCondition(wizardService.getCurrentStepId());
            };
        }
        if (enableCondition) {
            action.enableIfCondition = function() {
                return enableCondition(wizardService.getCurrentStepId());
            };
        }
        return action;
    }

    this.applyStrategy = function(wizardService, conf) {
        var nextAction = applyOverrides(wizardService, wizardActions.next(), conf.nextLabel, conf.onNext, conf.isFormValid);
        var doneAction = applyOverrides(wizardService, wizardActions.done(), conf.doneLabel, conf.onDone, conf.isFormValid);

        var backConf = conf.backLabel ? {
            i18n: conf.backLabel
        } : null;
        var backAction = wizardActions.back(backConf);

        conf.steps.forEach(function(step, index) {
            step.actions = [];
            if (index > 0) {
                step.actions.push(backAction);
            }
            if (index === (conf.steps.length - 1)) {
                step.actions.push(doneAction);
            } else {
                step.actions.push(nextAction);
            }
        });

        conf.cancelAction = applyOverrides(wizardService, wizardActions.cancel(), conf.cancelLabel, conf.onCancel, null);
        conf.templateOverride = 'modalWizardNavBarTemplate.html';
    };
}])

.service('wizardServiceFactory', ['WizardService', function(WizardService) {
    this.newWizardService = function() {
        return new WizardService();
    };
}])


.factory('WizardService', ['$q', 'defaultWizardActionStrategy', 'generateIdentifier', function($q, defaultWizardActionStrategy, generateIdentifier) {

    function validateConfig(config) {
        if (!config.steps || config.steps.length <= 0) {
            throw "Invalid WizardService configuration - no steps provided";
        }

        config.steps.forEach(function(step) {
            if (!step.templateUrl) {
                throw "Invalid WizardService configuration - Step missing a url: " + step;
            }
        });
    }

    function validateStepUids(steps) {
        var stepIds = {};
        steps.forEach(function(step) {
            if (step.id === undefined && step.id === null) {
                step.id = generateIdentifier();
            } else {
                if (stepIds[step.id]) {
                    throw "Invalid (Duplicate) step id: " + step.id;
                }
                stepIds[step.id] = step.id;
            }
        });
    }

    /**
     * @ngdoc service
     * @name wizardServiceModule.WizardManager
     *
     * @description
     * The Wizard Manager is a wizard management service that can be injected into your wizard controller.
     *
     */
    function WizardService() {
        // the overridable callbacks
        this.onLoadStep = function() {};
        this.onClose = function() {};
        this.onCancel = function() {};
        this.onStepsUpdated = function() {};
    }

    WizardService.prototype.initialize = function(conf) {

        validateConfig(conf);

        this._actionStrategy = conf.actionStrategy || defaultWizardActionStrategy;
        this._actionStrategy.applyStrategy(this, conf);

        this._currentIndex = 0;
        this._conf = angular.copy(conf);
        this._steps = this._conf.steps;
        this._getResult = conf.resultFn;
        validateStepUids(this._steps);

        this.goToStepWithIndex(0);
    };

    WizardService.prototype.getActionExecutor = function() {
        return this;
    };


    WizardService.prototype.executeAction = function(action) {
        if (action.executeIfCondition) {
            $q.when(action.executeIfCondition()).then(function() {
                return $q.when(action.execute(this));
            }.bind(this));
        } else {
            return $q.when(action.execute(this));
        }

    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#goToStepWithIndex
     * @methodOf wizardServiceModule.WizardManager
     * @description Navigates the wizard to the given step
     * @param {Number} index The 0-based index from the steps array returned by the wizard controllers getWizardConfig() function
     */
    WizardService.prototype.goToStepWithIndex = function(index) {
        var nextStep = this.getStepWithIndex(index);
        if (nextStep) {
            this.onLoadStep(index, nextStep);
            this._currentIndex = index;
        }
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#goToStepWithId
     * @methodOf wizardServiceModule.WizardManager
     * @description Navigates the wizard to the given step
     * @param {String} id The ID of a step returned by the wizard controllers getWizardConfig() function. Note that if
     * no id was provided for a given step, then one is automatically generated.
     */
    WizardService.prototype.goToStepWithId = function(id) {
        this.goToStepWithIndex(this.getStepIndexFromId(id));
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#addStep
     * @methodOf wizardServiceModule.WizardManager
     * @description Adds an additional step to the wizard at runtime
     * @param {Object} newStep A {@link wizardServiceModule.object:WizardStepConfig WizardStepConfig}
     * @param {Number} index (OPTIONAL) A 0-based index position in the steps array. Default is 0.
     */
    WizardService.prototype.addStep = function(newStep, index) {
        if (newStep.id !== 0 && !newStep.id) {
            newStep.id = generateIdentifier();
        }
        if (!index) {
            index = 0;
        }
        if (this._currentIndex >= index) {
            this._currentIndex++;
        }
        this._steps.splice(index, 0, newStep);
        validateStepUids(this._steps);
        this._actionStrategy.applyStrategy(this, this._conf);
        this.onStepsUpdated(this._steps);
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#removeStepById
     * @methodOf wizardServiceModule.WizardManager
     * @description Remove a step form the wizard at runtime. If you are removing the currently displayed step, the
     * wizard will return to the first step. Removing all the steps will result in an error.
     * @param {String} id The id of the step you wish to remove
     */
    WizardService.prototype.removeStepById = function(id) {
        this.removeStepByIndex(this.getStepIndexFromId(id));
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#removeStepByIndex
     * @methodOf wizardServiceModule.WizardManager
     * @description Remove a step form the wizard at runtime. If you are removing the currently displayed step, the
     * wizard will return to the first step. Removing all the steps will result in an error.
     * @param {Number} index The 0-based index of the step you wish to remove.
     */
    WizardService.prototype.removeStepByIndex = function(index) {
        if (index >= 0 && index < this.getStepsCount()) {
            this._steps.splice(index, 1);
            if (index === this._currentIndex) {
                this.goToStepWithIndex(0);
            }
            this._actionStrategy.applyStrategy(this, this._conf);
            this.onStepsUpdated(this._steps);
        }
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#close
     * @methodOf wizardServiceModule.WizardManager
     * @description Close the wizard. This will return a resolved promise to the creator of the wizard, and if any
     * resultFn was provided in the {@link wizardServiceModule.object:ModalWizardConfig ModalWizardConfig} the returned
     * value of this function will be passed as the result.
     */
    WizardService.prototype.close = function() {
        var result;
        if (typeof this._getResult === 'function') {
            result = this._getResult();
        }
        this.onClose(result);
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#cancel
     * @methodOf wizardServiceModule.WizardManager
     * @description Cancel the wizard. This will return a rejected promise to the creator of the wizard.
     */
    WizardService.prototype.cancel = function() {
        this.onCancel();
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getSteps
     * @methodOf wizardServiceModule.WizardManager
     * @returns {Array} An array of all the steps in the wizard
     */
    WizardService.prototype.getSteps = function() {
        return this._steps;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getStepIndexFromId
     * @methodOf wizardServiceModule.WizardManager
     * @param {String} id A step ID
     * @returns {Number} The index of the step with the provided ID
     */
    WizardService.prototype.getStepIndexFromId = function(stepId) {
        var index = this._steps.findIndex(function(step) {
            return step.id === stepId;
        });
        return index;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#containsStep
     * @methodOf wizardServiceModule.WizardManager
     * @param {String} id A step ID
     * @returns {Boolean} True if the ID exists in one of the steps
     */
    WizardService.prototype.containsStep = function(stepId) {
        return this.getStepIndexFromId(stepId) >= 0;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getCurrentStepId
     * @methodOf wizardServiceModule.WizardManager
     * @returns {String} The ID of the currently displayed step
     */
    WizardService.prototype.getCurrentStepId = function() {
        return this.getCurrentStep().id;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getCurrentStepIndex
     * @methodOf wizardServiceModule.WizardManager
     * @returns {Number} The index of the currently displayed step
     */
    WizardService.prototype.getCurrentStepIndex = function() {
        return this._currentIndex;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getCurrentStep
     * @methodOf wizardServiceModule.WizardManager
     * @returns {Object} The currently displayed step
     */
    WizardService.prototype.getCurrentStep = function() {
        return this.getStepWithIndex(this._currentIndex);
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getStepsCount
     * @methodOf wizardServiceModule.WizardManager
     * @returns {Number} The number of steps in the wizard. This should always be equal to the size of the array
     * returned by {@link wizardServiceModule.WizardManager#methods_getSteps getSteps()}
     */
    WizardService.prototype.getStepsCount = function() {
        return this._steps.length;
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getStepWithId
     * @methodOf wizardServiceModule.WizardManager
     * @param {String} id The ID of a step
     * @returns {Object} The {@link wizardServiceModule.object:WizardStepConfig step} with the given ID
     */
    WizardService.prototype.getStepWithId = function(id) {
        var index = this.getStepIndexFromId(id);
        if (index >= 0) {
            return this.getStepWithIndex(index);
        }
    };

    /**
     * @ngdoc method
     * @name wizardServiceModule.WizardManager#getStepWithIndex
     * @methodOf wizardServiceModule.WizardManager
     * @param {Number} index The ID of a step
     * @returns {Object} The {@link wizardServiceModule.object:WizardStepConfig step} with the given index
     */
    WizardService.prototype.getStepWithIndex = function(index) {
        if (index >= 0 && index < this.getStepsCount()) {
            return this._steps[index];
        }
        throw ("wizardService.getStepForIndex - Index out of bounds: " + index);
    };

    return WizardService;
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('PerspectiveModule', ['perspectiveServiceInterfaceModule', 'decoratorServiceModule', 'ngCookies'])

.constant('PerspectiveCookieVersion', "1.0")

.factory('systemPerspectives', ['Perspective', 'featureService', function(Perspective, featureService) {
    var perspectiveAll = new Perspective(
        'se.perspectives.perspectivename.all', [],
        function() {
            return featureService.getFeatureKeys();
        },
        true);

    var perspectiveNone = new Perspective(
        'se.perspectives.perspectivename.none', [],
        undefined,
        true);

    return [perspectiveAll, perspectiveNone];
}])

.factory('perspectivesCookieSerializer', ['PerspectiveCookieVersion', function(PerspectiveCookieVersion) {
    return {
        serialize: function(perspectivesArr) {
            var serializedData = {
                version: PerspectiveCookieVersion,
                perspectives: []
            };
            if (perspectivesArr) {
                for (var index in perspectivesArr) {
                    serializedData.perspectives.push({
                        name: perspectivesArr[index].name,
                        decorators: perspectivesArr[index].getFeatures()
                    });
                }
            }
            return serializedData;
        }
    };
}])

.factory('perspectiveCookieDeserializer', ['Perspective', '$log', function(Perspective, $log) {

    return {
        deserialize: function(cookieData) {
            $log.debug("Deserializing perspective cookie with version " + cookieData.version);
            try {
                switch (cookieData.version) {
                    case "1.0":
                        var perspectives = [];
                        for (var index in cookieData.perspectives) {
                            var storedPerspective = cookieData.perspectives[index];
                            perspectives.push(new Perspective(storedPerspective.name, storedPerspective.decorators));
                        }
                        return perspectives;

                    default:
                        $log.error("Unknown perspective serial version " + cookieData.version);
                }
            } catch (err) {
                $log.error("Error serializing perspective cookie");
            }
            // fallback
            return [];
        }
    };
}])

.factory('perspectiveStorage', ['$cookies', 'Perspective', 'perspectiveCookieDeserializer', 'perspectivesCookieSerializer', 'systemPerspectives', '$log', function($cookies, Perspective, perspectiveCookieDeserializer, perspectivesCookieSerializer, systemPerspectives, $log) {

    var perspectiveCookieName = 'cmsxperspectives';

    return {
        storePerspectives: function(perspectives) {
            $log.debug('Storing perspectives to cookie');
            var expiryDate = new Date();
            expiryDate.setMonth(expiryDate.getMonth() + 2);

            // strip out system perspectives, we do not want to store system perspectives
            var perspectivesToStore = [];
            for (var p in perspectives) {
                if (perspectives[p].system !== true) {
                    perspectivesToStore.push(perspectives[p]);
                }
            }
            var perspectiveCookieData = perspectivesCookieSerializer.serialize(perspectivesToStore);

            $cookies.putObject(perspectiveCookieName, perspectiveCookieData, {
                expires: expiryDate
            });
        },

        loadPerspectives: function() {
            $log.debug('Loading perspectives from cookie');
            var storedPerspectives = $cookies.getObject(perspectiveCookieName);
            var deserializedPerspectives = [];
            if (storedPerspectives) {
                deserializedPerspectives = perspectiveCookieDeserializer.deserialize(storedPerspectives);
            }

            // prepend all system perspectives
            var allPerspectives = [];
            for (var p in systemPerspectives) {
                allPerspectives.push(systemPerspectives[p]);
            }
            for (var index in deserializedPerspectives) {
                allPerspectives.push(deserializedPerspectives[index]);
            }

            return allPerspectives;
        }
    };
}])

.factory('decoratorFilterService', ['$log', 'decoratorService', 'perspectiveStorage', 'Perspective', function($log, decoratorService, perspectiveStorage, Perspective) {

    var observers = [];
    var perspectives = perspectiveStorage.loadPerspectives();
    var currentPerspective = perspectives[0];

    var notifyPerspectiveChanged = function() {
        for (var o in observers) {
            observers[o](currentPerspective);
        }
    };

    //------------------------------------------------
    return {

        setCurrentPerspective: function(perspectiveName) {
            for (var p in perspectives) {
                if (perspectives[p].name === perspectiveName) {
                    currentPerspective = perspectives[p];
                    notifyPerspectiveChanged();
                    return;
                }
            }
            $log.warn('Could not set current perspective. Perspective not found: ' + perspectiveName);
        },

        getCurrentPerspective: function() {
            if (currentPerspective === null || currentPerspective === undefined) {
                $log.error('Error retrieving current perspective: No current perspective is set!');
            } else {
                return currentPerspective.clone();
            }
        },

        getDecoratorsForComponent: function(componentType) {

            // get all the decorators elligable for this component type
            var decorators = decoratorService.getDecoratorsForComponent(componentType) || [];

            // get the current decorator set for the active perspective
            // this is kind of a weird hack, the pligins list form the application manager, use the module name: XYZDecorator
            // but the directive is actually "XYZ", so we need to strip the appended 'Decorator' form the name
            var decoratorWord = 'decorator';
            var perspDecorators = [];
            var persp = this.getCurrentPerspective();
            var gins = persp.getFeatures();
            for (var p in gins) {
                perspDecorators.push(gins[p].substring(0, gins[p].length - decoratorWord.length));
            }

            var toReturn = [];
            for (var p1 in decorators) {
                for (var p2 in perspDecorators) {
                    if (decorators[p1] === perspDecorators[p2]) {
                        toReturn.push(decorators[p1]);
                    }
                }
            }
            return toReturn;
        },

        getPerspectives: function() {
            var perspectivesClone = [];
            for (var key in perspectives) {
                perspectivesClone.push(perspectives[key].clone());
            }
            return perspectivesClone;
        },

        renamePerspective: function(oldName, newName) {
            $log.debug('Perspective Service - renamePerspective, old: ' + oldName + ', new: ' + newName);
            for (var p in perspectives) {
                if (perspectives[p].name === oldName) {
                    perspectives[p].name = newName;
                    perspectiveStorage.storePerspectives(perspectives);
                    return true;
                }
            }
            $log.error('Could not rename perspective. No perspective found with the name: ' + oldName);
            return false;
        },

        setDecoratorsForPerspective: function(perspectiveName, decorators) {
            for (var p in perspectives) {
                if (perspectives[p].name === perspectiveName) {

                    perspectives[p].setFeatures(decorators);

                    // if we're updating the decorators for the active perspective, then we need to
                    // fire an event to perspectiveChanged
                    if (perspectiveName === currentPerspective.name) {
                        notifyPerspectiveChanged();
                    }
                    perspectiveStorage.storePerspectives(perspectives);
                    return;
                }
            }
            $log.warn('Could not update perspective: ' + perspectiveName + ", no perspective found with that name.");
        },

        addPerspective: function(newPerspective) {
            $log.debug('Perspective Service - addPerspective: ' + newPerspective);
            if (newPerspective instanceof Perspective) {
                perspectives.push(newPerspective.clone());
                perspectiveStorage.storePerspectives(perspectives);
            }
        },

        removePerspective: function(perspectiveName) {
            $log.debug('Perspective Service - removePerspective: ' + perspectiveName);
            for (var p in perspectives) {
                if (perspectives[p].name === perspectiveName) {
                    if (perspectives[p].system === true) {
                        $log.warn('Cannot delete perspective [' + perspectiveName + '], this is a system perspective.');
                        break;
                    }
                    perspectives.splice(p, 1);

                    if (currentPerspective.name === perspectiveName) {
                        if (perspectives.length > 0) {
                            this.setCurrentPerspective(perspectives[0].name);
                        }
                    }
                    // else? shouldn't be possible to have no perspectives
                    perspectiveStorage.storePerspectives(perspectives);
                    return;
                }
            }
            $log.warn('Could not remove perspective: ' + perspectiveName + ", no perspective found.");
        },

        registerPerspectiveChangedListener: function(fnCallback) {
            if (observers.indexOf(fnCallback) >= 0) {
                $log.warn("This callback was already registered");
            } else {
                observers.push(fnCallback);
            }
        },

        unRegisterPerspectiveChangedListener: function(fnCallback) {
            if (observers.indexOf(fnCallback) >= 0) {
                observers.splice(observers.indexOf(fnCallback), 1);
            } else {
                $log.warn("This callback was never registered");
            }
        },

    };
}])

.controller('PerspectiveUIController', ['$scope', '$modalInstance', 'Perspective', 'decoratorFilterService', function($scope, $modalInstance, Perspective, decoratorFilterService) {

    var updateDecoratorSet = function(enabledDecorators) {
        //clear all existing
        $scope.decoratorSet.splice(0, $scope.decoratorSet.length);

        for (var i in $scope.allDecorators) {

            var decoratorName = $scope.allDecorators[i];
            var decoratorFound = false;
            for (var j in enabledDecorators) {
                if (enabledDecorators[j] === decoratorName) {
                    decoratorFound = true;
                    break;
                }
            }
            $scope.decoratorSet.push({
                name: $scope.allDecorators[i],
                checked: decoratorFound
            });
        }
    };

    $scope.warningForSystem = 'se.modal.perpectives.info.system.perspective';

    // get a copy of all perspectives and their decorators
    $scope.perspectives = decoratorFilterService.getPerspectives();

    // find the current (active) perspective in the $scope.perspectives
    // remember $scope.perspectives is a clone of the list, so we need to find the specific instance
    // in the clone, not a copy of it, so the binding can work properly with ng-model in the html
    var currentPerspName = decoratorFilterService.getCurrentPerspective().name;
    for (var key in $scope.perspectives) {
        if ($scope.perspectives[key].name === currentPerspName) {
            $scope.perspective = $scope.perspectives[key];
            break;
        }
    }

    $scope.allDecorators = []; //ApplicationManagerService.getAllDecorators();

    // The set of decorators enabled / disabled on the decorators list in the modal
    $scope.decoratorSet = [];

    $scope.perspectiveChanged = function() {
        $scope.perspective.originalName = $scope.perspective.name;
        updateDecoratorSet($scope.perspective.getFeatures());
    };
    $scope.perspectiveChanged(); // init perspective set with active perspective

    $scope.save = function() {

        var enabledDecoratorsForSelectedSet = [];
        for (var index in $scope.decoratorSet) {
            if ($scope.decoratorSet[index].checked === true) {
                enabledDecoratorsForSelectedSet.push($scope.decoratorSet[index].name);
            }
        }

        decoratorFilterService.setDecoratorsForPerspective($scope.perspective.name, enabledDecoratorsForSelectedSet);
        $scope.perspective.setFeatures(enabledDecoratorsForSelectedSet);
    };

    $scope.nameChanged = function() {
        if (!$scope.perspective.name || !$scope.perspective.name.trim()) {
            $scope.renameError = 'se.modal.perpectives.validation.error.nameempty';
            return;
        }
        for (var key in $scope.perspectives) {
            if ($scope.perspective !== $scope.perspectives[key]) {
                if ($scope.perspective.name === $scope.perspectives[key].name) {
                    //another perspective already exists with this name
                    $scope.renameError = 'se.modal.perpectives.validation.error.nameexists';
                    return;
                }
            }
        }

        $scope.renameError = "";
        decoratorFilterService.renamePerspective($scope.perspective.originalName, $scope.perspective.name);
        $scope.perspective.originalName = $scope.perspective.name;
    };

    $scope.createPerspective = function() {

        var perspNames = [];
        for (var p in $scope.perspectives) {
            perspNames.push($scope.perspectives[p].name);
        }

        var name = 'new';
        var i = 0;
        while (perspNames.indexOf(name) >= 0) {
            i++;
            name = 'new' + i;
        }

        var newPerspective = new Perspective(name, []);

        decoratorFilterService.addPerspective(newPerspective);
        $scope.perspectives.push(newPerspective);
        $scope.perspective = newPerspective;
        $scope.perspectiveChanged();
    };

    $scope.deletePerspective = function() {
        decoratorFilterService.removePerspective($scope.perspective.name);
        $scope.perspectives.splice($scope.perspectives.indexOf($scope.perspective), 1);
        $scope.perspective = $scope.perspectives[0];
        $scope.perspectiveChanged();
    };

    $scope.close = function() {
        $modalInstance.close();
    };
}])


.directive('decoratorFilter', ['$modal', '$translate', 'decoratorFilterService', function($modal, $translate, decoratorFilterService) {
    return {
        templateUrl: 'perspectiveTemplate.html',
        restrict: 'E',
        transclude: true,
        replace: false,
        link: function($scope) {


            var init = function() {
                // get a copy of all perspectives and their decorators
                $scope.perspectives = decoratorFilterService.getPerspectives();

                // find the current (active) perspective in the $scope.perspectives
                // remember $scope.perspectives is a clone of the list, so we need to find the specific instance
                // in the clone, not a copy of it, so the binding can work properly with ng-model in the html
                var currentPerspName = decoratorFilterService.getCurrentPerspective().name;
                for (var key in $scope.perspectives) {
                    if ($scope.perspectives[key].name === currentPerspName) {
                        $scope.perspective = $scope.perspectives[key];
                        break;
                    }
                }
            };
            init();

            var MODES = {
                button: 'button',
                full: 'full'
            };
            var mode = MODES.button;

            $scope.mouseOn = function() {
                mode = MODES.full;
            };

            $scope.mouseOff = function() {
                mode = MODES.button;
            };

            $scope.perspectiveSelected = function(newPersp) {
                $scope.mouseOff();
                $scope.perspective = newPersp;
                decoratorFilterService.setCurrentPerspective($scope.perspective.name);
            };

            $scope.showButtonOnly = function() {
                return mode === MODES.button;
            };

            $scope.manage = function() {
                var modalInstance = $modal.open({
                    templateUrl: 'managePerspectivesTemplate.html',
                    controller: 'PerspectiveUIController'
                        //size: 'sm'
                });

                modalInstance.result.then(function() {
                    init();
                }, function() {
                    init();
                });
            };

        }
    };
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('sanitizeHtmlInputModule', [])
    .directive('sanitizeHtmlInput', function() {
        return {
            restrict: "A",
            replace: false,
            transclude: false,
            priority: 1000,
            link: function($scope, element) {
                element.change(
                    function() {
                        var target = element.val();
                        target = target.replace(new RegExp('{', 'g'), '%7B');
                        target = target.replace(new RegExp('}', 'g'), '%7D');
                        element.val(target);
                    });
            }
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('sakExecutorDecorator', [
        'coretemplates',
        'componentHandlerServiceModule',
        'decoratorServiceModule',
        'yLoDashModule',
        'crossFrameEventServiceModule',
        'seConstantsModule'
    ])
    .factory('sakExecutor', ['$log', '$compile', 'yjQuery', 'lodash', 'componentHandlerService', 'decoratorService', 'ID_ATTRIBUTE', 'ELEMENT_UUID_ATTRIBUTE', function($log, $compile, yjQuery, lodash, componentHandlerService, decoratorService, ID_ATTRIBUTE, ELEMENT_UUID_ATTRIBUTE) {
        var ATTR_DATA = 'data-';
        var ATTR_SMARTEDIT = 'smartedit';
        var ATTR_DATA_SMARTEDIT = ATTR_DATA + ATTR_SMARTEDIT;

        // Array.<{scope: Object, element: Object}>
        var scopes = [];

        /*
         * Validates if a given attribute name present on the decorated element is eligible
         * to be added as a smartedit property.
         */
        var isValidSmartEditAttribute = function(nodeName) {
            return lodash.startsWith(nodeName, ATTR_DATA_SMARTEDIT) || lodash.startsWith(nodeName, ATTR_SMARTEDIT);
        };

        /*
         * Parses the attribute name by removing ATTR_DATA prefix and
         * converting to a camel case string representation.
         */
        var parseAttributeName = function(nodeName) {
            if (lodash.startsWith(nodeName, ATTR_DATA)) {
                nodeName = nodeName.substring(ATTR_DATA.length);
            }
            return lodash.camelCase(nodeName);
        };

        var getElementIndex = function(element) {
            var itemIndex = scopes.findIndex(function(item) {
                return componentHandlerService.getFromSelector(item.element).attr(ELEMENT_UUID_ATTRIBUTE) === componentHandlerService.getFromSelector(element).attr(ELEMENT_UUID_ATTRIBUTE);
            });
            return itemIndex;
        };

        return {
            getScopes: function() {
                return scopes;
            },

            wrapDecorators: function(transcludeFn, smarteditComponentId, smarteditComponentType) {
                return decoratorService.getDecoratorsForComponent(smarteditComponentType, smarteditComponentId).then(function(decorators) {
                    var template = "<div data-ng-transclude></div>";

                    decorators.forEach(function(decorator) {
                        template = "<div class='" + decorator + " se-decorator-wrap' data-active='active' data-smartedit-component-id='{{smarteditComponentId}}' " +
                            "data-smartedit-component-type='{{smarteditComponentType}}' data-smartedit-container-id='{{smarteditContainerId}}' " +
                            "data-smartedit-container-type='{{smarteditContainerType}}' data-component-attributes='componentAttributes'>" + template;
                        template += "</div>";
                    });

                    return $compile(template, transcludeFn);
                });
            },

            registerScope: function(scope, element) {
                scopes.push({
                    scope: scope,
                    element: angular.copy(element)
                });
            },

            destroyScope: function(element) {
                var itemIndex = getElementIndex(element);
                if (itemIndex !== -1) {
                    scopes[itemIndex].scope.$destroy();
                    scopes.splice(itemIndex, 1);
                } else {
                    $log.warn('sakExecutor::destroyScope failed to retrieve element:', yjQuery(element).attr(ID_ATTRIBUTE), yjQuery(element).attr(ELEMENT_UUID_ATTRIBUTE));
                }
            },

            destroyAllScopes: function() {
                scopes.forEach(function(item) {
                    item.scope.$destroy();
                });
                scopes = [];
            },

            prepareScope: function(scope, element) {
                this.registerScope(scope, element);
                var attributes = {};
                Array.prototype.slice.apply(element.get(0).attributes).forEach(function(node) {
                    var attrName = node.nodeName;
                    if (isValidSmartEditAttribute(attrName)) {
                        attrName = parseAttributeName(attrName);
                        attributes[attrName] = node.nodeValue;
                    }
                });

                scope.componentAttributes = attributes;
            }
        };
    }])
    .controller('smartEditComponentXController', ['$element', '$scope', '$transclude', '$q', '$compile', '$rootScope', 'sakExecutor', 'crossFrameEventService', 'EVENT_PERSPECTIVE_REFRESHED', function($element, $scope, $transclude, $q, $compile, $rootScope, sakExecutor, crossFrameEventService, EVENT_PERSPECTIVE_REFRESHED) {
        this.$postLink = function() {
            $scope.active = false;

            var compiledElement;
            var elementScope;

            this.unregisterPerspectiveRefreshedEvent = crossFrameEventService.subscribe(EVENT_PERSPECTIVE_REFRESHED, function() {
                elementScope.$destroy();
                $element.get(0).removeChild(compiledElement.get(0));
                sakExecutor.wrapDecorators($transclude, $scope.smarteditComponentId, $scope.smarteditComponentType).then(function(compiled) {
                    elementScope = $scope.$new(false);
                    $element.append(compiled(elementScope));
                });
                return $q.when();
            });

            $transclude($scope, function() {
                sakExecutor.wrapDecorators($transclude, $scope.smarteditComponentId, $scope.smarteditComponentType).then(function(compiled) {
                    elementScope = $scope.$new(false);
                    compiledElement = compiled(elementScope);
                    $element.append(compiledElement);

                    sakExecutor.prepareScope($scope, $element);

                    var inactivateDecorator = function() {
                        $scope.active = false;
                    };

                    var activateDecorator = function() {
                        $scope.active = true;
                    };

                    // Register Event Listeners
                    $element.bind("mouseleave", function() {
                        $rootScope.$apply(inactivateDecorator);
                    });

                    $element.bind("mouseenter", function() {
                        $rootScope.$apply(activateDecorator);
                    });

                });
            });
        };
        this.$onDestroy = function() {
            this.unregisterPerspectiveRefreshedEvent();
        };
    }])
    .directive('smartEditComponentX', function() {
        return {
            restrict: 'C',
            transclude: true,
            replace: false,
            scope: {
                smarteditComponentId: '@',
                smarteditComponentType: '@',
                smarteditContainerId: '@',
                smarteditContainerType: '@'
            },
            controller: 'smartEditComponentXController',
            controllerAs: 'ctrl'
        };
    });

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('decoratorServiceModule', ['functionsModule'])
    .factory('DecoratorServiceClass', ['$q', 'uniqueArray', 'hitch', 'regExpFactory', function($q, uniqueArray, hitch, regExpFactory) {

        // Constants

        var DecoratorServiceClass = function() {
            this._activeDecorators = {};
            this.componentDecoratorsMap = {};
        };

        /**
         * @ngdoc method
         * @name decoratorServiceModule.service:decoratorService#addMappings
         * @methodOf decoratorServiceModule.service:decoratorService
         * @description
         * This method enables a list of decorators for a group of component types.
         * The list to be {@link decoratorServiceModule.service:decoratorService#methods_enable enable} is identified by a matching pattern.
         * The list is enabled when a perspective or referenced perspective that it is bound to is activated/enabled.
         * @param {Object} map A key-map value; the key is the matching pattern and the value is an array of decorator keys. The key can be an exact type, an ant-like wild card, or a full regular expression:
         * <pre>
         * decoratorService.addMappings({
            '*Suffix': ['decorator1', 'decorator2'],
            '.*Suffix': ['decorator2', 'decorator3'],
            'MyExactType': ['decorator3', 'decorator4'],
            '^((?!Middle).)*$': ['decorator4', 'decorator5']
        	});
         * </pre>
         */
        DecoratorServiceClass.prototype.addMappings = function(map) {

            for (var regexpKey in map) {
                var decoratorsArray = map[regexpKey];
                this.componentDecoratorsMap[regexpKey] = uniqueArray((this.componentDecoratorsMap[regexpKey] || []), decoratorsArray);
            }

        };

        /**
         * @ngdoc method
         * @name decoratorServiceModule.service:decoratorService#enable
         * @methodOf decoratorServiceModule.service:decoratorService
         * @description
         * Enables a decorator
         * 
         * @param {String} decoratorKey The key that uniquely identifies the decorator.
         * @param {Function} displayCondition Returns a promise that will resolve to a boolean that determines whether the decorator will be displayed.
         */
        DecoratorServiceClass.prototype.enable = function(decoratorKey, displayCondition) {

            if (!(decoratorKey in this._activeDecorators)) {
                this._activeDecorators[decoratorKey] = {
                    'displayCondition': displayCondition
                };
            }
        };
        /**
         * @ngdoc method
         * @name decoratorServiceModule.service:decoratorService#disable
         * @methodOf decoratorServiceModule.service:decoratorService
         * @description
         * Disables a decorator
         * 
         * @param {String} decoratorKey the decorator key
         */
        DecoratorServiceClass.prototype.disable = function(decoratorKey) {
            if (this._activeDecorators[decoratorKey]) {
                delete this._activeDecorators[decoratorKey];
            }
        };

        /**
         * @ngdoc method
         * @name decoratorServiceModule.service:decoratorService#getDecoratorsForComponent
         * @methodOf decoratorServiceModule.service:decoratorService
         * @description
         * This method retrieves a list of decorator keys that is eligible for the specified component type.
         * The list retrieved depends on which perspective is active.
         *
         * This method uses the list of decorators enabled by the {@link decoratorServiceModule.service:decoratorService#methods_addMappings addMappings} method.
         *
         * @param {String} componentType The type of the component to be decorated.
         * @param {String} componentId The id of the component to be decorated.
         * @returns {Promise} A promise that resolves to a list of decorator keys.
         *
         */
        DecoratorServiceClass.prototype.getDecoratorsForComponent = function(componentType, componentId) {
            return this._getDecorators(componentType, componentId);
        };

        DecoratorServiceClass.prototype._getDecorators = function(componentType, componentId) {
            var decoratorArray = [];
            if (this.componentDecoratorsMap) {
                for (var regexpKey in this.componentDecoratorsMap) {
                    if (regExpFactory(regexpKey).test(componentType)) {
                        decoratorArray = uniqueArray(decoratorArray, this.componentDecoratorsMap[regexpKey]);
                    }
                }
            }

            var promisesToResolve = [];
            var displayedDecorators = [];
            decoratorArray.forEach(function(dec) {
                var activeDecorator = this._activeDecorators[dec];
                if (activeDecorator && activeDecorator.displayCondition) {
                    if (typeof activeDecorator.displayCondition !== 'function') {
                        throw new Error("The active decorator's displayCondition property must be a function and must return a boolean");
                    }

                    var deferred = $q.defer();
                    activeDecorator.displayCondition(componentType, componentId).then(function(display) {
                        if (display) {
                            deferred.resolve(dec);
                        } else {
                            deferred.resolve(null);
                        }
                    });

                    promisesToResolve.push(deferred.promise);
                } else if (activeDecorator) {
                    displayedDecorators.push(dec);
                }
            }.bind(this));

            return $q.all(promisesToResolve).then(function(decoratorsEnabled) {
                return displayedDecorators.concat(decoratorsEnabled.filter(function(dec) {
                    return dec;
                }));
            });
        };

        return DecoratorServiceClass;
    }])
    /**
     * @ngdoc service
     * @name decoratorServiceModule.service:decoratorService
     *
     * @description
     * This service enables and disables decorators. It also maps decorators to SmartEdit component types–regardless if they are enabled or disabled.
     * 
     */
    .factory('decoratorService', ['DecoratorServiceClass', function(DecoratorServiceClass) {
        return new DecoratorServiceClass();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("positionRegistryModule", ['componentHandlerServiceModule'])
    /*
     * Service aimed at determining the list of registered DOM elements that have been repositioned, regardless of how, since it was last queried
     */
    .service("positionRegistry", ['componentHandlerService', function(componentHandlerService) {

        var positionRegistry = [];

        function _floor(val) {
            return Math.floor(val * 100) / 100;
        }

        var _calculatePositionHash = function(element) {
            var rootPosition = componentHandlerService.getFromSelector('body')[0].getBoundingClientRect();
            var position = element.getBoundingClientRect();
            return _floor(position.top - rootPosition.top) + "_" + _floor(position.left - rootPosition.left);
        };

        /*
         * registers a given node in the repositioning registry
         */
        this.register = function(element) {
            this.unregister(element);
            positionRegistry.push({
                element: element,
                position: _calculatePositionHash(element)
            });
        }.bind(this);

        /*
         * unregisters a given node from the repositioning registry
         */
        this.unregister = function(element) {
            var entryToBeRemoved = positionRegistry.find(function(entry) {
                return entry.element === element;
            });

            var index = positionRegistry.indexOf(entryToBeRemoved);
            if (index > -1) {
                positionRegistry.splice(index, 1);
            }
        };

        /*
         * Method returning the list of nodes having been repositioned since last query
         */
        this.getRepositionedComponents = function() {
            return positionRegistry
                .filter(function(entry) {
                    //to ignore elements that would keep showing here because of things like display table-inline
                    return componentHandlerService.getFromSelector(entry.element).height() !== 0;
                })
                .filter(function(entry) {
                    var element = entry.element;
                    var newPosition = _calculatePositionHash(element);
                    if (newPosition !== entry.position) {
                        entry.position = newPosition;
                        return true;
                    } else {
                        return false;
                    }
                }).map(function(entry) {
                    return entry.element;
                });
        };


        /*
         * unregisters all nodes and cleans up
         */
        this.dispose = function() {
            positionRegistry = [];
        };

        /*
         * for e2e test purposes
         */
        this._listenerCount = function() {
            return positionRegistry.length;
        };


    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("resizeListenerModule", ['yjqueryModule'])
    /*
     * interval at which manual resizeListener will try to get a reference on resize containers and clone them for clean unregistering
     */
    .constant("RESIZE_LISTENER_REPROCESS_TIMEOUT", 500)
    /*
     * Service that wraps element-resize-detector from git://github.com/wnr/element-resize-detector.git#v1.1.12
     */
    .service("resizeListener", ['$window', '$document', '$interval', 'yjQuery', 'RESIZE_LISTENER_REPROCESS_TIMEOUT', function($window, $document, $interval, yjQuery, RESIZE_LISTENER_REPROCESS_TIMEOUT) {

        var resizeListenersRegistry = [];

        // default strategy is object. using 'scroll' strategy that is more performant.
        // see https://github.com/wnr/element-resize-detector
        var erd = $window.elementResizeDetectorMaker({
            strategy: "scroll"
        });

        var internalStatePropertyName = "_erd";
        /*
         * unregisters the resize listener of a given node
         */
        this.unregister = function(element) {

            /*
             * at this stage, a DOM manipulation driven by the storefront may have removed
             * the erd_scroll_detection_container added by erd library at register time, which will cause uninstall to fail
             * we then re-add it before uninstalling
             * 
             * Note: 
             *  It is necessary to check whether the document contains the container retrieved. However, 
             *  IE11 does not support the method contains for nodes, only for elements. Thus, it would throw an exception. 
             *  It is necessary to use yjQuery to be consistent with all browsers.
             */
            var entry = resizeListenersRegistry.find(function(entry) {
                return entry.element === element;
            });
            if (entry) {
                var container = element[internalStatePropertyName] ? element[internalStatePropertyName].container : undefined;
                if (container && !yjQuery.contains($document[0], container)) {
                    /*
                     *  in IE11, the container returns with no children, we then need to restore its children before reappending it to the element
                     */
                    if (!container.hasChildNodes() && entry.containerClone && entry.containerClone.hasChildNodes()) {
                        Array.prototype.slice.call(entry.containerClone.childNodes).forEach(function(child) {
                            container.appendChild(child);
                        });
                    }

                    element.appendChild(container);
                }

                erd.uninstall(element);

                resizeListenersRegistry.splice(resizeListenersRegistry.indexOf(entry), 1);
            }
        }.bind(this);



        /*
         * registers a resize listener of a given node, at this stage the containers are not created yet by the underlying library
         */
        this.register = function(element, listener) {
            erd.listenTo(element, listener);
            resizeListenersRegistry.push({
                element: element,
                containerClone: null
            });
        };

        var missingClonesListener = $interval(function() {
            resizeListenersRegistry.forEach(function(entry) {
                var container = entry.element[internalStatePropertyName].container;
                if (container && !entry.containerClone) {
                    entry.containerClone = container.cloneNode(true);
                }
            });
        }, RESIZE_LISTENER_REPROCESS_TIMEOUT);

        /*
         * unregisters listeners on all nodes and cleans up
         */
        this.dispose = function() {
            $interval.cancel(missingClonesListener);
            var registryCopy = resizeListenersRegistry.slice();
            registryCopy.forEach(function(entry) {
                this.unregister(entry.element);
            }.bind(this));
        };

        /*
         * for e2e test purposes
         */
        this._listenerCount = function() {
            return resizeListenersRegistry.length;
        };
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('seNamespaceModule', [])

.service('seNamespace', ['$log', function($log) {

    this._namespaceHasFunction = function(propName) {
        var nsp = this._getNamespace();
        return (nsp && typeof nsp[propName] === 'function');
    };

    this._getNamespace = function() {
        return window.smartedit;
    };

    this.reprocessPage = function() {
        if (this._getNamespace() && typeof this._getNamespace().reprocessPage === 'function') {
            return this._getNamespace().reprocessPage();
        } else {
            $log.warn('No reprocessPage function defined on smartedit namespace');
        }
    };

    // explain slot for multiple instances of component scenario
    this.renderComponent = function(componentId, componentType, parentId) {
        if (this._getNamespace() && typeof this._getNamespace().renderComponent === 'function') {
            return this._getNamespace().renderComponent(componentId, componentType, parentId);
        } else {
            return false;
        }
    };


}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module("smartEditContractChangeListenerModule", [
        'yjqueryModule',
        'yLoDashModule',
        'timerModule',
        'componentHandlerServiceModule',
        'resizeListenerModule',
        'positionRegistryModule'
    ])
    /*
     * interval at which manual listening/checks are executed
     * So far it is only by repositionListener
     * (resizeListener delegates to a self-contained third-party library and DOM mutations observation is done in native MutationObserver
     */
    .constant("REPROCESS_TIMEOUT", 100)


/*
 *  Fetches the HTML root element to observer for changes
 *  Exposed as a service for extensibility / optimization
 */
.service('smartEditContractChangeListenerObservableRoot', function() {
    this.getObservableRoot = function() {
        return document.getElementsByTagName('body')[0];
    };
})

/*
 * service that allows specifying callbacks for all events affecting elements of the smartEdit storefront contract:
 * - page identifier change
 * - smartEdit components added or removed
 * - contract bound attributes of smartEdit components changes
 * - smartEdit components repositioned
 * - smartEdit components resized
 */
.service("smartEditContractChangeListener", ['$log', '$interval', '$rootScope', '$document', 'yjQuery', 'lodash', 'componentHandlerService', 'resizeListener', 'positionRegistry', 'smartEditContractChangeListenerObservableRoot', 'REPROCESS_TIMEOUT', 'COMPONENT_CLASS', 'TYPE_ATTRIBUTE', 'ID_ATTRIBUTE', 'UUID_ATTRIBUTE', function($log, $interval, $rootScope, $document,
    yjQuery,
    lodash,
    componentHandlerService,
    resizeListener,
    positionRegistry,
    smartEditContractChangeListenerObservableRoot,
    REPROCESS_TIMEOUT,
    COMPONENT_CLASS,
    TYPE_ATTRIBUTE,
    ID_ATTRIBUTE,
    UUID_ATTRIBUTE) {

    /*
     * list of smartEdit component attributes the change of which we observe to trigger an onComponentChanged event
     */
    var smartEditAttributeNames = [TYPE_ATTRIBUTE, ID_ATTRIBUTE, UUID_ATTRIBUTE];

    /*
     * Mutation object (return in a list of mutations in mutation event) can be of different types.
     * We are here only interested in type attributes (used for onPageChanged and onComponentChanged events) and childList (used for onComponentRemoved and onComponentAdded events)
     */
    var MUTATION_TYPES = {
        CHILD_LIST: {
            NAME: "childList",
            REMOVE_OPERATION: "removedNodes",
            ADD_OPERATION: "addedNodes"
        },
        ATTRIBUTES: {
            NAME: "attributes"
        }
    };

    /*
     * Of All the DOM node types we only care for 1 and 2 (Element and Attributes)
     */
    var NODE_TYPES = {
        ELEMENT: 1,
        ATTRIBUTE: 2,
        TEXT: 3
    };

    /*
     * This is the configuration passed to the MutationObserver instance
     */
    var OBSERVER_OPTIONS = {
        /*
         * enables observation of attribute mutations
         */
        attributes: true,
        /*
         * instruct the observer to keep in store the former values of the mutated attributes
         */
        attributeOldValue: true,
        /*
         * enables observation of addition and removal of nodes
         */
        childList: true,
        characterData: false,
        /*
         * enables recursive lookup without which only addition and removal of DIRECT children of the observed DOM root would be collected
         */
        subtree: true
    };

    /*
     * unique instance of a MutationObserver on the body (enough since subtree:true)
     */
    var mutationObserver;
    /*
     * unique instance of a custom listener for repositioning invoking the positionRegistry
     */
    var repositionListener;

    /*
     * holder of the current value of the page since previous onPageChanged event
     */
    var currentPage;

    /*
     * nullable callbacks provided to smartEditContractChangeListener for all the observed events
     */
    this._componentAddedCallback = null;
    this._componentRemovedCallback = null;
    this._componentResizedCallback = null;
    this._componentRepositionedCallback = null;
    this._onComponentChangedCallback = null;
    this._pageChangedCallback = null;

    /*
     * Method used in mutationObserverCallback that extracts from mutations the list of nodes added or removed
     * The nodes are returned within a pair along with their nullable closest smartEditComponent parent
     */
    var aggregateAddOrRemoveNodesAndTheirParents = function(mutations, addOrRemoveOperation) {

        var entries = lodash.flatten(mutations.filter(function(mutation) {
            //only keep mutations of type childList and for the selected addOrRemoveOperation (removedNodes or addedNodes)
            return mutation.type === MUTATION_TYPES.CHILD_LIST.NAME && mutation[addOrRemoveOperation].length;
        }).map(function(mutation) {

            //the mutated child may not be smartEditComponent, in such case we return their first level smartEditComponent children
            var children = lodash.flatten(Array.prototype.slice.call(mutation[addOrRemoveOperation])
                .filter(function(node) {
                    return node.nodeType === NODE_TYPES.ELEMENT;
                }).map(function(child) {
                    return componentHandlerService.isSmartEditComponent(child) ? child : Array.prototype.slice.call(componentHandlerService.getFirstSmartEditComponentChildren(child));
                }));

            // nodes are returned in pairs with their nullable parent
            var parents = componentHandlerService.getClosestSmartEditComponent(mutation.target);
            return children.map(function(node) {
                return {
                    node: node,
                    parent: parents.length ? parents[0] : null
                };
            });
        }));

        /*
         * Despite MutationObserver specifications it so happens that sometimes,
         * depending on the very way a parent node is added with its children,
         * parent AND children will appear in a same mutation. We then must only keep the parent
         * Since the parent will appear first, the filtering lodash.uniqWith will always return the parent as opposed to the child which is what we need
         */

        return lodash.uniqWith(entries, function(entry1, entry2) {
            return entry1.node.contains(entry2.node) || entry2.node.contains(entry1.node);
        });
    };

    /*
     * Method used in mutationObserverCallback that extracts from mutations the list of nodes the smartEdit contract attributes of which have changed
     * The nodes are returned within a pair along with their map of changed attributes
     */
    var aggregateMutationsOnSmartEditAttributes = function(mutations) {
        return mutations.filter(function(mutation) {
            return mutation.target.nodeType === NODE_TYPES.ELEMENT && componentHandlerService.isSmartEditComponent(mutation.target) && mutation.type === MUTATION_TYPES.ATTRIBUTES.NAME && smartEditAttributeNames.indexOf(mutation.attributeName) > -1;
        }).reduce(function(seed, mutation) {
            var targetEntry = seed.find(function(entry) {
                return entry.node === mutation.target;
            });
            if (!targetEntry) {
                targetEntry = {
                    node: mutation.target,
                    oldAttributes: {}
                };
                seed.push(targetEntry);
            }
            targetEntry.oldAttributes[mutation.attributeName] = mutation.oldValue;
            return seed;
        }, []);
    };

    /*
     * Methods used in mutationObserverCallback that determines whether the smartEdit contract page identifier MAY have changed in the DOM
     */
    var mutationsHasPageChange = function(mutations) {
        return mutations.find(function(mutation) {
            return mutation.type === MUTATION_TYPES.ATTRIBUTES.NAME && mutation.target.tagName === "BODY" && mutation.attributeName === "class";
        });
    };

    /*
     * convenience method to invoke a callback on a node and recursively on all its smartEditComponent children
     */
    var applyToSelfAndAllChildren = function(node, callback) {
        callback(node);
        Array.prototype.slice.call(componentHandlerService.getFirstSmartEditComponentChildren(node)).forEach(function(component) {
            applyToSelfAndAllChildren(component, callback);
        });
    };


    var repairParentResizeListener = function(childAndParent) {
        if (childAndParent.parent) {
            //the adding of a component is likely to destroy the DOM added by the resizeListener on the parent, it needs be restored
            resizeListener.unregister(childAndParent.parent);
            if (yjQuery.contains($document[0], childAndParent.parent)) {
                resizeListener.register(childAndParent.parent, this._componentResizedCallback.bind(undefined, childAndParent.parent));
                this._componentResizedCallback(childAndParent.parent);
            }
        }
    }.bind(this);

    /*
     * when a callback is executed we make sure that angular is synchronized since it is occurring outside the life cycle
     */
    var executeCallback = function(callback) {
        $rootScope.$evalAsync(callback);
    };

    /*
     * callback executed by the mutation observer every time mutations occur.
     * repositioning and resizing are not part of this except that every time a smartEditComponent is added,
     * it is registered within the positionRegistry and the resizeListener 
     */
    var mutationObserverCallback = function(mutations) {
        $log.debug(mutations);

        if (this._pageChangedCallback && mutationsHasPageChange(mutations)) {
            var newPageUUID = componentHandlerService.getPageUUID();
            if (currentPage !== newPageUUID) {
                executeCallback(this._pageChangedCallback.bind(undefined, newPageUUID));
            }
            currentPage = newPageUUID;
        }
        if (this._componentRemovedCallback) {
            aggregateAddOrRemoveNodesAndTheirParents(mutations, MUTATION_TYPES.CHILD_LIST.REMOVE_OPERATION).forEach(function(childAndParent) {
                //we unregister repositioning and resizing listeners on all the subtree of the removed smartEditComponent
                if (this._componentRepositionedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, positionRegistry.unregister);
                }
                if (this._componentResizedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, resizeListener.unregister);

                    repairParentResizeListener(childAndParent);
                }
                //the onComponentRemoved is called with the top of smartEditComponent subtree being removed and its nullable closest smartEditComponent parent
                executeCallback(this._componentRemovedCallback.bind(undefined, childAndParent.node, childAndParent.parent));
            }.bind(this));
        }
        if (this._componentAddedCallback) {
            aggregateAddOrRemoveNodesAndTheirParents(mutations, MUTATION_TYPES.CHILD_LIST.ADD_OPERATION).forEach(function(childAndParent) {
                //we register repositioning and resizing listeners on all the subtree of the removed smartEditComponent
                if (this._componentRepositionedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, positionRegistry.register);
                }
                if (this._componentResizedCallback) {
                    applyToSelfAndAllChildren(childAndParent.node, function(node) {
                        resizeListener.register(node, this._componentResizedCallback.bind(undefined, node));
                    }.bind(this));

                    repairParentResizeListener(childAndParent);
                }
                //the onComponentAdded is called with the top of smartEditComponent subtree being added
                executeCallback(this._componentAddedCallback.bind(undefined, childAndParent.node));
            }.bind(this));
        }

        if (this._onComponentChangedCallback) {
            aggregateMutationsOnSmartEditAttributes(mutations).forEach(function(entry) {
                //the onComponentChanged is called with the mutated smartEditComponent subtree and the map of old attributes
                executeCallback(this._onComponentChangedCallback.bind(undefined, entry.node, entry.oldAttributes));
            }.bind(this));
        }

    };

    /*
     * wrapping for test purposes
     */
    this._newMutationObserver = function(callback) {
        return new MutationObserver(callback);
    };

    /*
     * initializes and starts all DOM listeners:
     * - DOM mutations on smartEditComponents and page identifier (by Means of native MutationObserver)
     * - smartEditComponents repositioning (by means of querying, with an interval, the list of repositioned components from the positionRegistry)
     * - smartEditComponents resizing (by delegating to the injected resizeListener)
     */
    this.initListener = function() {

        try {
            currentPage = componentHandlerService.getPageUUID();
            if (this._pageChangedCallback) {
                executeCallback(this._pageChangedCallback.bind(undefined, currentPage));
            }
        } catch (e) {
            //case when the page that has just loaded is an asynchronous one
        }

        if (!mutationObserver) {
            mutationObserver = this._newMutationObserver(mutationObserverCallback.bind(this));
            mutationObserver.observe(smartEditContractChangeListenerObservableRoot.getObservableRoot(), OBSERVER_OPTIONS);

            // Start listening for DOM resize and repositioning (for pre-existing nodes as opposed to the ones added on the fly)
            componentHandlerService.getFromSelector("." + COMPONENT_CLASS).each(function(index, component) {
                // TODO: compile pre-existing nodes if and when renderService stops doing it;
                if (this._componentResizedCallback) {
                    resizeListener.register(component, this._componentResizedCallback.bind(undefined, component));
                }
                if (this._componentRepositionedCallback) {
                    positionRegistry.register(component);
                }
            }.bind(this));

            // Initiate polling for DOM repositioning
            if (this._componentRepositionedCallback) {
                repositionListener = $interval(function() {
                    positionRegistry.getRepositionedComponents().forEach(function(component) {
                        this._componentRepositionedCallback(component);
                    }.bind(this));

                }.bind(this), REPROCESS_TIMEOUT);
            }
        }
    };

    /*
     * stops and clean up all listeners
     */
    this.stopListener = function() {
        // Stop listening for DOM mutations
        if (mutationObserver) {
            mutationObserver.disconnect();
        }
        mutationObserver = null;
        // Stop listening for DOM resize
        resizeListener.dispose();
        // Stop listening for DOM repositioning
        if (repositionListener) {
            $interval.cancel(repositionListener);
            repositionListener = null;
        }
        positionRegistry.dispose();
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is added to the DOM
     * it is executed only once per subtree of smarteditComponent nodes being added
     * the callback is invoked with the root node of a subtree
     */
    this.onComponentAdded = function(callback) {
        this._componentAddedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is removed from the DOM
     * it is executed only once per subtree of smarteditComponent nodes being removed
     * the callback is invoked with the root node of a subtree and its parent
     */
    this.onComponentRemoved = function(callback) {
        this._componentRemovedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time at least one of the smartEdit contract attributes of a smarteditComponent node is changed
     * the callback is invoked with the mutated node itself and the map of old attributes
     */
    this.onComponentChanged = function(callback) {
        this._onComponentChangedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is resized in the DOM
     * the callback is invoked with the resized node itself
     */
    this.onComponentResized = function(callback) {
        this._componentResizedCallback = callback;
    };

    /*
     * registers a unique callback to be executed every time a smarteditComponent node is repositioned (as per Node.getBoundingClientRect()) in the DOM
     * the callback is invoked with the resized node itself
     */
    this.onComponentRepositioned = function(callback) {
        this._componentRepositionedCallback = callback;
    };

    /*
     * registers a unique callback to be executed:
     * - upon bootstrapping smartEdit IF the page identifier is available
     * - every time the page identifier is changed in the DOM (see componentHandlerService.getPageUUID())
     * the callback is invoked with the new page identifier read from componentHandlerService.getPageUUID()
     */
    this.onPageChanged = function(callback) {
        this._pageChangedCallback = callback;
    };

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name alertServiceModule
 * @description
 * Smartedit has only an empty (proxied) implementation of alertService.<br />
 * See the alertServiceModule under SmartEdit Container for further details.
 */
angular.module('alertServiceModule', ['alertsBoxModule', 'gatewayProxyModule'])

.constant("SE_ALERT_SERVICE_GATEWAY_ID", 'SE_ALERT_SERVICE_GATEWAY_ID')

/**
 * @ngdoc service
 * @name alertServiceModule.alertService
 * @description
 * Smartedit has only an empty (proxied) implementation of alertService.<br />
 * See the alertServiceModule under SmartEdit Container for further details.
 */
.factory('alertService', ['gatewayProxy', 'SE_ALERT_SERVICE_GATEWAY_ID', function(gatewayProxy, SE_ALERT_SERVICE_GATEWAY_ID) {

    var AlertService = function() {

        gatewayProxy.initForService(this, null, SE_ALERT_SERVICE_GATEWAY_ID);

    };

    AlertService.prototype.showAlert = function() {};

    AlertService.prototype.showInfo = function() {};

    AlertService.prototype.showDanger = function() {};

    AlertService.prototype.showWarning = function() {};

    AlertService.prototype.showSuccess = function() {};


    // LEGACY!!! - should use showXY functions above

    AlertService.prototype.pushAlerts = function() {};

    AlertService.prototype.removeAlertById = function() {};

    return new AlertService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('authenticationModule', ['authenticationInterfaceModule', 'gatewayProxyModule'])

.factory('authenticationService', ['gatewayProxy', 'AuthenticationServiceInterface', 'extend', function(gatewayProxy, AuthenticationServiceInterface, extend) {

    var AuthenticationService = function() {
        this.reauthInProgress = {};
        this.gatewayId = "authenticationService";
        gatewayProxy.initForService(this);
    };

    AuthenticationService = extend(AuthenticationServiceInterface, AuthenticationService);

    return new AuthenticationService();

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('catalogServiceModule', ['catalogServiceInterfaceModule', 'gatewayProxyModule'])
    .factory('catalogService', ['CatalogServiceInterface', 'extend', 'gatewayProxy', function(CatalogServiceInterface, extend, gatewayProxy) {

        var CatalogService = function() {
            this.gatewayId = "catalogService";
            gatewayProxy.initForService(this);
        };
        CatalogService = extend(CatalogServiceInterface, CatalogService);

        return new CatalogService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name confirmationModalServiceModule
 * @description
 * # The confirmationModalServiceModule
 *
 * The confirmation modal service module provides a service that allows opening a confirmation (an OK/Cancel prompt with
 * a title and content) within a modal.
 *
 * This module is dependent on the {@link modalServiceModule modalServiceModule}.
 */
angular.module('confirmationModalServiceModule', ['gatewayProxyModule'])

/**
 * @ngdoc service
 * @name confirmationModalServiceModule.service:confirmationModalService
 *
 * @description
 * Service used to open a confirmation modal in which an end-user can confirm or cancel an action. A confirmation modal
 * consists of a title, content, and an OK and cancel button. This modal may be used in any context in which a
 * confirmation is required.
 */
.factory('confirmationModalService', ['gatewayProxy', function(gatewayProxy) {

    function ConfirmationModalService() {
        this.gatewayId = 'confirmationModal';
        gatewayProxy.initForService(this, ["confirm"]);
    }

    /**
     * @ngdoc method
     * @name confirmationModalServiceModule.service:confirmationModalService#open
     * @methodOf confirmationModalServiceModule.service:confirmationModalService
     *
     * @description
     * Uses the {@link modalServiceModule.modalService modalService} to open a confirmation modal.
     *
     * The confirmation modal is initialized by a default i18N key as a title or by an override title passed through the
     * input configuration object.
     *
     * @param {Object} configuration Configuration for the confirmation modal
     * @param {String} configuration.title The override title for the confirmation modal. If a title is provided, it
     * overrides the default title, which is an i18n key. This property is optional.
     * @param {String} configuration.description The description to be used as the content of the confirmation modal.
     * This is the text that is displayed to the end user.
     *
     * @returns {Promise} A promise that is resolved when the OK button is actioned or is rejected when the Cancel
     * button is actioned.
     */
    ConfirmationModalService.prototype.confirm = function() {};

    return new ConfirmationModalService();

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('contextualMenuServiceModule', ['functionsModule', 'eventServiceModule'])
    /**
     * @ngdoc object
     * @name contextualMenuServiceModule.object:REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT
     * @description
     * Name of the event triggered whenever SmartEdit decides to update items in contextual menus.
     */
    .constant('REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT', 'REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT')

/**
 * @ngdoc service
 * @name contextualMenuServiceModule.ContextualMenuServiceClass
 *
 * @description
 * The ContextualMenuServiceClass is used to add contextual menu items for each component.
 *
 * To add items to the contextual menu, you must call the addItems method of the contextualMenuService and pass a map
 * of the component-type array of contextual menu items mapping. The component type names are the keys in the mapping.
 * The component name can be the full name of the component type, an ant-like wildcard (such as  *middle*Suffix), or a
 * valid regex that includes or excludes a set of component types.
 *
 */
.factory('ContextualMenuServiceClass', ['$q', 'REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT', 'hitch', 'uniqueArray', 'regExpFactory', 'systemEventService', function($q, REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT, hitch, uniqueArray, regExpFactory, systemEventService) {

    var ContextualMenuServiceClass = function() {
        this.contextualMenus = {};
    };

    var initContextualMenuItems = function(map, componentType) {

        var componentTypeContextualMenus = map[componentType].filter(function(item) {
            this._validateItem(item);
            if (!item.key) {
                throw new Error("Item doesn't have key.");
            }

            if (this._featuresList.indexOf(item.key) !== -1) {
                throw new Error("Item with that key already exist.");
            }
            return true;
        }.bind(this));

        this.contextualMenus[componentType] = uniqueArray((this.contextualMenus[componentType] || []), componentTypeContextualMenus);
    };

    var getFeaturesList = function(contextualMenus) {
        // Would be better to use a set for this, but it's not currently supported by all browsers.
        var featuresList = [];
        for (var key in contextualMenus) {
            featuresList = featuresList.concat(contextualMenus[key].map(function(entry) {
                return entry.key;
            }));
        }

        return featuresList.reduce(function(previous, current) {
            if (previous.indexOf(current) === -1) {
                previous.push(current);
            }

            return previous;
        }, []);
    };

    var getUniqueItemArray = function(array1, array2) {
        var currItem;
        var notEqualToCurrentItem = function(item) {
            return (currItem.key !== item.key);
        };

        array2.forEach(function(item) {
            currItem = item;
            if (array1.every(notEqualToCurrentItem)) {
                array1.push(currItem);
            }
        });

        return array1;
    };

    ContextualMenuServiceClass.prototype._validateItem = function(item) {

        // fix legacy callback param, deprecated in 6.5
        if (item.callback && !item.action) {
            item.action = {
                callback: item.callback
            };
            delete item.callback;
        }

        if (!item.action) {
            throw "Contextual menu item must provide an action: " + item;
        }
        if ((!!item.action.callback ^ !!item.action.template ^ !!item.action.templateUrl) !== 1) {
            throw "Contextual menu item must have exactly ONE of action callback|template|templateUrl";
        }
    };

    /**
     * @ngdoc method
     * @name contextualMenuServiceModule.ContextualMenuServiceClass#addItems
     * @methodOf contextualMenuServiceModule.ContextualMenuServiceClass
     *
     * @description
     * The method called to add contextual menu items to component types in the SmartEdit application.
     * The contextual menu items are then retrieved by the contextual menu decorator to wire the set of menu items to the specified component.
     *
     * Sample Usage:
     * <pre>
     * contextualMenuService.addItems({
     * '.*Component': [{
     *  key: 'itemKey',
     *  i18nKey: 'CONTEXTUAL_MENU',
     *  condition: function(componentType, componentId) {
     * 	return componentId === 'ComponentType';
     * 	},
     *  callback: function(componentType, componentId) {
     * 	alert('callback for ' + componentType + "_" + componentId);
     * 	},
     *  displayClass: 'democlass',
     *  iconIdle: '.../icons/icon.png',
     *  iconNonIdle: '.../icons/icon.png',
     * }]
     * });
     * </pre>
     *
     * @param {Object} contextualMenuItemsMap An object containing list of componentType to contextual menu items mapping
     *
     * The object contains a list that maps component types to arrays of contextual menu items. The mapping is a key-value pair.
     * The key is the name of the component type, for example, Simple Responsive Banner Component, and the value is an array of contextual menu items, like add, edit, localize, etc.
     *
     * The name of the component type is the key in the mapping. The name can be the full name of the component type, an ant-like wildcard (such as *middle), or a vlide regex that includes or excludes a set of component types.
     * The value in the mapping is an array of contextual menu items to be activated for the component type match.
     *
     * The contextual menu items can have the following properties:
     * @param {String} contextualMenuItemsMap.key key Is the key that identifies a contextual menu item.
     * @param {String} contextualMenuItemsMap.i18nKey i18nKey Is the message key of the contextual menu item to be translated.
     * @param {Function} contextualMenuItemsMap.condition condition Is an optional entry that holds the condition function required to activate the menu item. It is invoked with the following payload:
     * <pre>
     * {
                    	componentType: the smartedit component type
                    	componentId: the smartedit component id
                    	containerType: the type of the container wrapping the component, if applicable
                    	containerId: the id of the container wrapping the component, if applicable
                    	element: the dom element of the component onto which the contextual menu is applied
		}
     * </pre>
     * @param {String} contextualMenuItemsMap.displayClass Contains the CSS classes used to style the contextual menu item
     * @param {String} contextualMenuItemsMap.iconIdle iconIdle Contains the location of the idle icon of the contextual menu item to be displayed.
     * @param {String} contextualMenuItemsMap.iconNonIdle iconNonIdle Contains the location of the non-idle icon of the contextual menu item to be displayed.
     * @param {String} contextualMenuItemsMap.smallIcon smallIcon Contains the location of the smaller version of the icon to be displayed when the menu item is part of the More... menu options.
     * @param {Function|String} contextualMenuItemsMap.callback Deprecated in 6.5 - use a action with callback instead
     * @param {Object} contextualMenuItemsMap.action The action to be performed when clicking the menu item.
     * Action is an object that must contain exactly one of callback | template | templateUrl<br />
     * <strong>callback</strong> A function executed on clicking of the menu item. It is invoked with:
     * <pre>
     * {
        componentType: the smartedit component type
        componentId: the smartedit component id
        containerType: the type of the container wrapping the component, if applicable
        containerId: the id of the container wrapping the component, if applicable
        slotId: the id of the content slot containing the component
		}
     * </pre>
     * <strong>template</strong> is an html string that will displayed below the menu item when the item is clicked.<br />
     * <strong>templateUrl</strong> is the same as template but instead of passing a string, pass a url to an html file.
     *
     */
    ContextualMenuServiceClass.prototype.addItems = function(contextualMenuItemsMap) {

        try {
            if (contextualMenuItemsMap !== undefined) {
                this._featuresList = getFeaturesList(this.contextualMenus);
                var componentTypes = Object.keys(contextualMenuItemsMap);
                componentTypes.forEach(hitch(this, initContextualMenuItems, contextualMenuItemsMap));
            }
        } catch (e) {
            throw new Error("addItems() - Cannot add items. " + e);
        }
    };

    /**
     * @ngdoc method
     * @name contextualMenuServiceModule.ContextualMenuServiceClass#removeItemByKey
     * @methodOf contextualMenuServiceModule.ContextualMenuServiceClass
     *
     * @description
     * This method removes the menu items identified by the provided key.
     *
     * @param {String} itemKey The key that identifies the menu items to remove.
     */
    ContextualMenuServiceClass.prototype.removeItemByKey = function(itemKey) {
        var filterFunction = function(item) {
            return (item.key !== itemKey);
        };

        for (var contextualMenuKey in this.contextualMenus) {
            var contextualMenuItems = this.contextualMenus[contextualMenuKey];
            this.contextualMenus[contextualMenuKey] = contextualMenuItems.filter(filterFunction);

            if (this.contextualMenus[contextualMenuKey].length === 0) {
                // Remove if the contextual menu is empty.
                delete this.contextualMenus[contextualMenuKey];
            }
        }
    };

    /**
     * @ngdoc method
     * @name contextualMenuServiceModule.ContextualMenuServiceClass#getContextualMenuByType
     * @methodOf contextualMenuServiceModule.ContextualMenuServiceClass
     *
     * @description
     * Will return an array of contextual menu items for a specific component type.
     * For each key in the contextual menus' object, the method converts each component type into a valid regex using the regExpFactory of the function module and then compares it with the input componentType and, if matched, will add it to an array and returns the array.
     *
     * @param {String} componentType The type code of the selected component
     *
     * @returns {Array} An array of contextual menu items assigned to the type.
     *
     */
    ContextualMenuServiceClass.prototype.getContextualMenuByType = function(componentType) {
        var contextualMenuArray = [];
        if (this.contextualMenus) {
            for (var regexpKey in this.contextualMenus) {
                if (regExpFactory(regexpKey).test(componentType)) {
                    contextualMenuArray = getUniqueItemArray(contextualMenuArray, this.contextualMenus[regexpKey]);
                }
            }
        }
        return contextualMenuArray;
    };

    /**
     * @ngdoc method
     * @name contextualMenuServiceModule.ContextualMenuServiceClass#getContextualMenuItems
     * @methodOf contextualMenuServiceModule.ContextualMenuServiceClass
     *
     * @description
     * Will return an object that contains a list of contextual menu items that are visible and those that are to be added to the More... options.
     *
     * For each component and display limit size, two arrays are generated.
     * One array contains the menu items that can be displayed and the other array contains the menu items that are available under the more menu items action.
     *
     * @param {Object} configuration The configuration used to determine the selected components
     * @param {String} configuration.componentType The type code of the selected component.
     * @param {String} configuration.componentId The ID of the selected component.
     * @param {String} configuration.containerType The type code of the container of the component if applicable, this is optional.
     * @param {String} configuration.containerId The ID of the container of the component if applicable, this is optional.
     * @param {Number} configuration.iLeftBtns The number of visible contextual menu items for a specified component.
     * @param {Element} configuration.element The DOM element of selected component
     * @returns {Promise} A promise that resolves to an array of contextual menu items assigned to the component type.
     *
     * The returned object contains the following properties
     * - leftMenuItems : An array of menu items that can be displayed on the component.
     * - moreMenuItems : An array of menu items that are available under the more menu items action.
     *
     */
    ContextualMenuServiceClass.prototype.getContextualMenuItems = function(configuration) {
        var iLeftBtns = configuration.iLeftBtns;
        delete configuration.iLeftBtns;

        var newMenuItems = [];
        var newMoreItems = [];
        var menuItems = this.getContextualMenuByType(configuration.componentType);

        var promisesToResolve = [];

        menuItems.forEach(function(item) {
            var deferred = $q.defer();
            promisesToResolve.push(deferred.promise);
            $q.when(item.condition ? item.condition(configuration) : true).then(pushEnabledMenuItemsAsync.bind(undefined, deferred, item, newMenuItems, newMoreItems, iLeftBtns));
        });

        return $q.all(promisesToResolve).then(function() {
            return {
                'leftMenuItems': newMenuItems,
                'moreMenuItems': newMoreItems
            };
        });
    };

    /**
     * @ngdoc method
     * @name contextualMenuServiceModule.ContextualMenuServiceClass#refreshMenuItems
     * @methodOf contextualMenuServiceModule.ContextualMenuServiceClass
     *
     * @description
     * This method can be used to ask SmartEdit to retrieve again the list of items in the enabled contextual menus.
     */
    ContextualMenuServiceClass.prototype.refreshMenuItems = function() {
        systemEventService.sendAsynchEvent(REFRESH_CONTEXTUAL_MENU_ITEMS_EVENT);
    };

    // Helper Methods
    function pushEnabledMenuItemsAsync(deferredPromise, menuItem, newMenuItems, newMoreItems, iLeftBtns, isItemEnabled) {

        var collection = newMenuItems.length < iLeftBtns ? newMenuItems : newMoreItems;
        if (isItemEnabled) {
            collection.push(menuItem);
        }
        deferredPromise.resolve();
    }

    return ContextualMenuServiceClass;
}])

/**
 * Maintaining for backwards compatibility, but use ContextualMenuServiceClass from now on
 * @Deprecated 6.5
 */
.factory('ContextualMenuService', ['ContextualMenuServiceClass', function(ContextualMenuServiceClass) {
    return ContextualMenuServiceClass;
}])


/**
 * @ngdoc service
 * @name contextualMenuServiceModule.contextualMenuService
 *
 * @description
 * The contextualMenuService returns an instance of
 * {@link contextualMenuServiceModule.ContextualMenuServiceClass ContextualMenuServiceClass}
 * and is used to add items to contextual menus over components and slots.
 */
.factory('contextualMenuService', ['ContextualMenuServiceClass', function(ContextualMenuServiceClass) {
    return new ContextualMenuServiceClass();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('experienceServiceModule', ['gatewayProxyModule'])

/**
 * @ngdoc service
 * @name experienceServiceModule.service:experienceService
 *
 * @description
 * The experience Service deals with building experience objects given a context.
 */
.service('experienceService', ['gatewayProxy', function(gatewayProxy) {


    var ExperienceService = function(gatewayId) {
        this.gatewayId = gatewayId;
        gatewayProxy.initForService(this);
    };

    ExperienceService.prototype.updateExperiencePageContext = function() {};

    return new ExperienceService('experienceService');

}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('featureServiceModule', ['functionsModule', 'featureInterfaceModule', 'gatewayProxyModule', 'decoratorServiceModule', 'contextualMenuServiceModule'])

.factory('featureService', ['$log', 'extend', 'hitch', 'copy', 'FeatureServiceInterface', 'gatewayProxy', 'decoratorService', 'contextualMenuService', function($log, extend, hitch, copy, FeatureServiceInterface, gatewayProxy, decoratorService, contextualMenuService) {

    var FeatureService = function() {
        this.gatewayId = "featureService";
        gatewayProxy.initForService(this);
    };

    FeatureService = extend(FeatureServiceInterface, FeatureService);

    FeatureService.prototype._remoteEnablingFromInner = function(key) {
        if (this.featuresToAlias && this.featuresToAlias[key]) {
            this.featuresToAlias[key].enablingCallback();
            return;
        } else {
            $log.warn("could not enable feature named " + key + ", it was not found in the iframe");
        }
    };

    FeatureService.prototype._remoteDisablingFromInner = function(key) {
        if (this.featuresToAlias && this.featuresToAlias[key]) {
            this.featuresToAlias[key].disablingCallback();
            return;
        } else {
            $log.warn("could not disable feature named " + key + ", it was not found in the iframe");
        }
    };

    FeatureService.prototype.addDecorator = function(configuration) {
        var prevEnablingCallback = configuration.enablingCallback;
        var prevDisablingCallback = configuration.disablingCallback;
        var displayCondition = configuration.displayCondition;

        configuration.enablingCallback = hitch(decoratorService, function() {
            this.enable(configuration.key, displayCondition);

            if (prevEnablingCallback) {
                prevEnablingCallback();
            }
        });

        configuration.disablingCallback = hitch(decoratorService, function() {
            this.disable(configuration.key);

            if (prevDisablingCallback) {
                prevDisablingCallback();
            }
        });

        delete configuration.displayCondition;

        this.register(configuration);
    };



    FeatureService.prototype.addContextualMenuButton = function(item) {
        var clone = copy(item);

        delete item.nameI18nKey;
        delete item.descriptionI18nKey;
        delete item.regexpKeys;

        clone.enablingCallback = function() {
            var mapping = {};
            clone.regexpKeys.forEach(function(regexpKey) {
                mapping[regexpKey] = [item];
            });
            contextualMenuService.addItems(mapping);
        };

        clone.disablingCallback = function() {
            contextualMenuService.removeItemByKey(clone.key);
        };

        this.register(clone);
    };

    return new FeatureService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name iframeClickDetectionServiceModule
 * @description
 * # The iframeClickDetectionServiceModule
 *
 * The iframe Click Detection module provides the functionality to transmit a click event from anywhere within
 * the contents of the SmartEdit application iFrame to the SmartEdit container. Specifically, the module uses yjQuery to
 * bind mousedown events on the iFrame document to a proxy function, triggering the event on the SmartEdit container.
 *
 * The iframe Click Detection module requires gatewayProxyModule to propagate click events.
 *
 */
angular.module('iframeClickDetectionServiceModule', ['gatewayProxyModule'])

/**
 * @ngdoc service
 * @name iframeClickDetectionServiceModule.service:iframeClickDetectionService
 *
 * @description
 * The iframe Click Detection service leverages the {@link gatewayProxyModule.gatewayProxy gatewayProxy} service to
 * propagate click events, specifically mousedown events, to the SmartEdit container.
 *
 */
.factory('iframeClickDetectionService', ['gatewayProxy', function(gatewayProxy) {
        function IframeClickDetectionService() {
            this.gatewayId = 'iframeClick';
            gatewayProxy.initForService(this, ["onIframeClick"]);
        }

        /**
         * @ngdoc method
         * @name iframeClickDetectionServiceModule.service:iframeClickDetectionService#onIframeClick
         * @methodOf iframeClickDetectionServiceModule.service:iframeClickDetectionService
         *
         * @description
         * The proxy function that delegates calls to the SmartEdit container.
         *
         * @returns {Promise} Promise that is resolved when the SmartEdit container completes the callback.
         */
        IframeClickDetectionService.prototype.onIframeClick = function() {};

        return new IframeClickDetectionService();
    }])
    .run(['iframeClickDetectionService', '$document', function(iframeClickDetectionService, $document) {
        $document.on('mousedown', function() {
            iframeClickDetectionService.onIframeClick();
        });
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('notificationMouseLeaveDetectionServiceModule', [
        'notificationMouseLeaveDetectionServiceInterfaceModule',
        'gatewayProxyModule',
        'functionsModule'
    ])
    /*
     * This service makes it possible to track the mouse position to detect when it leaves the
     * notification panel.
     * 
     * It is solely meant to be used with the notificationService.
     */
    .factory('notificationMouseLeaveDetectionService', ['NotificationMouseLeaveDetectionServiceInterface', 'SE_NOTIFICATION_MOUSE_LEAVE_DETECTION_SERVICE_GATEWAY_ID', 'gatewayProxy', '$document', 'extend', function(NotificationMouseLeaveDetectionServiceInterface, SE_NOTIFICATION_MOUSE_LEAVE_DETECTION_SERVICE_GATEWAY_ID, gatewayProxy, $document, extend) {
        var notificationPanelBounds = null;
        var mouseLeaveCallback = null;

        var NotificationMouseLeaveDetectionService = function() {
            gatewayProxy.initForService(this, ['stopDetection', '_remoteStartDetection', '_remoteStopDetection', '_callCallback'], SE_NOTIFICATION_MOUSE_LEAVE_DETECTION_SERVICE_GATEWAY_ID);

            /*
             * We need to bind the function in order for it to execute within the service's
             * scope and store it to be able to un-register the listener.
             */
            this._onMouseMove = this._onMouseMove.bind(this);
        };

        NotificationMouseLeaveDetectionService = extend(NotificationMouseLeaveDetectionServiceInterface, NotificationMouseLeaveDetectionService);

        NotificationMouseLeaveDetectionService.prototype._remoteStartDetection = function(innerBounds) {
            notificationPanelBounds = innerBounds;

            $document[0].addEventListener('mousemove', this._onMouseMove);
        };

        NotificationMouseLeaveDetectionService.prototype._remoteStopDetection = function() {
            $document[0].removeEventListener('mousemove', this._onMouseMove);

            notificationPanelBounds = null;
        };

        NotificationMouseLeaveDetectionService.prototype._getBounds = function() {
            return notificationPanelBounds;
        };

        NotificationMouseLeaveDetectionService.prototype._getCallback = function() {
            return mouseLeaveCallback;
        };

        return new NotificationMouseLeaveDetectionService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name notificationServiceModule
 * 
 * @description
 * The notification module provides a service to display visual cues to inform
 * the user of the state of the application in the container or the iFramed application.
 */
angular.module('notificationServiceModule', [
    'notificationServiceInterfaceModule',
    'notificationMouseLeaveDetectionServiceModule',
    'gatewayProxyModule',
    'functionsModule'
])

/**
 * @ngdoc service
 * @name notificationServiceModule.service:notificationService
 * 
 * @description
 * The notification service is used to display visual cues to inform the user of the state of the application.
 */
.factory('notificationService', ['NotificationServiceInterface', 'SE_NOTIFICATION_SERVICE_GATEWAY_ID', 'notificationMouseLeaveDetectionService', 'gatewayProxy', 'extend', function(NotificationServiceInterface, SE_NOTIFICATION_SERVICE_GATEWAY_ID, notificationMouseLeaveDetectionService, gatewayProxy, extend) {
    var NotificationService = function() {
        gatewayProxy.initForService(this, ['pushNotification', 'removeNotification', 'removeAllNotifications'], SE_NOTIFICATION_SERVICE_GATEWAY_ID);
    };

    NotificationService = extend(NotificationServiceInterface, NotificationService);

    return new NotificationService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('permissionServiceModule', ['permissionServiceInterfaceModule', 'gatewayProxyModule'])
    .factory('permissionService', ['$log', 'extend', 'PermissionServiceInterface', 'SE_PERMISSION_SERVICE_GATEWAY_ID', 'gatewayProxy', function($log, extend, PermissionServiceInterface, SE_PERMISSION_SERVICE_GATEWAY_ID, gatewayProxy) {
        var PermissionService = function() {
            gatewayProxy.initForService(this, null, SE_PERMISSION_SERVICE_GATEWAY_ID);
        };

        PermissionService = extend(PermissionServiceInterface, PermissionService);

        PermissionService.prototype._remoteCallRuleVerify = function(ruleKey, permissionNameObjs) {
            if (this.ruleVerifyFunctions && this.ruleVerifyFunctions[ruleKey]) {
                return this.ruleVerifyFunctions[ruleKey].verify(permissionNameObjs);
            } else {
                $log.warn("could not call rule verify function for rule key: " + ruleKey + ", it was not found in the iframe");
            }
        };

        return new PermissionService();
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('perspectiveServiceModule', ['functionsModule', 'perspectiveServiceInterfaceModule', 'gatewayProxyModule'])

.factory('perspectiveService', ['extend', 'PerspectiveServiceInterface', 'gatewayProxy', function(extend, PerspectiveServiceInterface, gatewayProxy) {

    var PerspectiveService = function() {
        this.gatewayId = "perspectiveService";
        gatewayProxy.initForService(this);
    };

    PerspectiveService = extend(PerspectiveServiceInterface, PerspectiveService);

    return new PerspectiveService();
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name renderServiceModule
 * @description
 * This module provides the renderService, which is responsible for rendering the SmartEdit overlays used for providing
 * CMS functionality to the storefront within the context of SmartEdit.
 */
angular.module('renderServiceModule', [
    'alertServiceModule',
    'componentHandlerServiceModule',
    'crossFrameEventServiceModule',
    'functionsModule',
    'gatewayFactoryModule',
    'gatewayProxyModule',
    'perspectiveServiceModule',
    'renderServiceInterfaceModule',
    'sakExecutorDecorator',
    'seConstantsModule',
    'yLoDashModule',
    'seNamespaceModule'
])

/**
 * @ngdoc service
 * @name renderServiceModule.renderService
 * @description
 * The renderService is responsible for rendering and resizing component overlays, and re-rendering components and slots
 * from the storefront.
 */
.service('renderService', ['$q', '$compile', '$rootScope', '$http', '$location', '$window', '$log', 'alertService', 'componentHandlerService', 'extractFromElement', 'gatewayFactory', 'gatewayProxy', 'isBlank', 'sakExecutor', 'perspectiveService', 'RenderServiceInterface', 'unsafeParseHTML', 'crossFrameEventService', 'COMPONENT_CLASS', 'UUID_ATTRIBUTE', 'CATALOG_VERSION_UUID_ATTRIBUTE', 'ID_ATTRIBUTE', 'OVERLAY_COMPONENT_CLASS', 'OVERLAY_ID', 'SMARTEDIT_ATTRIBUTE_PREFIX', 'TYPE_ATTRIBUTE', 'ELEMENT_UUID_ATTRIBUTE', 'lodash', 'OVERLAY_RERENDERED_EVENT', 'EVENT_PERSPECTIVE_CHANGED', 'seNamespace', 'generateIdentifier', function($q, $compile, $rootScope, $http, $location, $window, $log, alertService,
    componentHandlerService, extractFromElement, gatewayFactory, gatewayProxy, isBlank, sakExecutor,
    perspectiveService, RenderServiceInterface, unsafeParseHTML, crossFrameEventService, COMPONENT_CLASS,
    UUID_ATTRIBUTE, CATALOG_VERSION_UUID_ATTRIBUTE, ID_ATTRIBUTE, OVERLAY_COMPONENT_CLASS, OVERLAY_ID,
    SMARTEDIT_ATTRIBUTE_PREFIX, TYPE_ATTRIBUTE, ELEMENT_UUID_ATTRIBUTE, lodash,
    OVERLAY_RERENDERED_EVENT, EVENT_PERSPECTIVE_CHANGED, seNamespace, generateIdentifier) {

    angular.extend(this, RenderServiceInterface.prototype);
    RenderServiceInterface.call(this);

    this.gatewayId = "Renderer";
    this._slotOriginalHeights = {};
    this._smartEditBootstrapGateway = gatewayFactory.createGateway('smartEditBootstrap');

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService.toggleOverlay
     * @methodOf renderServiceModule.renderService
     * @description
     * Toggles the visibility of the overlay using CSS.
     *
     * @param {Boolean} isVisible Flag to show/hide the overlay.
     */
    this.toggleOverlay = function(isVisible) {
        var overlay = componentHandlerService.getOverlay();
        overlay.css('visibility', (isVisible ? 'visible' : 'hidden'));
    };

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService.refreshOverlayDimensions
     * @methodOf renderServiceModule.renderService
     * @description
     * Refreshes the dimensions and positions of the SmartEdit overlays. The overlays need to remain in synced with the
     * dynamic resizing of their original elements. In particular, this method is bound to the window resizing event
     * to refresh overlay dimensions for responsive storefronts.
     *
     * The implementation itself will search for children SmartEdit components from the root element provided. If no root
     * element is provided, the method will default to using the body element. The overlay specific to this component
     * is then fetched and resized, according to the dimensions of the component.
     *
     * @param {Element} element The root element from which to traverse and discover SmartEdit components.
     */
    this.refreshOverlayDimensions = function(element) {
        element = element || componentHandlerService.getFromSelector('body');
        var children = componentHandlerService.getFirstSmartEditComponentChildren(element);

        children.each(function(index, childElement) {
            var wrappedChild = componentHandlerService.getFromSelector(childElement);
            this._updateComponentSizeAndPosition(wrappedChild);
            this.refreshOverlayDimensions(wrappedChild);
        }.bind(this));
    };

    /*
     * Updates the dimensions of the overlay component element given the original component element and the overlay component itself.
     * If no overlay component is provided, it will be fetched through {@link componentHandlerService.getOverlayComponent}

     * The overlay component is resized to be the same dimensions of the component for which it overlays, and positioned absolutely
     * on the page. Additionally, it is provided with a minimum height and width. The resizing takes into account both
     * the size of the component element, and the position based on iframe scrolling.
     *
     * @param {Element} componentElem The original CMS component element from the storefront.
     * @param {Element=} componentOverlayElem The overlay component. If none is provided
     */
    this._updateComponentSizeAndPosition = function(componentElem, componentOverlayElem) {

        componentElem = componentHandlerService.getFromSelector(componentElem);

        var parentOverlayElem = this._getParentInOverlay(componentElem);

        componentOverlayElem = componentOverlayElem ||
            componentHandlerService.getOverlayComponent(componentElem).get(0);

        if (componentOverlayElem) {
            var pos = componentElem.get(0).getBoundingClientRect();
            var parentPos = parentOverlayElem.get(0).getBoundingClientRect();

            var innerHeight = componentElem.get(0).offsetHeight;
            var innerWidth = componentElem.get(0).offsetWidth;

            // Update the position based on the IFrame Scrolling
            var elementTopPos = pos.top - parentPos.top;
            var elementLeftPos = pos.left - parentPos.left;

            componentOverlayElem.style.position = "absolute";
            componentOverlayElem.style.top = elementTopPos + "px";
            componentOverlayElem.style.left = elementLeftPos + "px";
            componentOverlayElem.style.width = innerWidth + "px";
            componentOverlayElem.style.height = innerHeight + "px";
            componentOverlayElem.style.minWidth = "51px";
            componentOverlayElem.style.minHeight = "48px";


            var shallowCopy = componentHandlerService.getFromSelector(componentOverlayElem).find('[id="' + this._buildShallowCloneId(componentElem.attr(ID_ATTRIBUTE), componentElem.attr(TYPE_ATTRIBUTE)) + '"]');
            shallowCopy.width(innerWidth);
            shallowCopy.height(innerHeight);
            shallowCopy.css('min-height', 49);
            shallowCopy.css('min-width', 51);
        }
    };

    this._getParentInOverlay = function(element) {
        var parent = componentHandlerService.getParent(element);
        if (parent.length) {
            return componentHandlerService.getOverlayComponent(parent);
        } else {
            return componentHandlerService.getOverlay();
        }
    };

    /*
     * Given a smartEdit component in the storefront layer. An empty clone of it will be created, sized and positioned in the smartEdit overlay
     * then compiled with all eligible decorators for the given perspective (see {@link perspectiveInterfaceModule.service:PerspectiveServiceInterface perspectiveService})
     * This method operates recursively on all smartEditComponent children
     * @param {Element} element The original CMS component element from the storefront.
     */
    this._createComponent = function(element) {
        if (componentHandlerService.isOverlayOn()) {
            this._cloneAndCompileComponent(element);
            componentHandlerService.getFirstSmartEditComponentChildren(element).each(function(index, childElement) {
                if (this._isComponentVisible(childElement)) {
                    this._createComponent(childElement);
                }
            }.bind(this));
        }
    };

    this._buildShallowCloneId = function(smarteditComponentId, smarteditComponentType) {
        return smarteditComponentId + "_" + smarteditComponentType + "_overlay";
    };

    this._cloneAndCompileComponent = function(element) {

        if (componentHandlerService.getFromSelector(element).is(":visible")) {
            element = componentHandlerService.getFromSelector(element);

            var parentOverlay = this._getParentInOverlay(element);

            //if parentOverlay does not exists it means the overlay itself is not there
            if (parentOverlay.length) {
                if (validateComponentAttributesContract(element)) {
                    var elementUUID = generateIdentifier();
                    element.attr(ELEMENT_UUID_ATTRIBUTE, elementUUID);

                    var smarteditComponentId = element.attr(ID_ATTRIBUTE);
                    var smarteditComponentType = element.attr(TYPE_ATTRIBUTE);

                    var shallowCopy = this._getDocument().createElement("div");
                    shallowCopy.id = this._buildShallowCloneId(smarteditComponentId, smarteditComponentType);

                    var smartEditWrapper = this._getDocument().createElement("div");
                    var componentDecorator = componentHandlerService.getFromSelector(smartEditWrapper);
                    componentDecorator.append(shallowCopy);

                    this._updateComponentSizeAndPosition(element, smartEditWrapper);

                    if (smarteditComponentType === "NavigationBarCollectionComponent") {
                        // Make sure the Navigation Bar is on top of the navigation items
                        smartEditWrapper.style.zIndex = "7";
                    }


                    componentDecorator.addClass(OVERLAY_COMPONENT_CLASS);
                    Array.prototype.slice.apply(element.get(0).attributes).forEach(function(node) {
                        if (node.nodeName.indexOf(SMARTEDIT_ATTRIBUTE_PREFIX) === 0) {
                            componentDecorator.attr(node.nodeName, node.nodeValue);
                        }
                    });

                    var compiled = this._compile(smartEditWrapper, $rootScope);
                    parentOverlay.append(compiled);
                }
            }
        }
    };

    function validateComponentAttributesContract(element) {
        var requiredAttributes = [ID_ATTRIBUTE, UUID_ATTRIBUTE, TYPE_ATTRIBUTE, CATALOG_VERSION_UUID_ATTRIBUTE];
        var valid = true;
        requiredAttributes.forEach(function(reqAttribute) {
            if (!element || !element.attr(reqAttribute)) {
                valid = false;
                $log.warn('RenderService - smarteditComponent element discovered with missing contract attribute: ' + reqAttribute);
            }
        });
        return valid;
    }

    function _areAllImagesReady() {
        return Array.prototype.slice.call(document.querySelectorAll("img")).reduce(function(areImagesReady, next) {
            if (!next.complete) {
                areImagesReady = false;
            }
            return areImagesReady;
        }, true);
    }

    function _waitForAllImagesToBeReady(callback) {
            if (!_areAllImagesReady()) {
                setTimeout(function() {
                    _waitForAllImagesToBeReady(callback);
                }, 100);
                return;
            }
            callback();
        }
        // Component Rendering
    this.renderPage = function(isRerender) {

        _waitForAllImagesToBeReady(function() {
            this._resizeSlots();
            //need to destroy scopes BEFORE removing the directive elements
            sakExecutor.destroyAllScopes();
            componentHandlerService.getOverlay().remove();
            $q.all([perspectiveService.isEmptyPerspectiveActive(), this.isRenderingBlocked()]).then(function(promises) {

                this._markSmartEditAsReady();

                if (isRerender && !promises[1]) {

                    var overlayWrapper = componentHandlerService.getOverlay();
                    if (overlayWrapper.length === 0) {
                        var overlay = document.createElement("div");
                        overlay.id = OVERLAY_ID;
                        overlay.style.zIndex = "0";
                        overlay.style.position = "absolute";
                        overlay.style.top = "0px";
                        overlay.style.left = "0px";
                        overlay.style.bottom = "0px";
                        overlay.style.right = "0px";

                        document.body.appendChild(overlay);
                    } else {
                        overlayWrapper.empty();
                    }
                    var body = componentHandlerService.getFromSelector('body');

                    componentHandlerService.getFirstSmartEditComponentChildren(body).each(function(index, component) {
                        this._createComponent(component);
                    }.bind(this));
                }

                // Send an event to inform that the page was re-rendered.
                crossFrameEventService.publish(OVERLAY_RERENDERED_EVENT);
            }.bind(this));
        }.bind(this));
    };

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService._resizeSlots
     * @methodOf renderServiceModule.renderService
     * @private
     * @description
     * Resizes the height of all slots on the page based on the sizes of the components. The new height of the
     * slot is set to the minimum height encompassing its sub-components, calculated by comparing each of the
     * sub-components' top and bottom bounding rectangle values.
     *
     * Slots that do not have components inside still appear in the DOM. If the CMS manager is in a perspective in which
     * slot contextual menus are displayed, slots must have a height. Otherwise, overlays will overlap. Thus, empty slots
     * are given a minimum size so that overlays match.
     */
    this._resizeSlots = function() {
        componentHandlerService.getFirstSmartEditComponentChildren('body').each(function(index, slotComponent) {
            var slotComponentID = componentHandlerService.getFromSelector(slotComponent).attr(ID_ATTRIBUTE);
            var slotComponentType = componentHandlerService.getFromSelector(slotComponent).attr(TYPE_ATTRIBUTE);

            var newSlotTop = -1;
            var newSlotBottom = -1;

            var currentSlotHeight = parseFloat(window.getComputedStyle(slotComponent).height) || 0;
            var currentSlotVerticalPadding = parseFloat(window.getComputedStyle(slotComponent).paddingTop) +
                parseFloat(window.getComputedStyle(slotComponent).paddingBottom);

            componentHandlerService.getFromSelector(slotComponent)
                .find("." + COMPONENT_CLASS)
                .filter(function(index, componentInSlot) {
                    componentInSlot = componentHandlerService.getFromSelector(componentInSlot);
                    return (componentInSlot.attr(ID_ATTRIBUTE) !== slotComponentID && componentInSlot.attr(TYPE_ATTRIBUTE) !== slotComponentType);
                })
                .each(function(compIndex, component) {
                    if (componentHandlerService.getFromSelector(component).is(":visible")) {
                        var componentDimensions = component.getBoundingClientRect();
                        newSlotTop = newSlotTop === -1 ? componentDimensions.top :
                            Math.min(newSlotTop, componentDimensions.top);
                        newSlotBottom = newSlotBottom === -1 ? componentDimensions.bottom :
                            Math.max(newSlotBottom, componentDimensions.bottom);
                    }
                });

            var newSlotHeight = newSlotBottom - newSlotTop;

            if (Math.abs(currentSlotHeight - newSlotHeight) > 0.001) {
                var slotUniqueKey = slotComponentID + "_" + slotComponentType;
                var oldSlotHeight = this._slotOriginalHeights[slotUniqueKey];
                if (!oldSlotHeight) {
                    oldSlotHeight = currentSlotHeight;
                    this._slotOriginalHeights[slotUniqueKey] = oldSlotHeight;
                }
                if (newSlotHeight + currentSlotVerticalPadding > oldSlotHeight) {
                    slotComponent.style.height = (newSlotHeight + currentSlotVerticalPadding) + "px";
                } else {
                    slotComponent.style.height = oldSlotHeight + 'px';
                }
            }
        }.bind(this));
    };

    this.renderSlots = function(_slotIds) {

        if (isBlank(_slotIds) || (_slotIds instanceof Array && _slotIds.length === 0)) {
            return $q.reject("renderService.renderSlots.slotIds.required");
        }
        if (typeof _slotIds === 'string') {
            _slotIds = [_slotIds];
        }

        //need to retrieve unique set of slotIds, happens when moving a component within a slot
        var slotIds = lodash.uniqBy(_slotIds, function(slotId) {
            return slotId;
        });

        // see if storefront can handle the rerendering
        var slotsRemaining = slotIds.filter(function(id) {
            return !seNamespace.renderComponent(id);
        });

        if (slotsRemaining.length <= 0) {
            //all were handled by storefront
            return $q.when(true);
        } else {
            return $http({
                method: 'GET',
                url: $location.absUrl(),
                headers: {
                    'Pragma': 'no-cache'
                }
            }).then(function(response) {
                var root = unsafeParseHTML(response.data);
                slotsRemaining.forEach(function(slotId) {
                    var slotSelector = "." + COMPONENT_CLASS + "[" + TYPE_ATTRIBUTE + "='ContentSlot'][" + ID_ATTRIBUTE + "='" + slotId + "']";
                    var slotToBeRerendered = extractFromElement(root, slotSelector);
                    var originalSlot = componentHandlerService.getFromSelector(slotSelector);
                    originalSlot.html(slotToBeRerendered.html());
                });
                this._reprocessPage();
            }.bind(this), function(errorResponse) {
                alertService.showDanger({
                    message: errorResponse.message
                });
                return $q.reject(errorResponse.message);
            });
        }

    };

    this.renderComponent = function(componentId, componentType) {
        var component = componentHandlerService.getComponent(componentId, componentType);
        var slotId = componentHandlerService.getParent(component).attr(ID_ATTRIBUTE);
        if (seNamespace.renderComponent(componentId, componentType, slotId)) {
            return $q.when(true);
        } else {
            return this.renderSlots(slotId);
        }
    };



    this.renderRemoval = function(componentId, componentType, slotId) {
        var removedComponents = componentHandlerService.getComponentUnderSlot(componentId, componentType, slotId, null).remove();
        this.refreshOverlayDimensions();
        return removedComponents;
    };

    /*
     * Given a smartEdit component in the storefront layer, its clone in the smartEdit overlay is removed and the pertaining decorators destroyed.
     * This method operates recursively on all smartEditComponent children
     *
     * @param {Element} element The original CMS component element from the storefront.
     * @param {Element=} parent the closest smartEditComponent parent, expected to be null for the highest elements
     * @param {Object=} oldAttributes The map of former attributes of the element. necessary when the element has mutated since the last creation
     */
    this._destroyComponent = function(_component, _parent, oldAttributes) {

        var component = componentHandlerService.getFromSelector(_component);
        var parent = componentHandlerService.getFromSelector(_parent);

        sakExecutor.destroyScope(component);
        var componentInOverlayId = oldAttributes && oldAttributes[ID_ATTRIBUTE] ? oldAttributes[ID_ATTRIBUTE] : component.attr(ID_ATTRIBUTE);
        var componentInOverlayType = oldAttributes && oldAttributes[TYPE_ATTRIBUTE] ? oldAttributes[TYPE_ATTRIBUTE] : component.attr(TYPE_ATTRIBUTE);

        //the node is no longer attached so can't find parent
        if (parent.attr(ID_ATTRIBUTE)) {
            componentHandlerService.getOverlayComponentWithinSlot(componentInOverlayId, componentInOverlayType, parent.attr(ID_ATTRIBUTE)).remove();
        } else {
            componentHandlerService.getComponentInOverlay(componentInOverlayId, componentInOverlayType).remove();
        }

        componentHandlerService.getFirstSmartEditComponentChildren(component).each(function(index, childElement) {
            this._destroyComponent(childElement, component);
        }.bind(this));

    };


    this._markSmartEditAsReady = function() {
        this._smartEditBootstrapGateway.publish('smartEditReady');
    };

    this._isComponentVisible = function(component) {
        // NOTE: This might not work as expected for fixed positioned items. For those cases a more expensive
        // check must be performed (get the component style and check if it's visible or not).
        return (component.offsetParent !== null);
    };

    this._reprocessPage = function() {
        seNamespace.reprocessPage();
    };

    this._compile = function(component, scope) {
        return $compile(component)(scope);
    };

    this._getDocument = function() {
        return document;
    };

    crossFrameEventService.subscribe(EVENT_PERSPECTIVE_CHANGED, function(eventId, isNonEmptyPerspective) {
        this.renderPage(isNonEmptyPerspective);
    }.bind(this));

    gatewayProxy.initForService(this, ["blockRendering", "isRenderingBlocked", "renderSlots", "renderComponent", "renderRemoval", "toggleOverlay", "refreshOverlayDimensions", "renderPage"]);
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
(function() {

    /**
     * This is an internal service, no ngdocs.
     *
     * The resizeComponentServiceModule contains a service that resizes slots and components in the inner frame.
     */
    angular.module('resizeComponentServiceModule', ['componentHandlerServiceModule'])

    /**
     * This service provides methods that resize slots when the overlay is enable or disabled.
     */
    .factory('resizeComponentService', ['componentHandlerService', function(componentHandlerService) {

        var ResizeComponentService = function() {

            /**
             * This methods appends CSS classes to inner frame slots and components. Passing a boolean true to showResizing
             * enables the resizing, and false vice versa.
             */
            this._resizeComponents = function(showResizing) {

                var slots = componentHandlerService.getFromSelector(componentHandlerService.getAllSlotsSelector());
                var components = componentHandlerService.getFromSelector(componentHandlerService.getAllComponentsSelector());

                if (showResizing) {
                    slots.addClass('ySEEmptySlot');
                    components.addClass('se-storefront-component');
                } else {
                    slots.removeClass('ySEEmptySlot');
                    components.removeClass('se-storefront-component');
                }
            };

        };

        return new ResizeComponentService();
    }]);

})();

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name sharedDataServiceModule
 * @description
 * # The sharedDataServiceModule
 *
 * The Shared Data Service Module provides a service used to store and retrieve data using a key. The data can be shared
 * between the SmartEdit application and SmartEdit container.
 *
 */
angular.module('sharedDataServiceModule', ['gatewayProxyModule', 'sharedDataServiceInterfaceModule'])

/**
 * @ngdoc service
 * @name sharedDataServiceModule.sharedDataService
 *
 * @description
 * The Shared Data Service is used to store data that is to be shared between the SmartEdit application and the
 * SmartEdit container. It uses the {@link gatewayProxyModule.gatewayProxy gatewayProxy} service to share data between
 * the SmartEdit application and the container. It uses the gateway ID "sharedData".
 *
 * The Shared Data Service extends the {@link sharedDataServiceInterfaceModule.SharedDataServiceInterface
 * SharedDataServiceInterface}.
 *
 */
.factory('sharedDataService', ['gatewayProxy', 'SharedDataServiceInterface', 'extend', function(gatewayProxy, SharedDataServiceInterface, extend) {

    var SharedDataService = function(gatewayId) {
        this.gatewayId = gatewayId;

        gatewayProxy.initForService(this);
    };


    SharedDataService = extend(SharedDataServiceInterface, SharedDataService);

    return new SharedDataService('sharedData');
}]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */

/**
 * @ngdoc overview
 * @name storageServiceModule
 * @description
 * # The storageServiceModule
 *
 * The Storage Service Module provides a service that allows storing temporary information in the browser.
 *
 */
angular.module('storageServiceModule', ['gatewayProxyModule'])
    /**
     * @ngdoc service
     * @name storageServiceModule.service:storageService
     *
     * @description
     * The Storage service is used to store temporary information in the browser. The service keeps track of key/value pairs
     * of authTokens that authenticate the specified user on different URIs.
     *
     */
    .factory("storageService", ['gatewayProxy', function(gatewayProxy) {

        var StorageService = function(gatewayId) {
            this.gatewayId = gatewayId;

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#isInitialized
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method is used to determine if the storage service has been initialized properly. It
             * makes sure that the smartedit-sessions cookie is available in the browser.
             *
             * @returns {Boolean} Indicates if the storage service was properly initialized.
             */
            this.isInitialized = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#storePrincipalIdentifier
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method is used to store the principal's login name in storage service
             *
             * @param {String} principalUID Value associated to store principal's login.
             */
            this.storePrincipalIdentifier = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#removePrincipalIdentifier
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method is used to remove the principal's UID from storage service
             *
             */
            this.removePrincipalIdentifier = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#getPrincipalIdentifier
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method is used to retrieve the principal's login name from storage service
             *
             * @returns {String} principalNameValue principal's name associated with the key.
             */
            this.getPrincipalIdentifier = function() {};


            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#storeAuthToken
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method creates and stores a new key/value entry. It associates an authentication token with a
             * URI.
             *
             * @param {String} authURI The URI that identifies the resource(s) to be authenticated with the authToken. Will be used as a key.
             * @param {String} auth The token to be used to authenticate the user in the provided URI.
             */
            this.storeAuthToken = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#getAuthToken
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method is used to retrieve the authToken associated with the provided URI.
             *
             * @param {String} authURI The URI for which the associated authToken is to be retrieved.
             * @returns {String} The authToken used to authenticate the current user in the provided URI.
             */
            this.getAuthToken = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#removeAuthToken
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * Removes the authToken associated with the provided URI.
             *
             * @param {String} authURI The URI for which its authToken is to be removed.
             */
            this.removeAuthToken = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#removeAllAuthTokens
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * This method removes all authURI/authToken key/pairs from the storage service.
             */
            this.removeAllAuthTokens = function() {};

            /**
             * @ngdoc method
             * @name storageServiceModule.service:storageService#getValueFromCookie
             * @methodOf storageServiceModule.service:storageService
             *
             * @description
             * Retrieves the value stored in the cookie identified by the provided name.
             */
            this.getValueFromCookie = function() {};

            gatewayProxy.initForService(this);
        };

        return new StorageService("storage");
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name urlServiceModule
 * @description
 * # The urlServiceModule
 *
 * The Url Service Module is a module for providing functionality to open a URL
 * in a new browser url from within the SmartEdit container.
 *
 */
angular.module('urlServiceModule', ['gatewayProxyModule', 'urlServiceInterfaceModule'])
    .factory('urlService', ['gatewayProxy', 'UrlServiceInterface', 'extend', function(gatewayProxy, UrlServiceInterface, extend) {

        var UrlService = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this, ['openUrlInPopup', 'path']);
        };


        UrlService = extend(UrlServiceInterface, UrlService);

        return new UrlService('urlService');
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('waitDialogServiceModule', ['gatewayProxyModule'])
    .factory('waitDialogService', ['gatewayProxy', function(gatewayProxy) {

        var WaitDialogService = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };

        WaitDialogService.prototype.showWaitModal = function() {};

        WaitDialogService.prototype.hideWaitModal = function() {};


        return new WaitDialogService('waitDialog');
    }]);
