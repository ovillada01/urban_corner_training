/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
/**
 * @ngdoc overview
 * @name renderServiceModule
 * @description
 * This module provides the renderService, which is responsible for rendering the SmartEdit overlays used for providing
 * CMS functionality to the storefront within the context of SmartEdit.
 */
angular.module('renderServiceModule', [
    'alertServiceModule',
    'componentHandlerServiceModule',
    'crossFrameEventServiceModule',
    'functionsModule',
    'gatewayFactoryModule',
    'gatewayProxyModule',
    'perspectiveServiceModule',
    'renderServiceInterfaceModule',
    'sakExecutorDecorator',
    'seConstantsModule',
    'yLoDashModule',
    'seNamespaceModule'
])

/**
 * @ngdoc service
 * @name renderServiceModule.renderService
 * @description
 * The renderService is responsible for rendering and resizing component overlays, and re-rendering components and slots
 * from the storefront.
 */
.service('renderService', function($q, $compile, $rootScope, $http, $location, $window, $log, alertService,
    componentHandlerService, extractFromElement, gatewayFactory, gatewayProxy, isBlank, sakExecutor,
    perspectiveService, RenderServiceInterface, unsafeParseHTML, crossFrameEventService, COMPONENT_CLASS,
    UUID_ATTRIBUTE, CATALOG_VERSION_UUID_ATTRIBUTE, ID_ATTRIBUTE, OVERLAY_COMPONENT_CLASS, OVERLAY_ID,
    SMARTEDIT_ATTRIBUTE_PREFIX, TYPE_ATTRIBUTE, ELEMENT_UUID_ATTRIBUTE, lodash,
    OVERLAY_RERENDERED_EVENT, EVENT_PERSPECTIVE_CHANGED, seNamespace, generateIdentifier) {

    angular.extend(this, RenderServiceInterface.prototype);
    RenderServiceInterface.call(this);

    this.gatewayId = "Renderer";
    this._slotOriginalHeights = {};
    this._smartEditBootstrapGateway = gatewayFactory.createGateway('smartEditBootstrap');

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService.toggleOverlay
     * @methodOf renderServiceModule.renderService
     * @description
     * Toggles the visibility of the overlay using CSS.
     *
     * @param {Boolean} isVisible Flag to show/hide the overlay.
     */
    this.toggleOverlay = function(isVisible) {
        var overlay = componentHandlerService.getOverlay();
        overlay.css('visibility', (isVisible ? 'visible' : 'hidden'));
    };

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService.refreshOverlayDimensions
     * @methodOf renderServiceModule.renderService
     * @description
     * Refreshes the dimensions and positions of the SmartEdit overlays. The overlays need to remain in synced with the
     * dynamic resizing of their original elements. In particular, this method is bound to the window resizing event
     * to refresh overlay dimensions for responsive storefronts.
     *
     * The implementation itself will search for children SmartEdit components from the root element provided. If no root
     * element is provided, the method will default to using the body element. The overlay specific to this component
     * is then fetched and resized, according to the dimensions of the component.
     *
     * @param {Element} element The root element from which to traverse and discover SmartEdit components.
     */
    this.refreshOverlayDimensions = function(element) {
        element = element || componentHandlerService.getFromSelector('body');
        var children = componentHandlerService.getFirstSmartEditComponentChildren(element);

        children.each(function(index, childElement) {
            var wrappedChild = componentHandlerService.getFromSelector(childElement);
            this._updateComponentSizeAndPosition(wrappedChild);
            this.refreshOverlayDimensions(wrappedChild);
        }.bind(this));
    };

    /*
     * Updates the dimensions of the overlay component element given the original component element and the overlay component itself.
     * If no overlay component is provided, it will be fetched through {@link componentHandlerService.getOverlayComponent}

     * The overlay component is resized to be the same dimensions of the component for which it overlays, and positioned absolutely
     * on the page. Additionally, it is provided with a minimum height and width. The resizing takes into account both
     * the size of the component element, and the position based on iframe scrolling.
     *
     * @param {Element} componentElem The original CMS component element from the storefront.
     * @param {Element=} componentOverlayElem The overlay component. If none is provided
     */
    this._updateComponentSizeAndPosition = function(componentElem, componentOverlayElem) {

        componentElem = componentHandlerService.getFromSelector(componentElem);

        var parentOverlayElem = this._getParentInOverlay(componentElem);

        componentOverlayElem = componentOverlayElem ||
            componentHandlerService.getOverlayComponent(componentElem).get(0);

        if (componentOverlayElem) {
            var pos = componentElem.get(0).getBoundingClientRect();
            var parentPos = parentOverlayElem.get(0).getBoundingClientRect();

            var innerHeight = componentElem.get(0).offsetHeight;
            var innerWidth = componentElem.get(0).offsetWidth;

            // Update the position based on the IFrame Scrolling
            var elementTopPos = pos.top - parentPos.top;
            var elementLeftPos = pos.left - parentPos.left;

            componentOverlayElem.style.position = "absolute";
            componentOverlayElem.style.top = elementTopPos + "px";
            componentOverlayElem.style.left = elementLeftPos + "px";
            componentOverlayElem.style.width = innerWidth + "px";
            componentOverlayElem.style.height = innerHeight + "px";
            componentOverlayElem.style.minWidth = "51px";
            componentOverlayElem.style.minHeight = "48px";


            var shallowCopy = componentHandlerService.getFromSelector(componentOverlayElem).find('[id="' + this._buildShallowCloneId(componentElem.attr(ID_ATTRIBUTE), componentElem.attr(TYPE_ATTRIBUTE)) + '"]');
            shallowCopy.width(innerWidth);
            shallowCopy.height(innerHeight);
            shallowCopy.css('min-height', 49);
            shallowCopy.css('min-width', 51);
        }
    };

    this._getParentInOverlay = function(element) {
        var parent = componentHandlerService.getParent(element);
        if (parent.length) {
            return componentHandlerService.getOverlayComponent(parent);
        } else {
            return componentHandlerService.getOverlay();
        }
    };

    /*
     * Given a smartEdit component in the storefront layer. An empty clone of it will be created, sized and positioned in the smartEdit overlay
     * then compiled with all eligible decorators for the given perspective (see {@link perspectiveInterfaceModule.service:PerspectiveServiceInterface perspectiveService})
     * This method operates recursively on all smartEditComponent children
     * @param {Element} element The original CMS component element from the storefront.
     */
    this._createComponent = function(element) {
        if (componentHandlerService.isOverlayOn()) {
            this._cloneAndCompileComponent(element);
            componentHandlerService.getFirstSmartEditComponentChildren(element).each(function(index, childElement) {
                if (this._isComponentVisible(childElement)) {
                    this._createComponent(childElement);
                }
            }.bind(this));
        }
    };

    this._buildShallowCloneId = function(smarteditComponentId, smarteditComponentType) {
        return smarteditComponentId + "_" + smarteditComponentType + "_overlay";
    };

    this._cloneAndCompileComponent = function(element) {

        if (componentHandlerService.getFromSelector(element).is(":visible")) {
            element = componentHandlerService.getFromSelector(element);

            var parentOverlay = this._getParentInOverlay(element);

            //if parentOverlay does not exists it means the overlay itself is not there
            if (parentOverlay.length) {
                if (validateComponentAttributesContract(element)) {
                    var elementUUID = generateIdentifier();
                    element.attr(ELEMENT_UUID_ATTRIBUTE, elementUUID);

                    var smarteditComponentId = element.attr(ID_ATTRIBUTE);
                    var smarteditComponentType = element.attr(TYPE_ATTRIBUTE);

                    var shallowCopy = this._getDocument().createElement("div");
                    shallowCopy.id = this._buildShallowCloneId(smarteditComponentId, smarteditComponentType);

                    var smartEditWrapper = this._getDocument().createElement("div");
                    var componentDecorator = componentHandlerService.getFromSelector(smartEditWrapper);
                    componentDecorator.append(shallowCopy);

                    this._updateComponentSizeAndPosition(element, smartEditWrapper);

                    if (smarteditComponentType === "NavigationBarCollectionComponent") {
                        // Make sure the Navigation Bar is on top of the navigation items
                        smartEditWrapper.style.zIndex = "7";
                    }


                    componentDecorator.addClass(OVERLAY_COMPONENT_CLASS);
                    Array.prototype.slice.apply(element.get(0).attributes).forEach(function(node) {
                        if (node.nodeName.indexOf(SMARTEDIT_ATTRIBUTE_PREFIX) === 0) {
                            componentDecorator.attr(node.nodeName, node.nodeValue);
                        }
                    });

                    var compiled = this._compile(smartEditWrapper, $rootScope);
                    parentOverlay.append(compiled);
                }
            }
        }
    };

    function validateComponentAttributesContract(element) {
        var requiredAttributes = [ID_ATTRIBUTE, UUID_ATTRIBUTE, TYPE_ATTRIBUTE, CATALOG_VERSION_UUID_ATTRIBUTE];
        var valid = true;
        requiredAttributes.forEach(function(reqAttribute) {
            if (!element || !element.attr(reqAttribute)) {
                valid = false;
                $log.warn('RenderService - smarteditComponent element discovered with missing contract attribute: ' + reqAttribute);
            }
        });
        return valid;
    }

    function _areAllImagesReady() {
        return Array.prototype.slice.call(document.querySelectorAll("img")).reduce(function(areImagesReady, next) {
            if (!next.complete) {
                areImagesReady = false;
            }
            return areImagesReady;
        }, true);
    }

    function _waitForAllImagesToBeReady(callback) {
            if (!_areAllImagesReady()) {
                setTimeout(function() {
                    _waitForAllImagesToBeReady(callback);
                }, 100);
                return;
            }
            callback();
        }
        // Component Rendering
    this.renderPage = function(isRerender) {

        _waitForAllImagesToBeReady(function() {
            this._resizeSlots();
            //need to destroy scopes BEFORE removing the directive elements
            sakExecutor.destroyAllScopes();
            componentHandlerService.getOverlay().remove();
            $q.all([perspectiveService.isEmptyPerspectiveActive(), this.isRenderingBlocked()]).then(function(promises) {

                this._markSmartEditAsReady();

                if (isRerender && !promises[1]) {

                    var overlayWrapper = componentHandlerService.getOverlay();
                    if (overlayWrapper.length === 0) {
                        var overlay = document.createElement("div");
                        overlay.id = OVERLAY_ID;
                        overlay.style.zIndex = "0";
                        overlay.style.position = "absolute";
                        overlay.style.top = "0px";
                        overlay.style.left = "0px";
                        overlay.style.bottom = "0px";
                        overlay.style.right = "0px";

                        document.body.appendChild(overlay);
                    } else {
                        overlayWrapper.empty();
                    }
                    var body = componentHandlerService.getFromSelector('body');

                    componentHandlerService.getFirstSmartEditComponentChildren(body).each(function(index, component) {
                        this._createComponent(component);
                    }.bind(this));
                }

                // Send an event to inform that the page was re-rendered.
                crossFrameEventService.publish(OVERLAY_RERENDERED_EVENT);
            }.bind(this));
        }.bind(this));
    };

    /**
     * @ngdoc function
     * @name renderServiceModule.renderService._resizeSlots
     * @methodOf renderServiceModule.renderService
     * @private
     * @description
     * Resizes the height of all slots on the page based on the sizes of the components. The new height of the
     * slot is set to the minimum height encompassing its sub-components, calculated by comparing each of the
     * sub-components' top and bottom bounding rectangle values.
     *
     * Slots that do not have components inside still appear in the DOM. If the CMS manager is in a perspective in which
     * slot contextual menus are displayed, slots must have a height. Otherwise, overlays will overlap. Thus, empty slots
     * are given a minimum size so that overlays match.
     */
    this._resizeSlots = function() {
        componentHandlerService.getFirstSmartEditComponentChildren('body').each(function(index, slotComponent) {
            var slotComponentID = componentHandlerService.getFromSelector(slotComponent).attr(ID_ATTRIBUTE);
            var slotComponentType = componentHandlerService.getFromSelector(slotComponent).attr(TYPE_ATTRIBUTE);

            var newSlotTop = -1;
            var newSlotBottom = -1;

            var currentSlotHeight = parseFloat(window.getComputedStyle(slotComponent).height) || 0;
            var currentSlotVerticalPadding = parseFloat(window.getComputedStyle(slotComponent).paddingTop) +
                parseFloat(window.getComputedStyle(slotComponent).paddingBottom);

            componentHandlerService.getFromSelector(slotComponent)
                .find("." + COMPONENT_CLASS)
                .filter(function(index, componentInSlot) {
                    componentInSlot = componentHandlerService.getFromSelector(componentInSlot);
                    return (componentInSlot.attr(ID_ATTRIBUTE) !== slotComponentID && componentInSlot.attr(TYPE_ATTRIBUTE) !== slotComponentType);
                })
                .each(function(compIndex, component) {
                    if (componentHandlerService.getFromSelector(component).is(":visible")) {
                        var componentDimensions = component.getBoundingClientRect();
                        newSlotTop = newSlotTop === -1 ? componentDimensions.top :
                            Math.min(newSlotTop, componentDimensions.top);
                        newSlotBottom = newSlotBottom === -1 ? componentDimensions.bottom :
                            Math.max(newSlotBottom, componentDimensions.bottom);
                    }
                });

            var newSlotHeight = newSlotBottom - newSlotTop;

            if (Math.abs(currentSlotHeight - newSlotHeight) > 0.001) {
                var slotUniqueKey = slotComponentID + "_" + slotComponentType;
                var oldSlotHeight = this._slotOriginalHeights[slotUniqueKey];
                if (!oldSlotHeight) {
                    oldSlotHeight = currentSlotHeight;
                    this._slotOriginalHeights[slotUniqueKey] = oldSlotHeight;
                }
                if (newSlotHeight + currentSlotVerticalPadding > oldSlotHeight) {
                    slotComponent.style.height = (newSlotHeight + currentSlotVerticalPadding) + "px";
                } else {
                    slotComponent.style.height = oldSlotHeight + 'px';
                }
            }
        }.bind(this));
    };

    this.renderSlots = function(_slotIds) {

        if (isBlank(_slotIds) || (_slotIds instanceof Array && _slotIds.length === 0)) {
            return $q.reject("renderService.renderSlots.slotIds.required");
        }
        if (typeof _slotIds === 'string') {
            _slotIds = [_slotIds];
        }

        //need to retrieve unique set of slotIds, happens when moving a component within a slot
        var slotIds = lodash.uniqBy(_slotIds, function(slotId) {
            return slotId;
        });

        // see if storefront can handle the rerendering
        var slotsRemaining = slotIds.filter(function(id) {
            return !seNamespace.renderComponent(id);
        });

        if (slotsRemaining.length <= 0) {
            //all were handled by storefront
            return $q.when(true);
        } else {
            return $http({
                method: 'GET',
                url: $location.absUrl(),
                headers: {
                    'Pragma': 'no-cache'
                }
            }).then(function(response) {
                var root = unsafeParseHTML(response.data);
                slotsRemaining.forEach(function(slotId) {
                    var slotSelector = "." + COMPONENT_CLASS + "[" + TYPE_ATTRIBUTE + "='ContentSlot'][" + ID_ATTRIBUTE + "='" + slotId + "']";
                    var slotToBeRerendered = extractFromElement(root, slotSelector);
                    var originalSlot = componentHandlerService.getFromSelector(slotSelector);
                    originalSlot.html(slotToBeRerendered.html());
                });
                this._reprocessPage();
            }.bind(this), function(errorResponse) {
                alertService.showDanger({
                    message: errorResponse.message
                });
                return $q.reject(errorResponse.message);
            });
        }

    };

    this.renderComponent = function(componentId, componentType) {
        var component = componentHandlerService.getComponent(componentId, componentType);
        var slotId = componentHandlerService.getParent(component).attr(ID_ATTRIBUTE);
        if (seNamespace.renderComponent(componentId, componentType, slotId)) {
            return $q.when(true);
        } else {
            return this.renderSlots(slotId);
        }
    };



    this.renderRemoval = function(componentId, componentType, slotId) {
        var removedComponents = componentHandlerService.getComponentUnderSlot(componentId, componentType, slotId, null).remove();
        this.refreshOverlayDimensions();
        return removedComponents;
    };

    /*
     * Given a smartEdit component in the storefront layer, its clone in the smartEdit overlay is removed and the pertaining decorators destroyed.
     * This method operates recursively on all smartEditComponent children
     *
     * @param {Element} element The original CMS component element from the storefront.
     * @param {Element=} parent the closest smartEditComponent parent, expected to be null for the highest elements
     * @param {Object=} oldAttributes The map of former attributes of the element. necessary when the element has mutated since the last creation
     */
    this._destroyComponent = function(_component, _parent, oldAttributes) {

        var component = componentHandlerService.getFromSelector(_component);
        var parent = componentHandlerService.getFromSelector(_parent);

        sakExecutor.destroyScope(component);
        var componentInOverlayId = oldAttributes && oldAttributes[ID_ATTRIBUTE] ? oldAttributes[ID_ATTRIBUTE] : component.attr(ID_ATTRIBUTE);
        var componentInOverlayType = oldAttributes && oldAttributes[TYPE_ATTRIBUTE] ? oldAttributes[TYPE_ATTRIBUTE] : component.attr(TYPE_ATTRIBUTE);

        //the node is no longer attached so can't find parent
        if (parent.attr(ID_ATTRIBUTE)) {
            componentHandlerService.getOverlayComponentWithinSlot(componentInOverlayId, componentInOverlayType, parent.attr(ID_ATTRIBUTE)).remove();
        } else {
            componentHandlerService.getComponentInOverlay(componentInOverlayId, componentInOverlayType).remove();
        }

        componentHandlerService.getFirstSmartEditComponentChildren(component).each(function(index, childElement) {
            this._destroyComponent(childElement, component);
        }.bind(this));

    };


    this._markSmartEditAsReady = function() {
        this._smartEditBootstrapGateway.publish('smartEditReady');
    };

    this._isComponentVisible = function(component) {
        // NOTE: This might not work as expected for fixed positioned items. For those cases a more expensive
        // check must be performed (get the component style and check if it's visible or not).
        return (component.offsetParent !== null);
    };

    this._reprocessPage = function() {
        seNamespace.reprocessPage();
    };

    this._compile = function(component, scope) {
        return $compile(component)(scope);
    };

    this._getDocument = function() {
        return document;
    };

    crossFrameEventService.subscribe(EVENT_PERSPECTIVE_CHANGED, function(eventId, isNonEmptyPerspective) {
        this.renderPage(isNonEmptyPerspective);
    }.bind(this));

    gatewayProxy.initForService(this, ["blockRendering", "isRenderingBlocked", "renderSlots", "renderComponent", "renderRemoval", "toggleOverlay", "refreshOverlayDimensions", "renderPage"]);
});
