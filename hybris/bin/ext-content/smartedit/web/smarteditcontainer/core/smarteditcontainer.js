/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
window.smartEditBootstrapped = {}; //storefront actually loads twice all the JS files, including webApplicationInjector.js, smartEdit must be protected against receiving twice a smartEditBootstrap event
angular.module('smarteditcontainer', [
        'componentHandlerServiceModule',
        'configModule',
        'landingPageModule',
        'templateCacheDecoratorModule',
        'ngRoute',
        'ngResource',
        'ui.bootstrap',
        'coretemplates',
        'loadConfigModule',
        'iFrameManagerModule',
        'alertsBoxModule',
        'httpAuthInterceptorModule',
        'experienceInterceptorModule',
        'bootstrapServiceModule',
        'toolbarModule',
        'leftToolbarModule',
        'modalServiceModule',
        'previewTicketInterceptorModule',
        'catalogServiceModule',
        'catalogDetailsModule',
        'experienceSelectorButtonModule',
        'experienceSelectorModule',
        'sharedDataServiceModule',
        'inflectionPointSelectorModule',
        'paginationFilterModule',
        'resourceLocationsModule',
        'experienceServiceModule',
        'eventServiceModule',
        'urlServiceModule',
        'perspectiveServiceModule',
        'perspectiveSelectorModule',
        'authorizationModule',
        'hasOperationPermissionModule',
        'l10nModule',
        'treeModule',
        'yInfiniteScrollingModule',
        'ySelectModule',
        'yHelpModule',
        'crossFrameEventServiceModule',
        'renderServiceModule',
        'systemAlertsModule',
        'yCollapsibleContainerModule',
        'seDropdownModule',
        'permissionServiceModule',
        'yNotificationPanelModule',
        'catalogAwareRouteResolverModule',
        'catalogVersionPermissionModule',
        'httpErrorInterceptorServiceModule',
        'unauthorizedErrorInterceptorModule',
        'resourceNotFoundErrorInterceptorModule',
        'nonvalidationErrorInterceptorModule',
        'previewErrorInterceptorModule',
        'retryInterceptorModule',
        'seConstantsModule',
        'pageSensitiveDirectiveModule'
    ])
    .config(function(LANDING_PAGE_PATH, STOREFRONT_PATH, STOREFRONT_PATH_WITH_PAGE_ID, $routeProvider, $logProvider, catalogAwareRouteResolverFunctions) {
        $routeProvider.when(LANDING_PAGE_PATH, {
                template: '<landing-page></landing-page>'
            })
            .when(STOREFRONT_PATH, {
                templateUrl: 'mainview.html',
                controller: 'defaultController',
                resolve: {
                    setExperience: catalogAwareRouteResolverFunctions.setExperience
                }
            })
            .when(STOREFRONT_PATH_WITH_PAGE_ID, {
                templateUrl: 'mainview.html',
                controller: 'defaultController',
                resolve: {
                    setExperience: catalogAwareRouteResolverFunctions.setExperience
                }
            })
            .otherwise({
                redirectTo: LANDING_PAGE_PATH
            });

        $logProvider.debugEnabled(false);
    })
    .service('smartEditBootstrapGateway', function(gatewayFactory) {
        return gatewayFactory.createGateway('smartEditBootstrap');
    })
    .run(
        function($rootScope, $log, $q, DEFAULT_RULE_NAME, EVENTS, smartEditBootstrapGateway, toolbarServiceFactory, perspectiveService, gatewayFactory, loadConfigManagerService, bootstrapService, iFrameManager, restServiceFactory, sharedDataService, urlService, featureService, storageService, renderService, closeOpenModalsOnBrowserBack, authorizationService, permissionService, httpErrorInterceptorService, unauthorizedErrorInterceptor, resourceNotFoundErrorInterceptor, nonValidationErrorInterceptor, previewErrorInterceptor, retryInterceptor, componentHandlerService) {
            gatewayFactory.initListener();

            httpErrorInterceptorService.addInterceptor(retryInterceptor);
            httpErrorInterceptorService.addInterceptor(unauthorizedErrorInterceptor);
            httpErrorInterceptorService.addInterceptor(resourceNotFoundErrorInterceptor);
            httpErrorInterceptorService.addInterceptor(nonValidationErrorInterceptor);
            httpErrorInterceptorService.addInterceptor(previewErrorInterceptor);

            loadConfigManagerService.loadAsObject().then(function(configurations) {
                sharedDataService.set('defaultToolingLanguage', configurations.defaultToolingLanguage);
            });

            var smartEditTitleToolbarService = toolbarServiceFactory.getToolbarService("smartEditTitleToolbar");

            smartEditTitleToolbarService.addItems([{
                key: 'topToolbar.leftToolbarTemplate',
                type: 'TEMPLATE',
                include: 'leftToolbarWrapperTemplate.html',
                priority: 1,
                section: 'left'
            }, {
                key: 'topToolbar.logoTemplate',
                type: 'TEMPLATE',
                include: 'logoTemplate.html',
                priority: 2,
                section: 'left'
            }, {
                key: 'topToolbar.deviceSupportTemplate',
                type: 'TEMPLATE',
                include: 'deviceSupportTemplate.html',
                priority: 1,
                section: 'right'
            }, {
                type: 'TEMPLATE',
                key: 'topToolbar.experienceSelectorTemplate',
                className: 'ySEPreviewSelector',
                include: 'experienceSelectorWrapperTemplate.html',
                priority: 1, //first in the middle
                section: 'middle'
            }]);

            var experienceSelectorToolbarService = toolbarServiceFactory.getToolbarService("experienceSelectorToolbar");

            experienceSelectorToolbarService.addItems([{
                key: "bottomToolbar.perspectiveSelectorTemplate",
                type: 'TEMPLATE',
                section: 'right',
                priority: 1,
                include: 'perspectiveSelectorWrapperTemplate.html'
            }]);

            function offSetStorefront() {
                // Set the storefront offset
                componentHandlerService._getTargetIframeWrapper().css('padding-top', componentHandlerService.getFromSelector('.ySmartEditToolbars').height() + 'px');
            }

            smartEditBootstrapGateway.subscribe("reloadFormerPreviewContext", function() {
                offSetStorefront();
                var deferred = $q.defer();
                iFrameManager.initializeCatalogPreview();
                deferred.resolve();
                return deferred.promise;
            });
            smartEditBootstrapGateway.subscribe("loading", function(eventId, data) {
                var deferred = $q.defer();

                iFrameManager.setCurrentLocation(data.location);
                iFrameManager.showWaitModal();

                delete window.smartEditBootstrapped[data.location];

                perspectiveService.clearActivePerspective();

                return deferred.promise;
            });
            smartEditBootstrapGateway.subscribe("bootstrapSmartEdit", function(eventId, data) {
                offSetStorefront();
                var deferred = $q.defer();
                if (!window.smartEditBootstrapped[data.location]) {
                    window.smartEditBootstrapped[data.location] = true;
                    loadConfigManagerService.loadAsObject().then(function(configurations) {
                        bootstrapService.bootstrapSEApp(configurations);
                        deferred.resolve();
                    });
                } else {
                    deferred.resolve();
                }
                return deferred.promise;
            });

            smartEditBootstrapGateway.subscribe("smartEditReady", function() {
                var deferred = $q.defer();
                deferred.resolve();

                iFrameManager.hideWaitModal();
                return deferred.promise;
            });

            $rootScope.$on('$routeChangeSuccess', function() {
                closeOpenModalsOnBrowserBack();
            });

            gatewayFactory.createGateway('accessTokens').subscribe("get", function() {
                return $q.when(storageService.getAuthTokens());
            });

            permissionService.registerDefaultRule({
                names: [DEFAULT_RULE_NAME],
                verify: function(permissionNameObjs) {
                    var permisssionNames = permissionNameObjs.map(function(permissionName) {
                        return permissionName.name;
                    });
                    return authorizationService.hasGlobalPermissions(permisssionNames);
                }
            });
        })
    .controller(
        'defaultController',
        /* jshint unused:false*/
        /*need to inject for gatewayProxy initialization*/
        function(iFrameManager, experienceService, sharedDataService) {
            iFrameManager.applyDefault();
            iFrameManager.initializeCatalogPreview();

            var bodyTag = angular.element(document.querySelector('body'));
            bodyTag.addClass('is-storefront');
        });
