/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('catalogVersionPermissionModule', [
        'catalogVersionPermissionRestServiceModule',
        'catalogServiceModule',
        'resourceLocationsModule'
    ])
    .constant('PERMISSION_TYPES', {
        READ: 'read',
        WRITE: 'write'
    })
    /**
     * @ngdoc service
     * @name catalogVersionPermissionModule.service:catalogVersionPermissionService
     * @description
     * # The catalogVersionPermissionService
     *
     * The catalog version permission service provides a logic that allows to verify
     * read and write permissions for a particular catalog version.
     */
    .service('catalogVersionPermissionService', function($q, catalogVersionPermissionRestService, catalogService, CONTEXT_CATALOG, CONTEXT_CATALOG_VERSION, CONTEXT_SITE_ID, PERMISSION_TYPES) {
        var hasPermission = function(accessType, catalogId, catalogVersion, siteId) {

            return shouldIgnoreCatalogPermissions(accessType, catalogId, catalogVersion, siteId).then(function(shouldOverride) {
                return catalogVersionPermissionRestService.getCatalogVersionPermissions(
                    catalogId, catalogVersion
                ).then(function(response) {
                    if (response.permissions) {
                        var permission = response.permissions.find(function(permission) {
                            return permission.key === accessType;
                        });
                        var value = (permission ? permission.value : 'false');
                        return $q.when(value === 'true' || shouldOverride);
                    } else {
                        return $q.when(false);
                    }
                });
            });
        };

        /**
         * if in the context of an experience AND the catalogVersion is the active one, then permissions should be ignored in read mode
         */
        var shouldIgnoreCatalogPermissions = function(accessType, catalogId, catalogVersion, siteId) {
            var promise = (siteId && accessType === PERMISSION_TYPES.READ) ? catalogService.getActiveContentCatalogVersionByCatalogId(catalogId) : $q.when();
            return promise.then(function(versionCheckedAgainst) {
                return versionCheckedAgainst === catalogVersion;
            });
        };
        /**
         * Verifies whether current user has write or read permission for current catalog version.
         * @param {String} accessType
         */
        var hasCurrentCatalogPermission = function(accessType) {
            return catalogService.retrieveUriContext().then(function(data) {
                return hasPermission(accessType, data[CONTEXT_CATALOG], data[CONTEXT_CATALOG_VERSION], data[CONTEXT_SITE_ID]);
            });
        };

        /**
         * @ngdoc method
         * @name catalogVersionPermissionModule.service:catalogVersionPermissionService#hasWritePermission
         * @methodOf catalogVersionPermissionModule.service:catalogVersionPermissionService
         *
         * @description
         * Verifies whether current user has write permission for provided catalogId and catalogVersion.
         *
         * @param {String} catalogId catalog id
         * @param {String} catalogVersion catalog version
         * @returns {Promise<Boolean>} A promise resolving to a boolean `true` if current user has write permission, else `false`
         */
        this.hasWritePermission = function(catalogId, catalogVersion) {
            return hasPermission(PERMISSION_TYPES.WRITE, catalogId, catalogVersion);
        };

        /**
         * @ngdoc method
         * @name catalogVersionPermissionModule.service:catalogVersionPermissionService#hasReadPermission
         * @methodOf catalogVersionPermissionModule.service:catalogVersionPermissionService
         *
         * @description
         * Verifies whether current user has read permission for provided catalogId and catalogVersion.
         *
         * @param {String} catalogId catalog id
         * @param {String} catalogVersion catalog version
         * @returns {Promise<Boolean>} A promise resolving to a boolean `true` if current user has read permission, else `false`
         */
        this.hasReadPermission = function(catalogId, catalogVersion) {
            return hasPermission(PERMISSION_TYPES.READ, catalogId, catalogVersion);
        };

        /**
         * @ngdoc method
         * @name catalogVersionPermissionModule.service:catalogVersionPermissionService#hasWritePermissionOnCurrent
         * @methodOf catalogVersionPermissionModule.service:catalogVersionPermissionService
         *
         * @description
         * Verifies whether current user has write permission for current catalog version.
         *
         * @returns {Promise<Boolean>} A promise resolving to a boolean `true` if current user has write permission, else `false`
         */
        this.hasWritePermissionOnCurrent = function() {
            return hasCurrentCatalogPermission(PERMISSION_TYPES.WRITE);
        };

        /**
         * @ngdoc method
         * @name catalogVersionPermissionModule.service:catalogVersionPermissionService#hasReadPermissionOnCurrent
         * @methodOf catalogVersionPermissionModule.service:catalogVersionPermissionService
         *
         * @description
         * Verifies whether current user has read permission for current catalog version.
         *
         * @returns {Promise<Boolean>} A promise resolving to a boolean `true` if current user has read permission, else `false`
         */
        this.hasReadPermissionOnCurrent = function() {
            return hasCurrentCatalogPermission(PERMISSION_TYPES.READ);
        };
    });
