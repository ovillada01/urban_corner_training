angular.module('coretemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/common/components/yCollapsibleContainer/accordion-group.html',
    "<div role=\"tab\"\n" +
    "    id=\"{{::headingId}}\"\n" +
    "    aria-selected=\"{{isOpen}}\"\n" +
    "    class=\"yCollapsibleContainer__header\"\n" +
    "    ng-keypress=\"toggleOpen($event)\">\n" +
    "\n" +
    "    <a role=\"button\"\n" +
    "        data-toggle=\"collapse\"\n" +
    "        href\n" +
    "        aria-expanded=\"{{isOpen}}\"\n" +
    "        aria-controls=\"{{::panelId}}\"\n" +
    "        tabindex=\"0\"\n" +
    "        class=\"accordion-toggle yCollapsibleContainer__title\"\n" +
    "        ng-click=\"toggleOpen()\"\n" +
    "        uib-accordion-transclude=\"heading\"\n" +
    "        ng-disabled=\"isDisabled\"\n" +
    "        uib-tabindex-toggle>\n" +
    "        <span uib-accordion-header\n" +
    "            ng-class=\"{'text-muted': isDisabled}\">{{heading}}</span>\n" +
    "    </a>\n" +
    "\n" +
    "    <a class=\"yCollapsibleContainer__icon btn btn-link\"\n" +
    "        data-toggle-id=\"toggle-1\"\n" +
    "        title=\"{{isOpen ? 'se.ycollapsible.action.collapse' : 'se.ycollapsible.action.expand' | translate}}\"\n" +
    "        aria-expanded=\"{{isOpen}}\"\n" +
    "        ng-click=\"toggleOpen()\">\n" +
    "        <span class=\"hyicon hyicon-chevron y-collapsible-icon\"\n" +
    "            data-ng-class=\"{\n" +
    "            'y-collapsible-icon--collapse':isOpen, 'y-collapsible-icon--expand':!isOpen}\"></span>\n" +
    "    </a>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div id=\"{{::panelId}}\"\n" +
    "    aria-labelledby=\"{{::headingId}}\"\n" +
    "    aria-hidden=\"{{!isOpen}}\"\n" +
    "    role=\"tabpanel\"\n" +
    "    class=\"panel-collapse collapse\"\n" +
    "    uib-collapse=\"!isOpen\">\n" +
    "    <div ng-transclude></div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/components/yCollapsibleContainer/yCollapsibleContainer.html',
    "<uib-accordion class=\"y-collapsible\">\n" +
    "    <div uib-accordion-group\n" +
    "        class=\"yCollapsibleContainer__group y-collapsible__group\"\n" +
    "        data-template-url=\"web/common/components/yCollapsibleContainer/accordion-group.html\"\n" +
    "        data-is-open=\"$yCollapsibleContainerCtrl.configuration.expandedByDefault\"\n" +
    "        data-ng-class=\"$yCollapsibleContainerCtrl.getIconRelatedClassname()\">\n" +
    "\n" +
    "        <uib-accordion-heading class=\"y-collapsible__heading\">\n" +
    "            <div class=\"yCollapsibleContainer__title y-collapsible__title\"\n" +
    "                data-ng-transclude=\"collapsible-container-title\"></div>\n" +
    "        </uib-accordion-heading>\n" +
    "\n" +
    "        <div class=\"y-collapsible__content\"\n" +
    "            data-ng-transclude=\"collapsible-container-content\"></div>\n" +
    "\n" +
    "    </div>\n" +
    "</uib-accordion>"
  );


  $templateCache.put('web/common/components/yMessage/yMessage.html',
    "<div message-id=\"{{$ctrl.messageId}}\"\n" +
    "    class=\"{{$ctrl.classes}}\">\n" +
    "    <div class=\"y-message-icon-outer\">\n" +
    "        <span class=\"{{$ctrl.icon}} y-message-icon\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"y-message-text\">\n" +
    "        <div class=\"y-message-info-title\"\n" +
    "            data-ng-transclude=\"messageTitle\">\n" +
    "        </div>\n" +
    "        <div class=\"y-message-info-description\"\n" +
    "            data-ng-transclude=\"messageDescription\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/core/services/loginDialog.html',
    "<div data-ng-cloak\n" +
    "    class=\"se-login\"\n" +
    "    data-ng-class=\"{'se-login--full': !modalController.initialized, 'se-login--modal': modalController.initialized }\">\n" +
    "    <div class=\"se-login--wrapper\">\n" +
    "        <div class=\"se-login--logo-wrapper\"\n" +
    "            data-ng-class=\"{'se-login--logo-wrapper_full': !modalController.initialized}\">\n" +
    "            <img src=\"static-resources/images/SmartEditLogo.svg\"\n" +
    "                class=\"se-login--logo\">\n" +
    "        </div>\n" +
    "        <form autocomplete=\"off\"\n" +
    "            data-ng-submit=\"modalController.submit(loginDialogForm)\"\n" +
    "            class=\"se-login--form\"\n" +
    "            name='loginDialogForm'\n" +
    "            novalidate>\n" +
    "            <div class=\"se-login--auth-message\"\n" +
    "                data-ng-if=\"modalController.initialized\">\n" +
    "                <div data-translate=\"se.logindialogform.reauth.message1\"></div>\n" +
    "                <div data-translate=\"se.logindialogform.reauth.message2\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\"\n" +
    "                data-ng-if=\"loginDialogForm.errorMessage && loginDialogForm.posted && (loginDialogForm.$invalid || loginDialogForm.failed)\">\n" +
    "                <div class=\"alert se-login--alert-error\"\n" +
    "                    id=\"invalidError\"\n" +
    "                    data-translate=\"{{ loginDialogForm.errorMessage }}\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <input class=\"se-login--form-control\"\n" +
    "                    type=\"text\"\n" +
    "                    id=\"username_{{modalController.authURIKey}}\"\n" +
    "                    name=\"username\"\n" +
    "                    placeholder=\"{{ 'se.authentication.form.input.username' | translate}}\"\n" +
    "                    autofocus\n" +
    "                    data-ng-model=\"modalController.auth.username\"\n" +
    "                    required/>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <input class=\"se-login--form-control\"\n" +
    "                    type=\"password\"\n" +
    "                    id='password_{{modalController.authURIKey}}'\n" +
    "                    name='password'\n" +
    "                    placeholder=\"{{ 'se.authentication.form.input.password' | translate}}\"\n" +
    "                    data-ng-model=\"modalController.auth.password\"\n" +
    "                    required/>\n" +
    "            </div>\n" +
    "            <div class=\"se-login--form-group\">\n" +
    "                <language-selector class=\"se-login-language\"></language-selector>\n" +
    "            </div>\n" +
    "            <button class=\"btn btn-lg btn-primary btn-block se-login--btn\"\n" +
    "                id=\"submit_{{modalController.authURIKey}}\"\n" +
    "                name=\"submit\"\n" +
    "                type=\"submit\"\n" +
    "                data-translate=\"se.authentication.form.button.submit\"></button>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/core/services/waitDialog.html',
    "<div class=\"panel panel-default ySEPanelSpinner\">\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"spinner ySESpinner\">\n" +
    "            <div class=\"spinner-container spinner-container1\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"spinner-container spinner-container2\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"spinner-container spinner-container3\">\n" +
    "                <div class=\"spinner-circle1\"></div>\n" +
    "                <div class=\"spinner-circle2\"></div>\n" +
    "                <div class=\"spinner-circle3\"></div>\n" +
    "                <div class=\"circle4\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"ySESpinnerText\"\n" +
    "            data-translate=\"{{ modalController._modalManager.loadingMessage }}\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/alerts/alertsTemplate.html',
    "<div class=\"row-fluid\"\n" +
    "    data-ng-if=\"alerts!=null &amp;&amp; alerts.length>0\">\n" +
    "    <div data-ng-repeat=\"alert in alerts\"\n" +
    "        class=\"ng-class: {'col-xs-12':true, 'alert':true, 'alert-danger':(alert.successful==false),'alert-success':(alert.successful==true)};\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"close\"\n" +
    "            data-ng-hide=\"alert.closeable==false\"\n" +
    "            data-ng-click=\"dismissAlert($index);\">&times;</button>\n" +
    "        <span id=\"alertMsg\">{{alert.message | translate}}</span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/authorization/hasOperationPermissionTemplate.html',
    "<div>\n" +
    "    <div ng-if=\"ctrl.isPermissionGranted\">\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/dropdownMenu/yDropdownMenuTemplate.html',
    "<div class=\"y-dropdown-more-menu\"\n" +
    "    data-uib-dropdown>\n" +
    "    <button class=\"btn btn-link dropdown-toggle\"\n" +
    "        type=\"button\"\n" +
    "        data-uib-dropdown-toggle=\"dropdown\"\n" +
    "        aria-haspopup=\"true\"\n" +
    "        aria-expanded=\"true\">\n" +
    "        <span class=\"hyicon hyicon-more y-dropdown-more-menu__ddlb__icon\"></span>\n" +
    "    </button>\n" +
    "    <ul class=\"dropdown-menu dropdown-menu-right\"\n" +
    "        data-uib-dropdown-menu>\n" +
    "        <li data-ng-repeat='dropdownItem in dropdown.dropdownItems'\n" +
    "            data-ng-if=\"dropdownItem.condition(dropdown.selectedItem)\"><a data-ng-click=\"dropdownItem.callback(dropdown.selectedItem) \">{{dropdownItem.key | translate}}</a></li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/boolean/booleanTemplate.html',
    "<div class=\"ySEBooleanField\">\n" +
    "    <span class=\"y-toggle y-toggle-lg\">\n" +
    "        <input type=\"checkbox\"\n" +
    "            id=\"{{$ctrl.field.qualifier}}-checkbox\"\n" +
    "            class=\"ySEBooleanField__input\"\n" +
    "            placeholder=\"{{$ctrl.field.tooltip| translate}}\"\n" +
    "            name=\"{{$ctrl.field.qualifier}}\"\n" +
    "            data-ng-disabled=\"!$ctrl.field.editable\"\n" +
    "            data-ng-model=\"$ctrl.model[$ctrl.qualifier]\" />\n" +
    "        <label class=\"ySEBooleanField__label\"\n" +
    "            for=\"{{$ctrl.field.qualifier}}-checkbox\"></label>\n" +
    "        <p data-ng-if=\"$ctrl.field.labelText && !$ctrl.model[$ctrl.qualifier]\"\n" +
    "            class=\"ySEBooleanField__text\">{{$ctrl.field.labelText| translate}}</p>\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/dateTimePicker/dateTimePickerTemplate.html',
    "<div class=\"input-group se-date-field\"\n" +
    "    id=\"date-time-picker-{{::name}}\"\n" +
    "    data-ng-show=\"isEditable\">\n" +
    "    <input type=\"text\"\n" +
    "        class=\"form-control se-date-field--input\"\n" +
    "        data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings, 'se-input--is-disabled': !isEditable}\"\n" +
    "        placeholder=\"{{::placeholderText | translate}}\"\n" +
    "        name=\"{{::name}}\" />\n" +
    "    <span class=\"input-group-addon se-date-field--button\"\n" +
    "        data-ng-class=\"{ 'se-date-field--button-has-error': field.hasErrors, 'se-date-field--button-has-warning': field.hasWarnings }\">\n" +
    "        <span class=\"glyphicon glyphicon-calendar se-date-field--button-icon\"></span>\n" +
    "    </span>\n" +
    "</div>\n" +
    "<div class=\"input-group date se-date-field\"\n" +
    "    data-ng-if=\"!isEditable\">\n" +
    "    <input type='text'\n" +
    "        class=\"form-control se-date-field--input\"\n" +
    "        data-ng-class=\"{'se-input--is-disabled': !isEditable}\"\n" +
    "        data-ng-model=\"model\"\n" +
    "        data-date-formatter\n" +
    "        data-ng-disabled=\"true\"\n" +
    "        data-format-type=\"short\">\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/dropdown/seDropdownTemplate.html',
    "<y-select data-ng-if=\"dropdown.initialized\"\n" +
    "    data-id=\"{{dropdown.qualifier}}\"\n" +
    "    data-ng-click=\"dropdown.onClick()\"\n" +
    "    data-placeholder=\"field.placeholder\"\n" +
    "    data-ng-model=\"model[qualifier]\"\n" +
    "    data-on-change=\"dropdown.triggerAction\"\n" +
    "    data-fetch-strategy=\"dropdown.fetchStrategy\"\n" +
    "    data-reset=\"dropdown.reset\"\n" +
    "    data-multi-select=\"dropdown.isMultiDropdown\"\n" +
    "    data-controls=\"dropdown.isMultiDropdown\"\n" +
    "    data-is-read-only=\"!field.editable\"\n" +
    "    data-item-template=\"itemTemplateUrl\" />"
  );


  $templateCache.put('web/common/services/genericEditor/errors/seGenericEditorFieldErrorsTemplate.html',
    "<div data-ng-if=\"ctrl.getFilteredErrors().length > 0\">\n" +
    "    <span data-ng-repeat=\"error in ctrl.getFilteredErrors()\"\n" +
    "        class=\"se-help-block--has-error help-block\">\n" +
    "        {{error | translate}}\n" +
    "    </span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorFieldTemplate.html',
    "<div validation-id=\"{{field.qualifier}}\"\n" +
    "    data-ng-cloak\n" +
    "    class=\"ySEField\">\n" +
    "\n" +
    "    <div data-ng-if=\"field.prefixText\"\n" +
    "        class=\"ySEText ySEFieldPrefix\">{{field.prefixText | translate}}</div>\n" +
    "    <div data-ng-if=\"field.template\"\n" +
    "        class=\"ySEGenericEditorField\"\n" +
    "        data-ng-include=\"field.template\"></div>\n" +
    "\n" +
    "    <se-generic-editor-field-messages data-field=\"field\"\n" +
    "        data-qualifier=\"qualifier\"></se-generic-editor-field-messages>\n" +
    "    <div data-ng-if=\"field.postfixText\"\n" +
    "        class=\"ySEText ySEFieldPostfix\">{{field.postfixText | translate}}</div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorFieldWrapperTemplate.html',
    "<generic-editor-field data-editor=\"model.editor\"\n" +
    "    data-field=\"model.field\"\n" +
    "    data-model=\"model.editor.component[model.field.qualifier]\"\n" +
    "    data-qualifier=\"tabId\"\n" +
    "    data-id=\"id\" />"
  );


  $templateCache.put('web/common/services/genericEditor/genericEditorTemplate.html',
    "<div data-ng-cloak\n" +
    "    class=\"ySEGenericEditor\">\n" +
    "    <form name=\"componentForm\"\n" +
    "        novalidate\n" +
    "        data-ng-submit=\"ge.editor.submit(componentForm)\"\n" +
    "        class=\"no-enter-submit\">\n" +
    "        <div class=\"modal-header\"\n" +
    "            data-ng-show=\"modalHeaderTitle\">\n" +
    "            <h4 class=\"modal-title\">{{modalHeaderTitle| translate}}</h4>\n" +
    "        </div>\n" +
    "        <div class=\"ySErow\"\n" +
    "            data-ng-repeat=\"holder in ge.editor.holders\">\n" +
    "            <div class=\"\">\n" +
    "                <label class=\"control-label\"\n" +
    "                    data-ng-class=\"{'required': holder.field.required}\"\n" +
    "                    id=\"{{holder.field.qualifier}}-label\">{{holder.field.i18nKey | lowercase | translate}}\n" +
    "                </label>\n" +
    "\n" +
    "            </div>\n" +
    "            <div class=\"ySEGenericEditorFieldStructure\"\n" +
    "                id=\"{{holder.field.qualifier}}\"\n" +
    "                data-cms-field-qualifier=\"{{holder.field.qualifier}}\"\n" +
    "                data-cms-structure-type=\"{{holder.field.cmsStructureType}}\">\n" +
    "                <localized-element data-ng-if=\"holder.field.localized\"\n" +
    "                    data-model=\"holder\"\n" +
    "                    data-languages=\"ge.editor.languages\"\n" +
    "                    class=\"form-group multi-tabs-editor\"\n" +
    "                    data-input-template=\"genericEditorFieldWrapperTemplate.html\"></localized-element>\n" +
    "                <generic-editor-field data-ng-if=\"!holder.field.localized\"\n" +
    "                    data-editor=\"holder.editor\"\n" +
    "                    data-field=\"holder.field\"\n" +
    "                    data-model=\"ge.editor.component\"\n" +
    "                    data-qualifier=\"holder.field.qualifier\"\n" +
    "                    data-id=\"ge.editor.id\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"ySErow\"\n" +
    "            data-ng-if=\"ge.showNoEditSupportDisclaimer()\">\n" +
    "            <y-message data-type=\"info\"\n" +
    "                id=\"GenericEditor.NoEditingSupportDisclaimer\">\n" +
    "                <message-description>\n" +
    "                    <translate>se.editor.notification.editing.not.supported</translate>\n" +
    "                </message-description>\n" +
    "            </y-message>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"ySEBtnRow modal-footer\"\n" +
    "            data-ng-show=\"(ge.editor.alwaysShowReset || (ge.editor.showReset===true && ge.editor.isDirty()  && ge.editor.isValid())) || (ge.editor.alwaysShowSubmit || (ge.editor.showSubmit===true && ge.editor.isDirty()  && ge.editor.isValid() && componentForm.$valid))\">\n" +
    "            <button type=\"button\"\n" +
    "                id=\"cancel\"\n" +
    "                class=\"btn btn-subordinate\"\n" +
    "                data-ng-if=\"ge.editor.alwaysShowReset || (ge.editor.showReset===true && ge.editor.isDirty() && ge.editor.isValid())\"\n" +
    "                data-ng-click=\"ge.reset()\">{{ge.cancelButtonText| translate}}</button>\n" +
    "            <button type=\"submit\"\n" +
    "                id=\"submit\"\n" +
    "                class=\"btn btn-primary\"\n" +
    "                data-ng-if=\"ge.editor.alwaysShowSubmit || (ge.editor.showSubmit===true && ge.editor.isDirty()  && ge.editor.isValid() && componentForm.$valid)\"\n" +
    "                data-ng-disabled=\"ge.isSubmitDisabled(componentForm)\">{{ge.submitButtonText| translate}}</button>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/richText/seRichTextFieldTemplate.html',
    "<textarea class=\"form-control\"\n" +
    "    data-ng-class=\"{'has-error': ctrl.field.hasErrors}\"\n" +
    "    name=\"{{ctrl.field.qualifier}}-{{ctrl.qualifier}}\"\n" +
    "    data-ng-disabled=\"!ctrl.field.editable\"\n" +
    "    data-ng-model=\"ctrl.model[ctrl.qualifier]\"\n" +
    "    data-ng-change=\"ctrl.reassignUserCheck()\"></textarea>\n" +
    "<div data-ng-if=\"ctrl.requiresUserCheck()\">\n" +
    "    <input type=\"checkbox\"\n" +
    "        data-ng-model=\"ctrl.field.isUserChecked\" />\n" +
    "    <span class=\"ng-class:{'warning-check-msg':true, 'not-checked':ctrl.editor.hasFrontEndValidationErrors && !ctrl.field.isCheckedByUser}\"\n" +
    "        data-translate=\"se.editor.richtext.check\">{{'se.editor.richtext.check' | translate}}</span>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/booleanWrapperTemplate.html',
    "<se-boolean data-model=\"model\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-field=\"field\"></se-boolean>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dateTimePickerWrapperTemplate.html',
    "<date-time-picker data-name=\"field.qualifier\"\n" +
    "    data-model=\"model[qualifier]\"\n" +
    "    data-is-editable=\"field.editable\"\n" +
    "    data-field=\"field\"></date-time-picker>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dropdownTemplate.html',
    "<ui-select id=\"{{field.qualifier}}-selector\"\n" +
    "    ng-model=\"model[qualifier]\"\n" +
    "    theme=\"select2\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\"\n" +
    "    search-enabled=\"false\"\n" +
    "    data-dropdown-auto-width=\"false\"\n" +
    "    class=\"se-generic-editor-dropdown\">\n" +
    "    <ui-select-match placeholder=\"Select an option\"\n" +
    "        class=\"se-generic-editor-dropdown__match\">\n" +
    "        <span ng-bind=\"$select.selected.label\"></span>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{field.qualifier}}-list\"\n" +
    "        repeat=\"option in field.options\"\n" +
    "        position=\"down\"\n" +
    "        value=\"{{$selected.selected}}\">\n" +
    "        <span ng-bind=\"option.label\"></span>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/dropdownWrapperTemplate.html',
    "<se-dropdown data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\"></se-dropdown>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/enumTemplate.html',
    "<ui-select id=\"{{field.qualifier}}-selector\"\n" +
    "    ng-model=\"model[qualifier]\"\n" +
    "    theme=\"select2\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    reset-search-input=\"false\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\">\n" +
    "    <ui-select-match placeholder=\"{{'se.genericeditor.dropdown.placeholder' | translate}}\"\n" +
    "        allow-clear=\"true\">\n" +
    "        <span id=\"enum-{{field.qualifier}}\">{{$select.selected.label}}</span>\n" +
    "        <br/>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{field.qualifier}}-list\"\n" +
    "        repeat=\"option.code as option in field.options[qualifier]\"\n" +
    "        refresh=\"editor.refreshOptions(field, qualifier, $select.search)\"\n" +
    "        value=\"{{$select.selected.code}}\"\n" +
    "        refresh-delay=\"0\"\n" +
    "        position=\"down\">\n" +
    "        <small>\n" +
    "            <span>{{option.label}}</span>\n" +
    "        </small>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/errorMessageTemplate.html',
    "<span data-ng-if=\"field.errors\"\n" +
    "    data-ng-repeat=\"error in field.errors | filter:{language:qualifier}\"\n" +
    "    id='validation-error-{{field.qualifier}}-{{qualifier}}-{{$index}}'\n" +
    "    class=\"se-help-block--has-error help-block\">\n" +
    "    {{ error.message | translate }}\n" +
    "</span>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/floatTemplate.html',
    "<input type=\"number\"\n" +
    "    id=\"{{field.qualifier}}-float\"\n" +
    "    class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\"\n" +
    "    data-ng-pattern=\"/^([0-9]+)\\.{1}([0-9]{1,5})?$/\"\n" +
    "    step=\"{{ field.precision }}\" />"
  );


  $templateCache.put('web/common/services/genericEditor/templates/longStringTemplate.html',
    "<textarea class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\"></textarea>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/numberTemplate.html',
    "<input type=\"number\"\n" +
    "    min=\"1\"\n" +
    "    id=\"{{field.qualifier}}-number\"\n" +
    "    class=\"form-control\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\" />"
  );


  $templateCache.put('web/common/services/genericEditor/templates/richTextTemplate.html',
    "<se-rich-text-field data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-editor=\"editor\"></se-rich-text-field>"
  );


  $templateCache.put('web/common/services/genericEditor/templates/shortStringTemplate.html',
    "<input type=\"text\"\n" +
    "    id=\"{{field.qualifier}}-shortstring\"\n" +
    "    data-ng-class=\"{ 'se-input--has-error': field.hasErrors, 'se-input--has-warning': field.hasWarnings }\"\n" +
    "    class=\"form-control\"\n" +
    "    placeholder=\"{{field.tooltip| translate}}\"\n" +
    "    name=\"{{field.qualifier}}\"\n" +
    "    data-ng-disabled=\"!field.editable\"\n" +
    "    data-ng-model=\"model[qualifier]\" />"
  );


  $templateCache.put('web/common/services/genericEditor/validationMessages/seGenericEditorFieldMessagesTemplate.html',
    "<div>\n" +
    "    <div data-ng-if=\"ctrl.errors.length > 0\">\n" +
    "        <span data-ng-repeat=\"error in ctrl.errors\"\n" +
    "            class=\"se-help-block--has-error help-block\">\n" +
    "            {{ error | translate}}\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"ctrl.warnings.length > 0\">\n" +
    "        <span data-ng-repeat=\"warning in ctrl.warnings\"\n" +
    "            class=\"se-help-block--has-warning help-block\">\n" +
    "            {{ warning | translate }}\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/help/yHelpTemplate.html',
    "<span data-y-popover\n" +
    "    data-title=\"yHelp.title\"\n" +
    "    data-template=\"yHelp.template\"\n" +
    "    data-template-url=\"yHelp.templateUrl\"\n" +
    "    data-placement=\"yHelp.placement\"\n" +
    "    data-trigger=\"yHelp.trigger\">\n" +
    "    <span class=\"hyicon hyicon-help-icon\"></span>\n" +
    "</span>"
  );


  $templateCache.put('web/common/services/infiniteScrolling/yInfiniteScrollingTemplate.html',
    "<div data-ng-attr-id=\"{{scroll.containerId}}\"\n" +
    "    class=\"ySEInfiniteScrolling-container {{scroll.dropDownContainerClass}}\">\n" +
    "    <div class=\"ySEInfiniteScrolling {{scroll.dropDownClass}}\"\n" +
    "        data-infinite-scroll=\"scroll.nextPage()\"\n" +
    "        data-infinite-scroll-disabled=\"scroll.pagingDisabled\"\n" +
    "        data-infinite-scroll-distance=\"scroll.distance\"\n" +
    "        data-infinite-scroll-immediate-check=\"true\"\n" +
    "        data-infinite-scroll-container='scroll.container'>\n" +
    "        <div data-ng-transclude></div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"!scroll.pagingDisabled\"\n" +
    "        class=\"panel panel-default panel__ySEInfiniteScrolling\">\n" +
    "        <div class=\"panel-body panel-body__ySEInfiniteScrolling\">\n" +
    "            <div class=\"spinner\">\n" +
    "                <div class=\"spinner-container spinner-container1\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"spinner-container spinner-container2\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"spinner-container spinner-container3\">\n" +
    "                    <div class=\"spinner-circle1\"></div>\n" +
    "                    <div class=\"spinner-circle2\"></div>\n" +
    "                    <div class=\"spinner-circle3\"></div>\n" +
    "                    <div class=\"circle4\"></div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/languageSelector/languageSelectorTemplate.html',
    "<ui-select id=\"uiSelectToolingLanguage\"\n" +
    "    ng-model=\"ctrl.selectedLanguage\"\n" +
    "    on-select=\"ctrl.setSelectedLanguage($item)\"\n" +
    "    theme=\"select2\"\n" +
    "    title=\"\"\n" +
    "    style=\"width:100%\"\n" +
    "    search-enabled=\"false\"\n" +
    "    data-dropdown-auto-width=\"false\">\n" +
    "    <ui-select-match placeholder=\"{{'se.languageselector.dropdown.placeholder' | translate}}\">\n" +
    "        <span ng-bind=\"ctrl.selectedLanguage.name \"></span>\n" +
    "    </ui-select-match>\n" +
    "\n" +
    "    <ui-select-choices repeat=\"language in ctrl.languages track by language.isoCode\"\n" +
    "        position=\"down \">\n" +
    "        <span ng-bind=\"language.name\"></span>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/list/yEditableListDefaultItemTemplate.html',
    "<div>\n" +
    "    <span>{{node.uid}}</span>\n" +
    "    <y-drop-down-menu data-ng-if=\"ctrl.editable\"\n" +
    "        dropdown-items=\"ctrl.getDropdownItems()\"\n" +
    "        selected-item=\"this\"\n" +
    "        class=\"y-dropdown pull-right nav-node-editor-entry-item__more-menu\" />\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/list/yEditableListTemplate.html',
    "<div data-ng-class=\"{ 'y-editable-list-disabled': !ctrl.editable }\">\n" +
    "    <ytree remove-default-template=true\n" +
    "        data-root-node-uid=\"ctrl.rootId\"\n" +
    "        data-node-template-url=\"ctrl.itemTemplateUrl\"\n" +
    "        data-node-actions=\"ctrl.actions\"\n" +
    "        data-drag-options=\"ctrl.dragOptions\"\n" +
    "        data-show-as-list=\"true\" />\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/localizedElement/localizedElementTemplate.html',
    "<y-tabset data-model=\"model\"\n" +
    "    tabs-list=\"tabs\"\n" +
    "    data-num-tabs-displayed=\"6\">\n" +
    "</y-tabset>"
  );


  $templateCache.put('web/common/services/modalTemplate.html',
    "<div id=\"y-modal-dialog\">\n" +
    "    <div class=\"modal-header se-modal__header\"\n" +
    "        data-ng-if=\"modalController._modalManager.title\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"close\"\n" +
    "            data-ng-if=\"modalController._modalManager._showDismissButton()\"\n" +
    "            data-ng-click=\"modalController._modalManager._handleDismissButton()\">\n" +
    "            <span class=\"hyicon hyicon-close\"></span>\n" +
    "        </button>\n" +
    "        <h4 class=\"se-nowrap-ellipsis se-modal__header-title\"\n" +
    "            id=\"smartedit-modal-title-{{ modalController._modalManager.title }}\">{{ modalController._modalManager.title | translate }}&nbsp;{{ modalController._modalManager.titleSuffix | translate }}</h4>\n" +
    "    </div>\n" +
    "    <div class=\"modal-body\"\n" +
    "        id=\"modalBody\">\n" +
    "        <div data-ng-include=\"modalController.templateUrl\" />\n" +
    "    </div>\n" +
    "    <div class=\"modal-footer\"\n" +
    "        data-ng-if=\"modalController._modalManager._hasButtons()\">\n" +
    "        <span data-ng-repeat=\"button in modalController._modalManager.getButtons()\">\n" +
    "            <button id='{{ button.id }}'\n" +
    "                type=\"button\"\n" +
    "                data-ng-disabled=\"button.disabled\"\n" +
    "                data-ng-class=\"{ 'btn':true, 'btn-subordinate':button.style=='default', 'btn-primary':button.style=='primary' }\"\n" +
    "                data-ng-click=\"modalController._modalManager._buttonPressed(button)\">{{ button.label | translate }}</button>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/popover/yPopoverTemplate.html',
    "<div class=\"help-control\"\n" +
    "    data-uib-popover-html=\"ypop.template\"\n" +
    "    data-uib-popover-template=\"ypop.templateUrl\"\n" +
    "    data-popover-title=\"{{ypop.title | translate}}\"\n" +
    "    data-popover-append-to-body=\"true\"\n" +
    "    data-popover-placement=\"{{ypop.placement}}\"\n" +
    "    data-popover-trigger=\"'{{ypop.trigger}}'\"\n" +
    "    data-popover-is-open=\"ypop.isOpen\"\n" +
    "    data-ng-click=\"$event.stopPropagation()\">\n" +
    "    <div data-ng-transclude\n" +
    "        class=\"popoverAnchor\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/select/defaultItemTemplate.html',
    "<span class=\"y-select-default-item\"\n" +
    "    data-ng-bind-html=\"item.label | l10n | translate\"></span>"
  );


  $templateCache.put('web/common/services/select/uiSelectChoicesTemplate.html',
    "<ul tabindex=\"-1\"\n" +
    "    class=\"ui-select-choices ui-select-choices-content select2-results\">\n" +
    "    <li class=\"ui-select-choices-group\"\n" +
    "        ng-class=\"{'select2-result-with-children': $select.choiceGrouped($group) }\">\n" +
    "        <div ng-show=\"$select.choiceGrouped($group)\"\n" +
    "            class=\"ui-select-choices-group-label select2-result-label\"\n" +
    "            ng-bind=\"$group.name\"></div>\n" +
    "        <ul role=\"listbox\"\n" +
    "            id=\"ui-select-choices-{{ $select.generatedId }}\"\n" +
    "            ng-class=\"{'select2-result-sub': $select.choiceGrouped($group), 'select2-result-single': !$select.choiceGrouped($group) }\">\n" +
    "            <div data-ng-if=\"ySelect.bindingCompleted\">\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplate\"\n" +
    "                    data-compile-html=\"ySelect.resultsHeaderTemplate\"></div>\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplateUrl\"\n" +
    "                    data-ng-include=\"ySelect.resultsHeaderTemplateUrl\"></div>\n" +
    "            </div>\n" +
    "            <li class=\"y-infinite-scrolling__listbox-header\"\n" +
    "                data-ng-show=\"ySelect.showResultHeader()\">{{ ySelect.resultsHeaderLabel | translate }}</li>\n" +
    "            <li role=\"option\"\n" +
    "                ng-attr-id=\"ui-select-choices-row-{{ $select.generatedId }}-{{$index}}\"\n" +
    "                class=\"ui-select-choices-row\"\n" +
    "                ng-class=\"{'select2-highlighted': $select.isActive(this), 'select2-disabled': $select.isDisabled(this)}\">\n" +
    "                <div class=\"select2-result-label ui-select-choices-row-inner\"></div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/common/services/select/uiSelectPagedChoicesTemplate.html',
    "<ul tabindex=\"-1\"\n" +
    "    class=\"ui-select-choices ui-select-choices-content select2-results ui-select-choices__infinite-scrolling\">\n" +
    "    <li class=\"ui-select-choices-group\"\n" +
    "        data-ng-class=\"{'select2-result-with-children': $select.choiceGrouped($group) }\">\n" +
    "        <div data-ng-show=\"$select.choiceGrouped($group)\"\n" +
    "            class=\"ui-select-choices-group-label select2-result-label\"\n" +
    "            data-ng-bind=\"$group.name\">\n" +
    "        </div>\n" +
    "        <y-infinite-scrolling data-ng-if=\"$select.open\"\n" +
    "            data-drop-down-class=\"\"\n" +
    "            data-page-size=\"10\"\n" +
    "            data-mask=\"$select.search\"\n" +
    "            data-fetch-page=\"ySelect.fetchStrategy.fetchPage\"\n" +
    "            data-context=\"ySelect\">\n" +
    "            <ul role=\"listbox\"\n" +
    "                id=\"ui-select-choices-{{ $select.generatedId }}\"\n" +
    "                data-ng-class=\"{'select2-result-sub': $select.choiceGrouped($group), 'select2-result-single': !$select.choiceGrouped($group) }\">\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplate\"\n" +
    "                    data-compile-html=\"ySelect.resultsHeaderTemplate\"></div>\n" +
    "                <div data-ng-if=\"ySelect.resultsHeaderTemplateUrl\"\n" +
    "                    data-ng-include=\"ySelect.resultsHeaderTemplateUrl\"></div>\n" +
    "                <li class=\"y-infinite-scrolling__listbox-header\"\n" +
    "                    data-ng-show=\"ySelect.showResultHeader()\">{{ ySelect.resultsHeaderLabel | translate }}</li>\n" +
    "                <li role=\"option\"\n" +
    "                    data-ng-attr-id=\"ui-select-choices-row-{{ $select.generatedId }}-{{$index}}\"\n" +
    "                    class=\"ui-select-choices-row\"\n" +
    "                    data-ng-class=\"{'select2-highlighted': $select.isActive(this), 'select2-disabled': $select.isDisabled(this)}\">\n" +
    "                    <div class=\"select2-result-label ui-select-choices-row-inner\">\n" +
    "                    </div>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </y-infinite-scrolling>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/common/services/select/yMultiSelectTemplate.html',
    "<ui-select id=\"{{ySelect.id}}-selector\"\n" +
    "    multiple\n" +
    "    sortable=\"true\"\n" +
    "    theme=\"<%= theme %>\"\n" +
    "    data-ng-model=\"ySelect.model\"\n" +
    "    style=\"width:100%\"\n" +
    "    data-ng-change=\"ySelect.internalOnChange()\"\n" +
    "    ng-disabled=\"ySelect.isReadOnly\"\n" +
    "    class=\"se-generic-editor-dropdown\"\n" +
    "    data-ng-class=\"{'se-generic-editor-dropdown__paged':ySelect.requiresPaginatedStyling()}\">\n" +
    "    <ui-select-match class=\"se-generic-editor-dropdown__match se-generic-editor-multiple-dropdown__match\"\n" +
    "        placeholder=\"{{ySelect.placeholder || 'se.genericeditor.sedropdown.placeholder' | translate}}\">\n" +
    "        <item-printer id=\"{{ySelect.id}}-selected\"\n" +
    "            data-model=\"$item\"\n" +
    "            data-template-url=\"ySelect.itemTemplate\"\n" +
    "            class=\"\"\n" +
    "            data-ng-class=\"{'se-generic-editor-dropdown__item-printer':ySelect.hasControls()}\"> </item-printer>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{ySelect.id}}-list\"\n" +
    "        \"<%= filtering %>\"\n" +
    "        position=\"down\">\n" +
    "        <div data-ng-include=\"ySelect.itemTemplate\"></div>\n" +
    "    </ui-select-choices>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/select/ySelectExtensions/yActionableSearchItemTemplate.html',
    "<div class=\"se-actionable-search-item\"\n" +
    "    data-ng-if=\"$ctrl.getInputText()\">\n" +
    "\n" +
    "    <div class=\"se-actionable-search-item__name\">{{ $ctrl.getInputText() }}</div>\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn se-actionable-search-item__create-btn pull-right\"\n" +
    "        data-ng-show=\"$ctrl.showForm()\"\n" +
    "        data-ng-click=\"$ctrl.buttonPressed()\">\n" +
    "        {{ $ctrl.getActionText() | translate }}\n" +
    "    </button>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/select/ySelectTemplate.html',
    "<ui-select id=\"{{ySelect.id}}-selector\"\n" +
    "    data-ng-model=\"ySelect.model\"\n" +
    "    style=\"width:100%\"\n" +
    "    theme=\"<%= theme %>\"\n" +
    "    search-enabled=\"ySelect.searchEnabled\"\n" +
    "    data-ng-change=\"ySelect.internalOnChange()\"\n" +
    "    data-ng-class=\"{'se-generic-editor-dropdown__paged':ySelect.requiresPaginatedStyling()}\"\n" +
    "    class=\"se-generic-editor-dropdown\"\n" +
    "    ng-disabled=\"ySelect.isReadOnly\">\n" +
    "    <ui-select-match placeholder=\"{{ySelect.placeholder || 'se.genericeditor.sedropdown.placeholder' | translate}}\"\n" +
    "        class=\"se-generic-editor-dropdown__match2\">\n" +
    "        <span data-ng-if=\"ySelect.hasControls()\"\n" +
    "            class=\"input-group-addon glyphicon glyphicon-search ySESearchIcon__navigation\"></span>\n" +
    "        <item-printer id=\"{{ySelect.id}}-selected\"\n" +
    "            data-model=\"$select.selected\"\n" +
    "            data-template-url=\"ySelect.itemTemplate\"\n" +
    "            class=\"y-select-item-printer\"\n" +
    "            data-ng-class=\"{'se-generic-editor-dropdown__item-printer':ySelect.hasControls()}\"> </item-printer>\n" +
    "        <span data-ng-if=\"ySelect.hasControls()\"\n" +
    "            class=\"input-group-addon glyphicon glyphicon-remove-sign ySESearchIcon__navigation\"\n" +
    "            data-ng-click=\"ySelect.clear($select, $event)\"></span>\n" +
    "    </ui-select-match>\n" +
    "    <ui-select-choices id=\"{{ySelect.id}}-list\"\n" +
    "        \"<%= filtering %>\"\n" +
    "        position=\"down\"\n" +
    "        ui-disable-choice=\"ySelect.disableChoice(item)\">\n" +
    "        <div data-ng-include=\"ySelect.itemTemplate\"></div>\n" +
    "    </ui-select-choices>\n" +
    "    <ui-select-no-choice data-ng-if=\"ySelect.noResultLabel\"\n" +
    "        class=\"y-select-no-result-label\">\n" +
    "        {{ySelect.noResultLabel | translate}}\n" +
    "    </ui-select-no-choice>\n" +
    "</ui-select>"
  );


  $templateCache.put('web/common/services/sliderPanel/sliderPanelTemplate.html',
    "<div class=\"sliderpanel-container\"\n" +
    "    ng-class=\"::{'sliderpanel--noGreyedOutOverlay': $sliderPanelCtrl.sliderPanelConfiguration.noGreyedOutOverlay }\"\n" +
    "    ng-style=\"{'height': $sliderPanelCtrl.inlineStyling.container.height, 'width': $sliderPanelCtrl.inlineStyling.container.width,  'left': $sliderPanelCtrl.inlineStyling.container.left, 'top': $sliderPanelCtrl.inlineStyling.container.top, 'z-index': $sliderPanelCtrl.inlineStyling.container.zIndex }\"\n" +
    "    ng-if=\"$sliderPanelCtrl.isShown\">\n" +
    "\n" +
    "    <div class=\"sliderpanel-content {{ ::$sliderPanelCtrl.slideClassName }}\"\n" +
    "        ng-style=\"{{ ::$sliderPanelCtrl.inlineStyling.content }}\">\n" +
    "\n" +
    "        <div class=\"sliderpanel-header\"\n" +
    "            ng-show=\"{{ ::$sliderPanelCtrl.sliderPanelConfiguration.modal }}\">\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn-sliderpanel__close\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.showDismissButton\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelDismissAction()\">\n" +
    "                <span class=\"hyicon hyicon-close\"></span>\n" +
    "            </button>\n" +
    "\n" +
    "            <h4 class=\"h4__sliderpanel\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.title\">{{ $sliderPanelCtrl.sliderPanelConfiguration.modal.title | translate }}</h4>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"sliderpanel-body\"\n" +
    "            ng-transclude></div>\n" +
    "\n" +
    "        <div class=\"sliderpanel-footer\"\n" +
    "            data-ng-if=\"($sliderPanelCtrl.sliderPanelConfiguration.modal.cancel || $sliderPanelCtrl.sliderPanelConfiguration.modal.save)\">\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn btn-lg btn-subordinate sliderpanel-footer__button\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.cancel\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.cancel.onClick()\">\n" +
    "                {{ $sliderPanelCtrl.sliderPanelConfiguration.modal.cancel.label | translate }}\n" +
    "            </button>\n" +
    "\n" +
    "            <button type=\"button\"\n" +
    "                class=\"btn btn-lg btn-default sliderpanel-footer__button\"\n" +
    "                data-ng-if=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.save\"\n" +
    "                data-ng-click=\"$sliderPanelCtrl.sliderPanelConfiguration.modal.save.onClick()\"\n" +
    "                data-ng-disabled=\"$sliderPanelCtrl.isSaveDisabled()\">\n" +
    "                {{ $sliderPanelCtrl.sliderPanelConfiguration.modal.save.label | translate }}\n" +
    "            </button>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/tabset/yTabsetTemplate.html',
    "<div>\n" +
    "    <ul class=\"nav nav-tabs  multi-tabs\">\n" +
    "        <li ng-if=\"tabsList.length!=numTabsDisplayed\"\n" +
    "            data-ng-repeat=\"tab in tabsList.slice( 0, numTabsDisplayed-1 ) track by $index\"\n" +
    "            data-ng-class=\"{'active': tab.active }\"\n" +
    "            data-tab-id=\"{{tab.id}}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"!tab.message\">{{tab.title | translate}}</a>\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"tab.message\"\n" +
    "                data-popover=\"{{tab.message}}\"\n" +
    "                data-popover-trigger=\"mouseenter\">{{tab.title | translate}}</a>\n" +
    "        </li>\n" +
    "        <li ng-if=\"tabsList.length==numTabsDisplayed\"\n" +
    "            data-ng-repeat=\"tab in tabsList.slice( 0, numTabsDisplayed ) track by $index\"\n" +
    "            data-ng-class=\"{'active': tab.active }\"\n" +
    "            data-tab-id=\"{{tab.id}}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"!tab.message\">{{tab.title | translate}}</a>\n" +
    "            <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                data-ng-click=\"selectTab(tab)\"\n" +
    "                data-ng-if=\"tab.message\"\n" +
    "                data-popover=\"{{tab.message}}\"\n" +
    "                data-popover-trigger=\"mouseenter\">{{tab.title | translate}}</a>\n" +
    "        </li>\n" +
    "        <li data-ng-if=\"tabsList.length > numTabsDisplayed\"\n" +
    "            class=\"more-tab\"\n" +
    "            data-ng-class=\"{'active': isActiveInMoreTab()}\">\n" +
    "            <a data-ng-class=\"{'sm-tab-error': dropDownHasErrors()}\"\n" +
    "                class=\"dropdown-toggle\"\n" +
    "                data-toggle=\"dropdown\">\n" +
    "                <span data-ng-if=\"!isActiveInMoreTab()\"\n" +
    "                    class=\"multi-tabs__more-span\"\n" +
    "                    data-translate=\"se.ytabset.tabs.more\"></span>\n" +
    "                <span data-ng-if=\"isActiveInMoreTab()\"\n" +
    "                    class=\"multi-tabs__more-span\">{{selectedTab.title | translate}}</span>\n" +
    "                <span class=\"caret\"></span>\n" +
    "            </a>\n" +
    "            <ul class=\"dropdown-menu\">\n" +
    "                <li ng-if=\"tabsList.length!=numTabsDisplayed\"\n" +
    "                    data-ng-repeat=\"tab in tabsList.slice( numTabsDisplayed-1)\"\n" +
    "                    data-tab-id=\"{{tab.id}}\">\n" +
    "                    <a data-ng-class=\"{'sm-tab-error': tab.hasErrors}\"\n" +
    "                        data-ng-click=\"selectTab(tab)\">{{tab.title | translate}}</a>\n" +
    "                </li>\n" +
    "            </ul>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "\n" +
    "    <div>\n" +
    "        <y-tab data-ng-repeat=\"tab in tabsList\"\n" +
    "            ng-show=\"tab.active\"\n" +
    "            data-tab-id=\"{{tab.id}}\"\n" +
    "            content=\"tab.templateUrl\"\n" +
    "            data-model=\"model\"\n" +
    "            tab-control=\"tabControl\">\n" +
    "        </y-tab>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/tree/treeNodeRendererTemplate.html',
    "<div ui-tree-handle\n" +
    "    class=\"tree-node tree-node-content ySETreeNodeDesktop clearfix\"\n" +
    "    data-ng-class=\"{ hovered: node.mouseHovered }\"\n" +
    "    data-ng-mouseenter=\"ctrl.onNodeMouseEnter(node)\"\n" +
    "    data-ng-mouseleave=\"ctrl.onNodeMouseLeave(node)\">\n" +
    "\n" +
    "    <div data-ng-show=\"!ctrl.showAsList\"\n" +
    "        class=\"col-xs-1 text-center\">\n" +
    "        <a class=\"ySETreeNodeDesktop__expander btn btn-link btn-sm\"\n" +
    "            data-ng-if=\"ctrl.hasChildren(this)\"\n" +
    "            data-ng-disabled=\"ctrl.isDisabled\"\n" +
    "            data-ng-click=\"ctrl.toggleAndfetch(this)\">\n" +
    "            <span class=\"glyphicon\"\n" +
    "                data-ng-class=\"{'glyphicon-chevron-down': collapsed,'glyphicon-chevron-up': !collapsed}\"></span>\n" +
    "        </a>\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-show=\"ctrl.displayDefaultTemplate()\"\n" +
    "        class=\"ySETreeNodeDesktop__node-name col-xs-5\">\n" +
    "        <span>{{node.name | l10n}}</span>\n" +
    "        <h6 data-ng-show=\"node.title\">{{node.title | l10n}}\n" +
    "        </h6>\n" +
    "    </div>\n" +
    "    <div data-ng-include=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-ng-if=\"ctrl.nodeTemplateUrl\"\n" +
    "        data-include-replace></div>\n" +
    "\n" +
    "</div>\n" +
    "<ol data-ui-tree-nodes=\"\"\n" +
    "    data-ng-model=\"node.nodes\"\n" +
    "    data-ng-class=\"{hidden: collapsed}\"\n" +
    "    class=\"ySETreeNodeDesktop__ol\">\n" +
    "    <li data-ng-repeat=\"node in node.nodes\"\n" +
    "        data-ui-tree-node\n" +
    "        data-ng-include=\"'treeNodeRendererTemplate.html'\">\n" +
    "    </li>\n" +
    "</ol>"
  );


  $templateCache.put('web/common/services/tree/treeTemplate.html',
    "<div data-ui-tree=\"ctrl.treeOptions.callbacks\"\n" +
    "    data-drag-enabled=\"ctrl.treeOptions.dragEnabled\"\n" +
    "    data-drag-delay=\"ctrl.treeOptions.dragDelay\"\n" +
    "    class=\"categoryTable ySENavigationTree\">\n" +
    "\n" +
    "    <div class=\"ySENavigationTree-body\">\n" +
    "        <div class=\"mobileLayout clearfix visible-xs \">\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"desktopLayout clearfix visible-sm visible-md visible-lg \">\n" +
    "\n" +
    "            <ol data-ui-tree-nodes=\"\"\n" +
    "                data-ng-model=\"ctrl.root.nodes\"\n" +
    "                class=\"ySENavigationTree-body__nodes\">\n" +
    "\n" +
    "                <li data-ng-if=\"ctrl.root.nodes.length === 0\"\n" +
    "                    class=\"ySENavigationTree__nodata\"\n" +
    "                    data-translate=\"se.ytree.no.nodes.to.display\"></li>\n" +
    "\n" +
    "                <li class=\"ySENavigationTree-body__nodes__item\"\n" +
    "                    data-ng-repeat=\"node in ctrl.root.nodes\"\n" +
    "                    data-ui-tree-node\n" +
    "                    data-ng-include=\"'treeNodeRendererTemplate.html'\"></li>\n" +
    "            </ol>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/wizard/modalWizardNavBarTemplate.html',
    "<div class=\"modal-wizard-template\">\n" +
    "    <div class=\"modal-wizard-template__steps\">\n" +
    "        <div data-ng-repeat=\"action in wizardController._wizardContext.navActions track by action.id\"\n" +
    "            class=\"modal-wizard-template-step\">\n" +
    "            <button id=\"{{ action.id }}\"\n" +
    "                data-ng-class=\"{ 'btn modal-wizard-template-step__action': true, 'modal-wizard-template-step__action__enabled': action.enableIfCondition(), 'modal-wizard-template-step__action__disabled': !action.enableIfCondition(), 'modal-wizard-template-step__action__current': action.isCurrentStep() }\"\n" +
    "                data-ng-click=\"wizardController.executeAction(action)\"\n" +
    "                data-ng-disabled=\"!action.enableIfCondition()\">{{ action.i18n | translate }}\n" +
    "            </button>\n" +
    "            <span data-ng-if=\"!$last\"\n" +
    "                class=\"\"\n" +
    "                data-ng-class=\"{ 'modal-wizard-template-step__glyph-enabled':  action.enableIfCondition(), 'modal-wizard-template-step__glyph-disabled': !action.enableIfCondition()}\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div data-ng-repeat=\"step in wizardController._wizardContext._steps track by step.id\">\n" +
    "        <div data-ng-show=\"step.templateUrl === wizardController._wizardContext.templateUrl\"\n" +
    "            class=\"modal-wizard-template-content\"\n" +
    "            data-ng-include=\"step.templateUrl\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/common/services/wizard/modalWizardTemplate.html',
    "<div>\n" +
    "    <div id=\"yModalWizard\"\n" +
    "        data-ng-include=\"wizardController._wizardContext.templateOverride || wizardController._wizardContext.templateUrl\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/landingPage/landingPage.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"se-landing-page\">\n" +
    "    <span class=\"se-landing-page-title\"\n" +
    "        data-translate='se.landingpage.title' />\n" +
    "\n" +
    "    <div class=\"se-landing-page-site-selection\"\n" +
    "        data-ng-if=\"landingCtl.model != undefined\">\n" +
    "        <se-dropdown data-field=\"landingCtl.field\"\n" +
    "            data-qualifier=\"landingCtl.qualifier\"\n" +
    "            data-model=\"landingCtl.model\"\n" +
    "            data-id=\"landingCtl.sites_id\"></se-dropdown>\n" +
    "    </div>\n" +
    "\n" +
    "    <span class=\"se-landing-page-label\"\n" +
    "        data-translate='se.landingpage.label' />\n" +
    "\n" +
    "    <div class=\"se-landing-page-catalogs\">\n" +
    "        <div class=\"se-landing-page-catalog\"\n" +
    "            data-ng-repeat=\"catalog in landingCtl.catalogs\">\n" +
    "            <catalog-details data-catalog=\"catalog\"\n" +
    "                data-is-catalog-for-current-site=\"$last\"></catalog-details>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/hotkeys/perspectiveSelectorHotkey/perspectiveSelectorHotkeyNotificationTemplate.html',
    "<y-hotkey-notification data-hotkey-names=\"['esc']\"\n" +
    "    data-title=\"'se.cms.application.status.hotkey.title' | translate\"\n" +
    "    data-message=\"'se.cms.application.status.hotkey.message' | translate\">\n" +
    "</y-hotkey-notification>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/hotkeys/yHotkeyNotificationTemplate.html',
    "<div class=\"y-notification__hotkey\">\n" +
    "    <div data-ng-repeat=\"key in $ctrl.hotkeyNames\"\n" +
    "        data-ng-if=\"$ctrl.hotkeyNames\">\n" +
    "        <div class=\"y-notification__hotkey--key\">\n" +
    "            <span>{{key}}</span>\n" +
    "        </div>\n" +
    "        <span class=\"y-notification__hotkey__icon--add\"\n" +
    "            data-ng-if=\"!$last\">+</span>\n" +
    "    </div>\n" +
    "    <div class=\"y-notification__hotkey--text\">\n" +
    "        <div class=\"y-notification__hotkey--title\">{{$ctrl.title}}</div>\n" +
    "        <div class=\"y-notification__hotkey--message\">{{$ctrl.message}}</div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/yNotificationPanelTemplate.html',
    "<div class=\"y-notification-panel\"\n" +
    "    data-ng-if=\"$ctrl.getNotifications().length > 0\"\n" +
    "    data-ng-hide=\"$ctrl.isMouseOver\"\n" +
    "    data-ng-mouseenter=\"$ctrl.onMouseEnter()\">\n" +
    "    <y-notification data-ng-repeat=\"notification in $ctrl.getNotifications()\"\n" +
    "        data-notification=\"notification\">\n" +
    "    </y-notification>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/notifications/yNotificationTemplate.html',
    "<div class=\"y-notification\"\n" +
    "    id=\"{{ $ctrl.id }}\"\n" +
    "    data-ng-if=\"$ctrl.notification\">\n" +
    "    <div data-ng-if=\"$ctrl.hasTemplate()\"\n" +
    "        data-ng-bind-html=\"$ctrl.notification.template\"></div>\n" +
    "    <div data-ng-if=\"$ctrl.hasTemplateUrl()\"\n" +
    "        data-ng-include=\"$ctrl.notification.templateUrl\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/components/systemAlerts/systemAlertsTemplate.html',
    "<!--<div class=\"row-fluid alert-overlay\">-->\n" +
    "<div class=\"alert-overlay\"\n" +
    "    data-ng-if=\"$ctrl.getAlerts().length > 0\">\n" +
    "    <div uib-alert\n" +
    "        id=\"{{ 'system-alert-' + $index }}\"\n" +
    "        data-ng-repeat=\"alert in $ctrl.getAlerts()\"\n" +
    "        data-ng-class=\"['alert-' + (alert.type || 'info'), {'alert-item--closeable': alert.closeable}]\"\n" +
    "        class=\"alert-item\"\n" +
    "        data-close=\"alert.hide()\">\n" +
    "        <div class=\"alert-item--icon\">\n" +
    "            <span class=\"alert-item--icon-span hyicon\"\n" +
    "                data-ng-class=\"$ctrl.getIconType(alert.type)\"></span>\n" +
    "        </div>\n" +
    "        <div class=\"alert-item--content\">\n" +
    "            <span id=\"{{ 'close-alert-' + $index }}\"\n" +
    "                data-ng-if=\"alert.message\">\n" +
    "                {{ alert.message | translate:alert.messagePlaceholders }}\n" +
    "            </span>\n" +
    "            <div data-ng-if=\"alert.controller\"\n" +
    "                data-ng-controller=\"alert.controller as $alertInjectedCtrl\">\n" +
    "                <div data-ng-if=\"alert.template\"\n" +
    "                    data-compile-html=\"alert.template\"></div>\n" +
    "                <div data-ng-if=\"alert.templateUrl\"\n" +
    "                    data-ng-include=\"alert.templateUrl\"></div>\n" +
    "            </div>\n" +
    "            <div data-ng-if=\"!alert.controller\">\n" +
    "                <div data-ng-if=\"alert.template\"\n" +
    "                    data-compile-html=\"alert.template\"></div>\n" +
    "                <div data-ng-if=\"alert.templateUrl\"\n" +
    "                    data-ng-include=\"alert.templateUrl\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/experienceSelectorButton/experienceSelectorButtonTemplate.html',
    "<div class=\"ySEpreview\"\n" +
    "    data-uib-dropdown\n" +
    "    data-is-open=\"status.isopen\"\n" +
    "    data-auto-close=\"disabled\">\n" +
    "    <div class=\"btn yWebsiteSelectBtn\"\n" +
    "        type=\"button\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        id=\"experience-selector-btn\"\n" +
    "        data-ng-click=\"resetExperienceSelector()\">\n" +
    "        <span class=\"yWebsiteSelectBtn--info\">\n" +
    "            <span class=\"yWebsiteSelectBtn--label\"\n" +
    "                title=\"{{$ctrl.buildExperienceText()}}\"\n" +
    "                data-translate=\"se.experience.selector.previewing\"></span>\n" +
    "            <span class=\"yWebsiteSelectBtn--text-wrapper\">\n" +
    "                <span class=\"yWebsiteSelectBtn--globe\"\n" +
    "                    data-ng-if=\"$ctrl.iscurrentPageFromParent\"\n" +
    "                    data-y-popover\n" +
    "                    data-trigger=\"'hover'\"\n" +
    "                    data-placement=\"'bottom'\"\n" +
    "                    data-template=\"$ctrl.parentCatalogVersion\">\n" +
    "                    <span class=\"hyicon hyicon-globe yWebsiteSelectBtn--globe--icon\"></span>\n" +
    "                </span>\n" +
    "                <span title=\"{{$ctrl.buildExperienceText()}}\"\n" +
    "                    class=\"yWebsiteSelectBtn--text se-nowrap-ellipsis\">{{$ctrl.buildExperienceText()}}</span>\n" +
    "                <span class=\"yWebsiteSelectBtn--arrow hyicon hyicon-arrow \"></span>\n" +
    "            </span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"dropdown-menu bottom btn-block yDdResolution \"\n" +
    "        role=\"menu\">\n" +
    "        <experience-selector data-experience=\"$ctrl.experience\"\n" +
    "            data-dropdown-status=\"status\"\n" +
    "            data-reset-experience-selector=\"resetExperienceSelector\"></experience-selector>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/inflectionPointSelectorWidget/inflectionPointSelectorWidgetTemplate.html',
    "<div data-uib-dropdown\n" +
    "    is-open=\"status.isopen\"\n" +
    "    auto-close=\"disabled\"\n" +
    "    id=\"inflectionPtDropdown\">\n" +
    "    <button type=\"button\"\n" +
    "        class=\"btn btn-default yDddlbResolution\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        ng-disabled=\"disabled \"\n" +
    "        aria-pressed=\"false\">\n" +
    "        <img data-ng-src=\"{{imageRoot}}{{currentPointSelected.icon}}\"\n" +
    "            data-ng-typeSelected=\"{{currentPointSelected.type}}\" />\n" +
    "    </button>\n" +
    "    <ul class=\"dropdown-menu bottom btn-block yDdResolution \"\n" +
    "        role=\"menu\">\n" +
    "        <li ng-repeat=\"choice in points\"\n" +
    "            class=\"item text-center\">\n" +
    "            <a href\n" +
    "                data-ng-click=\"selectPoint(choice);\"\n" +
    "                data-ng-class=\"{'inflection-point-dropdown--option': currentPointSelected.type !== choice.type, 'inflection-point-dropdown--selected':currentPointSelected.type === choice.type}\">\n" +
    "                <img data-ng-show=\"currentPointSelected.type !== choice.type\"\n" +
    "                    data-ng-src=\"{{imageRoot}}{{choice.icon}}\"\n" +
    "                    class=\"file\" />\n" +
    "                <img data-ng-show=\"currentPointSelected.type === choice.type\"\n" +
    "                    data-ng-src=\"{{imageRoot}}{{choice.selectedIcon}}\"\n" +
    "                    class=\"file\" />\n" +
    "            </a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/perspectiveSelectorWidget/perspectiveSelectorHotkeyTooltipTemplate.html',
    "<div class=\"ySEPerspectiveSelector__hotkey-tooltip\">\n" +
    "    <span data-translate=\"se.hotkey.tooltip\"\n" +
    "        class=\"ySEPerspectiveSelector__hotkey-tooltip--info\"></span>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/perspectiveSelectorWidget/perspectiveSelectorWidgetTemplate.html',
    "<div data-uib-dropdown\n" +
    "    is-open=\"$ctrl.isOpen\"\n" +
    "    auto-close=\"disabled\"\n" +
    "    class=\"ySEPerspectiveSelector\">\n" +
    "\n" +
    "    <a type=\"button\"\n" +
    "        class=\"btn btn-default yHybridAction\"\n" +
    "        data-uib-dropdown-toggle\n" +
    "        ng-disabled=\"disabled \"\n" +
    "        aria-pressed=\"false\">\n" +
    "\n" +
    "        <span id=\"hotkeyTooltip\"\n" +
    "            data-ng-if=\"$ctrl.isHotkeyTooltipVisible() && !$ctrl.isOpen\"\n" +
    "            data-y-popover\n" +
    "            data-placement=\"'bottom'\"\n" +
    "            data-template-url=\"'perspectiveSelectorHotkeyTooltipTemplate.html'\"\n" +
    "            data-trigger=\"'hover'\">\n" +
    "            <span class=\"hyicon hyicon-info-icon ySEPerspectiveSelector__hotkey-tooltip--icon\"></span>\n" +
    "        </span>\n" +
    "        <span class=\"ySEPerspectiveSelector__btn-text\">{{$ctrl.getActivePerspectiveName() | translate}}</span>\n" +
    "        <span class=\"hyicon hyicon-arrow\"></span>\n" +
    "    </a>\n" +
    "    <ul class=\"dropdown-menu bottom btn-block ySEPerspectiveList\"\n" +
    "        role=\"menu\">\n" +
    "        <li ng-repeat=\"choice in $ctrl.getDisplayedPerspectives()\"\n" +
    "            class=\"item ySEPerspectiveList--item\"\n" +
    "            data-ng-click=\"$ctrl.selectPerspective(choice.key)\">\n" +
    "            <a href\n" +
    "                data-ng-click=\"$ctrl.selectPerspective(choice.key);\">{{choice.name | translate}}</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/deviceSupportTemplate.html',
    "<inflection-point-selector data-ng-if=\"isOnStorefront()\"></inflection-point-selector>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/experienceSelectorWrapperTemplate.html',
    "<experience-selector-button data-ng-if=\"isOnStorefront()\"></experience-selector-button>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/leftToolbarTemplate.html',
    "<div class=\"se-left-menu\">\n" +
    "    <nav class=\"se-left-nav\"\n" +
    "        ng-show=\"showLeftMenu\">\n" +
    "\n" +
    "        <ul class=\"se-left-nav__section se-left-nav__section--top\">\n" +
    "            <li class=\"se-left-nav__section__close\">\n" +
    "                <a href=\"#\"\n" +
    "                    class=\"se-left-nav__section__close--link\"\n" +
    "                    data-ng-click=\"closeLeftToolbar($event)\">\n" +
    "                    <span class=\"hyicon hyicon-close > se-left-nav__section__close--icon\"></span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <p class=\"se-user-name\">{{username | uppercase}}</p>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <a class=\"se-sign-out__link\"\n" +
    "                    data-ng-click=\"signOut($event)\">{{'se.left.toolbar.sign.out' | translate}}</a>\n" +
    "            </li>\n" +
    "\n" +
    "            <li class=\"se-left-nav__section--top__item\">\n" +
    "                <language-selector class=\"se-left-nav__section--top__language\"></language-selector>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "\n" +
    "        <ul data-ng-show=\"!showLevel2\"\n" +
    "            class=\"se-left-nav__section se-left-nav__section--level1\"\n" +
    "            id=\"hamburger-menu-level1\">\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <a class=\"se-left-nav__link\"\n" +
    "                    data-ng-click=\"showSites()\">{{'se.left.toolbar.sites' | translate}}</a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section__list-item\"\n" +
    "                has-operation-permission=\"configurationCenterReadPermissionKey\">\n" +
    "                <a id=\"configurationCenter\"\n" +
    "                    class=\"se-left-nav__link\"\n" +
    "                    data-ng-click=\"showCfgCenter($event)\">{{'se.left.toolbar.configuration.center' | translate}}\n" +
    "                    <span class=\"hyicon hyicon-chevron\"></span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "        <ul data-ng-show=\"showLevel2\"\n" +
    "            class=\"se-left-nav__section se-left-nav__section--level2\"\n" +
    "            id=\"hamburger-menu-level2\">\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <a data-ng-click=\"goBack()\"\n" +
    "                    class=\"se-left-nav__link\">\n" +
    "                    <span class=\"hyicon hyicon-back\"></span>\n" +
    "                    <span class=\"\">{{'se.left.toolbar.back' | translate}}</span>\n" +
    "                </a>\n" +
    "            </li>\n" +
    "            <li class=\"se-left-nav__section__list-item\">\n" +
    "                <general-configuration class=\"se-left-nav__link\" />\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </nav>\n" +
    "\n" +
    "    <div class=\"navbar-header se-menu-button-wrapper\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"navbar-toggle se-menu-button__toggle\"\n" +
    "            id=\"nav-expander\"\n" +
    "            data-ng-click=\"showToolbar($event) \">\n" +
    "            <span class=\"sr-only \">{{'se.left.toolbar.toggle.navigation' | translate}}</span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "            <span class=\"icon-bar se-menu-button__icon-bar\"></span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/leftToolbarWrapperTemplate.html',
    "<left-toolbar data-image-root=\"imageRoot\"></left-toolbar>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/logoTemplate.html',
    "<div class=\"ySmartEditAppLogo \"\n" +
    "    alt=\"Smart Edit \" />\n" +
    "\n" +
    "<svg xmlns=\"http://www.w3.org/2000/svg\"\n" +
    "    width=\"142\"\n" +
    "    height=\"25\"\n" +
    "    viewBox=\"0 0 142 25\">\n" +
    "    <defs>\n" +
    "        <style>\n" +
    "        .cls-1 {\n" +
    "            fill: #fff;\n" +
    "            fill-rule: evenodd;\n" +
    "        }\n" +
    "        </style>\n" +
    "    </defs>\n" +
    "    <path id=\"LOGO\"\n" +
    "        class=\"cls-1\"\n" +
    "        d=\"M115.056,33.546a30.4,30.4,0,0,1,1.458-9.4l3.809-.185V21l-5.972.278-0.376.88A31.674,31.674,0,0,0,112,33.546a31.674,31.674,0,0,0,1.975,11.389l0.376,0.926L120.275,46V43.037l-3.808-.093A30.782,30.782,0,0,1,115.056,33.546Zm9.968,2.268c0.189-.37.377-0.787,0.565-1.2a58.774,58.774,0,0,0,2.351-6.065l-3.2-.093c-0.329.88-.846,2.315-1.457,3.75-0.612-1.481-1.082-2.87-1.364-3.75l-3.15.139a52.838,52.838,0,0,0,2.257,6.065c0.8,1.713,1.787,3.565,2.445,4.768l3.432-.046C126.952,39.333,126.012,37.713,125.024,35.815Zm7.994-13.657-0.329-.88L126.717,21v2.963l3.809,0.185a30.964,30.964,0,0,1,1.457,9.4,32.976,32.976,0,0,1-1.41,9.4l-3.809.093V46l5.925-.139,0.376-.926a31.674,31.674,0,0,0,1.975-11.389A32.286,32.286,0,0,0,133.018,22.157Zm20.406,3.518a9.73,9.73,0,0,1,5.831,1.713l-1.646,2.454a7.951,7.951,0,0,0-4.09-1.3,2.607,2.607,0,0,0-1.74.556,1.657,1.657,0,0,0-.659,1.389,1.222,1.222,0,0,0,.424,1.019,5.041,5.041,0,0,0,1.5.694l2.116,0.556c2.962,0.787,4.42,2.454,4.42,5.046a5.1,5.1,0,0,1-1.975,4.167,8.087,8.087,0,0,1-5.266,1.574,12.179,12.179,0,0,1-5.971-1.528l1.316-2.685a10.646,10.646,0,0,0,4.8,1.389c2.069,0,3.056-.787,3.056-2.315,0-1.065-.752-1.806-2.3-2.222l-1.927-.509a5.385,5.385,0,0,1-3.433-2.176,5.048,5.048,0,0,1-.752-2.593,4.714,4.714,0,0,1,1.74-3.8A7.327,7.327,0,0,1,153.424,25.676Zm9.31,8.8a7.068,7.068,0,0,0-.47-3.194l3.056-.787a4.667,4.667,0,0,1,.471,1.25,4.522,4.522,0,0,1,3.15-1.25,3.609,3.609,0,0,1,2.633,1.019c0.141,0.139.282,0.324,0.517,0.6a5.023,5.023,0,0,1,3.668-1.62,3.831,3.831,0,0,1,2.68.926,3.878,3.878,0,0,1,.846,2.917v8.889h-3.291V34.935a5.086,5.086,0,0,0-.094-1.2,0.906,0.906,0,0,0-.94-0.556,3.6,3.6,0,0,0-2.257,1.065v8.982h-3.2V35.074a4.238,4.238,0,0,0-.141-1.343,0.968,0.968,0,0,0-1.034-.6,3.365,3.365,0,0,0-2.21.972v9.12h-3.292v-8.75h-0.094Zm21.018,0.046L182.3,32.157a17,17,0,0,1,2.069-1.019,9.107,9.107,0,0,1,3.526-.741c2.257,0,3.621.741,4.138,2.176a6.76,6.76,0,0,1,.235,2.268l-0.094,4.213v0.231a4.054,4.054,0,0,0,.188,1.482,3.5,3.5,0,0,0,.987,1.111l-1.786,1.991a3.572,3.572,0,0,1-1.834-1.528,3.737,3.737,0,0,1-.8.648,4.367,4.367,0,0,1-2.586.695,4.736,4.736,0,0,1-3.291-1.019,3.706,3.706,0,0,1-1.129-2.917q0-4.306,6.207-4.306a3.855,3.855,0,0,1,.752.046V34.982A2.728,2.728,0,0,0,188.6,33.5a1.426,1.426,0,0,0-1.222-.37A7.49,7.49,0,0,0,183.752,34.519Zm5.172,5.926,0.047-2.685H188.83a4.983,4.983,0,0,0-2.445.417,1.6,1.6,0,0,0-.658,1.528,1.715,1.715,0,0,0,.423,1.2,1.31,1.31,0,0,0,1.082.463A3.108,3.108,0,0,0,188.924,40.444Zm7.147-5.6a10.3,10.3,0,0,0-.47-3.657l3.01-.787a4.29,4.29,0,0,1,.517,1.806,4.816,4.816,0,0,1,1.928-1.574,2.6,2.6,0,0,1,1.128-.185,3.325,3.325,0,0,1,1.27.278l-0.941,2.87a1.932,1.932,0,0,0-.987-0.231,2.764,2.764,0,0,0-2.069,1.065v8.8h-3.386v-8.38Zm12.32-4.167H211.4l-0.846,2.176h-2.163v6.389a3.213,3.213,0,0,0,.282,1.62,1.322,1.322,0,0,0,1.175.417,6.572,6.572,0,0,0,1.223-.231l0.423,1.944a7.361,7.361,0,0,1-2.727.556,4.706,4.706,0,0,1-2.21-.509,2.711,2.711,0,0,1-1.27-1.343,6.39,6.39,0,0,1-.235-1.944v-6.9h-1.269v-2.13h1.269a21.49,21.49,0,0,1,.141-2.963l3.433-.833A32.137,32.137,0,0,0,208.391,30.676Zm4.8,12.546V26.232h9.968l-0.47,2.778h-5.972v3.935h5.031v2.778H216.76v4.537h6.63v2.963h-10.2Zm20.171-18.148,3.339,0.509V38.639a13.341,13.341,0,0,0,.47,4.583h-3.009a2.447,2.447,0,0,1-.189-0.741,4.458,4.458,0,0,1-2.962,1.065,4.817,4.817,0,0,1-3.9-1.713,6.852,6.852,0,0,1-1.458-4.63,7.114,7.114,0,0,1,1.552-4.768,5.046,5.046,0,0,1,4-1.852,3.674,3.674,0,0,1,2.3.694,15.107,15.107,0,0,1-.094-1.852V25.074h-0.047Zm0.094,14.907V34.009a2.678,2.678,0,0,0-1.833-.787c-1.458,0-2.163,1.343-2.163,4.028a5.461,5.461,0,0,0,.517,2.778,2.028,2.028,0,0,0,1.834.787A2.465,2.465,0,0,0,233.452,39.982Zm6.677-12.731a1.911,1.911,0,0,1,.611-1.435,2,2,0,0,1,1.5-.6,1.973,1.973,0,0,1,1.458.6,2.016,2.016,0,0,1,.611,1.435,1.911,1.911,0,0,1-.611,1.435,2,2,0,0,1-1.5.6,1.973,1.973,0,0,1-1.458-.6A1.911,1.911,0,0,1,240.129,27.25Zm0.376,15.972V30.907l3.386-.509V43.222h-3.386ZM250.9,30.676h3.009l-0.846,2.176H250.9v6.389a3.213,3.213,0,0,0,.282,1.62,1.322,1.322,0,0,0,1.175.417,6.572,6.572,0,0,0,1.223-.231L254,42.991a7.361,7.361,0,0,1-2.727.556,4.706,4.706,0,0,1-2.21-.509,2.711,2.711,0,0,1-1.27-1.343,6.39,6.39,0,0,1-.235-1.944v-6.9h-1.269v-2.13h1.269a21.49,21.49,0,0,1,.141-2.963l3.433-.833A32.137,32.137,0,0,0,250.9,30.676Z\"\n" +
    "        transform=\"translate(-112 -21)\" />\n" +
    "</svg>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/perspectiveSelectorWrapperTemplate.html',
    "<perspective-selector></perspective-selector>"
  );


  $templateCache.put('web/smarteditcontainer/core/services/topToolbars/toolbarActionTemplate.html',
    "<div>\n" +
    "    <div data-ng-if=\"item.type == 'ACTION'\"\n" +
    "        class=\"toolbar-action\">\n" +
    "        <button type=\"button\"\n" +
    "            class=\"btn btn-default toolbar-action--button\"\n" +
    "            data-ng-click=\"triggerAction(item, $event)\"\n" +
    "            aria-pressed=\"false\"\n" +
    "            aria-haspopup=\"true\"\n" +
    "            id=\"{{toolbarName}}_option_{{item.key}}_btn\"\n" +
    "            aria-expanded=\"false\">\n" +
    "            <span data-ng-if='item.iconClassName'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_iconclass\"\n" +
    "                data-ng-class=\"item.iconClassName\"></span>\n" +
    "            <img data-ng-if='!item.iconClassName && item.icons'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}\"\n" +
    "                data-ng-src=\"{{::imageRoot}}{{item.icons[0]}}\"\n" +
    "                class=\"file\"\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_icon\"\n" +
    "                title=\"{{item.name | translate}}\"\n" +
    "                alt=\"{{item.name | translate}}\" />\n" +
    "            <span class=\"toolbar-action--button--txt\"\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}_btn_lbl\">{{item.name | translate}}</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"item.type == 'HYBRID_ACTION'\"\n" +
    "        class=\"toolbar-action toolbar-action--hybrid\"\n" +
    "        data-uib-dropdown\n" +
    "        data-auto-close=\"outsideClick\"\n" +
    "        data-is-open=\"item.isOpen\"\n" +
    "        data-item-key=\"{{ item.key }}\">\n" +
    "        <button data-ng-show=\"item.iconClassName || item.icons\"\n" +
    "            type=\"button\"\n" +
    "            class=\"btn btn-default toolbar-action--button\"\n" +
    "            data-uib-dropdown-toggle\n" +
    "            ng-disabled=\"disabled \"\n" +
    "            aria-pressed=\"false \"\n" +
    "            data-ng-click=\"triggerAction(item, $event)\">\n" +
    "            <span data-ng-if='item.iconClassName'\n" +
    "                data-ng-class=\"item.iconClassName\"></span>\n" +
    "            <img data-ng-if='!item.iconClassName && item.icons'\n" +
    "                id=\"{{toolbarName}}_option_{{item.key}}\"\n" +
    "                data-ng-src=\"{{::imageRoot}}{{item.icons[0]}}\"\n" +
    "                class=\"file\"\n" +
    "                title=\"{{item.name | translate}}\"\n" +
    "                alt=\"{{item.name | translate}}\" />\n" +
    "            <span class=\"toolbar-action--button--txt\">{{item.name | translate}}</span>\n" +
    "        </button>\n" +
    "\n" +
    "        <div data-uib-dropdown-menu\n" +
    "            data-ng-class=\"{ 'dropdown-menu-left': item.section == 'left' || item.section == 'middle', 'dropdown-menu-right': item.section == 'right' }\"\n" +
    "            class=\"btn-block toolbar-action--include\">\n" +
    "            <div data-ng-if=\"getItemVisibility(item)\"\n" +
    "                data-ng-include=\"item.include\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/core/toolbar/toolbarTemplate.html',
    "<div class='{{cssClass}} yToolbar'>\n" +
    "\n" +
    "    <div class=\"yToolbar__Left\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'left'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__left {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "    <div class=\"yToolbar__Middle\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'middle'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__middle {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "    <div class=\"yToolbar__Right\">\n" +
    "        <div data-ng-repeat=\"item in actions | filter:{section:'right'}\"\n" +
    "            class=\"yTemplateToolbar yTemplateToolbar__right {{::item.className}}\"\n" +
    "            data-ng-include=\"item.include && item.type =='TEMPLATE' ? item.include : 'toolbarActionTemplate.html'\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/fragments/mainview.html',
    "<div class=\"ySmartEditToolbars\"\n" +
    "    style=\"position:absolute\">\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditTitleToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"smartEditTitleToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "    <div>\n" +
    "        <toolbar data-css-class=\"ySmartEditExperienceSelectorToolbar\"\n" +
    "            data-image-root=\"imageRoot\"\n" +
    "            data-toolbar-name=\"experienceSelectorToolbar\"></toolbar>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div id=\"js_iFrameWrapper\"\n" +
    "    class=\"iframeWrapper\">\n" +
    "    <iframe id=\"ySmartEditFrame\"\n" +
    "        src=\"\"\n" +
    "        hy-dropabpe-iframe></iframe>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/editConfigurationsTemplate.html',
    "<div id=\"editConfigurationsBody\"\n" +
    "    class=\"ySEConfigBody\">\n" +
    "    <form name=\"form.configurationForm\"\n" +
    "        novalidate\n" +
    "        data-ng-submit=\"editor.submit(form.configurationForm)\">\n" +
    "        <div class=\"row ySECfgTableHeader\">\n" +
    "            <div class=\"col-xs-6\">\n" +
    "                <label data-translate=\"se.configurationform.header.key.name\"></label>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-6\">\n" +
    "                <label data-translate=\"se.configurationform.header.value.name\"></label>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"row ySECfgAddEntity\">\n" +
    "            <button class=\"y-add-btn\"\n" +
    "                type=\"button\"\n" +
    "                data-ng-click=\"editor.addEntry(); \">\n" +
    "                <span class=\"hyicon hyicon-add \"></span>\n" +
    "                {{'se.general.configuration.add.button' | translate}}\n" +
    "            </button>\n" +
    "        </div>\n" +
    "        <div class=\"row ySECfgEntity \"\n" +
    "            data-ng-repeat=\"entry in editor.filterConfiguration() \"\n" +
    "            data-ng-mouseenter=\"mouseenter() \"\n" +
    "            data-ng-mouseout=\"mouseout() \">\n" +
    "            <div class=\"col-xs-6 \">\n" +
    "                <input type=\"text \"\n" +
    "                    class=\"ng-class:{ 'col-xs-12':true, 'has-error':entry.errors.keys.length>0}\"\n" +
    "                    name=\"{{entry.key}}_key\"\n" +
    "                    data-ng-model=\"entry.key\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-disabled=\"!entry.isNew\"\n" +
    "                    title=\"{{entry.key}}\" />\n" +
    "                <span id=\"{{entry.key}}_error_{{$index}}\"\n" +
    "                    data-ng-if=\"entry.errors.keys\"\n" +
    "                    data-ng-repeat=\"error in entry.errors.keys\"\n" +
    "                    class=\"error-input help-block\">\n" +
    "                    {{error.message|translate}}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-5\">\n" +
    "                <textarea class=\"ng-class:{'col-xs-12':true, 'has-error':entry.errors.values.length>0}\"\n" +
    "                    name=\"{{entry.key}}_value\"\n" +
    "                    data-ng-model=\"entry.value\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-change=\"editor._validateUserInput(entry)\"></textarea>\n" +
    "                <div data-ng-if=\"entry.requiresUserCheck\">\n" +
    "                    <input id=\"{{entry.key}}_absoluteUrl_check_{{$index}}\"\n" +
    "                        type=\"checkbox\"\n" +
    "                        data-ng-model=\"entry.isCheckedByUser\" />\n" +
    "                    <span id=\"{{entry.key}}_absoluteUrl_msg_{{$index}}\"\n" +
    "                        class=\"ng-class:{'warning-check-msg':true, 'not-checked':entry.hasErrors && !entry.isCheckedByUser}\">{{'se.configurationform.absoluteurl.check' | translate}}</span>\n" +
    "                </div>\n" +
    "\n" +
    "                <span id=\"{{entry.key}}_error_{{$index}}\"\n" +
    "                    data-ng-if=\"entry.errors.values\"\n" +
    "                    data-ng-repeat=\"error in entry.errors.values\"\n" +
    "                    class=\"error-input help-block\">\n" +
    "                    {{error.message|translate}}\n" +
    "                </span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-1\">\n" +
    "                <button type=\"button\"\n" +
    "                    id=\"{{entry.key}}_removeButton_{{$index}}\"\n" +
    "                    class=\"btn btn-subordinate\"\n" +
    "                    data-ng-click=\"editor.removeEntry(entry, form.configurationForm);\">\n" +
    "                    <span class=\"hyicon hyicon-remove\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/generalConfigurationTemplate.html',
    "<a id=\"generalConfiguration\"\n" +
    "    data-ng-click=\"editConfiguration()\">{{'se.general.configuration.title' | translate}}</a>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/multiProductCatalogVersionSelector/multiProductCatalogVersionSelectorTemplate.html',
    "<div class=\"se-multi-product-catalog-version-selector\">\n" +
    "\n" +
    "    <div class=\"form-group se-multi-product-catalog-version-selector__label\"> {{'se.product.catalogs.multiple.list.header' | translate}}</div>\n" +
    "\n" +
    "    <div class=\"se-multi-product-catalog-version-selector__catalog form-group\"\n" +
    "        data-ng-repeat=\"productCatalog in $ctrl.productCatalogs\">\n" +
    "\n" +
    "        <label class=\"control-label se-multi-product-catalog-version-selector__catalog-name\"\n" +
    "            id=\"{{productCatalog.catalogId}}-label\">\n" +
    "            {{productCatalog.name | l10n}}\n" +
    "        </label>\n" +
    "\n" +
    "        <div class=\"se-multi-product-catalog-version-selector__catalog-version\">\n" +
    "            <y-select data-id=\"{{productCatalog.catalogId}}\"\n" +
    "                data-ng-model=\"productCatalog.selectedItem\"\n" +
    "                data-on-change=\"$ctrl.updateModel\"\n" +
    "                data-fetch-strategy=\"productCatalog.fetchStrategy\" />\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/multiProductCatalogVersionsConfigurationsTemplate.html',
    "<multi-product-catalog-version-selector data-product-catalogs=\"modalController.productCatalogs\"\n" +
    "    data-selected-versions=\"modalController.selectedCatalogVersions\"\n" +
    "    data-on-selection-change=\"modalController.updateSelection($selectedVersions)\"></multi-product-catalog-version-selector>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/productCatalogVersionsSelectorTemplate.html',
    "<div data-ng-if=\"$ctrl.isReady\">\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.isSingleVersionSelector\">\n" +
    "        <y-select data-id=\"{{$ctrl.qualifier}}\"\n" +
    "            data-ng-model=\"$ctrl.model.productCatalogVersions[0]\"\n" +
    "            data-reset=\"$ctrl.reset\"\n" +
    "            data-fetch-strategy=\"$ctrl.fetchStrategy\" />\n" +
    "    </div>\n" +
    "\n" +
    "    <div data-ng-if=\"$ctrl.isMultiVersionSelector\"\n" +
    "        data-y-popover\n" +
    "        data-trigger=\"'hover'\"\n" +
    "        class=\"se-products-catalog-select-multiple__popover\"\n" +
    "        data-placement=\"'bottom'\"\n" +
    "        data-template=\"$ctrl.buildMultiProductCatalogVersionsTemplate()\"\n" +
    "        data-is-open=\"$ctrl.isTooltipOpen\">\n" +
    "        <div id=\"multi-product-catalog-versions-selector\"\n" +
    "            data-ng-click=\"$ctrl.onClick($ctrl.productCatalogs, $ctrl.model[$ctrl.qualifier])\"\n" +
    "            class=\"se-products-catalog-select-multiple\">\n" +
    "            <input type=\"text\"\n" +
    "                data-ng-model=\"$ctrl.model[$ctrl.qualifier]\"\n" +
    "                data-ng-value=\"$ctrl.getMultiProductCatalogVersionsSelectedOptions()\"\n" +
    "                class=\"form-control se-products-catalog-select-multiple__catalogs se-nowrap-ellipsis\"\n" +
    "                name=\"{{$ctrl.qualifier}}\"\n" +
    "                readonly/>\n" +
    "            <span class=\"hyicon hyicon-optionssm se-products-catalog-select-multiple__icon\">\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/modules/administrationModule/productCatalogVersionsSelector/productCatalogVersionsSelectorWrapperTemplate.html',
    "<se-product-catalog-versions-selector data-field=\"field\"\n" +
    "    data-qualifier=\"qualifier\"\n" +
    "    data-model=\"model\"\n" +
    "    data-id=\"id\" />"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogDetailsTemplate.html',
    "<div class=\"se-catalog-details\"\n" +
    "    data-ng-class=\"{'se-catalog-for-current-site': $ctrl.isCatalogForCurrentSite}\">\n" +
    "    <y-collapsible-container data-configuration=\"$ctrl.collapsibleConfiguration\"\n" +
    "        class=\"se-catalog-details__collapse\">\n" +
    "        <header class=\"se-catalog-details__header\">\n" +
    "            {{$ctrl.catalog.name | l10n}}\n" +
    "        </header>\n" +
    "        <content class=\"se-catalog-details__content\">\n" +
    "            <!-- Left Side -->\n" +
    "            <div class=\"se-catalog-details__panel se-catalog-details__panel--left\">\n" +
    "                <catalog-versions-thumbnail-carousel data-catalog=\"$ctrl.catalog\"\n" +
    "                    data-site-id=\"$ctrl.siteIdForCatalog\" />\n" +
    "            </div>\n" +
    "            <!-- Right Side -->\n" +
    "            <div class=\"se-catalog-details__panel se-catalog-details__panel--right\">\n" +
    "                <div data-ng-repeat=\"catalogVersion in $ctrl.sortedCatalogVersions\">\n" +
    "                    <catalog-version-details data-catalog=\"$ctrl.catalog\"\n" +
    "                        data-catalog-version=\"catalogVersion\"\n" +
    "                        data-active-catalog-version=\"$ctrl.activeCatalogVersion\"\n" +
    "                        data-site-id=\"$ctrl.siteIdForCatalog\" />\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </content>\n" +
    "    </y-collapsible-container>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"se-catalog-divider\"\n" +
    "    data-ng-if=\"!$ctrl.isCatalogForCurrentSite\">\n" +
    "    <img data-ng-src=\"{{::$ctrl.cataloDeviderImage}}\"\n" +
    "        alt=\"\" />\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogVersionDetailsTemplate.html',
    "<div class=\"se-catalog-version-container\">\n" +
    "    <div class=\"se-catalog-version-container__left\">\n" +
    "        <div class=\"se-catalog-version-container__name\">{{$ctrl.catalogVersion.version}}</div>\n" +
    "        <div class=\"se-catalog-version-container__left__templates\">\n" +
    "            <div class=\"se-catalog-version-container__left__template\"\n" +
    "                data-ng-repeat=\"item in $ctrl.leftItems\">\n" +
    "                <div data-ng-include=\"item.include\"></div>\n" +
    "                <div class=\"se-catalog-version-container__divider\"\n" +
    "                    data-ng-if=\"!$last\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"se-catalog-version-container__right\">\n" +
    "        <div class=\"se-catalog-version-container__right__template\"\n" +
    "            data-ng-repeat=\"item in $ctrl.rightItems\"\n" +
    "            ng-include=\"item.include\" />\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/catalogVersionsThumbnailCarouselTemplate.html',
    "<div class=\"se-active-catalog-thumbnail\">\n" +
    "    <div class=\"se-active-catalog-version-container__thumbnail\"\n" +
    "        data-ng-click=\"$ctrl.onClick()\">\n" +
    "        <div class=\"se-active-catalog-version-container__thumbnail__default-img\">\n" +
    "            <div class=\"se-active-catalog-version-container__thumbnail__img\"\n" +
    "                style=\"background-image: url('{{$ctrl.selectedVersion.thumbnailUrl}}');\">\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"se-active-catalog-version-container__name\">{{$ctrl.selectedVersion.version}}</div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/links/homePageLinkTemplate.html',
    "<div class=\"home-link-container\">\n" +
    "    <a class=\"home-link-item__link se-catalog-version__link\"\n" +
    "        data-ng-click=\"ctrl.onClick()\"\n" +
    "        data-translate=\"se.landingpage.homepage\"></a>\n" +
    "</div>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/catalogDetails/links/homePageLinkWrapperTemplate.html',
    "<home-page-link data-catalog=\"$ctrl.catalog\"\n" +
    "    data-catalog-version=\"$ctrl.catalogVersion\"\n" +
    "    data-site-id=\"$ctrl.siteId\"></home-page-link>"
  );


  $templateCache.put('web/smarteditcontainer/services/widgets/clientPagedList/clientPagedList.html',
    "<div class=\"fluid-container ySEPageListResult\">\n" +
    "\n" +
    "    <p class=\"paged-list-count\"\n" +
    "        ng-if=\"displayCount\">\n" +
    "        <span>({{ totalItems }} {{'se.cms.pagelist.countsearchresult' | translate}})</span>\n" +
    "    </p>\n" +
    "\n" +
    "    <table class=\"paged-list-table table table-striped table-hover techne-table\">\n" +
    "        <thead>\n" +
    "            <tr>\n" +
    "                <th data-ng-repeat=\"key in keys\"\n" +
    "                    data-ng-click=\"orderByColumn(key.property)\"\n" +
    "                    data-ng-style=\"{ 'width': columnWidth + '%' }\"\n" +
    "                    class=\"paged-list-header\"\n" +
    "                    data-ng-class=\"'paged-list-header-'+key.property\"\n" +
    "                    ng-if=\"key.i18n\">\n" +
    "                    {{ key.i18n | translate }}\n" +
    "                    <span class=\"header-icon\"\n" +
    "                        ng-show=\"visibleSortingHeader === key.property\"\n" +
    "                        ng-class=\"{ 'down': headersSortingState[key.property] === true, 'up': headersSortingState[key.property] === false }\"></span>\n" +
    "                </th>\n" +
    "\n" +
    "                <th class=\"paged-list-header\"></th>\n" +
    "                <th class=\"paged-list-header\"\n" +
    "                    data-ng-if=\"dropdownItems!==undefined\"></th>\n" +
    "            </tr>\n" +
    "        </thead>\n" +
    "        <tbody class=\"paged-list-table__body\">\n" +
    "            <tr data-ng-repeat=\" item in items | filterByField: query : getFilterKeys() : filterCallback | startFrom:(currentPage-1)*itemsPerPage | limitTo:itemsPerPage \"\n" +
    "                class=\"techne-table-xs-right techne-table-xs-left paged-list-item \">\n" +
    "                <td ng-repeat=\"key in keys\"\n" +
    "                    ng-class=\"'paged-list-item-'+key.property\">\n" +
    "                    <div data-ng-if=\"renderers[key.property]\"\n" +
    "                        compile-html=\"renderers[key.property](item, key)\"></div>\n" +
    "                    <span data-ng-if=\"!renderers[key.property]\">{{ item[key.property] }}</span>\n" +
    "                </td>\n" +
    "                <td>\n" +
    "                    <img data-ng-src=\"{{ item.visibilityIconSrc }}\"\n" +
    "                        tooltip-placement=\"bottom\"\n" +
    "                        tooltip=\"{{ 'se.cms.icon.tooltip.visibility' | translate: item.translationData }}\" />\n" +
    "                </td>\n" +
    "                <td data-ng-if=\"dropdownItems!==undefined\"\n" +
    "                    has-operation-permission=\"'se.edit.page'\"\n" +
    "                    class=\"paged-list-table__body__td paged-list-table__body__td-menu\">\n" +
    "                    <y-drop-down-menu dropdown-items=\"dropdownItems\"\n" +
    "                        selected-item=\"item\"\n" +
    "                        class=\"y-dropdown pull-right\" />\n" +
    "                </td>\n" +
    "            </tr>\n" +
    "        </tbody>\n" +
    "    </table>\n" +
    "\n" +
    "    <div class=\"pagination-container \">\n" +
    "        <ul data-uib-pagination\n" +
    "            boundary-links=\"true \"\n" +
    "            total-items=\"totalItems \"\n" +
    "            items-per-page=\"itemsPerPage \"\n" +
    "            ng-model=\"currentPage \"\n" +
    "            class=\"pagination-lg \"\n" +
    "            previous-text=\"&lsaquo; \"\n" +
    "            next-text=\"&rsaquo; \"\n" +
    "            first-text=\"&laquo; \"\n" +
    "            last-text=\"&raquo; \"></ul>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/core/decoratorFilter/managePerspectivesTemplate.html',
    "<div class=\"modal-header\">\n" +
    "    <h3 class=\"modal-title\">{{ 'se.modal.perpectives.title' | translate }}</h3>\n" +
    "</div>\n" +
    "<div class=\"modal-body\">\n" +
    "    <table>\n" +
    "        <tr>\n" +
    "            <td rowspan=\"2\">\n" +
    "                {{'se.modal.perpectives.header.perspectives.name' | translate}}\n" +
    "                <div>\n" +
    "                    <select size=\"12\"\n" +
    "                        data-ng-change=\"perspectiveChanged()\"\n" +
    "                        class=\"form-control\"\n" +
    "                        data-ng-options=\"p.name | translate for p in perspectives\"\n" +
    "                        data-ng-model=\"perspective\"></select>\n" +
    "                </div>\n" +
    "                <button type=\"button\"\n" +
    "                    class=\"btn btn-default btn-xs\"\n" +
    "                    data-ng-click=\"createPerspective()\">\n" +
    "                    <span class=\"glyphicon glyphicon-plus\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "                <button type=\"button\"\n" +
    "                    class=\"btn btn-danger btn-xs\"\n" +
    "                    data-ng-if=\"perspective.system !== true\"\n" +
    "                    data-ng-click=\"deletePerspective()\">\n" +
    "                    <span class=\"glyphicon glyphicon-remove\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </td>\n" +
    "            <td width=\"15px\">\n" +
    "            </td>\n" +
    "            <td valign=\"top\">\n" +
    "                {{'se.modal.perpectives.header.name.name' | translate}}:\n" +
    "                <span data-ng-if=\"perspective.system===true\">\n" +
    "                    <span style=\"padding-left: 5px\">{{ perspective.name | translate }}</span>\n" +
    "                    <span class='pull-right'\n" +
    "                        style=\"color: #feffc1; padding-left: 10px\">{{ warningForSystem | translate }}</span>\n" +
    "                </span>\n" +
    "                <input style='padding-left: 5px; color: black'\n" +
    "                    data-ng-if=\"perspective.system!==true\"\n" +
    "                    type=\"text\"\n" +
    "                    data-ng-required=\"true\"\n" +
    "                    data-ng-trim=\"true\"\n" +
    "                    data-ng-change=\"nameChanged()\"\n" +
    "                    data-ng-disabled=\"perspective.system === true\"\n" +
    "                    data-ng-model=\"perspective.name\">\n" +
    "                <span style=\"color: #ff0e18\">{{ renameError | translate }}</span>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "        <tr>\n" +
    "            <td width=\"25px\">\n" +
    "            </td>\n" +
    "            <td>\n" +
    "                {{'se.modal.perpectives.header.decorators.name' | translate}}\n" +
    "                <div data-ng-repeat=\"ps in decoratorSet\">\n" +
    "                    <input type=\"checkbox\"\n" +
    "                        data-ng-model=\"ps.checked\"\n" +
    "                        data-ng-change=\"save()\"\n" +
    "                        data-ng-disabled=\"perspective.system === true\"> {{ ps.name }}\n" +
    "                </div>\n" +
    "            </td>\n" +
    "        </tr>\n" +
    "    </table>\n" +
    "</div>\n" +
    "<div class=\"modal-footer\">\n" +
    "    <button class=\"btn btn-default\"\n" +
    "        data-ng-click=\"close()\"\n" +
    "        data-translate=\"se.perspectives.actions.close\"></button>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/core/decoratorFilter/perspectiveTemplate.html',
    "<div>\n" +
    "    <span class=\"floating-perspective\"\n" +
    "        data-ng-mouseleave=\"mouseOff()\"\n" +
    "        data-ng-mouseenter=\"mouseOn()\">\n" +
    "        <div>\n" +
    "            <span class=\"xposed-perspectives\">\n" +
    "                <span class=\"glyphicon glyphicon-th\"\n" +
    "                    aria-hidden=\"true\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"floating-bg\"\n" +
    "            data-ng-show=\"!showButtonOnly()\">\n" +
    "            <div>\n" +
    "                <span data-translate=\"se.modal.perpectives.header.perspectives.name\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"form-group form-inline\">\n" +
    "                <select data-ng-change=\"perspectiveSelected(perspective)\"\n" +
    "                    class=\"form-control\"\n" +
    "                    data-ng-options=\"p.name | translate for p in perspectives\"\n" +
    "                    ng-model=\"perspective\" />\n" +
    "                <button type=\"button\"\n" +
    "                    data-ng-click=\"manage()\"\n" +
    "                    class=\"btn btn-default\"\n" +
    "                    aria-hidden=\"true\">\n" +
    "                    <span class=\"glyphicon glyphicon-edit\"\n" +
    "                        aria-hidden=\"true\"></span>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </span>\n" +
    "</div>\n" +
    "<div data-ng-transclude></div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/contextualMenuDecoratorTemplate.html',
    "<div class=\"cmsx-ctx-wrapper1\">\n" +
    "    <div class=\"cmsx-ctx-wrapper2\">\n" +
    "        <div class=\"decorative-top-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-right-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-bottom-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"decorative-left-border decorative-border\"\n" +
    "            data-ng-if=\"ctrl.active\"></div>\n" +
    "        <div class=\"contextualMenuOverlay\"\n" +
    "            data-ng-show=\"ctrl.showOverlay() || ctrl.status.isopen\">\n" +
    "            <div data-ng-repeat=\"item in ctrl.getItems().leftMenuItems\"\n" +
    "                id=\"{{ item.key }}\"\n" +
    "                class=\"btn btn-primary cmsx-ctx-btns\">\n" +
    "                <contextual-menu-item data-mode=\"small\"\n" +
    "                    class=\"se-contextual--menu-item\"\n" +
    "                    data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                    y-popup-overlay=\"ctrl.itemTemplateOverlayWrapper\"\n" +
    "                    y-popup-overlay-trigger=\"{{ctrl.shouldShowTemplate(item)}}\"\n" +
    "                    y-popup-overlay-on-show=\"ctrl.onShowItemPopup()\"\n" +
    "                    y-popup-overlay-on-hide=\"ctrl.onHideItemPopup()\"\n" +
    "                    data-index=\"$index\"\n" +
    "                    data-component-attributes=\"ctrl.componentAttributes\"\n" +
    "                    data-slot-attributes=\"ctrl.slotAttributes\"\n" +
    "                    data-item-config=\"item\"\n" +
    "                    data-component-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                    data-component-uuid=\"{{ctrl.componentAttributes.smarteditComponentUuid}}\"\n" +
    "                    data-component-type=\"{{ctrl.smarteditComponentType}}\"\n" +
    "                    data-slot-id=\"{{ctrl.smarteditSlotId}}\"\n" +
    "                    data-slot-uuid=\"{{ctrl.smarteditSlotUuid}}\"\n" +
    "                    data-container-id=\"{{ctrl.smarteditContainerId}}\"\n" +
    "                    data-container-type=\"{{ctrl.smarteditContainerType}}\">\n" +
    "                </contextual-menu-item>\n" +
    "            </div>\n" +
    "            <div data-ng-if=\"ctrl.getItems().moreMenuItems.length > 0\"\n" +
    "                class=\"cmsx-ctx-more\">\n" +
    "                <div class=\"btn-group yCmsCtxMenu\">\n" +
    "                    <a type=\"button\"\n" +
    "                        class=\"cmsx-ctx-more-btn pull-right\"\n" +
    "                        data-ng-click=\"ctrl.moreMenuIsOpen = !ctrl.moreMenuIsOpen\"\n" +
    "                        y-popup-overlay=\"ctrl.moreMenuPopupConfig\"\n" +
    "                        y-popup-overlay-trigger=\"{{ctrl.moreMenuIsOpen}}\"\n" +
    "                        y-popup-overlay-on-show=\"ctrl.onShowMoreMenuPopup()\"\n" +
    "                        y-popup-overlay-on-hide=\"ctrl.onHideMoreMenuPopup()\">\n" +
    "                        <span title=\"{{ctrl.moreButton.i18nKey | translate}}\"\n" +
    "                            class=\"{{ctrl.moreButton.displayClass}}\"></span>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"contextualmenuBackground\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"yWrapperData\">\n" +
    "            <div data-ng-transclude></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/contextualMenuItemOverlayWrapper.html',
    "<div data-ng-if=\"item.action.template || item.action.templateUrl\"\n" +
    "    class=\"se-contextual-extra-menu\">\n" +
    "    <div data-ng-if=\"item.action.template\">\n" +
    "        <div data-compile-html=\"item.action.template\"></div>\n" +
    "    </div>\n" +
    "    <div data-ng-if=\"item.action.templateUrl\">\n" +
    "        <div data-ng-include=\"item.action.templateUrl\"></div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenu/moreItemsTemplate.html',
    "<div class=\"se-contextual-more-menu\">\n" +
    "    <div id=\"{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-more-menu\">\n" +
    "        <div data-ng-repeat=\"item in ctrl.getItems().moreMenuItems\">\n" +
    "            <a data-smartedit-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                data-smartedit-type=\"{{ctrl.smarteditComponentType}}\">\n" +
    "                <contextual-menu-item data-mode=\"compact\"\n" +
    "                    class=\"se-contextual-more-menu--item\"\n" +
    "                    data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                    y-popup-overlay=\"ctrl.itemTemplateOverlayWrapper\"\n" +
    "                    y-popup-overlay-trigger=\"{{ctrl.shouldShowTemplate(item)}}\"\n" +
    "                    y-popup-overlay-on-show=\"ctrl.onShowItemPopup()\"\n" +
    "                    y-popup-overlay-on-hide=\"ctrl.onHideItemPopup(true)\"\n" +
    "                    data-index=\"$index\"\n" +
    "                    data-component-attributes=\"ctrl.componentAttributes\"\n" +
    "                    data-slot-attributes=\"ctrl.slotAttributes\"\n" +
    "                    data-item-config=\"item\"\n" +
    "                    data-component-id=\"{{ctrl.smarteditComponentId}}\"\n" +
    "                    data-component-uuid=\"{{ctrl.componentAttributes.smarteditComponentUuid}}\"\n" +
    "                    data-component-type=\"{{ctrl.smarteditComponentType}}\"\n" +
    "                    data-slot-id=\"{{ctrl.smarteditSlotId}}\"\n" +
    "                    data-slot-uuid=\"{{ctrl.smarteditSlotUuid}}\"\n" +
    "                    data-container-id=\"{{ctrl.smarteditContainerId}}\"\n" +
    "                    data-container-type=\"{{ctrl.smarteditContainerType}}\">\n" +
    "                </contextual-menu-item>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/contextualMenuItem/contextualMenuItemComponentTemplate.html',
    "<span>\n" +
    "    <span data-ng-if=\"$ctrl.mode === 'small'\">\n" +
    "        <span id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-if=\"$ctrl.isHybrisIcon($ctrl.itemConfig.displayIconClass)\"\n" +
    "            data-ng-class=\"[$ctrl.itemConfig.displayIconClass, $ctrl.itemConfig.displayClass]\"\n" +
    "            class=\"se-contextual--icon clickable\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\">\n" +
    "        </span>\n" +
    "        <img data-ng-init=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseout=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseover=\"$ctrl.itemsrc = $ctrl.itemConfig.iconNonIdle\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\"\n" +
    "            data-ng-if=\"!$ctrl.isHybrisIcon($ctrl.itemConfig.displayIconClass)\"\n" +
    "            class=\"{{$ctrl.itemConfig.displayClass}}\"\n" +
    "            data-ng-class=\"{clickable:true}\"\n" +
    "            data-ng-src=\"{{$ctrl.itemsrc}}\" />\n" +
    "    </span>\n" +
    "    <span data-ng-if=\"$ctrl.mode === 'compact'\">\n" +
    "        <span id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-if=\"$ctrl.isHybrisIcon($ctrl.itemConfig.displaySmallIconClass)\"\n" +
    "            data-ng-class=\"[$ctrl.itemConfig.displaySmallIconClass, $ctrl.itemConfig.displayClass]\"\n" +
    "            class=\"se-contextual-more-menu--icon clickable\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\">\n" +
    "        </span>\n" +
    "        <img data-ng-init=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseout=\"$ctrl.itemsrc = $ctrl.itemConfig.iconIdle\"\n" +
    "            data-ng-mouseover=\"$ctrl.itemsrc = $ctrl.itemConfig.iconNonIdle\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            title=\"{{$ctrl.itemConfig.i18nKey | translate}}\"\n" +
    "            data-ng-if=\"$ctrl.itemConfig.smallIconIdle && !$ctrl.isHybrisIcon($ctrl.itemConfig.displaySmallIconClass)\"\n" +
    "            class=\"{{ $ctrl.classes }}\"\n" +
    "            data-ng-src=\"{{$ctrl.itemsrc}}\" />\n" +
    "        <span class=\"se-contextual--label\"\n" +
    "            id=\"{{$ctrl.itemConfig.i18nKey | translate}}-{{$ctrl.componentAttributes.smarteditComponentId}}-{{$ctrl.componentAttributes.smarteditComponentType}}-{{$ctrl.index}}\"\n" +
    "            data-ng-class=\"$ctrl.itemConfig.displayClass\">\n" +
    "            {{$ctrl.itemConfig.i18nKey | translate}}\n" +
    "        </span>\n" +
    "    </span>\n" +
    "</span>"
  );


  $templateCache.put('web/smartedit/modules/systemModule/features/slotContextualMenu/slotContextualMenuDecoratorTemplate.html',
    "<div>\n" +
    "    <div class=\"cmsx-ctx-wrapper1 se-slot-contextual-menu-level1\">\n" +
    "        <div class=\"cmsx-ctx-wrapper2 se-slot-contextual-menu-level2\">\n" +
    "            <div class=\"decorative-panel-area\"\n" +
    "                data-ng-if=\"ctrl.showOverlay()\">\n" +
    "                <p class=\"decorative-panel-text\">{{::ctrl.smarteditComponentId}}</p>\n" +
    "                <div class=\"decorator-panel-padding-center\"></div>\n" +
    "                <div class=\"decorative-panel-slot-contextual-menu\"\n" +
    "                    data-ng-if=\"ctrl.showOverlay()\">\n" +
    "                    <div data-ng-repeat=\"item in ctrl.getItems().leftMenuItems\"\n" +
    "                        class=\"btn btn-primary cmsx-ctx-btns pull-right\"\n" +
    "                        data-ng-init=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseout=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseover=\"itemsrc = item.iconNonIdle\">\n" +
    "                        <div data-ng-if=\"!item.templateUrl\">\n" +
    "                            <span id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon\"\n" +
    "                                data-ng-if=\"item.iconIdle && ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                class=\"ng-class:{clickable:true}\">\n" +
    "                                <img data-ng-src=\"{{itemsrc}}\"\n" +
    "                                    id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon-img\"\n" +
    "                                    title=\"{{::item.i18nKey | translate}}\" />\n" +
    "                            </span>\n" +
    "                            <img id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}\"\n" +
    "                                title=\"{{::item.i18nKey | translate}}\"\n" +
    "                                data-ng-if=\"item.iconIdle && !ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                class=\"{{item.displayClass}}\"\n" +
    "                                data-ng-class=\"{clickable:true}\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                data-ng-src=\"{{itemsrc}}\"\n" +
    "                                alt=\"{{item.i18nKey}}\" />\n" +
    "                        </div>\n" +
    "                        <div data-ng-if=\"item.templateUrl\">\n" +
    "                            <div data-ng-include=\"item.templateUrl\"></div>\n" +
    "                        </div>\n" +
    "                        <div class=\"slot-context-menu-divider\"></div>\n" +
    "                    </div>\n" +
    "                    <div data-ng-repeat=\"item in ctrl.getItems().moreMenuItems\"\n" +
    "                        class=\"btn btn-primary cmsx-ctx-btns pull-right\"\n" +
    "                        data-ng-init=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseout=\"itemsrc = item.iconIdle\"\n" +
    "                        data-ng-mouseover=\"itemsrc = item.iconNonIdle\">\n" +
    "                        <div data-ng-if=\"!item.templateUrl\">\n" +
    "                            <span id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon\"\n" +
    "                                data-ng-if=\"item.iconIdle && ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                class=\"ng-class:{clickable:true}\">\n" +
    "                                <img data-ng-src=\"{{itemsrc}}\"\n" +
    "                                    id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}-hyicon-img\"\n" +
    "                                    title=\"{{::item.i18nKey | translate}}\" />\n" +
    "                            </span>\n" +
    "                            <img id=\"{{::item.i18nKey | translate}}-{{::ctrl.smarteditComponentId}}-{{::ctrl.smarteditComponentType}}\"\n" +
    "                                title=\"{{::item.i18nKey | translate}}\"\n" +
    "                                data-ng-if=\"item.iconIdle && !ctrl.isHybrisIcon(item.displayClass)\"\n" +
    "                                class=\"{{item.displayClass}}\"\n" +
    "                                data-ng-class=\"{clickable:true}\"\n" +
    "                                data-ng-click=\"ctrl.triggerMenuItemAction(item, $event)\"\n" +
    "                                data-ng-src=\"{{itemsrc}}\"\n" +
    "                                alt=\"{{item.i18nKey}}\" />\n" +
    "                        </div>\n" +
    "                        <div data-ng-if=\"item.templateUrl\">\n" +
    "                            <div data-ng-include=\"item.templateUrl\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"decorator-padding-container\">\n" +
    "                <div class=\"decorator-padding-left\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"decorator-slot-border\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"yWrapperData\"\n" +
    "                    data-ng-transclude\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"decorator-padding-right\"\n" +
    "                    data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );

}]);
