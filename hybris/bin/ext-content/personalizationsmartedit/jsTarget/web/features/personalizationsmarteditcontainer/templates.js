angular.module('personalizationsmarteditcontainerTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationsmarteditcontainer/combinedView/personalizationsmarteditCombinedViewConfigureTemplate.html',
    "<form>\n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"pe-combinedview-config__ui-select-layout\">\n" +
    "            <label for=\"CombinedViewSearchField1\" class=\"control-label required\" data-translate=\"personalization.modal.combinedview.search.label\"></label>\n" +
    "            <ui-select uis-open-close=\"initUiSelect($select)\" ng-model=\"selectedElement\" theme=\"select2\" class=\"form-control\" ng-keyup=\"searchInputKeypress($event, $select.search)\" on-select=\"selectElement($item)\" reset-search-input=\"false\" id=\"CombinedViewSearchField1\">\n" +
    "                <ui-select-match placeholder=\"{{ 'personalization.modal.combinedview.search.placeholder' | translate}}\">\n" +
    "                    <span class=\"pe-combinedview-config__ui-select-placeholder\">{{'personalization.modal.combinedview.search.placeholder' | translate}}</span>\n" +
    "                </ui-select-match>\n" +
    "                <ui-select-choices repeat=\"item in selectionArray\" position=\"down\" ui-disable-choice=\"isItemInSelectDisabled(item)\" personalization-infinite-scroll=\"addMoreItems()\" personalization-infinite-scroll-distance=\"1\">\n" +
    "                    <div data-ng-class=\"{'pe-combinedview-config__ui-select-item--selected': isItemSelected(item)}\">\n" +
    "                        {{item.customization.name}} > {{item.variation.name}}\n" +
    "                    </div>\n" +
    "                </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"pe-combinedview-config__list-layout\" data-ng-repeat=\"item in selectedItems\" data-ng-class=\"{'pe-combinedview-config__divider': $first}\">\n" +
    "            <div data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "            <div class=\"pe-combinedview-config__names-layout\">\n" +
    "                <div class=\"perso-wrap-ellipsis pe-combinedview-config__cname\" data-ng-bind=\"item.customization.name\"></div>\n" +
    "                <span> ></span>\n" +
    "                <div class=\"perso-wrap-ellipsis pe-combinedview-config__vname\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"pe-combinedview-config__hyicon-remove\">\n" +
    "                <span class=\"hyicon hyicon-remove\" data-ng-click=\"removeSelectedItem(item)\"></span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</form>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/combinedView/personalizationsmarteditCombinedViewMenuTemplate.html',
    "<div data-ng-controller=\"topToolbarMenuController\" data-uib-dropdown data-is-open=\"item.isOpen\" class=\"btn-group pe-toolbar-action pe-combined-view-menu\" data-auto-close=\"disabled\">\n" +
    "    <button type=\"button\" class=\"btn btn-default pe-toolbar-action--button\" data-uib-dropdown-toggle data-ng-disabled=\"disabled\" aria-pressed=\"false\" data-ng-class=\"{'pe-toolbar-action-combined-view--button-with-context': isCombinedViewCustomizationSelected()}\">\n" +
    "        <span class=\"hyicon hyicon-combinedview pe-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <span class=\"pe-toolbar-action--button--txt\" data-translate=\"personalization.toolbar.combinedview.name\"></span>\n" +
    "    </button>\n" +
    "\n" +
    "    <personalizationsmartedit-combined-view-toolbar-context></personalizationsmartedit-combined-view-toolbar-context>\n" +
    "\n" +
    "    <div data-uib-dropdown-menu class=\"btn-block pe-toolbar-action--include\">\n" +
    "\n" +
    "        <ul class=\"pe-toolbar-menu-content  se-toolbar-menu-content--pe-customized\" role=\"menu\">\n" +
    "            <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "                <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.combinedview.header.title\"></h2>\n" +
    "                <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.combinedview.header.description\"></small>\n" +
    "            </div>\n" +
    "            <div class=\"pe-combined-view-menu\">\n" +
    "                <div class=\"pe-combined-view-menu__wrapper\" data-ng-controller=\"personalizationsmarteditCombinedViewMenuController\">\n" +
    "                    <div class=\"pe-combined-view-menu__configure-layout\">\n" +
    "                        <span class=\"y-toggle y-toggle-lg\">\n" +
    "                            <input type=\"checkbox\" id=\"test-checkbox\" data-ng-change=\"combinedViewEnabledChangeEvent()\" data-ng-model=\"combinedView.enabled\" />\n" +
    "                            <label class=\"pe-combined-view-menu__enable-label\" for=\"test-checkbox\"></label>\n" +
    "                        </span>\n" +
    "                        <button class=\"btn btn-link pe-combined-view-menu__configure-btn perso-wrap-ellipsis\" data-ng-click=\"$parent.combinedViewClick()\" data-translate=\"personalization.toolbar.combinedview.openconfigure.button\">\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                    <div class=\"pe-combined-view-menu__category-table\" data-ng-class=\"combinedView.enabled ? '':'combinedview-inactive-layout'\">\n" +
    "                        <div class=\"pe-combined-view-menu__category-table__item\" data-ng-repeat=\"item in selectedItems\" data-ng-class=\"{'combinedview-list-item--last': $last, 'combinedview-divider': $first, 'pe-combined-view-menu__list-item--highlighted': item.highlighted}\">\n" +
    "                            <div class=\"pe-combined-view-menu__category-table__item-icon\" data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "                            <div class=\"pe-combined-view-menu__category-table__item-name\" data-ng-click=\"itemClick(item)\">\n" +
    "                                <div data-ng-bind=\"item.customization.name\"></div>\n" +
    "                                <div class=\"text-uppercase combinedview-variation-layout\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "                            </div>\n" +
    "                            <div data-ng-class=\"{'hyicon hyicon-checkedlg pe-combined-view-menu__hyicon-checkedlg': item.highlighted}\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/contextMenu/personalizationsmarteditAddEditActionTemplate.html',
    "<div class=\"perso-customize-component\">\n" +
    "    <div class=\"perso-customize-component__title-layout\">\n" +
    "        <div data-ng-show='letterIndicatorForElement' class=\"perso-customize-component__title-layout__letter-block\">\n" +
    "            <span ng-class=\"colorIndicatorForElement\">{{letterIndicatorForElement}}</span>\n" +
    "        </div>\n" +
    "        <div class=\"perso-customize-component__title-layout__cust-name\">{{selectedCustomization.name}}</div>\n" +
    "        <div class=\"perso-customize-component__title-layout__target-group-name\">{{'> ' + selectedVariation.name}}</div>\n" +
    "    </div>\n" +
    "\n" +
    "    <dl class=\"perso-customize-component__data-list\">\n" +
    "        <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.addeditaction.selected.mastercomponent.title\"></label>\n" +
    "        <dd>{{componentType}}</dd>\n" +
    "    </dl>\n" +
    "\n" +
    "    <label class=\"control-label required\" data-translate=\"personalization.modal.addeditaction.selected.actions.title\"></label>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-model=\"action.selected\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in actions\" position=\"down\">\n" +
    "            <span ng-bind=\"item.name \"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-show=\"action.selected.id == 'use'\" ng-model=\"component.selected\" ng-keyup=\"componentSearchInputKeypress($event, $select.search)\" theme=\"select2\" title=\"\" reset-search-input=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.componentlist.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name | translate\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in components\" position=\"down\" personalization-infinite-scroll=\"addMoreComponentItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "            <div class=\"row\">\n" +
    "                <span class=\"col-md-7\" ng-bind=\"item.name\"></span>\n" +
    "                <span class=\"col-md-5\" ng-bind=\"item.typeCode\"></span>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-show=\"action.selected.id == 'create'\" on-select=\"newComponentTypeSelectedEvent($item, $model)\" ng-model=\"newComponent.selected\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.componenttype.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in newComponentTypes\" position=\"down\">\n" +
    "            <span ng-bind=\"item.i18nKey | translate\"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/contextMenu/personalizationsmarteditShowActionListTemplate.html',
    "<div class=\"pe-combinedview-ranking\">\n" +
    "    <p class=\"pe-combinedview-ranking__help-text\" data-translate=\"personalization.modal.showactionlist.help.label\"></p>\n" +
    "    <div class=\"pe-combinedview-ranking__divider\"></div>\n" +
    "    <div class=\"pe-combinedview-ranking__list-layout\" data-ng-repeat=\"item in selectedItems\" data-ng-init=\"initItem(item)\" data-ng-show=\"item.visible\">\n" +
    "        <div data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "        <div class=\"pe-combinedview-ranking__names-layout\">\n" +
    "            <div class=\"perso-wrap-ellipsis pe-combinedview-ranking__cname\" data-ng-bind=\"item.customization.name\"></div>\n" +
    "            <span> ></span>\n" +
    "            <div class=\"perso-wrap-ellipsis pe-combinedview-ranking__vname\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/commerceCustomizationView/personalizationsmarteditCommerceCustomizationViewTemplate.html',
    "<div id=\"commerceCustomizationBody-002\" class=\"perso-cc-modal\">\n" +
    "    <div class=\"perso-cc-modal__title-layout\">\n" +
    "        <div class=\"perso-wrap-ellipsis perso-cc-modal__title-cname\" data-ng-bind=\"customization.name\"></div>\n" +
    "        <div class=\"perso-cc-modal__title-status\" data-ng-class=\"customizationStatus\">{{ '(' + customizationStatusText + ')' }}</div>\n" +
    "        <span> > </span>\n" +
    "        <div class=\"perso-wrap-ellipsis perso-cc-modal__title-vname\" data-ng-bind=\"variation.name\"></div>\n" +
    "        <div class=\"perso-cc-modal__title-status\" data-ng-class=\"variationStatus\">{{' (' + variationStatusText + ')'}}</div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group perso-cc-modal__content-layout\">\n" +
    "        <label for=\"commerce-customization-type-1\" class=\"personalization-modal__label\" data-translate=\"personalization.modal.commercecustomization.action.type\"></label>\n" +
    "        <ui-select id=\"commerce-customization-type-1\" class=\"form-control\" ng-model=\"select.type\" theme=\"select2\" search-enabled=\"false\">\n" +
    "            <ui-select-match>\n" +
    "                <span data-ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"item in availableTypes\" position=\"down\">\n" +
    "                <span data-ng-bind=\"item.text | translate\"></span>\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "    </div>\n" +
    "    <div class=\"form-group perso-cc-modal__content-layout\">\n" +
    "        <ng-include src=\"select.type.template\" />\n" +
    "    </div>\n" +
    "    <div class=\"select2-choices\">\n" +
    "        <div class=\"ui-select-match-item select2-search-choice perso-cc-modal__ui-select--small\" data-ng-repeat=\"action in getActionsToDisplay()\">\n" +
    "            <span data-ng-bind=\"displayAction(action)\"></span>\n" +
    "            <span class=\"ui-select-match-close select2-search-choice-close\" data-ng-click=\"removeSelectedAction(action)\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/commerceCustomizationView/promotions/personalizationsmarteditPromotionsTemplate.html',
    "<div ng-controller=\"personalizationsmarteditPromotionController\">\n" +
    "    <label for=\"promotion-selector-1\" class=\"personalization-modal__label\" data-translate=\"personalization.modal.commercecustomization.promotion.label\"></label>\n" +
    "    <ui-select uis-open-close=\"initUiSelect($select)\" id=\"promotion-selector-1\" class=\"form-control\" ng-model=\"promotion\" on-select=\"promotionSelected($item, $select)\" theme=\"select2\" search-enabled=\"true\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.commercecustomization.promotion.search.placeholder' | translate}}\">\n" +
    "            <span>{{'personalization.modal.commercecustomization.promotion.search.placeholder' | translate}}</span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in availablePromotions | filter: $select.search\" ui-disable-choice=\"isItemInSelectDisabled(item)\" position=\"down\">\n" +
    "            <div class=\"row ng-scope\">\n" +
    "                <span class=\"col-md-8 ng-binding\" ng-bind=\"item.code\"></span>\n" +
    "                <span class=\"col-md-4 ng-binding\" ng-bind=\"item.promotionGroup\"></span>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewGridHeaderTemplate.html',
    "<div class=\"row tree-head hidden-sm hidden-xs\">\n" +
    "    <div class=\"col-md-4 text-left\" data-translate=\"personalization.modal.manager.grid.header.customization\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding\" data-translate=\"personalization.modal.manager.grid.header.variations\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding se-nowrap-ellipsis\" data-translate=\"personalization.modal.manager.grid.header.components\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding\">\n" +
    "        <span class=\"perso-library__status-header\" data-translate=\"personalization.modal.manager.grid.header.status\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-2 text-center\" data-translate=\"personalization.modal.manager.grid.header.startdate\"></div>\n" +
    "    <div class=\"col-md-2 text-center\" data-translate=\"personalization.modal.manager.grid.header.enddate\"></div>\n" +
    "</div>\n" +
    "<div class=\"row tree-head visible-sm visible-xs\">\n" +
    "    <div class=\"col-xs-10 text-left\" data-translate=\"personalization.modal.manager.grid.header.customization\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewNodeTemplate.html',
    "<div class=\"y-tree-row\" ng-init=\"customization.isCollapsed=true\" ng-class=\"allCustomizationsCollapsed()? 'active-level' : 'inactive-level'\">\n" +
    "    <div class=\"row desktop-layout hidden-sm hidden-xs customization-rank-{{customization.rank}}-row\">\n" +
    "        <div class=\"pull-left\" data-ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <a class=\"btn btn-link category-toggle\" title=\"{{customization.isCollapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span data-ng-class=\"customization.isCollapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon customization__glyphicon-chevron\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div ui-tree-handle class=\"y-tree-row__angular-ui-tree-handle\">\n" +
    "            <div class=\"col-md-4 text-left\">\n" +
    "                <p class=\"perso-library__primary-data\">\n" +
    "                    <span class=\"personalizationsmartedit-customization-code\" ng-bind=\"customization.name\"></span>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-1 text-left\" data-ng-bind=\"(customization.variations | statusNotDeleted).length || 0\">\n" +
    "            </div>\n" +
    "            <div class=\"col-md-1\"></div>\n" +
    "            <div class=\"col-md-1 col-reset-padding text-left perso-library__status-value\" data-ng-class=\"getActivityStateForCustomization(customization)\">\n" +
    "                <span ng-bind=\"getEnablementTextForCustomization(customization)\" class=\"perso-library__status-layout\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 perso-library__dates-layout\">\n" +
    "                <div data-ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"text-center perso-library__dates-value\">\n" +
    "                    <span data-ng-bind=\"getFormattedDate(customization.enabledStartDate)\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 perso-library__dates-layout\">\n" +
    "                <div data-ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"text-center perso-library__dates-value\">\n" +
    "                    <span data-ng-bind=\"getFormattedDate(customization.enabledEndDate)\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"pull-right\">\n" +
    "            <div class=\"dropdown\">\n" +
    "                <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                    <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                </button>\n" +
    "                <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                    <li>\n" +
    "                        <a data-ng-click=\"editCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.edit\"></a>\n" +
    "                    </li>\n" +
    "                    <li ng-class=\"isFilterEnabled() || $first ? 'disabled' : '' \">\n" +
    "                        <a data-ng-click=\"(isFilterEnabled() || $first) ? $event.stopPropagation() : setCustomizationRank(customization, -1)\" data-translate=\"personalization.modal.manager.customization.options.moveup\"></a>\n" +
    "                    </li>\n" +
    "                    <li ng-class=\"isFilterEnabled() || $last ? 'disabled' : '' \">\n" +
    "                        <a data-ng-click=\"(isFilterEnabled() || $last) ? $event.stopPropagation() : setCustomizationRank(customization, 1)\" data-translate=\"personalization.modal.manager.customization.options.movedown\"></a>\n" +
    "                    </li>\n" +
    "                    <li role=\"separator\" class=\"divider\"></li>\n" +
    "                    <li>\n" +
    "                        <a data-ng-click=\"deleteCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.delete\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <!--end contextual menu dropdown-->\n" +
    "    </div>\n" +
    "    <!--end desktop-layout for customization row-->\n" +
    "\n" +
    "    <div class=\"row mobile-layout visible-sm visible-xs customization-rank-{{customization.rank}}-row\">\n" +
    "        <div class=\"pull-left\" data-ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <a class=\"btn btn-link category-toggle\" title=\"{{customization.isCollapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span data-ng-class=\"customization.isCollapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon customization__glyphicon-chevron\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-10 text-left\" ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <p class=\"perso-library__primary-data\">\n" +
    "                <span class=\"personalizationsmartedit-customization-code\" ng-bind=\"customization.name\"></span>\n" +
    "            </p>\n" +
    "            <div class=\"mobile-data\">\n" +
    "                <span class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.variations\"></span>\n" +
    "                <p ng-bind=\"(customization.variations | statusNotDeleted).length || 0\"></p>\n" +
    "            </div>\n" +
    "            <div class=\"mobile-data perso-library__status-value\" data-ng-class=\"getActivityStateForCustomization(customization)\">\n" +
    "                <span ng-bind=\"getEnablementTextForCustomization(customization)\"></span>\n" +
    "            </div>\n" +
    "            <div ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\">\n" +
    "                <div class=\"mobile-data\" ng-if='customization.enabledStartDate || customization.enabledEndDate'>\n" +
    "                    <span ng-if='customization.enabledStartDate' class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.startdate\"></span>\n" +
    "                    <p ng-bind=\"getFormattedDate(customization.enabledStartDate)\"></p>\n" +
    "                    <span ng-if='customization.enabledEndDate' class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.enddate\"></span>\n" +
    "                    <p ng-bind=\"getFormattedDate(customization.enabledEndDate)\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"pull-left\">\n" +
    "            <div class=\"mobile-more-menu\">\n" +
    "                <div class=\"dropdown\">\n" +
    "                    <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                        <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a data-ng-click=\"editCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.edit\"></a>\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"isFilterEnabled() || $first ? 'disabled' : '' \">\n" +
    "                            <a data-ng-click=\"(isFilterEnabled() || $first) ? $event.stopPropagation() : setCustomizationRank(customization, -1)\" data-translate=\"personalization.modal.manager.customization.options.moveup\"></a>\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"isFilterEnabled() || $last ? 'disabled' : '' \">\n" +
    "                            <a data-ng-click=\"(isFilterEnabled() || $last) ? $event.stopPropagation() : setCustomizationRank(customization, 1)\" data-translate=\"personalization.modal.manager.customization.options.movedown\"></a>\n" +
    "                        </li>\n" +
    "                        <li role=\"separator\" class=\"divider\"></li>\n" +
    "                        <li>\n" +
    "                            <a data-ng-click=\"deleteCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.delete\"></a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!--end contextual menu dropdown-->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end mobile-layout for customization row-->\n" +
    "</div>\n" +
    "<!--end angular-ui-tree-handle-->\n" +
    "<div data-uib-collapse=\"customization.isCollapsed\">\n" +
    "    <div ui-tree-nodes ng-model=\"customization.variations\">\n" +
    "        <div class=\"y-tree-row child-row active-level\" data-ng-class=\"{'perso-library__angular-ui-tree-drag': variation.isDragging}\" ui-tree-node ng-repeat=\"variation in customization.variations | orderBy:'rank'\" ng-if=\"statusNotDeleted(variation)\" ng-init=\"variation.isCommerceCustomizationEnabled=isCommerceCustomizationEnabled()\">\n" +
    "            <div class=\"row desktop-layout variation-rank-{{variation.rank}}-row hidden-sm hidden-xs\" ng-class=\"$last ? 'active-level--last':''\">\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <span class=\"perso-library__cc-marker\" data-ng-class=\"{'perso-library__cc-icon': hasCommerceCustomization(variation)}\" data-uib-tooltip=\"{{getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></span>\n" +
    "                </div>\n" +
    "                <div ui-tree-handle class=\"y-tree-row__angular-ui-tree-handle\">\n" +
    "                    <div class=\"col-xs-5 text-left\">\n" +
    "                        <span ng-bind=\"variation.name\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-1 text-left\" data-ng-bind=\"variation.numberOfComponents\">\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-1 col-reset-padding text-left perso-library__status-value\" data-ng-class=\"getActivityStateForVariation(customization,variation)\">\n" +
    "                        <span ng-bind=\"getEnablementTextForVariation(variation)\" class=\"perso-library__status-layout\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-4 perso-library__dates-layout\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-right\">\n" +
    "                    <div class=\"dropdown\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                            <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                        </button>\n" +
    "                        <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                            <li>\n" +
    "                                <a data-ng-click=\"editVariationAction(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.edit\"></a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a data-ng-click=\"toogleVariationActive(customization,variation)\" ng-bind=\"getEnablementActionTextForVariation(variation)\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-show=\"variation.isCommerceCustomizationEnabled\">\n" +
    "                                <a data-ng-click=\"manageCommerceCustomization(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.commercecustomization\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                                <a data-ng-click=\"$first ? $event.stopPropagation() : setVariationRank(customization, variation, -1)\" data-translate=\"personalization.modal.manager.variation.options.moveup\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                                <a data-ng-click=\"$last ? $event.stopPropagation() : setVariationRank(customization, variation, 1)\" data-translate=\"personalization.modal.manager.variation.options.movedown\"></a>\n" +
    "                            </li>\n" +
    "                            <li role=\"separator\" class=\"divider\"></li>\n" +
    "                            <li ng-class=\"isDeleteVariationEnabled(customization) ? '' : 'disabled' \">\n" +
    "                                <a data-ng-click=\"deleteVariationAction(customization, variation, $event)\" data-translate=\"personalization.modal.manager.variation.options.delete\"></a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <!--end contextual menu dropdown-->\n" +
    "            </div>\n" +
    "            <!--end desktop-layout for variation row-->\n" +
    "            <div class=\"row mobile-layout visible-sm visible-xs variation-rank-{{variation.rank}}-row\" ng-class=\"$last ? 'active-level--last':''\">\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <span class=\"perso-library__cc-marker\" data-ng-class=\"{'perso-library__cc-icon': hasCommerceCustomization(variation)}\" data-uib-tooltip=\"{{getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-10 text-left\" ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "                    <p ng-bind=\"variation.name\"></p>\n" +
    "                    <div class=\"mobile-data\">\n" +
    "                        <span class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.components\"></span>\n" +
    "                        <div ng-bind=\"variation.numberOfComponents\"></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"mobile-data perso-library__status-value\" data-ng-class=\"getActivityStateForVariation(customization,variation)\">\n" +
    "                        <span ng-bind=\"getEnablementTextForVariation(variation)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <div class=\"mobile-more-menu\">\n" +
    "                        <div class=\"dropdown\">\n" +
    "                            <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                                <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                            </button>\n" +
    "                            <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                                <li>\n" +
    "                                    <a data-ng-click=\"editVariationAction(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.edit\"></a>\n" +
    "                                </li>\n" +
    "                                <li>\n" +
    "                                    <a data-ng-click=\"toogleVariationActive(customization,variation)\" ng-bind=\"getEnablementActionTextForVariation(variation)\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-show=\"variation.isCommerceCustomizationEnabled\">\n" +
    "                                    <a data-ng-click=\"manageCommerceCustomization(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.commercecustomization\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                                    <a data-ng-click=\"$first ? $event.stopPropagation() : setVariationRank(customization, variation, -1)\" data-translate=\"personalization.modal.manager.variation.options.moveup\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                                    <a data-ng-click=\"$last ? $event.stopPropagation() : setVariationRank(customization, variation, 1)\" data-translate=\"personalization.modal.manager.variation.options.movedown\"></a>\n" +
    "                                </li>\n" +
    "                                <li role=\"separator\" class=\"divider\"></li>\n" +
    "                                <li ng-class=\"isDeleteVariationEnabled(customization) ? '' : 'disabled' \">\n" +
    "                                    <a data-ng-click=\"deleteVariationAction(customization, variation, $event)\" data-translate=\"personalization.modal.manager.variation.options.delete\"></a>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <!--end contextual menu dropdown-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!--end mobile-layout for variation row-->\n" +
    "        </div>\n" +
    "        <!--end variation-repeat-->\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!--end collapse-->\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewTemplate.html',
    "<div id=\"editConfigurationsBody-001\" class=\"perso-library\">\n" +
    "\n" +
    "    <personalizationsmartedit-scroll-zone data-ng-if=\"scrollZoneElement != null\" data-is-transparent=\"true\" data-scroll-zone-visible=\"scrollZoneVisible\" data-get-element-to-scroll=\"getElementToScroll()\"></personalizationsmartedit-scroll-zone>\n" +
    "\n" +
    "    <div class=\"perso-library__scroll-zone perso__scrollbar--hidden\" personalization-current-element=\"setScrollZoneElement\" personalization-infinite-scroll=\"addMoreItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "        <div>\n" +
    "            <h2 class=\"text-capitalize perso-library__header\" data-ng-bind=\"catalogName\"></h2>\n" +
    "            <div class=\"row perso-library__search\">\n" +
    "                <div class=\"col-sm-4 col-sm-offset-3\">\n" +
    "                    <div class=\"input-group\">\n" +
    "                        <span class=\"input-group-addon hyicon hyicon-search perso-library__search-icon\"></span>\n" +
    "                        <input type=\"text\" class=\"form-control perso-library__search-input ng-pristine ng-untouched ng-valid\" placeholder=\"{{ 'personalization.modal.manager.search.placeholder' | translate}}\" ng-model=\"search.name\" ng-keyup=\"searchInputKeypress($event)\"></input>\n" +
    "                        <span data-ng-show=\"search.name\" class=\"input-group-addon glyphicon glyphicon-remove-sign ySESearchIcon\" ng-click=\"search.name=''; searchInputKeypress($event)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-2 pull-left perso__filter-layout\">\n" +
    "                    <ui-select ng-model=\"search.status\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "                        <ui-select-match>\n" +
    "                            <span ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "                        </ui-select-match>\n" +
    "                        <ui-select-choices repeat=\"item in statuses\" position=\"down\">\n" +
    "                            <span ng-bind=\"item.text | translate\"></span>\n" +
    "                        </ui-select-choices>\n" +
    "                    </ui-select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div data-ng-show=\"isSearchResultHidden()\" class=\"perso-library__search-results--fixed\">\n" +
    "                <span data-ng-bind=\"filteredCustomizationsCount\"></span>\n" +
    "                <span data-translate=\"personalization.modal.manager.search.result.label\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"perso-library__search-results\">\n" +
    "                <span data-ng-bind=\"filteredCustomizationsCount\"></span>\n" +
    "                <span data-translate=\"personalization.modal.manager.search.result.label\"></span>\n" +
    "            </div>\n" +
    "\n" +
    "            <button class=\"y-add y-add-btn perso-library__add-button\" type=\"button\" data-ng-click=\"openNewModal();\">\n" +
    "                <span class=\"hyicon hyicon-add\" data-translate=\"personalization.modal.manager.add.button\"></span>\n" +
    "            </button>\n" +
    "\n" +
    "            <div class=\"y-tree perso-library__y-tree\">\n" +
    "                <!-- headers -->\n" +
    "                <div data-ng-show=\"isSearchGridHeaderHidden()\" class=\"y-tree-header y-tree-header--fixed\">\n" +
    "                    <data-ng-include src=\"'personalizationsmarteditManagerViewGridHeaderTemplate.html'\"></data-ng-include>\n" +
    "                </div>\n" +
    "                <div class=\"y-tree-header\">\n" +
    "                    <data-ng-include src=\"'personalizationsmarteditManagerViewGridHeaderTemplate.html'\"></data-ng-include>\n" +
    "                </div>\n" +
    "                <!--end y-tree-header-->\n" +
    "\n" +
    "                <div ui-tree=\"treeOptions\" id=\"tree-root\">\n" +
    "                    <div ui-tree-nodes data-ng-model=\"customizations\">\n" +
    "                        <div data-ng-repeat=\"customization in customizations\" data-ng-class=\"{'perso-library__angular-ui-tree-drag': customization.isDragging}\" ui-tree-node data-ng-include=\"'personalizationsmarteditManagerViewNodeTemplate.html'\"></div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div data-ng-show=\"moreCustomizationsRequestProcessing\">\n" +
    "                    <div data-ng-include=\"'waitDialog.html'\"></div>\n" +
    "                </div>\n" +
    "                <!--end customization-repeat-->\n" +
    "            </div>\n" +
    "            <!--end y-tree-->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end scroll zone-->\n" +
    "\n" +
    "    <a class=\"perso-library__back-to-top-button\" title=\"{{'personalization.commons.button.title.backtotop' | translate}}\" data-ng-show=\"isReturnToTopButtonVisible()\" data-ng-click=\"scrollZoneReturnToTop()\">\n" +
    "        <span class=\"hyicon hyicon-top back-to-top\"></span>\n" +
    "    </a>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/personalizationsmarteditCustomizationManagMenuTemplate.html',
    "<ul data-ng-controller=\"topToolbarMenuController\" class=\"se-toolbar-menu-content se-toolbar-menu-content--pe-customized\">\n" +
    "    <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "        <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.library.header.title\"></h2>\n" +
    "        <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.library.header.description\"></small>\n" +
    "    </div>\n" +
    "    <li class=\"se-toolbar-menu-content--pe-customized__item\">\n" +
    "        <a class=\"se-toolbar-menu-content--pe-customized__item__link\" id=\"personalizationsmartedit-pagecustomizations-toolbar-customization-anchor\" data-translate=\"personalization.toolbar.library.manager.name\" data-ng-click=\"managerViewClick()\"></a>\n" +
    "    </li>\n" +
    "    <li class=\"se-toolbar-menu-content--pe-customized__item se-toolbar-menu-content--pe-customized__item--last\">\n" +
    "        <a class=\"se-toolbar-menu-content--pe-customized__item__link\" id=\"personalizationsmartedit-pagecustomizations-toolbar-customization-anchor\" data-translate=\"personalization.toolbar.library.customizationvariationmanagement.name\" data-ng-click=\"createCustomizationClick()\"></a>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/personalizationsmarteditCustomizationManagTemplate.html',
    "<div>\n" +
    "    <uib-tabset active=\"activeTabNumber\">\n" +
    "        <uib-tab ng-repeat=\"tab in tabsArr\" select=\"selectTab(tab)\" disable=\"tab.disabled\" heading=\"{{tab.heading}}\">\n" +
    "            <form name=\"{{tab.formName}}\" novalidate>\n" +
    "                <div>\n" +
    "                    <data-ng-include src=\"tab.template\"></data-ng-include>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </uib-tab>\n" +
    "    </uib-tabset>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/personalizationsmarteditCustVarManagBasicInfoTemplate.html',
    "<div class=\"customization-form\">\n" +
    "    <form>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-name\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.name\" class=\"control-label required\"></label>\n" +
    "            <input type=\"text\" class=\"form-control\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.basicinformationtab.name.placeholder' | translate}}\" name=\"{{customization.name}}_key\" data-ng-model=\"customization.name\" data-ng-required=\"true\" id=\"customization-name\">\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-description\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details\" class=\"personalization-modal__label\"></label>\n" +
    "            <textarea rows=\"2\" class=\"form-control description-area\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.basicinformationtab.details.placeholder' | translate}}\" name=\"{{customization.description}}_key\" data-ng-model=\"customization.description\" id=\"customization-description\"></textarea>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-status\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.status\" class=\"personalization-modal__label\"></label>\n" +
    "            <div class=\"status-toggle\">\n" +
    "                <span class=\"y-toggle y-toggle-lg\" id=\"customization-status\" ng-init=\"customization.status = PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\">\n" +
    "                    <input type=\"checkbox\" id=\"test-checkbox\" data-ng-model=\"customization.statusBoolean\" />\n" +
    "                    <label for=\"test-checkbox\"></label>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <div>\n" +
    "                <a ng-show=\"!edit.datetimeConfigurationEnabled\" data-ng-click=\"edit.datetimeConfigurationEnabled = !edit.datetimeConfigurationEnabled\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.showdateconfigdata\"></a>\n" +
    "                <a ng-show=\"edit.datetimeConfigurationEnabled\" data-ng-click=\"edit.datetimeConfigurationEnabled = !edit.datetimeConfigurationEnabled; resetDateTimeConfiguration();\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.hidedateconfigdata\"></a>\n" +
    "            </div>\n" +
    "            <div data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.statusfortimeframe.description\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <div ng-show=\"edit.datetimeConfigurationEnabled\">\n" +
    "                <div class=\"row form-group-dates\">\n" +
    "                    <date-time-picker-range name=\"data-date-time-from-to-key\" data-date-from=\"customization.enabledStartDate\" data-date-to=\"customization.enabledEndDate\" data-is-editable=\"true\" date-format=\"edit.viewDateFormat\"></date-time-picker-range>\n" +
    "                </div>\n" +
    "                <span class=\"help-inline {$modifiers}\">\n" +
    "                    <span data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.timeframe.description\" class=\"help-block-inline text-muted\"></span>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/personalizationsmarteditCustVarManagTargetGrpTemplate.html',
    "<div ng-controller=\"personalizationsmarteditManagerTargetGrpController as $ctrl\" class=\"customization-form\">\n" +
    "    <div class=\"customization-form__title\">\n" +
    "        <div class=\"customization-form__title-header\">\n" +
    "            <div ng-bind=\"customization.name\" class=\"customization-form__title-header-name\"></div>\n" +
    "            <div class=\"customization-form__title-header-badge\">\n" +
    "                <span class=\"badge\" ng-class=\"{'status-active':'badge-success', 'status-inactive':'', 'status-ignore':''}[getActivityStateForCustomization(customization)]\">\n" +
    "                    {{'personalization.modal.customizationvariationmanagement.targetgrouptab.customization.' + customization.status | lowercase | translate}}\n" +
    "\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-if=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"customization-form__title-dates\">\n" +
    "            <span ng-if='!customization.enabledStartDate && customization.enabledEndDate'>...</span>\n" +
    "            <span ng-bind=\"customization.enabledStartDate\"></span>\n" +
    "            <span ng-if='customization.enabledStartDate || customization.enabledEndDate'> - </span>\n" +
    "            <span ng-bind=\"customization.enabledEndDate\"></span>\n" +
    "            <span ng-if=\"isEndDateInThePast\" class=\"section-help help-inline help-inline--section help-inline--tooltip\">\n" +
    "                <span class=\"hyicon hyicon-msgwarning hyicon-msgwarning--dates help-icon-inline\"></span>\n" +
    "                <span class=\"help-block-inline help-block-inline--text \" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.datetooltip\"></span>\n" +
    "            </span>\n" +
    "            <span ng-if='customization.enabledStartDate && !customization.enabledEndDate'>...</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"customization-form__navtab-forms\">\n" +
    "        <div class=\"customization-form__navtab-forms--add-btn\">\n" +
    "            <button class=\"y-add-btn\" type=\"button\" data-ng-click=\"$ctrl.showSliderPanel()\">\n" +
    "                <span class=\"hyicon hyicon-add\"></span>\n" +
    "                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.addtargetgroup.button\"></span>\n" +
    "            </button>\n" +
    "        </div>\n" +
    "        <ul ng-show='filteredVariations.length > 0' class=\"customization-form__list-group\">\n" +
    "            <li class=\"customization-form__list-group__item\" data-ng-repeat=\"variation in customization.variations | statusNotDeleted as filteredVariations\">\n" +
    "                <div class=\"customization-form__variation-data\">\n" +
    "                    <a class=\"navtab-row__item__variation\" ng-bind=\"variation.name\" ng-click=\"editVariationAction(variation)\"></a>\n" +
    "                    <span ng-class=\"getActivityStateForVariation(customization, variation)\" ng-bind=\"getEnablementTextForVariation(variation)\"></span>\n" +
    "                    <div data-ng-show=\"isDefaultVariation(variation)\">\n" +
    "                        <span class=\"targetgroup-list__title\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.colon\"></span>\n" +
    "                        <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.default\"></span>\n" +
    "                    </div>\n" +
    "                    <div data-ng-show=\"!isDefaultVariation(variation)\">\n" +
    "                        <div>\n" +
    "                            <span class=\"targetgroup-list__title\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.colon\"></span>\n" +
    "                            <personalizationsmartedit-segment-expression-as-html data-segment-expression=\"variation.triggers\"></personalizationsmartedit-segment-expression-as-html>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"customization-form__variation-menu dropdown\">\n" +
    "                    <button type=\"button\" class=\"dropdown-toggle btn btn-link segment-contextual-menu__button dropdown-toggle\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n" +
    "                        <span class=\"hyicon hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"editVariationAction(variation)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.edit\" />\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"toogleVariationActive(variation)\" ng-bind=\"getActivityActionTextForVariation(variation)\" />\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                            <a ng-click=\"setVariationRank(variation, -1, $event, $first)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.moveup\" />\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                            <a ng-click=\"setVariationRank(variation, 1, $event, $last)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.movedown\" />\n" +
    "                        </li>\n" +
    "                        <li role=\"separator\" class=\"divider\"></li>\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"removeVariationClick(variation)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.remove\" />\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "\n" +
    "    <y-slider-panel data-slider-panel-show=\"$ctrl.showSliderPanel\" data-slider-panel-hide=\"$ctrl.hideSliderPanel\" data-slider-panel-configuration=\"$ctrl.sliderPanelConfiguration\" class=\"customization-form__sliderpanel\">\n" +
    "        <content>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"pull-right\">\n" +
    "                    <button class=\"btn btn-link perso-sliderpanel__btn-link\" data-ng-init=\"isFullscreen=false\" data-ng-click=\"toggleSliderFullscreen(); isFullscreen=!isFullscreen\">\n" +
    "                        <div>\n" +
    "                            <div data-ng-if=\"!isFullscreen\">\n" +
    "                                <span class=\"hyicon hyicon-maximize perso-sliderpanel__icon\"></span>\n" +
    "                                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.fullscreen.open\"></span>\n" +
    "                            </div>\n" +
    "                            <div data-ng-if=\"isFullscreen\">\n" +
    "                                <span class=\"hyicon hyicon-minimize perso-sliderpanel__icon\"></span>\n" +
    "                                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.fullscreen.close\"></span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"targetgroup-name\" class=\"control-label required\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupname\"></label>\n" +
    "                    <input uniquetargetgroupname type=\"text\" class=\"form-control\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupname.placeholder' | translate}}\" name=\"variationname_key\" data-ng-model=\"edit.name\" id=\"targetgroup-name\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"checkbox\">\n" +
    "                        <input type=\"checkbox\" id=\"targetgroup-isDefault-001\" ng-model=\"edit.isDefault\" ng-change=\"confirmDefaultTrigger(edit.isDefault);\" />\n" +
    "                        <label for=\"targetgroup-isDefault-001\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.default\"></label>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <span ng-show=\"{{tab.formName}}.variationname_key.$error.uniquetargetgroupname\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroup.uniquename.validation.message\"></span>\n" +
    "\n" +
    "                <div ng-show=\"edit.showExpression\">\n" +
    "                    <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupexpression\"></label>\n" +
    "                    <multi-segment-view data-triggers='edit.selectedVariation.triggers' data-expression='edit.expression'></multi-segment-view>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </content>\n" +
    "    </y-slider-panel>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/personalizationsmarteditSegmentNodeTemplate.html',
    "<div data-ng-class=\"{'perso-tree__segments-layout': ctrl.isContainer(node), 'perso-tree__empty-container': ctrl.isContainerWithDropzone(node), 'perso-tree__collapsed-container': collapsed }\">\n" +
    "    <div class=\"perso-tree\" data-ng-class=\"{'perso-tree__node': ctrl.isItem(node), 'perso-tree__container': ctrl.isContainer(node)}\">\n" +
    "        <div data-ng-if=\"ctrl.isContainer(node)\">\n" +
    "            <div class=\"perso-tree__toggle\" data-ng-click=\"ctrl.toggle(this)\" title=\"{{collapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span class=\"glyphicon perso-tree__glyphicon\" ng-class=\"{'glyphicon-chevron-right': collapsed, 'glyphicon-chevron-down': !collapsed}\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"perso-tree__dropdown\">\n" +
    "                <ui-select ng-model=\"node.operation\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "                    <ui-select-match>\n" +
    "                        <span ng-bind=\"$select.selected.name | translate\"></span>\n" +
    "                    </ui-select-match>\n" +
    "                    <ui-select-choices repeat=\"item in ctrl.actions\" position=\"down\">\n" +
    "                        <span ng-bind=\"item.name | translate\"></span>\n" +
    "                    </ui-select-choices>\n" +
    "                </ui-select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div data-ng-if=\"ctrl.isItem(node)\" ui-tree-handle class=\"perso-tree__node--content\">{{node.selectedSegment.code}}</div>\n" +
    "\n" +
    "        <div data-ng-if=\"ctrl.isDropzone(node)\" class=\"perso-tree__empty-container-node\">\n" +
    "            <div class=\"perso-tree__empty-container-node--text\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.dropzone\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div data-ng-if=\"ctrl.isContainer(node)\" ui-tree-handle class=\"perso-tree__angular-ui-tree-handle--empty\"></div>\n" +
    "        <div data-nodrag>\n" +
    "            <span class=\"pull-right\">\n" +
    "                <a data-ng-if=\"ctrl.isItem(node)\" class=\"perso-tree__actions perso-tree__node--icon\" data-ng-click=\"ctrl.duplicateItem(node)\" title=\"{{'personalization.commons.icon.title.duplicate' | translate}}\">\n" +
    "                    <span class=\"hyicon hyicon-duplicate\"></span>\n" +
    "                </a>\n" +
    "                <a data-ng-if=\"!ctrl.isTopContainer(this)\" class=\"perso-tree__actions\" data-ng-click=\"ctrl.removeItem(this)\">\n" +
    "                    <div data-ng-if=\"ctrl.isContainer(node)\" class=\"btn btn-link perso-tree__container--btn-icon\" title=\"{{'personalization.commons.icon.title.remove' | translate}}\">\n" +
    "                        <span class=\"hyicon hyicon-removelg\"></span>\n" +
    "                    </div>\n" +
    "                    <div data-ng-if=\"ctrl.isItem(node)\" class=\"perso-tree__node--icon\" title=\"{{'personalization.commons.icon.title.remove' | translate}}\">\n" +
    "                        <span class=\"hyicon hyicon-removelg\"></span>\n" +
    "                    </div>\n" +
    "                </a>\n" +
    "            </span>\n" +
    "            <a data-ng-if=\"ctrl.isContainer(node)\" class=\"btn btn-link perso-tree__btn\" data-ng-click=\"ctrl.newSubItem(this, 'container')\">\n" +
    "                <span class=\"perso-sliderpanel__btn-link\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.group.button\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ol data-ng-if=\"ctrl.isItem(node)\" ui-tree-nodes data-nodrop-enabled=\"true\" data-ng-model=\"node.nodes\" data-ng-class=\"{hidden: collapsed}\">\n" +
    "        <li data-ng-repeat=\"node in node.nodes\" ui-tree-node data-collapsed=\"true\" data-expand-on-hover=\"500\" data-ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\">\n" +
    "        </li>\n" +
    "    </ol>\n" +
    "    <ol data-ng-if=\"ctrl.isContainer(node)\" ui-tree-nodes data-ng-model=\"node.nodes\" data-ng-class=\"{hidden: collapsed}\">\n" +
    "        <li data-ng-repeat=\"node in node.nodes\" ui-tree-node data-collapsed=\"true\" data-expand-on-hover=\"500\" data-ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\">\n" +
    "        </li>\n" +
    "    </ol>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/personalizationsmarteditSegmentViewTemplate.html',
    "<div>\n" +
    "    <personalizationsmartedit-scroll-zone data-scroll-zone-visible=\"ctrl.scrollZoneVisible\" data-get-element-to-scroll=\"ctrl.getElementToScroll()\"></personalizationsmartedit-scroll-zone>\n" +
    "    <div class=\"form-group\">\n" +
    "        <personalizationsmartedit-segment-expression-as-html data-segment-expression=\"ctrl.expression[0]\"></personalizationsmartedit-segment-expression-as-html>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments\"></label>\n" +
    "        <ui-select class=\"form-control\" ng-model=\"ctrl.singleSegment\" theme=\"select2\" ng-keyup=\"ctrl.segmentSearchInputKeypress($event, $select.search)\" on-select=\"ctrl.segmentSelectedEvent($item, $select.items)\" reset-search-input=\"false\">\n" +
    "            <ui-select-match placeholder=\"{{ 'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.placeholder' | translate}}\">\n" +
    "                <span>{{'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.placeholder' | translate}}</span>\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"item in ctrl.segments\" position=\"down\" personalization-infinite-scroll=\"ctrl.addMoreSegmentItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "                <span ng-bind=\"item.code\"></span>\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ui-tree=\"ctrl.treeOptions\" id=\"tree-root\">\n" +
    "        <div ui-tree-nodes ng-model=\"ctrl.expression\" data-nodrop-enabled=\"true\">\n" +
    "            <div ng-repeat=\"node in ctrl.expression\" data-collapsed=\"false\" ui-tree-node ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/segmentExpressionAsHtml/personalizationsmarteditSegmentExpressionAsHtmlTemplate.html',
    "<span ng-repeat=\"word in ctrl.getExpressionAsArray() track by $index\">\n" +
    "    <span ng-if=\"ctrl.operators.indexOf(word) > -1\" class=\"perso-sliderpanel__expression-text\"> {{ ctrl.getLocalizationKeyForOperator(word) | translate }} </span>\n" +
    "    <span ng-if=\"ctrl.emptyGroup === word\" class=\"text-primary hyicon hyicon-caution\" data-uib-tooltip=\"{{'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.group.tooltip' | translate}}\" data-tooltip-placement=\"auto top\"></span>\n" +
    "    <span ng-if=\"ctrl.emptyGroupAndOperators.indexOf(word) === -1\">{{word}}</span>\n" +
    "</span>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/pageCustomizationsList/pageCustomizationsListTemplate.html',
    "<div class=\"categoryTable\">\n" +
    "    <div data-ng-repeat=\"customization in ctrl.customizationsList | orderBy: 'rank'\" data-ng-init=\"ctrl.initCustomization(customization)\">\n" +
    "        <div class=\"row row-lefttoolbar\" ng-class=\"$last && customization.collapsed ? 'customization--last':''\">\n" +
    "            <div ng-class=\"{'custFromLibExtraStyling customization-from-library':customization.fromLibrary, 'customization-rank-{{customization.rank}}':true}\">\n" +
    "                <div class=\"pull-left col-lefttoolbar-glyphicon\" data-ng-click=\"ctrl.clearAllSubMenu(); customization.collapsed = !customization.collapsed; ctrl.customizationClick(customization);\">\n" +
    "                    <a class=\"btn btn-link category-toggle\" title=\"{{customization.collapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                        <span data-ng-class=\"customization.collapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon lefttoolbar--glyphicon-chevron\"></span>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-9 col-reset-padding text-left\" ng-click=\"ctrl.clearAllSubMenu(); customization.collapsed = !customization.collapsed; ctrl.customizationClick(customization)\">\n" +
    "                    <span class=\"perso-list__primary-data\" ng-bind=\"customization.name\"></span>\n" +
    "                    <div>\n" +
    "                        <span class=\"perso-list__status--value\" ng-class=\"ctrl.getActivityStateForCustomization(customization)\" ng-bind=\"ctrl.getEnablementTextForCustomization(customization)\"></span>\n" +
    "                        <span class=\"perso-list__dates-layout\" ng-if=\"ctrl.isEnabled(customization)\" ng-bind=\"ctrl.getDatesForCustomization(customization)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-right dropdown\" ng-init=\"ctrl.clearAllSubMenu();\">\n" +
    "                    <button type=\"button\" class=\"btn btn-link dropdown-toggle customization-rank-{{customization.rank}}-dropdown-toggle\" data-toggle=\"dropdown\" ng-click=\"ctrl.customizationSubMenuAction(customization)\">\n" +
    "                        <span class=\"hyicon hyicon-lefttoolbar hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul ng-if=\"customization.subMenu\" class=\"dropdown-menu text-left dropdown-menu-leftoolbar\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a class=\"dropdown-menu-single-item cutomization-rank-{{customization.rank}}-edit-button\" ng-click=\"ctrl.clearAllSubMenu(); ctrl.editCustomizationAction(customization);\" data-translate=\"personalization.toolbar.pagecustomizations.customization.options.edit \"></a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <!--end row desktopLayout-->\n" +
    "        <div data-uib-collapse=\"customization.collapsed\">\n" +
    "            <div data-ng-repeat=\"variation in customization.variations | statusNotDeleted\">\n" +
    "                <div class=\"row row-lefttoolbar\" data-ng-class=\"ctrl.getSelectedVariationClass(variation)\">\n" +
    "                    <div class=\"pull-left col-lefttoolbar-glyphicon\">\n" +
    "                        <div data-ng-class=\"ctrl.isCommerceCustomization(variation) ? 'commerce-customization__marker--icon':'customization__marker'\" data-uib-tooltip=\"{{ctrl.getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-9 col-reset-padding text-left\" data-ng-click=\"ctrl.clearAllSubMenu(); ctrl.variationClick(customization, variation);\">\n" +
    "                        <span data-ng-bind=\"variation.name\"></span>\n" +
    "                        <div class=\"perso-list__status--value\" data-ng-class=\"ctrl.getActivityStateForVariation(customization, variation)\" data-ng-bind=\"ctrl.getEnablementTextForVariation(variation)\"></div>\n" +
    "                        <div data-ng-class=\"ctrl.getSelectedVariationClass(variation)\"></div>\n" +
    "                    </div>\n" +
    "                    <div data-ng-show=\"ctrl.getSelectedVariationClass(variation)\" class=\"pull-right\">\n" +
    "                        <span class=\"hyicon hyicon-checkedlg perso-panel__hyicon-checkedlg\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end data repeat-->\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/personalizationsmarteditPageCustomizationsToolbarItemTemplate.html',
    "<div id=\"personalizationsmartedit-right-toolbar-item-template\" class=\"perso-panel\">\n" +
    "    <div>\n" +
    "        <div class=\"overflow-wrapper-lefttoolbar\" personalization-infinite-scroll=\"ctrl.addMoreCustomizationItems()\" personalization-infinite-scroll-distance=\"2\" data-customizations-loaded=\"{{ctrl.customizationsOnPage.length > 0}}\">\n" +
    "            <search-customization-from-lib data-library-customizations=\"ctrl.libraryCustomizations\" data-add-more-library-customization-items=\"ctrl.addMoreLibraryCustomizationItems(searchObj)\" data-add-customization-from-library=\"ctrl.addCustomizationFromLibrary(selFromLib)\" data-is-menu-open=\"ctrl.isMenuOpen\" />\n" +
    "\n" +
    "            <statuses-dropdown data-set-selected-statuses=\"ctrl.setSelectedStatuses(value)\" data-is-customizations-empty=\"ctrl.isCustomizationsEmpty()\" data-clear-customize-context=\"ctrl.clearCustomizeContext()\" />\n" +
    "\n" +
    "            <page-customizations-list data-customizations-list=\"ctrl.customizationsList\" />\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/personalizationsmarteditPageCustomizationsToolbarItemWrapperTemplate.html',
    "<div data-ng-controller=\"topToolbarMenuController\" data-uib-dropdown data-is-open=\"item.isOpen\" class=\"btn-group pe-toolbar-action pe-combined-view-menu\" data-auto-close=\"outsideClick\">\n" +
    "    <button type=\"button\" class=\"btn btn-default pe-toolbar-action--button personalizationsmarteditTopToolbarCustomizeButton\" data-uib-dropdown-toggle data-ng-disabled=\"disabled\" aria-pressed=\"false\" data-ng-class=\"{'pe-toolbar-action-customize--button-with-context': isCustomizeCustomizationSelected()}\">\n" +
    "        <span class=\"hyicon hyicon-customize pe-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <span class=\"pe-toolbar-action--button--txt\" data-translate=\"personalization.toolbar.pagecustomizations\"></span>\n" +
    "    </button>\n" +
    "\n" +
    "    <personalizationsmartedit-customize-toolbar-context></personalizationsmartedit-customize-toolbar-context>\n" +
    "\n" +
    "    <div data-uib-dropdown-menu class=\"btn-block pe-toolbar-action--include\">\n" +
    "        <div class=\"pe-toolbar-menu-content se-toolbar-menu-content--pe-customized\" role=\"menu\">\n" +
    "            <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "                <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.pagecustomizations.header.title\"></h2>\n" +
    "                <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.pagecustomizations.header.description\"></small>\n" +
    "            </div>\n" +
    "            <div role=\"menuitem\" class=\"pe-combined-view-menu\">\n" +
    "                <personalizationsmartedit-page-customizations-toolbar-item data-is-menu-open=\"item.isOpen\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/searchCustomizationFromLib/searchCustomizationFromLibTemplate.html',
    "<div data-ng-show=\"!ctrl.searchCustomizationEnabled\">\n" +
    "    <button class=\"y-add y-add-btn\" type=\"button\" data-ng-click=\"ctrl.toggleAddMoreCustomizationsClick()\">\n" +
    "        <span class=\"hyicon hyicon-add\" data-translate=\"personalization.toolbar.pagecustomizations.addmorecustomizations.button\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "<div class=\"text-left search-item-lefttoolbar\" data-ng-show=\"ctrl.searchCustomizationEnabled\">\n" +
    "    <ui-select id=\"dropdownCustomizationsLibrary\" multiple='true' class=\"form-control\" ng-model=\"ctrl.selectedFromDropdownLibraryCustomizations\" theme=\"select2\" ng-disabled=\"disabled\" on-select=\"ctrl.addCustomizationFromLibrary({selFromLib: $item});ctrl.selectedFromDropdownLibraryCustomizations = [];ctrl.toggleAddMoreCustomizationsClick();\" ng-keyup=\"ctrl.customizationSearchInputKeypress($event, $select.search)\" uis-open-close=\"ctrl.libraryCustomizationsDropdownOpenClose(isOpen)\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.toolbar.pagecustomizations.addmorecustomizations.customization.library.search.placeholder' | translate}}\">\n" +
    "            {{$item.name}}\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in ctrl.libraryCustomizations\" personalization-infinite-scroll=\"ctrl.addMoreLibraryCustomizationItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "            <div ng-html-bind=\"item.name | highlight: $select.search\"></div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-9\" ng-bind=\"item.name\"></div>\n" +
    "                <div class=\"col-xs-3 perso-list__status--value\">\n" +
    "                    <small ng-class=\"ctrl.getActivityStateForCustomization(item)\" ng-bind=\"ctrl.getEnablementTextForCustomization(item)\"></small>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/statusesDropdown/statusesDropdownTemplate.html',
    "<div class=\"perso__filter-layout customizationsStatusDropdown\" ng-class=\"ctrl.isCustomizationsEmpty() ? 'perso__filter-layout--no-results':''\">\n" +
    "    <ui-select id=\"dropdownCustomizationsStatus\" ng-model=\"ctrl.selectedStatus\" theme=\"select2\" title=\"\" search-enabled=\"false\" on-select=\"ctrl.setSelectedStatuses({value: ctrl.selectedStatus.modelStatuses || []});ctrl.clearCustomizeContext();\" uis-open-close=\"ctrl.statusCustomizationsDropdownOpenClose(isOpen)\">\n" +
    "        <ui-select-match>\n" +
    "            <span ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in ctrl.statuses\" position=\"down\">\n" +
    "            <span ng-bind=\"item.text | translate\"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/toolbarContext/personalizationsmarteditToolbarContextTemplate.html',
    "<div data-ng-show=\"ctrl.visible\" class=\"pe-toolbar-item-context\">\n" +
    "    <span class=\"btn btn-default pe-toolbar-item-context__btn\">\n" +
    "        <div class=\"pe-toolbar-item-context__btn--txt\">\n" +
    "            <div class=\"pe-toolbar-item-context__btn--title\">{{ctrl.title}}</div>\n" +
    "            <div class=\"pe-toolbar-item-context__btn--subtitle\">{{ctrl.subtitle}}</div>\n" +
    "        </div>\n" +
    "    </span>\n" +
    "    <span class=\"btn btn-default pe-toolbar-item-context__btn--hyicon\" data-ng-click=\"ctrl.clear()\">\n" +
    "        <div class=\" pe-toolbar-item-context__btn--txt hyicon hyicon-remove\">\n" +
    "        </div>\n" +
    "    </span>\n" +
    "</div>"
  );

}]);
