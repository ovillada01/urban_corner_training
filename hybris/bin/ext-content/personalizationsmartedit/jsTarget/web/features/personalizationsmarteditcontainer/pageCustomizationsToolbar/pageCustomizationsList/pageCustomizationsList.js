angular.module('pageCustomizationsListModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditPreviewServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .controller('pageCustomizationsListController', ['$timeout', '$filter', 'personalizationsmarteditContextService', 'personalizationsmarteditRestService', 'personalizationsmarteditCommerceCustomizationService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditUtils', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditDateUtils', 'personalizationsmarteditContextUtils', 'personalizationsmarteditPreviewService', 'personalizationsmarteditManager', function($timeout, $filter, personalizationsmarteditContextService, personalizationsmarteditRestService, personalizationsmarteditCommerceCustomizationService, personalizationsmarteditMessageHandler, personalizationsmarteditUtils, personalizationsmarteditIFrameUtils, personalizationsmarteditDateUtils, personalizationsmarteditContextUtils, personalizationsmarteditPreviewService, personalizationsmarteditManager) {
        var self = this;

        //Private methods
        var updateCustomizationData = function(customization) {
            personalizationsmarteditRestService.getVariationsForCustomization(customization.code).then(
                function successCallback(response) {
                    customization.variations = response.variations || [];
                    customization.variations.forEach(function(variation) {
                        variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                        variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                    });
                },
                function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });
        };

        var getVisibleVariations = function(customization) {
            return personalizationsmarteditUtils.getVisibleItems(customization.variations);
        };

        var getAndSetComponentsForVariation = function(customizationId, variationId) {
            personalizationsmarteditRestService.getComponenentsIdsForVariation(customizationId, variationId).then(function successCallback(response) {
                var customize = personalizationsmarteditContextService.getCustomize();
                customize.selectedComponents = response.components;
                personalizationsmarteditContextService.setCustomize(customize);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponentsforvariation'));
            });
        };

        var updatePreviewTicket = function(customizationId, variationArray) {
            var previewTicketId = personalizationsmarteditContextService.getSeData().sePreviewData.previewTicketId;
            var variationKeys = personalizationsmarteditUtils.getVariationKey(customizationId, variationArray);
            personalizationsmarteditPreviewService.updatePreviewTicketWithVariations(previewTicketId, variationKeys).then(function successCallback() {
                var previewData = personalizationsmarteditContextService.getSeData().sePreviewData;
                personalizationsmarteditIFrameUtils.reloadPreview(previewData.resourcePath, previewData.previewTicketId);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingpreviewticket'));
            });
        };

        //Properties
        this.customizationEdited = false;

        //Methods
        this.initCustomization = function(customization) {
            customization.collapsed = true;
            if ((personalizationsmarteditContextService.getCustomize().selectedCustomization || {}).code === customization.code) {
                customization.collapsed = false;
            }
        };

        this.editCustomizationAction = function(customization) {
            self.customizationEdited = true;
            personalizationsmarteditContextUtils.clearCombinedViewContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            personalizationsmarteditManager.openEditCustomizationModal(customization.code);
        };

        this.customizationClick = function(customization) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            var currentVariations = personalizationsmarteditContextService.getCustomize().selectedVariations;
            updateCustomizationData(customization);
            var visibleVariations = getVisibleVariations(customization);
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = customization;
            customize.selectedVariations = visibleVariations;
            personalizationsmarteditContextService.setCustomize(customize);
            if (visibleVariations.length > 0) {
                var allVariations = personalizationsmarteditUtils.getVariationCodes(visibleVariations).join(",");
                getAndSetComponentsForVariation(customization.code, allVariations);
            }

            if ((angular.isObject(currentVariations) && !angular.isArray(currentVariations)) || combinedView.enabled) {
                updatePreviewTicket();
            }

            combinedView.enabled = false;
            personalizationsmarteditContextService.setCombinedView(combinedView);

            self.customizationsList.filter(function(cust) {
                return customization.name !== cust.name;
            }).forEach(function(cust, index) {
                cust.collapsed = true;
            });
        };

        this.getSelectedVariationClass = function(variation) {
            if (angular.equals(variation.code, (personalizationsmarteditContextService.getCustomize().selectedVariations || {}).code)) {
                return "selectedVariation";
            }
        };

        this.variationClick = function(customization, variation) {
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = customization;
            customize.selectedVariations = variation;
            personalizationsmarteditContextService.setCustomize(customize);
            getAndSetComponentsForVariation(customization.code, variation.code);
            updatePreviewTicket(customization.code, [variation]);
        };

        this.isCommerceCustomization = function(variation) {
            return variation.numberOfCommerceActions > 0;
        };

        this.getCommerceCustomizationTooltip = personalizationsmarteditUtils.getCommerceCustomizationTooltip;

        this.clearAllSubMenu = function() {
            angular.forEach(self.customizationsList, function(customization) {
                customization.subMenu = false;
            });
        };

        this.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        this.getActivityStateForVariation = function(customization, variation) {
            return personalizationsmarteditUtils.getActivityStateForVariation(customization, variation);
        };

        this.getEnablementTextForCustomization = function(customization) {
            return personalizationsmarteditUtils.getEnablementTextForCustomization(customization, 'personalization.toolbar.pagecustomizations');
        };

        this.getEnablementTextForVariation = function(variation) {
            return personalizationsmarteditUtils.getEnablementTextForVariation(variation, 'personalization.toolbar.pagecustomizations');
        };

        this.isEnabled = function(item) {
            return personalizationsmarteditUtils.isPersonalizationItemEnabled(item);
        };

        this.getDatesForCustomization = function(customization) {
            var activityStr = "";
            var startDateStr = "";
            var endDateStr = "";

            if (customization.enabledStartDate || customization.enabledEndDate) {
                startDateStr = personalizationsmarteditDateUtils.formatDateWithMessage(customization.enabledStartDate);
                endDateStr = personalizationsmarteditDateUtils.formatDateWithMessage(customization.enabledEndDate);
                if (!customization.enabledStartDate) {
                    startDateStr = " ...";
                }
                if (!customization.enabledEndDate) {
                    endDateStr = "... ";
                }
                activityStr += " (" + startDateStr + " - " + endDateStr + ") ";
            }
            return activityStr;
        };

        this.customizationSubMenuAction = function(customization) {
            if (!customization.subMenu) {
                self.clearAllSubMenu();
            }
            customization.subMenu = !customization.subMenu;
        };
    }])
    .component('pageCustomizationsList', {
        templateUrl: 'pageCustomizationsListTemplate.html',
        controller: 'pageCustomizationsListController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            customizationsList: '<'
        }
    });
