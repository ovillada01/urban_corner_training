angular.module('personalizationcommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationcommons/dateTimePickerRange/dateTimePickerRangeTemplate.html',
    "<div class=\"pe-datetime-range\">\n" +
    "    <div class=\"col-md-6 pe-datetime-range__from\" data-ng-class=\"{'has-error': !isFromDateValid}\">\n" +
    "        <label for=\"customization-start-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.startdate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateFrom\" id=\"customization-start-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateFrom\" data-date-formatter id=\"customization-start-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span ng-if=\"!isFromDateValid\" class=\"help-block pe-datetime__error-msg pe-datetime__msg\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformatfrom.description\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-6 pe-datetime-range__to\" data-ng-class=\"{'has-error':!isToDateValid, 'has-warning':isEndDateInThePast}\">\n" +
    "        <label for=\"customization-end-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateTo\" id=\"customization-end-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateTo\" data-date-formatter id=\"customization-end-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span class=\"help-block pe-datetime__error-msg pe-datetime__msg\" ng-if=\"!isToDateValid\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformat.description\">\n" +
    "        </span>\n" +
    "        <span ng-if=\"isEndDateInThePast\" class=\"help-block pe-datetime__warning-msg pe-datetime__msg\">\n" +
    "            <span class=\"hyicon hyicon-msgwarning\"></span>\n" +
    "            <span data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddateinthepast.description\"></span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditPagination/personalizationsmarteditPaginationTemplate.html',
    "<div class=\"row\">\n" +
    "    <div class=\"col-xs-4\"></div>\n" +
    "    <div class=\"col-xs-4\">\n" +
    "        <ul class=\"pagination pagination-lg\">\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"leftClick()\" ng-class=\"hasPrevious()?'enabled':'disabled'\"><a>&laquo;</a></li>\n" +
    "            <li class=\"no-underline\" ng-repeat=\"i in pagesToDisplay()\" ng-class=\"isActive({{i}})?'active':''\"><a ng-click=\"pageClick(i)\">{{i+1}}</a></li>\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"rightClick()\" ng-class=\"hasNext()?'enabled':'disabled'\"><a>&raquo;</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-2\"></div>\n" +
    "    <div class=\"col-xs-2\" ng-if=\"!isFixedPageSize()\">\n" +
    "        <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "            <span ng-bind=\"getCurrentPageSize()\"></span>\n" +
    "            <span data-translate=\"personalization.commons.pagination.rowsperpage\"></span>\n" +
    "            <span class=\"list-arrow hyicon hyicon-arrow\"></span>\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "            <li ng-repeat=\"i in availablePageSizes() track by $index\"><a ng-click=\"pageSizeClick(i)\">{{i}}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneBottomTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--bottom\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneBottom\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollBottom()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneTopTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--top\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneTop\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollTop()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );

}]);
