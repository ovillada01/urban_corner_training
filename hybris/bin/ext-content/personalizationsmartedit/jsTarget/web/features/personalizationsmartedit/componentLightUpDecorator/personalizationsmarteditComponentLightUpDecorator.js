angular.module('personalizationsmarteditComponentLightUpDecorator', [
        'yjqueryModule',
        'personalizationsmarteditTemplates',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditCommons'
    ])
    .directive('personalizationsmarteditComponentLightUp', ['yjQuery', 'personalizationsmarteditContextService', 'personalizationsmarteditUtils', function(yjQuery, personalizationsmarteditContextService, personalizationsmarteditUtils) {
        return {
            templateUrl: 'personalizationsmarteditComponentLightUpDecoratorTemplate.html',
            restrict: 'C',
            transclude: true,
            replace: false,
            scope: {
                smarteditComponentId: '@',
                smarteditComponentType: '@'
            },
            link: function($scope, element, attrs) {

                var isComponentSelected = function() {
                    var elementSelected = false;
                    if (angular.isArray(personalizationsmarteditContextService.getCustomize().selectedVariations)) {
                        var containerId = personalizationsmarteditUtils.getContainerIdForElement(element);
                        elementSelected = yjQuery.inArray(containerId, personalizationsmarteditContextService.getCustomize().selectedComponents) > -1;
                    }
                    return elementSelected;
                };

                var isVariationComponentSelected = function() {
                    var elementSelected = false;
                    var customize = personalizationsmarteditContextService.getCustomize();
                    if (customize.selectedCustomization && customize.selectedVariations) {
                        var container = element.parent().closest('[class~="smartEditComponentX"][data-smartedit-container-type="CxCmsComponentContainer"][data-smartedit-personalization-action-id]');
                        elementSelected = container.length > 0;
                    }
                    return elementSelected;
                };

                $scope.getPersonalizationComponentBorderClass = function() {
                    var container = element.parent().closest('[class~="smartEditComponentX"][data-smartedit-container-id][data-smartedit-container-type="CxCmsComponentContainer"]');
                    container.toggleClass("personalizationsmarteditVariationComponentSelected", isVariationComponentSelected());
                    container.toggleClass("personalizationsmarteditVariationComponentSelected-icon", isVariationComponentSelected());
                    container.toggleClass("personalizationsmarteditComponentSelected", isComponentSelected());
                };
            }
        };
    }]);
