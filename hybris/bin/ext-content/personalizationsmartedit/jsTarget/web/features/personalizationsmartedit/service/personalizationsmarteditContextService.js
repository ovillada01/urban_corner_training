angular.module('personalizationsmarteditContextServiceModule', [
        'yjqueryModule',
        'personalizationsmarteditCommons',
        'contextualMenuServiceModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .factory('personalizationsmarteditContextService', ['yjQuery', 'personalizationsmarteditContextServiceReverseProxy', 'contextualMenuService', 'personalizationsmarteditContextUtils', function(yjQuery, personalizationsmarteditContextServiceReverseProxy, contextualMenuService, personalizationsmarteditContextUtils) {

        /*
         * Usage
         * When using ContextService objects do not use properties directly,
         * always use getter(to retrieve object) and setter(to set object),
         * so all synchronization functions which are in getters and setters are called.
         * Example:
         * var examplePersonalization = ContextService.getPersonalization();
         * examplePersonalization.enabled = true;
         * examplePersonalization.myComponents = ["component1", "component2"];
         * ContextService.setPersonalization(examplePersonalization);
         */

        var ContextService = personalizationsmarteditContextUtils.getContextObject();
        var ContextServiceReverseProxy = new personalizationsmarteditContextServiceReverseProxy('PersonalizationCtxReverseGateway');

        var isCustomizeObjectValid = function(customize) {
            return angular.isObject(customize.selectedCustomization) && angular.isObject(customize.selectedVariations) && !angular.isArray(customize.selectedVariations);
        };

        var isContextualMenuEnabled = function() {
            return isCustomizeObjectValid(ContextService.customize) || (ContextService.combinedView.enabled && isCustomizeObjectValid(ContextService.combinedView.customize));
        };

        var isElementHighlighted = function(config) {
            if (ContextService.combinedView.enabled) {
                return yjQuery.inArray(config.containerId, ContextService.combinedView.customize.selectedComponents) > -1;
            } else {
                return yjQuery.inArray(config.containerId, ContextService.customize.selectedComponents) > -1;
            }
        };

        var isComponentInCurrentCatalog = function(configProperties) {
            var experienceCV = ContextService.getSeData().seExperienceData.catalogDescriptor.catalogVersionUuid;
            var componentCV = configProperties.smarteditCatalogVersionUuid;
            return experienceCV === componentCV;
        };

        ContextService.getPersonalization = function() {
            return ContextService.personalization;
        };
        ContextService.setPersonalization = function(personalization) {
            ContextService.personalization = personalization;
            contextualMenuService.refreshMenuItems();
        };

        ContextService.getCustomize = function() {
            return ContextService.customize;
        };
        ContextService.setCustomize = function(customize) {
            ContextService.customize = customize;
            contextualMenuService.refreshMenuItems();
        };

        ContextService.getCombinedView = function() {
            return ContextService.combinedView;
        };
        ContextService.setCombinedView = function(combinedView) {
            ContextService.combinedView = combinedView;
            contextualMenuService.refreshMenuItems();
        };

        ContextService.getSeData = function() {
            return ContextService.seData;
        };
        ContextService.setSeData = function(seData) {
            ContextService.seData = seData;
        };

        ContextService.isContextualMenuAddItemEnabled = function(config) {
            var configProperties = angular.fromJson(config.properties);
            return isContextualMenuEnabled() && (!isElementHighlighted(config)) && isComponentInCurrentCatalog(configProperties);
        };

        ContextService.isContextualMenuEditItemEnabled = function(config) {
            var isEnabled = ContextService.personalization.enabled;
            var configProperties = angular.fromJson(config.properties);
            isEnabled = isEnabled && angular.isDefined(configProperties.smarteditPersonalizationActionId) && isComponentInCurrentCatalog(configProperties);
            return isEnabled;
        };

        ContextService.isContextualMenuDeleteItemEnabled = ContextService.isContextualMenuEditItemEnabled;

        ContextService.isContextualMenuShowActionListEnabled = function(config) {
            var isEnabled = ContextService.isContextualMenuEditItemEnabled(config);
            isEnabled = isEnabled && ContextService.combinedView.enabled;
            isEnabled = isEnabled && !ContextService.combinedView.customize.selectedCustomization;
            return isEnabled;
        };

        ContextService.isContextualMenuInfoItemEnabled = function(element) {
            var isEnabled = ContextService.personalization.enabled;
            isEnabled = isEnabled && !angular.isObject(ContextService.customize.selectedVariations);
            isEnabled = isEnabled || angular.isArray(ContextService.customize.selectedVariations);
            isEnabled = isEnabled && !ContextService.combinedView.enabled;

            return isEnabled;
        };

        ContextService.isContextualMenuEditComponentItemEnabled = function(config) {
            var isEnabled = ContextService.personalization.enabled;
            var configProperties = angular.fromJson(config.properties);
            isEnabled = isEnabled && !ContextService.combinedView.enabled && isComponentInCurrentCatalog(configProperties);
            return isEnabled;
        };

        ContextService.applySynchronization = function() {
            ContextServiceReverseProxy.applySynchronization();
        };

        ContextService.setPageId = function(newPageId) {
            ContextService.seData.pageId = newPageId;
            ContextServiceReverseProxy.setPageId(newPageId);
        };

        return ContextService;
    }])
    .factory('personalizationsmarteditContextServiceProxy', ['gatewayProxy', 'personalizationsmarteditContextService', function(gatewayProxy, personalizationsmarteditContextService) {
        var proxy = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };

        proxy.prototype.setPersonalization = function(newPersonalization) {
            personalizationsmarteditContextService.setPersonalization(newPersonalization);
        };
        proxy.prototype.setCustomize = function(newCustomize) {
            personalizationsmarteditContextService.setCustomize(newCustomize);
        };
        proxy.prototype.setCombinedView = function(newCombinedView) {
            personalizationsmarteditContextService.setCombinedView(newCombinedView);
        };
        proxy.prototype.setSeData = function(newSeData) {
            personalizationsmarteditContextService.setSeData(newSeData);
        };

        return proxy;
    }])
    .factory('personalizationsmarteditContextServiceReverseProxy', ['gatewayProxy', function(gatewayProxy) {
        var reverseProxy = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };
        reverseProxy.prototype.applySynchronization = function() {};
        reverseProxy.prototype.setPageId = function(newPageId) {};

        return reverseProxy;
    }]);
