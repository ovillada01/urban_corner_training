angular.module('personalizationsmarteditTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationsmartedit/combinedView/personalizationsmarteditCombinedViewComponentLightUpDecoratorTemplate.html',
    "<div ng-class=\"getPersonalizationComponentBorderClass()\">\n" +
    "    <div ng-class=\"classForElement\">{{letterForElement}}</div>\n" +
    "    <div data-ng-transclude></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmartedit/componentLightUpDecorator/personalizationsmarteditComponentLightUpDecoratorTemplate.html',
    "<div ng-class=\"getPersonalizationComponentBorderClass()\">\n" +
    "    <div data-ng-transclude></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmartedit/sharedSlotDecorator/personalizationsmarteditSharedSlotDecoratorTemplate.html',
    "<div>\n" +
    "    <div class=\"cmsx-ctx-wrapper1 se-slot-contextual-menu-level1\">\n" +
    "        <div class=\"cmsx-ctx-wrapper2 se-slot-contextual-menu-level2\">\n" +
    "            <div class=\"decorative-panel-area\" data-ng-if=\"ctrl.active && ctrl.slotSharedFlag\">\n" +
    "                <div class=\"decorator-panel-padding-center\"></div>\n" +
    "                <div class=\"decorative-panel-slot-contextual-menu\">\n" +
    "                    <slot-shared-button data-slot-id=\"{{::ctrl.smarteditComponentId}}\"></slot-shared-button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"decorator-padding-container\">\n" +
    "                <div class=\"decorator-padding-left\" data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div data-ng-class=\"ctrl.active && ctrl.slotSharedFlag ? 'decorator-slot-border active' : ''\"></div>\n" +
    "                <div class=\"yWrapperData\" data-ng-transclude data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "                <div class=\"decorator-padding-right\" data-ng-class=\"{active: ctrl.active}\"></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );

}]);
