NG_DOCS={
  "sections": {
    "personalizationsmartedit": "personalizationsmartedit",
    "personalizationsmarteditcontainer": "personalizationsmarteditcontainer"
  },
  "pages": [],
  "apis": {
    "personalizationsmartedit": true,
    "personalizationsmarteditcontainer": true
  },
  "html5Mode": false,
  "editExample": true,
  "startPage": "/#/personalizationsmartedit",
  "scripts": [
    "angular.min.js"
  ]
};