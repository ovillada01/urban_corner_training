angular.module('personalizationsmarteditCommons', [
        'yjqueryModule',
        'alertServiceModule',
        'personalizationcommonsTemplates',
        'languageServiceModule',
        'seConstantsModule'
    ])
    .constant('PERSONALIZATION_MODEL_STATUS_CODES', {
        ENABLED: 'ENABLED',
        DISABLED: 'DISABLED'
    })
    .constant('PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', {
        ALL: 'ALL',
        ENABLED: 'ENABLED',
        DISABLED: 'DISABLED'
    })
    .constant('PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING', {
        0: {
            borderClass: 'personalizationsmarteditComponentSelected0',
            listClass: 'personalizationsmarteditComponentSelectedList0'
        },
        1: {
            borderClass: 'personalizationsmarteditComponentSelected1',
            listClass: 'personalizationsmarteditComponentSelectedList1'
        },
        2: {
            borderClass: 'personalizationsmarteditComponentSelected2',
            listClass: 'personalizationsmarteditComponentSelectedList2'
        },
        3: {
            borderClass: 'personalizationsmarteditComponentSelected3',
            listClass: 'personalizationsmarteditComponentSelectedList3'
        },
        4: {
            borderClass: 'personalizationsmarteditComponentSelected4',
            listClass: 'personalizationsmarteditComponentSelectedList4'
        },
        5: {
            borderClass: 'personalizationsmarteditComponentSelected5',
            listClass: 'personalizationsmarteditComponentSelectedList5'
        },
        6: {
            borderClass: 'personalizationsmarteditComponentSelected6',
            listClass: 'personalizationsmarteditComponentSelectedList6'
        },
        7: {
            borderClass: 'personalizationsmarteditComponentSelected7',
            listClass: 'personalizationsmarteditComponentSelectedList7'
        },
        8: {
            borderClass: 'personalizationsmarteditComponentSelected8',
            listClass: 'personalizationsmarteditComponentSelectedList8'
        },
        9: {
            borderClass: 'personalizationsmarteditComponentSelected9',
            listClass: 'personalizationsmarteditComponentSelectedList9'
        },
        10: {
            borderClass: 'personalizationsmarteditComponentSelected10',
            listClass: 'personalizationsmarteditComponentSelectedList10'
        },
        11: {
            borderClass: 'personalizationsmarteditComponentSelected11',
            listClass: 'personalizationsmarteditComponentSelectedList11'
        },
        12: {
            borderClass: 'personalizationsmarteditComponentSelected12',
            listClass: 'personalizationsmarteditComponentSelectedList12'
        },
        13: {
            borderClass: 'personalizationsmarteditComponentSelected13',
            listClass: 'personalizationsmarteditComponentSelectedList13'
        },
        14: {
            borderClass: 'personalizationsmarteditComponentSelected14',
            listClass: 'personalizationsmarteditComponentSelectedList14'
        }
    })
    .run(['$rootScope', 'PERSONALIZATION_MODEL_STATUS_CODES', function($rootScope, PERSONALIZATION_MODEL_STATUS_CODES) {
        $rootScope.PERSONALIZATION_MODEL_STATUS_CODES = PERSONALIZATION_MODEL_STATUS_CODES;
    }])
    .filter('statusNotDeleted', ['personalizationsmarteditUtils', function(personalizationsmarteditUtils) {
        return function(value) {
            if (angular.isArray(value)) {
                return personalizationsmarteditUtils.getVisibleItems(value);
            }
            return value;
        };
    }])
    .factory('personalizationsmarteditUtils', ['$filter', 'PERSONALIZATION_MODEL_STATUS_CODES', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', 'PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING', function($filter, PERSONALIZATION_MODEL_STATUS_CODES, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING) {
        var utils = {};

        utils.pushToArrayIfValueExists = function(array, key, value) {
            if (value) {
                array.push({
                    "key": key,
                    "value": value
                });
            }
        };

        utils.getContainerIdForElement = function(element) {
            var container = element.closest('[data-smartedit-container-id][data-smartedit-container-type="CxCmsComponentContainer"]');
            if (container.length) {
                return container.data().smarteditContainerId;
            }
            return null;
        };

        utils.getContainerIdForComponent = function(componentType, componentId) {
            var element = angular.element('[data-smartedit-component-id="' + componentId + '"][data-smartedit-component-type="' + componentType + '"]');
            if (angular.isArray(element)) {
                element = element[0];
            }
            return utils.getContainerIdForElement(element);
        };

        utils.getSlotIdForElement = function(element) {
            var slot = element.closest('[data-smartedit-component-type="ContentSlot"]');
            if (slot.length) {
                return slot.data().smarteditComponentId;
            }
            return null;
        };

        utils.getSlotIdForComponent = function(componentType, componentId) {
            var element = angular.element('[data-smartedit-component-id="' + componentId + '"][data-smartedit-component-type="' + componentType + '"]');
            if (angular.isArray(element)) {
                element = element[0];
            }
            return utils.getSlotIdForElement(element);
        };

        utils.getVariationCodes = function(variations) {
            if ((typeof variations === 'undefined') || (variations === null)) {
                return [];
            }
            var allVariationsCodes = variations.map(function(elem) {
                return elem.code;
            }).filter(function(elem) {
                return typeof elem !== 'undefined';
            });
            return allVariationsCodes;
        };

        utils.getPageId = function() {
            return /page\-([\w]+)/.exec($('iframe').contents().find('body').attr('class'))[1];
        };

        utils.getVariationKey = function(customizationId, variations) {
            if (customizationId === undefined || variations === undefined) {
                return [];
            }

            var allVariationsKeys = variations.map(function(elem) {
                return elem.code;
            }).filter(function(elem) {
                return typeof elem !== 'undefined';
            }).map(function(variationId) {
                return {
                    "variationCode": variationId,
                    "customizationCode": customizationId
                };
            });
            return allVariationsKeys;
        };

        utils.getSegmentTriggerForVariation = function(variation) {
            var triggers = variation.triggers || [];
            var segmentTriggerArr = triggers.filter(function(trigger) {
                return trigger.type === "segmentTriggerData";
            });

            if (segmentTriggerArr.length === 0) {
                return {};
            }

            return segmentTriggerArr[0];
        };

        utils.isPersonalizationItemEnabled = function(item) {
            return item.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED;
        };

        utils.getEnablementTextForCustomization = function(customization, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";
            if (utils.isPersonalizationItemEnabled(customization)) {
                return $filter('translate')(keyPrefix + '.customization.enabled');
            } else {
                return $filter('translate')(keyPrefix + '.customization.disabled');
            }
        };

        utils.getEnablementTextForVariation = function(variation, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";

            if (utils.isPersonalizationItemEnabled(variation)) {
                return $filter('translate')(keyPrefix + '.variation.enabled');
            } else {
                return $filter('translate')(keyPrefix + '.variation.disabled');
            }
        };

        utils.getEnablementActionTextForVariation = function(variation, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";

            if (utils.isPersonalizationItemEnabled(variation)) {
                return $filter('translate')(keyPrefix + '.variation.options.disable');
            } else {
                return $filter('translate')(keyPrefix + '.variation.options.enable');
            }
        };

        utils.getActivityStateForCustomization = function(customization) {
            if (customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED) {
                if (moment().isBetween(new Date(customization.enabledStartDate), new Date(customization.enabledEndDate), 'minute', '[]')) {
                    return "status-active";
                } else {
                    return "status-ignore";
                }
            } else {
                return "status-inactive";
            }
        };

        utils.getActivityStateForVariation = function(customization, variation) {
            if (variation.enabled) {
                return utils.getActivityStateForCustomization(customization);
            } else {
                return "status-inactive";
            }
        };

        utils.isItemVisible = function(item) {
            return item.status !== 'DELETED';
        };

        utils.getVisibleItems = function(items) {
            return items.filter(function(item) {
                return utils.isItemVisible(item);
            });
        };

        utils.getValidRank = function(items, item, increaseValue) {
            var from = items.indexOf(item);
            var delta = increaseValue < 0 ? -1 : 1;

            var increase = from + increaseValue;

            while (increase >= 0 && increase < items.length && !utils.isItemVisible(items[increase])) {
                increase += delta;
            }

            increase = increase >= items.length ? items.length - 1 : increase;
            increase = increase < 0 ? 0 : increase;

            return items[increase].rank;
        };

        utils.getStatusesMapping = function() {
            var statusesMapping = [];

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL,
                text: 'personalization.context.status.all',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.ENABLED, PERSONALIZATION_MODEL_STATUS_CODES.DISABLED]
            });

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ENABLED,
                text: 'personalization.context.status.enabled',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.ENABLED]
            });

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.DISABLED,
                text: 'personalization.context.status.disabled',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.DISABLED]
            });

            return statusesMapping;
        };

        utils.getClassForElement = function(index) {
            var wrappedIndex = index % Object.keys(PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING).length;
            return PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING[wrappedIndex].listClass;
        };

        utils.getLetterForElement = function(index) {
            var wrappedIndex = index % Object.keys(PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING).length;
            return String.fromCharCode('a'.charCodeAt() + wrappedIndex).toUpperCase();
        };

        utils.getCommerceCustomizationTooltip = function(variation) {
            var result = "";
            angular.forEach(variation.commerceCustomizations, function(propertyValue, propertyKey) {
                result += $filter('translate')('personalization.modal.manager.commercecustomization.' + propertyKey) + ": " + propertyValue + "\n";
            });
            return result;
        };

        return utils;

    }]).factory('personalizationsmarteditMessageHandler', ['alertService', function(alertService) {

        var messageHandler = {};
        messageHandler.sendInformation = function(informationMessage) {
            alertService.showInfo(informationMessage);
        };

        messageHandler.sendError = function(errorMessage) {
            alertService.showDanger(errorMessage);
        };

        messageHandler.sendWarning = function(warningMessage) {
            alertService.showWarning(warningMessage);
        };

        messageHandler.sendSuccess = function(successMessage) {
            alertService.showSuccess(successMessage);
        };

        return messageHandler;

    }]).factory('personalizationsmarteditCommerceCustomizationService', function() {
        var nonCommerceActionTypes = ['cxCmsActionData'];

        var ccService = {};
        var types = [];

        var isNonCommerceAction = function(action) {
            return nonCommerceActionTypes.some(function(val) {
                return val === action.type;
            });
        };

        var isCommerceAction = function(action) {
            return !isNonCommerceAction(action);
        };

        var isTypeEnabled = function(type, seConfigurationData) {
            return (seConfigurationData !== undefined && seConfigurationData !== null && seConfigurationData[type.confProperty] === true);
        };

        ccService.registerType = function(item) {
            var type = item.type;
            var exists = false;

            types.forEach(function(val) {
                if (val.type === type) {
                    exists = true;
                }
            });

            if (!exists) {
                types.push(item);
            }
        };

        ccService.getAvailableTypes = function(seConfigurationData) {
            return types.filter(function(item) {
                return isTypeEnabled(item, seConfigurationData);
            });
        };

        ccService.isCommerceCustomizationEnabled = function(seConfigurationData) {
            var at = ccService.getAvailableTypes(seConfigurationData);
            return at.length > 0;
        };

        ccService.getNonCommerceActionsCount = function(variation) {
            return (variation.actions || []).filter(isNonCommerceAction).length;
        };

        ccService.getCommerceActionsCountMap = function(variation) {
            var result = {};

            (variation.actions || [])
            .filter(isCommerceAction)
                .forEach(function(action) {
                    var typeKey = action.type.toLowerCase();

                    var count = result[typeKey];
                    if (count === undefined) {
                        count = 1;
                    } else {
                        count += 1;
                    }
                    result[typeKey] = count;
                });

            return result;
        };

        ccService.getCommerceActionsCount = function(variation) {
            return (variation.actions || [])
                .filter(isCommerceAction).length;
        };

        return ccService;
    })
    //To remove when angular-ui-select would be upgraded to version > 0.19
    .directive('uisOpenClose', ['$parse', '$timeout', function($parse, $timeout) {
        return {
            restrict: 'A',
            require: 'uiSelect',
            link: function(scope, element, attrs, $select) {
                $select.onOpenCloseCallback = $parse(attrs.uisOpenClose);

                scope.$watch('$select.open', function(isOpen, previousState) {
                    if (isOpen !== previousState) {
                        $timeout(function() {
                            $select.onOpenCloseCallback(scope, {
                                isOpen: isOpen
                            });
                        });
                    }
                });
            }
        };
    }])
    .directive('negate', [
        function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attribute, ngModelController) {
                    ngModelController.$isEmpty = function(value) {
                        return !!value;
                    };

                    ngModelController.$formatters.unshift(function(value) {
                        return !value;
                    });

                    ngModelController.$parsers.unshift(function(value) {
                        return !value;
                    });
                }
            };
        }
    ])
    .directive('personalizationCurrentElement', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    if (attrs.personalizationCurrentElement) {
                        scope.$eval(attrs.personalizationCurrentElement)(element);
                    }
                }
            };
        }
    ]);

angular.module('personalizationsmarteditContextUtilsModule', [])
    .factory('personalizationsmarteditContextUtils', function() {
        var self = this;

        var Personalization = function() { //NOSONAR
            this.enabled = false;
        };

        var Customize = function() { //NOSONAR
            this.enabled = false;
            this.selectedCustomization = null;
            this.selectedVariations = null;
            this.selectedComponents = null;
        };

        var CombinedView = function() { //NOSONAR
            this.enabled = false;
            this.selectedItems = null;
            this.customize = new Customize();
        };

        var SeData = function() { //NOSONAR
            this.pageId = null;
            this.seExperienceData = null;
            this.seConfigurationData = null;
            this.sePreviewData = null;
        };

        self.getContextObject = function() {
            return {
                personalization: new Personalization(),
                customize: new Customize(),
                combinedView: new CombinedView(),
                seData: new SeData()
            };
        };

        self.clearCustomizeContext = function(contexService) {
            var customize = contexService.getCustomize();
            customize.enabled = false;
            customize.selectedCustomization = null;
            customize.selectedVariations = null;
            customize.selectedComponents = null;
            contexService.setCustomize(customize);
        };

        self.clearCustomizeContextAndReloadPreview = function(iFrameUtils, contexService) {
            var selectedVariations = angular.copy(contexService.getCustomize().selectedVariations);
            self.clearCustomizeContext(contexService);
            iFrameUtils.clearAndReloadPreview(selectedVariations);
        };

        self.clearCombinedViewCustomizeContext = function(contextService) {
            var combinedView = contextService.getCombinedView();
            combinedView.customize.enabled = false;
            combinedView.customize.selectedCustomization = null;
            combinedView.customize.selectedVariations = null;
            combinedView.customize.selectedComponents = null;
            (combinedView.selectedItems || []).forEach(function(item) {
                delete item.highlighted;
            });
            contextService.setCombinedView(combinedView);
        };

        self.clearCombinedViewContext = function(contextService) {
            var combinedView = contextService.getCombinedView();
            combinedView.enabled = false;
            combinedView.selectedItems = null;
            contextService.setCombinedView(combinedView);
        };

        self.clearCombinedViewContextAndReloadPreview = function(iFrameUtils, contextService) {
            var cvEnabled = angular.copy(contextService.getCombinedView().enabled);
            var cvSelectedItems = angular.copy(contextService.getCombinedView().selectedItems);
            self.clearCombinedViewContext(contextService);
            if (cvEnabled && cvSelectedItems) {
                iFrameUtils.clearAndReloadPreview({});
            }
        };

        return self;
    });

angular.module('personalizationsmarteditCommons')
    .constant('PERSONALIZATION_DATE_FORMATS', {
        SHORT_DATE_FORMAT: 'M/D/YY',
        MODEL_DATE_FORMAT: 'YYYY-MM-DDTHH:mm:SSZ'
    })
    .factory('personalizationsmarteditDateUtils', ['$filter', 'DATE_CONSTANTS', 'isBlank', 'PERSONALIZATION_DATE_FORMATS', function($filter, DATE_CONSTANTS, isBlank, PERSONALIZATION_DATE_FORMATS) {
        var utils = {};

        utils.formatDate = function(dateStr, format) {
            format = format || DATE_CONSTANTS.MOMENT_FORMAT;
            if (dateStr) {
                if (dateStr.match && dateStr.match(/^(\d{4})\-(\d{2})\-(\d{2})T(\d{2}):(\d{2}):(\d{2})(\+|\-)(\d{4})$/)) {
                    dateStr = dateStr.slice(0, -2) + ":" + dateStr.slice(-2);
                }
                return moment(new Date(dateStr)).format(format);
            } else {
                return "";
            }
        };

        utils.formatDateWithMessage = function(dateStr, format) {
            format = format || PERSONALIZATION_DATE_FORMATS.SHORT_DATE_FORMAT;
            if (dateStr) {
                return utils.formatDate(dateStr, format);
            } else {
                return $filter('translate')('personalization.toolbar.pagecustomizations.nodatespecified');
            }
        };

        utils.isDateInThePast = function(modelValue) {
            if (isBlank(modelValue)) {
                return false;
            } else {
                return moment(new Date(modelValue)).isBefore();
            }
        };

        utils.isDateValidOrEmpty = function(modelValue) {
            return isBlank(modelValue) || Date.parse(modelValue);
        };

        utils.isDateRangeValid = function(startDate, endDate) {
            if (isBlank(startDate) || isBlank(endDate)) {
                return true;
            } else {
                return moment(new Date(startDate)).isSameOrBefore(moment(new Date(endDate)));
            }
        };

        utils.isDateStrFormatValid = function(dateStr, format) {
            format = format || DATE_CONSTANTS.MOMENT_FORMAT;
            if (isBlank(dateStr)) {
                return false;
            } else {
                return moment(dateStr, format, true).isValid();
            }
        };

        return utils;
    }]).directive('dateTimePickerRange', ['$timeout', 'languageService', 'personalizationsmarteditDateUtils', 'DATE_CONSTANTS', function($timeout, languageService, personalizationsmarteditDateUtils, DATE_CONSTANTS) {
        return {
            templateUrl: 'dateTimePickerRangeTemplate.html',
            restrict: 'E',
            transclude: true,
            replace: false,
            scope: {
                name: '=',
                dateFrom: '=',
                dateTo: '=',
                isEditable: '=',
                dateFormat: '='
            },
            link: function($scope, elem, attributes, ctrl) {
                $scope.placeholderText = 'personalization.commons.datetimepicker.placeholder';

                $scope.isFromDateValid = false;
                $scope.isToDateValid = false;

                $scope.isEndDateInThePast = false;

                if ($scope.isEditable) {

                    $scope.getDateOrDefault = function(date) {
                        if (date && Date.parse(date)) {
                            return moment(new Date(date));
                        } else {
                            return false;
                        }
                    };

                    $scope.getMinToDate = function(date) {
                        if (!personalizationsmarteditDateUtils.isDateInThePast(date)) {
                            return $scope.getDateOrDefault(date);
                        } else {
                            return new moment();
                        }
                    };

                    getFromPickerNode()
                        .datetimepicker({
                            format: DATE_CONSTANTS.MOMENT_FORMAT,
                            showClear: true,
                            showClose: true,
                            useCurrent: false,
                            keepInvalid: true,
                            locale: languageService.getBrowserLocale().split('-')[0]
                        }).on('dp.change', function(e) {
                            var dateFrom = personalizationsmarteditDateUtils.formatDate(e.date);
                            if (personalizationsmarteditDateUtils.isDateValidOrEmpty(dateFrom) &&
                                personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateTo) &&
                                !personalizationsmarteditDateUtils.isDateRangeValid(dateFrom, $scope.dateTo)) {
                                dateFrom = angular.copy($scope.dateTo);
                            }
                            $scope.dateFrom = dateFrom;
                        });

                    getToPickerNode()
                        .datetimepicker({
                            format: DATE_CONSTANTS.MOMENT_FORMAT,
                            showClear: true,
                            showClose: true,
                            useCurrent: false,
                            keepInvalid: true,
                            locale: languageService.getBrowserLocale().split('-')[0]
                        }).on('dp.change', function(e) {
                            var dateTo = personalizationsmarteditDateUtils.formatDate(e.date);
                            if (personalizationsmarteditDateUtils.isDateValidOrEmpty(dateTo) &&
                                personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateFrom) &&
                                !personalizationsmarteditDateUtils.isDateRangeValid($scope.dateFrom, dateTo)) {
                                dateTo = angular.copy($scope.dateFrom);
                            }
                            $scope.dateTo = dateTo;
                        });

                    $scope.$watch('dateFrom', function() {
                        $scope.isFromDateValid = personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateFrom);
                        if (personalizationsmarteditDateUtils.isDateStrFormatValid($scope.dateFrom, DATE_CONSTANTS.MOMENT_FORMAT)) {
                            getToDatetimepicker().minDate($scope.getMinToDate($scope.dateFrom));
                        } else {
                            getToDatetimepicker().minDate(new moment());
                        }
                    }, true);

                    $scope.$watch('dateTo', function() {
                        var dateToValid = personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateTo);
                        if (dateToValid) {
                            $scope.isToDateValid = true;
                            $scope.isEndDateInThePast = personalizationsmarteditDateUtils.isDateInThePast($scope.dateTo);
                        } else {
                            $scope.isToDateValid = false;
                            $scope.isEndDateInThePast = false;
                        }
                        if (personalizationsmarteditDateUtils.isDateStrFormatValid($scope.dateTo, DATE_CONSTANTS.MOMENT_FORMAT)) {
                            getFromDatetimepicker().maxDate($scope.getDateOrDefault($scope.dateTo));
                        }
                    }, true);
                }

                function getFromPickerNode() {
                    return elem.querySelectorAll('#date-picker-range-from');
                }

                function getFromDatetimepicker() {
                    return getFromPickerNode().datetimepicker().data("DateTimePicker");
                }

                function getToPickerNode() {
                    return elem.querySelectorAll('#date-picker-range-to');
                }

                function getToDatetimepicker() {
                    return getToPickerNode().datetimepicker().data("DateTimePicker");
                }
            }
        };
    }]).directive('isdatevalidorempty', ['isBlank', 'personalizationsmarteditDateUtils', function(isBlank, personalizationsmarteditDateUtils) {
        return {
            restrict: "A",
            require: "ngModel",
            scope: false,
            link: function(scope, element, attributes, ctrl) {
                ctrl.$validators.isdatevalidorempty = function(modelValue) {
                    return personalizationsmarteditDateUtils.isDateValidOrEmpty(modelValue);
                };
            }
        };
    }]);

angular.module('personalizationsmarteditCommons')
    .directive('personalizationInfiniteScroll', ['$rootScope', 'yjQuery', '$window', '$timeout', function($rootScope, yjQuery, $window, $timeout) {
        return {
            link: function(scope, elem, attrs) {
                var checkWhenEnabled, handler, scrollDistance, scrollEnabled;
                $window = angular.element($window);
                scrollDistance = 0;
                if (attrs.personalizationInfiniteScrollDistance !== null) {
                    scope.$watch(attrs.personalizationInfiniteScrollDistance, function(value) {
                        scrollDistance = parseInt(value, 10);
                        return scrollDistance;
                    });
                }
                scrollEnabled = true;
                checkWhenEnabled = false;
                if (attrs.personalizationInfiniteScrollDisabled !== null) {
                    scope.$watch(attrs.personalizationInfiniteScrollDisabled, function(value) {
                        scrollEnabled = !value;
                        if (scrollEnabled && checkWhenEnabled) {
                            checkWhenEnabled = false;
                            return handler();
                        }
                    });
                }
                $rootScope.$on('refreshStart', function() {
                    elem.animate({
                        scrollTop: "0"
                    });
                });
                handler = function() {
                    var container, elementBottom, remaining, shouldScroll, containerBottom;
                    if (elem.children().length <= 0) {
                        return;
                    }
                    container = yjQuery(elem.children()[0]);
                    elementBottom = elem.offset().top + elem.height();
                    containerBottom = container.offset().top + container.height();
                    remaining = containerBottom - elementBottom;
                    shouldScroll = remaining <= elem.height() * scrollDistance;
                    if (shouldScroll && scrollEnabled) {
                        if ($rootScope.$$phase) {
                            return scope.$eval(attrs.personalizationInfiniteScroll);
                        } else {
                            return scope.$apply(attrs.personalizationInfiniteScroll);
                        }
                    } else if (shouldScroll) {
                        checkWhenEnabled = true;
                        return checkWhenEnabled;
                    }
                };
                elem.on('scroll', handler);
                scope.$on('$destroy', function() {
                    return $window.off('scroll', handler);
                });
                return $timeout((function() {
                    if (attrs.personalizationInfiniteScrollImmediateCheck) {
                        if (scope.$eval(attrs.personalizationInfiniteScrollImmediateCheck)) {
                            return handler();
                        }
                    } else {
                        return handler();
                    }
                }), 0);
            }
        };
    }]);

angular.module('personalizationsmarteditCommons')
    .directive('personalizationsmarteditPagination', function() {
        return {
            templateUrl: 'personalizationsmarteditPaginationTemplate.html',
            restrict: 'E',
            scope: {
                callback: "=",
                pages: "=?",
                currentPage: "=?",
                pageSizes: "=?",
                currentSize: "=?",
                pagesOffset: "=?",
                fixedPageSize: "=?",
                incrementByOne: "=?"
            },

            link: function($scope, element, attrs) {

                if (!$scope.callback) {
                    console.log("callback is undefined!");
                }

                $scope.pages = $scope.pages || [0, 1, 2];
                $scope.currentPage = $scope.currentPage || 0;
                $scope.pageSizes = $scope.pageSizes || [5, 10, 25, 50, 100];
                $scope.currentSize = $scope.currentSize || 10;
                $scope.pagesOffset = $scope.pagesOffset || 1;
                $scope.fixedPageSize = $scope.fixedPageSize || false; //NOSONAR
                $scope.incrementByOne = $scope.incrementByOne || false; //NOSONAR

                $scope.pageClick = function(newValue) {
                    if ($scope.currentPage !== newValue) {
                        $scope.currentPage = newValue;
                        $scope.callback($scope);
                    }
                };

                $scope.pageSizeClick = function(newValue) {
                    if ($scope.currentSize !== newValue) {
                        $scope.currentSize = newValue;
                        $scope.currentPage = 0;
                        $scope.callback($scope);
                    }
                };

                $scope.hasPrevious = function() {
                    return $scope.currentPage > 0;
                };

                $scope.hasNext = function() {
                    return $scope.currentPage < $scope.pages.length - 1;
                };

                $scope.isActive = function(value) {
                    return $scope.currentPage === value;
                };

                $scope.rightClick = function() {
                    if ($scope.hasNext()) {
                        $scope.currentPage = $scope.incrementByOne ? $scope.currentPage + 1 : $scope.pages.length - 1;
                        $scope.callback($scope);
                    }
                };

                $scope.leftClick = function() {
                    if ($scope.hasPrevious()) {
                        $scope.currentPage = $scope.incrementByOne ? $scope.currentPage - 1 : 0;
                        $scope.callback($scope);
                    }
                };

                $scope.pagesToDisplay = function() {
                    var numberOfPages = 2 * $scope.pagesOffset + 1;
                    if ($scope.pages.length <= numberOfPages) {
                        return $scope.pages;
                    } else {
                        var start = Math.max($scope.currentPage - $scope.pagesOffset, 0);
                        if (start + numberOfPages > $scope.pages.length) {
                            start = $scope.pages.length - numberOfPages;
                        }
                        return $scope.pages.slice(start, start + numberOfPages);
                    }
                };

                $scope.availablePageSizes = function() {
                    return $scope.pageSizes;
                };

                $scope.getCurrentPageSize = function() {
                    return $scope.currentSize;
                };

                $scope.isFixedPageSize = function() {
                    return $scope.fixedPageSize;
                };

                $scope.showArrows = function() {
                    return $scope.pages.length > $scope.pagesOffset * 2 + 1;
                };

            }
        };
    });

angular.module('personalizationsmarteditScrollZone', ['yjqueryModule'])
    .controller('personalizationsmarteditScrollZoneController', ['$scope', '$timeout', '$compile', 'yjQuery', function($scope, $timeout, $compile, yjQuery) {
        var self = this;

        //Properties
        var scrollZoneTop = true;
        Object.defineProperty(this, 'scrollZoneTop', {
            get: function() {
                return scrollZoneTop;
            },
            set: function(newVal) {
                scrollZoneTop = newVal;
            }
        });

        var scrollZoneBottom = true;
        Object.defineProperty(this, 'scrollZoneBottom', {
            get: function() {
                return scrollZoneBottom;
            },
            set: function(newVal) {
                scrollZoneBottom = newVal;
            }
        });

        var start = false;
        Object.defineProperty(this, 'start', {
            get: function() {
                return start;
            },
            set: function(newVal) {
                start = newVal;
            }
        });

        var elementToScroll = {};
        Object.defineProperty(this, 'elementToScroll', {
            get: function() {
                return elementToScroll;
            },
            set: function(newVal) {
                elementToScroll = newVal;
            }
        });

        var scrollZoneVisible = false;
        Object.defineProperty(this, 'scrollZoneVisible', {
            get: function() {
                return scrollZoneVisible;
            },
            set: function(newVal) {
                scrollZoneVisible = newVal;
            }
        });

        //Methods
        this.stopScroll = function() {
            self.start = false;
        };

        this.scrollTop = function() {
            if (!self.start) {
                return;
            }
            self.scrollZoneTop = self.elementToScroll.scrollTop() <= 2 ? false : true;
            self.scrollZoneBottom = true;

            self.elementToScroll.scrollTop(self.elementToScroll.scrollTop() - 15);
            $timeout(function() {
                self.scrollTop();
            }, 100);
        };

        this.scrollBottom = function() {
            if (!self.start) {
                return;
            }
            self.scrollZoneTop = true;
            var heightVisibleFromTop = self.elementToScroll.get(0).scrollHeight - self.elementToScroll.scrollTop();
            self.scrollZoneBottom = Math.abs(heightVisibleFromTop - self.elementToScroll.outerHeight()) < 2 ? false : true;

            self.elementToScroll.scrollTop(self.elementToScroll.scrollTop() + 15);
            $timeout(function() {
                self.scrollBottom();
            }, 100);
        };

        //Lifecycle methods
        this.$onChanges = function(changes) {
            if (changes.scrollZoneVisible) {
                self.start = changes.scrollZoneVisible.currentValue;
                self.scrollZoneTop = true;
                self.scrollZoneBottom = true;
            }
        };

        this.$onInit = function() {
            var topScrollZone = $compile("<div id=\"sliderTopScrollZone\" data-ng-include=\"'personalizationsmarteditScrollZoneTopTemplate.html'\"></div>")($scope);
            angular.element("body").append(topScrollZone);
            var bottomScrollZone = $compile("<div id=\"sliderBottomScrollZone\" data-ng-include=\"'personalizationsmarteditScrollZoneBottomTemplate.html'\"></div>")($scope);
            angular.element("body").append(bottomScrollZone);
            self.elementToScroll = self.getElementToScroll();
        };

        this.$onDestroy = function() {
            angular.element("#sliderTopScrollZone").scope().$destroy();
            angular.element("#sliderTopScrollZone").remove();
            angular.element("#sliderBottomScrollZone").scope().$destroy();
            angular.element("#sliderBottomScrollZone").remove();
            angular.element("body").contents().each(function() {
                if (this.nodeType === Node.COMMENT_NODE && this.data.indexOf('personalizationsmarteditScrollZone') > -1) {
                    yjQuery(this).remove();
                }
            });
        };
    }])
    .component('personalizationsmarteditScrollZone', {
        controller: 'personalizationsmarteditScrollZoneController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            scrollZoneVisible: '<',
            getElementToScroll: '&',
            isTransparent: '<?'
        }
    });

angular.module('personalizationcommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationcommons/dateTimePickerRange/dateTimePickerRangeTemplate.html',
    "<div class=\"pe-datetime-range\">\n" +
    "    <div class=\"col-md-6 pe-datetime-range__from\" data-ng-class=\"{'has-error': !isFromDateValid}\">\n" +
    "        <label for=\"customization-start-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.startdate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateFrom\" id=\"customization-start-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateFrom\" data-date-formatter id=\"customization-start-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span ng-if=\"!isFromDateValid\" class=\"help-block pe-datetime__error-msg pe-datetime__msg\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformatfrom.description\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-6 pe-datetime-range__to\" data-ng-class=\"{'has-error':!isToDateValid, 'has-warning':isEndDateInThePast}\">\n" +
    "        <label for=\"customization-end-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateTo\" id=\"customization-end-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateTo\" data-date-formatter id=\"customization-end-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span class=\"help-block pe-datetime__error-msg pe-datetime__msg\" ng-if=\"!isToDateValid\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformat.description\">\n" +
    "        </span>\n" +
    "        <span ng-if=\"isEndDateInThePast\" class=\"help-block pe-datetime__warning-msg pe-datetime__msg\">\n" +
    "            <span class=\"hyicon hyicon-msgwarning\"></span>\n" +
    "            <span data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddateinthepast.description\"></span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditPagination/personalizationsmarteditPaginationTemplate.html',
    "<div class=\"row\">\n" +
    "    <div class=\"col-xs-4\"></div>\n" +
    "    <div class=\"col-xs-4\">\n" +
    "        <ul class=\"pagination pagination-lg\">\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"leftClick()\" ng-class=\"hasPrevious()?'enabled':'disabled'\"><a>&laquo;</a></li>\n" +
    "            <li class=\"no-underline\" ng-repeat=\"i in pagesToDisplay()\" ng-class=\"isActive({{i}})?'active':''\"><a ng-click=\"pageClick(i)\">{{i+1}}</a></li>\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"rightClick()\" ng-class=\"hasNext()?'enabled':'disabled'\"><a>&raquo;</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-2\"></div>\n" +
    "    <div class=\"col-xs-2\" ng-if=\"!isFixedPageSize()\">\n" +
    "        <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "            <span ng-bind=\"getCurrentPageSize()\"></span>\n" +
    "            <span data-translate=\"personalization.commons.pagination.rowsperpage\"></span>\n" +
    "            <span class=\"list-arrow hyicon hyicon-arrow\"></span>\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "            <li ng-repeat=\"i in availablePageSizes() track by $index\"><a ng-click=\"pageSizeClick(i)\">{{i}}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneBottomTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--bottom\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneBottom\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollBottom()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneTopTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--top\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneTop\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollTop()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );

}]);
