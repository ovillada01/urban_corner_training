angular.module('personalizationsmarteditCommons', [
        'yjqueryModule',
        'alertServiceModule',
        'personalizationcommonsTemplates',
        'languageServiceModule',
        'seConstantsModule'
    ])
    .constant('PERSONALIZATION_MODEL_STATUS_CODES', {
        ENABLED: 'ENABLED',
        DISABLED: 'DISABLED'
    })
    .constant('PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', {
        ALL: 'ALL',
        ENABLED: 'ENABLED',
        DISABLED: 'DISABLED'
    })
    .constant('PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING', {
        0: {
            borderClass: 'personalizationsmarteditComponentSelected0',
            listClass: 'personalizationsmarteditComponentSelectedList0'
        },
        1: {
            borderClass: 'personalizationsmarteditComponentSelected1',
            listClass: 'personalizationsmarteditComponentSelectedList1'
        },
        2: {
            borderClass: 'personalizationsmarteditComponentSelected2',
            listClass: 'personalizationsmarteditComponentSelectedList2'
        },
        3: {
            borderClass: 'personalizationsmarteditComponentSelected3',
            listClass: 'personalizationsmarteditComponentSelectedList3'
        },
        4: {
            borderClass: 'personalizationsmarteditComponentSelected4',
            listClass: 'personalizationsmarteditComponentSelectedList4'
        },
        5: {
            borderClass: 'personalizationsmarteditComponentSelected5',
            listClass: 'personalizationsmarteditComponentSelectedList5'
        },
        6: {
            borderClass: 'personalizationsmarteditComponentSelected6',
            listClass: 'personalizationsmarteditComponentSelectedList6'
        },
        7: {
            borderClass: 'personalizationsmarteditComponentSelected7',
            listClass: 'personalizationsmarteditComponentSelectedList7'
        },
        8: {
            borderClass: 'personalizationsmarteditComponentSelected8',
            listClass: 'personalizationsmarteditComponentSelectedList8'
        },
        9: {
            borderClass: 'personalizationsmarteditComponentSelected9',
            listClass: 'personalizationsmarteditComponentSelectedList9'
        },
        10: {
            borderClass: 'personalizationsmarteditComponentSelected10',
            listClass: 'personalizationsmarteditComponentSelectedList10'
        },
        11: {
            borderClass: 'personalizationsmarteditComponentSelected11',
            listClass: 'personalizationsmarteditComponentSelectedList11'
        },
        12: {
            borderClass: 'personalizationsmarteditComponentSelected12',
            listClass: 'personalizationsmarteditComponentSelectedList12'
        },
        13: {
            borderClass: 'personalizationsmarteditComponentSelected13',
            listClass: 'personalizationsmarteditComponentSelectedList13'
        },
        14: {
            borderClass: 'personalizationsmarteditComponentSelected14',
            listClass: 'personalizationsmarteditComponentSelectedList14'
        }
    })
    .run(['$rootScope', 'PERSONALIZATION_MODEL_STATUS_CODES', function($rootScope, PERSONALIZATION_MODEL_STATUS_CODES) {
        $rootScope.PERSONALIZATION_MODEL_STATUS_CODES = PERSONALIZATION_MODEL_STATUS_CODES;
    }])
    .filter('statusNotDeleted', ['personalizationsmarteditUtils', function(personalizationsmarteditUtils) {
        return function(value) {
            if (angular.isArray(value)) {
                return personalizationsmarteditUtils.getVisibleItems(value);
            }
            return value;
        };
    }])
    .factory('personalizationsmarteditUtils', ['$filter', 'PERSONALIZATION_MODEL_STATUS_CODES', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', 'PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING', function($filter, PERSONALIZATION_MODEL_STATUS_CODES, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING) {
        var utils = {};

        utils.pushToArrayIfValueExists = function(array, key, value) {
            if (value) {
                array.push({
                    "key": key,
                    "value": value
                });
            }
        };

        utils.getContainerIdForElement = function(element) {
            var container = element.closest('[data-smartedit-container-id][data-smartedit-container-type="CxCmsComponentContainer"]');
            if (container.length) {
                return container.data().smarteditContainerId;
            }
            return null;
        };

        utils.getContainerIdForComponent = function(componentType, componentId) {
            var element = angular.element('[data-smartedit-component-id="' + componentId + '"][data-smartedit-component-type="' + componentType + '"]');
            if (angular.isArray(element)) {
                element = element[0];
            }
            return utils.getContainerIdForElement(element);
        };

        utils.getSlotIdForElement = function(element) {
            var slot = element.closest('[data-smartedit-component-type="ContentSlot"]');
            if (slot.length) {
                return slot.data().smarteditComponentId;
            }
            return null;
        };

        utils.getSlotIdForComponent = function(componentType, componentId) {
            var element = angular.element('[data-smartedit-component-id="' + componentId + '"][data-smartedit-component-type="' + componentType + '"]');
            if (angular.isArray(element)) {
                element = element[0];
            }
            return utils.getSlotIdForElement(element);
        };

        utils.getVariationCodes = function(variations) {
            if ((typeof variations === 'undefined') || (variations === null)) {
                return [];
            }
            var allVariationsCodes = variations.map(function(elem) {
                return elem.code;
            }).filter(function(elem) {
                return typeof elem !== 'undefined';
            });
            return allVariationsCodes;
        };

        utils.getPageId = function() {
            return /page\-([\w]+)/.exec($('iframe').contents().find('body').attr('class'))[1];
        };

        utils.getVariationKey = function(customizationId, variations) {
            if (customizationId === undefined || variations === undefined) {
                return [];
            }

            var allVariationsKeys = variations.map(function(elem) {
                return elem.code;
            }).filter(function(elem) {
                return typeof elem !== 'undefined';
            }).map(function(variationId) {
                return {
                    "variationCode": variationId,
                    "customizationCode": customizationId
                };
            });
            return allVariationsKeys;
        };

        utils.getSegmentTriggerForVariation = function(variation) {
            var triggers = variation.triggers || [];
            var segmentTriggerArr = triggers.filter(function(trigger) {
                return trigger.type === "segmentTriggerData";
            });

            if (segmentTriggerArr.length === 0) {
                return {};
            }

            return segmentTriggerArr[0];
        };

        utils.isPersonalizationItemEnabled = function(item) {
            return item.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED;
        };

        utils.getEnablementTextForCustomization = function(customization, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";
            if (utils.isPersonalizationItemEnabled(customization)) {
                return $filter('translate')(keyPrefix + '.customization.enabled');
            } else {
                return $filter('translate')(keyPrefix + '.customization.disabled');
            }
        };

        utils.getEnablementTextForVariation = function(variation, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";

            if (utils.isPersonalizationItemEnabled(variation)) {
                return $filter('translate')(keyPrefix + '.variation.enabled');
            } else {
                return $filter('translate')(keyPrefix + '.variation.disabled');
            }
        };

        utils.getEnablementActionTextForVariation = function(variation, keyPrefix) {
            keyPrefix = keyPrefix || "personalization";

            if (utils.isPersonalizationItemEnabled(variation)) {
                return $filter('translate')(keyPrefix + '.variation.options.disable');
            } else {
                return $filter('translate')(keyPrefix + '.variation.options.enable');
            }
        };

        utils.getActivityStateForCustomization = function(customization) {
            if (customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED) {
                if (moment().isBetween(new Date(customization.enabledStartDate), new Date(customization.enabledEndDate), 'minute', '[]')) {
                    return "status-active";
                } else {
                    return "status-ignore";
                }
            } else {
                return "status-inactive";
            }
        };

        utils.getActivityStateForVariation = function(customization, variation) {
            if (variation.enabled) {
                return utils.getActivityStateForCustomization(customization);
            } else {
                return "status-inactive";
            }
        };

        utils.isItemVisible = function(item) {
            return item.status !== 'DELETED';
        };

        utils.getVisibleItems = function(items) {
            return items.filter(function(item) {
                return utils.isItemVisible(item);
            });
        };

        utils.getValidRank = function(items, item, increaseValue) {
            var from = items.indexOf(item);
            var delta = increaseValue < 0 ? -1 : 1;

            var increase = from + increaseValue;

            while (increase >= 0 && increase < items.length && !utils.isItemVisible(items[increase])) {
                increase += delta;
            }

            increase = increase >= items.length ? items.length - 1 : increase;
            increase = increase < 0 ? 0 : increase;

            return items[increase].rank;
        };

        utils.getStatusesMapping = function() {
            var statusesMapping = [];

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL,
                text: 'personalization.context.status.all',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.ENABLED, PERSONALIZATION_MODEL_STATUS_CODES.DISABLED]
            });

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ENABLED,
                text: 'personalization.context.status.enabled',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.ENABLED]
            });

            statusesMapping.push({
                code: PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.DISABLED,
                text: 'personalization.context.status.disabled',
                modelStatuses: [PERSONALIZATION_MODEL_STATUS_CODES.DISABLED]
            });

            return statusesMapping;
        };

        utils.getClassForElement = function(index) {
            var wrappedIndex = index % Object.keys(PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING).length;
            return PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING[wrappedIndex].listClass;
        };

        utils.getLetterForElement = function(index) {
            var wrappedIndex = index % Object.keys(PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING).length;
            return String.fromCharCode('a'.charCodeAt() + wrappedIndex).toUpperCase();
        };

        utils.getCommerceCustomizationTooltip = function(variation) {
            var result = "";
            angular.forEach(variation.commerceCustomizations, function(propertyValue, propertyKey) {
                result += $filter('translate')('personalization.modal.manager.commercecustomization.' + propertyKey) + ": " + propertyValue + "\n";
            });
            return result;
        };

        return utils;

    }]).factory('personalizationsmarteditMessageHandler', ['alertService', function(alertService) {

        var messageHandler = {};
        messageHandler.sendInformation = function(informationMessage) {
            alertService.showInfo(informationMessage);
        };

        messageHandler.sendError = function(errorMessage) {
            alertService.showDanger(errorMessage);
        };

        messageHandler.sendWarning = function(warningMessage) {
            alertService.showWarning(warningMessage);
        };

        messageHandler.sendSuccess = function(successMessage) {
            alertService.showSuccess(successMessage);
        };

        return messageHandler;

    }]).factory('personalizationsmarteditCommerceCustomizationService', function() {
        var nonCommerceActionTypes = ['cxCmsActionData'];

        var ccService = {};
        var types = [];

        var isNonCommerceAction = function(action) {
            return nonCommerceActionTypes.some(function(val) {
                return val === action.type;
            });
        };

        var isCommerceAction = function(action) {
            return !isNonCommerceAction(action);
        };

        var isTypeEnabled = function(type, seConfigurationData) {
            return (seConfigurationData !== undefined && seConfigurationData !== null && seConfigurationData[type.confProperty] === true);
        };

        ccService.registerType = function(item) {
            var type = item.type;
            var exists = false;

            types.forEach(function(val) {
                if (val.type === type) {
                    exists = true;
                }
            });

            if (!exists) {
                types.push(item);
            }
        };

        ccService.getAvailableTypes = function(seConfigurationData) {
            return types.filter(function(item) {
                return isTypeEnabled(item, seConfigurationData);
            });
        };

        ccService.isCommerceCustomizationEnabled = function(seConfigurationData) {
            var at = ccService.getAvailableTypes(seConfigurationData);
            return at.length > 0;
        };

        ccService.getNonCommerceActionsCount = function(variation) {
            return (variation.actions || []).filter(isNonCommerceAction).length;
        };

        ccService.getCommerceActionsCountMap = function(variation) {
            var result = {};

            (variation.actions || [])
            .filter(isCommerceAction)
                .forEach(function(action) {
                    var typeKey = action.type.toLowerCase();

                    var count = result[typeKey];
                    if (count === undefined) {
                        count = 1;
                    } else {
                        count += 1;
                    }
                    result[typeKey] = count;
                });

            return result;
        };

        ccService.getCommerceActionsCount = function(variation) {
            return (variation.actions || [])
                .filter(isCommerceAction).length;
        };

        return ccService;
    })
    //To remove when angular-ui-select would be upgraded to version > 0.19
    .directive('uisOpenClose', ['$parse', '$timeout', function($parse, $timeout) {
        return {
            restrict: 'A',
            require: 'uiSelect',
            link: function(scope, element, attrs, $select) {
                $select.onOpenCloseCallback = $parse(attrs.uisOpenClose);

                scope.$watch('$select.open', function(isOpen, previousState) {
                    if (isOpen !== previousState) {
                        $timeout(function() {
                            $select.onOpenCloseCallback(scope, {
                                isOpen: isOpen
                            });
                        });
                    }
                });
            }
        };
    }])
    .directive('negate', [
        function() {
            return {
                require: 'ngModel',
                link: function(scope, element, attribute, ngModelController) {
                    ngModelController.$isEmpty = function(value) {
                        return !!value;
                    };

                    ngModelController.$formatters.unshift(function(value) {
                        return !value;
                    });

                    ngModelController.$parsers.unshift(function(value) {
                        return !value;
                    });
                }
            };
        }
    ])
    .directive('personalizationCurrentElement', [
        function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    if (attrs.personalizationCurrentElement) {
                        scope.$eval(attrs.personalizationCurrentElement)(element);
                    }
                }
            };
        }
    ]);

angular.module('personalizationsmarteditContextUtilsModule', [])
    .factory('personalizationsmarteditContextUtils', function() {
        var self = this;

        var Personalization = function() { //NOSONAR
            this.enabled = false;
        };

        var Customize = function() { //NOSONAR
            this.enabled = false;
            this.selectedCustomization = null;
            this.selectedVariations = null;
            this.selectedComponents = null;
        };

        var CombinedView = function() { //NOSONAR
            this.enabled = false;
            this.selectedItems = null;
            this.customize = new Customize();
        };

        var SeData = function() { //NOSONAR
            this.pageId = null;
            this.seExperienceData = null;
            this.seConfigurationData = null;
            this.sePreviewData = null;
        };

        self.getContextObject = function() {
            return {
                personalization: new Personalization(),
                customize: new Customize(),
                combinedView: new CombinedView(),
                seData: new SeData()
            };
        };

        self.clearCustomizeContext = function(contexService) {
            var customize = contexService.getCustomize();
            customize.enabled = false;
            customize.selectedCustomization = null;
            customize.selectedVariations = null;
            customize.selectedComponents = null;
            contexService.setCustomize(customize);
        };

        self.clearCustomizeContextAndReloadPreview = function(iFrameUtils, contexService) {
            var selectedVariations = angular.copy(contexService.getCustomize().selectedVariations);
            self.clearCustomizeContext(contexService);
            iFrameUtils.clearAndReloadPreview(selectedVariations);
        };

        self.clearCombinedViewCustomizeContext = function(contextService) {
            var combinedView = contextService.getCombinedView();
            combinedView.customize.enabled = false;
            combinedView.customize.selectedCustomization = null;
            combinedView.customize.selectedVariations = null;
            combinedView.customize.selectedComponents = null;
            (combinedView.selectedItems || []).forEach(function(item) {
                delete item.highlighted;
            });
            contextService.setCombinedView(combinedView);
        };

        self.clearCombinedViewContext = function(contextService) {
            var combinedView = contextService.getCombinedView();
            combinedView.enabled = false;
            combinedView.selectedItems = null;
            contextService.setCombinedView(combinedView);
        };

        self.clearCombinedViewContextAndReloadPreview = function(iFrameUtils, contextService) {
            var cvEnabled = angular.copy(contextService.getCombinedView().enabled);
            var cvSelectedItems = angular.copy(contextService.getCombinedView().selectedItems);
            self.clearCombinedViewContext(contextService);
            if (cvEnabled && cvSelectedItems) {
                iFrameUtils.clearAndReloadPreview({});
            }
        };

        return self;
    });

angular.module('personalizationsmarteditCommons')
    .constant('PERSONALIZATION_DATE_FORMATS', {
        SHORT_DATE_FORMAT: 'M/D/YY',
        MODEL_DATE_FORMAT: 'YYYY-MM-DDTHH:mm:SSZ'
    })
    .factory('personalizationsmarteditDateUtils', ['$filter', 'DATE_CONSTANTS', 'isBlank', 'PERSONALIZATION_DATE_FORMATS', function($filter, DATE_CONSTANTS, isBlank, PERSONALIZATION_DATE_FORMATS) {
        var utils = {};

        utils.formatDate = function(dateStr, format) {
            format = format || DATE_CONSTANTS.MOMENT_FORMAT;
            if (dateStr) {
                if (dateStr.match && dateStr.match(/^(\d{4})\-(\d{2})\-(\d{2})T(\d{2}):(\d{2}):(\d{2})(\+|\-)(\d{4})$/)) {
                    dateStr = dateStr.slice(0, -2) + ":" + dateStr.slice(-2);
                }
                return moment(new Date(dateStr)).format(format);
            } else {
                return "";
            }
        };

        utils.formatDateWithMessage = function(dateStr, format) {
            format = format || PERSONALIZATION_DATE_FORMATS.SHORT_DATE_FORMAT;
            if (dateStr) {
                return utils.formatDate(dateStr, format);
            } else {
                return $filter('translate')('personalization.toolbar.pagecustomizations.nodatespecified');
            }
        };

        utils.isDateInThePast = function(modelValue) {
            if (isBlank(modelValue)) {
                return false;
            } else {
                return moment(new Date(modelValue)).isBefore();
            }
        };

        utils.isDateValidOrEmpty = function(modelValue) {
            return isBlank(modelValue) || Date.parse(modelValue);
        };

        utils.isDateRangeValid = function(startDate, endDate) {
            if (isBlank(startDate) || isBlank(endDate)) {
                return true;
            } else {
                return moment(new Date(startDate)).isSameOrBefore(moment(new Date(endDate)));
            }
        };

        utils.isDateStrFormatValid = function(dateStr, format) {
            format = format || DATE_CONSTANTS.MOMENT_FORMAT;
            if (isBlank(dateStr)) {
                return false;
            } else {
                return moment(dateStr, format, true).isValid();
            }
        };

        return utils;
    }]).directive('dateTimePickerRange', ['$timeout', 'languageService', 'personalizationsmarteditDateUtils', 'DATE_CONSTANTS', function($timeout, languageService, personalizationsmarteditDateUtils, DATE_CONSTANTS) {
        return {
            templateUrl: 'dateTimePickerRangeTemplate.html',
            restrict: 'E',
            transclude: true,
            replace: false,
            scope: {
                name: '=',
                dateFrom: '=',
                dateTo: '=',
                isEditable: '=',
                dateFormat: '='
            },
            link: function($scope, elem, attributes, ctrl) {
                $scope.placeholderText = 'personalization.commons.datetimepicker.placeholder';

                $scope.isFromDateValid = false;
                $scope.isToDateValid = false;

                $scope.isEndDateInThePast = false;

                if ($scope.isEditable) {

                    $scope.getDateOrDefault = function(date) {
                        if (date && Date.parse(date)) {
                            return moment(new Date(date));
                        } else {
                            return false;
                        }
                    };

                    $scope.getMinToDate = function(date) {
                        if (!personalizationsmarteditDateUtils.isDateInThePast(date)) {
                            return $scope.getDateOrDefault(date);
                        } else {
                            return new moment();
                        }
                    };

                    getFromPickerNode()
                        .datetimepicker({
                            format: DATE_CONSTANTS.MOMENT_FORMAT,
                            showClear: true,
                            showClose: true,
                            useCurrent: false,
                            keepInvalid: true,
                            locale: languageService.getBrowserLocale().split('-')[0]
                        }).on('dp.change', function(e) {
                            var dateFrom = personalizationsmarteditDateUtils.formatDate(e.date);
                            if (personalizationsmarteditDateUtils.isDateValidOrEmpty(dateFrom) &&
                                personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateTo) &&
                                !personalizationsmarteditDateUtils.isDateRangeValid(dateFrom, $scope.dateTo)) {
                                dateFrom = angular.copy($scope.dateTo);
                            }
                            $scope.dateFrom = dateFrom;
                        });

                    getToPickerNode()
                        .datetimepicker({
                            format: DATE_CONSTANTS.MOMENT_FORMAT,
                            showClear: true,
                            showClose: true,
                            useCurrent: false,
                            keepInvalid: true,
                            locale: languageService.getBrowserLocale().split('-')[0]
                        }).on('dp.change', function(e) {
                            var dateTo = personalizationsmarteditDateUtils.formatDate(e.date);
                            if (personalizationsmarteditDateUtils.isDateValidOrEmpty(dateTo) &&
                                personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateFrom) &&
                                !personalizationsmarteditDateUtils.isDateRangeValid($scope.dateFrom, dateTo)) {
                                dateTo = angular.copy($scope.dateFrom);
                            }
                            $scope.dateTo = dateTo;
                        });

                    $scope.$watch('dateFrom', function() {
                        $scope.isFromDateValid = personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateFrom);
                        if (personalizationsmarteditDateUtils.isDateStrFormatValid($scope.dateFrom, DATE_CONSTANTS.MOMENT_FORMAT)) {
                            getToDatetimepicker().minDate($scope.getMinToDate($scope.dateFrom));
                        } else {
                            getToDatetimepicker().minDate(new moment());
                        }
                    }, true);

                    $scope.$watch('dateTo', function() {
                        var dateToValid = personalizationsmarteditDateUtils.isDateValidOrEmpty($scope.dateTo);
                        if (dateToValid) {
                            $scope.isToDateValid = true;
                            $scope.isEndDateInThePast = personalizationsmarteditDateUtils.isDateInThePast($scope.dateTo);
                        } else {
                            $scope.isToDateValid = false;
                            $scope.isEndDateInThePast = false;
                        }
                        if (personalizationsmarteditDateUtils.isDateStrFormatValid($scope.dateTo, DATE_CONSTANTS.MOMENT_FORMAT)) {
                            getFromDatetimepicker().maxDate($scope.getDateOrDefault($scope.dateTo));
                        }
                    }, true);
                }

                function getFromPickerNode() {
                    return elem.querySelectorAll('#date-picker-range-from');
                }

                function getFromDatetimepicker() {
                    return getFromPickerNode().datetimepicker().data("DateTimePicker");
                }

                function getToPickerNode() {
                    return elem.querySelectorAll('#date-picker-range-to');
                }

                function getToDatetimepicker() {
                    return getToPickerNode().datetimepicker().data("DateTimePicker");
                }
            }
        };
    }]).directive('isdatevalidorempty', ['isBlank', 'personalizationsmarteditDateUtils', function(isBlank, personalizationsmarteditDateUtils) {
        return {
            restrict: "A",
            require: "ngModel",
            scope: false,
            link: function(scope, element, attributes, ctrl) {
                ctrl.$validators.isdatevalidorempty = function(modelValue) {
                    return personalizationsmarteditDateUtils.isDateValidOrEmpty(modelValue);
                };
            }
        };
    }]);

angular.module('personalizationsmarteditCommons')
    .directive('personalizationInfiniteScroll', ['$rootScope', 'yjQuery', '$window', '$timeout', function($rootScope, yjQuery, $window, $timeout) {
        return {
            link: function(scope, elem, attrs) {
                var checkWhenEnabled, handler, scrollDistance, scrollEnabled;
                $window = angular.element($window);
                scrollDistance = 0;
                if (attrs.personalizationInfiniteScrollDistance !== null) {
                    scope.$watch(attrs.personalizationInfiniteScrollDistance, function(value) {
                        scrollDistance = parseInt(value, 10);
                        return scrollDistance;
                    });
                }
                scrollEnabled = true;
                checkWhenEnabled = false;
                if (attrs.personalizationInfiniteScrollDisabled !== null) {
                    scope.$watch(attrs.personalizationInfiniteScrollDisabled, function(value) {
                        scrollEnabled = !value;
                        if (scrollEnabled && checkWhenEnabled) {
                            checkWhenEnabled = false;
                            return handler();
                        }
                    });
                }
                $rootScope.$on('refreshStart', function() {
                    elem.animate({
                        scrollTop: "0"
                    });
                });
                handler = function() {
                    var container, elementBottom, remaining, shouldScroll, containerBottom;
                    if (elem.children().length <= 0) {
                        return;
                    }
                    container = yjQuery(elem.children()[0]);
                    elementBottom = elem.offset().top + elem.height();
                    containerBottom = container.offset().top + container.height();
                    remaining = containerBottom - elementBottom;
                    shouldScroll = remaining <= elem.height() * scrollDistance;
                    if (shouldScroll && scrollEnabled) {
                        if ($rootScope.$$phase) {
                            return scope.$eval(attrs.personalizationInfiniteScroll);
                        } else {
                            return scope.$apply(attrs.personalizationInfiniteScroll);
                        }
                    } else if (shouldScroll) {
                        checkWhenEnabled = true;
                        return checkWhenEnabled;
                    }
                };
                elem.on('scroll', handler);
                scope.$on('$destroy', function() {
                    return $window.off('scroll', handler);
                });
                return $timeout((function() {
                    if (attrs.personalizationInfiniteScrollImmediateCheck) {
                        if (scope.$eval(attrs.personalizationInfiniteScrollImmediateCheck)) {
                            return handler();
                        }
                    } else {
                        return handler();
                    }
                }), 0);
            }
        };
    }]);

angular.module('personalizationsmarteditCommons')
    .directive('personalizationsmarteditPagination', function() {
        return {
            templateUrl: 'personalizationsmarteditPaginationTemplate.html',
            restrict: 'E',
            scope: {
                callback: "=",
                pages: "=?",
                currentPage: "=?",
                pageSizes: "=?",
                currentSize: "=?",
                pagesOffset: "=?",
                fixedPageSize: "=?",
                incrementByOne: "=?"
            },

            link: function($scope, element, attrs) {

                if (!$scope.callback) {
                    console.log("callback is undefined!");
                }

                $scope.pages = $scope.pages || [0, 1, 2];
                $scope.currentPage = $scope.currentPage || 0;
                $scope.pageSizes = $scope.pageSizes || [5, 10, 25, 50, 100];
                $scope.currentSize = $scope.currentSize || 10;
                $scope.pagesOffset = $scope.pagesOffset || 1;
                $scope.fixedPageSize = $scope.fixedPageSize || false; //NOSONAR
                $scope.incrementByOne = $scope.incrementByOne || false; //NOSONAR

                $scope.pageClick = function(newValue) {
                    if ($scope.currentPage !== newValue) {
                        $scope.currentPage = newValue;
                        $scope.callback($scope);
                    }
                };

                $scope.pageSizeClick = function(newValue) {
                    if ($scope.currentSize !== newValue) {
                        $scope.currentSize = newValue;
                        $scope.currentPage = 0;
                        $scope.callback($scope);
                    }
                };

                $scope.hasPrevious = function() {
                    return $scope.currentPage > 0;
                };

                $scope.hasNext = function() {
                    return $scope.currentPage < $scope.pages.length - 1;
                };

                $scope.isActive = function(value) {
                    return $scope.currentPage === value;
                };

                $scope.rightClick = function() {
                    if ($scope.hasNext()) {
                        $scope.currentPage = $scope.incrementByOne ? $scope.currentPage + 1 : $scope.pages.length - 1;
                        $scope.callback($scope);
                    }
                };

                $scope.leftClick = function() {
                    if ($scope.hasPrevious()) {
                        $scope.currentPage = $scope.incrementByOne ? $scope.currentPage - 1 : 0;
                        $scope.callback($scope);
                    }
                };

                $scope.pagesToDisplay = function() {
                    var numberOfPages = 2 * $scope.pagesOffset + 1;
                    if ($scope.pages.length <= numberOfPages) {
                        return $scope.pages;
                    } else {
                        var start = Math.max($scope.currentPage - $scope.pagesOffset, 0);
                        if (start + numberOfPages > $scope.pages.length) {
                            start = $scope.pages.length - numberOfPages;
                        }
                        return $scope.pages.slice(start, start + numberOfPages);
                    }
                };

                $scope.availablePageSizes = function() {
                    return $scope.pageSizes;
                };

                $scope.getCurrentPageSize = function() {
                    return $scope.currentSize;
                };

                $scope.isFixedPageSize = function() {
                    return $scope.fixedPageSize;
                };

                $scope.showArrows = function() {
                    return $scope.pages.length > $scope.pagesOffset * 2 + 1;
                };

            }
        };
    });

angular.module('personalizationsmarteditScrollZone', ['yjqueryModule'])
    .controller('personalizationsmarteditScrollZoneController', ['$scope', '$timeout', '$compile', 'yjQuery', function($scope, $timeout, $compile, yjQuery) {
        var self = this;

        //Properties
        var scrollZoneTop = true;
        Object.defineProperty(this, 'scrollZoneTop', {
            get: function() {
                return scrollZoneTop;
            },
            set: function(newVal) {
                scrollZoneTop = newVal;
            }
        });

        var scrollZoneBottom = true;
        Object.defineProperty(this, 'scrollZoneBottom', {
            get: function() {
                return scrollZoneBottom;
            },
            set: function(newVal) {
                scrollZoneBottom = newVal;
            }
        });

        var start = false;
        Object.defineProperty(this, 'start', {
            get: function() {
                return start;
            },
            set: function(newVal) {
                start = newVal;
            }
        });

        var elementToScroll = {};
        Object.defineProperty(this, 'elementToScroll', {
            get: function() {
                return elementToScroll;
            },
            set: function(newVal) {
                elementToScroll = newVal;
            }
        });

        var scrollZoneVisible = false;
        Object.defineProperty(this, 'scrollZoneVisible', {
            get: function() {
                return scrollZoneVisible;
            },
            set: function(newVal) {
                scrollZoneVisible = newVal;
            }
        });

        //Methods
        this.stopScroll = function() {
            self.start = false;
        };

        this.scrollTop = function() {
            if (!self.start) {
                return;
            }
            self.scrollZoneTop = self.elementToScroll.scrollTop() <= 2 ? false : true;
            self.scrollZoneBottom = true;

            self.elementToScroll.scrollTop(self.elementToScroll.scrollTop() - 15);
            $timeout(function() {
                self.scrollTop();
            }, 100);
        };

        this.scrollBottom = function() {
            if (!self.start) {
                return;
            }
            self.scrollZoneTop = true;
            var heightVisibleFromTop = self.elementToScroll.get(0).scrollHeight - self.elementToScroll.scrollTop();
            self.scrollZoneBottom = Math.abs(heightVisibleFromTop - self.elementToScroll.outerHeight()) < 2 ? false : true;

            self.elementToScroll.scrollTop(self.elementToScroll.scrollTop() + 15);
            $timeout(function() {
                self.scrollBottom();
            }, 100);
        };

        //Lifecycle methods
        this.$onChanges = function(changes) {
            if (changes.scrollZoneVisible) {
                self.start = changes.scrollZoneVisible.currentValue;
                self.scrollZoneTop = true;
                self.scrollZoneBottom = true;
            }
        };

        this.$onInit = function() {
            var topScrollZone = $compile("<div id=\"sliderTopScrollZone\" data-ng-include=\"'personalizationsmarteditScrollZoneTopTemplate.html'\"></div>")($scope);
            angular.element("body").append(topScrollZone);
            var bottomScrollZone = $compile("<div id=\"sliderBottomScrollZone\" data-ng-include=\"'personalizationsmarteditScrollZoneBottomTemplate.html'\"></div>")($scope);
            angular.element("body").append(bottomScrollZone);
            self.elementToScroll = self.getElementToScroll();
        };

        this.$onDestroy = function() {
            angular.element("#sliderTopScrollZone").scope().$destroy();
            angular.element("#sliderTopScrollZone").remove();
            angular.element("#sliderBottomScrollZone").scope().$destroy();
            angular.element("#sliderBottomScrollZone").remove();
            angular.element("body").contents().each(function() {
                if (this.nodeType === Node.COMMENT_NODE && this.data.indexOf('personalizationsmarteditScrollZone') > -1) {
                    yjQuery(this).remove();
                }
            });
        };
    }])
    .component('personalizationsmarteditScrollZone', {
        controller: 'personalizationsmarteditScrollZoneController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            scrollZoneVisible: '<',
            getElementToScroll: '&',
            isTransparent: '<?'
        }
    });

angular.module('personalizationcommonsTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationcommons/dateTimePickerRange/dateTimePickerRangeTemplate.html',
    "<div class=\"pe-datetime-range\">\n" +
    "    <div class=\"col-md-6 pe-datetime-range__from\" data-ng-class=\"{'has-error': !isFromDateValid}\">\n" +
    "        <label for=\"customization-start-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.startdate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateFrom\" id=\"customization-start-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-from\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_from_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateFrom\" data-date-formatter id=\"customization-start-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span ng-if=\"!isFromDateValid\" class=\"help-block pe-datetime__error-msg pe-datetime__msg\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformatfrom.description\">\n" +
    "        </span>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-6 pe-datetime-range__to\" data-ng-class=\"{'has-error':!isToDateValid, 'has-warning':isEndDateInThePast}\">\n" +
    "        <label for=\"customization-end-date\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddate\" class=\"personalization-modal__label\"></label>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input\" placeholder=\"{{ placeholderText | translate}}\" ng-disabled=\"!isEditable\" name=\"{{name}}\" isdatevalidorempty data-ng-model=\"dateTo\" id=\"customization-end-date\" />\n" +
    "            <span class=\"input-group-addon pe-datetime-range__picker\" data-ng-show=\"isEditable\">\n" +
    "                <span class=\"glyphicon glyphicon-calendar pe-datetime-range__picker__icon\"></span>\n" +
    "            </span>\n" +
    "        </div>\n" +
    "        <div class=\"input-group date pe-date-field\" id=\"date-picker-range-to\" data-ng-show=\"!isEditable\">\n" +
    "            <input type='text' name=\"date_to_key\" class=\"form-control pe-date-field__input \" data-ng-class=\"{'pe-input--is-disabled': !isEditable}\" data-ng-model=\"dateTo\" data-date-formatter id=\"customization-end-date\" isdatevalidorempty data-ng-disabled=\"true\" data-format-type=\"short\">\n" +
    "        </div>\n" +
    "        <span class=\"help-block pe-datetime__error-msg pe-datetime__msg\" ng-if=\"!isToDateValid\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.wrongdateformat.description\">\n" +
    "        </span>\n" +
    "        <span ng-if=\"isEndDateInThePast\" class=\"help-block pe-datetime__warning-msg pe-datetime__msg\">\n" +
    "            <span class=\"hyicon hyicon-msgwarning\"></span>\n" +
    "            <span data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.enddateinthepast.description\"></span>\n" +
    "        </span>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditPagination/personalizationsmarteditPaginationTemplate.html',
    "<div class=\"row\">\n" +
    "    <div class=\"col-xs-4\"></div>\n" +
    "    <div class=\"col-xs-4\">\n" +
    "        <ul class=\"pagination pagination-lg\">\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"leftClick()\" ng-class=\"hasPrevious()?'enabled':'disabled'\"><a>&laquo;</a></li>\n" +
    "            <li class=\"no-underline\" ng-repeat=\"i in pagesToDisplay()\" ng-class=\"isActive({{i}})?'active':''\"><a ng-click=\"pageClick(i)\">{{i+1}}</a></li>\n" +
    "            <li class=\"no-underline\" ng-if=\"showArrows()\" ng-click=\"rightClick()\" ng-class=\"hasNext()?'enabled':'disabled'\"><a>&raquo;</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "    <div class=\"col-xs-2\"></div>\n" +
    "    <div class=\"col-xs-2\" ng-if=\"!isFixedPageSize()\">\n" +
    "        <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "            <span ng-bind=\"getCurrentPageSize()\"></span>\n" +
    "            <span data-translate=\"personalization.commons.pagination.rowsperpage\"></span>\n" +
    "            <span class=\"list-arrow hyicon hyicon-arrow\"></span>\n" +
    "        </button>\n" +
    "        <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "            <li ng-repeat=\"i in availablePageSizes() track by $index\"><a ng-click=\"pageSizeClick(i)\">{{i}}</a></li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneBottomTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--bottom\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneBottom\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollBottom()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationcommons/personalizationsmarteditScrollZone/personalizationsmarteditScrollZoneTopTemplate.html',
    "<div class=\"perso__scrollzone perso__scrollzone--top\" data-ng-class=\"ctrl.isTransparent?'perso__scrollzone--transparent':'perso__scrollzone--normal'\" data-ng-show=\"ctrl.scrollZoneVisible && ctrl.scrollZoneTop\" data-ng-mouseenter=\"ctrl.start=true;ctrl.scrollTop()\" data-ng-mouseleave=\"ctrl.stopScroll()\">\n" +
    "</div>"
  );

}]);

angular.module('personalizationsmarteditCombinedViewModule', [
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditCommons',
        'ui.select',
        'personalizationsmarteditContextServiceModule',
        'renderServiceModule',
        'personalizationsmarteditDataFactory',
        'modalServiceModule',
        'personalizationsmarteditPreviewServiceModule'
    ])
    .factory('personalizationsmarteditCombinedView', ['$controller', 'modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', 'personalizationsmarteditContextService', 'personalizationsmarteditCombinedViewCommons', 'personalizationsmarteditContextUtils', function($controller, modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, personalizationsmarteditContextService, personalizationsmarteditCombinedViewCommons, personalizationsmarteditContextUtils) {
        var manager = {};
        manager.openManagerAction = function() {
            modalService.open({
                title: "personalization.modal.combinedview.title",
                templateUrl: 'personalizationsmarteditCombinedViewConfigureTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    $scope.modalManager = modalManager;
                    angular.extend(this, $controller('personalizationsmarteditCombinedViewController', {
                        $scope: $scope
                    }));
                }],
                buttons: [{
                    id: 'confirmCancel',
                    label: 'personalization.modal.combinedview.button.cancel',
                    style: MODAL_BUTTON_STYLES.SECONDARY,
                    action: MODAL_BUTTON_ACTIONS.DISMISS
                }, {
                    id: 'confirmOk',
                    label: 'personalization.modal.combinedview.button.ok',
                    action: MODAL_BUTTON_ACTIONS.CLOSE
                }]
            }).then(function(result) {
                personalizationsmarteditContextUtils.clearCombinedViewCustomizeContext(personalizationsmarteditContextService);
                if (personalizationsmarteditContextService.getCombinedView().enabled) {
                    personalizationsmarteditCombinedViewCommons.updatePreview(personalizationsmarteditCombinedViewCommons.getVariationsForPreviewTicket());
                }
            }, function(failure) {});
        };

        return manager;
    }])
    .factory('personalizationsmarteditCombinedViewCommons', ['$q', '$filter', 'personalizationsmarteditContextService', 'personalizationsmarteditPreviewService', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditRestService', function($q, $filter, personalizationsmarteditContextService, personalizationsmarteditPreviewService, personalizationsmarteditIFrameUtils, personalizationsmarteditMessageHandler, personalizationsmarteditRestService) {
        var service = {};

        var updateActionsOnSelectedVariations = function() {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            var promissesArray = [];
            (combinedView.selectedItems || []).forEach(function(item) {
                promissesArray.push(personalizationsmarteditRestService.getActions(item.customization.code, item.variation.code).then(function successCallback(response) {
                    item.variation.actions = response.actions;
                }));
            });
            $q.all(promissesArray).then(function() {
                personalizationsmarteditContextService.setCombinedView(combinedView);
            });
        };

        service.updatePreview = function(previewTicketVariations) {
            var previewTicketId = personalizationsmarteditContextService.getSeData().sePreviewData.previewTicketId;
            personalizationsmarteditPreviewService.updatePreviewTicketWithVariations(previewTicketId, previewTicketVariations).then(function successCallback() {
                var previewData = personalizationsmarteditContextService.getSeData().sePreviewData;
                personalizationsmarteditIFrameUtils.reloadPreview(previewData.resourcePath, previewData.previewTicketId);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingpreviewticket'));
            });
            updateActionsOnSelectedVariations();
        };

        service.getVariationsForPreviewTicket = function() {
            var previewTicketVariations = [];
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            (combinedView.selectedItems || []).forEach(function(item) {
                previewTicketVariations.push({
                    customizationCode: item.customization.code,
                    variationCode: item.variation.code
                });
            });
            return previewTicketVariations;
        };

        service.combinedViewEnabledEvent = function(isEnabled) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            combinedView.enabled = isEnabled;
            personalizationsmarteditContextService.setCombinedView(combinedView);
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = null;
            customize.selectedVariations = null;
            customize.selectedComponents = null;
            personalizationsmarteditContextService.setCustomize(customize);
            if (isEnabled) {
                service.updatePreview(service.getVariationsForPreviewTicket());
            } else {
                service.updatePreview([]);
            }
        };

        return service;
    }])
    .controller('personalizationsmarteditCombinedViewMenuController', ['$scope', '$filter', 'personalizationsmarteditUtils', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditRestService', 'personalizationsmarteditContextService', 'personalizationsmarteditCombinedViewCommons', function($scope, $filter, personalizationsmarteditUtils, personalizationsmarteditMessageHandler, personalizationsmarteditRestService, personalizationsmarteditContextService, personalizationsmarteditCombinedViewCommons) {

        $scope.combinedView = personalizationsmarteditContextService.getCombinedView();
        $scope.selectedItems = $scope.combinedView.selectedItems || [];

        var getAndSetComponentsForElement = function(customizationId, variationId) {
            personalizationsmarteditRestService.getComponenentsIdsForVariation(customizationId, variationId).then(function successCallback(response) {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                combinedView.customize.selectedComponents = response.components;
                personalizationsmarteditContextService.setCombinedView(combinedView);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponentsforvariation'));
            });
        };

        $scope.itemClick = function(item) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            if (!combinedView.enabled) {
                return;
            }

            $scope.selectedItems.forEach(function(elem) {
                elem.highlighted = false;
            });
            item.highlighted = true;

            combinedView.customize.selectedCustomization = item.customization;
            combinedView.customize.selectedVariations = item.variation;
            personalizationsmarteditContextService.setCombinedView(combinedView);
            getAndSetComponentsForElement(item.customization.code, item.variation.code);
            personalizationsmarteditCombinedViewCommons.updatePreview(personalizationsmarteditUtils.getVariationKey(item.customization.code, [item.variation]));
        };

        $scope.getClassForElement = personalizationsmarteditUtils.getClassForElement;
        $scope.getLetterForElement = personalizationsmarteditUtils.getLetterForElement;

        $scope.combinedViewEnabledChangeEvent = function() {
            personalizationsmarteditCombinedViewCommons.combinedViewEnabledEvent($scope.combinedView.enabled);
        };

        $scope.$watch('combinedView.selectedItems', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.selectedItems = $scope.combinedView.selectedItems || [];
            }
        }, true);

    }])
    .controller('personalizationsmarteditCombinedViewController', ['$q', '$scope', '$filter', 'personalizationsmarteditCombinedViewCommons', 'personalizationsmarteditContextService', 'personalizationsmarteditRestService', 'customizationDataFactory', 'PaginationHelper', 'personalizationsmarteditMessageHandler', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', 'personalizationsmarteditUtils', function($q, $scope, $filter, personalizationsmarteditCombinedViewCommons, personalizationsmarteditContextService, personalizationsmarteditRestService, customizationDataFactory, PaginationHelper, personalizationsmarteditMessageHandler, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, personalizationsmarteditUtils) {

        customizationDataFactory.resetData();

        var successCallback = function(response) {
            $scope.pagination = new PaginationHelper(response.pagination);
            $scope.selectionArray.length = 0;
            customizationDataFactory.items.map(function(customization) {
                customization.variations.forEach(function(variation) {
                    $scope.selectionArray.push({
                        customization: {
                            code: customization.code,
                            name: customization.name,
                            rank: customization.rank
                        },
                        variation: {
                            code: variation.code,
                            name: variation.name
                        }
                    });
                });
            });
            $scope.moreCustomizationsRequestProcessing = false;
        };

        var errorCallback = function(response) {
            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
            $scope.moreCustomizationsRequestProcessing = false;
        };

        var getDefaultStatus = function() {
            return personalizationsmarteditUtils.getStatusesMapping().filter(function(elem) {
                return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
            })[0];
        };

        var getCustomizationsFilterObject = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                currentSize: $scope.pagination.count,
                currentPage: $scope.pagination.page + 1,
                name: $scope.customizationFilter.name,
                statuses: getDefaultStatus().modelStatuses
            };
        };

        var getCustomizations = function(categoryFilter) {
            var params = {
                filter: categoryFilter,
                dataArrayName: 'customizations'
            };
            customizationDataFactory.updateData(params, successCallback, errorCallback);
        };

        $scope.pagination = new PaginationHelper();
        $scope.pagination.reset();

        $scope.combinedView = personalizationsmarteditContextService.getCombinedView();
        $scope.selectedItems = [];
        angular.copy($scope.combinedView.selectedItems || [], $scope.selectedItems);
        $scope.selectedElement = {};
        $scope.selectionArray = [];

        $scope.moreCustomizationsRequestProcessing = false;
        $scope.addMoreItems = function() {
            if ($scope.pagination.page < $scope.pagination.totalPages - 1 && !$scope.moreCustomizationsRequestProcessing) {
                $scope.moreCustomizationsRequestProcessing = true;
                getCustomizations(getCustomizationsFilterObject());
            }
        };

        $scope.selectElement = function(item) {
            $scope.selectedItems.push(item);
            $scope.selectedItems.sort(function(a, b) {
                return a.customization.rank - b.customization.rank;
            });

            $scope.selectedElement = null;
            $scope.searchInputKeypress(null, '');
        };

        $scope.initUiSelect = function(uiSelectController) {
            uiSelectController.isActive = function(item) {
                return false;
            };
        };

        $scope.removeSelectedItem = function(item) {
            $scope.selectedItems.splice($scope.selectedItems.indexOf(item), 1);
            $scope.selectedElement = null;
            $scope.searchInputKeypress(null, '');
        };

        $scope.getClassForElement = personalizationsmarteditUtils.getClassForElement;
        $scope.getLetterForElement = personalizationsmarteditUtils.getLetterForElement;

        $scope.isItemInSelectDisabled = function(item) {
            return $scope.selectedItems.find(function(currentItem) {
                return currentItem.customization.code === item.customization.code;
            });
        };

        $scope.isItemSelected = function(item) {
            return $scope.selectedItems.find(function(currentItem) {
                return currentItem.customization.code === item.customization.code && currentItem.variation.code === item.variation.code;
            });
        };

        $scope.customizationFilter = {
            name: ''
        };

        $scope.searchInputKeypress = function(keyEvent, searchObj) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }
            $scope.pagination.reset();
            $scope.customizationFilter.name = searchObj;
            customizationDataFactory.resetData();
            $scope.addMoreItems();
        };

        var isCombinedViewContextPersRemoved = function(combinedView) {
            return combinedView.selectedItems.filter(function(item) {
                return item.customization.code === combinedView.customize.selectedCustomization.code && item.variation.code === combinedView.customize.selectedVariations.code;
            }).length === 0;
        };

        var buttonHandlerFn = function(buttonId) {
            var deferred = $q.defer();
            if (buttonId === 'confirmOk') {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                combinedView.selectedItems = $scope.selectedItems;

                if (combinedView.enabled && combinedView.customize.selectedVariations !== null && isCombinedViewContextPersRemoved(combinedView)) {
                    combinedView.customize.selectedCustomization = null;
                    combinedView.customize.selectedVariations = null;
                    combinedView.customize.selectedComponents = null;
                }

                personalizationsmarteditContextService.setCombinedView(combinedView);
                return deferred.resolve();
            }
            return deferred.reject();
        };

        $scope.modalManager.setButtonHandler(buttonHandlerFn);

        $scope.$watch('selectedItems', function(newValue, oldValue) {
            $scope.modalManager.disableButton("confirmOk");
            if (newValue !== oldValue) {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                var arrayEquals = (combinedView.selectedItems || []).length === 0 && $scope.selectedItems.length === 0;
                arrayEquals = arrayEquals || angular.equals(combinedView.selectedItems, $scope.selectedItems);
                if (!arrayEquals) {
                    $scope.modalManager.enableButton("confirmOk");
                }
            }
        }, true);

    }]);

angular.module('personalizationsmarteditContextMenu', [
        'modalServiceModule',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditCommons',
        'ui.select',
        'genericEditorModule',
        'editorModalServiceModule',
        'gatewayProxyModule',
        'personalizationsmarteditContextServiceModule',
        'renderServiceModule',
        'personalizationsmarteditDataFactory',
        'slotRestrictionsServiceModule'
    ])
    .factory('personalizationsmarteditContextModal', ['$controller', 'modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', '$filter', 'personalizationsmarteditMessageHandler', 'gatewayProxy', 'personalizationsmarteditContextService', 'renderService', 'personalizationsmarteditRestService', 'editorModalService', 'personalizationsmarteditUtils', function($controller, modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, $filter, personalizationsmarteditMessageHandler, gatewayProxy, personalizationsmarteditContextService, renderService, personalizationsmarteditRestService, editorModalService, personalizationsmarteditUtils) {

        var PersonalizationsmarteditContextModal = function() { //NOSONAR
            this.gatewayId = "personalizationsmarteditContextModal";
            gatewayProxy.initForService(this);
        };

        var modalButtons = [{
            id: 'cancel',
            label: "personalization.modal.addeditaction.button.cancel",
            style: MODAL_BUTTON_STYLES.SECONDARY,
            action: MODAL_BUTTON_ACTIONS.DISMISS
        }, {
            id: 'submit',
            label: "personalization.modal.addeditaction.button.submit",
            action: MODAL_BUTTON_ACTIONS.CLOSE
        }];

        var confirmModalButtons = [{
            id: 'confirmCancel',
            label: 'personalization.modal.deleteaction.button.cancel',
            style: MODAL_BUTTON_STYLES.SECONDARY,
            action: MODAL_BUTTON_ACTIONS.DISMISS
        }, {
            id: 'confirmOk',
            label: 'personalization.modal.deleteaction.button.ok',
            action: MODAL_BUTTON_ACTIONS.CLOSE
        }];

        PersonalizationsmarteditContextModal.prototype.openDeleteAction = function(config) {
            modalService.open({
                size: 'md',
                title: 'personalization.modal.deleteaction.title',
                templateInline: '<div id="confirmationModalDescription">{{ "' + "personalization.modal.deleteaction.content" + '" | translate }}</div>',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    angular.merge($scope, config);
                    $scope.modalManager = modalManager;
                    angular.extend(this, $controller('modalDeleteActionController', {
                        $scope: $scope
                    }));
                }],
                cssClasses: 'yFrontModal',
                buttons: confirmModalButtons
            }).then(function(result) {
                if (personalizationsmarteditContextService.getCombinedView().enabled) {
                    personalizationsmarteditRestService.getActions(config.selectedCustomizationCode, config.selectedVariationCode)
                        .then(function successCallback(response) {
                            var combinedView = personalizationsmarteditContextService.getCombinedView();
                            if (combinedView.customize.selectedComponents) {
                                combinedView.customize.selectedComponents.splice(combinedView.customize.selectedComponents.indexOf(config.containerId), 1);
                            }
                            angular.forEach(combinedView.selectedItems, function(value, key) {
                                if (value.customization.code === config.selectedCustomizationCode && value.variation.code === config.selectedVariationCode) {
                                    value.variation.actions = response.actions;
                                }
                            });
                            personalizationsmarteditContextService.setCombinedView(combinedView);
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingactions'));
                        });
                } else {
                    var customize = personalizationsmarteditContextService.getCustomize();
                    customize.selectedComponents.splice(customize.selectedComponents.indexOf(config.containerId), 1);
                    personalizationsmarteditContextService.setCustomize(customize);
                }
                renderService.renderSlots([config.slotId]).then(function(result) {
                    renderService.renderPage(true);
                });
            });
        };

        PersonalizationsmarteditContextModal.prototype.openAddAction = function(config) {
            modalService.open({
                title: "personalization.modal.addaction.title",
                templateUrl: 'personalizationsmarteditAddEditActionTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    angular.merge($scope, config);
                    $scope.defaultComponentId = config.componentId;
                    $scope.modalManager = modalManager;
                    $scope.editEnabled = false;
                    angular.extend(this, $controller('modalAddEditActionController', {
                        $scope: $scope
                    }));
                    angular.extend(this, $controller('modalAddActionController', {
                        $scope: $scope
                    }));
                }],
                cssClasses: 'yPersonalizationContextModal',
                buttons: modalButtons
            }).then(function(resultContainer) {
                if (personalizationsmarteditContextService.getCombinedView().enabled) {
                    var combinedView = personalizationsmarteditContextService.getCombinedView();
                    combinedView.customize.selectedComponents.push(resultContainer);
                    personalizationsmarteditContextService.setCombinedView(combinedView);
                } else {
                    var customize = personalizationsmarteditContextService.getCustomize();
                    customize.selectedComponents.push(resultContainer);
                    personalizationsmarteditContextService.setCustomize(customize);
                }
                renderService.renderSlots([config.slotId]).then(function(result) {
                    renderService.renderPage(true);
                });
            }, function(failure) {});
        };

        PersonalizationsmarteditContextModal.prototype.openEditAction = function(config) {
            modalService.open({
                title: "personalization.modal.editaction.title",
                templateUrl: 'personalizationsmarteditAddEditActionTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    angular.merge($scope, config);
                    $scope.modalManager = modalManager;
                    $scope.editEnabled = true;
                    angular.extend(this, $controller('modalAddEditActionController', {
                        $scope: $scope
                    }));
                    angular.extend(this, $controller('modalEditActionController', {
                        $scope: $scope
                    }));
                }],
                cssClasses: 'yPersonalizationContextModal',
                buttons: modalButtons
            }).then(function(result) {
                renderService.renderSlots([config.slotId]).then(function(result) {
                    renderService.renderPage(true);
                });
            });
        };

        PersonalizationsmarteditContextModal.prototype.openInfoAction = function() {
            personalizationsmarteditMessageHandler.sendWarning($filter('translate')('personalization.error.nocustomizationvariationselected'));
        };

        PersonalizationsmarteditContextModal.prototype.openEditComponentAction = function(config) {
            editorModalService.open(config.componentType, config.componentId);
        };

        PersonalizationsmarteditContextModal.prototype.openShowActionList = function(config) {
            modalService.open({
                title: "personalization.modal.showactionlist.title",
                templateUrl: 'personalizationsmarteditShowActionListTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    $scope.selectedItems = personalizationsmarteditContextService.getCombinedView().selectedItems;

                    $scope.getClassForElement = personalizationsmarteditUtils.getClassForElement;
                    $scope.getLetterForElement = personalizationsmarteditUtils.getLetterForElement;

                    $scope.initItem = function(item) {
                        item.visible = false;
                        (item.variation.actions || []).forEach(function(elem) {
                            if (elem.containerId && elem.containerId === config.containerId) {
                                item.visible = true;
                            }
                        });
                    };

                }]
            });
        };

        return new PersonalizationsmarteditContextModal();

    }])
    .controller('modalAddEditActionController', ['$scope', '$filter', '$q', '$timeout', 'editorModalService', 'personalizationsmarteditContextService', 'personalizationsmarteditRestService', 'personalizationsmarteditMessageHandler', 'PaginationHelper', 'slotRestrictionsService', 'PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING', function($scope, $filter, $q, $timeout, editorModalService, personalizationsmarteditContextService, personalizationsmarteditRestService, personalizationsmarteditMessageHandler, PaginationHelper, slotRestrictionsService, PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING) {

        $scope.actions = [{
            id: "create",
            name: $filter('translate')("personalization.modal.addeditaction.createnewcomponent")
        }, {
            id: "use",
            name: $filter('translate')("personalization.modal.addeditaction.usecomponent")
        }];

        $scope.newComponent = {};
        $scope.component = {};
        $scope.components = [];
        $scope.newComponentTypes = [];

        var initNewComponentTypes = function() {
            slotRestrictionsService.getSlotRestrictions($scope.slotId).then(function(restrictions) {
                personalizationsmarteditRestService.getNewComponentTypes().then(function success(resp) {
                    $scope.newComponentTypes = resp.componentTypes.filter(function(elem) {
                        return restrictions.indexOf(elem.code) > -1;
                    });
                }, function error(resp) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponentstypes'));
                });
            }, function error(resp) {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingslotrestrictions'));
            });
        };

        var getAndSetComponentById = function(componentId) {
            personalizationsmarteditRestService.getComponent(componentId).then(function successCallback(resp) {
                $scope.component.selected = resp;
            }, function errorCallback(resp) {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponents'));
            });
        };

        $scope.newComponentTypeSelectedEvent = function($item, $model) {
            var options = {
                render: false
            };
            editorModalService.open($scope.newComponent.selected.code, null, options, personalizationsmarteditContextService.getSeData().pageId).then(
                function(response) {
                    $scope.action = {
                        selected: $filter('filter')($scope.actions, {
                            id: "use"
                        }, true)[0]
                    };
                    $scope.componentId = response.uid;
                    getAndSetComponentById($scope.componentId);
                },
                function(response) {
                    $scope.newComponent = {};
                });

        };

        var editAction = function(customizationId, variationId, actionId, componentId) {
            var deferred = $q.defer();
            personalizationsmarteditRestService.editAction(customizationId, variationId, actionId, componentId).then(
                function successCallback(response) {
                    personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.updatingaction'));
                    deferred.resolve();
                },
                function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingaction'));
                    deferred.reject();
                });
            return deferred.promise;
        };

        var addActionToContainer = function(componentId, catalogId, containerId, customizationId, variationId) {
            var deferred = $q.defer();
            personalizationsmarteditRestService.addActionToContainer(componentId, catalogId, containerId, customizationId, variationId).then(
                function successCallback(response) {
                    personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.creatingaction'));
                    deferred.resolve(containerId);
                },
                function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.creatingaction'));
                    deferred.reject();
                });
            return deferred.promise;
        };

        var buttonHandlerFn = function(buttonId) {
            if (buttonId === 'submit') {
                if ($scope.editEnabled) {
                    return editAction($scope.selectedCustomization.code, $scope.selectedVariation.code, $scope.actionId, $scope.component.selected.uid);
                } else {
                    if ($scope.containerId) {
                        return addActionToContainer($scope.component.selected.uid, $scope.component.selected.catalog, $scope.containerId, $scope.selectedCustomization.code, $scope.selectedVariation.code);
                    } else {
                        return personalizationsmarteditRestService.replaceComponentWithContainer($scope.defaultComponentId, $scope.slotId).then(
                            function successCallback(result) {
                                return addActionToContainer($scope.component.selected.uid, $scope.component.selected.catalog, result.uid, $scope.selectedCustomization.code, $scope.selectedVariation.code);
                            },
                            function errorCallback(response) {
                                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.replacingcomponent'));
                                return $q.defer().reject();
                            });
                    }
                }
            }
            return $q.defer().reject();
        };
        $scope.modalManager.setButtonHandler(buttonHandlerFn);

        $scope.componentFilter = {
            name: ''
        };
        var getComponentFilterObject = function() {
            return {
                currentPage: $scope.componentPagination.page + 1,
                mask: $scope.componentFilter.name,
                pageSize: 100,
                sort: 'name'
            };
        };

        $scope.componentPagination = new PaginationHelper();
        $scope.componentPagination.reset();
        $scope.moreComponentRequestProcessing = false;

        $scope.addMoreComponentItems = function() {
            if ($scope.componentPagination.page < $scope.componentPagination.totalPages - 1 && !$scope.moreComponentRequestProcessing) {
                $scope.moreComponentRequestProcessing = true;
                personalizationsmarteditRestService.getComponents(getComponentFilterObject()).then(function successCallback(response) {
                    var filteredComponents = response.componentItems.filter(function(elem) {
                        return $scope.newComponentTypes.some(function(element, index, array) {
                            return elem.typeCode === element.code;
                        });
                    });
                    Array.prototype.push.apply($scope.components, filteredComponents);
                    $scope.componentPagination = new PaginationHelper(response.pagination);
                    $scope.moreComponentRequestProcessing = false;
                    if ($scope.components.length < 20) { //not enough components on list to enable scroll
                        $timeout((function() {
                            $scope.addMoreComponentItems();
                        }), 0);
                    }
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponents'));
                    $scope.moreComponentRequestProcessing = false;
                });
            }
        };

        $scope.componentSearchInputKeypress = function(keyEvent, searchObj) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }
            $scope.componentPagination.reset();
            $scope.componentFilter.name = searchObj;
            $scope.components.length = 0;
            $scope.addMoreComponentItems();
        };

        var getAndSetColorAndLetter = function() {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            if (combinedView.enabled) {
                (combinedView.selectedItems || []).forEach(function(element, index, array) {
                    var state = $scope.selectedCustomizationCode === element.customization.code;
                    state = state && $scope.selectedVariationCode === element.variation.code;
                    var wrappedIndex = index % Object.keys(PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING).length;
                    if (state) {
                        $scope.letterIndicatorForElement = String.fromCharCode('a'.charCodeAt() + wrappedIndex).toUpperCase();
                        $scope.colorIndicatorForElement = PERSONALIZATION_COMBINED_VIEW_CSS_MAPPING[wrappedIndex].listClass;
                    }
                });
            }
        };

        $scope.$watch('action.selected', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.component.selected = undefined;
                if ($scope.editEnabled) {
                    getAndSetComponentById($scope.componentId);
                }
            }
        }, true);

        $scope.$watch('component.selected', function(newValue, oldValue) {
            $scope.modalManager.disableButton("submit");
            if (newValue !== undefined) {
                $scope.modalManager.enableButton("submit");
            }
        }, true);

        $scope.$watch('newComponentTypes', function(newValue, oldValue) {
            if (newValue !== undefined) {
                $scope.componentPagination = new PaginationHelper();
                $scope.componentPagination.reset();

                $scope.moreComponentRequestProcessing = false;

                $scope.addMoreComponentItems();
            }
        }, true);

        //init
        (function() {
            personalizationsmarteditRestService.getCustomization($scope.selectedCustomizationCode)
                .then(function successCallback(response) {
                    $scope.selectedCustomization = response;
                    $scope.selectedVariation = response.variations.filter(function(elem) {
                        return elem.code === $scope.selectedVariationCode;
                    })[0];
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });

            if ($scope.editEnabled) {
                getAndSetComponentById($scope.componentId);
            }

            initNewComponentTypes();
            getAndSetColorAndLetter();
        })();

    }])
    .controller('modalAddActionController', ['$scope', function($scope) {
        $scope.action = {};
    }])
    .controller('modalEditActionController', ['$scope', '$filter', function($scope, $filter) {
        $scope.action = {
            selected: $filter('filter')($scope.actions, {
                id: "use"
            }, true)[0]
        };
    }])
    .controller('modalDeleteActionController', ['$scope', '$q', 'personalizationsmarteditRestService', function($scope, $q, personalizationsmarteditRestService) {
        var buttonHandlerFn = function(buttonId) {
            if (buttonId === 'confirmOk') {
                return personalizationsmarteditRestService.deleteAction($scope.selectedCustomizationCode, $scope.selectedVariationCode, $scope.actionId);
            }
            return $q.defer().reject();
        };
        $scope.modalManager.setButtonHandler(buttonHandlerFn);
    }]);

angular.module('personalizationsmarteditDataFactory', [
        'personalizationsmarteditRestServiceModule'
    ])
    .factory('PaginationHelper', function() {
        var paginationHelper = function(initialData) {
            initialData = initialData || {};

            this.count = initialData.count || 0;
            this.page = initialData.page || 0;
            this.totalCount = initialData.totalCount || 0;
            this.totalPages = initialData.totalPages || 0;

            this.reset = function() {
                this.count = 50;
                this.page = -1;
                this.totalPages = 1;
            };
        };

        return paginationHelper;
    })
    .factory('customizationDataFactory', ['personalizationsmarteditRestService', function(personalizationsmarteditRestService) {
        var factory = {};
        var defaultFilter = {};
        var defaultDataArrayName = "customizations";
        var defaultSuccessCallbackFunction = function() {};
        var defaultErrorCallbackFunction = function() {};

        var getCustomizations = function(filter) {
            personalizationsmarteditRestService.getCustomizations(filter).then(function(response) {
                Array.prototype.push.apply(factory.items, response[defaultDataArrayName]);
                defaultSuccessCallbackFunction(response);
            }, function(response) {
                defaultErrorCallbackFunction(response);
            });
        };

        factory.items = [];

        factory.updateData = function(params, successCallbackFunction, errorCallbackFunction) {
            params = params || {};
            defaultFilter = params.filter || defaultFilter;
            defaultDataArrayName = params.dataArrayName || defaultDataArrayName;
            if (successCallbackFunction && typeof(successCallbackFunction) == "function") {
                defaultSuccessCallbackFunction = successCallbackFunction;
            }
            if (errorCallbackFunction && typeof(errorCallbackFunction) == "function") {
                defaultErrorCallbackFunction = errorCallbackFunction;
            }

            getCustomizations(defaultFilter);
        };

        factory.refreshData = function() {
            if (angular.equals({}, defaultFilter)) {
                return;
            }
            var tempFilter = {};
            angular.copy(defaultFilter, tempFilter);
            tempFilter.currentSize = factory.items.length;
            tempFilter.currentPage = 0;
            factory.resetData();
            getCustomizations(tempFilter);
        };

        factory.resetData = function() {
            factory.items.length = 0;
        };

        factory.pushData = function(newData) {
            if (angular.isObject(newData)) {
                if (angular.isArray(newData)) {
                    Array.prototype.push.apply(factory.items, newData);
                } else {
                    factory.items.push(newData);
                }
            }
        };

        return factory;
    }]);

angular.module('personalizationsmarteditCommerceCustomizationModule', [
        'modalServiceModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'eventServiceModule',
        'personalizationsmarteditContextServiceModule',
        'confirmationModalServiceModule'
    ])
    .factory(
        'personalizationsmarteditCommerceCustomizationView',
        ['$controller', 'modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', function($controller, modalService, MODAL_BUTTON_ACTIONS,
            MODAL_BUTTON_STYLES) {
            var manager = {};
            manager.openCommerceCustomizationAction = function(
                customization, variation) {
                modalService.open({
                    title: "personalization.modal.commercecustomization.title",
                    templateUrl: 'personalizationsmarteditCommerceCustomizationViewTemplate.html',
                    controller: ['$scope', 'modalManager', function($scope, modalManager) {
                        $scope.customization = customization;
                        $scope.variation = variation;
                        $scope.modalManager = modalManager;
                        angular.extend(this, $controller('personalizationsmarteditCommerceCustomizationViewController', {
                            $scope: $scope
                        }));
                    }],
                    buttons: [{
                        id: 'confirmCancel',
                        label: 'personalization.modal.commercecustomization.button.cancel',
                        style: MODAL_BUTTON_STYLES.SECONDARY,
                        action: MODAL_BUTTON_ACTIONS.CLOSE
                    }, {
                        id: 'confirmSave',
                        label: 'personalization.modal.commercecustomization.button.submit',
                        action: MODAL_BUTTON_ACTIONS.CLOSE
                    }]
                }).then(function(result) {

                }, function(failure) {});
            };

            return manager;
        }])
    .controller(
        'personalizationsmarteditCommerceCustomizationViewController',
        ['$scope', '$filter', '$q', 'personalizationsmarteditRestService', 'personalizationsmarteditMessageHandler', 'systemEventService', 'personalizationsmarteditCommerceCustomizationService', 'personalizationsmarteditContextService', 'personalizationsmarteditUtils', 'confirmationModalService', function($scope, $filter, $q,
            personalizationsmarteditRestService,
            personalizationsmarteditMessageHandler,
            systemEventService,
            personalizationsmarteditCommerceCustomizationService,
            personalizationsmarteditContextService,
            personalizationsmarteditUtils,
            confirmationModalService) {

            var STATUS_OLD = 'old';
            var STATUS_NEW = 'new';
            var STATUS_DELETE = 'delete';


            $scope.availableTypes = [];
            $scope.select = {};
            $scope.actions = [];
            $scope.removedActions = [];

            var populateActions = function() {
                personalizationsmarteditRestService.getActions($scope.customization.code, $scope.variation.code)
                    .then(function successCallback(response) {

                        $scope.actions = response.actions.filter(function(elem) {
                            return elem.type !== 'cxCmsActionData';
                        }).map(function(item) {
                            return {
                                action: item,
                                status: STATUS_OLD
                            };
                        });
                    }, function errorCallback() {
                        personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingactions'));
                    });
            };

            var getType = function(type) {
                for (var i = 0; i < $scope.availableTypes.length; ++i) {
                    if ($scope.availableTypes[i].type === type) {
                        return $scope.availableTypes[i];
                    }
                }
                return {};
            };

            var sendRefreshEvent = function() {
                systemEventService.sendSynchEvent('CUSTOMIZATIONS_MODIFIED', {});
            };

            var dismissModalCallback = function() {
                if ($scope.isDirty()) {
                    return confirmationModalService.confirm({
                        description: 'personalization.modal.commercecustomization.cancelconfirmation'
                    }).then(function() {
                        return $q.resolve();
                    }, function() {
                        return $q.reject();
                    });
                } else {
                    return $q.resolve();
                }
            };

            $scope.getActionsToDisplay = function() {
                return $scope.actions;
            };

            $scope.isItemInSelectedActions = function(action, comparer) {
                return $scope.actions.find(function(wrapper) {
                    return comparer(action, wrapper.action);
                });
            };

            $scope.displayAction = function(actionWrapper) {
                var action = actionWrapper.action;
                var type = getType(action.type);
                if (type.getName) {
                    return type.getName(action);
                } else {
                    return action.code;
                }
            };

            //This function requires two parameters
            // action to be added
            // and comparer = function(action,action) for defining if two actions are identical
            // comparer is used
            $scope.addAction = function(action, comparer) {

                var exist = false;
                $scope.actions.forEach(function(wrapper) {
                    exist = exist || comparer(action, wrapper.action);
                });
                if (!exist) {
                    var status = STATUS_NEW;
                    var removedIndex = -1;
                    $scope.removedActions.forEach(function(wrapper, index) {
                        if (comparer(action, wrapper.action)) {
                            removedIndex = index;
                        }
                    });
                    if (removedIndex >= 0) //we found or action in delete queue
                    {
                        status = STATUS_OLD;
                        $scope.removedActions.splice(removedIndex, 1);
                    }
                    $scope.actions.push({
                        action: action,
                        status: status
                    });
                }
            };

            $scope.removeSelectedAction = function(actionWrapper) {
                var index = $scope.actions.indexOf(actionWrapper);
                if (index < 0) {
                    return;
                }
                var removed = $scope.actions.splice(index, 1);
                //only old item should be added to delete queue
                //new items are just deleted
                if (removed[0].status === STATUS_OLD) {
                    removed[0].status = STATUS_DELETE;
                    $scope.removedActions.push(removed[0]);
                }
            };

            $scope.isDirty = function() {
                var dirty = false;
                //dirty if at least one new
                $scope.actions.forEach(function(wrapper) {
                    dirty = dirty || wrapper.status === STATUS_NEW;
                });
                //or one deleted
                dirty = dirty || $scope.removedActions.length > 0;
                return dirty;
            };

            // customization and variation status helper fucntions
            $scope.customizationStatusText = personalizationsmarteditUtils.getEnablementTextForCustomization($scope.customization, 'personalization.modal.commercecustomization');
            $scope.variationStatusText = personalizationsmarteditUtils.getEnablementTextForVariation($scope.variation, 'personalization.modal.commercecustomization');
            $scope.customizationStatus = personalizationsmarteditUtils.getActivityStateForCustomization($scope.customization);
            $scope.variationStatus = personalizationsmarteditUtils.getActivityStateForVariation($scope.customization, $scope.variation);

            // modal buttons
            $scope.onSave = function() {
                var createData = {
                    actions: $scope.actions.filter(function(item) {
                        return item.status === STATUS_NEW;
                    }).map(function(item) {
                        return item.action;
                    })
                };

                var deleteData = $scope.removedActions.filter(function(item) {
                    return item.status === STATUS_DELETE;
                }).map(function(item) {
                    return item.action.code;
                });

                var shouldCreate = createData.actions.length > 0;
                var shouldDelete = deleteData.length > 0;

                if (shouldCreate) {
                    personalizationsmarteditRestService.createActions($scope.customization.code, $scope.variation.code, createData)
                        .then(function successCallback(response) {
                            personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.creatingaction'));
                            sendRefreshEvent();
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.creatingaction'));
                        });
                }

                if (shouldDelete) {
                    personalizationsmarteditRestService.deleteActions($scope.customization.code, $scope.variation.code, deleteData)
                        .then(function successCallback(response) {
                            personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.removingaction'));
                            sendRefreshEvent();
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.removingaction'));
                        });
                }
            };

            $scope.$watch('actions', function() {
                if ($scope.isDirty()) {
                    $scope.modalManager.enableButton("confirmSave");
                } else {
                    $scope.modalManager.disableButton("confirmSave");
                }
            }, true);

            $scope.modalManager.setButtonHandler(function(buttonId) {
                if (buttonId === 'confirmSave') {
                    $scope.onSave();
                } else if (buttonId === 'confirmCancel') {
                    return dismissModalCallback();
                }
            });

            $scope.modalManager.setDismissCallback(function() {
                return dismissModalCallback();
            });

            //init
            (function() {
                $scope.availableTypes = personalizationsmarteditCommerceCustomizationService.getAvailableTypes(personalizationsmarteditContextService.getSeData().seConfigurationData);
                $scope.select = {
                    type: $scope.availableTypes[0]
                };
                populateActions();
            })();
        }]);

angular.module('personalizationsmarteditPromotionModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditCommerceCustomizationModule'
    ])
    .run(['personalizationsmarteditCommerceCustomizationService', '$filter', function(personalizationsmarteditCommerceCustomizationService, $filter) {

        personalizationsmarteditCommerceCustomizationService.registerType({
            type: 'cxPromotionActionData',
            text: 'personalization.modal.commercecustomization.action.type.promotion',
            template: 'personalizationsmarteditPromotionsTemplate.html',
            confProperty: 'personalizationsmartedit.commercecustomization.promotions.enabled',
            getName: function(action) {
                return $filter('translate')('personalization.modal.commercecustomization.promotion.display.name') + " - " + action.promotionId;
            }
        });
    }])

.controller('personalizationsmarteditPromotionController', ['$q', '$scope', '$filter', 'personalizationsmarteditRestService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditManager', function($q, $scope, $filter, personalizationsmarteditRestService, personalizationsmarteditMessageHandler, personalizationsmarteditManager) {

    $scope.promotion = null;
    $scope.availablePromotions = [];


    var getPromotions = function() {
        var deferred = $q.defer();

        personalizationsmarteditRestService.getPreviewTicket().then(function successCallback(previewTicket) {
            var catalogsVersions = previewTicket.catalogVersions;
            personalizationsmarteditRestService.getPromotions(catalogsVersions).then(
                function successCallback(response) {
                    deferred.resolve(response);
                },
                function errorCallback(response) {
                    deferred.reject(response);
                }
            );
        }, function errorCallback(response) {
            deferred.reject(response);
        });

        return deferred.promise;
    };

    var getAvailablePromotions = function() {
        getPromotions()
            .then(function successCallback(response) {
                $scope.availablePromotions = response.promotions;
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingpromotions'));
            });

    };

    var buildAction = function(item) {
        return {
            type: 'cxPromotionActionData',
            promotionId: item.code
        };
    };

    var comparer = function(a1, a2) {
        return a1.type === a2.type && a1.promotionId === a2.promotionId;
    };

    $scope.promotionSelected = function(item, uiSelectObject) {
        var action = buildAction(item);
        $scope.addAction(action, comparer);
        uiSelectObject.selected = null;
    };

    $scope.isItemInSelectDisabled = function(item) {
        var action = buildAction(item);
        return $scope.isItemInSelectedActions(action, comparer);
    };

    $scope.initUiSelect = function(uiSelectController) {
        uiSelectController.isActive = function(item) {
            return false;
        };

        //workaround of existing ui-select issue - remove after upgrade of ui-select library on smartedit side
        $scope.availablePromotions = JSON.parse(JSON.stringify($scope.availablePromotions));
    };

    (function init() {
        getAvailablePromotions();
    })();
}]);

angular.module('personalizationsmarteditManagerViewModule', [
        'modalServiceModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'confirmationModalServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditCommerceCustomizationModule',
        'eventServiceModule',
        'personalizationsmarteditDataFactory',
        'waitDialogServiceModule'
    ])
    .factory('personalizationsmarteditManagerView', ['modalService', function(modalService) {
        var manager = {};
        manager.openManagerAction = function() {
            modalService.open({
                title: "personalization.modal.manager.title",
                templateUrl: 'personalizationsmarteditManagerViewTemplate.html',
                controller: 'personalizationsmarteditManagerViewController',
                size: 'fullscreen',
                cssClasses: 'perso-library'
            }).then(function(result) {

            }, function(failure) {});
        };

        return manager;
    }])
    .controller('personalizationsmarteditManagerViewController', ['$q', '$scope', '$filter', 'confirmationModalService', 'personalizationsmarteditRestService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditContextService', 'personalizationsmarteditManager', 'systemEventService', 'personalizationsmarteditUtils', 'personalizationsmarteditDateUtils', 'personalizationsmarteditCommerceCustomizationView', 'personalizationsmarteditCommerceCustomizationService', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', 'PERSONALIZATION_MODEL_STATUS_CODES', 'PaginationHelper', 'waitDialogService', function($q, $scope, $filter, confirmationModalService, personalizationsmarteditRestService, personalizationsmarteditMessageHandler, personalizationsmarteditContextService, personalizationsmarteditManager, systemEventService, personalizationsmarteditUtils, personalizationsmarteditDateUtils, personalizationsmarteditCommerceCustomizationView, personalizationsmarteditCommerceCustomizationService, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, PERSONALIZATION_MODEL_STATUS_CODES, PaginationHelper, waitDialogService) { //NOSONAR

        var getCustomizations = function(filter) {
            personalizationsmarteditRestService.getCustomizations(filter)
                .then(function successCallback(response) {
                    Array.prototype.push.apply($scope.customizations, response.customizations);
                    $scope.filteredCustomizationsCount = response.pagination.totalCount;
                    $scope.pagination = new PaginationHelper(response.pagination);
                    $scope.moreCustomizationsRequestProcessing = false;
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
                    $scope.moreCustomizationsRequestProcessing = false;
                });
        };

        var getCustomizationsFilterObject = function() {
            return {
                active: "all",
                name: $scope.search.name,
                currentSize: $scope.pagination.count,
                currentPage: $scope.pagination.page + 1,
                statuses: $scope.search.status.modelStatuses
            };
        };

        var refreshGrid = function() {
            $scope.pagination.reset();
            $scope.customizations.length = 0;
            $scope.addMoreItems();
        };

        var getDefaultStatus = function() {
            return $scope.statuses.filter(function(elem) {
                return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
            })[0];
        };

        var updateCustomizationRank = function(customizationCode, increaseValue) {
            var deferred = $q.defer();
            personalizationsmarteditRestService.updateCustomizationRank(customizationCode, increaseValue)
                .then(function successCallback() {
                    deferred.resolve();
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingcustomization'));
                    deferred.reject();
                });
            return deferred.promise;
        };

        var updateVariationRank = function(customization, variationCode, increaseValue) {
            var deferred = $q.defer();
            personalizationsmarteditRestService.getVariation(customization.code, variationCode)
                .then(function successCallback(responseVariation) {
                    responseVariation.rank = responseVariation.rank + increaseValue;
                    personalizationsmarteditRestService.editVariation(customization.code, responseVariation)
                        .then(function successCallback() {
                            deferred.resolve();
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.editingvariation'));
                            deferred.reject();
                        });
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingvariation'));
                    deferred.reject();
                });
            return deferred.promise;
        };

        var isLastInArray = function(array, item) {
            return array.indexOf(item) === array.length - 1;
        };

        var updateRanks = function(item, nextItem, itemsArray, event, increaseValue) {
            var startIndex = (increaseValue > 0) ? event.source.index : event.dest.index;
            var endIndex = (increaseValue > 0) ? event.dest.index : event.source.index;
            for (var i = startIndex; i <= endIndex; i++) {
                itemsArray[i].rank += (increaseValue > 0) ? (-1) : 1;
            }
            itemsArray[event.dest.index].rank = isLastInArray(itemsArray, item) ? nextItem.rank + 1 : nextItem.rank - 1;
        };

        var seExperienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
        var currentLanguageIsocode = seExperienceData.languageDescriptor.isocode;
        $scope.catalogName = seExperienceData.catalogDescriptor.name[currentLanguageIsocode];
        $scope.catalogName += " - " + seExperienceData.catalogDescriptor.catalogVersion;
        $scope.customizations = [];
        $scope.filteredCustomizationsCount = 0;
        $scope.scrollZoneElement = undefined;

        $scope.statuses = personalizationsmarteditUtils.getStatusesMapping();

        $scope.search = {
            name: '',
            status: getDefaultStatus()
        };

        $scope.moreCustomizationsRequestProcessing = false;
        $scope.addMoreItems = function() {
            if ($scope.pagination.page < $scope.pagination.totalPages - 1 && !$scope.moreCustomizationsRequestProcessing) {
                $scope.moreCustomizationsRequestProcessing = true;
                getCustomizations(getCustomizationsFilterObject());
            }
        };

        $scope.pagination = new PaginationHelper();
        $scope.pagination.reset();

        $scope.searchInputKeypress = function(keyEvent) {
            if (keyEvent.which === 13 || $scope.search.name.length > 2 || $scope.search.name.length === 0) {
                refreshGrid();
            }
        };

        $scope.resetSearchInput = function() {
            $scope.search.name = "";
            $scope.search.status = getDefaultStatus();
            refreshGrid();
        };

        $scope.editCustomizationAction = function(customization) {
            personalizationsmarteditManager.openEditCustomizationModal(customization.code);
        };
        $scope.deleteCustomizationAction = function(customization) {
            confirmationModalService.confirm({
                description: 'personalization.modal.manager.deletecustomization.content'
            }).then(function() {
                personalizationsmarteditRestService.getCustomization(customization.code)
                    .then(function successCallback(responseCustomization) {
                        responseCustomization.status = "DELETED";
                        personalizationsmarteditRestService.updateCustomization(responseCustomization)
                            .then(function successCallback() {
                                $scope.customizations.splice($scope.customizations.indexOf(customization), 1);
                            }, function errorCallback() {
                                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.deletingcustomization'));
                            });
                    }, function errorCallback() {
                        personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.deletingcustomization'));
                    });
            });
        };
        $scope.editVariationAction = function(customization, variation) {
            personalizationsmarteditManager.openEditCustomizationModal(customization.code, variation.code);
        };

        $scope.isCommerceCustomizationEnabled = function() {
            return personalizationsmarteditCommerceCustomizationService.isCommerceCustomizationEnabled(personalizationsmarteditContextService.getSeData().seConfigurationData);
        };

        $scope.manageCommerceCustomization = function(customization, variation) {
            personalizationsmarteditCommerceCustomizationView.openCommerceCustomizationAction(customization, variation);
        };

        $scope.isDeleteVariationEnabled = function(customization) {
            return personalizationsmarteditUtils.getVisibleItems(customization.variations).length > 1;
        };

        $scope.deleteVariationAction = function(customization, variation, $event) {
            if ($scope.isDeleteVariationEnabled(customization)) {
                confirmationModalService.confirm({
                    description: 'personalization.modal.manager.deletevariation.content'
                }).then(function() {
                    personalizationsmarteditRestService.getVariation(customization.code, variation.code)
                        .then(function successCallback(responseVariation) {
                            responseVariation.status = "DELETED";
                            personalizationsmarteditRestService.editVariation(customization.code, responseVariation)
                                .then(function successCallback(response) {
                                    variation.status = response.status;
                                }, function errorCallback() {
                                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.deletingvariation'));
                                });
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.deletingvariation'));
                        });
                });
            } else {
                $event.stopPropagation();
            }
        };

        $scope.openNewModal = function() {
            personalizationsmarteditManager.openCreateCustomizationModal();
        };

        $scope.toogleVariationActive = function(customization, variation) {
            personalizationsmarteditRestService.getVariation(customization.code, variation.code)
                .then(function successCallback(responseVariation) {
                    responseVariation.enabled = !responseVariation.enabled;
                    responseVariation.status = responseVariation.enabled ? PERSONALIZATION_MODEL_STATUS_CODES.ENABLED : PERSONALIZATION_MODEL_STATUS_CODES.DISABLED;
                    personalizationsmarteditRestService.editVariation(customization.code, responseVariation)
                        .then(function successCallback(response) {
                            variation.enabled = response.enabled;
                            variation.status = response.status;
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.editingvariation'));
                        });
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingvariation'));
                });
        };

        $scope.isUndefined = function(value) {
            return value === undefined;
        };

        $scope.customizationClickAction = function(customization) {
            personalizationsmarteditRestService.getVariationsForCustomization(customization.code).then(
                function successCallback(response) {
                    customization.variations = response.variations || [];
                    customization.variations.forEach(function(variation) {
                        variation.numberOfComponents = personalizationsmarteditCommerceCustomizationService.getNonCommerceActionsCount(variation);
                        variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                        variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                    });
                },
                function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });
        };

        $scope.getCommerceCustomizationTooltip = personalizationsmarteditUtils.getCommerceCustomizationTooltip;

        $scope.hasCommerceCustomization = function(variation) {
            return variation.numberOfCommerceActions > 0;
        };

        $scope.getFormattedDate = function(myDate) {
            if (myDate) {
                return personalizationsmarteditDateUtils.formatDate(myDate);
            } else {
                return "";
            }
        };

        $scope.getEnablementTextForCustomization = function(customization) {
            return personalizationsmarteditUtils.getEnablementTextForCustomization(customization, 'personalization.modal.manager');
        };

        $scope.getEnablementTextForVariation = function(variation) {
            return personalizationsmarteditUtils.getEnablementTextForVariation(variation, 'personalization.modal.manager');
        };

        $scope.getEnablementActionTextForVariation = function(variation) {
            return personalizationsmarteditUtils.getEnablementActionTextForVariation(variation, 'personalization.modal.manager');
        };

        $scope.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        $scope.getActivityStateForVariation = function(customization, variation) {
            return personalizationsmarteditUtils.getActivityStateForVariation(customization, variation);
        };

        $scope.allCustomizationsCollapsed = function() {
            return $scope.customizations.map(function(elem) {
                return elem.isCollapsed;
            }).reduce(function(previousValue, currentValue) {
                return previousValue && currentValue;
            }, true);
        };

        $scope.isReturnToTopButtonVisible = function() {
            return $scope.scrollZoneElement.scrollTop() > 50;
        };

        $scope.scrollZoneReturnToTop = function() {
            $scope.scrollZoneElement.animate({
                scrollTop: 0
            }, 500);
        };

        $scope.treeOptions = {
            dragStart: function(e) {
                $scope.scrollZoneVisible = $scope.isScrollZoneVisible();
                e.source.nodeScope.$modelValue.isDragging = true;
            },
            dragStop: function(e) {
                e.source.nodeScope.$modelValue.isDragging = undefined;
            },
            dropped: function(e) {
                $scope.scrollZoneVisible = false;
                var item = e.source.nodeScope.$modelValue;
                var nextItem, increaseValue;
                if (item.variations) { //Customization
                    nextItem = $scope.customizations[$scope.customizations.indexOf(item) + 1];
                    nextItem = nextItem ? nextItem : $scope.customizations[$scope.customizations.indexOf(item) - 1]; //Moved to last position
                    increaseValue = nextItem.rank - item.rank;
                    increaseValue = (increaseValue > 0 && !isLastInArray($scope.customizations, item)) ? increaseValue - 1 : increaseValue;
                    if (increaseValue !== 0) {
                        waitDialogService.showWaitModal();
                        updateCustomizationRank(item.code, increaseValue)
                            .then(function successCallback() {
                                updateRanks(item, nextItem, $scope.customizations, e, increaseValue);
                            }).finally(function() {
                                waitDialogService.hideWaitModal();
                            });
                    }
                } else { //Target group
                    var variationsArray = e.source.nodesScope.$modelValue;
                    nextItem = variationsArray[variationsArray.indexOf(item) + 1];
                    nextItem = nextItem ? nextItem : variationsArray[variationsArray.indexOf(item) - 1];
                    increaseValue = nextItem.rank - item.rank;
                    increaseValue = (increaseValue > 0 && !isLastInArray(variationsArray, item)) ? increaseValue - 1 : increaseValue;
                    var customization = e.source.nodesScope.$parent.$modelValue;
                    if (increaseValue !== 0) {
                        waitDialogService.showWaitModal();
                        updateVariationRank(customization, item.code, increaseValue)
                            .then(function successCallback() {
                                updateRanks(item, nextItem, customization.variations, e, increaseValue);
                            }).finally(function() {
                                waitDialogService.hideWaitModal();
                            });
                    }
                }
            },
            accept: function(sourceNodeScope, destNodesScope, destIndex) {
                if (angular.isArray(destNodesScope.$modelValue) && destNodesScope.$modelValue.indexOf(sourceNodeScope.$modelValue) > -1) {
                    return true;
                }
                return false;
            }
        };

        $scope.isScrollZoneVisible = function() {
            return $scope.scrollZoneElement.get(0).scrollHeight > $scope.scrollZoneElement.get(0).clientHeight;
        };

        $scope.isSearchResultHidden = function() {
            return $scope.scrollZoneElement.scrollTop() >= 125;
        };

        $scope.isSearchGridHeaderHidden = function() {
            return $scope.scrollZoneElement.scrollTop() >= 220;
        };

        $scope.setScrollZoneElement = function(element) {
            $scope.scrollZoneElement = element;
        };

        $scope.getElementToScroll = function() {
            return $scope.scrollZoneElement;
        };

        $scope.statusNotDeleted = function(variation) {
            return personalizationsmarteditUtils.isItemVisible(variation);
        };

        $scope.isFilterEnabled = function() {
            return $scope.search.name !== '' || $scope.search.status !== getDefaultStatus();
        };

        $scope.setCustomizationRank = function(customization, increaseValue) {
            var nextItem = $scope.customizations[$scope.customizations.indexOf(customization) + increaseValue];
            waitDialogService.showWaitModal();
            updateCustomizationRank(customization.code, nextItem.rank - customization.rank)
                .then(function successCallback() {
                    customization.rank += increaseValue;
                    nextItem.rank -= increaseValue;
                    var index = $scope.customizations.indexOf(customization);
                    var tempItem = $scope.customizations.splice(index, 1);
                    $scope.customizations.splice(index + increaseValue, 0, tempItem[0]);
                }).finally(function() {
                    waitDialogService.hideWaitModal();
                });
        };

        $scope.setVariationRank = function(customization, variation, increaseValue) {
            var nextItem = customization.variations[customization.variations.indexOf(variation) + increaseValue];
            waitDialogService.showWaitModal();
            updateVariationRank(customization, variation.code, increaseValue)
                .then(function successCallback() {
                    variation.rank += increaseValue;
                    nextItem.rank -= increaseValue;
                }).finally(function() {
                    waitDialogService.hideWaitModal();
                });
        };

        $scope.$watch('search.status', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                refreshGrid();
            }
        }, true);

        //init
        (function() {
            systemEventService.registerEventHandler('CUSTOMIZATIONS_MODIFIED', function() {
                refreshGrid();
                return $q.when();
            });
        })();

    }]);

angular.module('personalizationsmarteditManagerModule', [
        'modalServiceModule',
        'coretemplates',
        'ui.select',
        'confirmationModalServiceModule',
        'functionsModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditManagementServiceModule',
        'eventServiceModule',
        'personalizationsmarteditDataFactory',
        'sliderPanelModule',
        'seConstantsModule',
        'yjqueryModule'
    ])
    .constant('CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS', {
        BASIC_INFO_TAB_NAME: 'basicinfotab',
        BASIC_INFO_TAB_FORM_NAME: 'form.basicinfotab',
        TARGET_GROUP_TAB_NAME: 'targetgrptab',
        TARGET_GROUP_TAB_FORM_NAME: 'form.targetgrptab'
    })
    .constant('CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS', {
        CONFIRM_OK: 'confirmOk',
        CONFIRM_CANCEL: 'confirmCancel',
        CONFIRM_NEXT: 'confirmNext'
    })
    .constant('CUSTOMIZATION_VARIATION_MANAGEMENT_SEGMENTTRIGGER_GROUPBY', {
        CRITERIA_AND: 'AND',
        CRITERIA_OR: 'OR'
    })
    .factory('personalizationsmarteditManager', ['$controller', 'modalService', 'MODAL_BUTTON_ACTIONS', 'MODAL_BUTTON_STYLES', 'CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS', function($controller, modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS) {

        var manager = {};

        manager.openCreateCustomizationModal = function() {
            return modalService.open({
                title: 'personalization.modal.customizationvariationmanagement.title',
                templateUrl: 'personalizationsmarteditCustomizationManagTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    $scope.modalManager = modalManager;
                    angular.extend(this, $controller('personalizationsmarteditManagerController', {
                        $scope: $scope
                    }));
                }],
                buttons: [{
                    id: CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_CANCEL,
                    label: 'personalization.modal.customizationvariationmanagement.button.cancel',
                    style: MODAL_BUTTON_STYLES.SECONDARY
                }],
                size: 'lg modal_bigger sliderPanelParentModal'
            });
        };

        manager.openEditCustomizationModal = function(customizationCode, variationCode) {
            return modalService.open({
                title: 'personalization.modal.customizationvariationmanagement.title',
                templateUrl: 'personalizationsmarteditCustomizationManagTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    $scope.customizationCode = customizationCode;
                    $scope.variationCode = variationCode;
                    $scope.modalManager = modalManager;
                    angular.extend(this, $controller('personalizationsmarteditManagerController', {
                        $scope: $scope
                    }));
                }],
                buttons: [{
                    id: 'confirmCancel',
                    label: 'personalization.modal.customizationvariationmanagement.button.cancel',
                    style: MODAL_BUTTON_STYLES.SECONDARY
                }],
                size: 'lg modal_bigger sliderPanelParentModal'
            });
        };

        return manager;
    }])
    .controller('personalizationsmarteditManagerController', ['$scope', 'hitch', '$q', '$log', 'personalizationsmarteditManagementService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditUtils', 'personalizationsmarteditDateUtils', 'confirmationModalService', '$filter', 'MODAL_BUTTON_ACTIONS', 'CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS', 'CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS', 'systemEventService', 'CUSTOMIZATION_VARIATION_MANAGEMENT_SEGMENTTRIGGER_GROUPBY', 'PERSONALIZATION_DATE_FORMATS', 'PERSONALIZATION_MODEL_STATUS_CODES', 'personalizationsmarteditCommerceCustomizationService', 'customizationDataFactory', 'DATE_CONSTANTS', function($scope, hitch, $q, $log, personalizationsmarteditManagementService, personalizationsmarteditMessageHandler, personalizationsmarteditUtils, personalizationsmarteditDateUtils, confirmationModalService, $filter, MODAL_BUTTON_ACTIONS, CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS, CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS, systemEventService, CUSTOMIZATION_VARIATION_MANAGEMENT_SEGMENTTRIGGER_GROUPBY, PERSONALIZATION_DATE_FORMATS, PERSONALIZATION_MODEL_STATUS_CODES, personalizationsmarteditCommerceCustomizationService, customizationDataFactory, DATE_CONSTANTS) { //NOSONAR
        var self = this;

        var getVariationsForCustomization = function(customizationCode) {
            var filter = {
                includeFullFields: true
            };

            return personalizationsmarteditManagementService.getVariationsForCustomization(customizationCode, filter);
        };

        var createCommerceCustomizationData = function(variations) {
            variations.forEach(function(variation) {
                variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                delete variation.actions; //no more use for this property and it existence may be harmful
            });
        };

        self.isModallDirty = false;

        $scope.form = {};

        $scope.customization = {
            code: '',
            description: '',
            rank: 0,
            variations: [],
            active: false
        };
        $scope.activeTabNumber = 0;
        $scope.tabsArr = [{
            name: CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.BASIC_INFO_TAB_NAME,
            active: true,
            disabled: false,
            heading: $filter('translate')("personalization.modal.customizationvariationmanagement.basicinformationtab"),
            template: 'personalizationsmarteditCustVarManagBasicInfoTemplate.html',
            formName: CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.BASIC_INFO_TAB_FORM_NAME,
            isDirty: function() {
                return $scope.form.basicinfotab && $scope.form.basicinfotab.$dirty;
            },
            setPristine: function() {
                $scope.form.basicinfotab.$setPristine();
            },
            isValid: function() {
                return $scope.form.basicinfotab && $scope.form.basicinfotab.$valid;
            },
            setEnabled: function(enabled) {
                if (enabled) {
                    $scope.tabsArr[1].disabled = false;
                    $scope.modalManager.enableButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT);
                } else {
                    $scope.tabsArr[1].disabled = true;
                    $scope.modalManager.disableButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT);
                }
            },
            onConfirm: function() {
                $scope.activeTabNumber = 1;
            },
            onCancel: function() {
                self.onCancel();
            }
        }, {
            name: CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.TARGET_GROUP_TAB_NAME,
            active: false,
            disabled: true,
            heading: $filter('translate')("personalization.modal.customizationvariationmanagement.targetgrouptab"),
            template: 'personalizationsmarteditCustVarManagTargetGrpTemplate.html',
            formName: CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.TARGET_GROUP_TAB_FORM_NAME,
            isDirty: function() {
                return ($scope.form.targetgrptab && $scope.form.targetgrptab.$dirty) || $scope.edit.variationsListDirty;
            },
            setPristine: function() {
                $scope.form.targetgrptab.$setPristine();
            },
            isValid: function() {
                var isVariationListValid = personalizationsmarteditUtils.getVisibleItems($scope.customization.variations).length > 0;
                var isInVariationEditingMode = angular.isDefined($scope.edit.selectedVariation);
                return ($scope.form.targetgrptab && $scope.form.targetgrptab.$valid) && isVariationListValid && !isInVariationEditingMode;
            },
            setEnabled: function(enabled) {
                if (enabled) {
                    $scope.modalManager.enableButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK);
                } else {
                    $scope.modalManager.disableButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK);
                }
            },
            onConfirm: function() {
                self.onSave();
            },
            onCancel: function() {
                self.onCancel();
            }
        }];

        $scope.edit = {
            code: '',
            name: '',
            variationsListChanged: false,
            selectedTab: $scope.tabsArr[0],
            variationsLoaded: false,
            viewDateFormat: DATE_CONSTANTS.MOMENT_FORMAT,
            datetimeConfigurationEnabled: false
        };

        $scope.editMode = angular.isDefined($scope.customizationCode);

        if ($scope.editMode) {
            personalizationsmarteditManagementService.getCustomization($scope.customizationCode).then(function successCallback(response) {
                $scope.customization = response;

                $scope.customization.enabledStartDate = personalizationsmarteditDateUtils.formatDate($scope.customization.enabledStartDate);
                $scope.customization.enabledEndDate = personalizationsmarteditDateUtils.formatDate($scope.customization.enabledEndDate);
                $scope.customization.statusBoolean = ($scope.customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED);

                if ($scope.customization.enabledStartDate || $scope.customization.enabledEndDate) {
                    $scope.edit.datetimeConfigurationEnabled = true;
                }

                if (angular.isDefined($scope.variationCode)) {

                    getVariationsForCustomization($scope.customizationCode).then(function successCallback(response) {
                        createCommerceCustomizationData(response.variations);
                        $scope.customization.variations = response.variations;
                        $scope.edit.variationsLoaded = true;

                        var filteredCollection = $scope.customization.variations.filter(function(elem) {
                            return elem.code === $scope.variationCode;
                        });

                        if (filteredCollection.length > 0) {

                            $scope.activeTabNumber = 1;
                            $scope.edit.selectedTab = $scope.tabsArr[1];

                            var selVariation = filteredCollection[0];
                            $scope.edit.selectedVariation = selVariation;
                        }

                    }, function errorCallback(response) {
                        personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingsegments'));
                    });
                } else {
                    $scope.edit.selectedTab = $scope.tabsArr[0];
                }
            }, function errorCallback(response) {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponents'));
            });
        }

        $scope.selectTab = function(tab) {
            $scope.edit.selectedTab = tab;
            $scope.activeTabNumber = $scope.tabsArr.indexOf(tab);
            switch (tab.name) {
                case CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.BASIC_INFO_TAB_NAME:
                    $scope.modalManager.removeButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK);
                    if (!$scope.modalManager.getButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT)) {
                        $scope.modalManager.addButton({
                            id: CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT,
                            label: 'personalization.modal.customizationvariationmanagement.basicinformationtab.button.next'
                        });
                    }
                    break;
                case CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.TARGET_GROUP_TAB_NAME:
                    $scope.modalManager.removeButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT);
                    if (!$scope.modalManager.getButton(CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK)) {
                        $scope.modalManager.addButton({
                            id: CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK,
                            label: 'personalization.modal.customizationvariationmanagement.targetgrouptab.button.submit',
                            action: MODAL_BUTTON_ACTIONS.CLOSE
                        });
                    }
                    break;
                default:
                    break;
            }
        };

        self.onSave = function() {
            if ($scope.customization.enabledStartDate) {
                $scope.customization.enabledStartDate = personalizationsmarteditDateUtils.formatDate($scope.customization.enabledStartDate, PERSONALIZATION_DATE_FORMATS.MODEL_DATE_FORMAT);
            } else {
                $scope.customization.enabledStartDate = undefined;
            }

            if ($scope.customization.enabledEndDate) {
                $scope.customization.enabledEndDate = personalizationsmarteditDateUtils.formatDate($scope.customization.enabledEndDate, PERSONALIZATION_DATE_FORMATS.MODEL_DATE_FORMAT);
            } else {
                $scope.customization.enabledEndDate = undefined;
            }

            if ($scope.editMode) {
                personalizationsmarteditManagementService.updateCustomizationPackage($scope.customization).then(function successCallback(response) {
                    systemEventService.sendSynchEvent('CUSTOMIZATIONS_MODIFIED', {});
                    customizationDataFactory.refreshData();
                    personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.updatingcustomization'));
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingcustomization'));
                });
            } else {
                personalizationsmarteditManagementService.createCustomization($scope.customization).then(function successCallback(response) {
                    systemEventService.sendSynchEvent('CUSTOMIZATIONS_MODIFIED', {});
                    personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.creatingcustomization'));
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.creatingcustomization'));
                });
            }

        };

        self.onCancel = function() {
            var deferred = $q.defer();
            if (self.isModallDirty) {
                confirmationModalService.confirm({
                    description: $filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.cancelconfirmation')
                }).then(function() {
                    $scope.modalManager.dismiss();
                    deferred.resolve();
                }, function() {
                    deferred.reject();
                });
            } else {
                $scope.modalManager.dismiss();
                deferred.resolve();
            }

            return deferred.promise;
        };

        self.init = function() {
            $scope.modalManager.setDismissCallback(self.onCancel);

            $scope.modalManager.setButtonHandler(hitch(this, function(buttonId) {
                switch (buttonId) {
                    case CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_OK:
                        return $scope.edit.selectedTab.onConfirm();
                    case CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_NEXT:
                        return $scope.edit.selectedTab.onConfirm();
                    case CUSTOMIZATION_VARIATION_MANAGEMENT_BUTTONS.CONFIRM_CANCEL:
                        return $scope.edit.selectedTab.onCancel();
                    default:
                        $log.error($filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.invalidbuttonid'), buttonId);
                        break;
                }
            }));

            $scope.resetDateTimeConfiguration = function() {
                $scope.customization.enabledStartDate = undefined;
                $scope.customization.enabledEndDate = undefined;
            };

            $scope.$watch(hitch(this, function() {
                var isSelectedTabDirty = $scope.edit.selectedTab.isDirty();
                var isSelectedTabValid = $scope.edit.selectedTab.isValid();
                return {
                    isDirty: isSelectedTabDirty,
                    isValid: isSelectedTabValid
                };
            }), hitch(this, function(obj) {
                if (obj.isDirty) {
                    self.isModallDirty = true;
                    if (obj.isValid) {
                        $scope.edit.selectedTab.setEnabled(true);
                    } else {
                        $scope.edit.selectedTab.setEnabled(false);
                    }
                } else if ($scope.editMode) {
                    if (obj.isValid) {
                        $scope.edit.selectedTab.setEnabled(true);
                    } else {
                        $scope.edit.selectedTab.setEnabled(false);
                    }
                } else {
                    self.isModallDirty = false;
                    $scope.edit.selectedTab.setEnabled(false);
                }
            }), true);

            $scope.$watch('customization.variations', function() {
                if ($scope.edit.variationsLoaded || !$scope.editMode) {
                    if ($scope.edit.variationsListDirty === false) {
                        $scope.edit.variationsListDirty = true;
                    } else {
                        $scope.edit.variationsListDirty = false;
                    }
                }
            }, true);

            $scope.$watch('customization.enabledEndDate', function() {
                if ($scope.form.basicinfotab) {
                    $scope.form.basicinfotab.date_to_key.$validate();
                }
                $scope.isEndDateInThePast = personalizationsmarteditDateUtils.isDateInThePast($scope.customization.enabledEndDate);
            }, true);

            $scope.$watch('edit.selectedTab', function() {
                if ($scope.editMode && !$scope.edit.variationsLoaded && ($scope.edit.selectedTab && $scope.edit.selectedTab.name === CUSTOMIZATION_VARIATION_MANAGEMENT_TABS_CONSTANTS.TARGET_GROUP_TAB_NAME)) {

                    getVariationsForCustomization($scope.customizationCode).then(function successCallback(response) {
                        createCommerceCustomizationData(response.variations);
                        $scope.customization.variations = response.variations;
                        $scope.edit.variationsLoaded = true;
                    }, function errorCallback(response) {
                        personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingsegments'));
                    });
                }
            }, true);
        };
    }])
    .directive('uniquetargetgroupname',
        ['isBlank', function(isBlank) {
            var isNameTheSameAsEditedTargetGroup = function(scope, targetGroupName) {
                return scope.edit.selectedVariation && targetGroupName === scope.edit.selectedVariation.code;
            };

            return {
                require: "ngModel",
                scope: false,
                link: function(scope, element, attributes, ctrl) {
                    ctrl.$validators.uniquetargetgroupname = function(modelValue) {
                        if (isBlank(modelValue) || isNameTheSameAsEditedTargetGroup(scope, modelValue)) {
                            return true;
                        } else {
                            return scope.customization.variations.filter(function(e) {
                                return e.code === modelValue;
                            }).length === 0;
                        }
                    };
                }
            };
        }]
    );

angular.module('personalizationsmarteditManagementServiceModule', [
        'personalizationsmarteditRestServiceModule',
        'sharedDataServiceModule',
        'personalizationsmarteditCommons'
    ])
    .factory('personalizationsmarteditManagementService', ['personalizationsmarteditRestService', function(personalizationsmarteditRestService) {
        var ManagementService = {};

        ManagementService.getSegments = function(filter) {
            return personalizationsmarteditRestService.getSegments(filter);
        };

        ManagementService.getCustomization = function(customizationCode) {
            return personalizationsmarteditRestService.getCustomization(customizationCode);
        };

        ManagementService.getVariation = function(customizationCode, variationCode) {
            return personalizationsmarteditRestService.getVariation(customizationCode, variationCode);
        };

        ManagementService.createCustomization = function(customization) {
            return personalizationsmarteditRestService.createCustomization(customization);
        };

        ManagementService.updateCustomizationPackage = function(customization) {
            return personalizationsmarteditRestService.updateCustomizationPackage(customization);
        };


        ManagementService.createVariationForCustomization = function(customizationCode, variation) {
            return personalizationsmarteditRestService.createVariationForCustomization(customizationCode, variation);
        };

        ManagementService.getVariationsForCustomization = function(customizationCode, filter) {
            return personalizationsmarteditRestService.getVariationsForCustomization(customizationCode, filter);
        };

        return ManagementService;
    }]);

angular.module('personalizationsmarteditManagerModule')
    .service('personalizationsmarteditTriggerService', function() {
        var triggerService = {};

        var DEFAULT_TRIGGER = 'defaultTriggerData';
        var SEGMENT_TRIGGER = 'segmentTriggerData';
        var EXPRESSION_TRIGGER = 'expressionTriggerData';

        var GROUP_EXPRESSION = 'groupExpressionData';
        var SEGMENT_EXPRESSION = 'segmentExpressionData';
        var NEGATION_EXPRESSION = 'negationExpressionData';

        var CONTAINER_TYPE = 'container';
        var ITEM_TYPE = 'item';
        var DROPZONE_TYPE = 'dropzone';

        var supportedTypes = [DEFAULT_TRIGGER, SEGMENT_TRIGGER, EXPRESSION_TRIGGER];
        var actions = [{
            id: 'AND',
            name: 'personalization.modal.customizationvariationmanagement.targetgrouptab.expression.and'
        }, {
            id: 'OR',
            name: 'personalization.modal.customizationvariationmanagement.targetgrouptab.expression.or'
        }, {
            id: 'NOT',
            name: 'personalization.modal.customizationvariationmanagement.targetgrouptab.expression.not'
        }];

        var isElementOfType = function(element, type) {
            return angular.isDefined(element) ? (element.type === type) : false;
        };

        var isDropzone = function(element) {
            return isElementOfType(element, DROPZONE_TYPE);
        };

        var isItem = function(element) {
            return isElementOfType(element, ITEM_TYPE);
        };

        var isContainer = function(element) {
            return isElementOfType(element, CONTAINER_TYPE);
        };

        var isEmptyContainer = function(element) {
            return isContainer(element) && element.nodes.length === 0;
        };

        var isNotEmptyContainer = function(element) {
            return isContainer(element) && element.nodes.length > 0;
        };

        var isNegation = function(element) {
            return isContainer(element) && element.operation.id === 'NOT';
        };

        var isDefaultData = function(form) {
            return form.isDefault;
        };

        var isExpressionData = function(element) {
            return element.operation.id === 'NOT' || element.nodes.some(function(item) {
                return !isItem(item);
            });
        };

        var isValidExpression = function(element) {
            if (!element) {
                return false;
            }
            if (isContainer(element)) {
                return element.nodes && element.nodes.length > 0 && element.nodes.every(isValidExpression);
            } else {
                return typeof element.selectedSegment !== 'undefined';
            }
        };

        var isSupportedTrigger = function(trigger) {
            return supportedTypes.indexOf(trigger.type) >= 0;
        };

        var isDefaultTrigger = function(trigger) {
            return isElementOfType(trigger, DEFAULT_TRIGGER);
        };

        var isSegmentTrigger = function(trigger) {
            return isElementOfType(trigger, SEGMENT_TRIGGER);
        };

        var isExpressionTrigger = function(trigger) {
            return isElementOfType(trigger, EXPRESSION_TRIGGER);
        };

        var isGroupExpressionData = function(expression) {
            return isElementOfType(expression, GROUP_EXPRESSION);
        };

        var isSegmentExpressionData = function(expression) {
            return isElementOfType(expression, SEGMENT_EXPRESSION);
        };

        var isNegationExpressionData = function(expression) {
            return isElementOfType(expression, NEGATION_EXPRESSION);
        };

        var isDefault = function(triggers) {
            return (triggers && triggers.find(isDefaultTrigger)) ? true : false;
        };

        var getExpressionAsString = function(expressionContainer) {
            var retStr = "";
            if (expressionContainer === undefined) {
                return retStr;
            }

            var currOperator = isNegation(expressionContainer) ? "AND" : expressionContainer.operation.id;
            retStr += isNegation(expressionContainer) ? " NOT " : "";
            retStr += "(";

            expressionContainer.nodes.forEach(function(element, index, array) {
                if (isDropzone(element)) {
                    retStr += " [] ";
                } else {
                    retStr += (index > 0) ? " " + currOperator + " " : "";
                    retStr += isItem(element) ? element.selectedSegment.code : getExpressionAsString(element);
                }
            });

            retStr += ")";

            return retStr;
        };

        //------------------------ FORM DATA -> TRIGGER ---------------------------

        var buildSegmentsForTrigger = function(element) {
            return element.nodes.filter(isItem).map(function(item) {
                return item.selectedSegment;
            });
        };

        var buildExpressionForTrigger = function(element) {
            if (isNegation(element)) {
                var negationElements = Array.from(element.nodes, buildExpressionForTrigger);

                return {
                    type: NEGATION_EXPRESSION,
                    element: {
                        type: GROUP_EXPRESSION,
                        operator: 'AND',
                        elements: negationElements
                    }
                };
            } else if (isContainer(element)) {
                var groupElements = Array.from(element.nodes, buildExpressionForTrigger);
                return {
                    type: GROUP_EXPRESSION,
                    operator: element.operation.id,
                    elements: groupElements
                };
            } else {
                return {
                    type: SEGMENT_EXPRESSION,
                    code: element.selectedSegment.code
                };
            }
        };

        var buildDefaultTrigger = function() {
            return {
                type: DEFAULT_TRIGGER
            };
        };

        var buildExpressionTrigger = function(element) {
            var expression = buildExpressionForTrigger(element);

            return {
                type: EXPRESSION_TRIGGER,
                expression: expression
            };
        };

        var buildSegmentTrigger = function(element) {
            var groupBy = element.operation.id;
            var segments = buildSegmentsForTrigger(element);
            return {
                type: SEGMENT_TRIGGER,
                groupBy: groupBy,
                segments: segments
            };
        };

        var mergeTriggers = function(triggers, trigger) {
            if (!angular.isDefined(triggers)) {
                return [trigger];
            }

            var index = triggers.findIndex(function(t) {
                return t.type === trigger.type;
            });
            if (index >= 0) {
                trigger.code = triggers[index].code;
            }

            //remove other instanced of supported types (there can be only one) but maintain unsupported types
            var result = triggers.filter(function(t) {
                return !isSupportedTrigger(t);
            });
            result.push(trigger);
            return result;
        };

        var buildTriggers = function(form, existingTriggers) {
            var trigger = {};
            form = form || {};

            if (isDefaultData(form)) {
                trigger = buildDefaultTrigger();
            } else if (form.expression && form.expression.length > 0) {
                var element = form.expression[0];
                if (isExpressionData(element)) {
                    trigger = buildExpressionTrigger(element);
                } else {
                    trigger = buildSegmentTrigger(element);
                }
            }

            return mergeTriggers(existingTriggers, trigger);
        };

        //------------------------ TRIGGER -> FORM DATA ---------------------------

        var buildContainer = function(actionId) {
            var action = actions.find(function(a) {
                return a.id === actionId;
            });
            return {
                type: CONTAINER_TYPE,
                operation: action,
                nodes: []
            };
        };

        var buildItem = function(value) {
            return {
                type: ITEM_TYPE,
                operation: '',
                selectedSegment: {
                    code: value
                },
                nodes: []
            };
        };

        var getBaseData = function() {
            var data = buildContainer('AND');
            return [data];
        };

        var buildExpressionFromTrigger = function(expression) {
            var data = {};
            if (isGroupExpressionData(expression)) {
                data = buildContainer(expression.operator);
                data.nodes = expression.elements.map(function(item) {
                    return buildExpressionFromTrigger(item);
                });
            } else if (isNegationExpressionData(expression)) {
                data = buildContainer('NOT');
                var element = buildExpressionFromTrigger(expression.element);

                if (isGroupExpressionData(expression.element) && expression.element.operator === 'AND') {
                    data.nodes = element.nodes;
                } else {
                    data.nodes.push(element);
                }
            } else if (isSegmentExpressionData(expression)) {
                data = buildItem(expression.code);
            }
            return data;
        };

        var buildSegmentTriggerData = function(trigger) {
            var data = buildContainer(trigger.groupBy);

            trigger.segments.forEach(function(segment) {
                data.nodes.push(buildItem(segment.code));
            });
            return [data];
        };

        var buildExpressionTriggerData = function(trigger) {
            var data = buildExpressionFromTrigger(trigger.expression);
            return [data];
        };

        var buildData = function(triggers) {
            var trigger = {};
            var data = getBaseData();
            if (triggers && triggers.length > 0) {
                trigger = triggers.find(isSupportedTrigger);
            }
            if (isDefaultTrigger(trigger)) {
                //nothing to do here
                //we leave baseData - it will be used if user removes default trigger
            } else if (isExpressionTrigger(trigger)) {
                data = buildExpressionTriggerData(trigger);
            } else if (isSegmentTrigger(trigger)) {
                data = buildSegmentTriggerData(trigger);
            }
            return data;
        };

        //------------------------ PUBLIC ---------------------------
        triggerService.isContainer = isContainer;
        triggerService.isNotEmptyContainer = isNotEmptyContainer;
        triggerService.isEmptyContainer = isEmptyContainer;
        triggerService.isItem = isItem;
        triggerService.isDropzone = isDropzone;
        triggerService.isValidExpression = isValidExpression;
        triggerService.buildTriggers = buildTriggers;
        triggerService.buildData = buildData;
        triggerService.isDefault = isDefault;
        triggerService.getExpressionAsString = getExpressionAsString;

        triggerService.actions = actions;

        return triggerService;
    })
    .controller('personalizationsmarteditManagerTargetGrpController', ['$scope', 'isBlank', '$filter', 'yjQuery', 'personalizationsmarteditUtils', 'personalizationsmarteditManagementService', 'personalizationsmarteditTriggerService', 'personalizationsmarteditMessageHandler', 'confirmationModalService', 'CUSTOMIZATION_VARIATION_MANAGEMENT_SEGMENTTRIGGER_GROUPBY', 'PERSONALIZATION_MODEL_STATUS_CODES', '$timeout', function($scope, isBlank, $filter, yjQuery, personalizationsmarteditUtils, personalizationsmarteditManagementService, personalizationsmarteditTriggerService, personalizationsmarteditMessageHandler, confirmationModalService, CUSTOMIZATION_VARIATION_MANAGEMENT_SEGMENTTRIGGER_GROUPBY, PERSONALIZATION_MODEL_STATUS_CODES, $timeout) {

        var self = this;
        $scope.edit.expression = [];
        $scope.edit.isDefault = false;
        $scope.edit.showExpression = true;

        this.sliderPanelConfiguration = {
            modal: {
                showDismissButton: true,
                title: "personalization.modal.customizationvariationmanagement.targetgrouptab.slidingpanel.title",
                cancel: {
                    label: "personalization.modal.customizationvariationmanagement.targetgrouptab.cancelchanges",
                    onClick: function() {
                        self.cancelChangesClick();
                    }
                },
                dismiss: {
                    onClick: function() {
                        self.cancelChangesClick();
                    }
                },
                save: {}
            },
            cssSelector: "#y-modal-dialog"
        };

        var clearEditedVariationDetails = function() {
            $scope.edit.code = '';
            $scope.edit.name = '';
            $scope.edit.expression = [];
            $scope.edit.isDefault = false;
            $scope.edit.showExpression = true;
        };

        this.addVariationClick = function() {
            var triggers = personalizationsmarteditTriggerService.buildTriggers($scope.edit);

            $scope.customization.variations.push({
                code: $scope.edit.code,
                name: $scope.edit.name,
                enabled: true,
                status: PERSONALIZATION_MODEL_STATUS_CODES.ENABLED,
                triggers: triggers,
                rank: $scope.customization.variations.length,
                isNew: true
            });

            clearEditedVariationDetails();
            $scope.toggleSliderFullscreen(false);
            $timeout((function() {
                self.hideSliderPanel();
            }), 0);
        };

        this.submitChangesClick = function() {

            var triggers = personalizationsmarteditTriggerService.buildTriggers($scope.edit, $scope.edit.selectedVariation.triggers || []);
            $scope.edit.selectedVariation.triggers = triggers;

            $scope.edit.selectedVariation.name = $scope.edit.name;
            $scope.edit.selectedVariation = undefined;
            $scope.toggleSliderFullscreen(false);
        };

        this.cancelChangesClick = function() {
            if ($scope.isVariationSelected()) {
                $scope.edit.selectedVariation = undefined;
            } else {
                clearEditedVariationDetails();

                $timeout((function() {
                    self.hideSliderPanel();
                }), 0);
            }
            $scope.toggleSliderFullscreen(false);
        };

        $scope.removeVariationClick = function(variation) {
            confirmationModalService.confirm({
                description: 'personalization.modal.manager.targetgrouptab.deletevariation.content'
            }).then(function() {
                if (variation.isNew) {
                    $scope.customization.variations.splice($scope.customization.variations.indexOf(variation), 1);
                } else {
                    variation.status = "DELETED";
                }
                $scope.edit.selectedVariation = undefined;
                $scope.recalculateRanksForVariations();
            });
        };

        $scope.setVariationRank = function(variation, increaseValue, $event, firstOrLast) {
            if (firstOrLast) {
                $event.stopPropagation();
            } else {
                var from = $scope.customization.variations.indexOf(variation);
                var to = personalizationsmarteditUtils.getValidRank($scope.customization.variations, variation, increaseValue);
                var variationsArr = $scope.customization.variations;
                if (to >= 0 && to < variationsArr.length) {
                    variationsArr.splice(to, 0, variationsArr.splice(from, 1)[0]);
                    $scope.recalculateRanksForVariations();
                }
            }
        };

        $scope.toogleVariationActive = function(variation) {
            variation.enabled = !variation.enabled;
            variation.status = variation.enabled ? PERSONALIZATION_MODEL_STATUS_CODES.ENABLED : PERSONALIZATION_MODEL_STATUS_CODES.DISABLED;
        };

        $scope.recalculateRanksForVariations = function() {
            $scope.customization.variations.forEach(function(part, index) {
                $scope.customization.variations[index].rank = index;
            });
        };

        $scope.editVariationAction = function(variation) {
            $scope.edit.selectedVariation = variation;
        };

        this.canSaveVariation = function() {
            return !isBlank($scope.edit.name) && ($scope.edit.isDefault || personalizationsmarteditTriggerService.isValidExpression($scope.edit.expression[0]));
        };

        $scope.isVariationSelected = function() {
            return angular.isDefined($scope.edit.selectedVariation);
        };

        $scope.getSegmentLenghtForVariation = function(variation) {
            var segments = personalizationsmarteditUtils.getSegmentTriggerForVariation(variation).segments || [];
            return segments.length;
        };

        $scope.getEnablementTextForVariation = function(variation) {
            return '(' + personalizationsmarteditUtils.getEnablementTextForVariation(variation, 'personalization.modal.customizationvariationmanagement.targetgrouptab') + ')';
        };

        $scope.getActivityActionTextForVariation = function(variation) {
            if (variation.enabled) {
                return $filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.disable');
            } else {
                return $filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.enable');
            }
        };

        $scope.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        $scope.getActivityStateForVariation = function(customization, variation) {
            return personalizationsmarteditUtils.getActivityStateForVariation(customization, variation);
        };

        $scope.getCommerceCustomizationText = function(variation) {
            var result = "";
            angular.forEach(variation.commerceCustomizations, function(propertyValue, propertyKey) {
                result += propertyValue + ' ' + $filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.commercecustomization.' + propertyKey);
            });
            return result;
        };

        $scope.hasCommerceCustomization = function(variation) {
            return variation.numberOfCommerceActions > 0;
        };

        $scope.toggleSliderFullscreen = function(enableFullscreen) {
            var modalObject = angular.element(".sliderPanelParentModal");
            var className = "modal-fullscreen";
            if (modalObject.hasClass(className) || enableFullscreen === false) {
                modalObject.removeClass(className);
            } else {
                modalObject.addClass(className);
            }
            $timeout((function() {
                yjQuery(window).resize();
            }), 0);
        };

        $scope.confirmDefaultTrigger = function(isDefault) {
            if (isDefault && personalizationsmarteditTriggerService.isValidExpression($scope.edit.expression[0])) {
                confirmationModalService.confirm({
                    description: 'personalization.modal.manager.targetgrouptab.defaulttrigger.content'
                }).then(function() {
                    $scope.edit.showExpression = false;
                }, function() {
                    $scope.edit.isDefault = false;
                });
            } else {
                $scope.edit.showExpression = !isDefault;
            }
        };

        $scope.isDefaultVariation = function(variation) {
            return personalizationsmarteditTriggerService.isDefault(variation.triggers);
        };

        var setSliderConfigForAdd = function() {
            self.sliderPanelConfiguration.modal.save.label = "personalization.modal.customizationvariationmanagement.targetgrouptab.addvariation";
            self.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
                return !self.canSaveVariation();
            };
            self.sliderPanelConfiguration.modal.save.onClick = function() {
                self.addVariationClick();
            };
        };

        var setSliderConfigForEditing = function() {
            self.sliderPanelConfiguration.modal.save.label = "personalization.modal.customizationvariationmanagement.targetgrouptab.savechanges";
            self.sliderPanelConfiguration.modal.save.isDisabledFn = function() {
                return !self.canSaveVariation();
            };
            self.sliderPanelConfiguration.modal.save.onClick = function() {
                self.submitChangesClick();
            };
        };

        $scope.$watch('customization.statusBoolean', function() {
            $scope.customization.status = $scope.customization.statusBoolean ? PERSONALIZATION_MODEL_STATUS_CODES.ENABLED : PERSONALIZATION_MODEL_STATUS_CODES.DISABLED;
        }, true);

        $scope.$watch('edit.selectedVariation', function(variation) {
            if (variation) {
                setSliderConfigForEditing();
                $scope.edit.code = variation.code;
                $scope.edit.name = variation.name;
                $scope.edit.isDefault = personalizationsmarteditTriggerService.isDefault(variation.triggers);
                $scope.edit.showExpression = !$scope.edit.isDefault;

                $timeout((function() {
                    self.showSliderPanel();
                }), 0);
            } else {
                clearEditedVariationDetails();

                setSliderConfigForAdd();

                $timeout((function() {
                    self.hideSliderPanel();
                }), 0);
            }
        }, true);

    }]);

angular.module('personalizationsmarteditSegmentViewModule', [
        'modalServiceModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'confirmationModalServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditCommerceCustomizationModule',
        'eventServiceModule',
        'personalizationsmarteditDataFactory',
        'ui.tree',
        'personalizationsmarteditScrollZone',
        'personalizationsmarteditSegmentExpressionAsHtml'
    ])
    .controller('personalizationsmarteditSegmentViewController', ['personalizationsmarteditRestService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditTriggerService', 'PaginationHelper', '$timeout', '$filter', 'confirmationModalService', function(personalizationsmarteditRestService, personalizationsmarteditMessageHandler, personalizationsmarteditTriggerService, PaginationHelper, $timeout, $filter, confirmationModalService) {
        var self = this;

        this.actions = personalizationsmarteditTriggerService.actions;

        //Properties
        var elementToScrollHeight = 0;
        Object.defineProperty(this, 'elementToScrollHeight', {
            get: function() {
                return elementToScrollHeight;
            },
            set: function(newVal) {
                elementToScrollHeight = newVal;
            }
        });

        var getSegmentsFilterObject = function() {
            return {
                code: self.segmentFilter.code,
                pageSize: self.segmentPagination.count,
                currentPage: self.segmentPagination.page + 1
            };
        };

        var findElementAndDuplicate = function(element, index, array) {
            var elementToDuplicate = this; //'this' is additional argument passed to function so in this case it is 'elementToDuplicate'
            if (elementToDuplicate === element) {
                array.splice(index, 0, angular.copy(elementToDuplicate));
                return true;
            }
            if (self.isContainer(element)) {
                element.nodes.some(findElementAndDuplicate, elementToDuplicate); //recursive call to check all sub containers
            }
            return false;
        };

        var dropzoneItem = {
            type: 'dropzone'
        };

        var initExpression = [{
            'type': 'container',
            'operation': self.actions[0],
            'nodes': [dropzoneItem]
        }];

        var removeDropzoneItem = function(nodes) {
            nodes.forEach(function(element, index, array) {
                if (self.isDropzone(element)) {
                    array.splice(index, 1);
                }
            });
        };

        var fixEmptyContainer = function(nodes) {
            nodes.forEach(function(element) {
                if (self.isEmptyContainer(element)) {
                    element.nodes.push(dropzoneItem);
                }
                if (self.isContainer(element)) {
                    fixEmptyContainer(element.nodes);
                }
            });
        };

        this.treeOptions = {
            dragStart: function(e) {
                self.scrollZoneVisible = self.isScrollZoneVisible();
            },
            dragStop: function(e) {
                self.scrollZoneVisible = false;
                removeDropzoneItem(e.dest.nodesScope.$nodeScope.$modelValue.nodes);
                fixEmptyContainer(self.expression);
            },
            dragMove: function(e) {
                self.highlightedContainer = e.dest.nodesScope.$nodeScope.$modelValue.$$hashKey;
                if (self.isScrollZoneVisible() !== self.scrollZoneVisible) {
                    self.scrollZoneVisible = self.isScrollZoneVisible();
                } else if (Math.abs(self.elementToScrollHeight - self.elementToScroll.get(0).scrollHeight) > 10) {
                    self.elementToScrollHeight = self.elementToScroll.get(0).scrollHeight;
                    self.scrollZoneVisible = false;
                }
            }
        };

        this.isScrollZoneVisible = function() {
            return self.elementToScroll.get(0).scrollHeight > self.elementToScroll.get(0).clientHeight;
        };

        this.getElementToScroll = function() {
            return self.elementToScroll;
        };

        this.removeItem = function(scope) {
            if (personalizationsmarteditTriggerService.isNotEmptyContainer(scope.$modelValue) && !self.isContainerWithDropzone(scope.$modelValue)) {
                confirmationModalService.confirm({
                    description: $filter('translate')('personalization.modal.customizationvariationmanagement.targetgrouptab.segments.removecontainerconfirmation')
                }).then(function() {
                    scope.remove();
                    fixEmptyContainer(self.expression);
                });
            } else {
                scope.remove();
                fixEmptyContainer(self.expression);
            }
        };

        this.duplicateItem = function(elementToDuplicate) {
            self.expression[0].nodes.some(findElementAndDuplicate, elementToDuplicate);
        };

        this.toggle = function(scope) {
            scope.toggle();
        };

        this.newSubItem = function(scope, type) {
            var nodeData = scope.$modelValue;
            removeDropzoneItem(nodeData.nodes);
            nodeData.nodes.unshift({
                type: type,
                operation: (type === 'item' ? '' : self.actions[0]),
                nodes: [dropzoneItem]
            });
            scope.expand();
            $timeout(function() {
                var childArray = scope.childNodes();
                childArray[0].expand();
            }, 0);
        };

        this.segments = [];
        this.segmentPagination = new PaginationHelper();
        this.segmentPagination.reset();
        this.segmentFilter = {
            code: ''
        };

        this.segmentSearchInputKeypress = function(keyEvent, searchObj) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }
            self.segmentPagination.reset();
            self.segmentFilter.code = searchObj;
            self.segments.length = 0;
            self.addMoreSegmentItems();
        };

        this.segmentSelectedEvent = function(item, itemsToSelect) {
            self.expression[0].nodes.unshift({
                type: 'item',
                operation: '',
                selectedSegment: item,
                nodes: []
            });
            self.singleSegment = null;
            self.highlightedContainer = self.expression[0].$$hashKey;
            removeDropzoneItem(self.expression[0].nodes);
        };

        this.moreSegmentRequestProcessing = false;
        this.addMoreSegmentItems = function() {
            if (self.segmentPagination.page < self.segmentPagination.totalPages - 1 && !self.moreSegmentRequestProcessing) {
                self.moreSegmentRequestProcessing = true;
                personalizationsmarteditRestService.getSegments(getSegmentsFilterObject()).then(function successCallback(response) {
                    Array.prototype.push.apply(self.segments, response.segments);
                    self.segmentPagination = new PaginationHelper(response.pagination);
                    self.moreSegmentRequestProcessing = false;
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingsegments'));
                    self.moreSegmentRequestProcessing = false;
                });
            }
        };

        this.isTopContainer = function(element) {
            return angular.equals(self.expression[0], element.node);
        };

        this.isEmptyContainer = function(element) {
            return self.isContainer(element) && element.nodes.length === 0;
        };

        this.isContainerWithDropzone = function(element) {
            return self.isContainer(element) && element.nodes.length === 1 && self.isDropzone(element.nodes[0]);
        };

        this.isItem = personalizationsmarteditTriggerService.isItem;
        this.isDropzone = personalizationsmarteditTriggerService.isDropzone;
        this.isContainer = personalizationsmarteditTriggerService.isContainer;

        this.isHighlightedContainer = function(node) {
            return node.$$hashKey === self.highlightedContainer;
        };

        //Lifecycle methods
        this.$onInit = function() {
            if (self.triggers && self.triggers.length > 0) {
                self.expression = personalizationsmarteditTriggerService.buildData(self.triggers);
            } else {
                self.expression = angular.copy(initExpression);
            }
            self.elementToScroll = angular.element(".sliderpanel-body");
        };

    }])
    .component('multiSegmentView', {
        templateUrl: 'personalizationsmarteditSegmentViewTemplate.html',
        controller: 'personalizationsmarteditSegmentViewController',
        controllerAs: 'ctrl',
        bindings: {
            triggers: '<',
            expression: '='
        }
    });

angular.module('personalizationsmarteditSegmentExpressionAsHtml', [])
    .controller('personalizationsmarteditSegmentExpressionAsHtmlController', ['personalizationsmarteditTriggerService', function(personalizationsmarteditTriggerService) {
        var self = this;

        //Properties
        var segmentExpression = {};
        Object.defineProperty(this, 'segmentExpression', {
            get: function() {
                return segmentExpression;
            },
            set: function(newVal) {
                segmentExpression = newVal;
            }
        });

        var operators = ['AND', 'OR', 'NOT'];
        Object.defineProperty(this, 'operators', {
            get: function() {
                return operators;
            }
        });

        var emptyGroup = '[]';
        Object.defineProperty(this, 'emptyGroup', {
            get: function() {
                return emptyGroup;
            }
        });

        var emptyGroupAndOperators = self.operators.concat(emptyGroup);
        Object.defineProperty(this, 'emptyGroupAndOperators', {
            get: function() {
                return emptyGroupAndOperators;
            }
        });

        //Methods

        //A segmentExpression parameter can be 'variation.triggers' object or 'segmentExpression' object
        //If variations.triggers is passed it will converted to segmentExpression
        this.getExpressionAsArray = function() {
            if (angular.isDefined(self.segmentExpression) && !angular.isDefined(self.segmentExpression.operation)) {
                self.segmentExpression = personalizationsmarteditTriggerService.buildData(self.segmentExpression)[0];
            }
            return personalizationsmarteditTriggerService.getExpressionAsString(self.segmentExpression).split(" ");
        };

        this.getLocalizationKeyForOperator = function(operator) {
            return 'personalization.modal.customizationvariationmanagement.targetgrouptab.expression.' + operator.toLowerCase();
        };

    }])
    .component('personalizationsmarteditSegmentExpressionAsHtml', {
        templateUrl: 'personalizationsmarteditSegmentExpressionAsHtmlTemplate.html',
        controller: 'personalizationsmarteditSegmentExpressionAsHtmlController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            segmentExpression: '<'
        }
    });

angular.module('pageCustomizationsListModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditPreviewServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .controller('pageCustomizationsListController', ['$timeout', '$filter', 'personalizationsmarteditContextService', 'personalizationsmarteditRestService', 'personalizationsmarteditCommerceCustomizationService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditUtils', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditDateUtils', 'personalizationsmarteditContextUtils', 'personalizationsmarteditPreviewService', 'personalizationsmarteditManager', function($timeout, $filter, personalizationsmarteditContextService, personalizationsmarteditRestService, personalizationsmarteditCommerceCustomizationService, personalizationsmarteditMessageHandler, personalizationsmarteditUtils, personalizationsmarteditIFrameUtils, personalizationsmarteditDateUtils, personalizationsmarteditContextUtils, personalizationsmarteditPreviewService, personalizationsmarteditManager) {
        var self = this;

        //Private methods
        var updateCustomizationData = function(customization) {
            personalizationsmarteditRestService.getVariationsForCustomization(customization.code).then(
                function successCallback(response) {
                    customization.variations = response.variations || [];
                    customization.variations.forEach(function(variation) {
                        variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                        variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                    });
                },
                function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });
        };

        var getVisibleVariations = function(customization) {
            return personalizationsmarteditUtils.getVisibleItems(customization.variations);
        };

        var getAndSetComponentsForVariation = function(customizationId, variationId) {
            personalizationsmarteditRestService.getComponenentsIdsForVariation(customizationId, variationId).then(function successCallback(response) {
                var customize = personalizationsmarteditContextService.getCustomize();
                customize.selectedComponents = response.components;
                personalizationsmarteditContextService.setCustomize(customize);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponentsforvariation'));
            });
        };

        var updatePreviewTicket = function(customizationId, variationArray) {
            var previewTicketId = personalizationsmarteditContextService.getSeData().sePreviewData.previewTicketId;
            var variationKeys = personalizationsmarteditUtils.getVariationKey(customizationId, variationArray);
            personalizationsmarteditPreviewService.updatePreviewTicketWithVariations(previewTicketId, variationKeys).then(function successCallback() {
                var previewData = personalizationsmarteditContextService.getSeData().sePreviewData;
                personalizationsmarteditIFrameUtils.reloadPreview(previewData.resourcePath, previewData.previewTicketId);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingpreviewticket'));
            });
        };

        //Properties
        this.customizationEdited = false;

        //Methods
        this.initCustomization = function(customization) {
            customization.collapsed = true;
            if ((personalizationsmarteditContextService.getCustomize().selectedCustomization || {}).code === customization.code) {
                customization.collapsed = false;
            }
        };

        this.editCustomizationAction = function(customization) {
            self.customizationEdited = true;
            personalizationsmarteditContextUtils.clearCombinedViewContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            personalizationsmarteditManager.openEditCustomizationModal(customization.code);
        };

        this.customizationClick = function(customization) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            var currentVariations = personalizationsmarteditContextService.getCustomize().selectedVariations;
            updateCustomizationData(customization);
            var visibleVariations = getVisibleVariations(customization);
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = customization;
            customize.selectedVariations = visibleVariations;
            personalizationsmarteditContextService.setCustomize(customize);
            if (visibleVariations.length > 0) {
                var allVariations = personalizationsmarteditUtils.getVariationCodes(visibleVariations).join(",");
                getAndSetComponentsForVariation(customization.code, allVariations);
            }

            if ((angular.isObject(currentVariations) && !angular.isArray(currentVariations)) || combinedView.enabled) {
                updatePreviewTicket();
            }

            combinedView.enabled = false;
            personalizationsmarteditContextService.setCombinedView(combinedView);

            self.customizationsList.filter(function(cust) {
                return customization.name !== cust.name;
            }).forEach(function(cust, index) {
                cust.collapsed = true;
            });
        };

        this.getSelectedVariationClass = function(variation) {
            if (angular.equals(variation.code, (personalizationsmarteditContextService.getCustomize().selectedVariations || {}).code)) {
                return "selectedVariation";
            }
        };

        this.variationClick = function(customization, variation) {
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = customization;
            customize.selectedVariations = variation;
            personalizationsmarteditContextService.setCustomize(customize);
            getAndSetComponentsForVariation(customization.code, variation.code);
            updatePreviewTicket(customization.code, [variation]);
        };

        this.isCommerceCustomization = function(variation) {
            return variation.numberOfCommerceActions > 0;
        };

        this.getCommerceCustomizationTooltip = personalizationsmarteditUtils.getCommerceCustomizationTooltip;

        this.clearAllSubMenu = function() {
            angular.forEach(self.customizationsList, function(customization) {
                customization.subMenu = false;
            });
        };

        this.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        this.getActivityStateForVariation = function(customization, variation) {
            return personalizationsmarteditUtils.getActivityStateForVariation(customization, variation);
        };

        this.getEnablementTextForCustomization = function(customization) {
            return personalizationsmarteditUtils.getEnablementTextForCustomization(customization, 'personalization.toolbar.pagecustomizations');
        };

        this.getEnablementTextForVariation = function(variation) {
            return personalizationsmarteditUtils.getEnablementTextForVariation(variation, 'personalization.toolbar.pagecustomizations');
        };

        this.isEnabled = function(item) {
            return personalizationsmarteditUtils.isPersonalizationItemEnabled(item);
        };

        this.getDatesForCustomization = function(customization) {
            var activityStr = "";
            var startDateStr = "";
            var endDateStr = "";

            if (customization.enabledStartDate || customization.enabledEndDate) {
                startDateStr = personalizationsmarteditDateUtils.formatDateWithMessage(customization.enabledStartDate);
                endDateStr = personalizationsmarteditDateUtils.formatDateWithMessage(customization.enabledEndDate);
                if (!customization.enabledStartDate) {
                    startDateStr = " ...";
                }
                if (!customization.enabledEndDate) {
                    endDateStr = "... ";
                }
                activityStr += " (" + startDateStr + " - " + endDateStr + ") ";
            }
            return activityStr;
        };

        this.customizationSubMenuAction = function(customization) {
            if (!customization.subMenu) {
                self.clearAllSubMenu();
            }
            customization.subMenu = !customization.subMenu;
        };
    }])
    .component('pageCustomizationsList', {
        templateUrl: 'pageCustomizationsListTemplate.html',
        controller: 'pageCustomizationsListController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            customizationsList: '<'
        }
    });

angular.module('personalizationsmarteditPageCustomizationsToolbarItemModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditDataFactory',
        'personalizationsmarteditContextUtilsModule',
        'searchCustomizationFromLibModule',
        'statusesDropdownModule',
        'pageCustomizationsListModule'
    ])
    .controller('personalizationsmarteditPageCustomizationsToolbarController', ['$filter', '$timeout', 'personalizationsmarteditContextService', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditDateUtils', 'personalizationsmarteditIFrameUtils', 'customizationDataFactory', 'PaginationHelper', 'personalizationsmarteditUtils', 'personalizationsmarteditContextUtils', 'personalizationsmarteditRestService', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', 'personalizationsmarteditCommerceCustomizationService', function($filter, $timeout, personalizationsmarteditContextService, personalizationsmarteditMessageHandler, personalizationsmarteditDateUtils, personalizationsmarteditIFrameUtils, customizationDataFactory, PaginationHelper, personalizationsmarteditUtils, personalizationsmarteditContextUtils, personalizationsmarteditRestService, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, personalizationsmarteditCommerceCustomizationService) { //NOSONAR
        var self = this;

        //Private methods
        var refreshCustomizationData = function(customization) {
            personalizationsmarteditRestService.getVariationsForCustomization(customization.code).then(
                function successCallback(response) {
                    customization.variations = response.variations || [];
                    customization.variations.forEach(function(variation) {
                        variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                        variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                    });
                },
                function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });
        };

        var errorCallback = function(response) {
            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
            self.moreCustomizationsRequestProcessing = false;
        };

        var setSelectedItemsInCustomizeContext = function(selectedVariations, selectedCustomization) {
            var customize = personalizationsmarteditContextService.getCustomize();

            customize.selectedCustomization = selectedCustomization;
            customize.selectedVariations = selectedVariations;

            personalizationsmarteditContextService.setCustomize(customize);
        };

        var refreshCustomizeContext = function(selectedVariation, selectedCustomization) {
            if (angular.isObject(selectedVariation)) {
                if (!angular.isArray(selectedVariation)) {
                    var newVariation = selectedCustomization.variations.find(function(elem) {
                        return angular.equals(elem.code, selectedVariation.code);
                    });
                    setSelectedItemsInCustomizeContext(newVariation, selectedCustomization);
                } else {
                    setSelectedItemsInCustomizeContext(selectedVariation, selectedCustomization);
                }
            }
        };

        var getCustomizationsForSelectedStatuses = function() {
            angular.forEach(self.customizationsOnPage, function(valueOnPage, keyOnPage) {
                angular.forEach(self.customizationsSelectedFromLibrary, function(valueFromLibrary, keyFromLibrary) {
                    if (valueOnPage.code === valueFromLibrary.code) {
                        self.customizationsSelectedFromLibrary.splice(keyFromLibrary, 1);
                    }
                });
            });
            return ((self.customizationsOnPage || []).concat(self.customizationsSelectedFromLibrary || []))
                .filter(function(customization) {
                    return self.hasSelectedStatus(customization);
                });
        };

        var refreshCustomizationList = function() {
            self.customizationsList = getCustomizationsForSelectedStatuses();
        };

        var successCallback = function(response) {
            self.pagination = new PaginationHelper(response.pagination);
            self.moreCustomizationsRequestProcessing = false;

            //Update context, because value could change name
            var customize = personalizationsmarteditContextService.getCustomize();
            var currentCustomization = customize.selectedCustomization;
            var currentVariations = customize.selectedVariations;

            if (angular.isObject(currentCustomization)) {
                var newCustomization = getCustomizationsForSelectedStatuses().filter(function(elem) {
                    return angular.equals(elem.code, currentCustomization.code);
                });

                if (newCustomization.length === 0) {
                    personalizationsmarteditRestService.getCustomization(currentCustomization.code)
                        .then(function successCallback(response) {
                            currentCustomization.name = response.name;
                            currentCustomization.variations = angular.copy(response.variations);

                            Array.prototype.push.apply(self.customizationsOnPage, [currentCustomization]);

                            refreshCustomizeContext(currentVariations, currentCustomization);
                            refreshCustomizationList();
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                        });
                } else {
                    if (newCustomization.length > 1) {
                        customizationDataFactory.items.splice(customizationDataFactory.items.indexOf(newCustomization[1]), 1);
                    }
                    refreshCustomizeContext(currentVariations, newCustomization[0]);

                    refreshCustomizationData(newCustomization[0]);
                }
            }

            refreshCustomizationList();
        };

        var getCustomizations = function(categoryFilter) {
            var params = {
                filter: categoryFilter,
                dataArrayName: 'customizations'
            };
            customizationDataFactory.updateData(params, successCallback, errorCallback);
        };

        var getCustomizationsFilterObject = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                currentSize: self.pagination.count,
                currentPage: self.pagination.page + 1,
                statuses: self.selectedStatuses
            };
        };

        var getCustomizationsFilterObjectForLibrary = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                negatePageId: true,
                name: self.libraryCustomizationFilter.name,
                currentSize: self.libraryCustPagination.count,
                currentPage: self.libraryCustPagination.page + 1,
                statuses: personalizationsmarteditUtils.getStatusesMapping().filter(function(elem) {
                    return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
                })[0].modelStatuses
            };
        };

        var refreshGrid = function() {
            $timeout(function() {
                getCustomizations(getCustomizationsFilterObject());
            }, 0);
        };

        var removeArrayFromArrayByCode = function(fromArray, toRemoveArray) {
            var filteredArray = fromArray.filter(function(elem) {
                return toRemoveArray.map(function(e) {
                    return e.code;
                }).indexOf(elem.code) < 0;
            });

            return filteredArray;
        };

        //Properties
        this.selectedStatuses = [];
        this.customizationsOnPage = [];
        this.customizationsList = [];
        this.libraryCustPagination = {};
        this.libraryCustomizationFilter = {};
        this.moreLibraryCustomizationsRequestProcessing = false;
        this.libraryCustomizations = [];
        this.customizationsSelectedFromLibrary = [];

        this.setSelectedStatuses = function(selectedStatuses) {
            self.selectedStatuses = selectedStatuses;

            self.pagination.reset();
            customizationDataFactory.resetData();
            self.addMoreCustomizationItems();
        };

        this.clearCustomizeContext = function() {
            personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
        };

        this.isCustomizationsEmpty = function() {
            return self.customizationsList.length < 1;
        };

        this.hasSelectedStatus = function(customization) {
            return (self.selectedStatuses || []).indexOf(customization.status) > -1;
        };

        this.addMoreCustomizationItems = function() {
            if (self.pagination.page < self.pagination.totalPages - 1 && !self.moreCustomizationsRequestProcessing) {
                self.moreCustomizationsRequestProcessing = true;
                refreshGrid();
            }
        };

        this.addMoreLibraryCustomizationItems = function(searchObj) {
            if (angular.isDefined(searchObj)) {
                self.libraryCustPagination.reset();
                self.libraryCustomizationFilter.name = searchObj;
                self.libraryCustomizations.length = 0;
            }

            if (self.libraryCustPagination.page < self.libraryCustPagination.totalPages - 1 && !self.moreLibraryCustomizationsRequestProcessing) {
                self.moreLibraryCustomizationsRequestProcessing = true;
                personalizationsmarteditRestService.getCustomizations(getCustomizationsFilterObjectForLibrary()).then(function successCallback(response) {
                    var filteredCategories = removeArrayFromArrayByCode(response.customizations, self.customizationsSelectedFromLibrary);
                    var customization = personalizationsmarteditContextService.getCustomize().selectedCustomization;
                    if (customization) {
                        filteredCategories = removeArrayFromArrayByCode(filteredCategories, [customization]);
                    }
                    filteredCategories.forEach(function(customization) {
                        customization.fromLibrary = true;
                    });

                    Array.prototype.push.apply(self.libraryCustomizations, filteredCategories);

                    self.libraryCustPagination = new PaginationHelper(response.pagination);
                    self.moreLibraryCustomizationsRequestProcessing = false;
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
                    self.moreLibraryCustomizationsRequestProcessing = false;
                });
            }
        };

        this.addCustomizationFromLibrary = function(selFromLib) {
            if (!Array.isArray(selFromLib)) {
                selFromLib = [selFromLib];
            }

            Array.prototype.push.apply(self.customizationsSelectedFromLibrary, selFromLib);
            self.libraryCustomizations = removeArrayFromArrayByCode(self.libraryCustomizations, selFromLib);

            refreshCustomizationList();
        };

        //Lifecycle methods
        this.$onInit = function() {
            personalizationsmarteditContextService.refreshExperienceData();

            self.pagination = new PaginationHelper();
            self.pagination.reset();

            self.libraryCustPagination = new PaginationHelper();
            self.libraryCustPagination.reset();

            self.moreLibraryCustomizationsRequestProcessing = false;
            self.moreCustomizationsRequestProcessing = false;

            self.customizationsOnPage = customizationDataFactory.items;

            customizationDataFactory.resetData();
        };

        this.$onChanges = function(changes) {
            if (changes.isMenuOpen && changes.isMenuOpen.currentValue) {
                self.pagination.reset();
                customizationDataFactory.resetData();
                self.moreCustomizationsRequestProcessing = false;
                self.addMoreCustomizationItems();

                self.moreLibraryCustomizationsRequestProcessing = false;
                self.libraryCustPagination.reset();
                self.libraryCustomizations.length = 0;
                self.addMoreLibraryCustomizationItems();

                var customize = personalizationsmarteditContextService.getCustomize();
                for (var i = self.customizationsSelectedFromLibrary.length - 1; i >= 0; i--) {
                    if (self.customizationsSelectedFromLibrary[i].code !== (customize.selectedCustomization || {}).code) {
                        self.customizationsSelectedFromLibrary.splice(i, 1);
                    }
                }

            }
        };
    }])
    .component('personalizationsmarteditPageCustomizationsToolbarItem', {
        templateUrl: 'personalizationsmarteditPageCustomizationsToolbarItemTemplate.html',
        controller: 'personalizationsmarteditPageCustomizationsToolbarController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            isMenuOpen: '<'
        }
    });

angular.module('searchCustomizationFromLibModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .controller('searchCustomizationFromLibController', ['$timeout', 'personalizationsmarteditUtils', function($timeout, personalizationsmarteditUtils) {
        var self = this;

        //Properties
        this.selectedFromDropdownLibraryCustomizations = [];
        this.searchCustomizationEnabled = false;

        //Methods
        this.customizationSearchInputKeypress = function(keyEvent, searchStr) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }

            self.addMoreLibraryCustomizationItems({
                searchObj: searchStr
            });
        };

        this.toggleAddMoreCustomizationsClick = function() {
            this.searchCustomizationEnabled = !this.searchCustomizationEnabled;
        };

        this.libraryCustomizationsDropdownOpenClose = function(isOpen) {
            if (isOpen) {
                $timeout((function() {
                    angular.element("#dropdownCustomizationsStatus").controller('uiSelect').close();
                }), 0);
            }
        };

        this.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        this.getEnablementTextForCustomization = function(customization) {
            return personalizationsmarteditUtils.getEnablementTextForCustomization(customization, 'personalization.toolbar.pagecustomizations');
        };

        //Lifecycle methods
        this.$onChanges = function(changes) {
            if (changes.isMenuOpen && !changes.isMenuOpen.currentValue) {
                this.searchCustomizationEnabled = false;
            }
        };

    }])
    .component('searchCustomizationFromLib', {
        templateUrl: 'searchCustomizationFromLibTemplate.html',
        controller: 'searchCustomizationFromLibController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            addMoreLibraryCustomizationItems: '&',
            addCustomizationFromLibrary: '&',
            libraryCustomizations: '<',
            isMenuOpen: '<'
        }
    });

angular.module('statusesDropdownModule', [
        'personalizationsmarteditCommons'
    ])
    .controller('statusesDropdownController', ['$timeout', 'personalizationsmarteditUtils', 'PERSONALIZATION_VIEW_STATUS_MAPPING_CODES', function($timeout, personalizationsmarteditUtils, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES) {
        var self = this;

        //Properties
        this.statuses = [];

        //Methods
        this.statusCustomizationsDropdownOpenClose = function(isOpen) {
            if (isOpen) {
                $timeout((function() {
                    angular.element("#dropdownCustomizationsLibrary").controller('uiSelect').close();
                }), 0);
            }
        };

        //Lifecycle methods
        this.$onInit = function() {
            self.statuses = personalizationsmarteditUtils.getStatusesMapping();
            self.selectedStatus = self.statuses.filter(function(elem) {
                return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
            })[0];

            self.setSelectedStatuses({
                value: self.selectedStatus.modelStatuses || []
            });
        };
    }])
    .component('statusesDropdown', {
        templateUrl: 'statusesDropdownTemplate.html',
        controller: 'statusesDropdownController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            setSelectedStatuses: '&',
            isCustomizationsEmpty: '&',
            clearCustomizeContext: '&'
        }
    });

angular.module('personalizationsmarteditcontainermodule', [
        'personalizationsmarteditcontainerTemplates',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditRestServiceModule',
        'ui.bootstrap',
        'personalizationsmarteditCommons',
        'functionsModule',
        'personalizationsmarteditPreviewServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditManagerViewModule',
        'personalizationsmarteditContextMenu',
        'personalizationsmarteditPageCustomizationsToolbarItemModule',
        'featureServiceModule',
        'perspectiveServiceModule',
        'iFrameManagerModule',
        'personalizationsmarteditCombinedViewModule',
        'personalizationsmarteditPromotionModule',
        'personalizationsmarteditSegmentViewModule',
        'personalizationsmarteditToolbarContextModule',
        'crossFrameEventServiceModule',
        'seConstantsModule',
        'personalizationsmarteditRulesAndPermissionsRegistrationModule',
        'gatewayFactoryModule',
        'yjqueryModule'
    ])
    .factory('personalizationsmarteditIFrameUtils', ['$filter', 'iFrameManager', 'personalizationsmarteditContextService', 'personalizationsmarteditPreviewService', 'personalizationsmarteditMessageHandler', function($filter, iFrameManager, personalizationsmarteditContextService, personalizationsmarteditPreviewService, personalizationsmarteditMessageHandler) {
        var iframeUtils = {};

        iframeUtils.reloadPreview = function(resourcePath, previewTicketId) {
            iFrameManager.loadPreview(resourcePath, previewTicketId);
        };

        iframeUtils.clearAndReloadPreview = function(currentVariations) {
            if (angular.isObject(currentVariations) && !angular.isArray(currentVariations)) {
                var previewTicketId = personalizationsmarteditContextService.getSeData().sePreviewData.previewTicketId;
                personalizationsmarteditPreviewService.removePersonalizationDataFromPreview(previewTicketId).then(function successCallback() {
                    var previewData = personalizationsmarteditContextService.getSeData().sePreviewData;
                    iframeUtils.reloadPreview(previewData.resourcePath, previewData.previewTicketId);
                }, function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingpreviewticket'));
                });
            }
        };

        return iframeUtils;
    }])
    .controller('topToolbarMenuController', ['$scope', 'personalizationsmarteditManager', 'personalizationsmarteditManagerView', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditContextService', 'personalizationsmarteditContextUtils', 'personalizationsmarteditCombinedView', function($scope, personalizationsmarteditManager, personalizationsmarteditManagerView, personalizationsmarteditIFrameUtils, personalizationsmarteditContextService, personalizationsmarteditContextUtils, personalizationsmarteditCombinedView) {
        $scope.status = {
            isopen: false
        };

        $scope.preventDefault = function(oEvent) {
            oEvent.stopPropagation();
        };

        $scope.createCustomizationClick = function() {
            personalizationsmarteditManager.openCreateCustomizationModal();
        };

        $scope.managerViewClick = function() {
            personalizationsmarteditContextUtils.clearCombinedViewCustomizeContext(personalizationsmarteditContextService);
            personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            personalizationsmarteditContextUtils.clearCombinedViewContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            personalizationsmarteditManagerView.openManagerAction();
        };

        $scope.combinedViewClick = function() {
            personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            personalizationsmarteditCombinedView.openManagerAction();
        };
        $scope.isCustomizeCustomizationSelected = function() {
            return personalizationsmarteditContextService.getCustomize().selectedCustomization;
        };
        $scope.isCombinedViewCustomizationSelected = function() {
            return personalizationsmarteditContextService.getCombinedView().customize.selectedCustomization;
        };

    }])
    .run(
        ['$rootScope', 'yjQuery', 'personalizationsmarteditContextService', 'personalizationsmarteditContextServiceReverseProxy', 'personalizationsmarteditContextModal', 'featureService', 'perspectiveService', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditContextUtils', 'crossFrameEventService', 'EVENT_PERSPECTIVE_UNLOADING', 'smartEditBootstrapGateway', function($rootScope, yjQuery, personalizationsmarteditContextService, personalizationsmarteditContextServiceReverseProxy, personalizationsmarteditContextModal, featureService, perspectiveService, personalizationsmarteditIFrameUtils, personalizationsmarteditContextUtils, crossFrameEventService, EVENT_PERSPECTIVE_UNLOADING, smartEditBootstrapGateway) {

            var loadCSS = function(href) {
                var cssLink = yjQuery("<link rel='stylesheet' type='text/css' href='" + href + "'>");
                yjQuery("head").append(cssLink);
            };

            loadCSS("/personalizationsmartedit/css/style.css");


            var PERSONALIZATION_PERSPECTIVE_KEY = "personalizationsmartedit.perspective";

            var PersonalizationviewContextServiceReverseProxy = new personalizationsmarteditContextServiceReverseProxy('PersonalizationCtxReverseGateway'); //NOSONAR

            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'personalizationsmartedit.container.pagecustomizations.toolbar',
                type: 'TEMPLATE',
                nameI18nKey: 'personalization.toolbar.pagecustomizations',
                priority: 4,
                section: 'left',
                include: 'personalizationsmarteditPageCustomizationsToolbarItemWrapperTemplate.html',
                keepAliveOnClose: false,
                permissions: ['se.edit.page']
            });
            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'personalizationsmartedit.container.combinedview.toolbar',
                type: 'TEMPLATE',
                nameI18nKey: 'personalization.toolbar.combinedview.name',
                priority: 6,
                section: 'left',
                include: 'personalizationsmarteditCombinedViewMenuTemplate.html',
                keepAliveOnClose: false,
                permissions: ['se.read.page']
            });
            featureService.addToolbarItem({
                toolbarId: 'experienceSelectorToolbar',
                key: 'personalizationsmartedit.container.manager.toolbar',
                type: 'HYBRID_ACTION',
                nameI18nKey: 'personalization.toolbar.library.name',
                iconClassName: 'hyicon hyicon-library se-toolbar-menu-ddlb--button__icon',
                priority: 8,
                section: 'left',
                include: 'personalizationsmarteditCustomizationManagMenuTemplate.html',
                keepAliveOnClose: false,
                permissions: ['se.edit.page']
            });
            featureService.register({
                key: 'personalizationsmartedit.context.service',
                nameI18nKey: 'personalization.context.service.name',
                descriptionI18nKey: 'personalization.context.service.description',
                enablingCallback: function() {
                    var personalization = personalizationsmarteditContextService.getPersonalization();
                    personalization.enabled = true;
                    personalizationsmarteditContextService.setPersonalization(personalization);
                },
                disablingCallback: function() {
                    var personalization = personalizationsmarteditContextService.getPersonalization();
                    personalization.enabled = false;
                    personalizationsmarteditContextService.setPersonalization(personalization);
                },
                permissions: ['se.edit.page']
            });

            perspectiveService.register({
                key: PERSONALIZATION_PERSPECTIVE_KEY,
                nameI18nKey: 'personalization.perspective.name',
                descriptionI18nKey: 'personalization.perspective.description',
                features: ['personalizationsmartedit.context.service',
                    'personalizationsmartedit.container.pagecustomizations.toolbar',
                    'personalizationsmartedit.container.manager.toolbar',
                    'personalizationsmartedit.container.combinedview.toolbar',
                    'personalizationsmarteditSharedSlot',
                    'personalizationsmarteditComponentLightUp',
                    'personalizationsmarteditCombinedViewComponentLightUp',
                    'personalizationsmartedit.context.add.action',
                    'personalizationsmartedit.context.edit.action',
                    'personalizationsmartedit.context.delete.action',
                    'personalizationsmartedit.context.info.action',
                    'personalizationsmartedit.context.component.edit.action',
                    'personalizationsmartedit.context.show.action.list',
                    'se.contextualMenu',
                    'se.emptySlotFix'
                ],
                perspectives: [],
                permissions: ['se.personalization.open']
            });

            crossFrameEventService.subscribe(EVENT_PERSPECTIVE_UNLOADING, function(eventId, unloadingPerspective) {
                if (unloadingPerspective === PERSONALIZATION_PERSPECTIVE_KEY) {
                    personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
                    personalizationsmarteditContextUtils.clearCombinedViewContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
                }
            });

            $rootScope.$on('$locationChangeSuccess', function() {
                personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
                personalizationsmarteditContextUtils.clearCombinedViewContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            });

            smartEditBootstrapGateway.subscribe("smartEditReady", function(eventId, data) {
                personalizationsmarteditContextService.applySynchronization();
            });

        }]);

angular.module('personalizationsmarteditContextServiceModule', [
        'sharedDataServiceModule',
        'loadConfigModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .factory('personalizationsmarteditContextService', ['$q', 'personalizationsmarteditContextServiceProxy', 'sharedDataService', 'loadConfigManagerService', 'personalizationsmarteditContextUtils', function($q, personalizationsmarteditContextServiceProxy, sharedDataService, loadConfigManagerService, personalizationsmarteditContextUtils) {

        /*
         * Usage
         * When using ContextService objects do not use properties directly,
         * always use getter(to retrieve object) and setter(to set object),
         * so all synchronization functions which are in getters and setters are called.
         * Example:
         * var examplePersonalization = ContextService.getPersonalization();
         * examplePersonalization.enabled = true;
         * examplePersonalization.myComponents = ["component1", "component2"];
         * ContextService.setPersonalization(examplePersonalization);
         */

        var ContextService = personalizationsmarteditContextUtils.getContextObject();
        var ContextServiceProxy = new personalizationsmarteditContextServiceProxy('PersonalizationCtxGateway');

        ContextService.getPersonalization = function() {
            return ContextService.personalization;
        };
        ContextService.setPersonalization = function(personalization) {
            ContextService.personalization = personalization;
            ContextServiceProxy.setPersonalization(personalization);
        };

        ContextService.getCustomize = function() {
            return ContextService.customize;
        };
        ContextService.setCustomize = function(customize) {
            ContextService.customize = customize;
            ContextServiceProxy.setCustomize(customize);
        };

        ContextService.getCombinedView = function() {
            return ContextService.combinedView;
        };
        ContextService.setCombinedView = function(combinedView) {
            ContextService.combinedView = combinedView;
            ContextServiceProxy.setCombinedView(combinedView);
        };

        ContextService.getSeData = function() {
            return ContextService.seData;
        };
        ContextService.setSeData = function(seData) {
            ContextService.seData = seData;
            ContextServiceProxy.setSeData(seData);
        };

        ContextService.refreshExperienceData = function() {
            return sharedDataService.get('experience').then(function(data) {
                var seData = ContextService.getSeData();
                seData.seExperienceData = data;
                ContextService.setSeData(seData);
                return $q.when();
            });
        };

        ContextService.refreshPreviewData = function() {
            sharedDataService.get('preview').then(function(data) {
                var seData = ContextService.getSeData();
                seData.sePreviewData = data;
                ContextService.setSeData(seData);
            });
        };

        ContextService.refreshConfigurationData = function() {
            loadConfigManagerService.loadAsObject().then(function(configurations) {
                var seData = ContextService.getSeData();
                seData.seConfigurationData = configurations;
                ContextService.setSeData(seData);
            });
        };

        ContextService.applySynchronization = function() {
            ContextServiceProxy.setPersonalization(ContextService.personalization);
            ContextServiceProxy.setCustomize(ContextService.customize);
            ContextServiceProxy.setCombinedView(ContextService.combinedView);
            ContextServiceProxy.setSeData(ContextService.seData);

            ContextService.refreshExperienceData();
            ContextService.refreshPreviewData();
            ContextService.refreshConfigurationData();
        };

        ContextService.getContexServiceProxy = function() {
            return ContextServiceProxy;
        };

        return ContextService;
    }])
    .factory('personalizationsmarteditContextServiceProxy', ['gatewayProxy', function(gatewayProxy) {
        var proxy = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };

        proxy.prototype.setPersonalization = function() {};
        proxy.prototype.setCustomize = function() {};
        proxy.prototype.setCombinedView = function() {};
        proxy.prototype.setSeData = function() {};

        return proxy;
    }])
    .factory('personalizationsmarteditContextServiceReverseProxy', ['gatewayProxy', 'personalizationsmarteditContextService', function(gatewayProxy, personalizationsmarteditContextService) {
        var reverseProxy = function(gatewayId) {
            this.gatewayId = gatewayId;
            gatewayProxy.initForService(this);
        };

        reverseProxy.prototype.applySynchronization = function() {
            personalizationsmarteditContextService.applySynchronization();
        };

        reverseProxy.prototype.setPageId = function(newPageId) {
            var seData = personalizationsmarteditContextService.getSeData();
            seData.pageId = newPageId;
            personalizationsmarteditContextService.setSeData(seData);
        };

        return reverseProxy;
    }]);

angular.module('personalizationsmarteditPreviewServiceModule', [
        'personalizationsmarteditRestServiceModule',
        'sharedDataServiceModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditContextServiceModule'
    ])
    .factory('personalizationsmarteditPreviewService', ['$q', '$filter', 'personalizationsmarteditRestService', 'sharedDataService', 'personalizationsmarteditUtils', 'personalizationsmarteditMessageHandler', 'personalizationsmarteditContextService', function($q, $filter, personalizationsmarteditRestService, sharedDataService, personalizationsmarteditUtils, personalizationsmarteditMessageHandler, personalizationsmarteditContextService) {

        var previewService = {};

        previewService.removePersonalizationDataFromPreview = function(previewTicketId) {
            var deferred = $q.defer();
            previewService.updatePreviewTicketWithVariations(previewTicketId, []).then(function successCallback(previewTicket) {
                deferred.resolve(previewTicket);
            }, function errorCallback(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        previewService.updatePreviewTicketWithVariations = function(previewTicketId, variations) {
            var deferred = $q.defer();
            personalizationsmarteditRestService.getPreviewTicket(previewTicketId).then(function successCallback(previewTicket) {
                previewTicket.variations = variations;
                deferred.resolve(personalizationsmarteditRestService.updatePreviewTicket(previewTicket));
            }, function errorCallback(response) {
                if (response.status === 404) {
                    //preview ticket not found - let's try to create a new one with the same parameters
                    deferred.resolve(previewService.createPreviewTicket(variations));
                } else {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingpreviewticket'));
                    deferred.reject(response);
                }
            });

            return deferred.promise;
        };

        previewService.createPreviewTicket = function(variationsForPreview) {
            var experience = personalizationsmarteditContextService.getSeData().seExperienceData;
            var configuration = personalizationsmarteditContextService.getSeData().seConfigurationData;

            var deferred = $q.defer();

            var resourcePath = configuration.domain + experience.siteDescriptor.previewUrl;

            var previewTicket = {
                catalog: experience.catalogDescriptor.catalogId,
                catalogVersion: experience.catalogDescriptor.catalogVersion,
                language: experience.languageDescriptor.isocode,
                resourcePath: resourcePath,
                variations: variationsForPreview
            };

            personalizationsmarteditRestService.createPreviewTicket(previewTicket).then(function successCallback(response) {
                previewService.storePreviewTicketData(response.resourcePath, response.ticketId);
                personalizationsmarteditContextService.refreshPreviewData();
                personalizationsmarteditMessageHandler.sendSuccess($filter('translate')('personalization.info.newpreviewticketcreated'));
                deferred.resolve(response);
            }, function errorCallback(response) {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.creatingpreviewticket'));
                deferred.reject(response);
            });

            return deferred.promise;
        };

        previewService.storePreviewTicketData = function(resourcePathToStore, previewTicketIdToStore) {
            var previewToStore = {
                previewTicketId: previewTicketIdToStore,
                resourcePath: resourcePathToStore
            };
            sharedDataService.set('preview', previewToStore);
        };


        return previewService;
    }]);

angular.module('personalizationsmarteditRestServiceModule', [
        'restServiceFactoryModule',
        'personalizationsmarteditCommons',
        'personalizationsmarteditContextServiceModule'
    ])
    .factory('personalizationsmarteditRestService', ['restServiceFactory', 'personalizationsmarteditUtils', 'personalizationsmarteditContextService', '$http', '$q', function(restServiceFactory, personalizationsmarteditUtils, personalizationsmarteditContextService, $http, $q) {

        var CUSTOMIZATIONS = "/personalizationwebservices/v1/catalogs/:catalogId/catalogVersions/:catalogVersion/customizations";
        var CUSTOMIZATION = CUSTOMIZATIONS + "/:customizationCode";

        var CUSTOMIZATION_PACKAGES = "/personalizationwebservices/v1/catalogs/:catalogId/catalogVersions/:catalogVersion/customizationpackages";
        var CUSTOMIZATION_PACKAGE = CUSTOMIZATION_PACKAGES + "/:customizationCode";

        var VARIATIONS = CUSTOMIZATION + "/variations";
        var VARIATION = VARIATIONS + "/:variationCode";

        var ACTIONS = VARIATION + "/actions";
        var ACTION = ACTIONS + "/:actionId";

        var CXCMSC_ACTIONS_FROM_VARIATIONS = "/personalizationwebservices/v1/query/cxcmscomponentsfromvariations";
        var AVAILABLE_PROMOTIONS = "/personalizationwebservices/v1/query/cxpromotionsforcatalog";

        var PREVIEWTICKET = "/previewwebservices/v1/preview/:ticketId";
        var SEGMENTS = "/personalizationwebservices/v1/segments";

        var CATALOGS = "/cmswebservices/v1/sites/:siteId/catalogs/:catalogId/versions/:catalogVersion/items";
        var CATALOG = CATALOGS + "/:itemId";

        var ADD_CONTAINER = "/personalizationwebservices/v1/query/cxReplaceComponentWithContainer";

        var COMPONENT_TYPES = '/cmswebservices/v1/types?category=COMPONENT';

        var UPDATE_CUSTOMIZATION_RANK = "/personalizationwebservices/v1/query/cxUpdateCustomizationRank";

        var VARIATION_FOR_CUSTOMIZATION_DEFAULT_FIELDS = "variations(active,actions,enabled,code,name,rank,status)";

        var FULL_FIELDS = "FULL";

        var extendRequestParamObjWithCatalogAwarePathVariables = function(requestParam) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
            var catalogAwareParams = {
                catalogId: experienceData.catalogDescriptor.catalogId,
                catalogVersion: experienceData.catalogDescriptor.catalogVersion
            };
            requestParam = angular.extend(requestParam, catalogAwareParams);
            return requestParam;
        };

        var extendRequestParamObjWithCustomizatonCode = function(requestParam, customizatiodCode) {
            var customizationCodeParam = {
                customizationCode: customizatiodCode
            };
            requestParam = angular.extend(requestParam, customizationCodeParam);
            return requestParam;
        };

        var getParamsAction = function(oldComponentId, newComponentId, slotId, containerId, customizationId, variationId) {
            var entries = [];
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "oldComponentId", oldComponentId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "newComponentId", newComponentId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "slotId", slotId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "containerId", containerId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "variationId", variationId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "customizationId", customizationId);
            return {
                "params": {
                    "entry": entries
                }
            };
        };

        var getPathVariablesObjForModifyingActionURI = function(customizationId, variationId, actionId) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
            return {
                customizationCode: customizationId,
                variationCode: variationId,
                actionId: actionId,
                catalogId: experienceData.catalogDescriptor.catalogId,
                catalogVersion: experienceData.catalogDescriptor.catalogVersion
            };
        };

        var prepareURI = function(uri, pathVariables) {
            return uri.replace(/((?:\:)(\w*)(?:\/))/g, function(match, p1, p2) {
                return pathVariables[p2] + "/";
            });
        };

        var restService = {};

        restService.getCustomizations = function(filter) { //NOSONAR
            filter = filter || {};

            var restService = restServiceFactory.get(CUSTOMIZATIONS);
            var requestParams = {};

            requestParams = extendRequestParamObjWithCatalogAwarePathVariables(requestParams);
            requestParams = extendRequestParamObjWithCustomizatonCode(requestParams);

            requestParams.pageSize = filter.currentSize || 10;
            requestParams.currentPage = filter.currentPage || 0;

            if (angular.isDefined(filter.code)) {
                requestParams.code = filter.code;
            }
            if (angular.isDefined(filter.pageId)) {
                requestParams.pageId = filter.pageId;
            }
            if (angular.isDefined(filter.name)) {
                requestParams.name = filter.name;
            }
            if (angular.isDefined(filter.negatePageId)) {
                requestParams.negatePageId = filter.negatePageId;
            }
            if (angular.isDefined(filter.statuses)) {
                requestParams.statuses = filter.statuses.join(',');
            }
            return restService.get(requestParams);
        };

        restService.getComponenentsIdsForVariation = function(customizationId, variationId) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;

            var restService = restServiceFactory.get(CXCMSC_ACTIONS_FROM_VARIATIONS);
            var entries = [];
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "customization", customizationId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "variations", variationId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "catalog", experienceData.catalogDescriptor.catalogId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "catalogVersion", experienceData.catalogDescriptor.catalogVersion);
            var requestParams = {
                "params": {
                    "entry": entries
                }
            };
            return restService.save(requestParams);
        };

        restService.getPreviewTicket = function() {
            var previewTicketData = personalizationsmarteditContextService.getSeData().sePreviewData;
            var restService = restServiceFactory.get(PREVIEWTICKET, "ticketId");
            var previewTicket = {
                ticketId: previewTicketData.previewTicketId
            };
            return restService.get(previewTicket);
        };

        restService.updatePreviewTicket = function(previewTicket) {
            var restService = restServiceFactory.get(PREVIEWTICKET, "ticketId");
            return restService.update(previewTicket);
        };

        restService.createPreviewTicket = function(previewTicket) {
            var previewRESTService = restServiceFactory.get(PREVIEWTICKET);
            return previewRESTService.save(previewTicket);
        };

        restService.getSegments = function(filter) {
            var restService = restServiceFactory.get(SEGMENTS);
            return restService.get(filter);
        };

        restService.getCustomization = function(customizationCode) {
            var restService = restServiceFactory.get(CUSTOMIZATION, "customizationCode");

            var requestParams = {
                customizationCode: customizationCode
            };

            return restService.get(extendRequestParamObjWithCatalogAwarePathVariables(requestParams));
        };

        restService.createCustomization = function(customization) {
            var restService = restServiceFactory.get(CUSTOMIZATION_PACKAGES);

            return restService.save(extendRequestParamObjWithCatalogAwarePathVariables(customization));
        };

        restService.updateCustomization = function(customization) {
            var restService = restServiceFactory.get(CUSTOMIZATION, "customizationCode");
            customization.customizationCode = customization.code;
            return restService.update(extendRequestParamObjWithCatalogAwarePathVariables(customization));
        };

        restService.updateCustomizationPackage = function(customization) {
            var restService = restServiceFactory.get(CUSTOMIZATION_PACKAGE, "customizationCode");
            customization.customizationCode = customization.code;
            return restService.update(extendRequestParamObjWithCatalogAwarePathVariables(customization));
        };

        restService.deleteCustomization = function(customizationCode) {
            var restService = restServiceFactory.get(CUSTOMIZATION, "customizationCode");

            var requestParams = {
                customizationCode: customizationCode
            };

            return restService.remove(extendRequestParamObjWithCatalogAwarePathVariables(requestParams));
        };

        restService.getVariation = function(customizationCode, variationCode) {
            var restService = restServiceFactory.get(VARIATION, "variationCode");
            var requestParams = {
                variationCode: variationCode
            };
            requestParams = extendRequestParamObjWithCatalogAwarePathVariables(requestParams);
            requestParams = extendRequestParamObjWithCustomizatonCode(requestParams, customizationCode);
            return restService.get(requestParams);
        };

        restService.editVariation = function(customizationCode, variation) {
            var restService = restServiceFactory.get(VARIATION, "variationCode");

            variation = extendRequestParamObjWithCatalogAwarePathVariables(variation);
            variation = extendRequestParamObjWithCustomizatonCode(variation, customizationCode);
            variation.variationCode = variation.code;
            return restService.update(variation);
        };

        restService.deleteVariation = function(customizationCode, variationCode) {
            var restService = restServiceFactory.get(VARIATION, "variationCode");
            var requestParams = {
                variationCode: variationCode
            };

            requestParams = extendRequestParamObjWithCatalogAwarePathVariables(requestParams);
            requestParams = extendRequestParamObjWithCustomizatonCode(requestParams, customizationCode);

            return restService.remove(requestParams);
        };

        restService.createVariationForCustomization = function(customizationCode, variation) {
            var restService = restServiceFactory.get(VARIATIONS);

            variation = extendRequestParamObjWithCatalogAwarePathVariables(variation);
            variation = extendRequestParamObjWithCustomizatonCode(variation, customizationCode);

            return restService.save(variation);
        };

        restService.getVariationsForCustomization = function(customizationCode, filter) {
            var restService = restServiceFactory.get(VARIATIONS);

            var requestParams = {};
            var varForCustFilter = filter || {};

            requestParams = extendRequestParamObjWithCatalogAwarePathVariables(requestParams);
            requestParams = extendRequestParamObjWithCustomizatonCode(requestParams, customizationCode);

            requestParams.fields = VARIATION_FOR_CUSTOMIZATION_DEFAULT_FIELDS;

            var includeFullFields = typeof varForCustFilter.includeFullFields === "undefined" ? false : varForCustFilter.includeFullFields;

            if (includeFullFields) {
                requestParams.fields = FULL_FIELDS;
            }

            return restService.get(requestParams);
        };

        restService.getComponents = function(filter) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
            var restService = restServiceFactory.get(CATALOGS);
            var compomentsParams = {
                catalogId: experienceData.catalogDescriptor.catalogId,
                catalogVersion: experienceData.catalogDescriptor.catalogVersion,
                siteId: experienceData.siteDescriptor.uid
            };
            compomentsParams = angular.extend(compomentsParams, filter);

            return restService.get(compomentsParams).then(function successCallback(response) {
                response.componentItems.forEach(function(item) {
                    item.catalog = compomentsParams.catalogId;
                });
                return response;
            });
        };

        restService.replaceComponentWithContainer = function(componentId, slotId) {
            var restService = restServiceFactory.get(ADD_CONTAINER);
            var catalogParams = extendRequestParamObjWithCatalogAwarePathVariables({});
            var requestParams = getParamsAction(componentId, null, slotId, null, null, null);
            personalizationsmarteditUtils.pushToArrayIfValueExists(requestParams.params.entry, "catalog", catalogParams.catalogId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(requestParams.params.entry, "catalogVersion", catalogParams.catalogVersion);

            return restService.save(requestParams);
        };

        restService.getActions = function(customizationId, variationId) {
            var restService = restServiceFactory.get(ACTIONS);
            var pathVariables = getPathVariablesObjForModifyingActionURI(customizationId, variationId);

            var requestParams = {
                "fields": FULL_FIELDS
            };
            requestParams = angular.extend(requestParams, pathVariables);

            return restService.get(requestParams);
        };

        restService.createActions = function(customizationId, variationId, data) {

            var pathVariables = getPathVariablesObjForModifyingActionURI(customizationId, variationId);
            var url = prepareURI(ACTIONS, pathVariables);

            return $http({
                url: url,
                method: 'PATCH',
                data: data,
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                }
            });
        };

        restService.addActionToContainer = function(componentId, catalogId, containerId, customizationId, variationId) {
            var restService = restServiceFactory.get(ACTIONS);
            var pathVariables = getPathVariablesObjForModifyingActionURI(customizationId, variationId);
            var requestParams = {
                "type": "cxCmsActionData",
                "containerId": containerId,
                "componentId": componentId,
                "componentCatalog": catalogId
            };
            requestParams = angular.extend(requestParams, pathVariables);
            return restService.save(requestParams);
        };

        restService.editAction = function(customizationId, variationId, actionId, newComponentId) {
            var restService = restServiceFactory.get(ACTION, "actionId");

            var requestParams = getPathVariablesObjForModifyingActionURI(customizationId, variationId, actionId);

            return restService.get(requestParams).then(function successCallback(actionInfo) {
                actionInfo = angular.extend(actionInfo, requestParams);
                actionInfo.componentId = newComponentId;
                return restService.update(actionInfo);
            });
        };

        restService.deleteAction = function(customizationId, variationId, actionId) {
            var restService = restServiceFactory.get(ACTION, "actionId");

            var requestParams = getPathVariablesObjForModifyingActionURI(customizationId, variationId, actionId);

            return restService.remove(requestParams);
        };

        restService.deleteActions = function(customizationId, variationId, actionIds) {
            var pathVariables = getPathVariablesObjForModifyingActionURI(customizationId, variationId);
            var url = prepareURI(ACTIONS, pathVariables);

            return $http({
                url: url,
                method: 'DELETE',
                data: actionIds,
                headers: {
                    "Content-Type": "application/json;charset=utf-8"
                }
            });
        };

        restService.getComponent = function(itemId) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
            var restService = restServiceFactory.get(CATALOG, "itemId");
            var requestParams = {
                itemId: itemId,
                siteId: experienceData.siteDescriptor.uid
            };
            requestParams = extendRequestParamObjWithCatalogAwarePathVariables(requestParams);

            return restService.get(requestParams).then(function successCallback(response) {
                response.catalog = requestParams.catalogId;
                return response;
            });
        };

        restService.getNewComponentTypes = function() {
            var restService = restServiceFactory.get(COMPONENT_TYPES);
            return restService.get();
        };

        restService.updateCustomizationRank = function(customizationId, icreaseValue) {
            var experienceData = personalizationsmarteditContextService.getSeData().seExperienceData;
            var restService = restServiceFactory.get(UPDATE_CUSTOMIZATION_RANK);
            var entries = [];
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "customization", customizationId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "increaseValue", icreaseValue);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "catalog", experienceData.catalogDescriptor.catalogId);
            personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "catalogVersion", experienceData.catalogDescriptor.catalogVersion);
            var requestParams = {
                "params": {
                    "entry": entries
                }
            };
            return restService.save(requestParams);
        };

        restService.getPromotions = function(catalogVersions) {
            var restService = restServiceFactory.get(AVAILABLE_PROMOTIONS);
            var entries = [];

            catalogVersions = catalogVersions || [];

            catalogVersions.forEach(
                function(element, i) {
                    personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "catalog" + i, element.catalog);
                    personalizationsmarteditUtils.pushToArrayIfValueExists(entries, "version" + i, element.catalogVersion);
                }
            );

            var requestParams = {
                "params": {
                    "entry": entries
                }
            };

            return restService.save(requestParams);
        };

        return restService;
    }]);

/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
angular.module('personalizationsmarteditRulesAndPermissionsRegistrationModule', [
    'permissionServiceModule',
    'personalizationsmarteditRestServiceModule',
    'personalizationsmarteditContextServiceModule'
]).run(['$q', 'permissionService', 'personalizationsmarteditRestService', 'personalizationsmarteditContextService', function($q, permissionService, personalizationsmarteditRestService, personalizationsmarteditContextService) {

    var getCustomizationFilter = function() {
        return {
            currentPage: 0,
            currentSize: 1
        };
    };

    // Rules
    permissionService.registerRule({
        names: ['se.access.personalization'],
        verify: function(permissionNameObjs) {
            return personalizationsmarteditContextService.refreshExperienceData().then(function() {
                return personalizationsmarteditRestService.getCustomizations(getCustomizationFilter()).then(function(response) {
                    return $q.when(true);
                }, function(errorResp) {
                    if (errorResp.status === 403) {
                        //Forbidden status on GET /customizations - user doesn't have permission to personalization perspective
                        return $q.when(false);
                    } else {
                        //other errors will be handled with personalization perspective turned on
                        return $q.when(true);
                    }
                });
            });
        }
    });

    // Permissions
    permissionService.registerPermission({
        aliases: ['se.personalization.open'],
        rules: ['se.read.page', 'se.access.personalization']
    });
}]);

angular.module('personalizationsmarteditcontainerTemplates', []).run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('web/features/personalizationsmarteditcontainer/combinedView/personalizationsmarteditCombinedViewConfigureTemplate.html',
    "<form>\n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"pe-combinedview-config__ui-select-layout\">\n" +
    "            <label for=\"CombinedViewSearchField1\" class=\"control-label required\" data-translate=\"personalization.modal.combinedview.search.label\"></label>\n" +
    "            <ui-select uis-open-close=\"initUiSelect($select)\" ng-model=\"selectedElement\" theme=\"select2\" class=\"form-control\" ng-keyup=\"searchInputKeypress($event, $select.search)\" on-select=\"selectElement($item)\" reset-search-input=\"false\" id=\"CombinedViewSearchField1\">\n" +
    "                <ui-select-match placeholder=\"{{ 'personalization.modal.combinedview.search.placeholder' | translate}}\">\n" +
    "                    <span class=\"pe-combinedview-config__ui-select-placeholder\">{{'personalization.modal.combinedview.search.placeholder' | translate}}</span>\n" +
    "                </ui-select-match>\n" +
    "                <ui-select-choices repeat=\"item in selectionArray\" position=\"down\" ui-disable-choice=\"isItemInSelectDisabled(item)\" personalization-infinite-scroll=\"addMoreItems()\" personalization-infinite-scroll-distance=\"1\">\n" +
    "                    <div data-ng-class=\"{'pe-combinedview-config__ui-select-item--selected': isItemSelected(item)}\">\n" +
    "                        {{item.customization.name}} > {{item.variation.name}}\n" +
    "                    </div>\n" +
    "                </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <div class=\"pe-combinedview-config__list-layout\" data-ng-repeat=\"item in selectedItems\" data-ng-class=\"{'pe-combinedview-config__divider': $first}\">\n" +
    "            <div data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "            <div class=\"pe-combinedview-config__names-layout\">\n" +
    "                <div class=\"perso-wrap-ellipsis pe-combinedview-config__cname\" data-ng-bind=\"item.customization.name\"></div>\n" +
    "                <span> ></span>\n" +
    "                <div class=\"perso-wrap-ellipsis pe-combinedview-config__vname\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "            </div>\n" +
    "            <div class=\"pe-combinedview-config__hyicon-remove\">\n" +
    "                <span class=\"hyicon hyicon-remove\" data-ng-click=\"removeSelectedItem(item)\"></span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</form>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/combinedView/personalizationsmarteditCombinedViewMenuTemplate.html',
    "<div data-ng-controller=\"topToolbarMenuController\" data-uib-dropdown data-is-open=\"item.isOpen\" class=\"btn-group pe-toolbar-action pe-combined-view-menu\" data-auto-close=\"disabled\">\n" +
    "    <button type=\"button\" class=\"btn btn-default pe-toolbar-action--button\" data-uib-dropdown-toggle data-ng-disabled=\"disabled\" aria-pressed=\"false\" data-ng-class=\"{'pe-toolbar-action-combined-view--button-with-context': isCombinedViewCustomizationSelected()}\">\n" +
    "        <span class=\"hyicon hyicon-combinedview pe-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <span class=\"pe-toolbar-action--button--txt\" data-translate=\"personalization.toolbar.combinedview.name\"></span>\n" +
    "    </button>\n" +
    "\n" +
    "    <personalizationsmartedit-combined-view-toolbar-context></personalizationsmartedit-combined-view-toolbar-context>\n" +
    "\n" +
    "    <div data-uib-dropdown-menu class=\"btn-block pe-toolbar-action--include\">\n" +
    "\n" +
    "        <ul class=\"pe-toolbar-menu-content  se-toolbar-menu-content--pe-customized\" role=\"menu\">\n" +
    "            <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "                <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.combinedview.header.title\"></h2>\n" +
    "                <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.combinedview.header.description\"></small>\n" +
    "            </div>\n" +
    "            <div class=\"pe-combined-view-menu\">\n" +
    "                <div class=\"pe-combined-view-menu__wrapper\" data-ng-controller=\"personalizationsmarteditCombinedViewMenuController\">\n" +
    "                    <div class=\"pe-combined-view-menu__configure-layout\">\n" +
    "                        <span class=\"y-toggle y-toggle-lg\">\n" +
    "                            <input type=\"checkbox\" id=\"test-checkbox\" data-ng-change=\"combinedViewEnabledChangeEvent()\" data-ng-model=\"combinedView.enabled\" />\n" +
    "                            <label class=\"pe-combined-view-menu__enable-label\" for=\"test-checkbox\"></label>\n" +
    "                        </span>\n" +
    "                        <button class=\"btn btn-link pe-combined-view-menu__configure-btn perso-wrap-ellipsis\" data-ng-click=\"$parent.combinedViewClick()\" data-translate=\"personalization.toolbar.combinedview.openconfigure.button\">\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                    <div class=\"pe-combined-view-menu__category-table\" data-ng-class=\"combinedView.enabled ? '':'combinedview-inactive-layout'\">\n" +
    "                        <div class=\"pe-combined-view-menu__category-table__item\" data-ng-repeat=\"item in selectedItems\" data-ng-class=\"{'combinedview-list-item--last': $last, 'combinedview-divider': $first, 'pe-combined-view-menu__list-item--highlighted': item.highlighted}\">\n" +
    "                            <div class=\"pe-combined-view-menu__category-table__item-icon\" data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "                            <div class=\"pe-combined-view-menu__category-table__item-name\" data-ng-click=\"itemClick(item)\">\n" +
    "                                <div data-ng-bind=\"item.customization.name\"></div>\n" +
    "                                <div class=\"text-uppercase combinedview-variation-layout\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "                            </div>\n" +
    "                            <div data-ng-class=\"{'hyicon hyicon-checkedlg pe-combined-view-menu__hyicon-checkedlg': item.highlighted}\"></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </ul>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/contextMenu/personalizationsmarteditAddEditActionTemplate.html',
    "<div class=\"perso-customize-component\">\n" +
    "    <div class=\"perso-customize-component__title-layout\">\n" +
    "        <div data-ng-show='letterIndicatorForElement' class=\"perso-customize-component__title-layout__letter-block\">\n" +
    "            <span ng-class=\"colorIndicatorForElement\">{{letterIndicatorForElement}}</span>\n" +
    "        </div>\n" +
    "        <div class=\"perso-customize-component__title-layout__cust-name\">{{selectedCustomization.name}}</div>\n" +
    "        <div class=\"perso-customize-component__title-layout__target-group-name\">{{'> ' + selectedVariation.name}}</div>\n" +
    "    </div>\n" +
    "\n" +
    "    <dl class=\"perso-customize-component__data-list\">\n" +
    "        <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.addeditaction.selected.mastercomponent.title\"></label>\n" +
    "        <dd>{{componentType}}</dd>\n" +
    "    </dl>\n" +
    "\n" +
    "    <label class=\"control-label required\" data-translate=\"personalization.modal.addeditaction.selected.actions.title\"></label>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-model=\"action.selected\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in actions\" position=\"down\">\n" +
    "            <span ng-bind=\"item.name \"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-show=\"action.selected.id == 'use'\" ng-model=\"component.selected\" ng-keyup=\"componentSearchInputKeypress($event, $select.search)\" theme=\"select2\" title=\"\" reset-search-input=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.componentlist.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name | translate\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in components\" position=\"down\" personalization-infinite-scroll=\"addMoreComponentItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "            <div class=\"row\">\n" +
    "                <span class=\"col-md-7\" ng-bind=\"item.name\"></span>\n" +
    "                <span class=\"col-md-5\" ng-bind=\"item.typeCode\"></span>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "\n" +
    "    <ui-select class=\"perso-customize-component__select2-container\" ng-show=\"action.selected.id == 'create'\" on-select=\"newComponentTypeSelectedEvent($item, $model)\" ng-model=\"newComponent.selected\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.addeditaction.dropdown.componenttype.placeholder' | translate}}\">\n" +
    "            <span ng-bind=\"$select.selected.name\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in newComponentTypes\" position=\"down\">\n" +
    "            <span ng-bind=\"item.i18nKey | translate\"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/contextMenu/personalizationsmarteditShowActionListTemplate.html',
    "<div class=\"pe-combinedview-ranking\">\n" +
    "    <p class=\"pe-combinedview-ranking__help-text\" data-translate=\"personalization.modal.showactionlist.help.label\"></p>\n" +
    "    <div class=\"pe-combinedview-ranking__divider\"></div>\n" +
    "    <div class=\"pe-combinedview-ranking__list-layout\" data-ng-repeat=\"item in selectedItems\" data-ng-init=\"initItem(item)\" data-ng-show=\"item.visible\">\n" +
    "        <div data-ng-class=\"getClassForElement($index)\" data-ng-bind=\"getLetterForElement($index)\"></div>\n" +
    "        <div class=\"pe-combinedview-ranking__names-layout\">\n" +
    "            <div class=\"perso-wrap-ellipsis pe-combinedview-ranking__cname\" data-ng-bind=\"item.customization.name\"></div>\n" +
    "            <span> ></span>\n" +
    "            <div class=\"perso-wrap-ellipsis pe-combinedview-ranking__vname\" data-ng-bind=\"item.variation.name\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/commerceCustomizationView/personalizationsmarteditCommerceCustomizationViewTemplate.html',
    "<div id=\"commerceCustomizationBody-002\" class=\"perso-cc-modal\">\n" +
    "    <div class=\"perso-cc-modal__title-layout\">\n" +
    "        <div class=\"perso-wrap-ellipsis perso-cc-modal__title-cname\" data-ng-bind=\"customization.name\"></div>\n" +
    "        <div class=\"perso-cc-modal__title-status\" data-ng-class=\"customizationStatus\">{{ '(' + customizationStatusText + ')' }}</div>\n" +
    "        <span> > </span>\n" +
    "        <div class=\"perso-wrap-ellipsis perso-cc-modal__title-vname\" data-ng-bind=\"variation.name\"></div>\n" +
    "        <div class=\"perso-cc-modal__title-status\" data-ng-class=\"variationStatus\">{{' (' + variationStatusText + ')'}}</div>\n" +
    "    </div>\n" +
    "    <div class=\"form-group perso-cc-modal__content-layout\">\n" +
    "        <label for=\"commerce-customization-type-1\" class=\"personalization-modal__label\" data-translate=\"personalization.modal.commercecustomization.action.type\"></label>\n" +
    "        <ui-select id=\"commerce-customization-type-1\" class=\"form-control\" ng-model=\"select.type\" theme=\"select2\" search-enabled=\"false\">\n" +
    "            <ui-select-match>\n" +
    "                <span data-ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"item in availableTypes\" position=\"down\">\n" +
    "                <span data-ng-bind=\"item.text | translate\"></span>\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "    </div>\n" +
    "    <div class=\"form-group perso-cc-modal__content-layout\">\n" +
    "        <ng-include src=\"select.type.template\" />\n" +
    "    </div>\n" +
    "    <div class=\"select2-choices\">\n" +
    "        <div class=\"ui-select-match-item select2-search-choice perso-cc-modal__ui-select--small\" data-ng-repeat=\"action in getActionsToDisplay()\">\n" +
    "            <span data-ng-bind=\"displayAction(action)\"></span>\n" +
    "            <span class=\"ui-select-match-close select2-search-choice-close\" data-ng-click=\"removeSelectedAction(action)\"></span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/commerceCustomizationView/promotions/personalizationsmarteditPromotionsTemplate.html',
    "<div ng-controller=\"personalizationsmarteditPromotionController\">\n" +
    "    <label for=\"promotion-selector-1\" class=\"personalization-modal__label\" data-translate=\"personalization.modal.commercecustomization.promotion.label\"></label>\n" +
    "    <ui-select uis-open-close=\"initUiSelect($select)\" id=\"promotion-selector-1\" class=\"form-control\" ng-model=\"promotion\" on-select=\"promotionSelected($item, $select)\" theme=\"select2\" search-enabled=\"true\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.modal.commercecustomization.promotion.search.placeholder' | translate}}\">\n" +
    "            <span>{{'personalization.modal.commercecustomization.promotion.search.placeholder' | translate}}</span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in availablePromotions | filter: $select.search\" ui-disable-choice=\"isItemInSelectDisabled(item)\" position=\"down\">\n" +
    "            <div class=\"row ng-scope\">\n" +
    "                <span class=\"col-md-8 ng-binding\" ng-bind=\"item.code\"></span>\n" +
    "                <span class=\"col-md-4 ng-binding\" ng-bind=\"item.promotionGroup\"></span>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewGridHeaderTemplate.html',
    "<div class=\"row tree-head hidden-sm hidden-xs\">\n" +
    "    <div class=\"col-md-4 text-left\" data-translate=\"personalization.modal.manager.grid.header.customization\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding\" data-translate=\"personalization.modal.manager.grid.header.variations\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding se-nowrap-ellipsis\" data-translate=\"personalization.modal.manager.grid.header.components\"></div>\n" +
    "    <div class=\"col-md-1 col-reset-padding\">\n" +
    "        <span class=\"perso-library__status-header\" data-translate=\"personalization.modal.manager.grid.header.status\"></span>\n" +
    "    </div>\n" +
    "    <div class=\"col-md-2 text-center\" data-translate=\"personalization.modal.manager.grid.header.startdate\"></div>\n" +
    "    <div class=\"col-md-2 text-center\" data-translate=\"personalization.modal.manager.grid.header.enddate\"></div>\n" +
    "</div>\n" +
    "<div class=\"row tree-head visible-sm visible-xs\">\n" +
    "    <div class=\"col-xs-10 text-left\" data-translate=\"personalization.modal.manager.grid.header.customization\"></div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewNodeTemplate.html',
    "<div class=\"y-tree-row\" ng-init=\"customization.isCollapsed=true\" ng-class=\"allCustomizationsCollapsed()? 'active-level' : 'inactive-level'\">\n" +
    "    <div class=\"row desktop-layout hidden-sm hidden-xs customization-rank-{{customization.rank}}-row\">\n" +
    "        <div class=\"pull-left\" data-ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <a class=\"btn btn-link category-toggle\" title=\"{{customization.isCollapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span data-ng-class=\"customization.isCollapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon customization__glyphicon-chevron\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div ui-tree-handle class=\"y-tree-row__angular-ui-tree-handle\">\n" +
    "            <div class=\"col-md-4 text-left\">\n" +
    "                <p class=\"perso-library__primary-data\">\n" +
    "                    <span class=\"personalizationsmartedit-customization-code\" ng-bind=\"customization.name\"></span>\n" +
    "                </p>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-1 text-left\" data-ng-bind=\"(customization.variations | statusNotDeleted).length || 0\">\n" +
    "            </div>\n" +
    "            <div class=\"col-md-1\"></div>\n" +
    "            <div class=\"col-md-1 col-reset-padding text-left perso-library__status-value\" data-ng-class=\"getActivityStateForCustomization(customization)\">\n" +
    "                <span ng-bind=\"getEnablementTextForCustomization(customization)\" class=\"perso-library__status-layout\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 perso-library__dates-layout\">\n" +
    "                <div data-ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"text-center perso-library__dates-value\">\n" +
    "                    <span data-ng-bind=\"getFormattedDate(customization.enabledStartDate)\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-2 perso-library__dates-layout\">\n" +
    "                <div data-ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"text-center perso-library__dates-value\">\n" +
    "                    <span data-ng-bind=\"getFormattedDate(customization.enabledEndDate)\"></span>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"pull-right\">\n" +
    "            <div class=\"dropdown\">\n" +
    "                <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                    <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                </button>\n" +
    "                <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                    <li>\n" +
    "                        <a data-ng-click=\"editCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.edit\"></a>\n" +
    "                    </li>\n" +
    "                    <li ng-class=\"isFilterEnabled() || $first ? 'disabled' : '' \">\n" +
    "                        <a data-ng-click=\"(isFilterEnabled() || $first) ? $event.stopPropagation() : setCustomizationRank(customization, -1)\" data-translate=\"personalization.modal.manager.customization.options.moveup\"></a>\n" +
    "                    </li>\n" +
    "                    <li ng-class=\"isFilterEnabled() || $last ? 'disabled' : '' \">\n" +
    "                        <a data-ng-click=\"(isFilterEnabled() || $last) ? $event.stopPropagation() : setCustomizationRank(customization, 1)\" data-translate=\"personalization.modal.manager.customization.options.movedown\"></a>\n" +
    "                    </li>\n" +
    "                    <li role=\"separator\" class=\"divider\"></li>\n" +
    "                    <li>\n" +
    "                        <a data-ng-click=\"deleteCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.delete\"></a>\n" +
    "                    </li>\n" +
    "                </ul>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "        <!--end contextual menu dropdown-->\n" +
    "    </div>\n" +
    "    <!--end desktop-layout for customization row-->\n" +
    "\n" +
    "    <div class=\"row mobile-layout visible-sm visible-xs customization-rank-{{customization.rank}}-row\">\n" +
    "        <div class=\"pull-left\" data-ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <a class=\"btn btn-link category-toggle\" title=\"{{customization.isCollapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span data-ng-class=\"customization.isCollapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon customization__glyphicon-chevron\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-10 text-left\" ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "            <p class=\"perso-library__primary-data\">\n" +
    "                <span class=\"personalizationsmartedit-customization-code\" ng-bind=\"customization.name\"></span>\n" +
    "            </p>\n" +
    "            <div class=\"mobile-data\">\n" +
    "                <span class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.variations\"></span>\n" +
    "                <p ng-bind=\"(customization.variations | statusNotDeleted).length || 0\"></p>\n" +
    "            </div>\n" +
    "            <div class=\"mobile-data perso-library__status-value\" data-ng-class=\"getActivityStateForCustomization(customization)\">\n" +
    "                <span ng-bind=\"getEnablementTextForCustomization(customization)\"></span>\n" +
    "            </div>\n" +
    "            <div ng-show=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\">\n" +
    "                <div class=\"mobile-data\" ng-if='customization.enabledStartDate || customization.enabledEndDate'>\n" +
    "                    <span ng-if='customization.enabledStartDate' class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.startdate\"></span>\n" +
    "                    <p ng-bind=\"getFormattedDate(customization.enabledStartDate)\"></p>\n" +
    "                    <span ng-if='customization.enabledEndDate' class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.enddate\"></span>\n" +
    "                    <p ng-bind=\"getFormattedDate(customization.enabledEndDate)\"></p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"pull-left\">\n" +
    "            <div class=\"mobile-more-menu\">\n" +
    "                <div class=\"dropdown\">\n" +
    "                    <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                        <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a data-ng-click=\"editCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.edit\"></a>\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"isFilterEnabled() || $first ? 'disabled' : '' \">\n" +
    "                            <a data-ng-click=\"(isFilterEnabled() || $first) ? $event.stopPropagation() : setCustomizationRank(customization, -1)\" data-translate=\"personalization.modal.manager.customization.options.moveup\"></a>\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"isFilterEnabled() || $last ? 'disabled' : '' \">\n" +
    "                            <a data-ng-click=\"(isFilterEnabled() || $last) ? $event.stopPropagation() : setCustomizationRank(customization, 1)\" data-translate=\"personalization.modal.manager.customization.options.movedown\"></a>\n" +
    "                        </li>\n" +
    "                        <li role=\"separator\" class=\"divider\"></li>\n" +
    "                        <li>\n" +
    "                            <a data-ng-click=\"deleteCustomizationAction(customization)\" data-translate=\"personalization.modal.manager.customization.options.delete\"></a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!--end contextual menu dropdown-->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end mobile-layout for customization row-->\n" +
    "</div>\n" +
    "<!--end angular-ui-tree-handle-->\n" +
    "<div data-uib-collapse=\"customization.isCollapsed\">\n" +
    "    <div ui-tree-nodes ng-model=\"customization.variations\">\n" +
    "        <div class=\"y-tree-row child-row active-level\" data-ng-class=\"{'perso-library__angular-ui-tree-drag': variation.isDragging}\" ui-tree-node ng-repeat=\"variation in customization.variations | orderBy:'rank'\" ng-if=\"statusNotDeleted(variation)\" ng-init=\"variation.isCommerceCustomizationEnabled=isCommerceCustomizationEnabled()\">\n" +
    "            <div class=\"row desktop-layout variation-rank-{{variation.rank}}-row hidden-sm hidden-xs\" ng-class=\"$last ? 'active-level--last':''\">\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <span class=\"perso-library__cc-marker\" data-ng-class=\"{'perso-library__cc-icon': hasCommerceCustomization(variation)}\" data-uib-tooltip=\"{{getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></span>\n" +
    "                </div>\n" +
    "                <div ui-tree-handle class=\"y-tree-row__angular-ui-tree-handle\">\n" +
    "                    <div class=\"col-xs-5 text-left\">\n" +
    "                        <span ng-bind=\"variation.name\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-1 text-left\" data-ng-bind=\"variation.numberOfComponents\">\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-1 col-reset-padding text-left perso-library__status-value\" data-ng-class=\"getActivityStateForVariation(customization,variation)\">\n" +
    "                        <span ng-bind=\"getEnablementTextForVariation(variation)\" class=\"perso-library__status-layout\"></span>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-4 perso-library__dates-layout\"></div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-right\">\n" +
    "                    <div class=\"dropdown\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                            <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                        </button>\n" +
    "                        <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                            <li>\n" +
    "                                <a data-ng-click=\"editVariationAction(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.edit\"></a>\n" +
    "                            </li>\n" +
    "                            <li>\n" +
    "                                <a data-ng-click=\"toogleVariationActive(customization,variation)\" ng-bind=\"getEnablementActionTextForVariation(variation)\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-show=\"variation.isCommerceCustomizationEnabled\">\n" +
    "                                <a data-ng-click=\"manageCommerceCustomization(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.commercecustomization\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                                <a data-ng-click=\"$first ? $event.stopPropagation() : setVariationRank(customization, variation, -1)\" data-translate=\"personalization.modal.manager.variation.options.moveup\"></a>\n" +
    "                            </li>\n" +
    "                            <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                                <a data-ng-click=\"$last ? $event.stopPropagation() : setVariationRank(customization, variation, 1)\" data-translate=\"personalization.modal.manager.variation.options.movedown\"></a>\n" +
    "                            </li>\n" +
    "                            <li role=\"separator\" class=\"divider\"></li>\n" +
    "                            <li ng-class=\"isDeleteVariationEnabled(customization) ? '' : 'disabled' \">\n" +
    "                                <a data-ng-click=\"deleteVariationAction(customization, variation, $event)\" data-translate=\"personalization.modal.manager.variation.options.delete\"></a>\n" +
    "                            </li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <!--end contextual menu dropdown-->\n" +
    "            </div>\n" +
    "            <!--end desktop-layout for variation row-->\n" +
    "            <div class=\"row mobile-layout visible-sm visible-xs variation-rank-{{variation.rank}}-row\" ng-class=\"$last ? 'active-level--last':''\">\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <span class=\"perso-library__cc-marker\" data-ng-class=\"{'perso-library__cc-icon': hasCommerceCustomization(variation)}\" data-uib-tooltip=\"{{getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></span>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-10 text-left\" ng-click=\"customization.isCollapsed =! customization.isCollapsed; customizationClickAction(customization)\">\n" +
    "                    <p ng-bind=\"variation.name\"></p>\n" +
    "                    <div class=\"mobile-data\">\n" +
    "                        <span class=\"tree-head y-tree-row__mobile-layout__header\" data-translate=\"personalization.modal.manager.grid.header.components\"></span>\n" +
    "                        <div ng-bind=\"variation.numberOfComponents\"></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"mobile-data perso-library__status-value\" data-ng-class=\"getActivityStateForVariation(customization,variation)\">\n" +
    "                        <span ng-bind=\"getEnablementTextForVariation(variation)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-left\">\n" +
    "                    <div class=\"mobile-more-menu\">\n" +
    "                        <div class=\"dropdown\">\n" +
    "                            <button type=\"button\" class=\"btn btn-link dropdown-toggle pull-right\" data-toggle=\"dropdown\">\n" +
    "                                <span class=\"hyicon hyicon-more perso-library__hyicon-more\"></span>\n" +
    "                            </button>\n" +
    "                            <ul class=\"dropdown-menu pull-right text-left\" role=\"menu\">\n" +
    "                                <li>\n" +
    "                                    <a data-ng-click=\"editVariationAction(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.edit\"></a>\n" +
    "                                </li>\n" +
    "                                <li>\n" +
    "                                    <a data-ng-click=\"toogleVariationActive(customization,variation)\" ng-bind=\"getEnablementActionTextForVariation(variation)\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-show=\"variation.isCommerceCustomizationEnabled\">\n" +
    "                                    <a data-ng-click=\"manageCommerceCustomization(customization, variation)\" data-translate=\"personalization.modal.manager.variation.options.commercecustomization\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                                    <a data-ng-click=\"$first ? $event.stopPropagation() : setVariationRank(customization, variation, -1)\" data-translate=\"personalization.modal.manager.variation.options.moveup\"></a>\n" +
    "                                </li>\n" +
    "                                <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                                    <a data-ng-click=\"$last ? $event.stopPropagation() : setVariationRank(customization, variation, 1)\" data-translate=\"personalization.modal.manager.variation.options.movedown\"></a>\n" +
    "                                </li>\n" +
    "                                <li role=\"separator\" class=\"divider\"></li>\n" +
    "                                <li ng-class=\"isDeleteVariationEnabled(customization) ? '' : 'disabled' \">\n" +
    "                                    <a data-ng-click=\"deleteVariationAction(customization, variation, $event)\" data-translate=\"personalization.modal.manager.variation.options.delete\"></a>\n" +
    "                                </li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <!--end contextual menu dropdown-->\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <!--end mobile-layout for variation row-->\n" +
    "        </div>\n" +
    "        <!--end variation-repeat-->\n" +
    "    </div>\n" +
    "</div>\n" +
    "<!--end collapse-->\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/managerView/personalizationsmarteditManagerViewTemplate.html',
    "<div id=\"editConfigurationsBody-001\" class=\"perso-library\">\n" +
    "\n" +
    "    <personalizationsmartedit-scroll-zone data-ng-if=\"scrollZoneElement != null\" data-is-transparent=\"true\" data-scroll-zone-visible=\"scrollZoneVisible\" data-get-element-to-scroll=\"getElementToScroll()\"></personalizationsmartedit-scroll-zone>\n" +
    "\n" +
    "    <div class=\"perso-library__scroll-zone perso__scrollbar--hidden\" personalization-current-element=\"setScrollZoneElement\" personalization-infinite-scroll=\"addMoreItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "        <div>\n" +
    "            <h2 class=\"text-capitalize perso-library__header\" data-ng-bind=\"catalogName\"></h2>\n" +
    "            <div class=\"row perso-library__search\">\n" +
    "                <div class=\"col-sm-4 col-sm-offset-3\">\n" +
    "                    <div class=\"input-group\">\n" +
    "                        <span class=\"input-group-addon hyicon hyicon-search perso-library__search-icon\"></span>\n" +
    "                        <input type=\"text\" class=\"form-control perso-library__search-input ng-pristine ng-untouched ng-valid\" placeholder=\"{{ 'personalization.modal.manager.search.placeholder' | translate}}\" ng-model=\"search.name\" ng-keyup=\"searchInputKeypress($event)\"></input>\n" +
    "                        <span data-ng-show=\"search.name\" class=\"input-group-addon glyphicon glyphicon-remove-sign ySESearchIcon\" ng-click=\"search.name=''; searchInputKeypress($event)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-sm-2 pull-left perso__filter-layout\">\n" +
    "                    <ui-select ng-model=\"search.status\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "                        <ui-select-match>\n" +
    "                            <span ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "                        </ui-select-match>\n" +
    "                        <ui-select-choices repeat=\"item in statuses\" position=\"down\">\n" +
    "                            <span ng-bind=\"item.text | translate\"></span>\n" +
    "                        </ui-select-choices>\n" +
    "                    </ui-select>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div data-ng-show=\"isSearchResultHidden()\" class=\"perso-library__search-results--fixed\">\n" +
    "                <span data-ng-bind=\"filteredCustomizationsCount\"></span>\n" +
    "                <span data-translate=\"personalization.modal.manager.search.result.label\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"perso-library__search-results\">\n" +
    "                <span data-ng-bind=\"filteredCustomizationsCount\"></span>\n" +
    "                <span data-translate=\"personalization.modal.manager.search.result.label\"></span>\n" +
    "            </div>\n" +
    "\n" +
    "            <button class=\"y-add y-add-btn perso-library__add-button\" type=\"button\" data-ng-click=\"openNewModal();\">\n" +
    "                <span class=\"hyicon hyicon-add\" data-translate=\"personalization.modal.manager.add.button\"></span>\n" +
    "            </button>\n" +
    "\n" +
    "            <div class=\"y-tree perso-library__y-tree\">\n" +
    "                <!-- headers -->\n" +
    "                <div data-ng-show=\"isSearchGridHeaderHidden()\" class=\"y-tree-header y-tree-header--fixed\">\n" +
    "                    <data-ng-include src=\"'personalizationsmarteditManagerViewGridHeaderTemplate.html'\"></data-ng-include>\n" +
    "                </div>\n" +
    "                <div class=\"y-tree-header\">\n" +
    "                    <data-ng-include src=\"'personalizationsmarteditManagerViewGridHeaderTemplate.html'\"></data-ng-include>\n" +
    "                </div>\n" +
    "                <!--end y-tree-header-->\n" +
    "\n" +
    "                <div ui-tree=\"treeOptions\" id=\"tree-root\">\n" +
    "                    <div ui-tree-nodes data-ng-model=\"customizations\">\n" +
    "                        <div data-ng-repeat=\"customization in customizations\" data-ng-class=\"{'perso-library__angular-ui-tree-drag': customization.isDragging}\" ui-tree-node data-ng-include=\"'personalizationsmarteditManagerViewNodeTemplate.html'\"></div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div data-ng-show=\"moreCustomizationsRequestProcessing\">\n" +
    "                    <div data-ng-include=\"'waitDialog.html'\"></div>\n" +
    "                </div>\n" +
    "                <!--end customization-repeat-->\n" +
    "            </div>\n" +
    "            <!--end y-tree-->\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end scroll zone-->\n" +
    "\n" +
    "    <a class=\"perso-library__back-to-top-button\" title=\"{{'personalization.commons.button.title.backtotop' | translate}}\" data-ng-show=\"isReturnToTopButtonVisible()\" data-ng-click=\"scrollZoneReturnToTop()\">\n" +
    "        <span class=\"hyicon hyicon-top back-to-top\"></span>\n" +
    "    </a>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/personalizationsmarteditCustomizationManagMenuTemplate.html',
    "<ul data-ng-controller=\"topToolbarMenuController\" class=\"se-toolbar-menu-content se-toolbar-menu-content--pe-customized\">\n" +
    "    <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "        <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.library.header.title\"></h2>\n" +
    "        <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.library.header.description\"></small>\n" +
    "    </div>\n" +
    "    <li class=\"se-toolbar-menu-content--pe-customized__item\">\n" +
    "        <a class=\"se-toolbar-menu-content--pe-customized__item__link\" id=\"personalizationsmartedit-pagecustomizations-toolbar-customization-anchor\" data-translate=\"personalization.toolbar.library.manager.name\" data-ng-click=\"managerViewClick()\"></a>\n" +
    "    </li>\n" +
    "    <li class=\"se-toolbar-menu-content--pe-customized__item se-toolbar-menu-content--pe-customized__item--last\">\n" +
    "        <a class=\"se-toolbar-menu-content--pe-customized__item__link\" id=\"personalizationsmartedit-pagecustomizations-toolbar-customization-anchor\" data-translate=\"personalization.toolbar.library.customizationvariationmanagement.name\" data-ng-click=\"createCustomizationClick()\"></a>\n" +
    "    </li>\n" +
    "</ul>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/personalizationsmarteditCustomizationManagTemplate.html',
    "<div>\n" +
    "    <uib-tabset active=\"activeTabNumber\">\n" +
    "        <uib-tab ng-repeat=\"tab in tabsArr\" select=\"selectTab(tab)\" disable=\"tab.disabled\" heading=\"{{tab.heading}}\">\n" +
    "            <form name=\"{{tab.formName}}\" novalidate>\n" +
    "                <div>\n" +
    "                    <data-ng-include src=\"tab.template\"></data-ng-include>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </uib-tab>\n" +
    "    </uib-tabset>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/personalizationsmarteditCustVarManagBasicInfoTemplate.html',
    "<div class=\"customization-form\">\n" +
    "    <form>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-name\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.name\" class=\"control-label required\"></label>\n" +
    "            <input type=\"text\" class=\"form-control\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.basicinformationtab.name.placeholder' | translate}}\" name=\"{{customization.name}}_key\" data-ng-model=\"customization.name\" data-ng-required=\"true\" id=\"customization-name\">\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-description\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details\" class=\"personalization-modal__label\"></label>\n" +
    "            <textarea rows=\"2\" class=\"form-control description-area\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.basicinformationtab.details.placeholder' | translate}}\" name=\"{{customization.description}}_key\" data-ng-model=\"customization.description\" id=\"customization-description\"></textarea>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"customization-status\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.status\" class=\"personalization-modal__label\"></label>\n" +
    "            <div class=\"status-toggle\">\n" +
    "                <span class=\"y-toggle y-toggle-lg\" id=\"customization-status\" ng-init=\"customization.status = PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\">\n" +
    "                    <input type=\"checkbox\" id=\"test-checkbox\" data-ng-model=\"customization.statusBoolean\" />\n" +
    "                    <label for=\"test-checkbox\"></label>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <div>\n" +
    "                <a ng-show=\"!edit.datetimeConfigurationEnabled\" data-ng-click=\"edit.datetimeConfigurationEnabled = !edit.datetimeConfigurationEnabled\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.showdateconfigdata\"></a>\n" +
    "                <a ng-show=\"edit.datetimeConfigurationEnabled\" data-ng-click=\"edit.datetimeConfigurationEnabled = !edit.datetimeConfigurationEnabled; resetDateTimeConfiguration();\" data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.hidedateconfigdata\"></a>\n" +
    "            </div>\n" +
    "            <div data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.statusfortimeframe.description\"></div>\n" +
    "        </div>\n" +
    "        <div class=\"form-group\">\n" +
    "            <div ng-show=\"edit.datetimeConfigurationEnabled\">\n" +
    "                <div class=\"row form-group-dates\">\n" +
    "                    <date-time-picker-range name=\"data-date-time-from-to-key\" data-date-from=\"customization.enabledStartDate\" data-date-to=\"customization.enabledEndDate\" data-is-editable=\"true\" date-format=\"edit.viewDateFormat\"></date-time-picker-range>\n" +
    "                </div>\n" +
    "                <span class=\"help-inline {$modifiers}\">\n" +
    "                    <span data-translate=\"personalization.modal.customizationvariationmanagement.basicinformationtab.details.timeframe.description\" class=\"help-block-inline text-muted\"></span>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </form>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/personalizationsmarteditCustVarManagTargetGrpTemplate.html',
    "<div ng-controller=\"personalizationsmarteditManagerTargetGrpController as $ctrl\" class=\"customization-form\">\n" +
    "    <div class=\"customization-form__title\">\n" +
    "        <div class=\"customization-form__title-header\">\n" +
    "            <div ng-bind=\"customization.name\" class=\"customization-form__title-header-name\"></div>\n" +
    "            <div class=\"customization-form__title-header-badge\">\n" +
    "                <span class=\"badge\" ng-class=\"{'status-active':'badge-success', 'status-inactive':'', 'status-ignore':''}[getActivityStateForCustomization(customization)]\">\n" +
    "                    {{'personalization.modal.customizationvariationmanagement.targetgrouptab.customization.' + customization.status | lowercase | translate}}\n" +
    "\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div ng-if=\"customization.status === PERSONALIZATION_MODEL_STATUS_CODES.ENABLED\" class=\"customization-form__title-dates\">\n" +
    "            <span ng-if='!customization.enabledStartDate && customization.enabledEndDate'>...</span>\n" +
    "            <span ng-bind=\"customization.enabledStartDate\"></span>\n" +
    "            <span ng-if='customization.enabledStartDate || customization.enabledEndDate'> - </span>\n" +
    "            <span ng-bind=\"customization.enabledEndDate\"></span>\n" +
    "            <span ng-if=\"isEndDateInThePast\" class=\"section-help help-inline help-inline--section help-inline--tooltip\">\n" +
    "                <span class=\"hyicon hyicon-msgwarning hyicon-msgwarning--dates help-icon-inline\"></span>\n" +
    "                <span class=\"help-block-inline help-block-inline--text \" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.datetooltip\"></span>\n" +
    "            </span>\n" +
    "            <span ng-if='customization.enabledStartDate && !customization.enabledEndDate'>...</span>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"customization-form__navtab-forms\">\n" +
    "        <div class=\"customization-form__navtab-forms--add-btn\">\n" +
    "            <button class=\"y-add-btn\" type=\"button\" data-ng-click=\"$ctrl.showSliderPanel()\">\n" +
    "                <span class=\"hyicon hyicon-add\"></span>\n" +
    "                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.addtargetgroup.button\"></span>\n" +
    "            </button>\n" +
    "        </div>\n" +
    "        <ul ng-show='filteredVariations.length > 0' class=\"customization-form__list-group\">\n" +
    "            <li class=\"customization-form__list-group__item\" data-ng-repeat=\"variation in customization.variations | statusNotDeleted as filteredVariations\">\n" +
    "                <div class=\"customization-form__variation-data\">\n" +
    "                    <a class=\"navtab-row__item__variation\" ng-bind=\"variation.name\" ng-click=\"editVariationAction(variation)\"></a>\n" +
    "                    <span ng-class=\"getActivityStateForVariation(customization, variation)\" ng-bind=\"getEnablementTextForVariation(variation)\"></span>\n" +
    "                    <div data-ng-show=\"isDefaultVariation(variation)\">\n" +
    "                        <span class=\"targetgroup-list__title\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.colon\"></span>\n" +
    "                        <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.default\"></span>\n" +
    "                    </div>\n" +
    "                    <div data-ng-show=\"!isDefaultVariation(variation)\">\n" +
    "                        <div>\n" +
    "                            <span class=\"targetgroup-list__title\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.colon\"></span>\n" +
    "                            <personalizationsmartedit-segment-expression-as-html data-segment-expression=\"variation.triggers\"></personalizationsmartedit-segment-expression-as-html>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"customization-form__variation-menu dropdown\">\n" +
    "                    <button type=\"button\" class=\"dropdown-toggle btn btn-link segment-contextual-menu__button dropdown-toggle\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">\n" +
    "                        <span class=\"hyicon hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul class=\"dropdown-menu\" aria-labelledby=\"dropdownMenu1\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"editVariationAction(variation)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.edit\" />\n" +
    "                        </li>\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"toogleVariationActive(variation)\" ng-bind=\"getActivityActionTextForVariation(variation)\" />\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"$first ? 'disabled' : '' \">\n" +
    "                            <a ng-click=\"setVariationRank(variation, -1, $event, $first)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.moveup\" />\n" +
    "                        </li>\n" +
    "                        <li ng-class=\"$last ? 'disabled' : '' \">\n" +
    "                            <a ng-click=\"setVariationRank(variation, 1, $event, $last)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.movedown\" />\n" +
    "                        </li>\n" +
    "                        <li role=\"separator\" class=\"divider\"></li>\n" +
    "                        <li>\n" +
    "                            <a ng-click=\"removeVariationClick(variation)\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.options.remove\" />\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </li>\n" +
    "        </ul>\n" +
    "    </div>\n" +
    "\n" +
    "    <y-slider-panel data-slider-panel-show=\"$ctrl.showSliderPanel\" data-slider-panel-hide=\"$ctrl.hideSliderPanel\" data-slider-panel-configuration=\"$ctrl.sliderPanelConfiguration\" class=\"customization-form__sliderpanel\">\n" +
    "        <content>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"pull-right\">\n" +
    "                    <button class=\"btn btn-link perso-sliderpanel__btn-link\" data-ng-init=\"isFullscreen=false\" data-ng-click=\"toggleSliderFullscreen(); isFullscreen=!isFullscreen\">\n" +
    "                        <div>\n" +
    "                            <div data-ng-if=\"!isFullscreen\">\n" +
    "                                <span class=\"hyicon hyicon-maximize perso-sliderpanel__icon\"></span>\n" +
    "                                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.fullscreen.open\"></span>\n" +
    "                            </div>\n" +
    "                            <div data-ng-if=\"isFullscreen\">\n" +
    "                                <span class=\"hyicon hyicon-minimize perso-sliderpanel__icon\"></span>\n" +
    "                                <span data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.fullscreen.close\"></span>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"targetgroup-name\" class=\"control-label required\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupname\"></label>\n" +
    "                    <input uniquetargetgroupname type=\"text\" class=\"form-control\" placeholder=\"{{'personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupname.placeholder' | translate}}\" name=\"variationname_key\" data-ng-model=\"edit.name\" id=\"targetgroup-name\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <div class=\"checkbox\">\n" +
    "                        <input type=\"checkbox\" id=\"targetgroup-isDefault-001\" ng-model=\"edit.isDefault\" ng-change=\"confirmDefaultTrigger(edit.isDefault);\" />\n" +
    "                        <label for=\"targetgroup-isDefault-001\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.variation.default\"></label>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <span ng-show=\"{{tab.formName}}.variationname_key.$error.uniquetargetgroupname\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroup.uniquename.validation.message\"></span>\n" +
    "\n" +
    "                <div ng-show=\"edit.showExpression\">\n" +
    "                    <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.targetgroupexpression\"></label>\n" +
    "                    <multi-segment-view data-triggers='edit.selectedVariation.triggers' data-expression='edit.expression'></multi-segment-view>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "        </content>\n" +
    "    </y-slider-panel>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/personalizationsmarteditSegmentNodeTemplate.html',
    "<div data-ng-class=\"{'perso-tree__segments-layout': ctrl.isContainer(node), 'perso-tree__empty-container': ctrl.isContainerWithDropzone(node), 'perso-tree__collapsed-container': collapsed }\">\n" +
    "    <div class=\"perso-tree\" data-ng-class=\"{'perso-tree__node': ctrl.isItem(node), 'perso-tree__container': ctrl.isContainer(node)}\">\n" +
    "        <div data-ng-if=\"ctrl.isContainer(node)\">\n" +
    "            <div class=\"perso-tree__toggle\" data-ng-click=\"ctrl.toggle(this)\" title=\"{{collapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                <span class=\"glyphicon perso-tree__glyphicon\" ng-class=\"{'glyphicon-chevron-right': collapsed, 'glyphicon-chevron-down': !collapsed}\"></span>\n" +
    "            </div>\n" +
    "            <div class=\"perso-tree__dropdown\">\n" +
    "                <ui-select ng-model=\"node.operation\" theme=\"select2\" title=\"\" search-enabled=\"false\">\n" +
    "                    <ui-select-match>\n" +
    "                        <span ng-bind=\"$select.selected.name | translate\"></span>\n" +
    "                    </ui-select-match>\n" +
    "                    <ui-select-choices repeat=\"item in ctrl.actions\" position=\"down\">\n" +
    "                        <span ng-bind=\"item.name | translate\"></span>\n" +
    "                    </ui-select-choices>\n" +
    "                </ui-select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div data-ng-if=\"ctrl.isItem(node)\" ui-tree-handle class=\"perso-tree__node--content\">{{node.selectedSegment.code}}</div>\n" +
    "\n" +
    "        <div data-ng-if=\"ctrl.isDropzone(node)\" class=\"perso-tree__empty-container-node\">\n" +
    "            <div class=\"perso-tree__empty-container-node--text\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.dropzone\"></div>\n" +
    "        </div>\n" +
    "\n" +
    "        <div data-ng-if=\"ctrl.isContainer(node)\" ui-tree-handle class=\"perso-tree__angular-ui-tree-handle--empty\"></div>\n" +
    "        <div data-nodrag>\n" +
    "            <span class=\"pull-right\">\n" +
    "                <a data-ng-if=\"ctrl.isItem(node)\" class=\"perso-tree__actions perso-tree__node--icon\" data-ng-click=\"ctrl.duplicateItem(node)\" title=\"{{'personalization.commons.icon.title.duplicate' | translate}}\">\n" +
    "                    <span class=\"hyicon hyicon-duplicate\"></span>\n" +
    "                </a>\n" +
    "                <a data-ng-if=\"!ctrl.isTopContainer(this)\" class=\"perso-tree__actions\" data-ng-click=\"ctrl.removeItem(this)\">\n" +
    "                    <div data-ng-if=\"ctrl.isContainer(node)\" class=\"btn btn-link perso-tree__container--btn-icon\" title=\"{{'personalization.commons.icon.title.remove' | translate}}\">\n" +
    "                        <span class=\"hyicon hyicon-removelg\"></span>\n" +
    "                    </div>\n" +
    "                    <div data-ng-if=\"ctrl.isItem(node)\" class=\"perso-tree__node--icon\" title=\"{{'personalization.commons.icon.title.remove' | translate}}\">\n" +
    "                        <span class=\"hyicon hyicon-removelg\"></span>\n" +
    "                    </div>\n" +
    "                </a>\n" +
    "            </span>\n" +
    "            <a data-ng-if=\"ctrl.isContainer(node)\" class=\"btn btn-link perso-tree__btn\" data-ng-click=\"ctrl.newSubItem(this, 'container')\">\n" +
    "                <span class=\"perso-sliderpanel__btn-link\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments.group.button\"></span>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <ol data-ng-if=\"ctrl.isItem(node)\" ui-tree-nodes data-nodrop-enabled=\"true\" data-ng-model=\"node.nodes\" data-ng-class=\"{hidden: collapsed}\">\n" +
    "        <li data-ng-repeat=\"node in node.nodes\" ui-tree-node data-collapsed=\"true\" data-expand-on-hover=\"500\" data-ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\">\n" +
    "        </li>\n" +
    "    </ol>\n" +
    "    <ol data-ng-if=\"ctrl.isContainer(node)\" ui-tree-nodes data-ng-model=\"node.nodes\" data-ng-class=\"{hidden: collapsed}\">\n" +
    "        <li data-ng-repeat=\"node in node.nodes\" ui-tree-node data-collapsed=\"true\" data-expand-on-hover=\"500\" data-ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\">\n" +
    "        </li>\n" +
    "    </ol>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/personalizationsmarteditSegmentViewTemplate.html',
    "<div>\n" +
    "    <personalizationsmartedit-scroll-zone data-scroll-zone-visible=\"ctrl.scrollZoneVisible\" data-get-element-to-scroll=\"ctrl.getElementToScroll()\"></personalizationsmartedit-scroll-zone>\n" +
    "    <div class=\"form-group\">\n" +
    "        <personalizationsmartedit-segment-expression-as-html data-segment-expression=\"ctrl.expression[0]\"></personalizationsmartedit-segment-expression-as-html>\n" +
    "    </div>\n" +
    "    <div class=\"form-group\">\n" +
    "        <label class=\"personalization-modal__label\" data-translate=\"personalization.modal.customizationvariationmanagement.targetgrouptab.segments\"></label>\n" +
    "        <ui-select class=\"form-control\" ng-model=\"ctrl.singleSegment\" theme=\"select2\" ng-keyup=\"ctrl.segmentSearchInputKeypress($event, $select.search)\" on-select=\"ctrl.segmentSelectedEvent($item, $select.items)\" reset-search-input=\"false\">\n" +
    "            <ui-select-match placeholder=\"{{ 'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.placeholder' | translate}}\">\n" +
    "                <span>{{'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.placeholder' | translate}}</span>\n" +
    "            </ui-select-match>\n" +
    "            <ui-select-choices repeat=\"item in ctrl.segments\" position=\"down\" personalization-infinite-scroll=\"ctrl.addMoreSegmentItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "                <span ng-bind=\"item.code\"></span>\n" +
    "            </ui-select-choices>\n" +
    "        </ui-select>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ui-tree=\"ctrl.treeOptions\" id=\"tree-root\">\n" +
    "        <div ui-tree-nodes ng-model=\"ctrl.expression\" data-nodrop-enabled=\"true\">\n" +
    "            <div ng-repeat=\"node in ctrl.expression\" data-collapsed=\"false\" ui-tree-node ng-include=\"'personalizationsmarteditSegmentNodeTemplate.html'\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/management/tabs/segmentView/segmentExpressionAsHtml/personalizationsmarteditSegmentExpressionAsHtmlTemplate.html',
    "<span ng-repeat=\"word in ctrl.getExpressionAsArray() track by $index\">\n" +
    "    <span ng-if=\"ctrl.operators.indexOf(word) > -1\" class=\"perso-sliderpanel__expression-text\"> {{ ctrl.getLocalizationKeyForOperator(word) | translate }} </span>\n" +
    "    <span ng-if=\"ctrl.emptyGroup === word\" class=\"text-primary hyicon hyicon-caution\" data-uib-tooltip=\"{{'personalization.modal.customizationvariationmanagement.targetgrouptab.segments.group.tooltip' | translate}}\" data-tooltip-placement=\"auto top\"></span>\n" +
    "    <span ng-if=\"ctrl.emptyGroupAndOperators.indexOf(word) === -1\">{{word}}</span>\n" +
    "</span>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/pageCustomizationsList/pageCustomizationsListTemplate.html',
    "<div class=\"categoryTable\">\n" +
    "    <div data-ng-repeat=\"customization in ctrl.customizationsList | orderBy: 'rank'\" data-ng-init=\"ctrl.initCustomization(customization)\">\n" +
    "        <div class=\"row row-lefttoolbar\" ng-class=\"$last && customization.collapsed ? 'customization--last':''\">\n" +
    "            <div ng-class=\"{'custFromLibExtraStyling customization-from-library':customization.fromLibrary, 'customization-rank-{{customization.rank}}':true}\">\n" +
    "                <div class=\"pull-left col-lefttoolbar-glyphicon\" data-ng-click=\"ctrl.clearAllSubMenu(); customization.collapsed = !customization.collapsed; ctrl.customizationClick(customization);\">\n" +
    "                    <a class=\"btn btn-link category-toggle\" title=\"{{customization.collapsed ? 'personalization.commons.icon.title.expand' : 'personalization.commons.icon.title.collapse' | translate}}\">\n" +
    "                        <span data-ng-class=\"customization.collapsed ? 'glyphicon-chevron-right' : 'glyphicon-chevron-down'\" class=\"glyphicon lefttoolbar--glyphicon-chevron\"></span>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "                <div class=\"col-xs-9 col-reset-padding text-left\" ng-click=\"ctrl.clearAllSubMenu(); customization.collapsed = !customization.collapsed; ctrl.customizationClick(customization)\">\n" +
    "                    <span class=\"perso-list__primary-data\" ng-bind=\"customization.name\"></span>\n" +
    "                    <div>\n" +
    "                        <span class=\"perso-list__status--value\" ng-class=\"ctrl.getActivityStateForCustomization(customization)\" ng-bind=\"ctrl.getEnablementTextForCustomization(customization)\"></span>\n" +
    "                        <span class=\"perso-list__dates-layout\" ng-if=\"ctrl.isEnabled(customization)\" ng-bind=\"ctrl.getDatesForCustomization(customization)\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"pull-right dropdown\" ng-init=\"ctrl.clearAllSubMenu();\">\n" +
    "                    <button type=\"button\" class=\"btn btn-link dropdown-toggle customization-rank-{{customization.rank}}-dropdown-toggle\" data-toggle=\"dropdown\" ng-click=\"ctrl.customizationSubMenuAction(customization)\">\n" +
    "                        <span class=\"hyicon hyicon-lefttoolbar hyicon-more\"></span>\n" +
    "                    </button>\n" +
    "                    <ul ng-if=\"customization.subMenu\" class=\"dropdown-menu text-left dropdown-menu-leftoolbar\" role=\"menu\">\n" +
    "                        <li>\n" +
    "                            <a class=\"dropdown-menu-single-item cutomization-rank-{{customization.rank}}-edit-button\" ng-click=\"ctrl.clearAllSubMenu(); ctrl.editCustomizationAction(customization);\" data-translate=\"personalization.toolbar.pagecustomizations.customization.options.edit \"></a>\n" +
    "                        </li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <!--end row desktopLayout-->\n" +
    "        <div data-uib-collapse=\"customization.collapsed\">\n" +
    "            <div data-ng-repeat=\"variation in customization.variations | statusNotDeleted\">\n" +
    "                <div class=\"row row-lefttoolbar\" data-ng-class=\"ctrl.getSelectedVariationClass(variation)\">\n" +
    "                    <div class=\"pull-left col-lefttoolbar-glyphicon\">\n" +
    "                        <div data-ng-class=\"ctrl.isCommerceCustomization(variation) ? 'commerce-customization__marker--icon':'customization__marker'\" data-uib-tooltip=\"{{ctrl.getCommerceCustomizationTooltip(variation)}}\" tooltip-placement=\"top-left\"></div>\n" +
    "                    </div>\n" +
    "                    <div class=\"col-xs-9 col-reset-padding text-left\" data-ng-click=\"ctrl.clearAllSubMenu(); ctrl.variationClick(customization, variation);\">\n" +
    "                        <span data-ng-bind=\"variation.name\"></span>\n" +
    "                        <div class=\"perso-list__status--value\" data-ng-class=\"ctrl.getActivityStateForVariation(customization, variation)\" data-ng-bind=\"ctrl.getEnablementTextForVariation(variation)\"></div>\n" +
    "                        <div data-ng-class=\"ctrl.getSelectedVariationClass(variation)\"></div>\n" +
    "                    </div>\n" +
    "                    <div data-ng-show=\"ctrl.getSelectedVariationClass(variation)\" class=\"pull-right\">\n" +
    "                        <span class=\"hyicon hyicon-checkedlg perso-panel__hyicon-checkedlg\"></span>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--end data repeat-->\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/personalizationsmarteditPageCustomizationsToolbarItemTemplate.html',
    "<div id=\"personalizationsmartedit-right-toolbar-item-template\" class=\"perso-panel\">\n" +
    "    <div>\n" +
    "        <div class=\"overflow-wrapper-lefttoolbar\" personalization-infinite-scroll=\"ctrl.addMoreCustomizationItems()\" personalization-infinite-scroll-distance=\"2\" data-customizations-loaded=\"{{ctrl.customizationsOnPage.length > 0}}\">\n" +
    "            <search-customization-from-lib data-library-customizations=\"ctrl.libraryCustomizations\" data-add-more-library-customization-items=\"ctrl.addMoreLibraryCustomizationItems(searchObj)\" data-add-customization-from-library=\"ctrl.addCustomizationFromLibrary(selFromLib)\" data-is-menu-open=\"ctrl.isMenuOpen\" />\n" +
    "\n" +
    "            <statuses-dropdown data-set-selected-statuses=\"ctrl.setSelectedStatuses(value)\" data-is-customizations-empty=\"ctrl.isCustomizationsEmpty()\" data-clear-customize-context=\"ctrl.clearCustomizeContext()\" />\n" +
    "\n" +
    "            <page-customizations-list data-customizations-list=\"ctrl.customizationsList\" />\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/personalizationsmarteditPageCustomizationsToolbarItemWrapperTemplate.html',
    "<div data-ng-controller=\"topToolbarMenuController\" data-uib-dropdown data-is-open=\"item.isOpen\" class=\"btn-group pe-toolbar-action pe-combined-view-menu\" data-auto-close=\"outsideClick\">\n" +
    "    <button type=\"button\" class=\"btn btn-default pe-toolbar-action--button personalizationsmarteditTopToolbarCustomizeButton\" data-uib-dropdown-toggle data-ng-disabled=\"disabled\" aria-pressed=\"false\" data-ng-class=\"{'pe-toolbar-action-customize--button-with-context': isCustomizeCustomizationSelected()}\">\n" +
    "        <span class=\"hyicon hyicon-customize pe-toolbar-menu-ddlb--button__icon\"></span>\n" +
    "        <span class=\"pe-toolbar-action--button--txt\" data-translate=\"personalization.toolbar.pagecustomizations\"></span>\n" +
    "    </button>\n" +
    "\n" +
    "    <personalizationsmartedit-customize-toolbar-context></personalizationsmartedit-customize-toolbar-context>\n" +
    "\n" +
    "    <div data-uib-dropdown-menu class=\"btn-block pe-toolbar-action--include\">\n" +
    "        <div class=\"pe-toolbar-menu-content se-toolbar-menu-content--pe-customized\" role=\"menu\">\n" +
    "            <div class=\"se-toolbar-menu-content--pe-customized__headers\">\n" +
    "                <h2 class=\"h2 se-toolbar-menu-content--pe-customized__headers--h2\" data-translate=\"personalization.toolbar.pagecustomizations.header.title\"></h2>\n" +
    "                <small class=\"se-toolbar-menu-content--pe-customized__headers--small\" data-translate=\"personalization.toolbar.pagecustomizations.header.description\"></small>\n" +
    "            </div>\n" +
    "            <div role=\"menuitem\" class=\"pe-combined-view-menu\">\n" +
    "                <personalizationsmartedit-page-customizations-toolbar-item data-is-menu-open=\"item.isOpen\" />\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/searchCustomizationFromLib/searchCustomizationFromLibTemplate.html',
    "<div data-ng-show=\"!ctrl.searchCustomizationEnabled\">\n" +
    "    <button class=\"y-add y-add-btn\" type=\"button\" data-ng-click=\"ctrl.toggleAddMoreCustomizationsClick()\">\n" +
    "        <span class=\"hyicon hyicon-add\" data-translate=\"personalization.toolbar.pagecustomizations.addmorecustomizations.button\"></span>\n" +
    "    </button>\n" +
    "</div>\n" +
    "<div class=\"text-left search-item-lefttoolbar\" data-ng-show=\"ctrl.searchCustomizationEnabled\">\n" +
    "    <ui-select id=\"dropdownCustomizationsLibrary\" multiple='true' class=\"form-control\" ng-model=\"ctrl.selectedFromDropdownLibraryCustomizations\" theme=\"select2\" ng-disabled=\"disabled\" on-select=\"ctrl.addCustomizationFromLibrary({selFromLib: $item});ctrl.selectedFromDropdownLibraryCustomizations = [];ctrl.toggleAddMoreCustomizationsClick();\" ng-keyup=\"ctrl.customizationSearchInputKeypress($event, $select.search)\" uis-open-close=\"ctrl.libraryCustomizationsDropdownOpenClose(isOpen)\">\n" +
    "        <ui-select-match placeholder=\"{{'personalization.toolbar.pagecustomizations.addmorecustomizations.customization.library.search.placeholder' | translate}}\">\n" +
    "            {{$item.name}}\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in ctrl.libraryCustomizations\" personalization-infinite-scroll=\"ctrl.addMoreLibraryCustomizationItems()\" personalization-infinite-scroll-distance=\"2\">\n" +
    "            <div ng-html-bind=\"item.name | highlight: $select.search\"></div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-9\" ng-bind=\"item.name\"></div>\n" +
    "                <div class=\"col-xs-3 perso-list__status--value\">\n" +
    "                    <small ng-class=\"ctrl.getActivityStateForCustomization(item)\" ng-bind=\"ctrl.getEnablementTextForCustomization(item)\"></small>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/pageCustomizationsToolbar/statusesDropdown/statusesDropdownTemplate.html',
    "<div class=\"perso__filter-layout customizationsStatusDropdown\" ng-class=\"ctrl.isCustomizationsEmpty() ? 'perso__filter-layout--no-results':''\">\n" +
    "    <ui-select id=\"dropdownCustomizationsStatus\" ng-model=\"ctrl.selectedStatus\" theme=\"select2\" title=\"\" search-enabled=\"false\" on-select=\"ctrl.setSelectedStatuses({value: ctrl.selectedStatus.modelStatuses || []});ctrl.clearCustomizeContext();\" uis-open-close=\"ctrl.statusCustomizationsDropdownOpenClose(isOpen)\">\n" +
    "        <ui-select-match>\n" +
    "            <span ng-bind=\"$select.selected.text | translate\"></span>\n" +
    "        </ui-select-match>\n" +
    "        <ui-select-choices repeat=\"item in ctrl.statuses\" position=\"down\">\n" +
    "            <span ng-bind=\"item.text | translate\"></span>\n" +
    "        </ui-select-choices>\n" +
    "    </ui-select>\n" +
    "</div>"
  );


  $templateCache.put('web/features/personalizationsmarteditcontainer/toolbarContext/personalizationsmarteditToolbarContextTemplate.html',
    "<div data-ng-show=\"ctrl.visible\" class=\"pe-toolbar-item-context\">\n" +
    "    <span class=\"btn btn-default pe-toolbar-item-context__btn\">\n" +
    "        <div class=\"pe-toolbar-item-context__btn--txt\">\n" +
    "            <div class=\"pe-toolbar-item-context__btn--title\">{{ctrl.title}}</div>\n" +
    "            <div class=\"pe-toolbar-item-context__btn--subtitle\">{{ctrl.subtitle}}</div>\n" +
    "        </div>\n" +
    "    </span>\n" +
    "    <span class=\"btn btn-default pe-toolbar-item-context__btn--hyicon\" data-ng-click=\"ctrl.clear()\">\n" +
    "        <div class=\" pe-toolbar-item-context__btn--txt hyicon hyicon-remove\">\n" +
    "        </div>\n" +
    "    </span>\n" +
    "</div>"
  );

}]);

angular.module('personalizationsmarteditToolbarContextModule', [])
    .controller('personalizationsmarteditCustomizeToolbarContextController', ['$scope', '$timeout', 'personalizationsmarteditContextService', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditContextUtils', function($scope, $timeout, personalizationsmarteditContextService, personalizationsmarteditIFrameUtils, personalizationsmarteditContextUtils) {
        var self = this;

        $scope.$watch(function() {
            return personalizationsmarteditContextService.getCustomize().selectedCustomization;
        }, function(newValue, oldValue) {
            if (newValue && newValue !== oldValue) {
                self.title = newValue.name;
                self.visible = true;
            } else if (!newValue) {
                self.visible = false;
            }
        });

        $scope.$watch(function() {
            return personalizationsmarteditContextService.getCustomize().selectedVariations;
        }, function(newValue, oldValue) {
            if (newValue && newValue !== oldValue) {
                self.subtitle = newValue.name;
            }
        });

        self.clear = function() {
            personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
            $timeout((function() {
                angular.element(".personalizationsmarteditTopToolbarCustomizeButton[aria-expanded='true']").click();
            }), 0);
        };

        self.$onInit = function() {
            self.visible = false;
            if (personalizationsmarteditContextService.getCustomize().selectedCustomization) {
                self.title = personalizationsmarteditContextService.getCustomize().selectedCustomization.name;
                self.visible = true;
                if (!angular.isArray(personalizationsmarteditContextService.getCustomize().selectedVariations)) {
                    self.subtitle = personalizationsmarteditContextService.getCustomize().selectedVariations.name;
                }
            }
        };

    }])
    .controller('personalizationsmarteditCombinedViewToolbarContextController', ['$scope', 'personalizationsmarteditCombinedViewCommons', '$timeout', 'personalizationsmarteditContextService', 'personalizationsmarteditIFrameUtils', 'personalizationsmarteditContextUtils', function($scope, personalizationsmarteditCombinedViewCommons, $timeout, personalizationsmarteditContextService, personalizationsmarteditIFrameUtils, personalizationsmarteditContextUtils) {
        var self = this;

        $scope.$watch(function() {
            return personalizationsmarteditContextService.getCombinedView().customize.selectedCustomization;
        }, function(newValue, oldValue) {
            if (newValue && newValue !== oldValue) {
                self.title = newValue.name;
                self.subtitle = personalizationsmarteditContextService.getCombinedView().customize.selectedVariations.name;
                self.visible = true;
            } else if (!newValue) {
                self.visible = false;
            }
        });

        $scope.$watch(function() {
            return personalizationsmarteditContextService.getCombinedView().enabled;
        }, function(newValue, oldValue) {
            if (newValue === false && newValue !== oldValue) {
                personalizationsmarteditContextUtils.clearCombinedViewCustomizeContext(personalizationsmarteditContextService);
            }
        });

        self.clear = function() {
            personalizationsmarteditContextUtils.clearCombinedViewCustomizeContext(personalizationsmarteditContextService);
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            var variations = [];
            (combinedView.selectedItems || []).forEach(function(item) {
                variations.push({
                    customizationCode: item.customization.code,
                    variationCode: item.variation.code
                });
            });
            personalizationsmarteditCombinedViewCommons.updatePreview(variations);
        };

        self.$onInit = function() {
            self.visible = false;
            if (personalizationsmarteditContextService.getCombinedView().customize.selectedCustomization) {
                self.title = personalizationsmarteditContextService.getCombinedView().customize.selectedCustomization.name;
                self.subtitle = personalizationsmarteditContextService.getCombinedView().customize.selectedVariations.name;
                self.visible = true;
            }
        };

    }])
    .component('personalizationsmarteditCustomizeToolbarContext', {
        templateUrl: 'personalizationsmarteditToolbarContextTemplate.html',
        controller: 'personalizationsmarteditCustomizeToolbarContextController',
        controllerAs: 'ctrl',
        transclude: true
    })
    .component('personalizationsmarteditCombinedViewToolbarContext', {
        templateUrl: 'personalizationsmarteditToolbarContextTemplate.html',
        controller: 'personalizationsmarteditCombinedViewToolbarContextController',
        controllerAs: 'ctrl',
        transclude: true
    });
