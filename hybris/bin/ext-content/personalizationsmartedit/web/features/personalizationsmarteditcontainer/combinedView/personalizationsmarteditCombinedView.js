angular.module('personalizationsmarteditCombinedViewModule', [
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditCommons',
        'ui.select',
        'personalizationsmarteditContextServiceModule',
        'renderServiceModule',
        'personalizationsmarteditDataFactory',
        'modalServiceModule',
        'personalizationsmarteditPreviewServiceModule'
    ])
    .factory('personalizationsmarteditCombinedView', function($controller, modalService, MODAL_BUTTON_ACTIONS, MODAL_BUTTON_STYLES, personalizationsmarteditContextService, personalizationsmarteditCombinedViewCommons, personalizationsmarteditContextUtils) {
        var manager = {};
        manager.openManagerAction = function() {
            modalService.open({
                title: "personalization.modal.combinedview.title",
                templateUrl: 'personalizationsmarteditCombinedViewConfigureTemplate.html',
                controller: ['$scope', 'modalManager', function($scope, modalManager) {
                    $scope.modalManager = modalManager;
                    angular.extend(this, $controller('personalizationsmarteditCombinedViewController', {
                        $scope: $scope
                    }));
                }],
                buttons: [{
                    id: 'confirmCancel',
                    label: 'personalization.modal.combinedview.button.cancel',
                    style: MODAL_BUTTON_STYLES.SECONDARY,
                    action: MODAL_BUTTON_ACTIONS.DISMISS
                }, {
                    id: 'confirmOk',
                    label: 'personalization.modal.combinedview.button.ok',
                    action: MODAL_BUTTON_ACTIONS.CLOSE
                }]
            }).then(function(result) {
                personalizationsmarteditContextUtils.clearCombinedViewCustomizeContext(personalizationsmarteditContextService);
                if (personalizationsmarteditContextService.getCombinedView().enabled) {
                    personalizationsmarteditCombinedViewCommons.updatePreview(personalizationsmarteditCombinedViewCommons.getVariationsForPreviewTicket());
                }
            }, function(failure) {});
        };

        return manager;
    })
    .factory('personalizationsmarteditCombinedViewCommons', function($q, $filter, personalizationsmarteditContextService, personalizationsmarteditPreviewService, personalizationsmarteditIFrameUtils, personalizationsmarteditMessageHandler, personalizationsmarteditRestService) {
        var service = {};

        var updateActionsOnSelectedVariations = function() {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            var promissesArray = [];
            (combinedView.selectedItems || []).forEach(function(item) {
                promissesArray.push(personalizationsmarteditRestService.getActions(item.customization.code, item.variation.code).then(function successCallback(response) {
                    item.variation.actions = response.actions;
                }));
            });
            $q.all(promissesArray).then(function() {
                personalizationsmarteditContextService.setCombinedView(combinedView);
            });
        };

        service.updatePreview = function(previewTicketVariations) {
            var previewTicketId = personalizationsmarteditContextService.getSeData().sePreviewData.previewTicketId;
            personalizationsmarteditPreviewService.updatePreviewTicketWithVariations(previewTicketId, previewTicketVariations).then(function successCallback() {
                var previewData = personalizationsmarteditContextService.getSeData().sePreviewData;
                personalizationsmarteditIFrameUtils.reloadPreview(previewData.resourcePath, previewData.previewTicketId);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.updatingpreviewticket'));
            });
            updateActionsOnSelectedVariations();
        };

        service.getVariationsForPreviewTicket = function() {
            var previewTicketVariations = [];
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            (combinedView.selectedItems || []).forEach(function(item) {
                previewTicketVariations.push({
                    customizationCode: item.customization.code,
                    variationCode: item.variation.code
                });
            });
            return previewTicketVariations;
        };

        service.combinedViewEnabledEvent = function(isEnabled) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            combinedView.enabled = isEnabled;
            personalizationsmarteditContextService.setCombinedView(combinedView);
            var customize = personalizationsmarteditContextService.getCustomize();
            customize.selectedCustomization = null;
            customize.selectedVariations = null;
            customize.selectedComponents = null;
            personalizationsmarteditContextService.setCustomize(customize);
            if (isEnabled) {
                service.updatePreview(service.getVariationsForPreviewTicket());
            } else {
                service.updatePreview([]);
            }
        };

        return service;
    })
    .controller('personalizationsmarteditCombinedViewMenuController', function($scope, $filter, personalizationsmarteditUtils, personalizationsmarteditMessageHandler, personalizationsmarteditRestService, personalizationsmarteditContextService, personalizationsmarteditCombinedViewCommons) {

        $scope.combinedView = personalizationsmarteditContextService.getCombinedView();
        $scope.selectedItems = $scope.combinedView.selectedItems || [];

        var getAndSetComponentsForElement = function(customizationId, variationId) {
            personalizationsmarteditRestService.getComponenentsIdsForVariation(customizationId, variationId).then(function successCallback(response) {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                combinedView.customize.selectedComponents = response.components;
                personalizationsmarteditContextService.setCombinedView(combinedView);
            }, function errorCallback() {
                personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcomponentsforvariation'));
            });
        };

        $scope.itemClick = function(item) {
            var combinedView = personalizationsmarteditContextService.getCombinedView();
            if (!combinedView.enabled) {
                return;
            }

            $scope.selectedItems.forEach(function(elem) {
                elem.highlighted = false;
            });
            item.highlighted = true;

            combinedView.customize.selectedCustomization = item.customization;
            combinedView.customize.selectedVariations = item.variation;
            personalizationsmarteditContextService.setCombinedView(combinedView);
            getAndSetComponentsForElement(item.customization.code, item.variation.code);
            personalizationsmarteditCombinedViewCommons.updatePreview(personalizationsmarteditUtils.getVariationKey(item.customization.code, [item.variation]));
        };

        $scope.getClassForElement = personalizationsmarteditUtils.getClassForElement;
        $scope.getLetterForElement = personalizationsmarteditUtils.getLetterForElement;

        $scope.combinedViewEnabledChangeEvent = function() {
            personalizationsmarteditCombinedViewCommons.combinedViewEnabledEvent($scope.combinedView.enabled);
        };

        $scope.$watch('combinedView.selectedItems', function(newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.selectedItems = $scope.combinedView.selectedItems || [];
            }
        }, true);

    })
    .controller('personalizationsmarteditCombinedViewController', function($q, $scope, $filter, personalizationsmarteditCombinedViewCommons, personalizationsmarteditContextService, personalizationsmarteditRestService, customizationDataFactory, PaginationHelper, personalizationsmarteditMessageHandler, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, personalizationsmarteditUtils) {

        customizationDataFactory.resetData();

        var successCallback = function(response) {
            $scope.pagination = new PaginationHelper(response.pagination);
            $scope.selectionArray.length = 0;
            customizationDataFactory.items.map(function(customization) {
                customization.variations.forEach(function(variation) {
                    $scope.selectionArray.push({
                        customization: {
                            code: customization.code,
                            name: customization.name,
                            rank: customization.rank
                        },
                        variation: {
                            code: variation.code,
                            name: variation.name
                        }
                    });
                });
            });
            $scope.moreCustomizationsRequestProcessing = false;
        };

        var errorCallback = function(response) {
            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
            $scope.moreCustomizationsRequestProcessing = false;
        };

        var getDefaultStatus = function() {
            return personalizationsmarteditUtils.getStatusesMapping().filter(function(elem) {
                return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
            })[0];
        };

        var getCustomizationsFilterObject = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                currentSize: $scope.pagination.count,
                currentPage: $scope.pagination.page + 1,
                name: $scope.customizationFilter.name,
                statuses: getDefaultStatus().modelStatuses
            };
        };

        var getCustomizations = function(categoryFilter) {
            var params = {
                filter: categoryFilter,
                dataArrayName: 'customizations'
            };
            customizationDataFactory.updateData(params, successCallback, errorCallback);
        };

        $scope.pagination = new PaginationHelper();
        $scope.pagination.reset();

        $scope.combinedView = personalizationsmarteditContextService.getCombinedView();
        $scope.selectedItems = [];
        angular.copy($scope.combinedView.selectedItems || [], $scope.selectedItems);
        $scope.selectedElement = {};
        $scope.selectionArray = [];

        $scope.moreCustomizationsRequestProcessing = false;
        $scope.addMoreItems = function() {
            if ($scope.pagination.page < $scope.pagination.totalPages - 1 && !$scope.moreCustomizationsRequestProcessing) {
                $scope.moreCustomizationsRequestProcessing = true;
                getCustomizations(getCustomizationsFilterObject());
            }
        };

        $scope.selectElement = function(item) {
            $scope.selectedItems.push(item);
            $scope.selectedItems.sort(function(a, b) {
                return a.customization.rank - b.customization.rank;
            });

            $scope.selectedElement = null;
            $scope.searchInputKeypress(null, '');
        };

        $scope.initUiSelect = function(uiSelectController) {
            uiSelectController.isActive = function(item) {
                return false;
            };
        };

        $scope.removeSelectedItem = function(item) {
            $scope.selectedItems.splice($scope.selectedItems.indexOf(item), 1);
            $scope.selectedElement = null;
            $scope.searchInputKeypress(null, '');
        };

        $scope.getClassForElement = personalizationsmarteditUtils.getClassForElement;
        $scope.getLetterForElement = personalizationsmarteditUtils.getLetterForElement;

        $scope.isItemInSelectDisabled = function(item) {
            return $scope.selectedItems.find(function(currentItem) {
                return currentItem.customization.code === item.customization.code;
            });
        };

        $scope.isItemSelected = function(item) {
            return $scope.selectedItems.find(function(currentItem) {
                return currentItem.customization.code === item.customization.code && currentItem.variation.code === item.variation.code;
            });
        };

        $scope.customizationFilter = {
            name: ''
        };

        $scope.searchInputKeypress = function(keyEvent, searchObj) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }
            $scope.pagination.reset();
            $scope.customizationFilter.name = searchObj;
            customizationDataFactory.resetData();
            $scope.addMoreItems();
        };

        var isCombinedViewContextPersRemoved = function(combinedView) {
            return combinedView.selectedItems.filter(function(item) {
                return item.customization.code === combinedView.customize.selectedCustomization.code && item.variation.code === combinedView.customize.selectedVariations.code;
            }).length === 0;
        };

        var buttonHandlerFn = function(buttonId) {
            var deferred = $q.defer();
            if (buttonId === 'confirmOk') {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                combinedView.selectedItems = $scope.selectedItems;

                if (combinedView.enabled && combinedView.customize.selectedVariations !== null && isCombinedViewContextPersRemoved(combinedView)) {
                    combinedView.customize.selectedCustomization = null;
                    combinedView.customize.selectedVariations = null;
                    combinedView.customize.selectedComponents = null;
                }

                personalizationsmarteditContextService.setCombinedView(combinedView);
                return deferred.resolve();
            }
            return deferred.reject();
        };

        $scope.modalManager.setButtonHandler(buttonHandlerFn);

        $scope.$watch('selectedItems', function(newValue, oldValue) {
            $scope.modalManager.disableButton("confirmOk");
            if (newValue !== oldValue) {
                var combinedView = personalizationsmarteditContextService.getCombinedView();
                var arrayEquals = (combinedView.selectedItems || []).length === 0 && $scope.selectedItems.length === 0;
                arrayEquals = arrayEquals || angular.equals(combinedView.selectedItems, $scope.selectedItems);
                if (!arrayEquals) {
                    $scope.modalManager.enableButton("confirmOk");
                }
            }
        }, true);

    });
