angular.module('statusesDropdownModule', [
        'personalizationsmarteditCommons'
    ])
    .controller('statusesDropdownController', function($timeout, personalizationsmarteditUtils, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES) {
        var self = this;

        //Properties
        this.statuses = [];

        //Methods
        this.statusCustomizationsDropdownOpenClose = function(isOpen) {
            if (isOpen) {
                $timeout((function() {
                    angular.element("#dropdownCustomizationsLibrary").controller('uiSelect').close();
                }), 0);
            }
        };

        //Lifecycle methods
        this.$onInit = function() {
            self.statuses = personalizationsmarteditUtils.getStatusesMapping();
            self.selectedStatus = self.statuses.filter(function(elem) {
                return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
            })[0];

            self.setSelectedStatuses({
                value: self.selectedStatus.modelStatuses || []
            });
        };
    })
    .component('statusesDropdown', {
        templateUrl: 'statusesDropdownTemplate.html',
        controller: 'statusesDropdownController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            setSelectedStatuses: '&',
            isCustomizationsEmpty: '&',
            clearCustomizeContext: '&'
        }
    });
