angular.module('searchCustomizationFromLibModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditContextUtilsModule'
    ])
    .controller('searchCustomizationFromLibController', function($timeout, personalizationsmarteditUtils) {
        var self = this;

        //Properties
        this.selectedFromDropdownLibraryCustomizations = [];
        this.searchCustomizationEnabled = false;

        //Methods
        this.customizationSearchInputKeypress = function(keyEvent, searchStr) {
            if (keyEvent && ([37, 38, 39, 40].indexOf(keyEvent.which) > -1)) { //keyleft, keyup, keyright, keydown
                return;
            }

            self.addMoreLibraryCustomizationItems({
                searchObj: searchStr
            });
        };

        this.toggleAddMoreCustomizationsClick = function() {
            this.searchCustomizationEnabled = !this.searchCustomizationEnabled;
        };

        this.libraryCustomizationsDropdownOpenClose = function(isOpen) {
            if (isOpen) {
                $timeout((function() {
                    angular.element("#dropdownCustomizationsStatus").controller('uiSelect').close();
                }), 0);
            }
        };

        this.getActivityStateForCustomization = function(customization) {
            return personalizationsmarteditUtils.getActivityStateForCustomization(customization);
        };

        this.getEnablementTextForCustomization = function(customization) {
            return personalizationsmarteditUtils.getEnablementTextForCustomization(customization, 'personalization.toolbar.pagecustomizations');
        };

        //Lifecycle methods
        this.$onChanges = function(changes) {
            if (changes.isMenuOpen && !changes.isMenuOpen.currentValue) {
                this.searchCustomizationEnabled = false;
            }
        };

    })
    .component('searchCustomizationFromLib', {
        templateUrl: 'searchCustomizationFromLibTemplate.html',
        controller: 'searchCustomizationFromLibController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            addMoreLibraryCustomizationItems: '&',
            addCustomizationFromLibrary: '&',
            libraryCustomizations: '<',
            isMenuOpen: '<'
        }
    });
