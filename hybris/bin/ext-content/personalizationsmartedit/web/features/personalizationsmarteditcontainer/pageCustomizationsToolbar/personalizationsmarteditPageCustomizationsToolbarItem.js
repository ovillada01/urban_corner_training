angular.module('personalizationsmarteditPageCustomizationsToolbarItemModule', [
        'personalizationsmarteditCommons',
        'personalizationsmarteditRestServiceModule',
        'personalizationsmarteditContextServiceModule',
        'personalizationsmarteditManagerModule',
        'personalizationsmarteditDataFactory',
        'personalizationsmarteditContextUtilsModule',
        'searchCustomizationFromLibModule',
        'statusesDropdownModule',
        'pageCustomizationsListModule'
    ])
    .controller('personalizationsmarteditPageCustomizationsToolbarController', function($filter, $timeout, personalizationsmarteditContextService, personalizationsmarteditMessageHandler, personalizationsmarteditDateUtils, personalizationsmarteditIFrameUtils, customizationDataFactory, PaginationHelper, personalizationsmarteditUtils, personalizationsmarteditContextUtils, personalizationsmarteditRestService, PERSONALIZATION_VIEW_STATUS_MAPPING_CODES, personalizationsmarteditCommerceCustomizationService) { //NOSONAR
        var self = this;

        //Private methods
        var refreshCustomizationData = function(customization) {
            personalizationsmarteditRestService.getVariationsForCustomization(customization.code).then(
                function successCallback(response) {
                    customization.variations = response.variations || [];
                    customization.variations.forEach(function(variation) {
                        variation.numberOfCommerceActions = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCount(variation);
                        variation.commerceCustomizations = personalizationsmarteditCommerceCustomizationService.getCommerceActionsCountMap(variation);
                    });
                },
                function errorCallback() {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                });
        };

        var errorCallback = function(response) {
            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
            self.moreCustomizationsRequestProcessing = false;
        };

        var setSelectedItemsInCustomizeContext = function(selectedVariations, selectedCustomization) {
            var customize = personalizationsmarteditContextService.getCustomize();

            customize.selectedCustomization = selectedCustomization;
            customize.selectedVariations = selectedVariations;

            personalizationsmarteditContextService.setCustomize(customize);
        };

        var refreshCustomizeContext = function(selectedVariation, selectedCustomization) {
            if (angular.isObject(selectedVariation)) {
                if (!angular.isArray(selectedVariation)) {
                    var newVariation = selectedCustomization.variations.find(function(elem) {
                        return angular.equals(elem.code, selectedVariation.code);
                    });
                    setSelectedItemsInCustomizeContext(newVariation, selectedCustomization);
                } else {
                    setSelectedItemsInCustomizeContext(selectedVariation, selectedCustomization);
                }
            }
        };

        var getCustomizationsForSelectedStatuses = function() {
            angular.forEach(self.customizationsOnPage, function(valueOnPage, keyOnPage) {
                angular.forEach(self.customizationsSelectedFromLibrary, function(valueFromLibrary, keyFromLibrary) {
                    if (valueOnPage.code === valueFromLibrary.code) {
                        self.customizationsSelectedFromLibrary.splice(keyFromLibrary, 1);
                    }
                });
            });
            return ((self.customizationsOnPage || []).concat(self.customizationsSelectedFromLibrary || []))
                .filter(function(customization) {
                    return self.hasSelectedStatus(customization);
                });
        };

        var refreshCustomizationList = function() {
            self.customizationsList = getCustomizationsForSelectedStatuses();
        };

        var successCallback = function(response) {
            self.pagination = new PaginationHelper(response.pagination);
            self.moreCustomizationsRequestProcessing = false;

            //Update context, because value could change name
            var customize = personalizationsmarteditContextService.getCustomize();
            var currentCustomization = customize.selectedCustomization;
            var currentVariations = customize.selectedVariations;

            if (angular.isObject(currentCustomization)) {
                var newCustomization = getCustomizationsForSelectedStatuses().filter(function(elem) {
                    return angular.equals(elem.code, currentCustomization.code);
                });

                if (newCustomization.length === 0) {
                    personalizationsmarteditRestService.getCustomization(currentCustomization.code)
                        .then(function successCallback(response) {
                            currentCustomization.name = response.name;
                            currentCustomization.variations = angular.copy(response.variations);

                            Array.prototype.push.apply(self.customizationsOnPage, [currentCustomization]);

                            refreshCustomizeContext(currentVariations, currentCustomization);
                            refreshCustomizationList();
                        }, function errorCallback() {
                            personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomization'));
                        });
                } else {
                    if (newCustomization.length > 1) {
                        customizationDataFactory.items.splice(customizationDataFactory.items.indexOf(newCustomization[1]), 1);
                    }
                    refreshCustomizeContext(currentVariations, newCustomization[0]);

                    refreshCustomizationData(newCustomization[0]);
                }
            }

            refreshCustomizationList();
        };

        var getCustomizations = function(categoryFilter) {
            var params = {
                filter: categoryFilter,
                dataArrayName: 'customizations'
            };
            customizationDataFactory.updateData(params, successCallback, errorCallback);
        };

        var getCustomizationsFilterObject = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                currentSize: self.pagination.count,
                currentPage: self.pagination.page + 1,
                statuses: self.selectedStatuses
            };
        };

        var getCustomizationsFilterObjectForLibrary = function() {
            return {
                pageId: personalizationsmarteditContextService.getSeData().pageId,
                negatePageId: true,
                name: self.libraryCustomizationFilter.name,
                currentSize: self.libraryCustPagination.count,
                currentPage: self.libraryCustPagination.page + 1,
                statuses: personalizationsmarteditUtils.getStatusesMapping().filter(function(elem) {
                    return elem.code === PERSONALIZATION_VIEW_STATUS_MAPPING_CODES.ALL;
                })[0].modelStatuses
            };
        };

        var refreshGrid = function() {
            $timeout(function() {
                getCustomizations(getCustomizationsFilterObject());
            }, 0);
        };

        var removeArrayFromArrayByCode = function(fromArray, toRemoveArray) {
            var filteredArray = fromArray.filter(function(elem) {
                return toRemoveArray.map(function(e) {
                    return e.code;
                }).indexOf(elem.code) < 0;
            });

            return filteredArray;
        };

        //Properties
        this.selectedStatuses = [];
        this.customizationsOnPage = [];
        this.customizationsList = [];
        this.libraryCustPagination = {};
        this.libraryCustomizationFilter = {};
        this.moreLibraryCustomizationsRequestProcessing = false;
        this.libraryCustomizations = [];
        this.customizationsSelectedFromLibrary = [];

        this.setSelectedStatuses = function(selectedStatuses) {
            self.selectedStatuses = selectedStatuses;

            self.pagination.reset();
            customizationDataFactory.resetData();
            self.addMoreCustomizationItems();
        };

        this.clearCustomizeContext = function() {
            personalizationsmarteditContextUtils.clearCustomizeContextAndReloadPreview(personalizationsmarteditIFrameUtils, personalizationsmarteditContextService);
        };

        this.isCustomizationsEmpty = function() {
            return self.customizationsList.length < 1;
        };

        this.hasSelectedStatus = function(customization) {
            return (self.selectedStatuses || []).indexOf(customization.status) > -1;
        };

        this.addMoreCustomizationItems = function() {
            if (self.pagination.page < self.pagination.totalPages - 1 && !self.moreCustomizationsRequestProcessing) {
                self.moreCustomizationsRequestProcessing = true;
                refreshGrid();
            }
        };

        this.addMoreLibraryCustomizationItems = function(searchObj) {
            if (angular.isDefined(searchObj)) {
                self.libraryCustPagination.reset();
                self.libraryCustomizationFilter.name = searchObj;
                self.libraryCustomizations.length = 0;
            }

            if (self.libraryCustPagination.page < self.libraryCustPagination.totalPages - 1 && !self.moreLibraryCustomizationsRequestProcessing) {
                self.moreLibraryCustomizationsRequestProcessing = true;
                personalizationsmarteditRestService.getCustomizations(getCustomizationsFilterObjectForLibrary()).then(function successCallback(response) {
                    var filteredCategories = removeArrayFromArrayByCode(response.customizations, self.customizationsSelectedFromLibrary);
                    var customization = personalizationsmarteditContextService.getCustomize().selectedCustomization;
                    if (customization) {
                        filteredCategories = removeArrayFromArrayByCode(filteredCategories, [customization]);
                    }
                    filteredCategories.forEach(function(customization) {
                        customization.fromLibrary = true;
                    });

                    Array.prototype.push.apply(self.libraryCustomizations, filteredCategories);

                    self.libraryCustPagination = new PaginationHelper(response.pagination);
                    self.moreLibraryCustomizationsRequestProcessing = false;
                }, function errorCallback(response) {
                    personalizationsmarteditMessageHandler.sendError($filter('translate')('personalization.error.gettingcustomizations'));
                    self.moreLibraryCustomizationsRequestProcessing = false;
                });
            }
        };

        this.addCustomizationFromLibrary = function(selFromLib) {
            if (!Array.isArray(selFromLib)) {
                selFromLib = [selFromLib];
            }

            Array.prototype.push.apply(self.customizationsSelectedFromLibrary, selFromLib);
            self.libraryCustomizations = removeArrayFromArrayByCode(self.libraryCustomizations, selFromLib);

            refreshCustomizationList();
        };

        //Lifecycle methods
        this.$onInit = function() {
            personalizationsmarteditContextService.refreshExperienceData();

            self.pagination = new PaginationHelper();
            self.pagination.reset();

            self.libraryCustPagination = new PaginationHelper();
            self.libraryCustPagination.reset();

            self.moreLibraryCustomizationsRequestProcessing = false;
            self.moreCustomizationsRequestProcessing = false;

            self.customizationsOnPage = customizationDataFactory.items;

            customizationDataFactory.resetData();
        };

        this.$onChanges = function(changes) {
            if (changes.isMenuOpen && changes.isMenuOpen.currentValue) {
                self.pagination.reset();
                customizationDataFactory.resetData();
                self.moreCustomizationsRequestProcessing = false;
                self.addMoreCustomizationItems();

                self.moreLibraryCustomizationsRequestProcessing = false;
                self.libraryCustPagination.reset();
                self.libraryCustomizations.length = 0;
                self.addMoreLibraryCustomizationItems();

                var customize = personalizationsmarteditContextService.getCustomize();
                for (var i = self.customizationsSelectedFromLibrary.length - 1; i >= 0; i--) {
                    if (self.customizationsSelectedFromLibrary[i].code !== (customize.selectedCustomization || {}).code) {
                        self.customizationsSelectedFromLibrary.splice(i, 1);
                    }
                }

            }
        };
    })
    .component('personalizationsmarteditPageCustomizationsToolbarItem', {
        templateUrl: 'personalizationsmarteditPageCustomizationsToolbarItemTemplate.html',
        controller: 'personalizationsmarteditPageCustomizationsToolbarController',
        controllerAs: 'ctrl',
        transclude: true,
        bindings: {
            isMenuOpen: '<'
        }
    });
