angular.module('personalizationsmarteditContextMenu', [
        'gatewayProxyModule'
    ])
    .factory('personalizationsmarteditContextModal', function(gatewayProxy) {

        var PersonalizationsmarteditContextModal = function() { //NOSONAR
            this.gatewayId = "personalizationsmarteditContextModal";
            gatewayProxy.initForService(this);
        };

        PersonalizationsmarteditContextModal.prototype.openDeleteAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openAddAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openEditAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openInfoAction = function() {};

        PersonalizationsmarteditContextModal.prototype.openEditComponentAction = function(configuration) {};

        PersonalizationsmarteditContextModal.prototype.openShowActionList = function() {};

        return new PersonalizationsmarteditContextModal();
    })
    .factory('personalizationsmarteditContextModalHelper', function(personalizationsmarteditContextModal, personalizationsmarteditContextService) {
        var helper = {};

        var getSelectedVariationCode = function() {
            if (personalizationsmarteditContextService.getCombinedView().enabled) {
                return personalizationsmarteditContextService.getCombinedView().customize.selectedVariations.code;
            }
            return personalizationsmarteditContextService.getCustomize().selectedVariations.code;
        };

        var getSelectedCustomizationCode = function() {
            if (personalizationsmarteditContextService.getCombinedView().enabled) {
                return personalizationsmarteditContextService.getCombinedView().customize.selectedCustomization.code;
            }
            return personalizationsmarteditContextService.getCustomize().selectedCustomization.code;
        };

        helper.openDeleteAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = configProperties.smarteditPersonalizationVariationId || null;
            configurationToPass.selectedCustomizationCode = configProperties.smarteditPersonalizationCustomizationId || null;

            return personalizationsmarteditContextModal.openDeleteAction(configurationToPass);
        };

        helper.openAddAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = getSelectedVariationCode();
            configurationToPass.selectedCustomizationCode = getSelectedCustomizationCode();

            return personalizationsmarteditContextModal.openAddAction(configurationToPass);
        };

        helper.openEditAction = function(config) {
            var configProperties = angular.fromJson(config.properties);

            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            configurationToPass.containerId = config.containerId;
            configurationToPass.slotId = config.slotId;
            configurationToPass.actionId = configProperties.smarteditPersonalizationActionId || null;
            configurationToPass.selectedVariationCode = configProperties.smarteditPersonalizationVariationId || null;
            configurationToPass.selectedCustomizationCode = configProperties.smarteditPersonalizationCustomizationId || null;

            return personalizationsmarteditContextModal.openEditAction(configurationToPass);
        };

        helper.openInfoAction = function(config) {
            return personalizationsmarteditContextModal.openInfoAction();
        };

        helper.openEditComponentAction = function(config) {
            var configurationToPass = {};
            configurationToPass.componentType = config.componentType;
            configurationToPass.componentId = config.componentId;
            return personalizationsmarteditContextModal.openEditComponentAction(configurationToPass);
        };

        helper.openShowActionList = function(config) {
            var configurationToPass = {};
            configurationToPass.containerId = config.containerId;
            return personalizationsmarteditContextModal.openShowActionList(configurationToPass);
        };

        return helper;
    });
