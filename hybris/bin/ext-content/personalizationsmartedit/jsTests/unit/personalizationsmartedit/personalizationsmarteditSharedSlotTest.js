describe('personalizationsmarteditSharedSlot', function() {
    var mockModules = {};
    setupMockModules(mockModules);

    var $compile, $rootScope;

    beforeEach(module('personalizationsmarteditTemplates'));
    beforeEach(module('personalizationsmarteditSharedSlotDecorator'));
    beforeEach(inject(function(_$compile_, _$rootScope_, $templateCache) {
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        var directiveTemplate = $templateCache.get('web/features/personalizationsmartedit/sharedSlotDecorator/personalizationsmarteditSharedSlotDecoratorTemplate.html');
        $templateCache.put('personalizationsmarteditSharedSlotDecoratorTemplate.html', directiveTemplate);
    }));

    it('Replaces the element with the appropriate content', function() {
        // given
        var element = $compile("<div class=\"personalizationsmarteditSharedSlot\" data-active=\"true\" data-smartedit-component-id=\"Test\"></div>")($rootScope);
        // when
        $rootScope.$apply();
        // then
        var subText = "<div>\n" + "    <div class=\"cmsx-ctx-wrapper1 se-slot-contextual-menu-level1\">\n";
        expect(element.html().substring(0, subText.length)).toContain(subText);
    });

});
