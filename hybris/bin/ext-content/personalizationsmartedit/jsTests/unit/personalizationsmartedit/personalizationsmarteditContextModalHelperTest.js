describe('personalizationsmarteditContextModalHelper', function() {
    var mockModules = {};
    setupMockModules(mockModules);

    var personalizationsmarteditContextModalHelper, personalizationsmarteditContextModal;

    var mockConfigProperties = {
        name: "name",
        smarteditPersonalizationActionId: "actionId",
        smarteditPersonalizationVariationId: "variationCode",
        smarteditPersonalizationCustomizationId: "customizationCode"
    };
    var mockConfig = {
        properties: JSON.stringify(mockConfigProperties),
        componentId: "id",
        componentType: "type",
        containerId: "containerId",
        slotId: "slotId"
    };

    var mockCustomize = {
        selectedCustomization: {
            code: "mockNameCustomization"
        },
        selectedVariations: {
            code: "mockNameVariation"
        },
        selectedComponents: null
    };

    beforeEach(module('personalizationsmarteditContextMenu', function($provide) {
        mockModules.personalizationsmarteditContextService = jasmine.createSpyObj('personalizationsmarteditContextService', ['getCustomize', 'getCombinedView']);
        $provide.value('personalizationsmarteditContextService', mockModules.personalizationsmarteditContextService);
    }));
    beforeEach(inject(function(_personalizationsmarteditContextModalHelper_, _personalizationsmarteditContextModal_) {
        personalizationsmarteditContextModalHelper = _personalizationsmarteditContextModalHelper_;
        personalizationsmarteditContextModal = _personalizationsmarteditContextModal_;
        spyOn(personalizationsmarteditContextModal, 'openDeleteAction').and.callThrough();
        spyOn(personalizationsmarteditContextModal, 'openAddAction').and.callThrough();
        spyOn(personalizationsmarteditContextModal, 'openEditAction').and.callThrough();
        spyOn(personalizationsmarteditContextModal, 'openInfoAction').and.callThrough();
        spyOn(personalizationsmarteditContextModal, 'openEditComponentAction').and.callThrough();
        mockModules.personalizationsmarteditContextService.getCustomize.and.callFake(function() {
            return mockCustomize;
        });
        mockModules.personalizationsmarteditContextService.getCombinedView.and.callFake(function() {
            return {
                enabled: false
            };
        });
    }));

    describe('openDeleteAction', function() {

        it('should be defined', function() {
            expect(personalizationsmarteditContextModalHelper.openDeleteAction).toBeDefined();
        });

        it('should call proper service with parameters', function() {
            personalizationsmarteditContextModalHelper.openDeleteAction(mockConfig);
            expect(personalizationsmarteditContextModal.openDeleteAction).toHaveBeenCalledWith({
                containerId: 'containerId',
                slotId: 'slotId',
                actionId: "actionId",
                selectedVariationCode: "variationCode",
                selectedCustomizationCode: "customizationCode"
            });
        });

    });

    describe('openAddAction', function() {

        it('should be defined', function() {
            expect(personalizationsmarteditContextModalHelper.openAddAction).toBeDefined();
        });

        it('should call proper service with parameters', function() {

            mockModules.personalizationsmarteditContextService.selectedCustomizations = {
                code: "mockNameCustomization",
            };
            mockModules.personalizationsmarteditContextService.selectedVariations = {
                code: "mockNameVariation",
            };

            personalizationsmarteditContextModalHelper.openAddAction(mockConfig);
            expect(personalizationsmarteditContextModal.openAddAction).toHaveBeenCalledWith({
                componentType: "type",
                componentId: "id",
                containerId: 'containerId',
                slotId: 'slotId',
                actionId: "actionId",
                selectedVariationCode: "mockNameVariation",
                selectedCustomizationCode: "mockNameCustomization"
            });
        });

    });

    describe('openEditAction', function() {

        it('should be defined', function() {
            expect(personalizationsmarteditContextModalHelper.openEditAction).toBeDefined();
        });

        it('should call proper service', function() {
            personalizationsmarteditContextModalHelper.openEditAction(mockConfig);
            expect(personalizationsmarteditContextModal.openEditAction).toHaveBeenCalledWith({
                componentType: "type",
                componentId: "id",
                containerId: 'containerId',
                slotId: 'slotId',
                actionId: "actionId",
                selectedVariationCode: "variationCode",
                selectedCustomizationCode: "customizationCode"
            });
        });

    });

    describe('openInfoAction', function() {

        it('should be defined', function() {
            expect(personalizationsmarteditContextModalHelper.openInfoAction).toBeDefined();
        });

        it('should call proper service', function() {
            personalizationsmarteditContextModalHelper.openInfoAction(mockConfig);
            expect(personalizationsmarteditContextModal.openInfoAction).toHaveBeenCalledWith();
        });

    });

    describe('openEditComponentAction', function() {

        it('should be defined', function() {
            expect(personalizationsmarteditContextModalHelper.openEditComponentAction).toBeDefined();
        });

        it('should call proper service', function() {
            personalizationsmarteditContextModalHelper.openEditComponentAction(mockConfig);
            expect(personalizationsmarteditContextModal.openEditComponentAction).toHaveBeenCalledWith({
                componentType: "type",
                componentId: "id"
            });
        });

    });

});
