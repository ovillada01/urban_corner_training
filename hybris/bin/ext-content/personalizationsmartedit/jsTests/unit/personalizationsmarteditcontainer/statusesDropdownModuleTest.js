describe('statusesDropdownModule', function() {
    var mockModules = {};
    setupMockModules(mockModules);

    var $componentController;

    beforeEach(module('statusesDropdownModule'));
    beforeEach(inject(function(_$componentController_) {
        $componentController = _$componentController_;
    }));

    describe('Component API', function() {

        it('should have proper api when initialized without parameters', function() {
            var ctrl = $componentController('statusesDropdown', null);

            expect(ctrl.statuses.length).toBe(0);
            expect(ctrl.statusCustomizationsDropdownOpenClose).toBeDefined();
            expect(ctrl.$onInit).toBeDefined();
        });
    });
});
