describe('searchCustomizationFromLibModule', function() {
    var mockModules = {};
    setupMockModules(mockModules);

    var mockVariation = {
        code: "testVariation"
    };
    var mockCustomization = {
        code: "testCustomization",
        variations: [mockVariation]
    };

    var $componentController, scope, $timeout;

    beforeEach(module('personalizationsmarteditRestServiceModule', function($provide) {
        mockModules.personalizationsmarteditRestService = jasmine.createSpyObj('personalizationsmarteditRestService', ['getCustomizations']);
        $provide.value('personalizationsmarteditRestService', mockModules.personalizationsmarteditRestService);
    }));

    beforeEach(module('searchCustomizationFromLibModule'));
    beforeEach(inject(function(_$q_, _$timeout_, _$rootScope_, _$componentController_) {
        $componentController = _$componentController_;
        scope = _$rootScope_.$new();
        $timeout = _$timeout_;

        mockModules.personalizationsmarteditRestService.getCustomizations.and.callFake(function() {
            var deferred = _$q_.defer();
            deferred.resolve({
                customizations: [mockCustomization],
                pagination: {
                    count: 5,
                    page: 0,
                    totalCount: 5,
                    totalPages: 1
                }
            });
            return deferred.promise;
        });
    }));

    describe('Component API', function() {

        it('should have proper api when initialized without parameters', function() {
            var ctrl = $componentController('searchCustomizationFromLib', null);

            expect(ctrl.searchCustomizationEnabled).toBe(false);
            expect(ctrl.selectedFromDropdownLibraryCustomizations).toBeDefined();
            expect(ctrl.customizationSearchInputKeypress).toBeDefined();
            expect(ctrl.toggleAddMoreCustomizationsClick).toBeDefined();
            expect(ctrl.getActivityStateForCustomization).toBeDefined();
            expect(ctrl.getEnablementTextForCustomization).toBeDefined();
            expect(ctrl.libraryCustomizationsDropdownOpenClose).toBeDefined();
        });
    });

    describe('toggleAddMoreCustomizationsClick', function() {

        it('after called, searchCustomizationEnabled should be negated', function() {
            //given
            var ctrl = $componentController('searchCustomizationFromLib', null);
            var searchCustEnabled = false;
            ctrl.searchCustomizationEnabled = searchCustEnabled;

            // when
            ctrl.toggleAddMoreCustomizationsClick();

            // then
            expect(ctrl.searchCustomizationEnabled).toBeDefined();
            expect(ctrl.searchCustomizationEnabled).toBe(!searchCustEnabled);
        });
    });
});
