describe('personalizationsmarteditPageCustomizationsToolbarItemModule', function() {
    var mockModules = {};
    setupMockModules(mockModules);

    var mockVariation = {
        code: "testVariation"
    };
    var mockCustomization = {
        code: "testCustomization",
        variations: [mockVariation],
        status: "stat1"
    };

    var mockLibraryCustomization = {
        code: "testLibraryCustomization",
        variations: [mockVariation],
        status: "stat1"
    };

    var mockSelectedStatuses = ["stat1", "stat2"];

    var $componentController, $timeout;

    beforeEach(module('personalizationsmarteditRestServiceModule', function($provide) {
        mockModules.personalizationsmarteditRestService = jasmine.createSpyObj('personalizationsmarteditRestService', ['getCustomizations']);
        $provide.value('personalizationsmarteditRestService', mockModules.personalizationsmarteditRestService);
    }));

    beforeEach(module('personalizationsmarteditPageCustomizationsToolbarItemModule', function($provide) {
        mockModules.personalizationsmarteditIFrameUtils = jasmine.createSpyObj('personalizationsmarteditIFrameUtils', ['reloadPreview']);
        $provide.value('personalizationsmarteditIFrameUtils', mockModules.personalizationsmarteditIFrameUtils);
    }));

    beforeEach(inject(function(_$q_, _$timeout_, _$componentController_) {
        $componentController = _$componentController_;
        $timeout = _$timeout_;

        mockModules.sharedDataService.get.and.callFake(function() {
            var deferred = _$q_.defer();
            deferred.resolve({});
            return deferred.promise;
        });

        mockModules.personalizationsmarteditRestService.getCustomizations.and.callFake(function(args) {
            var deferred = _$q_.defer();
            var retCustomizatons = [];
            if (args.negatePageId) {
                retCustomizatons = [mockLibraryCustomization];
            } else {
                retCustomizatons = [mockCustomization, mockCustomization];
            }
            deferred.resolve({
                customizations: retCustomizatons,
                pagination: {
                    count: 5,
                    page: 0,
                    totalCount: 5,
                    totalPages: 1
                }
            });
            return deferred.promise;
        });
    }));

    describe('Component API', function() {

        it('should have proper api when initialized without parameters', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);

            expect(ctrl.customizationsList.length).toBe(0);
            expect(ctrl.customizationsSelectedFromLibrary.length).toBe(0);
            expect(ctrl.selectedStatuses.length).toBe(0);
            expect(ctrl.customizationsOnPage.length).toBe(0);
            expect(ctrl.customizationsList.length).toBe(0);
            expect(ctrl.libraryCustomizations.length).toBe(0);
            expect(ctrl.customizationsSelectedFromLibrary.length).toBe(0);
            expect(ctrl.setSelectedStatuses).toBeDefined();
            expect(ctrl.clearCustomizeContext).toBeDefined();
            expect(ctrl.isCustomizationsEmpty).toBeDefined();
            expect(ctrl.hasSelectedStatus).toBeDefined();
            expect(ctrl.addMoreCustomizationItems).toBeDefined();
            expect(ctrl.addMoreLibraryCustomizationItems).toBeDefined();
            expect(ctrl.addCustomizationFromLibrary).toBeDefined();
            expect(ctrl.$onInit).toBeDefined();
        });
    });

    describe('customizationsOnPage', function() {

        it('should be instantianed and empty', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);

            expect(ctrl.customizationsOnPage).toBeDefined();
            expect(ctrl.customizationsOnPage.length).toBe(0);
        });

    });

    describe('customizationsList', function() {

        it('should be instantianed and empty', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);

            expect(ctrl.customizationsList).toBeDefined();
            expect(ctrl.customizationsList.length).toBe(0);
        });

    });

    describe('libraryCustomizations', function() {

        it('should be instantianed and empty', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);

            expect(ctrl.libraryCustomizations).toBeDefined();
            expect(ctrl.libraryCustomizations.length).toBe(0);
        });

    });

    describe('customizationsSelectedFromLibrary', function() {

        it('should be instantianed and empty', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);

            expect(ctrl.customizationsSelectedFromLibrary).toBeDefined();
            expect(ctrl.customizationsSelectedFromLibrary.length).toBe(0);
        });

    });

    describe('getCustomization', function() {

        it('after called array ctrl.customizationsOnPage should contain objects return by REST service', function() {
            //given
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            ctrl.selectedStatuses = mockSelectedStatuses;
            // when
            ctrl.$onInit();
            ctrl.addMoreCustomizationItems();

            $timeout.flush();
            // then
            expect(ctrl.customizationsOnPage).toBeDefined();
            expect(ctrl.customizationsOnPage.length).toBe(2);
            expect(ctrl.customizationsOnPage).toContain(mockCustomization);
        });

    });

    describe('addMoreCustomizationItems', function() {

        it('after called array ctrl.customizationsOnPage should contain objects return by REST service', function() {
            //given
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            ctrl.selectedStatuses = mockSelectedStatuses;
            // when
            ctrl.$onInit();
            ctrl.addMoreCustomizationItems();

            $timeout.flush();
            // then
            expect(ctrl.customizationsOnPage).toBeDefined();
            expect(ctrl.customizationsOnPage.length).toBe(2);
            expect(ctrl.customizationsOnPage).toContain(mockCustomization);
        });

    });

    describe('addMoreLibraryCustomizationItems', function() {

        it('after called array ctrl.customizationsOnPage should contain objects return by REST service', function() {
            //given
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            ctrl.selectedStatuses = mockSelectedStatuses;
            // when
            ctrl.$onInit();
            ctrl.addMoreLibraryCustomizationItems();

            $timeout.flush();
            // then
            expect(ctrl.libraryCustomizations).toBeDefined();
            expect(ctrl.libraryCustomizations.length).toBe(1);
            expect(ctrl.libraryCustomizations).toContain(mockLibraryCustomization);
        });

    });

    describe('$onChanges', function() {

        it('should be defined', function() {
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            expect(ctrl.$onChanges).toBeDefined();
        });

        it('should not set properties if called without parameters', function() {
            //given
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            ctrl.selectedStatuses = mockSelectedStatuses;
            // when
            ctrl.$onChanges({});

            // then
            expect(ctrl.customizationsOnPage).toBeDefined();
            expect(ctrl.customizationsOnPage.length).toBe(0);
            expect(ctrl.libraryCustomizations).toBeDefined();
            expect(ctrl.libraryCustomizations.length).toBe(0);
        });

        it('should not set properties if called without parameters', function() {
            //given
            var ctrl = $componentController('personalizationsmarteditPageCustomizationsToolbarItem', null);
            ctrl.selectedStatuses = mockSelectedStatuses;
            var changes = {
                isMenuOpen: {
                    currentValue: true
                }
            };
            // when
            ctrl.$onInit();
            ctrl.$onChanges(changes);

            $timeout.flush();
            // then
            expect(ctrl.customizationsOnPage).toBeDefined();
            expect(ctrl.customizationsOnPage.length).toBe(2);
            expect(ctrl.libraryCustomizations).toBeDefined();
            expect(ctrl.libraryCustomizations.length).toBe(1);
            expect(ctrl.customizationsList).toBeDefined();
            expect(ctrl.customizationsList.length).toBe(2);
        });

    });
});
