var DEFAULT_IMPLICIT_WAIT = 1000;

exports.config = {
    allScriptsTimeout: 10000,

    specs: [
        '../**/*Test.js'
    ],

    seleniumServerJar: '../../../node_modules/protractor/selenium/selenium-server-standalone-3.0.1.jar',

    capabilities: {
        'browserName': 'phantomjs',
        'phantomjs.binary.path': 'node_modules/karma-phantomjs-launcher/node_modules/phantomjs/bin/phantomjs',
        'phantomjs.ghostdriver.cli.args': ['--loglevel=INFO'],
        'maxInstances': 4
    },

    //    capabilities: {
    //        'browserName': 'chrome',
    //        'shardTestFiles': false,
    //        'maxInstances': 10,
    //        'chromeOptions': {
    //            args: ['lang=en-US']
    //        }
    //    },
    //    chromeDriver: '../../../node_modules/protractor/selenium/chromedriver/chromedriver',
    //    directConnect: true,

    troubleshoot: false,

    baseUrl: 'http://127.0.0.1:7000',

    framework: 'jasmine2',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    },

    //method found in node_modules/protractor/docs/referenceConf.js :
    onPrepare: function() {

        //screenshot utility
        var fs = require('fs');
        global.ScreenshotUtils = {
            screenshotDirectory: "./jsTarget/test/protractor/screenshots/",

            createScreenshotDir: function() {
                var dir_split = this.screenshotDirectory.split('/');
                var dir_current = ".";

                for (var i = 1; i < dir_split.length - 1; i++) {
                    dir_current += '/' + dir_split[i];

                    if (!fs.existsSync(dir_current)) {
                        fs.mkdirSync(dir_current);
                    }
                }
            },

            writeScreenshot: function(data, filename) {
                if (!fs.existsSync(this.screenshotDirectory)) {
                    this.createScreenshotDir();
                }

                var screenshotPath = (this.screenshotDirectory + filename);

                var stream = fs.createWriteStream(screenshotPath);
                stream.write(new Buffer(data, 'base64'));
                stream.end();
            }
        };

        global.EC = protractor.ExpectedConditions;

        var jasmineReporters = require('jasmine-reporters');
        jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter({
            consolidateAll: false,
            savePath: 'jsTarget/test/protractor',
            filePrefix: 'TESTS-'
        }));

        //this is protractor but both protractor and browser instances are available in this closure

        browser.setSize = function() {
            return browser.driver.manage().window().setSize(1366, 820);
        };
        browser.setSize();

        // Wait up to 5 seconds for trying to find an element before failing
        browser.driver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT);

        browser.dumpLogs = function() {
            browser.manage().logs().get('browser').then(function(browserLogs) {
                browserLogs.forEach(function(log) {
                    console.log(log.message);
                });
            });
        };


        browser.getInnerHTML = function(source) {
            var ele = browser._getElementFromSource(source);
            return browser.executeScript("return arguments[0].innerHTML;", ele);
        };

        browser.waitForContainerToBeReady = function() {
            //click on load preview button
            return browser.wait(protractor.ExpectedConditions.elementToBeClickable(element(by.id('nav-expander'))), 20000, "could not find burger menu/toolbar when first loading app").then(function() {
                return browser.waitForAngular().then(function() {
                    // wait for any modal overlay to disappear
                    return browser.waitUntilNoModal();
                });
            });
        };

        browser.waitUntilNoModal = function() {
            return true;
        };

        browser.waitForFrameToBeReady = function() {
            /*
             * protractor cannot nicely use browser api until angular app is bootstrapped.
             * to do so it needs to see ng-app attribute.
             * But in our smartEdit setup, app is bootstrapped programmatically, not through ng-app
             * workaround consists then in waiting arbitrary amount fo time
             */
            return browser.wait(function() {
                var initialWaitForAngularEnabled = browser.waitForAngularEnabled();
                browser.waitForAngularEnabled(false);
                browser.waitUntilNoModal();
                return element.all(by.css('body')).then(function(bodyArray) {
                    var hasBody = bodyArray.length === 1;
                    if (hasBody && initialWaitForAngularEnabled) {
                        browser.waitForAngularEnabled(true);
                    }
                    return hasBody;
                });
            }, 30000, "could not find data-smartedit-ready='true' attribute on the iframe body tag");
        };

        browser.switchToIFrame = function(waitForFrameToBeReady) {
            return browser.switchToParent().then(function() {
                return browser.driver.switchTo().frame(element(by.tagName('iframe')).getWebElement('')).then(function() {
                    if (waitForFrameToBeReady !== false) {
                        return browser.waitForFrameToBeReady();
                    } else {
                        return;
                    }
                });
            });
        };

        browser.waitForWholeAppToBeReady = function() {
            return browser.waitForContainerToBeReady().then(function() {
                return browser.switchToIFrame().then(function() {
                    return browser.waitForFrameToBeReady().then(function() {
                        return browser.switchToParent().then(function() {
                            //console.info("whole app is ready");
                            return;
                        });
                    });
                });
            });
        };

        browser.linkAndBackToParent = function(bySelector) {
            return browser.switchToIFrame().then(function() {
                return browser.click(bySelector).then(function() {
                    return browser.switchToParent().then(function() {
                        return browser.waitForWholeAppToBeReady();
                    });
                });
            });
        };

        browser.clickLoadPreview = function() {
            //click on load preview button
            return browser.waitForContainerToBeReady().then(function() {
                return element(by.id('loadPreview')).click();
            });
        };

        browser.switchToParent = function() {
            return browser.driver.switchTo().defaultContent();
        };

        browser.waitForUrlToMatch = function(regex) {
            browser.wait(function() {
                return browser.getCurrentUrl().then(function(url) {
                    return regex.test(url);
                });
            }, 5000, 'URL did not change');
        };

        var disableNgAnimate = function() {
            angular.module('disableNgAnimate', []).run(['$animate', function($animate) {
                $animate.enabled(false);
            }]);
        };
        browser.addMockModule('disableNgAnimate', disableNgAnimate);

        var disableCssAnimate = function() {
            angular
                .module('disableCssAnimate', [])
                .run(function() {
                    var style = document.createElement('style');
                    style.type = 'text/css';
                    style.innerHTML = '* {' +
                        /*CSS transitions*/
                        '-o-transition:none !important; ' +
                        '-moz-transition:none !important; ' +
                        '-ms-transition:none !important; ' +
                        '-webkit-transition:none !important; ' +
                        'transition:none !important; ' +
                        '}';
                    document.getElementsByTagName('head')[0].appendChild(style);
                });
        };

        browser.addMockModule('disableCssAnimate', disableCssAnimate);

        browser._getElementFromSource = function(source) {
            if (typeof source === 'string') {
                return element(by.css(source));
            } else if (source.hasOwnProperty('then')) {
                return source;
            } else {
                return element(source);
            }
        };

        browser.click = function(source, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "could not click on element " + source;
            return browser.waitUntil(protractor.ExpectedConditions.elementToBeClickable(ele), message).then(function() {
                return this._getElementFromSource(source).click();
            }.bind(this));
        };

        browser.clear = function(source, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "could not find element " + source;
            return browser.waitForPresence(source, message).then(function() {
                return ele.clear();
            });
        };

        browser.sendKeys = function(source, text, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "could not find element " + source;
            return browser.waitForPresence(source, message).then(function() {
                return ele.sendKeys(text);
            });
        };

        browser.clearAndSendKeys = function(source, text, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "could not click on element " + source;
            return browser.waitUntil(protractor.ExpectedConditions.elementToBeClickable(ele), message).then(function() {
                return this._getElementFromSource(source).clear().sendKeys(text);
            }.bind(this));
        };

        browser.waitUntil = function(assertionFunction, errorMessage) {
            var message = errorMessage ? errorMessage : "could not match condition";
            return browser.wait(assertionFunction, 5000, errorMessage);
        };

        browser.scrollToBottom = function(scrollElm) {
            return browser.executeScript('arguments[0].scrollTop = arguments[0].scrollHeight;', scrollElm.getWebElement());
        };

        browser.scrollToTop = function(scrollElm) {
            return browser.executeScript('arguments[0].scrollTop = 0;', scrollElm.getWebElement());
        };

        browser.waitForPresence = function(source, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "could not find element " + source;
            return browser.waitUntil(protractor.ExpectedConditions.presenceOf(ele), message);
        };

        browser.waitForNonPresence = function(source, errorMessage) {
            var ele = this._getElementFromSource(source);
            var message = errorMessage ? errorMessage : "should not have found element " + source;
            return browser.waitUntil(EC.not(EC.presenceOf(ele)), message);
        };
        browser.waitForAbsence = browser.waitForNonPresence;

        browser.waitToBeDisplayed = function(source, errorMessage) {
            var message = errorMessage ? errorMessage : "could not find element " + source;
            return browser.waitUntil(function() {
                return this._getElementFromSource(source).isDisplayed().then(function() {
                    return true;
                });
            }.bind(this), errorMessage);
        };

        browser.waitNotToBeDisplayed = function(source, errorMessage) {
            var message = errorMessage ? errorMessage : "could not find element " + source;
            return browser.waitUntil(function() {
                return this._getElementFromSource(source).isDisplayed().then(function() {
                    return false;
                });
            }.bind(this), errorMessage);
        };

        browser.waitForVisibility = function(source) {
            return browser.waitFor(source, "visibilityOf", "Could not verify visibility of " + source);
        };

        browser.waitForInvisibility = function(source) {
            return browser.waitFor(source, "invisibilityOf", "Could not verify invisibility of " + source);
        };

        browser.waitFor = function(source, expectedConditions, errorMessage) {
            return browser.wait(EC[expectedConditions](browser._getElementFromSource(source)), DEFAULT_IMPLICIT_WAIT, (errorMessage ? errorMessage : "Could not verify " + expectedConditions + " for " + source));
        };

        // [ find element helper ]

        browser.findElement = function(source, searchOption, errorMessage) {

            if (!errorMessage) {
                errorMessage = "Could not find element " + source;
            }

            switch (typeof searchOption) {
                case "boolean":
                    searchOption = (searchOption) ? "visibilityOf" : "presenceOf";
                    break;
                case "string":
                    break;
                default:
                    searchOption = "presenceOf";
            }

            browser.waitFor(source, searchOption, errorMessage);
            return browser._getElementFromSource(source);

        };

        // [ scrolling helper ]

        browser.testThatOverflowingContentIsHidden = function(source) {
            var element = browser._getElementFromSource(source);
            return element.getCssValue("height").then(function(height) {
                expect(element.getAttribute("scrollHeight")).toBeGreaterThan(height.replace("px", ""));
            });
        };

        //---------------------------------------------------------------------------------
        //-----------------------------------ACTIONS---------------------------------------
        //---------------------------------------------------------------------------------

        /* all keys of protractor.Key :
         *[ 'NULL', 'CANCEL', 'HELP', 'BACK_SPACE', 'TAB', 'CLEAR', 'RETURN', 'ENTER', 'SHIFT', 'CONTROL',
         *  'ALT', 'PAUSE', 'ESCAPE', 'SPACE', 'PAGE_UP', 'PAGE_DOWN', 'END', 'HOME', 'ARROW_LEFT', 'LEFT',
         *  'ARROW_UP', 'UP', 'ARROW_RIGHT', 'RIGHT', 'ARROW_DOWN', 'DOWN', 'INSERT', 'DELETE', 'SEMICOLON',
         *  'EQUALS', 'NUMPAD0', 'NUMPAD1', 'NUMPAD2', 'NUMPAD3', 'NUMPAD4', 'NUMPAD5', 'NUMPAD6', 'NUMPAD7',
         *  'NUMPAD8', 'NUMPAD9', 'MULTIPLY', 'ADD', 'SEPARATOR', 'SUBTRACT', 'DECIMAL', 'DIVIDE', 'F1',
         *  'F2', 'F3', 'F4', 'F5', 'F6', 'F7', 'F8', 'F9', 'F10', 'F11', 'F12', 'COMMAND', 'META', 'chord' ]
         */

        browser.press = function(protractorKey) {
            browser.actions().sendKeys(protractorKey).perform();
        };

        browser.isDelayed = false;

        var currentDelayedState;

        var controlFlow = browser.driver.controlFlow();
        var originalExecute = browser.driver.controlFlow().execute.bind(controlFlow);
        browser.driver.controlFlow().execute = function() {

            if (currentDelayedState != browser.isDelayed) {
                console.info("switched to isDelayed ", browser.isDelayed);
            }
            currentDelayedState = browser.isDelayed;

            // queue 10ms wait
            var args = arguments;
            if (browser.isDelayed) {
                return originalExecute(function() {
                    return protractor.promise.delayed(10).then(function() {
                        return originalExecute.apply(null, args);
                    });
                });
            } else {
                return originalExecute.apply(null, args);
            }
        };

        //---------------------------------------------------------------------------------
        //---------------------------------ASSERTIONS--------------------------------------
        //---------------------------------------------------------------------------------

        beforeEach(function() {
            jasmine.addMatchers({
                toEqualData: function() {
                    return {
                        compare: function(actual, expected) {
                            var passed = (JSON.stringify(actual) === JSON.stringify(expected));

                            return {
                                pass: JSON.stringify(actual) === JSON.stringify(expected),
                                message: 'Expected ' + actual + (passed ? '' : ' not') + ' to equal ' + expected
                            };
                        }
                    };
                },
                toBeEmptyString: function() {
                    return {
                        compare: function(actual, expected) {
                            return {
                                pass: actual === ''
                            };
                        }
                    };
                },
                toContain: function() {
                    return {
                        compare: function(actual, expected) {
                            return {
                                pass: actual.indexOf(expected) > -1
                            };
                        }
                    };
                },
                toBeDisplayed: function() {
                    return {
                        compare: function(actual) {
                            return {
                                pass: actual.isDisplayed()
                            };
                        }
                    };
                },
                toBeWithinRange: function() {
                    return {
                        compare: function(actual, expected, range) {
                            range = range || 1;
                            return {
                                pass: Math.abs(expected - actual) < range
                            };
                        }
                    };
                },
                toBeAbsent: function() {
                    return {
                        compare: function(locator) {
                            var message = 'Expected element with locator ' + locator + ' to be present in DOM';
                            return {
                                pass: browser.driver.manage().timeouts().implicitlyWait(0).then(function() {
                                    return browser.wait(function() {
                                        return element(locator).isPresent().then(function(isPresent) {
                                            return !isPresent;
                                        });
                                    }, 5000, message).then(function(result) {
                                        return browser.driver.manage().timeouts().implicitlyWait(DEFAULT_IMPLICIT_WAIT).then(function() {
                                            return result;
                                        });
                                    });
                                }),
                                message: message
                            };
                        }
                    };
                }
            });

        });

        afterEach(function(done) {
            browser.waitForAngularEnabled(true);
            done();
        });

    },
    params: {
        implicitWait: DEFAULT_IMPLICIT_WAIT
    }
};
