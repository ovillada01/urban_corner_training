describe('Customization Modal - ', function() {

    beforeEach(function() {
        browser.ignoreSynchronization = true;

        browser.get('jsTests/ui/common/dummysmartedit.html');
        browser.waitForWholeAppToBeReady();
        openPerspectiveSelector();
        clickPersonalizationPerspective();

        clickLibrary();
        clickCreateNewCustomization();
    });

    afterEach(function() {
        browser.driver.manage().deleteAllCookies();

        browser.takeScreenshot().then(function(png) {
            var filename = 'customizationVariationManagerModal' + Date.now() + '.png';
            ScreenshotUtils.writeScreenshot(png, filename);
        });
    });

    it('GIVEN user sets disabled status in basic information tab WHEN user navigates to target group tab THEN proper status is displayed', function() {
        // GIVEN
        enterCustomizationName("customization");

        // WHEN
        clickNext();

        // THEN
        customizationIsDisabled();
    });

    it('GIVEN user sets enabled status in basic information tab WHEN user navigates to target group tab THEN proper status is displayed', function() {
        // GIVEN
        enterCustomizationName("customization");
        toggleStatusConfiguration();

        // WHEN
        clickNext();

        // THEN
        customizationIsEnabled();
    });

    it('GIVEN user sets start and end date in basic information tab WHEN user navigates to target group tab THEN proper dates are displayed', function() {
        var startDate = "3/14/17 8:00 AM";
        var endDate = "7/26/18 8:00 PM";

        // GIVEN
        enterCustomizationName("customization");
        toggleStatusConfiguration();
        clickConfigureDate();
        enterStartDate(startDate);
        enterEndDate(endDate);

        // WHEN
        clickNext();

        // THEN
        customizationStartDateEquals(startDate);
        customizationEndDateEquals(endDate);
    });

    it('GIVEN user sets a target group name WHEN user has no segment related data THEN add button is not clickable', function() {
        // GIVEN
        enterCustomizationName("customization");
        clickNext();
        clickAddTargetGroup();
        enterTargetGroupName("target group");

        // WHEN nothing else is done        

        // THEN
        addButtonIsNotClickable();
    });

    it('GIVEN user sets a target group name WHEN user types a target group name and selects a segment THEN add button is clickable', function() {
        // GIVEN
        enterCustomizationName("customization");
        clickNext();
        clickAddTargetGroup();
        enterTargetGroupName("target group");

        // WHEN
        openSegmentDropdown();
        selectNthSegmentFromTop(0);

        // THEN
        addButtonIsClickable();
    });

    // Actions
    function openPerspectiveSelector() {
        element(by.css("div[class*='ySEPerspectiveSelector']")).element(by.css("a[type='button']")).click();
    }

    function clickPersonalizationPerspective() {
        element(by.cssContainingText("li[class*='ySEPerspectiveList--item'] a", 'PERSONALIZATION')).click();
    }

    function clickLibrary() {
        element(by.css("span[class*='hyicon hyicon-library se-toolbar-menu-ddlb--button__icon']")).element(by.xpath('..')).click();
    }

    function clickCreateNewCustomization() {
        element(by.css("a[data-translate='personalization.toolbar.library.customizationvariationmanagement.name']")).click();
    }

    function enterCustomizationName(customizationName) {
        element(by.model('customization.name')).sendKeys(customizationName);
    }

    function enterTargetGroupName(targetGroupName) {
        element(by.model('edit.name')).sendKeys(targetGroupName);
    }

    function clickNext() {
        element(by.id('confirmNext')).click();
    }

    function clickAddTargetGroup() {
        element(by.css("[data-ng-click*='$ctrl.showSliderPanel()']")).click();
        browser.sleep(500); //wait for slider panel to appear
    }

    function clickOpenFullscreen() {
        element(by.css("[data-ng-click*='toggleSliderFullscreen()']")).click();
    }

    function openSegmentDropdown() {
        element(by.model('ctrl.singleSegment')).element(by.css("a")).click();
    }

    function selectNthSegmentFromTop(n) {
        element.all(by.repeater('item in $select.items')).get(n).click();
    }

    function clickConfirmOk() {
        element(by.id('smartedit-modal-title-confirmation.modal.title')).element(by.xpath('../..')).element(by.id('confirmOk')).click();
    }

    function toggleStatusConfiguration() {
        element(by.model('customization.statusBoolean')).click();
    }

    function clickConfigureDate() {
        element(by.css("a[data-translate='personalization.modal.customizationvariationmanagement.basicinformationtab.details.showdateconfigdata']")).click();
    }

    function enterStartDate(date) {
        var startDateInput = element(by.css("[ng-disabled='!isEditable'][id='customization-start-date']")).sendKeys(date);
    }

    function enterEndDate(date) {
        var endDateInput = element(by.css("[ng-disabled='!isEditable'][id='customization-end-date']")).sendKeys(date);
    }

    // Expectations
    function saveButtonIsClickable() {
        var saveButton = element(by.id('confirmOk'));

        expect(saveButton.isDisplayed()).toBeTruthy();
        expect(saveButton.isEnabled()).toBeTruthy();
    }

    function nextButtonIsNotClickable() {
        var nextButton = element(by.id('confirmNext'));

        expect(nextButton.isDisplayed()).toBeTruthy();
        expect(nextButton.isEnabled()).toBeFalsy();
    }

    function addButtonIsClickable() {
        var addButton = element(by.css("[data-ng-click='$sliderPanelCtrl.sliderPanelConfiguration.modal.save.onClick()']"));

        expect(addButton.isDisplayed()).toBeTruthy();
        expect(addButton.isEnabled()).toBeTruthy();
    }

    function addButtonIsNotClickable() {
        var addButton = element(by.css("[data-ng-click='$sliderPanelCtrl.sliderPanelConfiguration.modal.save.onClick()']"));

        expect(addButton.isDisplayed()).toBeTruthy();
        expect(addButton.isEnabled()).toBeFalsy();
    }

    function customizationIsEnabled() {
        var statusLabel = element(by.binding('customization.status')).getText();
        expect(statusLabel).toEqual("ENABLED");
    }

    function customizationIsDisabled() {
        var statusLabel = element(by.binding('customization.status')).getText();
        expect(statusLabel).toEqual("DISABLED");
    }

    function customizationStartDateEquals(date) {
        var startDateLabel = element(by.binding('customization.enabledStartDate'));
        browser.wait(protractor.ExpectedConditions.textToBePresentInElement(startDateLabel, date), 10000);
    }

    function customizationEndDateEquals(date) {
        var endDateLabel = element(by.binding('customization.enabledEndDate'));
        browser.wait(protractor.ExpectedConditions.textToBePresentInElement(endDateLabel, date), 10000);
    }
});
