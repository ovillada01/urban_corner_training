/*
 *  
 * [y] hybris Platform
 *  
 * Copyright (c) 2000-2011 hybris AG
 * All rights reserved.
 *  
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 *  
 */
package de.hybris.platform.bmecathmc.constants;

@SuppressWarnings("PMD")
public class BmecathmcConstants extends GeneratedBmecathmcConstants
{
	public static final String EXTENSIONNAME = "bmecathmc";
	
	private BmecathmcConstants()
	{
		//empty
	}
	
	
}
