/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.types.populator;

import de.hybris.platform.cms2.servicelayer.services.AttributeDescriptorModelHelperService;
import de.hybris.platform.cmsfacades.data.ComponentTypeAttributeData;
import de.hybris.platform.converters.Populator;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.servicelayer.dto.converter.ConversionException;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Required;

import static java.util.Arrays.asList;
import static java.util.Optional.ofNullable;


/**
 * Populator aimed at setting all necessary information for the receiving end to build a cms item dropdown widget:
 * <ul>
 * <li>identifies the cmsStructureType as {@link #CMS_ITEM_DROPDOWN}</li>
 * <li>marks the dropdown to use {@link #ID_ATTRIBUTE} as idAttribute</li>
 * </ul>
 */
public class CMSItemDropdownComponentTypeAttributePopulator implements
		Populator<AttributeDescriptorModel, ComponentTypeAttributeData>
{

	private static final String MODEL_CLASSES_PATERN = "(.*)Model$";
	private AttributeDescriptorModelHelperService attributeDescriptorModelHelperService;
	private static final String ID_ATTRIBUTE = "uuid";
	private static final String LABEL_ATTRIBUTE_NAME = "name";
	private static final String LABEL_ATTRIBUTE_UID = "uid";
	private final String TYPE_CODE = "typeCode";

	private static final String CMS_ITEM_DROPDOWN = "CMSItemDropdown";

	@Override
	public void populate(final AttributeDescriptorModel source, final ComponentTypeAttributeData target)
			throws ConversionException
	{
		target.setCmsStructureType(CMS_ITEM_DROPDOWN);
		target.setIdAttribute(ID_ATTRIBUTE);
		target.setLabelAttributes(asList(LABEL_ATTRIBUTE_NAME, LABEL_ATTRIBUTE_UID));

		Class<?> type = getAttributeDescriptorModelHelperService().getAttributeClass(source);

		final Map<String, String> paramsMap = ofNullable(target.getParams()).orElse(new HashMap<String, String>());
		paramsMap.put(TYPE_CODE, type.getSimpleName().replaceAll(MODEL_CLASSES_PATERN, "$1"));

		target.setParams(paramsMap);
	}

	@Required
	public void setAttributeDescriptorModelHelperService(
			AttributeDescriptorModelHelperService attributeDescriptorModelHelperService)
	{
		this.attributeDescriptorModelHelperService = attributeDescriptorModelHelperService;
	}

	protected AttributeDescriptorModelHelperService getAttributeDescriptorModelHelperService()
	{
		return attributeDescriptorModelHelperService;
	}

}
