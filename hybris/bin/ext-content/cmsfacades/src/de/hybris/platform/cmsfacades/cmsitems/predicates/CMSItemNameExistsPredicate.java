/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.predicates;

import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminItemService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.servicelayer.exceptions.AmbiguousIdentifierException;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.search.SearchResult;

import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Required;


/**
 * Predicate to test if a given cms item name maps to an existing cms item.
 * <p>
 * Returns <tt>TRUE</tt> if the cms item exists; <tt>FALSE</tt> otherwise.
 * </p>
 */
public class CMSItemNameExistsPredicate implements Predicate<CMSItemModel>
{
	private CMSAdminItemService cmsAdminItemService;

	private CMSAdminSiteService cmsAdminSiteService;

	private Predicate<Object> cloneContextSameAsActiveCatalogVersionPredicate;

	/**
	 * Suppress sonar warning (squid:S1166 | Exception handlers should preserve the original exception) : The exception
	 * is correctly handled in the catch clause.
	 */
	@SuppressWarnings("squid:S1166")
	@Override
	public boolean test(final CMSItemModel cmsItemModel)
	{
		if (cmsItemModel == null)
		{
			return false;
		}
		boolean result = false;
		try
		{
			if (getCloneContextSameAsActiveCatalogVersionPredicate().test(cmsItemModel))
			{
				SearchResult<CMSItemModel> searchResult = getCmsAdminItemService()
						.findByTypeCodeAndName(getCmsAdminSiteService().getActiveCatalogVersion(), cmsItemModel.getItemtype(), cmsItemModel.getName());

				if (searchResult.getCount() == 0)
				{
					result = false;
				}
				else if (searchResult.getCount() == 1)
				{
					result = !searchResult.getResult().get(0).getUid().equals(cmsItemModel.getUid());
				}
				else
				{
					return true;
				}
			}
		}
		catch (UnknownIdentifierException | AmbiguousIdentifierException e)
		{
			result = false;
		}
		return result;
	}

	protected CMSAdminItemService getCmsAdminItemService()
	{
		return cmsAdminItemService;
	}

	@Required
	public void setCmsAdminItemService(final CMSAdminItemService cmsAdminItemService)
	{
		this.cmsAdminItemService = cmsAdminItemService;
	}

	protected CMSAdminSiteService getCmsAdminSiteService()
	{
		return cmsAdminSiteService;
	}

	@Required
	public void setCmsAdminSiteService(final CMSAdminSiteService cmsAdminSiteService)
	{
		this.cmsAdminSiteService = cmsAdminSiteService;
	}

	protected Predicate<Object> getCloneContextSameAsActiveCatalogVersionPredicate()
	{
		return cloneContextSameAsActiveCatalogVersionPredicate;
	}

	@Required
	public void setCloneContextSameAsActiveCatalogVersionPredicate(
			Predicate<Object> cloneContextSameAsActiveCatalogVersionPredicate)
	{
		this.cloneContextSameAsActiveCatalogVersionPredicate = cloneContextSameAsActiveCatalogVersionPredicate;
	}
}
