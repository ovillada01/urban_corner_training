/*
 * [y] hybris Platform
 *
 * Copyright (c) 2017 SAP SE or an SAP affiliate company. All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package de.hybris.platform.cmsfacades.cmsitems.predicates;


import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.catalog.model.CatalogModel;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.model.contents.CMSItemModel;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminItemService;
import de.hybris.platform.cms2.servicelayer.services.admin.CMSAdminSiteService;
import de.hybris.platform.servicelayer.search.impl.SearchResultImpl;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.access.method.P;


@UnitTest
@RunWith(MockitoJUnitRunner.class)
public class CMSItemNameExistsPredicateTest
{
	
	@InjectMocks
	private CMSItemNameExistsPredicate predicate;

	@Mock
	private CMSAdminItemService cmsAdminItemService;

	@Mock
	private CMSAdminSiteService cmsAdminSiteService;
	
	@Mock
	private CatalogVersionModel catalogVersion;

	@Mock
	private CatalogModel catalog;

	@Mock
	private CloneContextSameAsActiveCatalogVersionPredicate cloneContextSameAsActiveCatalogVersionPredicate;
	
	private CMSItemModel cmsItemModel = new CMSItemModel();

	@Before
	public void setup()
	{
		when(cmsAdminSiteService.getActiveCatalogVersion()).thenReturn(catalogVersion);
		when(catalogVersion.getCatalog()).thenReturn(catalog);
		when(cloneContextSameAsActiveCatalogVersionPredicate.test(cmsItemModel)).thenReturn(true);
		
		cmsItemModel.setName("name");
		cmsItemModel.setUid("uid");
	}
	
	@Test
	public void whenItemIsNotPresentInSearchShouldReturnFalse()
	{
		when(cmsAdminItemService.findByTypeCodeAndName(any(), any(), any())).thenReturn(new SearchResultImpl<>(asList(), 0, 50, 0));
		
		final boolean result = predicate.test(cmsItemModel);

		assertThat(result, is(false));
	}

	@Test
	public void whenItemIsPresentInSearchShouldReturnFalseWhenOnlyItselfMatch()
	{
		when(cmsAdminItemService.findByTypeCodeAndName(any(), any(), any())).thenReturn(new SearchResultImpl<>(asList(cmsItemModel), 1, 50, 0));

		final boolean result = predicate.test(cmsItemModel);

		assertThat(result, is(false));
	}

	@Test
	public void whenItemIsPresentInSearchShouldReturnFalseWhenOnlyOtherMatch()
	{
		final CMSItemModel otherCmsItemModel = new CMSItemModel();
		otherCmsItemModel.setName("name");
		otherCmsItemModel.setUid("other-uid");
		when(cmsAdminItemService.findByTypeCodeAndName(any(), any(), any())).thenReturn(new SearchResultImpl<>(asList(otherCmsItemModel), 1, 50, 0));

		final boolean result = predicate.test(cmsItemModel);

		assertThat(result, is(true));
	}

	@Test
	public void whenItemIsPresentInSearchShouldReturnTrueWhenOtherItemMatch()
	{
		final CMSItemModel otherCmsItemModel = new CMSItemModel();
		otherCmsItemModel.setName("name");
		otherCmsItemModel.setUid("other-uid");
		when(cmsAdminItemService.findByTypeCodeAndName(any(), any(), any())).thenReturn(new SearchResultImpl<>(asList(cmsItemModel, otherCmsItemModel), 2, 50, 0));

		final boolean result = predicate.test(cmsItemModel);

		assertThat(result, is(true));
	}

	@Test
	public void whenCloneContextSameAsActiveCatalogVersionPredicateReturnsFalseThenResultIsFalse()
	{
		// GIVEN
		when(cloneContextSameAsActiveCatalogVersionPredicate.test(cmsItemModel)).thenReturn(false);

		// WHEN
		final boolean result = predicate.test(cmsItemModel);

		// THEN
		assertFalse("CMSItemNameExistsPredicateTest should return false whenever clone context does not contain same catalog id and version as active catalog id and version", result);
	}
	
}
